/*
Implement a djkstra that we can use in booksim routing.

The skeleton code is already in python, let's translate that first.
Will worry about the interfaces later.
*/

/*
Data type:
    - We are not saving all the paths. Rather it'll be just the parent tables 
    from which paths can be generated on the fly.
    - For each src, we need to consider parent for each destination. So a 2D vector
    of N*N dimension.
    - Because we consider multiple paths, each cell needs to be a vector as well.
    - So a 3d vector of N*N*<var>
*/

#include <iostream>
#include <vector>
#include <queue>

#include "djkstra.hpp"

#define INF 9999

using namespace std;

std::vector < std::vector <std::vector <short> > > parents;
//std::vector < std::vector <short>  > parents;
int N = 5;

void d3_vector_test();
void check_djkstra();


void d3_vector_test(){
    /*
    Just a simple test to see how to work with a 3D vector, with the 
    3rd dimension of variable length.
    */
    parents.resize(N, std::vector< std::vector<short> > (N, std::vector<short>() ) );
    cout << "vector resized" << endl;
    
    //populate vector
    short val;
    for(short ii = 0; ii<parents.size(); ii++){
        for(short jj = 0; jj<parents[ii].size(); jj++){
            for (short kk =0; kk < ii+jj+1; kk++){
                val = ii * 100 + jj * 10 + kk;
                parents[ii][jj].push_back(val);
            }
        }
    }
    cout << "population done." << endl;
    //print contents
    for(short ii = 0; ii<parents.size(); ii++){
        for(short jj = 0; jj<parents[ii].size(); jj++){
            cout << "(" << ii << "," << jj << ") :" ;
            for (short kk =0; kk < parents[ii][jj].size(); kk++){
                cout << parents[ii][jj][kk] << " ";
            }
            cout << endl;
        }
    }
}

class MyComparison{
    /*  A comparison function object that compares two pairs and
        chooses the one with the smaller second element as the winner.*/
    public:
        MyComparison(){
        }
        
        bool operator() (const std::pair<short,short>& lhs, const std::pair<short,short>& rhs) const{
            return (lhs.second > rhs.second);
        }

};

void djkstra(int src, int N, std::vector < std::vector < std::pair<short,short> > >& graph, std::vector< short >& distance, std::vector< std::vector<short> >& parents){
    /*
    Finds all wieghted shortest paths from src.
    Will populate the Parents list. Will think about its structure later.
    
    src:    the node from where the paths are started
    N:      total nodes in the network
    graph:  Adjacency list along with path weight.
            For each src node, there will be a list of (dst, link_weight)
    */
    
    cout << "inside djkstra ..." << endl;
    cout << "src: " << src << endl;
    cout << "N: " << N << endl;
    
    //std::vector< short > distance(N, INF);
    //std::vector< std::vector<short> > parents(N);
    std::vector< bool > visited(N, false);    
    
    std::priority_queue< std::pair<short,short>, vector<std::pair<short,short>>, MyComparison > min_heap;
    
    std::pair<short,short> top_pair;
    short top_distance, top_node;
    short neighbor_weight, neighbor_node;
    
    //actual algorithm starts. put the top node in heap.
    distance[src] = 0;
    parents[src].push_back(-1); //-1 will work as a path terminator
    
    min_heap.push(std::make_pair(src,distance[src]));    
        //Unlike the python version, here the comparison function in the heap actually
        //works with the second element. So have the distance as the second element.
        
    //while the heap is empty
    while(!min_heap.empty()){
        //pop the top node, the one with minimum weight
        top_pair = min_heap.top();
        top_node = top_pair.first;
        top_distance = top_pair.second;
        
        min_heap.pop();
        
        //mark it as visited
        visited[top_node] = true;
        
        //check each neighbor of the top_node
        for(int ii = 0; ii < graph[top_node].size(); ii++){
            neighbor_node = graph[top_node][ii].first;
            neighbor_weight = graph[top_node][ii].second;
            
            //if neighbor is not already visited:
            if (visited[neighbor_node] == false){
                //weight checking case 1: smaller path
                if ( (distance[top_node] + neighbor_weight) < distance[neighbor_node]) {
                    distance[neighbor_node] = distance[top_node] + neighbor_weight;
                    parents[neighbor_node] = std::vector<short>{top_node};
                    min_heap.push(std::make_pair(neighbor_node, distance[neighbor_node]));
                } 
                //weight checking case 2: equal path     
                else if( (distance[top_node] + neighbor_weight) == distance[neighbor_node]){
                    distance[neighbor_node] = distance[top_node] + neighbor_weight;
                    parents[neighbor_node].push_back(top_node);
                    min_heap.push(std::make_pair(neighbor_node, distance[neighbor_node]));
                }
            }
        }
        
    }//that should be it    
    
    cout << "end of djkstra." << endl;
    
    /*cout << "Test printing the distances table " << endl;
    for(int ii = 0; ii<distance.size(); ii++){
        cout << ii << "," << distance[ii] << endl;
    }
    
    cout << "\ntest printing the parents table:" << endl;
    for(int ii=0; ii<parents.size(); ii++){
        cout << ii << " : ";
        for(int jj=0; jj < parents[ii].size(); jj++){
            cout << parents[ii][jj] << " ";
        }
        cout << endl;
    }*/
    
}

void all_pair_djkstra(int N, std::vector < std::vector < std::pair<short,short> > >& graph, std::vector< std::vector< short >>& distance, std::vector< std::vector< std::vector<short> > >& parents){
    /*
    For every source node in the graph, call djksta.
    */
    
    /*
    N: total nodes
    graph: weighted adjacency list
    distance: 2D vector containing the djksta distance between each SD pair
    parents: 3d vector containing the parent of each node according to djkstra
    */
    
    int src;
    
    for(src = 0; src<N; src++){
        djkstra(src, N, graph, distance[src], parents[src]);
        
        cout << "Test printing the distances table " << endl;
        for(int ii = 0; ii<distance[src].size(); ii++){
            cout << ii << "," << distance[src][ii] << endl;
        }
        
        cout << "\ntest printing the parents table:" << endl;
        for(int ii=0; ii<parents[src].size(); ii++){
            cout << ii << " : ";
            for(int jj=0; jj < parents[src][ii].size(); jj++){
                cout << parents[src][ii][jj] << " ";
            }
            cout << endl;
        }
    }
    
    
    generate_path(1, 2, parents);
}


void generate_path(int src, int dst, std::vector< std::vector< std::vector<short> > >& parents){
    /*
    Given that a parent list is alreayd generated, this function
    generates the path from a given source to a given destination.
    
    src, dst: as they sounds.
    parents: data structure generated by djkstra.
    */
    short src_node = (short) src;
    short current = (short) dst;
    
    std::vector<short> current_path;
    std::vector< std::vector<short> > final_path;
    
    generate_path_internal(src_node, current, current_path, final_path, parents);
    
    cout << "\nBack to outer function: " << endl;
    cout << "src node: " << src_node << endl;
    cout << "current: " << current << endl;
    /*cout << "current path: ";
    for(auto it = current_path.begin(); it!= current_path.end(); it++){
        cout << *it << " ";
    }
    cout << endl;*/
    
    cout << "total path found: " << final_path.size() << endl;
    for(short ii = 0; ii < final_path.size(); ii++){
        cout << "path " << ii << " : ";
        for(short jj = 0; jj < final_path[ii].size(); jj++ ){
            cout << final_path[ii][jj] << " ";
        }
        cout << endl;
    }
    cout << endl;
}

void generate_path_internal(short src_node, short current, std::vector<short>&  current_path, std::vector<std::vector<short> >& final_path, std::vector< std::vector< std::vector<short> > >& parents){
    /*
    Recursive function to generate all the paths between a src and dst. 
    Called by generate_path() from outside.
    
    src_node: the src of the path. The way this function works, it starts from dest and
            comes inward until the src_node is found. That is why this needs to be passed
            around to know when the terminating condition is found.
    current: current node. This is added to the path, and the function 
            is called recursively over its parent(s)
    current_path: running path. Starts with only the dest, and added one node at a time
                until the src is found.
    final_path: 2D vector to hold all the paths. Whenever a path is found, 
                it is appended to this. 
    parents: data structure, populated by djkstra. Basically keep the predecessor node 
            determined through djkstra algorithm.
    
    */

    /* This one is the actual recursive function called by the generate_path() */
    
    //    cout << "src node: " << src_node << endl;
    //    cout << "current : " << current << endl;
    //    cout << "parents: ";
    //    for(size_t ii = 0; ii < parents[src_node][current].size(); ii++){
    //        cout << parents[src_node][current][ii] << " ";
    //    }
    //    cout << endl;
    
    current_path.push_back(current);
    
    //    cout << "current path:" ;
    //    for(size_t ii = 0; ii < current_path.size(); ii++){
    //        cout << current_path[ii] << " ";
    //    }
    //    cout << endl;
    //    
    
    short parent_count = parents[src_node][current].size();
    //cout << "parent_count: " << parent_count << endl;
    
    short parent;

    if (parent_count == 1){
            parent = parents[src_node][current][0];
            
            if (parent == -1){
                //path found. save final result.
                std::vector<short> temp_path(current_path.size(), -1);
                short jj, kk;
                for(jj = current_path.size() - 1, kk = 0; jj >= 0 ; jj--, kk++){
                    //we generated the path from dest to src, so reversing it
                    temp_path[kk] = current_path[jj];
                }
                final_path.push_back(temp_path);
                
            }else{
                //path not found yet. dig deeper.
                
                //no branching. No need to copy the path. Pass by reference.
                generate_path_internal(src_node, parent, current_path, final_path, parents);
            }
        
    }else{
    
        for (short ii = 0; ii < parent_count; ii++){
            parent = parents[src_node][current][ii];
            
            if (parent == -1){
                //path found. save final result.
                std::vector<short> temp_path(current_path.size(), -1);
                short jj, kk;
                for(jj = current_path.size() - 1, kk = 0; jj >= 0 ; jj--, kk++){
                    //we generated the path from dest to src, so reversing it
                    temp_path[kk] = current_path[jj];
                }
                final_path.push_back(temp_path);
                
            }else{
                //path not found yet. dig deeper.
                
                //branching. So make a new copy of current_path
                std::vector<short>  current_path_copy(current_path);
                
                generate_path_internal(src_node, parent, current_path_copy, final_path, parents);
            }
        }
        
    }
}

void check_djkstra(){
    //genreate the graph as per function requirement
        //test network (DF of a=2, g=3)
        //        0 [(1, 1), (2, 3)]
        //        1 [(0, 1), (4, 3)]
        //        2 [(0, 3), (3, 1)]
        //        3 [(2, 1), (5, 3)]
        //        4 [(1, 3), (5, 1)]
        //        5 [(3, 3), (4, 1)]
    
    int N = 4;
    std::vector < std::vector < std::pair<short,short> > > graph(N);
    
    graph[0].push_back(std::make_pair(1,1));
    graph[0].push_back(std::make_pair(2,3));
    
    graph[1].push_back(std::make_pair(0,1));
    graph[1].push_back(std::make_pair(3,3));
    
    graph[2].push_back(std::make_pair(0,3));
    graph[2].push_back(std::make_pair(3,1));
    
    graph[3].push_back(std::make_pair(1,3));
    graph[3].push_back(std::make_pair(2,1));
    
    /*graph[4].push_back(std::make_pair(1,3));
    graph[4].push_back(std::make_pair(5,1));
    
    graph[5].push_back(std::make_pair(3,3));
    graph[5].push_back(std::make_pair(4,1));
    */
    
    cout << "list of link generated: " << endl;
    for(int ii = 0; ii<graph.size(); ii++){
        for (int jj = 0; jj<graph[ii].size(); jj++){
            cout << ii << " , " << graph[ii][jj].first << " , " << graph[ii][jj].second << endl; 
        }
    }
    
    std::vector< std::vector< short >> distance(N, std::vector< short >(N,INF));
    std::vector< std::vector< std::vector<short> > > parents(N, std::vector< std::vector<short> >(N));
    
    all_pair_djkstra(N, graph, distance, parents);
    
    //call djkstra
    /*djkstra(0, 4, graph);
    djkstra(1, 4, graph);
    djkstra(2, 4, graph);
    djkstra(3, 4, graph);*/
}

int main(){
    cout << "Hello World!" << endl;
    
    check_djkstra();
    
    cout << "Bye.Go die!" << endl;
    return 0;
}



























