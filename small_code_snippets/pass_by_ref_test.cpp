#include <iostream>
#include <vector>
using namespace std;

void print_vector(std::vector<int> &vect){
    cout << "vector: ";
    for(int ii = 0; ii<vect.size(); ii++){
        cout << vect[ii] << " ";
    }
    vect[0] = 99;
    cout << endl;
}

int main(){
    cout << "hello world!" << endl;
    
    std::vector<int> vect{1,2,3,4,5,0};
    
    print_vector(vect);
    
    for(int ii = 0; ii<vect.size(); ii++){
        cout << vect[ii] << " ";
    }
    cout << endl;
    cout << "Done. Go die!" << endl;
    return 0;
}
