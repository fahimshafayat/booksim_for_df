/*Need to have boost installed */

#include <iostream>
#include <vector>
#include <unordered_map>

//#include <boost/math/common_factor.hpp>
//#include <boost/functional/hash.hpp>

using namespace std;

void map_test(){
    cout << "Hello world" << endl;
    
    std::unordered_map<string, std::pair<int,int> > my_map;
    
    my_map["apple"] = std::make_pair(1,10);
    my_map["ball"] = std::make_pair(2,20);
    my_map["cat"] = std::make_pair(3,30);
    
    if (my_map.find("apple") != my_map.end()) {
        cout << "found" << endl;
    }else{
        cout << "Not found" << endl;
    }
    
    
    if (my_map.find("adam") != my_map.end()) {
        cout << "found" << endl;
    }else{
        cout << "Not found" << endl;
    }
    
    std::unordered_map<string, std::pair<int,int>>::iterator it;
    
    for(it = my_map.begin(); it!= my_map.end(); it++){
        cout << it->first << " , "  << it->second.first << "," << it->second.second << endl;
    }
    
    cout << "the world never says hello back." << endl;
}

struct pair_hash{
    long int operator() (const std::pair<int, int> &p) const{
        long int res = p.first * 100000 + p.second;
        return res;
    }
};

void map_test_with_pair_key(){
    //C++11 does not provide a hash function for a pair. 
    //So there is no straight-forward way to use a pair as the key in a map.
    //Either provide a hash function yourself, or use a third-party one.
    
    //Here, let's try with the hash fucntion that comes with boost.
    //std::unordered_map< std::pair<int, int>, int, boost::hash<std::pair<int,int> > > my_map;
    
    //Alternatively, we can define our own hash. lets try that.
    cout << "using pair_hash" << endl;
    std::unordered_map< std::pair<int, int>, int, pair_hash > my_map;
    
    
    my_map[std::make_pair(1,2)] = 12;
    my_map[std::make_pair(4,2)] = 42;
    my_map[std::make_pair(5,5)] = 55;
    my_map[std::make_pair(3,7)] = 37;
    
    my_map[std::pair<int, int>(2,9)] = 29;
    
    if (my_map.find(std::make_pair(1,2)) != my_map.end()) {
        cout << "found" << endl;
    }else{
        cout << "Not found" << endl;
    }
    
    
    if (my_map.find(std::pair<int, int>(4,0)) != my_map.end()) {
        cout << "found" << endl;
    }else{
        cout << "Not found" << endl;
    }
    
    for(auto it = my_map.begin(); it!= my_map.end(); it++){
        cout << it->first.first << " , "  << it->first.second << " : " << it->second << endl;
    }
    
}

/*
To install boost:

In Ubuntu:
    sudo apt-get install libboost-all-dev

In Centos:
    yum update
    yum install epel-release
    yum install boost boost-thread boost-devel
*/

void boost_test(){
    int a = 10;
    int b = 15;
    int gcd = boost::math::gcd(a,b);
    cout << "gcd: " << gcd << endl;    
}

int main_1(){
    cout << "hello " << endl;
    
    map_test_with_pair_key();
    
    cout << "bye " << endl;
    
    return 0;
}
