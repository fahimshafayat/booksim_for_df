#include <iostream>
#include <queue>        //for std::priority_queue        
using namespace std;

void test_max_heap(){
    std::priority_queue<int> max_heap;
    
    max_heap.push(5);
    max_heap.push(1);
    max_heap.push(10);
    max_heap.push(30);
    max_heap.push(20);
    max_heap.push(25);
    
    cout << "total element in heap: " << max_heap.size() << endl;
    
    while(!max_heap.empty()){
        cout << max_heap.top() << endl;
        max_heap.pop();
    }

}

void test_min_heap(){
    std::priority_queue<int, vector<int>, greater<int> > min_heap;
    
    min_heap.push(5);
    min_heap.push(1);
    min_heap.push(10);
    min_heap.push(30);
    min_heap.push(20);
    min_heap.push(25);
    
    cout << "total element in heap: " << min_heap.size() << endl;
    
    while(!min_heap.empty()){
        cout << min_heap.top() << endl;
        min_heap.pop();
    }

}


class myComparison{

    bool reverse;
    
    public:
        myComparison(const bool& revparam = false){
            reverse = revparam;
        }
        
        bool operator() (const int& lhs, const int& rhs) const{
            if (reverse){
                return (lhs > rhs);
            }
            else{
                return (lhs < rhs);
            }
        }

};

class MyComparison2{
    /*  A comparison function object that compares two pairs and
        chooses the one with the smaller first element as the winner.*/
    public:
        MyComparison2(){
        }
        
        bool operator() (const std::pair<short,short>& lhs, const std::pair<short,short>& rhs) const{
            return (lhs.first > rhs.first);
        }

};

void test_custom_comp2(){
    //this time make a heap with pairs of shorts
    std::priority_queue<std::pair<short,short>, std::vector<std::pair<short,short>>, MyComparison2 > min_heap;
    
    min_heap.push(std::make_pair(0,1));
    min_heap.push(std::make_pair(3,2));
    min_heap.push(std::make_pair(4,3));
    min_heap.push(std::make_pair(2,4));
    min_heap.push(std::make_pair(1,5));
    min_heap.push(std::make_pair(4,6));
    min_heap.push(std::make_pair(5,7));
    min_heap.push(std::make_pair(1,8));
    
    cout << "total element in heap: " << min_heap.size() << endl;
    
    while(!min_heap.empty()){
        cout << min_heap.top().first << "," << min_heap.top().second << endl;
        min_heap.pop();
    }
    
    cout << "heap test done." << endl;
} 


void test_custom_comp(){
    std::priority_queue<int, std::vector<int>, myComparison> mypq;
    
    mypq.push(5);
    mypq.push(1);
    mypq.push(10);
    mypq.push(30);
    mypq.push(20);
    mypq.push(25);
    
    cout << "total element in heap: " << mypq.size() << endl;
    
    while(!mypq.empty()){
        cout << mypq.top() << endl;
        mypq.pop();
    }
    
    cout << "\nnow test with reverse: " << endl;
    
    std::priority_queue<int, std::vector<int>, myComparison> mypq1(myComparison(true));
    
    mypq1.push(5);
    mypq1.push(1);
    mypq1.push(10);
    mypq1.push(30);
    mypq1.push(20);
    mypq1.push(25);
    
    cout << "total element in heap: " << mypq1.size() << endl;
    
    while(!mypq1.empty()){
        cout << mypq1.top() << endl;
        mypq1.pop();
    }
}



int main(){

    //test_max_heap();    
    //test_min_heap();
    //test_custom_comp();
    test_custom_comp2();
    return 0;
}
