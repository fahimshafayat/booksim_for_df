void generate_path(int src, int dst, std::vector< std::vector< std::vector<short> > >& parents);

void generate_path_internal(short current_node, short dst, std::vector<short> & current_path, std::vector<std::vector<short> >& final_path, std::vector< std::vector< std::vector<short> > >& parents);

void djkstra(int src, int N, std::vector < std::vector < std::pair<short,short> > >& graph, std::vector< short >& distance, std::vector< std::vector<short> >& parents);

void all_pair_djkstra(int N, std::vector < std::vector < std::pair<short,short> > >& graph, std::vector< std::vector< short >>& distance, std::vector< std::vector< std::vector<short> > >& parents);
