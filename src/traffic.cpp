// $Id$

/*
 Copyright (c) 2007-2015, Trustees of The Leland Stanford Junior University
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 Redistributions of source code must retain the above copyright notice, this 
 list of conditions and the following disclaimer.
 Redistributions in binary form must reproduce the above copyright notice, this
 list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <sstream>
#include <ctime>
#include "random_utils.hpp"
#include "traffic.hpp"



TrafficPattern::TrafficPattern(int nodes)
: _nodes(nodes)
{
  if(nodes <= 0) {
    cout << "Error: Traffic patterns require at least one node." << endl;
    exit(-1);
  }
}

void TrafficPattern::reset()
{

}

TrafficPattern * TrafficPattern::New(string const & pattern, int nodes, 
				     Configuration const * const config)
{
  string pattern_name;
  string param_str;
  size_t left = pattern.find_first_of('(');
  if(left == string::npos) {
    pattern_name = pattern;
  } else {
    pattern_name = pattern.substr(0, left);
    size_t right = pattern.find_last_of(')');
    if(right == string::npos) {
      param_str = pattern.substr(left+1);
    } else {
      param_str = pattern.substr(left+1, right-left-1);
    }
  }
  vector<string> params = tokenize_str(param_str);
  
  TrafficPattern * result = NULL;
  if(pattern_name == "bitcomp") {
    result = new BitCompTrafficPattern(nodes);
  } else if(pattern_name == "transpose") {
    result = new TransposeTrafficPattern(nodes);
  } else if(pattern_name == "bitrev") {
    result = new BitRevTrafficPattern(nodes);
  } else if(pattern_name == "shuffle") {
    result = new ShuffleTrafficPattern(nodes);
  } else if(pattern_name == "randperm") {
    int perm_seed = -1;
    if(params.empty()) {
      if(config) {
	if(config->GetStr("perm_seed") == "time") {
	  perm_seed = int(time(NULL));
	  cout << "SEED: perm_seed=" << perm_seed << endl;
	} else {
	  perm_seed = config->GetInt("perm_seed");
	}
      } else {
	cout << "Error: Missing parameter for random permutation traffic pattern: " << pattern << endl;
	exit(-1);
      }
    } else {
      perm_seed = atoi(params[0].c_str());
    }
    result = new RandomPermutationTrafficPattern(nodes, perm_seed);
  } else if (pattern_name == "group_shift"){
    int df_a = config->GetInt("df_a");
    int df_g = config->GetInt("df_g");
    int df_p = config->GetInt("df_p");
    int shift_by_group = config->GetInt("shift_by_group");
    result = new GroupShiftTrafficPattern(nodes, df_a, df_g, df_p, shift_by_group);
  } 
  else if(pattern_name == "shift") {
      int shift_offset = config->GetInt("shift_offset");
      result = new ShiftTrafficPattern(nodes,shift_offset);
  }
  else if (pattern_name == "df_wc"){

    int p = config->GetInt( "df_p" );
    int a = config->GetInt( "df_a" );
    //int h = config->GetInt( "dfly_h" );
    int g = config->GetInt( "df_g" );
    int s = config->GetInt( "df_wc_seed" );

    cout << "pattern: " << pattern_name << endl;

    //result = new Dragonfly_WC( nodes,a,g,p,s);
    result = new DF_WC( nodes,a,g,p,s);

 }
  else if(pattern_name == "router_based_shift_uniform") {
      int shift_percentage = config->GetInt("shift_percentage");
      int shift_offset_by_group = config->GetInt("shift_offset"); //for drgaonflyfull. Change later to accomodate Edison. 
      
      int df_a = config->GetInt("df_a");
      int shift_offset = shift_offset_by_group * df_a;
      
      int df_p = df_a/2;  //for dragonfly_full. Probably needs change for Edison. Will worry later.
      
      result = new RouterBasedShiftUniformPattern(df_p,nodes,shift_percentage, shift_offset);
  }
  else if(pattern_name == "pe_based_shift_uniform") {
      int shift_percentage = config->GetInt("shift_percentage");
      int shift_offset_by_group = config->GetInt("shift_offset"); //for drgaonflyfull. Change later to accomodate Edison. 

      int df_a = config->GetInt("df_a");
      int shift_offset = shift_offset_by_group * df_a;
      
      int df_p = df_a/2;  //for dragonfly_full. Probably needs change for Edison. Will worry later.
      
      result = new PEBasedShiftUniformPattern(df_p,nodes,shift_percentage, shift_offset);
  }

  else if(pattern_name == "time_pe_based_shift_uniform") {
      int shift_percentage = config->GetInt("shift_percentage");
      int shift_offset_by_group = config->GetInt("shift_offset"); //for drgaonflyfull. Change later to accomodate Edison. 

      int df_a = config->GetInt("df_a");
      int shift_offset = shift_offset_by_group * df_a;
      
      int df_p = df_a/2;  //for dragonfly_full. Probably needs change for Edison. Will worry later.
      
      result = new TimePEBasedShiftUniformPattern(df_p,nodes,shift_percentage, shift_offset);
  }

  else if(pattern_name == "uniform") {
    result = new UniformRandomTrafficPattern(nodes);
  } else if(pattern_name == "background") {
    vector<int> excludes = tokenize_int(params[0]);
    result = new UniformBackgroundTrafficPattern(nodes, excludes);
  } else if(pattern_name == "diagonal") {
    result = new DiagonalTrafficPattern(nodes);
  } else if(pattern_name == "asymmetric") {
    result = new AsymmetricTrafficPattern(nodes);
  } else if(pattern_name == "taper64") {
    result = new Taper64TrafficPattern(nodes);
  } else if(pattern_name == "bad_dragon") {
    bool missing_params = false;
    int k = -1;
    if(params.size() < 1) {
      if(config) {
	k = config->GetInt("k");
      } else {
	missing_params = true;
      }
    } else {
      k = atoi(params[0].c_str());
    }
    int n = -1;
    if(params.size() < 2) {
      if(config) {
	n = config->GetInt("n");
      } else {
	missing_params = true;
      }
    } else {
      n = atoi(params[1].c_str());
    }
    if(missing_params) {
      cout << "Error: Missing parameters for dragonfly bad permutation traffic pattern: " << pattern << endl;
      exit(-1);
    }
    result = new BadPermDFlyTrafficPattern(nodes, k, n);
  } else if((pattern_name == "tornado") || (pattern_name == "neighbor") ||
	    (pattern_name == "badperm_yarc")) {
    bool missing_params = false;
    int k = -1;
    if(params.size() < 1) {
      if(config) {
	k = config->GetInt("k");
      } else {
	missing_params = true;
      }
    } else {
      k = atoi(params[0].c_str());
    }
    int n = -1;
    if(params.size() < 2) {
      if(config) {
	n = config->GetInt("n");
      } else {
	missing_params = true;
      }
    } else {
      n = atoi(params[1].c_str());
    }
    int xr = -1;
    if(params.size() < 3) {
      if(config) {
	xr = config->GetInt("xr");
      } else {
	missing_params = true;
      }
    } else {
      xr = atoi(params[2].c_str());
    }
    if(missing_params) {
      cout << "Error: Missing parameters for digit permutation traffic pattern: " << pattern << endl;
      exit(-1);
    }
    if(pattern_name == "tornado") {
      result = new TornadoTrafficPattern(nodes, k, n, xr);
    } else if(pattern_name == "neighbor") {
      result = new NeighborTrafficPattern(nodes, k, n, xr);
    } else if(pattern_name == "badperm_yarc") {
      result = new BadPermYarcTrafficPattern(nodes, k, n, xr);
    }
  } else if(pattern_name == "hotspot") {
    if(params.empty()) {
      params.push_back("-1");
    } 
    vector<int> hotspots = tokenize_int(params[0]);
    for(size_t i = 0; i < hotspots.size(); ++i) {
      if(hotspots[i] < 0) {
	hotspots[i] = RandomInt(nodes - 1);
      }
    }
    vector<int> rates;
    if(params.size() >= 2) {
      rates = tokenize_int(params[1]);
      rates.resize(hotspots.size(), rates.back());
    } else {
      rates.resize(hotspots.size(), 1);
    }
    result = new HotSpotTrafficPattern(nodes, hotspots, rates);
  } else {
    cout << "Error: Unknown traffic pattern: " << pattern << endl;
    exit(-1);
  }
  
  return result;


}

PermutationTrafficPattern::PermutationTrafficPattern(int nodes)
  : TrafficPattern(nodes)
{
  
}

BitPermutationTrafficPattern::BitPermutationTrafficPattern(int nodes)
  : PermutationTrafficPattern(nodes)
{
  if((nodes & -nodes) != nodes) {
    cout << "Error: Bit permutation traffic patterns require the number of "
	 << "nodes to be a power of two." << endl;
    exit(-1);
  }
}

BitCompTrafficPattern::BitCompTrafficPattern(int nodes)
  : BitPermutationTrafficPattern(nodes)
{
  
}

int BitCompTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  int const mask = _nodes - 1;
  return ~source & mask;
}

TransposeTrafficPattern::TransposeTrafficPattern(int nodes)
  : BitPermutationTrafficPattern(nodes), _shift(0)
{
  while(nodes >>= 1) {
    ++_shift;
  }
  if(_shift % 2) {
    cout << "Error: Transpose traffic pattern requires the number of nodes to "
	 << "be an even power of two." << endl;
    exit(-1);
  }
  _shift >>= 1;
}

int TransposeTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  int const mask_lo = (1 << _shift) - 1;
  int const mask_hi = mask_lo << _shift;
  return (((source >> _shift) & mask_lo) | ((source << _shift) & mask_hi));
}

BitRevTrafficPattern::BitRevTrafficPattern(int nodes)
  : BitPermutationTrafficPattern(nodes)
{
  
}

int BitRevTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  int result = 0;
  for(int n = _nodes; n > 1; n >>= 1) {
    result = (result << 1) | (source % 2);
    source >>= 1;
  }
  return result;
}

ShuffleTrafficPattern::ShuffleTrafficPattern(int nodes)
  : BitPermutationTrafficPattern(nodes)
{

}

int ShuffleTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  int const shifted = source << 1;
  return ((shifted & (_nodes - 1)) | bool(shifted & _nodes));
}

DigitPermutationTrafficPattern::DigitPermutationTrafficPattern(int nodes, int k,
							       int n, int xr)
  : PermutationTrafficPattern(nodes), _k(k), _n(n), _xr(xr)
{
  
}

TornadoTrafficPattern::TornadoTrafficPattern(int nodes, int k, int n, int xr)
  : DigitPermutationTrafficPattern(nodes, k, n, xr)
{

}

int TornadoTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));

  int offset = 1;
  int result = 0;

  for(int n = 0; n < _n; ++n) {
    result += offset *
      (((source / offset) % (_xr * _k) + ((_xr * _k + 1) / 2 - 1)) % (_xr * _k));
    offset *= (_xr * _k);
  }
  return result;
}

NeighborTrafficPattern::NeighborTrafficPattern(int nodes, int k, int n, int xr)
  : DigitPermutationTrafficPattern(nodes, k, n, xr)
{

}

int NeighborTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));

  int offset = 1;
  int result = 0;
  
  for(int n = 0; n < _n; ++n) {
    result += offset *
      (((source / offset) % (_xr * _k) + 1) % (_xr * _k));
    offset *= (_xr * _k);
  }
  return result;
}

RandomPermutationTrafficPattern::RandomPermutationTrafficPattern(int nodes, 
								 int seed)
  : TrafficPattern(nodes)
{
  _dest.resize(nodes);
  randomize(seed);
}

void RandomPermutationTrafficPattern::randomize(int seed)
{
  vector<long> save_x;
  vector<double> save_u;
  SaveRandomState(save_x, save_u);
  RandomSeed(seed);

  _dest.assign(_nodes, -1);

  for(int i = 0; i < _nodes; ++i) {
    int ind = RandomInt(_nodes - 1 - i);

    int j = 0;
    int cnt = 0;
    while((cnt < ind) || (_dest[j] != -1)) {
      if(_dest[j] == -1) {
	++cnt;
      }
      ++j;
      assert(j < _nodes);
    }

    _dest[j] = i;
  }

  RestoreRandomState(save_x, save_u); 
}

int RandomPermutationTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  assert((_dest[source] >= 0) && (_dest[source] < _nodes));
  return _dest[source];
}

/*

class GroupShiftTrafficPattern : public TrafficPattern{
private:
    vector<int> _dest;
public:
    GroupShiftTrafficPattern(int nodes, int df_a, int df_g, int shift_by_group);
    virtual int dest(int source);
}
*/

GroupShiftTrafficPattern::GroupShiftTrafficPattern(int nodes, int a, int g, int p, int shift_by_group_amount) : TrafficPattern(nodes)
{

    //    /*cout << "GroupShiftTrafficPattern constructor ..." << endl;
    //    cout << "nodes: " << nodes << endl;
    //    cout << "a: " << a << endl;
    //    cout << "g: " << g << endl;
    //    cout << "p: " << p << endl;
    //    cout << "shift_by_group_amount: " << shift_by_group_amount << endl;*/
    
    df_a = a;
    df_g = g;
    if (p == 0){
        df_p = df_a / 2;
    }else{
        df_p = p;
    }
    
    assert(nodes == (df_a * df_g * df_p));
    
    shift_by_group = shift_by_group_amount;
    
}

int GroupShiftTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  
  int src_router = source / df_p;
  int src_in_router_idx = source % df_p;
  
  //cout << "src: " << source << " src_router: " << src_router << " src_in_router_idx: " << src_in_router_idx << endl;
  
  int dst_router = (src_router + (shift_by_group * df_a) ) % (df_a * df_g);
  int dest = dst_router * df_p + src_in_router_idx; 
  
  //cout << "dst_router: " << dst_router << " dest: " << dest  << endl;
  
  
  return dest;
}


RandomTrafficPattern::RandomTrafficPattern(int nodes)
  : TrafficPattern(nodes)
{

}

UniformRandomTrafficPattern::UniformRandomTrafficPattern(int nodes)
  : RandomTrafficPattern(nodes)
{

}

int UniformRandomTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  return RandomInt(_nodes - 1);
}

UniformBackgroundTrafficPattern::UniformBackgroundTrafficPattern(int nodes, vector<int> excluded_nodes)
  : RandomTrafficPattern(nodes)
{
  for(size_t i = 0; i < excluded_nodes.size(); ++i) {
    int const node = excluded_nodes[i];
    assert((node >= 0) && (node < _nodes));
    _excluded.insert(node);
  }
}

int UniformBackgroundTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));

  int result;

  do {
    result = RandomInt(_nodes - 1);
  } while(_excluded.count(result) > 0);

  return result;
}

DiagonalTrafficPattern::DiagonalTrafficPattern(int nodes)
  : RandomTrafficPattern(nodes)
{

}

int DiagonalTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  return ((RandomInt(2) == 0) ? ((source + 1) % _nodes) : source);
}

AsymmetricTrafficPattern::AsymmetricTrafficPattern(int nodes)
  : RandomTrafficPattern(nodes)
{

}

int AsymmetricTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  int const half = _nodes / 2;
  return (source % half) + (RandomInt(1) ? half : 0);
}

Taper64TrafficPattern::Taper64TrafficPattern(int nodes)
  : RandomTrafficPattern(nodes)
{
  if(nodes != 64) {
    cout << "Error: Tthe Taper64 traffic pattern requires the number of nodes "
	 << "to be exactly 64." << endl;
    exit(-1);
  }
}

int Taper64TrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  if(RandomInt(1)) {
    return ((64 + source + 8 * (RandomInt(2) - 1) + (RandomInt(2) - 1)) % 64);
  } else {
    return RandomInt(_nodes - 1);
  }
}

BadPermDFlyTrafficPattern::BadPermDFlyTrafficPattern(int nodes, int k, int n)
  : DigitPermutationTrafficPattern(nodes, k, n, 1)
{
  
}

int BadPermDFlyTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));

  int const grp_size_routers = 2 * _k;
  int const grp_size_nodes = grp_size_routers * _k;

  return ((RandomInt(grp_size_nodes - 1) + ((source / grp_size_nodes) + 1) * grp_size_nodes) % _nodes);
}

BadPermYarcTrafficPattern::BadPermYarcTrafficPattern(int nodes, int k, int n, 
						     int xr)
  : DigitPermutationTrafficPattern(nodes, k, n, xr)
{

}

int BadPermYarcTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  int const row = source / (_xr * _k);
  return RandomInt((_xr * _k) - 1) * (_xr * _k) + row;
}

HotSpotTrafficPattern::HotSpotTrafficPattern(int nodes, vector<int> hotspots, 
					     vector<int> rates)
  : TrafficPattern(nodes), _hotspots(hotspots), _rates(rates), _max_val(-1)
{
  assert(!_hotspots.empty());
  size_t const size = _hotspots.size();
  _rates.resize(size, _rates.empty() ? 1 : _rates.back());
  for(size_t i = 0; i < size; ++i) {
    int const hotspot = _hotspots[i];
    assert((hotspot >= 0) && (hotspot < _nodes));
    int const rate = _rates[i];
    assert(rate > 0);
    _max_val += rate;
  }
}

int HotSpotTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));

  if(_hotspots.size() == 1) {
    return _hotspots[0];
  }

  int pct = RandomInt(_max_val);

  for(size_t i = 0; i < (_hotspots.size() - 1); ++i) {
    int const limit = _rates[i];
    if(limit > pct) {
      return _hotspots[i];
    } else {
      pct -= limit;
    }
  }
  assert(_rates.back() > pct);
  return _hotspots.back();
}


ShiftTrafficPattern::ShiftTrafficPattern(int nodes, int shift_by_rank)
  : TrafficPattern(nodes)
{
  cout << "inside ShiftTrafficPatter. nodes: " << nodes << endl;
  _shift_offset = shift_by_rank; 
}

int ShiftTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  int dst = (source + _shift_offset) % _nodes;
  //cout << "src, src_group, dst, dst_group: " << source << " , " << source / (96*4)  << " , " << dst << " , " << dst / (96*4) << endl;
  return dst;
}

RouterBasedShiftUniformPattern::RouterBasedShiftUniformPattern(int df_p, int nodes, int shift_percentage, int shift_offset) : TrafficPattern(nodes) {

    cout << "inside RouterBasedShiftUniformPattern constructor for (df_p, nodes, shift_percentage, shift_offset) : "<< df_p << "," << nodes << "," << shift_percentage << "," << shift_offset << endl;
    
    _df_p = df_p; //for dragonflyfull. Change later for Edison.  


    _nTOR = nodes / _df_p;   
    
    int shuffler[_nTOR];
    int ii;
    int threshold;

    cout << "_nTOR: " << _nTOR << endl;

    _traffic_choice.resize(_nTOR, 0); //0->uniform, 1->shift
    _shift_offset = shift_offset;
    _shift_percentage = shift_percentage;


    for (ii = 0; ii < _nTOR; ii++){
      shuffler[ii] = ii;
    }

    //shuffle the shuffler array
    fisher_yates_shuffle(shuffler, _nTOR);

    threshold = (_nTOR * shift_percentage)/100;
    cout << "threshold: " << threshold << endl;

    //test print
    // cout << "first 10 entries in shuffler: " ;
    // for(ii = 0; ii < 10; ii++){
    //   cout << shuffler[ii] << " ";
    // }
    // cout << endl;

    //choose routers for shift or uniform
    for (ii = 0; ii < threshold; ii++){
      _traffic_choice[ shuffler[ii] ] = 1; //shift 
    }
    for(ii = threshold; ii < _nTOR; ii++){
      _traffic_choice[ shuffler[ii] ] = 0; //uniform
    }

    // //test print
    // cout << "first 10 entries in _traffic_choice: " ;
    // for(ii = 0; ii < 10; ii++){
    //   cout << _traffic_choice[ii] << " ";
    // }
    // cout << endl;

    // cout << "last 10 entries in _traffic_choice: " ;
    // for(ii = nTOR-1-10; ii < nTOR; ii++){
    //   cout << _traffic_choice[ii] << " ";
    // }
    // cout << endl;

}

int RouterBasedShiftUniformPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  int dst; 
  int dst_router;
  
  int src_router = source / _df_p;
  int src_idx = source % _df_p; 

  if (_traffic_choice[src_router] == 1){
    //dst = (source + _shift_offset) % _nodes;
    dst_router = (src_router + _shift_offset) % _nTOR;
    dst = dst_router * _df_p + src_idx;
  }else{
    dst = RandomInt(_nodes - 1);
  }

  //cout << "src, src_router, _shift_offset[src_router] : " << source << " , " << src_router << " , " << _traffic_choice[src_router] << endl;
  //cout << "src, src_group, dst, dst_group: " << source << " , " << source / (4*_df_p)  << " , " << dst << " , " << dst / (4*_df_p) << endl;
  return dst;


  return 0;
}

PEBasedShiftUniformPattern::PEBasedShiftUniformPattern(int df_p, int nodes, int shift_percentage, int shift_offset) : TrafficPattern(nodes) {

    //cout << "inside PEBasedShiftUniformPattern constructor for (nodes, shift_percentage, shift_offset) : " << nodes << "," << shift_percentage << "," << shift_offset << endl;
    _df_p = df_p; //for dragonflyfull. Change later for Edison.  

    _nTOR = nodes / _df_p;   
    
    int shuffler[nodes];
    int ii;
    int threshold;

    //cout << "nodes: " << nodes << endl;

    _traffic_choice.resize(nodes, 0); //0->uniform, 1->shift
    _shift_offset = shift_offset;
    _shift_percentage = shift_percentage;


    for (ii = 0; ii < nodes; ii++){
      shuffler[ii] = ii;
    }

    //shuffle the shuffler array
    fisher_yates_shuffle(shuffler, nodes);

    threshold = (nodes * shift_percentage) / 100;
    //cout << "threshold: " << threshold;

    //test print
    // cout << "first 10 entries in shuffler: " ;
    // for(ii = 0; ii < 10; ii++){
    //   cout << shuffler[ii] << " ";
    // }
    // cout << endl;

    //choose routers for shift or uniform
    for (ii = 0; ii < threshold; ii++){
      _traffic_choice[ shuffler[ii] ] = 1; //shift 
    }
    for(ii = threshold; ii < nodes; ii++){
      _traffic_choice[ shuffler[ii] ] = 0; //uniform
    }

    // //test print
    // cout << "first 10 entries in _traffic_choice: " ;
    // for(ii = 0; ii < 10; ii++){
    //   cout << _traffic_choice[ii] << " ";
    // }
    // cout << endl;

    // cout << "last 10 entries in _traffic_choice: " ;
    // for(ii = nodes-1-10; ii < nodes; ii++){
    //   cout << _traffic_choice[ii] << " ";
    // }
    // cout << endl;

}

int PEBasedShiftUniformPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));

  int dst; 
  int dst_router;
  
  int src_router = source / _df_p;
  int src_idx = source % _df_p; 


  if (_traffic_choice[source] == 1){
    
    //dst = (source + _shift_offset) % _nodes;
    
    dst_router = (src_router + _shift_offset) % _nTOR;
    dst = dst_router * _df_p + src_idx;
 
  }else{
    dst = RandomInt(_nodes - 1);
  }

  //cout << "src, src_router, _shift_offset[src] : " << source << " , " << src_router << " , " << _traffic_choice[source] << endl;
  //cout << "src, src_group, dst, dst_group: " << source << " , " << source / (4*_df_p)  << " , " << dst << " , " << dst / (4*_df_p) << endl;
  
  
  return dst;


  
}


void fisher_yates_shuffle(int arr[], int size){
  for(int ii = size-1; ii > 0; ii--){
      //pick a random index from 0 to ii
      int jj = RandomInt(ii-1); //RandomInt is inclusive of ii
                                //here, using ii-1 means a node can not be paired with itself.
      //swap 
      std::swap(arr[ii], arr[jj]);
  }
}

TimePEBasedShiftUniformPattern::TimePEBasedShiftUniformPattern(int df_p, int nodes, int shift_percentage, int shift_offset) : TrafficPattern(nodes) {
    //cout << "inside TimePEBasedShiftUniformPattern constructor for (p, nodes, shift_percentage, shift_offset) : " << df_p << "," << nodes << "," << shift_percentage << "," << shift_offset << endl;
    
    _df_p = df_p; //for dragonflyfull. Change later for Edison.  

    _nTOR = nodes / _df_p;   
    
    _shift_offset = shift_offset;
    _shift_percentage = shift_percentage;
}

int TimePEBasedShiftUniformPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));

  int dst; 
  int dst_router;
  
  int src_router = source / _df_p;
  int src_idx = source % _df_p; 

  int cutoff = RandomInt(99) + 1; 
  // cout << " src: " << source;
  // cout << " src_router: " << src_router;
  // cout << " src_idx: " << src_idx;
  // cout << endl;

  //cout << " cutoff: " << cutoff;
  
  if (cutoff <= _shift_percentage){
    //cout << " shift, ";  
    dst_router = (src_router + _shift_offset) % _nTOR;
    dst = dst_router * _df_p + src_idx;
    //cout << " dst_router: " << dst_router;
      
  }else{
    dst = RandomInt(_nodes - 1);
    //cout << " uniform, ";    
  }

  // cout << " dst: " << dst;
  // cout << endl;

  
  return dst;


  
}



DF_WC::DF_WC(int nodes, int a, int g, int p, int seed)
        : TrafficPattern(nodes)
{

  _seed = seed;
  df_dest.resize(nodes);
       
  if (p == 0){
    p = a/2;
  }

  init(nodes, a, g, p);
}


void DF_WC::init(int nodes, int a, int g, int p)
{
  cout << "inside init " << endl;
  cout << "node: " << nodes << endl;
  cout << "a: " << a << endl;
  cout << "g: " << g << endl;
  cout << "p: " << p << endl;
  cout << "seed: " << _seed << endl;

  vector<long> save_x;
  vector<double> save_u;
  SaveRandomState(save_x, save_u);
  RandomSeed(_seed);


  int nTOR = a*g;
  int groups[g];
  int routers_in_groups[a];
  int ii, jj,kk;
  int src_group, dst_group;
  int src_router, dst_router;
  int src_pe, dst_pe;

  //basically shuffle all the groups and have them in a vector
  for(ii = 0; ii< g; ii++){
    groups[ii] = ii;
  }
  fisher_yates_shuffle(groups, g);
  
  //For each group pair, shuffle the dest group routers and 
  //then pair src_router and dst_router.
  for(src_group = 0; src_group < g; src_group++){
    dst_group = groups[src_group];
  
    for(jj = 0; jj < a; jj++){
      routers_in_groups[jj] = jj;
    }

    fisher_yates_shuffle(routers_in_groups, a);
  
    for(jj = 0; jj < a; jj++){
      src_router = src_group*a + jj;
      dst_router = dst_group*a + routers_in_groups[jj];
    
      //For each PE, get the paired dst_router, and add the corresponding
      //PE as the dest PE.
      for(kk = 0; kk < p; kk++){
        src_pe = src_router * p + kk;
        dst_pe = dst_router * p + kk;
        df_dest[src_pe] = dst_pe;
      }

    }
  

  }

  // cout << "done with traffic generation." << endl;
  // for(ii = 0; ii < df_dest.size(); ii++){
  //   cout << ii << " , " << df_dest[ii] << endl;
  // }
  

  RestoreRandomState(save_x, save_u); 

}

int DF_WC::dest(int source)
{
    //cout<<"src:"<<source <<", size:"<<df_dest.size()<<endl;
  return df_dest[source];
}



