vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = UGAL_L;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50013,   0.50013,    11.727,    11.728,         0,      3.9064
simulation,         2,      0.74,   0.73992,   0.73995,     14.32,    14.321,         0,       3.803
simulation,         3,      0.86,   0.85987,   0.85987,    18.418,    18.421,         0,      3.7673
simulation,         4,      0.92,   0.91992,   0.91998,    24.008,    24.015,         0,      3.7519
simulation,         5,      0.95,   0.94996,   0.94994,    30.591,    30.603,         0,      3.7447
simulation,         6,      0.97,   0.96983,   0.96987,    40.262,    40.281,         0,      3.7369
simulation,         7,      0.98,   0.97988,    0.9799,    50.986,    51.019,         0,      3.7327
simulation,         8,       0.1,   0.09993,   0.09993,    10.559,    10.559,         0,       4.159
simulation,         9,       0.2,   0.20004,   0.20005,    10.783,    10.784,         0,       4.121
simulation,        10,       0.3,   0.29995,   0.29995,     10.97,    10.971,         0,      4.0385
simulation,        11,       0.4,    0.4002,   0.40022,    11.265,    11.265,         0,      3.9644
simulation,        12,       0.5,   0.50013,   0.50013,    11.727,    11.728,         0,      3.9064
simulation,        13,       0.6,   0.59987,   0.59988,    12.446,    12.446,         0,      3.8579
simulation,        14,       0.7,   0.69978,   0.69979,    13.614,    13.615,         0,      3.8171
simulation,        15,       0.8,   0.80001,   0.80002,     15.83,    15.831,         0,       3.784
simulation,        16,       0.9,    0.8999,   0.89993,    21.528,    21.531,         0,      3.7576
