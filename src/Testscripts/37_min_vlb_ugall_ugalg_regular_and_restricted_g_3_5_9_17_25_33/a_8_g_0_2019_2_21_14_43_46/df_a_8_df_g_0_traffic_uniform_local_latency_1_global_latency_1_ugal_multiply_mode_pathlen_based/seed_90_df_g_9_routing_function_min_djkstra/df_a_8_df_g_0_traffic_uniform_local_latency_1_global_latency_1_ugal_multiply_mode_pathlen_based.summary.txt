vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = min_djkstra;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50013,   0.50013,    9.2104,    9.2107,         0,      3.0418
simulation,         2,      0.74,   0.73996,   0.73995,    11.018,    11.018,         0,      3.0419
simulation,         3,      0.86,   0.85991,   0.85987,    13.741,    13.742,         0,      3.0414
simulation,         4,      0.92,   0.91996,   0.91998,    17.496,    17.499,         0,      3.0417
simulation,         5,      0.95,   0.94999,   0.94994,    22.236,    22.242,         0,      3.0421
simulation,         6,      0.97,   0.96999,   0.96987,    30.103,    30.128,         0,      3.0417
simulation,         7,      0.98,   0.97995,    0.9799,    38.179,    38.217,         0,      3.0413
simulation,         8,       0.1,  0.099929,   0.09993,    8.2149,    8.2149,         0,      3.0412
simulation,         9,       0.2,   0.20005,   0.20005,    8.3806,    8.3808,         0,      3.0433
simulation,        10,       0.3,   0.29995,   0.29995,    8.5783,    8.5785,         0,       3.041
simulation,        11,       0.4,   0.40021,   0.40022,    8.8451,    8.8452,         0,      3.0406
simulation,        12,       0.5,   0.50013,   0.50013,    9.2104,    9.2107,         0,      3.0418
simulation,        13,       0.6,   0.59988,   0.59988,    9.7319,     9.732,         0,      3.0424
simulation,        14,       0.7,    0.6998,   0.69979,     10.54,    10.541,         0,      3.0412
simulation,        15,       0.8,   0.80001,   0.80002,    12.046,    12.047,         0,      3.0415
simulation,        16,       0.9,   0.89992,   0.89993,    15.814,    15.817,         0,      3.0417
