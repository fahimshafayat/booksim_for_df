vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 33;
df_wc_seed = 60;
routing_function = UGAL_G;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49985,   0.49989,    55.071,    55.086,         0,      6.2888
simulation,         2,      0.74,    0.3824,   0.46638,    913.83,    677.87,         1,         0.0
simulation,         3,      0.62,   0.41761,   0.49707,    560.75,    504.49,         1,         0.0
simulation,         4,      0.56,   0.42625,   0.46241,    761.13,    554.51,         1,         0.0
simulation,         5,      0.53,   0.45352,   0.48263,     429.9,    361.16,         1,         0.0
simulation,         6,      0.51,   0.47527,   0.47358,    445.11,    250.27,         1,         0.0
simulation,         7,       0.1,  0.099899,  0.099898,    14.771,    14.772,         0,      5.7644
simulation,         8,       0.2,       0.2,   0.20001,    16.474,    16.474,         0,       6.006
simulation,         9,       0.3,   0.29997,   0.29998,    18.884,    18.885,         0,       6.154
simulation,        10,       0.4,   0.39987,   0.39989,    23.387,    23.387,         0,      6.2323
simulation,        11,       0.5,   0.49985,   0.49989,    55.071,    55.086,         0,      6.2888
