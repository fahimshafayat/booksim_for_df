vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 9;
df_wc_seed = 62;
routing_function = min;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.12478,   0.22526,    1866.5,    1861.1,         1,         0.0
simulation,         2,      0.25,   0.12476,   0.22488,    1222.0,    1222.0,         1,         0.0
simulation,         3,      0.13,   0.12501,   0.12977,    649.13,    672.43,         1,         0.0
simulation,         4,      0.07,  0.069795,  0.069798,     10.17,     10.17,         0,      3.7513
simulation,         5,       0.1,  0.099961,  0.099966,    11.468,    11.468,         0,      3.7508
simulation,         6,      0.11,   0.10991,   0.10991,    13.044,    13.043,         0,      3.7497
simulation,         7,      0.12,   0.11994,   0.11992,    21.162,    21.174,         0,      3.7505
simulation,         8,      0.05,  0.049939,  0.049943,    9.8563,    9.8563,         0,      3.7482
simulation,         9,       0.1,  0.099961,  0.099966,    11.468,    11.468,         0,      3.7508
