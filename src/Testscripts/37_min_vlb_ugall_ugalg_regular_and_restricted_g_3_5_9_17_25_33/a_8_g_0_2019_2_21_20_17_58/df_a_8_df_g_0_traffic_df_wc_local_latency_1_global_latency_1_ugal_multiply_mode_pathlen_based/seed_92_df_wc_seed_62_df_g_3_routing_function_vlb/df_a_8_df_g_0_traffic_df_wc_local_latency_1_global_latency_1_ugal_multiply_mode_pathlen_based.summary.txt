vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 3;
df_wc_seed = 62;
routing_function = vlb;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.24643,    0.3584,    1284.4,    1261.2,         1,         0.0
simulation,         2,      0.25,   0.24861,   0.24995,    278.27,    284.44,         0,      6.4996
simulation,         3,      0.37,    0.2463,    0.3563,     838.1,    838.03,         1,         0.0
simulation,         4,      0.31,   0.24588,   0.30977,    523.37,    523.37,         1,         0.0
simulation,         5,      0.28,   0.24738,   0.27964,    587.85,    587.84,         1,         0.0
simulation,         6,      0.26,   0.24897,   0.25926,    566.99,    566.99,         1,         0.0
simulation,         7,      0.05,  0.050218,  0.050221,    15.407,    15.407,         0,      6.5016
simulation,         8,       0.1,   0.10012,   0.10012,    16.015,    16.015,         0,      6.5013
simulation,         9,      0.15,   0.15025,   0.15025,      17.1,      17.1,         0,      6.5006
simulation,        10,       0.2,   0.20013,   0.20013,    19.895,    19.894,         0,      6.4996
simulation,        11,      0.25,   0.24861,   0.24995,    278.27,    284.44,         0,      6.4996
