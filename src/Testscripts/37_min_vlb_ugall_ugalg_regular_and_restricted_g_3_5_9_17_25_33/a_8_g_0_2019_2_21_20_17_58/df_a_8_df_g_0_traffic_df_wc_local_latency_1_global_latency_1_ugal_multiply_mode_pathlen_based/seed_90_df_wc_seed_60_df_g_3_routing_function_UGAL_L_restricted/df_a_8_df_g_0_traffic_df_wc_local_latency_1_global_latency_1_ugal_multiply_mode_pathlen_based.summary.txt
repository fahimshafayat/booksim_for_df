vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 3;
df_wc_seed = 60;
routing_function = UGAL_L_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50013,    11.933,    11.933,         0,      3.9277
simulation,         2,      0.74,      0.74,   0.74014,    52.889,    52.925,         0,      4.0197
simulation,         3,      0.86,   0.73459,    0.7826,    698.51,    483.36,         1,         0.0
simulation,         4,       0.8,   0.72907,   0.73272,    944.25,    609.57,         1,         0.0
simulation,         5,      0.77,   0.74031,   0.74973,    553.44,    502.43,         1,         0.0
simulation,         6,      0.75,   0.74879,   0.75009,    134.75,     135.8,         0,      3.9024
simulation,         7,      0.76,   0.74399,   0.75112,    502.55,    421.33,         1,         0.0
simulation,         8,       0.1,   0.10056,   0.10056,    10.112,    10.112,         0,      3.9555
simulation,         9,       0.2,   0.20056,   0.20056,    10.399,    10.399,         0,      3.9594
simulation,        10,       0.3,    0.3002,   0.30022,    10.739,    10.739,         0,      3.9501
simulation,        11,       0.4,   0.39989,    0.3999,    11.215,    11.215,         0,      3.9397
simulation,        12,       0.5,   0.50014,   0.50013,    11.933,    11.933,         0,      3.9277
simulation,        13,       0.6,   0.60008,    0.6001,     13.37,     13.37,         0,      3.9259
simulation,        14,       0.7,   0.69993,   0.70001,    22.184,    22.186,         0,      3.9878
