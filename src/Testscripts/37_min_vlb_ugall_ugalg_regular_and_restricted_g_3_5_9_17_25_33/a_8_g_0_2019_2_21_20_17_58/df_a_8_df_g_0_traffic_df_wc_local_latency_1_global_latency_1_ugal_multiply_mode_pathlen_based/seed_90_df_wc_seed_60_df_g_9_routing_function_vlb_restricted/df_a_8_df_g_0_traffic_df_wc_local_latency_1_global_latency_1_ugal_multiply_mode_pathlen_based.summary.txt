vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 9;
df_wc_seed = 60;
routing_function = vlb_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.37677,   0.45114,    415.04,    412.94,         1,         0.0
simulation,         2,      0.25,   0.25017,   0.25019,    14.111,    14.111,         0,      5.1153
simulation,         3,      0.37,   0.37016,   0.37019,    20.585,    20.595,         0,      5.1153
simulation,         4,      0.43,   0.35499,   0.37252,    1465.6,    956.47,         1,         0.0
simulation,         5,       0.4,   0.37854,   0.38653,     543.2,    505.83,         1,         0.0
simulation,         6,      0.38,   0.38006,    0.3802,    28.109,    28.169,         0,      5.1155
simulation,         7,      0.39,   0.35686,   0.36642,    659.32,    471.01,         1,         0.0
simulation,         8,       0.1,  0.099925,   0.09993,    12.677,    12.677,         0,      5.1143
simulation,         9,       0.2,   0.20005,   0.20005,    13.456,    13.456,         0,      5.1156
simulation,        10,       0.3,   0.29993,   0.29995,    15.189,     15.19,         0,      5.1144
