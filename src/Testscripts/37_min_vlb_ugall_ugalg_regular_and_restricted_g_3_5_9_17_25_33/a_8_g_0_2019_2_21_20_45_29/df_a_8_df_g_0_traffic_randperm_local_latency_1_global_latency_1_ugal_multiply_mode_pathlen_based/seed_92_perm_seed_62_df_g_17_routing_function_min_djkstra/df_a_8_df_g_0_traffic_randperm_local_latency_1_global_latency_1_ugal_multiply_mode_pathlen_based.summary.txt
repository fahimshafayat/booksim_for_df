vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 17;
perm_seed = 62;
routing_function = min_djkstra;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.44344,   0.44493,    879.14,    380.34,         1,         0.0
simulation,         2,      0.25,   0.25008,   0.25008,    10.822,     10.82,         0,      3.4397
simulation,         3,      0.37,   0.36056,   0.36066,    476.46,    184.74,         1,         0.0
simulation,         4,      0.31,   0.30907,   0.30907,    189.15,    26.282,         0,      3.4392
simulation,         5,      0.34,    0.3365,   0.33725,    209.61,    126.87,         1,         0.0
simulation,         6,      0.32,   0.31887,   0.31886,    250.08,    26.548,         0,      3.4392
simulation,         7,      0.33,   0.32854,   0.32855,    327.35,     37.36,         0,      3.4392
simulation,         8,       0.1,   0.10011,   0.10011,     9.001,    9.0012,         0,        3.44
simulation,         9,       0.2,   0.20017,   0.20018,     9.226,    9.2261,         0,        3.44
simulation,        10,       0.3,   0.29927,   0.29928,    139.27,    26.393,         0,      3.4394
