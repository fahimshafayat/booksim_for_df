vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
perm_seed = 60;
routing_function = min;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49851,   0.49851,    154.25,     22.14,         0,      3.3281
simulation,         2,      0.74,   0.70007,   0.70016,    457.24,    129.53,         1,         0.0
simulation,         3,      0.62,   0.60986,   0.61016,    417.29,    102.75,         1,         0.0
simulation,         4,      0.56,   0.55758,   0.55756,    249.42,    22.831,         0,      3.3272
simulation,         5,      0.59,   0.58467,   0.58476,     438.2,     87.72,         0,      3.3269
simulation,         6,       0.6,    0.5929,   0.59291,    301.81,    86.722,         1,         0.0
simulation,         7,       0.1,   0.10047,   0.10047,    8.7649,     8.765,         0,      3.3262
simulation,         8,       0.2,   0.20032,   0.20032,    8.9163,    8.9165,         0,      3.3287
simulation,         9,       0.3,   0.30003,   0.30002,    9.1137,     9.114,         0,      3.3296
simulation,        10,       0.4,   0.40032,   0.40031,     10.37,    10.378,         0,      3.3293
simulation,        11,       0.5,   0.49851,   0.49851,    154.25,     22.14,         0,      3.3281
