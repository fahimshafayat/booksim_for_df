vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 25;
perm_seed = 60;
routing_function = UGAL_L;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49949,   0.49951,    66.343,     53.51,         0,      4.6973
simulation,         2,      0.74,   0.56348,   0.62918,    418.72,    379.32,         1,         0.0
simulation,         3,      0.62,   0.55058,   0.55146,    948.29,    400.26,         1,         0.0
simulation,         4,      0.56,   0.53744,   0.53811,    421.56,    223.35,         1,         0.0
simulation,         5,      0.53,   0.52279,   0.52296,    358.47,    130.45,         1,         0.0
simulation,         6,      0.51,   0.50851,   0.50857,    164.63,    71.294,         0,      4.7421
simulation,         7,      0.52,    0.5164,   0.51657,    331.11,    103.02,         0,      4.7865
simulation,         8,       0.1,  0.099895,  0.099902,    11.313,    11.313,         0,      4.5142
simulation,         9,       0.2,   0.19996,   0.19997,    11.676,    11.677,         0,      4.4937
simulation,        10,       0.3,   0.29999,       0.3,    12.331,    12.332,         0,      4.4544
simulation,        11,       0.4,   0.39988,   0.39987,    20.381,    20.408,         0,      4.4752
simulation,        12,       0.5,   0.49949,   0.49951,    66.343,     53.51,         0,      4.6973
