vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
perm_seed = 60;
routing_function = UGAL_G;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50007,   0.50007,    10.162,    10.162,         0,      3.4911
simulation,         2,      0.74,    0.7402,   0.74021,    11.747,    11.748,         0,      3.4594
simulation,         3,      0.86,   0.86027,   0.86027,     14.05,     14.05,         0,      3.4718
simulation,         4,      0.92,   0.92021,   0.92021,    16.439,    16.441,         0,      3.4855
simulation,         5,      0.95,   0.95009,   0.95007,    18.558,    18.558,         0,      3.4943
simulation,         6,      0.97,   0.97014,   0.97012,    20.809,    20.812,         0,      3.5013
simulation,         7,      0.98,   0.98011,   0.98012,    22.415,    22.418,         0,      3.5052
simulation,         8,       0.1,   0.10047,   0.10047,    9.5825,    9.5833,         0,      3.7097
simulation,         9,       0.2,   0.20032,   0.20032,    9.5935,    9.5938,         0,      3.6231
simulation,        10,       0.3,   0.30001,   0.30002,     9.693,    9.6934,         0,      3.5649
simulation,        11,       0.4,   0.40014,   0.40013,    9.8728,    9.8731,         0,      3.5213
simulation,        12,       0.5,   0.50007,   0.50007,    10.162,    10.162,         0,      3.4911
simulation,        13,       0.6,   0.60026,   0.60026,    10.602,    10.602,         0,      3.4703
simulation,        14,       0.7,   0.70013,   0.70014,    11.317,    11.317,         0,      3.4603
simulation,        15,       0.8,   0.80033,   0.80034,    12.639,     12.64,         0,      3.4614
simulation,        16,       0.9,   0.90028,   0.90029,     15.46,    15.462,         0,      3.4796
