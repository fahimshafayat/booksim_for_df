vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
perm_seed = 62;
routing_function = vlb;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49964,   0.49969,    65.458,    53.315,         0,      5.7298
simulation,         2,      0.74,   0.52858,    0.6356,    722.81,    678.22,         1,         0.0
simulation,         3,      0.62,   0.52585,   0.57371,    715.92,    598.74,         1,         0.0
simulation,         4,      0.56,   0.52525,   0.53339,    683.06,    491.28,         1,         0.0
simulation,         5,      0.53,   0.51875,   0.52006,    524.59,    288.03,         1,         0.0
simulation,         6,      0.51,   0.50697,   0.50777,    191.53,    112.46,         0,      5.7283
simulation,         7,      0.52,   0.51428,   0.51548,    374.51,    192.63,         0,      5.7255
simulation,         8,       0.1,  0.099932,  0.099931,    13.948,    13.948,         0,      5.7336
simulation,         9,       0.2,   0.19974,   0.19973,    14.684,    14.684,         0,      5.7293
simulation,        10,       0.3,   0.29976,   0.29977,    15.962,    15.962,         0,      5.7262
simulation,        11,       0.4,   0.39983,   0.39985,     18.87,    18.871,         0,      5.7282
simulation,        12,       0.5,   0.49964,   0.49969,    65.458,    53.315,         0,      5.7298
