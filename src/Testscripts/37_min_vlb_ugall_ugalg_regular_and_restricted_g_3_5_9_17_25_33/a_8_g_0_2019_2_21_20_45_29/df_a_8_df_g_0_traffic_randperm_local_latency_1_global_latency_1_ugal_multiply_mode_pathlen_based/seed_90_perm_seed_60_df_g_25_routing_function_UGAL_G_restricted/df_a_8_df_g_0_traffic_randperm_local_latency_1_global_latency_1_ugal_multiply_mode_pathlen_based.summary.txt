vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 25;
perm_seed = 60;
routing_function = UGAL_G_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49979,    0.4998,    13.207,    13.207,         0,      4.1399
simulation,         2,      0.74,   0.69719,   0.69815,     524.7,    242.24,         1,         0.0
simulation,         3,      0.62,   0.61995,   0.61995,    18.307,    18.308,         0,      4.2133
simulation,         4,      0.68,      0.68,   0.68001,    31.855,    31.861,         0,      4.3162
simulation,         5,      0.71,   0.69782,    0.6979,    417.67,    140.71,         1,         0.0
simulation,         6,      0.69,   0.69005,   0.69004,    40.031,    40.039,         0,      4.3433
simulation,         7,       0.7,   0.69886,    0.6989,    111.31,    77.554,         0,      4.3873
simulation,         8,       0.1,  0.099899,  0.099902,    10.666,    10.666,         0,      4.2156
simulation,         9,       0.2,   0.19997,   0.19997,    10.935,    10.936,         0,      4.1892
simulation,        10,       0.3,   0.29999,       0.3,    11.331,    11.332,         0,       4.162
simulation,        11,       0.4,   0.39987,   0.39987,    11.959,     11.96,         0,       4.141
simulation,        12,       0.5,   0.49979,    0.4998,    13.207,    13.207,         0,      4.1399
simulation,        13,       0.6,    0.5999,   0.59989,    16.774,    16.775,         0,      4.1923
simulation,        14,       0.7,   0.69886,    0.6989,    111.31,    77.554,         0,      4.3873
