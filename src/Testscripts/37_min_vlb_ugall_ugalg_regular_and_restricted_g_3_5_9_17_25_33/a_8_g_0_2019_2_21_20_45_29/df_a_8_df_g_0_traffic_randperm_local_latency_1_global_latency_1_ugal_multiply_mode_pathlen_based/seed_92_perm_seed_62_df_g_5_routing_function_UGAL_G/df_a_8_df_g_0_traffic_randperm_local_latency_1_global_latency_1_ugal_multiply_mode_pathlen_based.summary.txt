vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
perm_seed = 62;
routing_function = UGAL_G;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49983,   0.49982,     10.25,    10.251,         0,      3.5216
simulation,         2,      0.74,   0.73951,   0.73949,     11.88,    11.881,         0,      3.4865
simulation,         3,      0.86,   0.85974,   0.85973,    14.369,    14.369,         0,      3.4985
simulation,         4,      0.92,   0.91992,   0.91994,    17.034,    17.035,         0,      3.5137
simulation,         5,      0.95,   0.94995,   0.94991,    19.497,    19.497,         0,      3.5232
simulation,         6,      0.97,   0.96992,   0.96994,    22.132,    22.133,         0,      3.5301
simulation,         7,      0.98,   0.98006,   0.98005,    24.272,    24.278,         0,      3.5344
simulation,         8,       0.1,  0.099936,  0.099931,    9.6819,    9.6824,         0,       3.756
simulation,         9,       0.2,   0.19974,   0.19973,    9.6766,    9.6771,         0,      3.6601
simulation,        10,       0.3,   0.29977,   0.29977,    9.7753,    9.7754,         0,      3.5981
simulation,        11,       0.4,   0.39985,   0.39985,    9.9553,    9.9555,         0,      3.5525
simulation,        12,       0.5,   0.49983,   0.49982,     10.25,    10.251,         0,      3.5216
simulation,        13,       0.6,   0.59909,   0.59907,      10.7,    10.701,         0,       3.501
simulation,        14,       0.7,   0.69944,   0.69942,     11.44,    11.441,         0,       3.488
simulation,        15,       0.8,   0.79937,   0.79936,    12.834,    12.835,         0,      3.4891
simulation,        16,       0.9,   0.89994,   0.89997,    15.978,    15.979,         0,      3.5087
