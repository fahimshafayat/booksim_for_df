vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 25;
perm_seed = 60;
routing_function = min_djkstra;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.41608,    0.4379,    534.51,    390.21,         1,         0.0
simulation,         2,      0.25,   0.24971,   0.24977,    101.96,    30.688,         0,      3.6111
simulation,         3,      0.37,   0.34429,   0.34691,    618.03,    409.83,         1,         0.0
simulation,         4,      0.31,    0.3024,   0.30243,    474.23,    184.21,         1,         0.0
simulation,         5,      0.28,   0.27672,   0.27684,    305.68,    139.71,         1,         0.0
simulation,         6,      0.26,    0.2586,     0.259,    249.91,    122.65,         0,      3.6116
simulation,         7,      0.27,   0.26764,   0.26786,    395.63,    140.34,         0,      3.6118
simulation,         8,      0.05,  0.049932,  0.049935,    9.2868,    9.2868,         0,      3.6119
simulation,         9,       0.1,  0.099898,  0.099902,    9.3714,    9.3715,         0,      3.6111
simulation,        10,      0.15,   0.14992,   0.14993,    9.4993,    9.4995,         0,      3.6113
simulation,        11,       0.2,   0.19996,   0.19997,    10.232,    10.232,         0,      3.6115
simulation,        12,      0.25,   0.24971,   0.24977,    101.96,    30.688,         0,      3.6111
