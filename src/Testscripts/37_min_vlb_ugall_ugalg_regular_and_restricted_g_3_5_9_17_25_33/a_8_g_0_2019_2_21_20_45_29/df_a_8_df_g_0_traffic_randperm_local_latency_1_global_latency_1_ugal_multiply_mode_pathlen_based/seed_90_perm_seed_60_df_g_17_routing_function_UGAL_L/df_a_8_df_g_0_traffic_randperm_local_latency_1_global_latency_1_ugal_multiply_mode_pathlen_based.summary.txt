vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 17;
perm_seed = 60;
routing_function = UGAL_L;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49994,   0.49994,    19.102,    19.127,         0,      4.2859
simulation,         2,      0.74,   0.63005,   0.66167,     562.4,    378.14,         1,         0.0
simulation,         3,      0.62,   0.61555,   0.61562,    364.73,    88.395,         0,      4.5642
simulation,         4,      0.68,    0.6286,   0.63121,    709.49,    343.39,         1,         0.0
simulation,         5,      0.65,   0.62756,   0.62799,    434.44,    214.72,         1,         0.0
simulation,         6,      0.63,   0.62118,   0.62133,    379.38,    128.17,         1,         0.0
simulation,         7,       0.1,  0.099913,  0.099915,    11.064,    11.065,         0,       4.404
simulation,         8,       0.2,   0.19997,   0.19998,     11.33,     11.33,         0,      4.3679
simulation,         9,       0.3,   0.29996,   0.29997,    11.661,    11.661,         0,      4.3061
simulation,        10,       0.4,    0.3999,   0.39992,     12.53,     12.53,         0,      4.2682
simulation,        11,       0.5,   0.49994,   0.49994,    19.102,    19.127,         0,      4.2859
simulation,        12,       0.6,   0.59876,   0.59879,    102.88,     53.77,         0,      4.4659
