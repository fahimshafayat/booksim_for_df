vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 60;
routing_function = UGAL_G;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50012,   0.50013,    9.8723,    9.8728,         0,      3.3867
simulation,         2,      0.74,   0.74012,   0.74014,    11.264,    11.265,         0,      3.3544
simulation,         3,      0.86,   0.86026,   0.86025,    13.214,    13.214,         0,      3.3632
simulation,         4,      0.92,   0.92018,   0.92019,    15.118,     15.12,         0,      3.3746
simulation,         5,      0.95,   0.95022,   0.95025,    16.648,     16.65,         0,      3.3807
simulation,         6,      0.97,   0.97032,   0.97029,    18.244,    18.246,         0,       3.387
simulation,         7,      0.98,   0.98021,   0.98018,     19.31,    19.312,         0,        3.39
simulation,         8,       0.1,   0.10055,   0.10056,    9.3219,    9.3223,         0,      3.5856
simulation,         9,       0.2,   0.20055,   0.20056,    9.3383,    9.3384,         0,      3.5082
simulation,        10,       0.3,   0.30021,   0.30022,    9.4309,    9.4314,         0,      3.4545
simulation,        11,       0.4,    0.3999,    0.3999,    9.6056,    9.6063,         0,       3.415
simulation,        12,       0.5,   0.50012,   0.50013,    9.8723,    9.8728,         0,      3.3867
simulation,        13,       0.6,    0.6001,    0.6001,    10.261,    10.261,         0,      3.3657
simulation,        14,       0.7,   0.69998,   0.70001,    10.893,    10.894,         0,      3.3565
simulation,        15,       0.8,   0.80011,    0.8001,    12.038,    12.039,         0,      3.3558
simulation,        16,       0.9,   0.90014,   0.90014,    14.376,    14.376,         0,      3.3697
