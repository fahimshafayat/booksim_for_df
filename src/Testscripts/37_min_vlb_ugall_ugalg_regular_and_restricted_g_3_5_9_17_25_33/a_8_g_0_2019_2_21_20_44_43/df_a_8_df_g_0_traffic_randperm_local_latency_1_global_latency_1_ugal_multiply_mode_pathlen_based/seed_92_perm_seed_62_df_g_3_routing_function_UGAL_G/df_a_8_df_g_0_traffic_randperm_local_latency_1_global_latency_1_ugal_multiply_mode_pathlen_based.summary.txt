vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 62;
routing_function = UGAL_G;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50043,   0.50043,    9.6697,      9.67,         0,      3.3131
simulation,         2,      0.74,   0.73983,   0.73984,    11.052,    11.052,         0,      3.2865
simulation,         3,      0.86,   0.85977,   0.85976,    13.112,    13.112,         0,      3.3043
simulation,         4,      0.92,   0.91987,   0.91985,    15.248,    15.248,         0,      3.3234
simulation,         5,      0.95,   0.94983,   0.94985,    16.765,    16.766,         0,      3.3323
simulation,         6,      0.97,   0.96985,   0.96987,    18.498,      18.5,         0,      3.3403
simulation,         7,      0.98,   0.97992,   0.97993,    19.699,    19.701,         0,      3.3438
simulation,         8,       0.1,   0.10012,   0.10012,    9.1187,    9.1192,         0,      3.4889
simulation,         9,       0.2,   0.20014,   0.20013,    9.1574,    9.1577,         0,      3.4262
simulation,        10,       0.3,   0.30055,   0.30053,    9.2487,    9.2491,         0,      3.3748
simulation,        11,       0.4,   0.40055,   0.40055,     9.424,    9.4244,         0,      3.3409
simulation,        12,       0.5,   0.50043,   0.50043,    9.6697,      9.67,         0,      3.3131
simulation,        13,       0.6,   0.59985,   0.59986,    10.046,    10.046,         0,      3.2959
simulation,        14,       0.7,   0.69987,   0.69987,    10.669,     10.67,         0,      3.2872
simulation,        15,       0.8,   0.79959,   0.79957,    11.859,     11.86,         0,      3.2913
simulation,        16,       0.9,   0.89988,   0.89985,     14.39,     14.39,         0,       3.316
