vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 9;
perm_seed = 60;
routing_function = vlb_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.40512,   0.40562,    1271.6,    618.61,         1,         0.0
simulation,         2,      0.25,   0.25018,   0.25019,    12.989,    12.989,         0,      4.8621
simulation,         3,      0.37,   0.37019,   0.37019,    14.717,    14.717,         0,      4.8617
simulation,         4,      0.43,   0.43012,   0.43014,    18.281,    18.288,         0,      4.8603
simulation,         5,      0.46,   0.39044,   0.40442,    492.57,    363.44,         1,         0.0
simulation,         6,      0.44,   0.43994,   0.44005,    26.425,    26.818,         0,      4.8604
simulation,         7,      0.45,   0.41282,    0.4159,    437.67,    293.27,         1,         0.0
simulation,         8,       0.1,  0.099927,   0.09993,    12.076,    12.076,         0,      4.8623
simulation,         9,       0.2,   0.20004,   0.20005,    12.605,    12.606,         0,      4.8617
simulation,        10,       0.3,   0.29994,   0.29995,    13.509,     13.51,         0,      4.8615
simulation,        11,       0.4,   0.40021,   0.40022,    15.607,    15.609,         0,      4.8611
