vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 9;
perm_seed = 60;
routing_function = UGAL_G_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50012,   0.50013,    11.107,    11.107,         0,      3.7534
simulation,         2,      0.74,   0.73994,   0.73995,    13.657,    13.658,         0,      3.7242
simulation,         3,      0.86,   0.85986,   0.85987,    19.585,    19.588,         0,      3.7435
simulation,         4,      0.92,   0.91925,   0.91998,    61.374,    61.522,         0,      3.7827
simulation,         5,      0.95,   0.90854,   0.90965,    511.09,    193.32,         1,         0.0
simulation,         6,      0.93,   0.91086,   0.91093,    515.12,    147.25,         1,         0.0
simulation,         7,       0.1,  0.099929,   0.09993,    9.9063,    9.9066,         0,      3.8593
simulation,         8,       0.2,   0.20004,   0.20005,    10.084,    10.084,         0,      3.8295
simulation,         9,       0.3,   0.29995,   0.29995,    10.325,    10.325,         0,      3.8009
simulation,        10,       0.4,   0.40021,   0.40022,    10.656,    10.656,         0,      3.7763
simulation,        11,       0.5,   0.50012,   0.50013,    11.107,    11.107,         0,      3.7534
simulation,        12,       0.6,   0.59988,   0.59988,    11.777,    11.778,         0,      3.7364
simulation,        13,       0.7,   0.69978,   0.69979,    12.918,    12.919,         0,      3.7256
simulation,        14,       0.8,   0.80001,   0.80002,    15.442,    15.443,         0,      3.7281
simulation,        15,       0.9,   0.89993,   0.89993,    29.045,    29.053,         0,      3.7636
