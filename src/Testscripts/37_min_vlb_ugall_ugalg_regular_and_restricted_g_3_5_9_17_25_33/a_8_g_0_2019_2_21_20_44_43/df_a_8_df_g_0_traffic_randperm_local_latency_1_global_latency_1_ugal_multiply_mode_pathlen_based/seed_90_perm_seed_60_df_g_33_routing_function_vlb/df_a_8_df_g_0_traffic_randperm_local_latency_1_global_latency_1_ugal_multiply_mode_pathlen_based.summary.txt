vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 60;
routing_function = vlb;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.48075,    0.4889,    522.24,    456.98,         1,         0.0
simulation,         2,      0.25,   0.25001,   0.25002,    17.182,    17.182,         0,      6.3615
simulation,         3,      0.37,   0.36991,   0.36993,    21.412,    21.412,         0,      6.3612
simulation,         4,      0.43,   0.42981,   0.42985,    28.294,    28.294,         0,       6.361
simulation,         5,      0.46,   0.45989,   0.45992,    38.498,    38.501,         0,       6.361
simulation,         6,      0.48,   0.47989,   0.47995,    64.451,    64.501,         0,      6.3612
simulation,         7,      0.49,   0.48242,   0.48598,    438.12,     339.2,         0,      6.3605
simulation,         8,       0.1,  0.099897,  0.099898,    15.341,    15.342,         0,      6.3599
simulation,         9,       0.2,       0.2,   0.20001,    16.375,    16.375,         0,      6.3611
simulation,        10,       0.3,   0.29996,   0.29998,    18.362,    18.362,         0,      6.3611
simulation,        11,       0.4,   0.39986,   0.39989,    23.919,    23.919,         0,      6.3614
