vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 5;
df_wc_seed = 62;
routing_function = UGAL_L;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,       0.0,       0.0,       0.0,       0.0,         0,         0.0
simulation,         2,      0.74,       0.0,       0.0,       0.0,       0.0,         0,         0.0
simulation,         3,      0.86,       0.0,       0.0,       0.0,       0.0,         0,         0.0
simulation,         4,      0.92,       0.0,       0.0,       0.0,       0.0,         0,         0.0
simulation,         5,      0.95,       0.0,       0.0,       0.0,       0.0,         0,         0.0
simulation,         6,      0.97,       0.0,       0.0,       0.0,       0.0,         0,         0.0
simulation,         7,      0.98,       0.0,       0.0,       0.0,       0.0,         0,         0.0
simulation,         8,       0.1,       0.0,       0.0,       0.0,       0.0,         0,         0.0
simulation,         9,       0.2,       0.0,       0.0,       0.0,       0.0,         0,         0.0
simulation,        10,       0.3,       0.0,       0.0,       0.0,       0.0,         0,         0.0
