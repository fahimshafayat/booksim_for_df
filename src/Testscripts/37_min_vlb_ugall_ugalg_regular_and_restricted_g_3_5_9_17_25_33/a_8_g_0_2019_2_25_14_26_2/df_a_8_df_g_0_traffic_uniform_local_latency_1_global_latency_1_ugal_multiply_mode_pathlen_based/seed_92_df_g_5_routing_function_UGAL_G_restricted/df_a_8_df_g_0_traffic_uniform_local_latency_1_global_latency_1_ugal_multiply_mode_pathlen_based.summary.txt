vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_G_restricted;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49982,   0.49982,    10.522,    10.523,         0,      3.5126
simulation,         2,      0.74,   0.73952,   0.73949,    12.448,    12.449,         0,      3.4715
simulation,         3,      0.86,   0.85975,   0.85973,    15.022,    15.023,         0,      3.4521
simulation,         4,      0.92,   0.91994,   0.91994,    18.517,    18.519,         0,      3.4445
simulation,         5,      0.95,   0.94989,   0.94991,    22.665,    22.674,         0,      3.4396
simulation,         6,      0.97,   0.96998,   0.96994,    29.633,    29.669,         0,      3.4371
simulation,         7,      0.98,   0.98013,   0.98005,    38.235,    38.276,         0,      3.4361
simulation,         8,       0.1,  0.099935,  0.099931,    9.3834,    9.3838,         0,      3.5998
simulation,         9,       0.2,   0.19973,   0.19973,    9.5609,     9.561,         0,      3.5771
simulation,        10,       0.3,   0.29978,   0.29977,    9.7912,    9.7914,         0,      3.5536
simulation,        11,       0.4,   0.39986,   0.39985,    10.101,    10.102,         0,      3.5327
simulation,        12,       0.5,   0.49982,   0.49982,    10.522,    10.523,         0,      3.5126
simulation,        13,       0.6,   0.59909,   0.59907,    11.097,    11.098,         0,      3.4958
simulation,        14,       0.7,   0.69944,   0.69942,     11.97,    11.971,         0,      3.4792
simulation,        15,       0.8,   0.79936,   0.79936,     13.45,    13.451,         0,      3.4636
simulation,        16,       0.9,   0.89994,   0.89997,    16.928,    16.931,         0,      3.4478
