vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_L_restricted;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50012,   0.50012,    11.937,    11.937,         0,      3.9576
simulation,         2,      0.74,   0.73999,   0.74001,    14.908,    14.908,         0,      3.8748
simulation,         3,      0.86,   0.86014,   0.86014,    19.771,    19.773,         0,       3.835
simulation,         4,      0.92,   0.92015,   0.92012,    27.397,    27.402,         0,       3.815
simulation,         5,      0.95,   0.95008,   0.95004,    41.037,    41.056,         0,      3.8129
simulation,         6,      0.97,   0.97032,   0.96999,    83.279,    83.333,         0,      3.7941
simulation,         7,      0.98,    0.9798,   0.97996,    99.089,    99.224,         0,      3.7668
simulation,         8,       0.1,   0.10011,   0.10011,    10.413,    10.413,         0,      4.0898
simulation,         9,       0.2,   0.20018,   0.20018,    10.757,    10.757,         0,      4.1066
simulation,        10,       0.3,   0.30013,   0.30013,    11.046,    11.046,         0,       4.062
simulation,        11,       0.4,    0.4001,    0.4001,    11.405,    11.406,         0,      4.0061
simulation,        12,       0.5,   0.50012,   0.50012,    11.937,    11.937,         0,      3.9576
simulation,        13,       0.6,   0.59999,   0.60001,    12.753,    12.754,         0,      3.9195
simulation,        14,       0.7,   0.69989,    0.6999,    14.094,    14.094,         0,      3.8868
simulation,        15,       0.8,   0.80003,   0.80004,    16.664,    16.665,         0,      3.8552
simulation,        16,       0.9,   0.90013,   0.90012,    23.852,    23.856,         0,      3.8216
