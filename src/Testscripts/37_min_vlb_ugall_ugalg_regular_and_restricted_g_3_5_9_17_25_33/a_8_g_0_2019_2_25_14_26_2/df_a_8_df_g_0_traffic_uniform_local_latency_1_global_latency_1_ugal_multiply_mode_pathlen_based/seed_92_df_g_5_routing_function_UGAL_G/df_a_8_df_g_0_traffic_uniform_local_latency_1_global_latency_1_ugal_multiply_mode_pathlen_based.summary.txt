vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_G;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49982,   0.49982,    10.357,    10.357,         0,      3.4858
simulation,         2,      0.74,   0.73951,   0.73949,    12.213,    12.214,         0,      3.4377
simulation,         3,      0.86,   0.85975,   0.85973,     14.92,    14.921,         0,      3.4254
simulation,         4,      0.92,   0.91993,   0.91994,    18.495,    18.499,         0,      3.4214
simulation,         5,      0.95,   0.94981,   0.94991,    22.947,    22.954,         0,        3.42
simulation,         6,      0.97,    0.9699,   0.96994,    30.168,    30.184,         0,      3.4208
simulation,         7,      0.98,   0.97993,   0.98005,    37.847,    37.932,         0,      3.4207
simulation,         8,       0.1,  0.099926,  0.099931,    9.6644,    9.6648,         0,      3.7373
simulation,         9,       0.2,   0.19974,   0.19973,    9.6673,    9.6679,         0,      3.6338
simulation,        10,       0.3,   0.29978,   0.29977,    9.7938,    9.7943,         0,      3.5696
simulation,        11,       0.4,   0.39985,   0.39985,    10.014,    10.015,         0,      3.5201
simulation,        12,       0.5,   0.49982,   0.49982,    10.357,    10.357,         0,      3.4858
simulation,        13,       0.6,   0.59909,   0.59907,    10.885,    10.886,         0,      3.4614
simulation,        14,       0.7,   0.69944,   0.69942,    11.729,    11.729,         0,      3.4441
simulation,        15,       0.8,   0.79939,   0.79936,    13.235,    13.236,         0,      3.4308
simulation,        16,       0.9,   0.89999,   0.89997,    16.955,    16.957,         0,      3.4238
