vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_L;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50006,   0.50007,    11.117,    11.118,         0,      3.7229
simulation,         2,      0.74,   0.74018,   0.74021,    13.232,    13.233,         0,      3.6314
simulation,         3,      0.86,   0.86023,   0.86027,     16.28,    16.281,         0,      3.5972
simulation,         4,      0.92,   0.92022,   0.92021,    20.478,    20.483,         0,      3.5843
simulation,         5,      0.95,   0.95001,   0.95007,    25.125,    25.135,         0,      3.5773
simulation,         6,      0.97,   0.97008,   0.97012,    32.602,     32.62,         0,      3.5743
simulation,         7,      0.98,   0.98007,   0.98012,    41.511,    41.532,         0,      3.5731
simulation,         8,       0.1,   0.10047,   0.10047,    10.091,    10.091,         0,      3.9378
simulation,         9,       0.2,   0.20032,   0.20032,    10.314,    10.315,         0,      3.9154
simulation,        10,       0.3,   0.30002,   0.30002,    10.478,    10.478,         0,      3.8417
simulation,        11,       0.4,   0.40013,   0.40013,    10.737,    10.738,         0,      3.7788
simulation,        12,       0.5,   0.50006,   0.50007,    11.117,    11.118,         0,      3.7229
simulation,        13,       0.6,   0.60025,   0.60026,    11.724,    11.725,         0,      3.6808
simulation,        14,       0.7,    0.7001,   0.70014,    12.666,    12.667,         0,      3.6442
simulation,        15,       0.8,   0.80035,   0.80034,    14.377,    14.378,         0,      3.6133
simulation,        16,       0.9,   0.90032,   0.90029,    18.569,    18.571,         0,      3.5883
