vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_G_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49993,   0.49994,     11.35,     11.35,         0,      3.7838
simulation,         2,      0.74,   0.74028,   0.74027,    13.891,    13.891,         0,      3.7281
simulation,         3,      0.86,   0.86009,   0.86008,    17.672,    17.674,         0,      3.7099
simulation,         4,      0.92,   0.92008,   0.92006,    22.798,      22.8,         0,      3.7016
simulation,         5,      0.95,   0.95003,      0.95,    28.647,    28.657,         0,      3.6971
simulation,         6,      0.97,    0.9699,   0.96992,    37.595,    37.614,         0,      3.6937
simulation,         7,      0.98,   0.98001,   0.97993,    47.238,    47.272,         0,      3.6907
simulation,         8,       0.1,  0.099911,  0.099915,    10.163,    10.164,         0,      3.9709
simulation,         9,       0.2,   0.19997,   0.19998,    10.298,    10.299,         0,      3.9061
simulation,        10,       0.3,   0.29997,   0.29997,    10.522,    10.522,         0,      3.8549
simulation,        11,       0.4,   0.39991,   0.39992,    10.857,    10.858,         0,      3.8147
simulation,        12,       0.5,   0.49993,   0.49994,     11.35,     11.35,         0,      3.7838
simulation,        13,       0.6,   0.60001,   0.60001,    12.068,    12.069,         0,      3.7572
simulation,        14,       0.7,   0.70018,   0.70018,    13.224,    13.224,         0,      3.7366
simulation,        15,       0.8,   0.80021,   0.80021,    15.296,    15.297,         0,      3.7178
simulation,        16,       0.9,   0.90006,   0.90004,    20.528,     20.53,         0,      3.7051
