vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 17;
df_wc_seed = 60;
routing_function = UGAL_L;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.38594,   0.45706,    462.61,    451.15,         1,         0.0
simulation,         2,      0.25,   0.25005,   0.25006,    42.718,    42.848,         0,      5.8011
simulation,         3,      0.37,   0.36028,   0.36742,    558.86,    525.21,         1,         0.0
simulation,         4,      0.31,   0.30994,   0.30995,    46.518,    46.717,         0,      5.9359
simulation,         5,      0.34,   0.33992,   0.33995,    53.338,    53.655,         0,      5.9869
simulation,         6,      0.35,   0.34985,   0.34992,    62.032,    62.619,         0,       6.001
simulation,         7,      0.36,   0.35859,   0.35977,    178.96,     184.5,         0,       6.013
simulation,         8,       0.1,  0.099912,  0.099915,    13.633,    13.633,         0,      5.0045
simulation,         9,       0.2,   0.19997,   0.19998,    41.856,    41.952,         0,      5.6308
simulation,        10,       0.3,   0.29996,   0.29997,    45.428,    45.614,         0,      5.9169
