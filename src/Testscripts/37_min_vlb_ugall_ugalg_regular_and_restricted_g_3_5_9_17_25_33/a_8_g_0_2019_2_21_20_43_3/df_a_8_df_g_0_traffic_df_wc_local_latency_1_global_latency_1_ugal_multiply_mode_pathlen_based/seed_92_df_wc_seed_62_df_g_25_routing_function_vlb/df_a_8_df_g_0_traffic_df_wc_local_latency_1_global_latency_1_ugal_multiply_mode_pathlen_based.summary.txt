vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 25;
df_wc_seed = 62;
routing_function = vlb;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35596,     0.459,    708.05,    701.78,         1,         0.0
simulation,         2,      0.25,   0.24986,   0.24986,    18.382,    18.382,         0,      6.5006
simulation,         3,      0.37,   0.35365,   0.36801,    608.92,    633.35,         1,         0.0
simulation,         4,      0.31,   0.30979,   0.30981,    22.347,    22.348,         0,      6.4999
simulation,         5,      0.34,   0.33983,   0.33981,    31.131,     31.14,         0,      6.5001
simulation,         6,      0.35,   0.34979,    0.3498,    43.769,    43.822,         0,      6.5002
simulation,         7,      0.36,    0.3562,   0.35963,    355.39,    367.43,         0,         6.5
simulation,         8,       0.1,  0.099977,  0.099978,     15.71,     15.71,         0,      6.5011
simulation,         9,       0.2,   0.19989,   0.19988,    17.067,    17.068,         0,      6.5005
simulation,        10,       0.3,   0.29977,   0.29978,    21.224,    21.225,         0,      6.4998
