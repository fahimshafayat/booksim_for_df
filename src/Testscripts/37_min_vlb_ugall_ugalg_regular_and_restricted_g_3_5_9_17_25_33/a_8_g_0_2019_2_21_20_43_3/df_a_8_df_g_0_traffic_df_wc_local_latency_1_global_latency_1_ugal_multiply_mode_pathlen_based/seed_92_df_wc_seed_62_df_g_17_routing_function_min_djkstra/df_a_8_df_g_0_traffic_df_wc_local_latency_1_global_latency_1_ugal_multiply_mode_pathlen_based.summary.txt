vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 17;
df_wc_seed = 62;
routing_function = min_djkstra;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,  0.062404,   0.15653,    2095.0,    2094.5,         1,         0.0
simulation,         2,      0.25,  0.062399,   0.15654,    1688.3,    1688.3,         1,         0.0
simulation,         3,      0.13,  0.062386,   0.12861,    966.79,    966.79,         1,         0.0
simulation,         4,      0.07,  0.062301,  0.069756,    440.46,    440.46,         1,         0.0
simulation,         5,      0.04,  0.040025,  0.040026,    10.001,    10.001,         0,      3.5578
simulation,         6,      0.05,  0.050037,  0.050043,     11.19,    11.191,         0,      3.5579
simulation,         7,      0.06,  0.059851,  0.059891,    128.32,     80.59,         0,      3.5588
simulation,         8,      0.02,  0.020036,  0.020037,     9.354,     9.354,         0,      3.5574
simulation,         9,      0.04,  0.040025,  0.040026,    10.001,    10.001,         0,      3.5578
simulation,        10,      0.06,  0.059851,  0.059891,    128.32,     80.59,         0,      3.5588
