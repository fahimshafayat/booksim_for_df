vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 25;
df_wc_seed = 60;
routing_function = UGAL_L;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.34908,   0.42065,    602.76,    562.78,         1,         0.0
simulation,         2,      0.25,   0.25002,   0.25004,    38.337,    38.468,         0,      6.0008
simulation,         3,      0.37,   0.34244,   0.34485,     730.0,    495.12,         1,         0.0
simulation,         4,      0.31,   0.30349,   0.30383,    482.59,    293.36,         1,         0.0
simulation,         5,      0.28,   0.27898,   0.28001,    145.76,     150.1,         0,      6.0527
simulation,         6,      0.29,   0.28707,   0.28855,    328.03,    268.52,         0,      6.0663
simulation,         7,       0.3,   0.29531,   0.29611,    445.03,    287.04,         1,         0.0
simulation,         8,      0.05,  0.049933,  0.049935,     12.75,     12.75,         0,      4.9998
simulation,         9,       0.1,  0.099895,  0.099902,    25.806,    25.844,         0,      5.3543
simulation,        10,      0.15,   0.14992,   0.14993,    37.139,    37.209,         0,      5.6768
simulation,        11,       0.2,   0.19996,   0.19997,    36.913,    37.006,         0,      5.8778
simulation,        12,      0.25,   0.25002,   0.25004,    38.337,    38.468,         0,      6.0008
