vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 25;
df_wc_seed = 62;
routing_function = min;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,  0.036193,   0.12974,    2154.1,    2153.7,         1,         0.0
simulation,         2,      0.25,  0.036189,   0.12968,    1804.4,    1804.4,         1,         0.0
simulation,         3,      0.13,  0.036185,   0.11644,    1196.0,    1196.0,         1,         0.0
simulation,         4,      0.07,   0.03617,  0.069815,    895.52,    895.52,         1,         0.0
simulation,         5,      0.04,  0.032664,  0.039949,     821.6,     821.6,         1,         0.0
simulation,         6,      0.02,  0.019992,  0.019993,    10.261,    10.262,         0,      3.7502
simulation,         7,      0.03,  0.030025,  0.030025,    20.208,    20.217,         0,      3.7504
simulation,         8,      0.02,  0.019992,  0.019993,    10.261,    10.262,         0,      3.7502
