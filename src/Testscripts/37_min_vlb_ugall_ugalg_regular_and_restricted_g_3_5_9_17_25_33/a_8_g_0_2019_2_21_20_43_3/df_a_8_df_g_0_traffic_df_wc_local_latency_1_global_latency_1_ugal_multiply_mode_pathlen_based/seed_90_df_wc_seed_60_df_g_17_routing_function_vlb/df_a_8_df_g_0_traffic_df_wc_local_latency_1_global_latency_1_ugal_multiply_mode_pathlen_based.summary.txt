vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 17;
df_wc_seed = 60;
routing_function = vlb;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.45829,   0.47615,    1028.0,    954.72,         1,         0.0
simulation,         2,      0.25,   0.25004,   0.25006,    17.775,    17.775,         0,      6.5001
simulation,         3,      0.37,   0.36994,   0.36995,    23.512,    23.512,         0,      6.4998
simulation,         4,      0.43,      0.43,   0.42998,    38.425,    38.426,         0,      6.5001
simulation,         5,      0.46,   0.45993,   0.45994,    118.36,    118.35,         0,         6.5
simulation,         6,      0.48,   0.45886,   0.47791,    528.36,    524.06,         1,         0.0
simulation,         7,      0.47,   0.46024,   0.46791,    534.75,    510.03,         1,         0.0
simulation,         8,       0.1,  0.099912,  0.099915,    15.671,    15.671,         0,      6.5002
simulation,         9,       0.2,   0.19997,   0.19998,    16.827,    16.827,         0,      6.5001
simulation,        10,       0.3,   0.29996,   0.29997,    19.223,    19.223,         0,      6.4999
simulation,        11,       0.4,    0.3999,   0.39992,    27.842,    27.841,         0,      6.4995
