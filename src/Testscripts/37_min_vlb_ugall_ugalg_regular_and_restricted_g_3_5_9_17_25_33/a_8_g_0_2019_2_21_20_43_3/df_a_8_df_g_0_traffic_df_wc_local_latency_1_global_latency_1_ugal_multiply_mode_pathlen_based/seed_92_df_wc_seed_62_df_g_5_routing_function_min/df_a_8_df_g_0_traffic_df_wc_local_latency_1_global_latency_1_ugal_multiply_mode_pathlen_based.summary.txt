vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 5;
df_wc_seed = 62;
routing_function = min;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.24952,    0.3571,    1239.2,    1207.4,         1,         0.0
simulation,         2,      0.25,    0.2493,   0.24989,    145.44,    148.52,         0,        3.75
simulation,         3,      0.37,   0.24948,   0.35603,    790.29,    790.16,         1,         0.0
simulation,         4,      0.31,    0.2497,   0.30114,    957.14,    955.14,         1,         0.0
simulation,         5,      0.28,   0.24964,   0.27958,    525.89,    525.89,         1,         0.0
simulation,         6,      0.26,      0.25,   0.26021,    656.92,    686.28,         1,         0.0
simulation,         7,      0.05,  0.050043,  0.050043,    9.6545,    9.6545,         0,      3.7486
simulation,         8,       0.1,   0.09993,  0.099931,    9.9033,    9.9032,         0,      3.7495
simulation,         9,      0.15,   0.14999,   0.14998,    10.359,    10.359,         0,        3.75
simulation,        10,       0.2,   0.19974,   0.19973,    11.593,    11.592,         0,      3.7506
simulation,        11,      0.25,    0.2493,   0.24989,    145.44,    148.52,         0,        3.75
