vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 25;
df_wc_seed = 60;
routing_function = vlb;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35637,   0.45819,    705.59,    699.35,         1,         0.0
simulation,         2,      0.25,   0.25003,   0.25004,    18.432,    18.432,         0,      6.4997
simulation,         3,      0.37,   0.35469,   0.36805,    611.57,    641.13,         1,         0.0
simulation,         4,      0.31,   0.30995,   0.30998,    22.494,    22.495,         0,         6.5
simulation,         5,      0.34,   0.33996,   0.33998,     31.49,    31.496,         0,      6.5007
simulation,         6,      0.35,   0.34989,   0.34994,    44.955,    45.003,         0,      6.4996
simulation,         7,      0.36,   0.35498,   0.35944,    378.01,    400.02,         0,      6.5001
simulation,         8,       0.1,  0.099898,  0.099902,     15.71,     15.71,         0,      6.4996
simulation,         9,       0.2,   0.19996,   0.19997,    17.085,    17.085,         0,      6.5003
simulation,        10,       0.3,   0.29998,       0.3,    21.359,    21.358,         0,      6.5003
