wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
global_latency = 40;
internal_speedup = 2.0;
local_latency = 40;
perm_seed = 50;
routing_function = UGAL_L_restricted_src_only;
seed = 80;
vc_buf_size = 64;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50012,   0.50015,    119.66,    119.71,         0,      3.6823
simulation,         2,      0.74,   0.69486,    0.7063,     505.0,    292.14,         1,         0.0
simulation,         3,      0.62,    0.6182,   0.61822,    388.85,    145.27,         0,       3.685
simulation,         4,      0.68,   0.66505,   0.66525,    498.86,    217.96,         1,         0.0
simulation,         5,      0.65,   0.64412,   0.64419,    493.56,    175.59,         1,         0.0
simulation,         6,      0.63,   0.62737,   0.62736,    353.61,    154.15,         1,         0.0
simulation,         7,      0.15,   0.15004,   0.15006,    119.92,    119.97,         0,      3.7302
simulation,         8,       0.3,    0.3001,   0.30011,    118.91,    118.95,         0,       3.697
simulation,         9,      0.45,   0.45005,   0.45009,    119.07,    119.12,         0,      3.6841
simulation,        10,       0.6,   0.59895,   0.59909,    209.55,    136.35,         0,      3.6839
