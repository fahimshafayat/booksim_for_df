wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
global_latency = 40;
internal_speedup = 2.5;
local_latency = 40;
perm_seed = 50;
routing_function = UGAL_G;
seed = 80;
vc_buf_size = 64;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49931,   0.49933,    226.28,    135.11,         0,      3.6017
simulation,         2,      0.74,   0.63468,   0.66612,    580.91,    395.85,         1,         0.0
simulation,         3,      0.62,   0.59428,   0.59466,    820.58,    283.75,         1,         0.0
simulation,         4,      0.56,   0.55086,   0.55092,    455.81,    186.66,         1,         0.0
simulation,         5,      0.53,   0.52682,   0.52683,     440.8,    157.61,         1,         0.0
simulation,         6,      0.51,   0.50858,   0.50861,    361.37,    144.71,         0,      3.6024
simulation,         7,      0.52,   0.51781,   0.51783,    356.16,    147.15,         1,         0.0
simulation,         8,       0.1,   0.10003,   0.10003,     115.7,    115.74,         0,      3.6454
simulation,         9,       0.2,   0.19998,   0.19998,    114.67,    114.71,         0,      3.6175
simulation,        10,       0.3,    0.3001,   0.30011,     114.5,    114.53,         0,      3.6082
simulation,        11,       0.4,       0.4,   0.40002,    114.64,    114.68,         0,      3.6036
simulation,        12,       0.5,   0.49931,   0.49933,    226.28,    135.11,         0,      3.6017
