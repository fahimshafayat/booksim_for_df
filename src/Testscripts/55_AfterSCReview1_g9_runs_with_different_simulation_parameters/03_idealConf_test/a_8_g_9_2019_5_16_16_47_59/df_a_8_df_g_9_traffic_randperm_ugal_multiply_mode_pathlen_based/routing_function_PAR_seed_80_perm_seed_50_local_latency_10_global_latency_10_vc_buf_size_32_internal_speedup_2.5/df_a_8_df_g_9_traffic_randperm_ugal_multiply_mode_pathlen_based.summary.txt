wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
global_latency = 10;
internal_speedup = 2.5;
local_latency = 10;
perm_seed = 50;
routing_function = PAR;
seed = 80;
vc_buf_size = 32;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50015,    40.241,    40.248,         0,      3.8321
simulation,         2,      0.74,   0.73145,   0.73145,    533.47,    94.343,         1,         0.0
simulation,         3,      0.62,   0.61989,    0.6199,    42.845,    42.852,         0,      3.8373
simulation,         4,      0.68,   0.67988,    0.6799,    48.486,    48.444,         0,      3.8753
simulation,         5,      0.71,   0.70888,   0.70891,     171.1,    60.698,         0,      3.9145
simulation,         6,      0.72,   0.71753,   0.71754,    336.33,    67.212,         0,      3.9327
simulation,         7,      0.73,   0.72556,   0.72556,    390.96,    78.291,         1,         0.0
simulation,         8,      0.15,   0.15006,   0.15006,    41.718,    41.728,         0,       4.083
simulation,         9,       0.3,   0.30012,   0.30011,    40.013,    40.021,         0,      3.9092
simulation,        10,      0.45,   0.45009,   0.45009,    39.939,    39.946,         0,      3.8428
simulation,        11,       0.6,   0.59992,   0.59993,    42.088,    42.095,         0,       3.833
