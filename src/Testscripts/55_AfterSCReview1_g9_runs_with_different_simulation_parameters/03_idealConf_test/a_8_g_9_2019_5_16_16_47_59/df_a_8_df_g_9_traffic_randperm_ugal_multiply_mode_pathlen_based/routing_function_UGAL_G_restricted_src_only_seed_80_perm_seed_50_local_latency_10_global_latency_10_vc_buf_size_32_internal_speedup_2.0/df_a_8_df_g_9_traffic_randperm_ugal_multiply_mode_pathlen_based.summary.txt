wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
global_latency = 10;
internal_speedup = 2.0;
local_latency = 10;
perm_seed = 50;
routing_function = UGAL_G_restricted_src_only;
seed = 80;
vc_buf_size = 32;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50015,    38.351,    38.354,         0,      3.6464
simulation,         2,      0.74,   0.73785,   0.73788,    258.22,    55.195,         0,      3.6453
simulation,         3,      0.86,    0.7945,   0.80043,    573.94,    138.62,         1,         0.0
simulation,         4,       0.8,   0.77641,   0.77633,    565.63,    91.208,         1,         0.0
simulation,         5,      0.77,   0.75852,   0.75853,    543.29,    73.233,         1,         0.0
simulation,         6,      0.75,   0.74467,    0.7447,    388.89,    63.693,         1,         0.0
simulation,         7,      0.15,   0.15006,   0.15006,    37.955,    37.959,         0,      3.7244
simulation,         8,       0.3,   0.30012,   0.30011,    37.728,    37.732,         0,      3.6747
simulation,         9,      0.45,   0.45009,   0.45009,    38.089,    38.093,         0,      3.6519
simulation,        10,       0.6,   0.59993,   0.59993,    39.285,    39.289,         0,      3.6395
