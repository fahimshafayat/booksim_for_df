wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
internal_speedup = 4.0;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
routing_function = UGAL_G_restricted_src_only;
seed = 80;
shift_by_group = 1;
vc_buf_size = 32;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50015,    18.973,    18.973,         0,      4.6355
simulation,         2,      0.74,   0.52141,   0.54483,    1284.3,     391.0,         1,         0.0
simulation,         3,      0.62,   0.53805,   0.55474,    539.02,     255.6,         1,         0.0
simulation,         4,      0.56,   0.54911,   0.54927,    483.98,    120.55,         1,         0.0
simulation,         5,      0.53,   0.53009,   0.53011,    21.796,    21.796,         0,      4.6569
simulation,         6,      0.54,   0.53999,   0.54002,    23.573,    23.573,         0,       4.665
simulation,         7,      0.55,   0.54996,   0.55001,     26.47,    26.469,         0,      4.6722
simulation,         8,       0.1,   0.10003,   0.10003,     11.22,     11.22,         0,      4.3986
simulation,         9,       0.2,   0.19998,   0.19998,    12.023,    12.023,         0,      4.4387
simulation,        10,       0.3,   0.30011,   0.30011,    13.097,    13.097,         0,      4.4743
simulation,        11,       0.4,   0.40003,   0.40002,    15.058,    15.058,         0,       4.555
simulation,        12,       0.5,   0.50014,   0.50015,    18.973,    18.973,         0,      4.6355
