wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
internal_speedup = 4.0;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
routing_function = UGAL_L;
seed = 80;
shift_by_group = 1;
vc_buf_size = 64;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35023,   0.40542,    1346.3,    1048.5,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24998,    52.097,    52.135,         0,      5.1234
simulation,         3,      0.37,   0.35285,    0.3546,    1108.2,    787.89,         1,         0.0
simulation,         4,      0.31,   0.31017,   0.31018,    62.436,    62.524,         0,      5.3766
simulation,         5,      0.34,    0.3388,   0.33913,    367.83,     236.6,         0,      5.4648
simulation,         6,      0.35,   0.34487,   0.34784,    551.13,    520.38,         1,         0.0
simulation,         7,       0.1,   0.10003,   0.10003,    12.008,    12.008,         0,      4.7253
simulation,         8,       0.2,   0.19997,   0.19998,    18.772,    18.773,         0,       4.829
simulation,         9,       0.3,    0.3001,   0.30011,    60.015,    60.094,         0,      5.3404
