wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
internal_speedup = 4.0;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
perm_seed = 50;
routing_function = UGAL_G_restricted_src_only;
seed = 80;
vc_buf_size = 8;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50015,   0.50015,    11.056,    11.056,         0,      3.7443
simulation,         2,      0.74,   0.73995,   0.73994,    32.002,    14.312,         0,      3.7088
simulation,         3,      0.86,   0.79295,   0.79454,    579.18,    37.914,         1,         0.0
simulation,         4,       0.8,   0.78451,   0.78445,    502.18,    25.416,         1,         0.0
simulation,         5,      0.77,   0.76734,   0.76734,     300.5,    16.835,         0,      3.7084
simulation,         6,      0.78,   0.77514,   0.77513,    335.53,    18.637,         1,         0.0
simulation,         7,      0.15,   0.15005,   0.15006,     9.972,    9.9722,         0,      3.8377
simulation,         8,       0.3,   0.30011,   0.30011,    10.292,    10.292,         0,       3.792
simulation,         9,      0.45,   0.45009,   0.45009,    10.816,    10.816,         0,      3.7554
simulation,        10,       0.6,   0.59993,   0.59993,    11.707,    11.707,         0,      3.7259
simulation,        11,      0.75,   0.74923,   0.74923,    96.785,    14.724,         0,      3.7088
