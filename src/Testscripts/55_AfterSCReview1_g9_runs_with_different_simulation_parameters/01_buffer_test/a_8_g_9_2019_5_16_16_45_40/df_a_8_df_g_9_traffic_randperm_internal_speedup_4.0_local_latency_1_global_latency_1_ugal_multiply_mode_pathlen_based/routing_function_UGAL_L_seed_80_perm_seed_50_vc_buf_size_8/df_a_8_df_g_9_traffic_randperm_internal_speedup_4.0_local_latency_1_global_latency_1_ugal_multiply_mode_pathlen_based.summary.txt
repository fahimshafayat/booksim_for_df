wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
internal_speedup = 4.0;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
perm_seed = 50;
routing_function = UGAL_L;
seed = 80;
vc_buf_size = 8;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50015,   0.50015,    11.923,    11.923,         0,      3.9859
simulation,         2,      0.74,   0.70282,   0.70296,    976.73,    41.754,         1,         0.0
simulation,         3,      0.62,   0.61991,    0.6199,    13.762,    13.753,         0,      3.9591
simulation,         4,      0.68,    0.6774,   0.67741,    325.61,     19.41,         0,      3.9647
simulation,         5,      0.71,   0.69642,   0.69641,    524.88,    27.306,         1,         0.0
simulation,         6,      0.69,     0.685,   0.68499,    427.36,    21.584,         1,         0.0
simulation,         7,      0.15,   0.15006,   0.15006,    10.753,    10.753,         0,      4.1948
simulation,         8,       0.3,   0.30011,   0.30011,     11.04,     11.04,         0,      4.0918
simulation,         9,      0.45,    0.4501,   0.45009,    11.604,    11.604,         0,      4.0073
simulation,        10,       0.6,   0.59992,   0.59993,    13.141,    13.142,         0,      3.9606
