wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
internal_speedup = 4.0;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
perm_seed = 50;
routing_function = PAR_restricted_src_only;
seed = 80;
vc_buf_size = 32;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50015,   0.50015,    12.146,    12.146,         0,      4.0301
simulation,         2,      0.74,   0.74012,   0.74012,    17.463,    17.464,         0,      4.0188
simulation,         3,      0.86,   0.79756,   0.80569,    592.88,    177.12,         1,         0.0
simulation,         4,       0.8,   0.79099,    0.7911,    540.99,     106.0,         1,         0.0
simulation,         5,      0.77,   0.76998,      0.77,    22.375,    22.379,         0,      4.0597
simulation,         6,      0.78,   0.77993,   0.77995,    25.461,    25.465,         0,      4.0861
simulation,         7,      0.79,     0.787,   0.78714,    233.29,    69.083,         0,      4.2293
simulation,         8,      0.15,   0.15005,   0.15006,    10.883,    10.883,         0,      4.2397
simulation,         9,       0.3,   0.30011,   0.30011,    11.231,    11.231,         0,      4.1396
simulation,        10,      0.45,    0.4501,   0.45009,    11.839,    11.839,         0,      4.0533
simulation,        11,       0.6,   0.59992,   0.59993,    13.112,    13.112,         0,      3.9988
simulation,        12,      0.75,   0.75008,   0.75009,    18.416,    18.416,         0,      4.0283
