wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
internal_speedup = 4.0;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
perm_seed = 50;
routing_function = UGAL_L;
seed = 80;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50015,   0.50015,    11.926,    11.927,         0,       3.987
simulation,         2,      0.74,   0.73532,   0.73532,    359.63,    30.156,         1,         0.0
simulation,         3,      0.62,   0.61991,    0.6199,    13.656,    13.657,         0,      3.9612
simulation,         4,      0.68,   0.67997,   0.67997,    23.078,    16.988,         0,      3.9727
simulation,         5,      0.71,   0.70917,   0.70918,    121.89,    20.369,         0,      3.9886
simulation,         6,      0.72,   0.71858,   0.71858,    191.99,    22.699,         0,      3.9978
simulation,         7,      0.73,   0.72705,   0.72703,    367.89,    26.826,         0,      4.0083
simulation,         8,      0.15,   0.15006,   0.15006,    10.757,    10.758,         0,      4.1969
simulation,         9,       0.3,   0.30011,   0.30011,    11.034,    11.035,         0,      4.0902
simulation,        10,      0.45,   0.45009,   0.45009,    11.604,    11.604,         0,      4.0076
simulation,        11,       0.6,   0.59993,   0.59993,    13.194,    13.195,         0,      3.9625
