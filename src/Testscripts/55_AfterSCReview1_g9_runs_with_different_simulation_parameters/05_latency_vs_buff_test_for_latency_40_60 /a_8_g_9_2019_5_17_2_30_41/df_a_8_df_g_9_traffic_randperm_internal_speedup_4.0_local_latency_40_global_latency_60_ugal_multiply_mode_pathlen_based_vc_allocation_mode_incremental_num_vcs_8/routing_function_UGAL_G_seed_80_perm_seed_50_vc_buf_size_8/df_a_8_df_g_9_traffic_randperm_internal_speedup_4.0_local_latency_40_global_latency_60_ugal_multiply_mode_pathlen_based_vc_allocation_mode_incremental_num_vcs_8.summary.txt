wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 60;
internal_speedup = 4.0;
local_latency = 40;
num_vcs = 8;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
perm_seed = 50;
routing_function = UGAL_G;
seed = 80;
vc_buf_size = 8;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,  0.068092,  0.075559,    3811.5,    998.98,         1,         0.0
simulation,         2,      0.25,   0.06433,  0.071822,    3297.3,    1043.5,         1,         0.0
simulation,         3,      0.13,  0.062632,  0.069913,    2128.1,    986.46,         1,         0.0
simulation,         4,      0.07,  0.058277,  0.062868,    685.63,    551.81,         1,         0.0
simulation,         5,      0.04,  0.039927,  0.039927,    137.08,    137.15,         0,      3.6995
simulation,         6,      0.05,  0.049304,  0.049286,    492.21,    228.32,         1,         0.0
simulation,         7,      0.02,   0.01992,   0.01992,     138.3,     138.4,         0,      3.7962
simulation,         8,      0.04,  0.039927,  0.039927,    137.08,    137.15,         0,      3.6995
