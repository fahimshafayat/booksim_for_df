alloc_iters = 1;
sw_alloc_delay = 1;
sim_count = 1;
wait_for_tail_credit = 0;
packet_size = 1;
routing_delay = 0;
priority = none;
st_final_delay = 1;
sample_period = 10000;
vc_alloc_delay = 1;
sw_allocator = separable_input_first;
injection_rate_uses_flits = 1;
credit_delay = 2;
warmup_periods = 3;
input_speedup = 1;
vc_allocator = separable_input_first;
output_speedup = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 60;
internal_speedup = 4.0;
local_latency = 40;
num_vcs = 8;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
routing_function = UGAL_G_restricted_src_only;
seed = 80;
shift_by_group = 1;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50008,   0.50015,    225.73,    225.75,         0,      4.5467
simulation,         2,      0.74,   0.51109,   0.67242,    1239.3,    1237.2,         1,         0.0
simulation,         3,      0.62,   0.53271,    0.6183,    671.38,    671.38,         1,         0.0
simulation,         4,      0.56,   0.55874,      0.56,    327.14,    330.27,         0,      4.6096
simulation,         5,      0.59,   0.53876,   0.59003,    483.48,    483.48,         1,         0.0
simulation,         6,      0.57,   0.54204,   0.55662,    953.44,    941.81,         1,         0.0
simulation,         7,       0.1,   0.10001,   0.10003,    177.34,     177.4,         0,      4.4383
simulation,         8,       0.2,   0.19996,   0.19998,    181.44,    181.49,         0,      4.4366
simulation,         9,       0.3,   0.30007,   0.30011,    185.97,    186.02,         0,      4.4298
simulation,        10,       0.4,   0.39998,   0.40002,    202.59,    202.61,         0,      4.4815
simulation,        11,       0.5,   0.50008,   0.50015,    225.73,    225.75,         0,      4.5467
