wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 60;
internal_speedup = 4.0;
local_latency = 40;
num_vcs = 8;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
routing_function = UGAL_L;
seed = 80;
shift_by_group = 1;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,  0.037706,  0.052147,    4780.6,    2908.2,         1,         0.0
simulation,         2,      0.25,  0.040404,  0.054878,    4096.9,    2637.8,         1,         0.0
simulation,         3,      0.13,  0.041319,  0.055848,    3201.2,    2407.5,         1,         0.0
simulation,         4,      0.07,  0.042614,  0.057005,    1748.0,    1678.4,         1,         0.0
simulation,         5,      0.04,   0.03732,  0.040103,    483.97,    483.97,         1,         0.0
simulation,         6,      0.02,  0.019918,   0.01992,    185.95,    186.07,         0,      4.7267
simulation,         7,      0.03,  0.029943,  0.029929,    299.82,    300.34,         0,      4.8751
simulation,         8,      0.02,  0.019918,   0.01992,    185.95,    186.07,         0,      4.7267
