wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 60;
internal_speedup = 4.0;
local_latency = 40;
num_vcs = 8;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
routing_function = UGAL_G_restricted_src_only;
seed = 80;
shift_by_group = 1;
vc_buf_size = 32;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.10981,   0.13831,    3794.6,    2070.8,         1,         0.0
simulation,         2,      0.25,   0.11991,   0.14809,    2217.9,    1654.8,         1,         0.0
simulation,         3,      0.13,   0.11346,   0.12584,    569.08,    564.21,         1,         0.0
simulation,         4,      0.07,  0.069879,  0.069877,    177.53,    177.58,         0,      4.4167
simulation,         5,       0.1,   0.10002,   0.10003,    186.15,    186.22,         0,      4.3814
simulation,         6,      0.11,   0.10902,    0.1092,    507.74,    402.33,         1,         0.0
simulation,         7,      0.02,  0.019918,   0.01992,    176.09,    176.16,         0,      4.4265
simulation,         8,      0.04,  0.039923,  0.039927,    176.45,     176.5,         0,      4.4289
simulation,         9,      0.06,  0.059904,  0.059903,    177.08,    177.12,         0,      4.4271
simulation,        10,      0.08,  0.079882,  0.079867,    177.89,    177.96,         0,      4.4035
simulation,        11,       0.1,   0.10002,   0.10003,    186.15,    186.22,         0,      4.3814
