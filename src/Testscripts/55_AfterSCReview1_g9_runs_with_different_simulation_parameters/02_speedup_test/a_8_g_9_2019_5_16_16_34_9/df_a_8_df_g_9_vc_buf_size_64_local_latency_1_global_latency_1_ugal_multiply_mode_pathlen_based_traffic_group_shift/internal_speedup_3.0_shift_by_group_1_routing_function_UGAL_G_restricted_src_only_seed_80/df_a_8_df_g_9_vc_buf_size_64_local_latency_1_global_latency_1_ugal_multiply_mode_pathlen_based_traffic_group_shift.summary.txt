packet_size = 1;
output_speedup = 1;
alloc_iters = 1;
num_vcs = 8;
vc_alloc_delay = 1;
sim_count = 1;
injection_rate_uses_flits = 1;
routing_delay = 0;
input_speedup = 1;
vc_allocator = separable_input_first;
priority = none;
warmup_periods = 3;
wait_for_tail_credit = 0;
sw_allocator = separable_input_first;
credit_delay = 2;
st_final_delay = 1;
sw_alloc_delay = 1;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
internal_speedup = 3.0;
routing_function = UGAL_G_restricted_src_only;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50015,    19.188,    19.189,         0,      4.6348
simulation,         2,      0.74,   0.53264,    0.5783,    1192.7,    671.04,         1,         0.0
simulation,         3,      0.62,   0.55051,   0.57948,    459.34,    374.54,         1,         0.0
simulation,         4,      0.56,   0.55988,      0.56,    35.741,    35.741,         0,      4.6814
simulation,         5,      0.59,    0.5547,   0.56597,    512.59,    331.41,         1,         0.0
simulation,         6,      0.57,    0.5575,   0.55846,    599.49,    241.61,         1,         0.0
simulation,         7,       0.1,   0.10003,   0.10003,    11.219,    11.219,         0,      4.3948
simulation,         8,       0.2,   0.19998,   0.19998,    12.055,    12.055,         0,       4.435
simulation,         9,       0.3,   0.30011,   0.30011,    13.201,    13.201,         0,       4.475
simulation,        10,       0.4,   0.40003,   0.40002,    15.259,    15.259,         0,      4.5528
simulation,        11,       0.5,   0.50014,   0.50015,    19.188,    19.189,         0,      4.6348
