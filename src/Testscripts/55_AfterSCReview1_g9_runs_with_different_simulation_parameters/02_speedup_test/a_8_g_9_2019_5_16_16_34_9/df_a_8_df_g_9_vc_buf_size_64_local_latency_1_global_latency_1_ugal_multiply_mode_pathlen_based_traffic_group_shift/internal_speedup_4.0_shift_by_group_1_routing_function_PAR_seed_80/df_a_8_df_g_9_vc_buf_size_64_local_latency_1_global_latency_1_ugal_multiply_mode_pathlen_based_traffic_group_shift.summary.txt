packet_size = 1;
output_speedup = 1;
alloc_iters = 1;
num_vcs = 8;
vc_alloc_delay = 1;
sim_count = 1;
injection_rate_uses_flits = 1;
routing_delay = 0;
input_speedup = 1;
vc_allocator = separable_input_first;
priority = none;
warmup_periods = 3;
wait_for_tail_credit = 0;
sw_allocator = separable_input_first;
credit_delay = 2;
st_final_delay = 1;
sw_alloc_delay = 1;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
internal_speedup = 4.0;
routing_function = PAR;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46897,   0.47984,    549.49,    356.99,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24998,    15.062,    15.063,         0,      5.3969
simulation,         3,      0.37,   0.37017,   0.37017,    21.781,    21.781,         0,      5.7887
simulation,         4,      0.43,      0.43,   0.43003,    32.252,    32.252,         0,      5.9435
simulation,         5,      0.46,   0.45767,   0.45768,    283.86,    92.256,         1,         0.0
simulation,         6,      0.44,   0.44004,   0.44008,    39.018,    39.033,         0,      5.9608
simulation,         7,      0.45,   0.44937,   0.44949,    228.23,    82.893,         0,      5.9685
simulation,         8,       0.1,   0.10003,   0.10003,    13.395,    13.395,         0,      5.4318
simulation,         9,       0.2,   0.19998,   0.19998,    14.147,    14.148,         0,      5.3604
simulation,        10,       0.3,   0.30012,   0.30011,    16.975,    16.975,         0,      5.5234
simulation,        11,       0.4,   0.40002,   0.40002,    25.104,    25.104,         0,      5.8763
