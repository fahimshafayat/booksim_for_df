packet_size = 1;
output_speedup = 1;
alloc_iters = 1;
num_vcs = 8;
vc_alloc_delay = 1;
sim_count = 1;
injection_rate_uses_flits = 1;
routing_delay = 0;
input_speedup = 1;
vc_allocator = separable_input_first;
priority = none;
warmup_periods = 3;
wait_for_tail_credit = 0;
sw_allocator = separable_input_first;
credit_delay = 2;
st_final_delay = 1;
sw_alloc_delay = 1;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
internal_speedup = 2.0;
routing_function = UGAL_L;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.3495,   0.40472,    1375.3,    1068.1,         1,         0.0
simulation,         2,      0.25,   0.24997,   0.24998,     63.12,    63.173,         0,      5.2008
simulation,         3,      0.37,   0.33936,     0.354,    755.65,    607.65,         1,         0.0
simulation,         4,      0.31,   0.30968,   0.31007,    192.34,    184.62,         0,       5.452
simulation,         5,      0.34,   0.32611,   0.32772,    1013.8,     839.4,         1,         0.0
simulation,         6,      0.32,   0.31606,   0.31895,    511.97,    499.26,         1,         0.0
simulation,         7,       0.1,   0.10003,   0.10003,    16.755,    16.755,         0,      4.7264
simulation,         8,       0.2,   0.19997,   0.19998,    22.631,     22.63,         0,      4.8293
simulation,         9,       0.3,   0.30008,   0.30011,    79.479,    79.653,         0,      5.4204
