wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
internal_speedup = 3.0;
perm_seed = 50;
routing_function = UGAL_L_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50015,   0.50015,    11.523,    11.523,         0,      3.8576
simulation,         2,      0.74,   0.74009,   0.74012,    21.173,    21.183,         0,      3.8442
simulation,         3,      0.86,   0.83148,   0.83197,    673.71,    179.52,         1,         0.0
simulation,         4,       0.8,   0.79925,   0.79924,    104.31,    44.851,         0,       3.882
simulation,         5,      0.83,   0.82023,   0.82019,     527.7,    117.16,         1,         0.0
simulation,         6,      0.81,   0.80782,   0.80781,    228.96,    59.538,         0,      3.8914
simulation,         7,      0.82,   0.81533,   0.81541,    471.34,     82.76,         0,      3.9015
simulation,         8,      0.15,   0.15005,   0.15006,    10.184,    10.184,         0,      3.9329
simulation,         9,       0.3,   0.30011,   0.30011,    10.625,    10.625,         0,      3.9152
simulation,        10,      0.45,   0.45009,   0.45009,    11.229,    11.229,         0,      3.8701
simulation,        11,       0.6,   0.59992,   0.59993,    12.482,    12.482,         0,      3.8404
simulation,        12,      0.75,   0.75007,   0.75009,    22.456,    22.465,         0,      3.8479
