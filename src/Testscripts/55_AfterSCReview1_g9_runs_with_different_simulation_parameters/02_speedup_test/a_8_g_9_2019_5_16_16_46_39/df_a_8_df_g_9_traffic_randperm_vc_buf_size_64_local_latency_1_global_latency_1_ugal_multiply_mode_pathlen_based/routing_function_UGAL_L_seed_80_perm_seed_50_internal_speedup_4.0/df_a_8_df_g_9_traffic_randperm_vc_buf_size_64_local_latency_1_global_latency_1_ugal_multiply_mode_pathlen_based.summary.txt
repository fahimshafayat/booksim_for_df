wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
internal_speedup = 4.0;
perm_seed = 50;
routing_function = UGAL_L;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50015,   0.50015,    11.936,    11.936,         0,      3.9867
simulation,         2,      0.74,   0.73923,   0.73926,    114.43,    34.256,         0,      4.0395
simulation,         3,      0.86,   0.77946,   0.79509,    743.58,    323.38,         1,         0.0
simulation,         4,       0.8,   0.77457,   0.77406,    647.49,     196.7,         1,         0.0
simulation,         5,      0.77,   0.76452,   0.76447,     417.9,    83.799,         1,         0.0
simulation,         6,      0.75,   0.74888,   0.74892,    165.46,    37.849,         0,      4.0588
simulation,         7,      0.76,   0.75727,   0.75724,     345.4,    59.756,         0,      4.0852
simulation,         8,      0.15,   0.15006,   0.15006,    10.757,    10.758,         0,      4.1969
simulation,         9,       0.3,   0.30011,   0.30011,    11.034,    11.035,         0,      4.0902
simulation,        10,      0.45,    0.4501,   0.45009,    11.607,    11.607,         0,      4.0074
simulation,        11,       0.6,   0.59993,   0.59993,     13.96,    13.963,         0,      3.9624
simulation,        12,      0.75,   0.74888,   0.74892,    165.46,    37.849,         0,      4.0588
