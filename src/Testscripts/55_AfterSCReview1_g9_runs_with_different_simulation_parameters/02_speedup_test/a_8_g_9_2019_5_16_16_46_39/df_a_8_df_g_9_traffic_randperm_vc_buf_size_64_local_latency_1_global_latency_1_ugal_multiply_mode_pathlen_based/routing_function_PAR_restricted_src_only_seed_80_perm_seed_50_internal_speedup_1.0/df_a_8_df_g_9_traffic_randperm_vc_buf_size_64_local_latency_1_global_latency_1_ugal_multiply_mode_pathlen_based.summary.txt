wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
internal_speedup = 1.0;
perm_seed = 50;
routing_function = PAR_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4954,   0.49537,    523.94,     156.8,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24998,    20.545,    20.546,         0,      4.1069
simulation,         3,      0.37,   0.37019,   0.37017,    23.512,    23.513,         0,      4.0056
simulation,         4,      0.43,   0.43003,   0.43003,    28.581,    28.584,         0,      3.9825
simulation,         5,      0.46,   0.46013,   0.46016,    35.102,    35.125,         0,      3.9942
simulation,         6,      0.48,   0.48017,    0.4802,    50.346,    50.653,         0,      4.0266
simulation,         7,      0.49,   0.48912,   0.48918,    161.75,    88.254,         0,      4.0628
simulation,         8,       0.1,   0.10003,   0.10003,    19.531,    19.532,         0,      4.2477
simulation,         9,       0.2,   0.19999,   0.19998,    20.065,    20.066,         0,      4.1567
simulation,        10,       0.3,   0.30012,   0.30011,    21.318,    21.319,         0,      4.0594
simulation,        11,       0.4,   0.40003,   0.40002,    25.378,     25.38,         0,      3.9898
