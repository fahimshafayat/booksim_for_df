wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
internal_speedup = 3.0;
perm_seed = 50;
routing_function = UGAL_G;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50015,    11.041,    11.041,         0,      3.7377
simulation,         2,      0.74,   0.74011,   0.74012,    14.719,    14.719,         0,      3.7398
simulation,         3,      0.86,   0.86003,   0.86005,    27.645,    27.648,         0,      3.8438
simulation,         4,      0.92,   0.88772,   0.88812,    676.37,    204.09,         1,         0.0
simulation,         5,      0.89,   0.89003,   0.89009,    50.229,    50.241,         0,      3.8991
simulation,         6,       0.9,   0.89433,   0.89436,    369.38,    111.29,         1,         0.0
simulation,         7,      0.15,   0.15005,   0.15006,    10.156,    10.156,         0,      3.9278
simulation,         8,       0.3,   0.30011,   0.30011,    10.325,    10.326,         0,      3.8155
simulation,         9,      0.45,    0.4501,   0.45009,    10.787,    10.787,         0,      3.7517
simulation,        10,       0.6,   0.59993,   0.59993,     11.83,     11.83,         0,       3.722
simulation,        11,      0.75,   0.75008,   0.75009,    15.133,    15.134,         0,      3.7444
