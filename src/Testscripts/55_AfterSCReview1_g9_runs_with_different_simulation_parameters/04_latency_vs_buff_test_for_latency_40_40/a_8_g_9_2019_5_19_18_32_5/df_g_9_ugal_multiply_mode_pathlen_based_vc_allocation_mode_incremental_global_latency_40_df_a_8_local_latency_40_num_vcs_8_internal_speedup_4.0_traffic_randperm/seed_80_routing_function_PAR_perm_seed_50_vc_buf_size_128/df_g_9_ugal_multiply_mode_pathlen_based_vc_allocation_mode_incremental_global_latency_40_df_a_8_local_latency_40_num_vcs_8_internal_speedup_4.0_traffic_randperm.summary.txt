credit_delay = 2;
routing_delay = 0;
packet_size = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
input_speedup = 1;
priority = none;
wait_for_tail_credit = 0;
alloc_iters = 1;
output_speedup = 1;
injection_rate_uses_flits = 1;
sw_alloc_delay = 1;
sample_period = 10000;
st_final_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 40;
internal_speedup = 4.0;
local_latency = 40;
num_vcs = 8;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
perm_seed = 50;
routing_function = PAR;
seed = 80;
vc_buf_size = 128;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50012,   0.50015,    118.31,    118.37,         0,      3.7404
simulation,         2,      0.74,   0.73939,   0.73942,    211.12,    178.26,         0,      3.9061
simulation,         3,      0.86,   0.76572,   0.82957,    512.39,    493.41,         1,         0.0
simulation,         4,       0.8,   0.76883,   0.77104,     864.1,    519.62,         1,         0.0
simulation,         5,      0.77,   0.76223,   0.76229,    507.56,    282.46,         1,         0.0
simulation,         6,      0.75,   0.74864,   0.74866,     299.8,    205.13,         0,      3.9375
simulation,         7,      0.76,   0.75544,   0.75555,    473.61,    242.55,         1,         0.0
simulation,         8,      0.15,   0.15004,   0.15006,    120.01,    120.09,         0,      3.8231
simulation,         9,       0.3,   0.30011,   0.30011,    117.84,    117.91,         0,      3.7608
simulation,        10,      0.45,   0.45007,   0.45009,    117.81,    117.87,         0,       3.742
simulation,        11,       0.6,   0.59989,   0.59993,    122.25,    122.32,         0,      3.7519
simulation,        12,      0.75,   0.74864,   0.74866,     299.8,    205.13,         0,      3.9375
