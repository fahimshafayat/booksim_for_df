credit_delay = 2;
routing_delay = 0;
packet_size = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
input_speedup = 1;
priority = none;
wait_for_tail_credit = 0;
alloc_iters = 1;
output_speedup = 1;
injection_rate_uses_flits = 1;
sw_alloc_delay = 1;
sample_period = 10000;
st_final_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 40;
internal_speedup = 4.0;
local_latency = 40;
num_vcs = 8;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
perm_seed = 50;
routing_function = PAR_restricted_src_only;
seed = 80;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50011,   0.50015,    117.06,    117.11,         0,      3.7179
simulation,         2,      0.74,   0.74006,   0.74012,    132.13,     132.2,         0,      3.7598
simulation,         3,      0.86,   0.81503,   0.85642,    513.27,    512.85,         1,         0.0
simulation,         4,       0.8,   0.79998,   0.79997,     162.5,    162.66,         0,      3.8371
simulation,         5,      0.83,   0.82966,   0.83009,    242.68,    243.51,         0,       3.972
simulation,         6,      0.84,   0.81412,    0.8372,    573.12,     572.7,         1,         0.0
simulation,         7,      0.15,   0.15005,   0.15006,    118.91,    118.96,         0,      3.7961
simulation,         8,       0.3,    0.3001,   0.30011,    117.02,    117.07,         0,       3.741
simulation,         9,      0.45,   0.45007,   0.45009,    116.83,    116.88,         0,      3.7208
simulation,        10,       0.6,    0.5999,   0.59993,    118.51,    118.55,         0,      3.7161
simulation,        11,      0.75,   0.75005,   0.75009,    134.73,    134.81,         0,      3.7684
