credit_delay = 2;
routing_delay = 0;
packet_size = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
input_speedup = 1;
priority = none;
wait_for_tail_credit = 0;
alloc_iters = 1;
output_speedup = 1;
injection_rate_uses_flits = 1;
sw_alloc_delay = 1;
sample_period = 10000;
st_final_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 40;
internal_speedup = 4.0;
local_latency = 40;
num_vcs = 8;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
perm_seed = 50;
routing_function = UGAL_L_restricted_src_only;
seed = 80;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50012,   0.50015,    115.48,    115.53,         0,      3.6827
simulation,         2,      0.74,   0.74006,   0.74012,    144.93,    145.13,         0,      3.7005
simulation,         3,      0.86,   0.84191,   0.84749,    559.56,     505.8,         1,         0.0
simulation,         4,       0.8,   0.79999,   0.79997,     184.7,    185.21,         0,      3.7476
simulation,         5,      0.83,   0.82652,   0.82689,    398.22,    303.38,         1,         0.0
simulation,         6,      0.81,   0.80968,   0.80999,    206.34,    207.79,         0,      3.7613
simulation,         7,      0.82,   0.81827,   0.81863,    322.34,    268.28,         0,      3.7771
simulation,         8,      0.15,   0.15005,   0.15006,    116.25,     116.3,         0,      3.7317
simulation,         9,       0.3,   0.30011,   0.30011,    115.25,    115.29,         0,      3.6986
simulation,        10,      0.45,   0.45007,   0.45009,     115.3,    115.34,         0,      3.6856
simulation,        11,       0.6,    0.5999,   0.59993,    116.72,    116.76,         0,      3.6797
simulation,        12,      0.75,   0.75001,   0.75009,    149.49,     149.7,         0,      3.7055
