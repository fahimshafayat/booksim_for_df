input_speedup = 1;
sw_allocator = separable_input_first;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
wait_for_tail_credit = 0;
packet_size = 1;
warmup_periods = 3;
st_final_delay = 1;
vc_allocator = separable_input_first;
priority = none;
sim_count = 1;
injection_rate_uses_flits = 1;
output_speedup = 1;
alloc_iters = 1;
sample_period = 10000;
credit_delay = 2;
routing_delay = 0;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 40;
internal_speedup = 4.0;
local_latency = 40;
num_vcs = 8;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
routing_function = UGAL_L_restricted_src_only;
seed = 80;
shift_by_group = 1;
vc_buf_size = 32;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.17255,   0.20162,    3159.6,    1424.8,         1,         0.0
simulation,         2,      0.25,   0.17565,   0.19982,    1245.1,    960.22,         1,         0.0
simulation,         3,      0.13,   0.13004,   0.13007,    224.09,    224.36,         0,      4.4365
simulation,         4,      0.19,   0.16364,   0.17801,    628.08,     576.1,         1,         0.0
simulation,         5,      0.16,   0.15023,   0.15547,    604.81,    518.25,         1,         0.0
simulation,         6,      0.14,   0.13869,   0.13908,    485.92,    424.64,         1,         0.0
simulation,         7,      0.05,   0.04989,  0.049886,    140.59,    140.63,         0,      4.3277
simulation,         8,       0.1,   0.10001,   0.10003,    172.68,    172.71,         0,      4.3758
