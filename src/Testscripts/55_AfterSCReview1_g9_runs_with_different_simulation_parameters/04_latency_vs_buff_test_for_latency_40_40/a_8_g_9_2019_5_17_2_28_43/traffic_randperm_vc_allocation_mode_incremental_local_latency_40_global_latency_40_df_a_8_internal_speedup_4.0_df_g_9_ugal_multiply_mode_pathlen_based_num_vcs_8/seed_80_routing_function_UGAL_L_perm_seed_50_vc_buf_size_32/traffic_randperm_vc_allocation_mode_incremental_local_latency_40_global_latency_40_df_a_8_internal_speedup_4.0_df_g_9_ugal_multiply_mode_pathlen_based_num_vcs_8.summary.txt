sw_allocator = separable_input_first;
routing_delay = 0;
wait_for_tail_credit = 0;
injection_rate_uses_flits = 1;
output_speedup = 1;
vc_allocator = separable_input_first;
sw_alloc_delay = 1;
packet_size = 1;
sim_count = 1;
alloc_iters = 1;
credit_delay = 2;
sample_period = 10000;
warmup_periods = 3;
st_final_delay = 1;
priority = none;
vc_alloc_delay = 1;
input_speedup = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 40;
internal_speedup = 4.0;
local_latency = 40;
num_vcs = 8;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
perm_seed = 50;
routing_function = UGAL_L;
seed = 80;
vc_buf_size = 32;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.36196,   0.38682,    1115.9,    573.27,         1,         0.0
simulation,         2,      0.25,   0.25002,   0.24998,     119.5,    119.71,         0,      3.7443
simulation,         3,      0.37,   0.34182,   0.34856,    635.07,    339.03,         1,         0.0
simulation,         4,      0.31,   0.30635,    0.3064,    511.39,    191.16,         1,         0.0
simulation,         5,      0.28,   0.27936,   0.27939,    339.43,    141.58,         0,      3.7434
simulation,         6,      0.29,   0.28885,   0.28886,    348.93,    150.49,         1,         0.0
simulation,         7,      0.05,  0.049888,  0.049886,    123.37,    123.46,         0,      3.9095
simulation,         8,       0.1,   0.10002,   0.10003,    119.61,    119.68,         0,      3.8157
simulation,         9,      0.15,   0.15005,   0.15006,    118.07,    118.13,         0,      3.7759
simulation,        10,       0.2,   0.19998,   0.19998,    117.39,    117.45,         0,      3.7554
simulation,        11,      0.25,   0.25002,   0.24998,     119.5,    119.71,         0,      3.7443
