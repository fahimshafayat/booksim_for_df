sw_allocator = separable_input_first;
routing_delay = 0;
wait_for_tail_credit = 0;
injection_rate_uses_flits = 1;
output_speedup = 1;
vc_allocator = separable_input_first;
sw_alloc_delay = 1;
packet_size = 1;
sim_count = 1;
alloc_iters = 1;
credit_delay = 2;
sample_period = 10000;
warmup_periods = 3;
st_final_delay = 1;
priority = none;
vc_alloc_delay = 1;
input_speedup = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 40;
internal_speedup = 4.0;
local_latency = 40;
num_vcs = 8;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
perm_seed = 50;
routing_function = UGAL_L;
seed = 80;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.18336,   0.19859,    2793.2,    760.59,         1,         0.0
simulation,         2,      0.25,   0.17776,   0.19043,    1169.2,    599.82,         1,         0.0
simulation,         3,      0.13,   0.12999,   0.13003,    131.53,    127.57,         0,      3.7915
simulation,         4,      0.19,   0.17049,   0.17829,    458.68,    350.84,         1,         0.0
simulation,         5,      0.16,   0.15722,   0.15732,    519.29,    218.19,         1,         0.0
simulation,         6,      0.14,   0.13989,   0.13987,    214.22,    141.52,         0,       3.788
simulation,         7,      0.15,   0.14898,   0.14897,    501.23,    173.52,         1,         0.0
simulation,         8,      0.05,  0.049893,  0.049886,    123.36,    123.44,         0,      3.9091
simulation,         9,       0.1,   0.10003,   0.10003,    119.89,    119.96,         0,      3.8168
