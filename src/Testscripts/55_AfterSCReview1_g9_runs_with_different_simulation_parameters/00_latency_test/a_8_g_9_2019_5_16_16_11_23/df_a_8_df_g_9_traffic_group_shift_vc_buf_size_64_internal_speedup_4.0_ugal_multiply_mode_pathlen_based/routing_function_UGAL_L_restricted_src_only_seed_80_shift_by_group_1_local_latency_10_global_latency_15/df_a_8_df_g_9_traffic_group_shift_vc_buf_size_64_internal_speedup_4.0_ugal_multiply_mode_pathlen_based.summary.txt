wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
internal_speedup = 4.0;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
global_latency = 15;
local_latency = 10;
routing_function = UGAL_L_restricted_src_only;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.45689,   0.46907,     721.4,    433.27,         1,         0.0
simulation,         2,      0.25,   0.24997,   0.24998,    81.044,     81.06,         0,      4.4258
simulation,         3,      0.37,   0.37015,   0.37017,     113.7,    113.89,         0,      4.5876
simulation,         4,      0.43,   0.42609,   0.42639,    493.12,    255.67,         1,         0.0
simulation,         5,       0.4,   0.39999,   0.40002,    121.47,    121.74,         0,      4.6108
simulation,         6,      0.41,   0.40998,   0.41002,     124.8,    125.13,         0,      4.6174
simulation,         7,      0.42,   0.41861,   0.41877,    312.29,    180.88,         0,      4.6236
simulation,         8,       0.1,   0.10003,   0.10003,    48.179,    48.182,         0,      4.3338
simulation,         9,       0.2,   0.19998,   0.19998,    52.809,     52.81,         0,      4.3146
simulation,        10,       0.3,    0.3001,   0.30011,    96.003,    96.055,         0,      4.5122
simulation,        11,       0.4,   0.39999,   0.40002,    121.47,    121.74,         0,      4.6108
