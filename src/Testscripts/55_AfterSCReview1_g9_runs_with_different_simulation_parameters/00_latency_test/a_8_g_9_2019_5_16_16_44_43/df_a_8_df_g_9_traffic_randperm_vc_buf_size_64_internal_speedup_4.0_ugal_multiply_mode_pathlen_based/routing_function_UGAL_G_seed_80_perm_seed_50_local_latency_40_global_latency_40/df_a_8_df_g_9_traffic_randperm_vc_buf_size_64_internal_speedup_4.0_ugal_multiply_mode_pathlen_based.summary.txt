wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
internal_speedup = 4.0;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
global_latency = 40;
local_latency = 40;
perm_seed = 50;
routing_function = UGAL_G;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49948,   0.49962,    164.82,    126.99,         0,      3.6026
simulation,         2,      0.74,   0.64109,   0.67245,    553.15,    386.35,         1,         0.0
simulation,         3,      0.62,   0.59866,   0.59856,     692.9,    241.06,         1,         0.0
simulation,         4,      0.56,   0.55254,   0.55264,    511.09,    185.03,         1,         0.0
simulation,         5,      0.53,   0.52746,   0.52749,    383.78,    152.55,         1,         0.0
simulation,         6,      0.51,   0.50921,   0.50926,    241.56,    137.88,         0,      3.6029
simulation,         7,      0.52,   0.51807,   0.51813,     407.5,    143.75,         0,      3.6027
simulation,         8,       0.1,   0.10002,   0.10003,    112.62,    112.67,         0,       3.646
simulation,         9,       0.2,   0.19998,   0.19998,    111.65,    111.69,         0,      3.6181
simulation,        10,       0.3,   0.30012,   0.30011,    111.52,    111.56,         0,       3.609
simulation,        11,       0.4,   0.40001,   0.40002,    111.65,    111.69,         0,      3.6037
simulation,        12,       0.5,   0.49948,   0.49962,    164.82,    126.99,         0,      3.6026
