wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
internal_speedup = 4.0;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
global_latency = 10;
local_latency = 10;
perm_seed = 50;
routing_function = PAR_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50015,    36.639,    36.642,         0,      3.8059
simulation,         2,      0.74,    0.7401,   0.74012,    43.479,    43.484,         0,      3.8269
simulation,         3,      0.86,   0.80866,   0.82266,    493.26,    278.18,         1,         0.0
simulation,         4,       0.8,   0.79995,   0.79997,    56.612,    56.626,         0,      3.9101
simulation,         5,      0.83,   0.80819,   0.80849,    488.63,    236.53,         1,         0.0
simulation,         6,      0.81,   0.80994,   0.80999,    61.696,    61.723,         0,      3.9397
simulation,         7,      0.82,   0.81318,   0.81418,    385.32,    165.09,         0,      4.0986
simulation,         8,      0.15,   0.15007,   0.15006,     37.23,    37.235,         0,       3.991
simulation,         9,       0.3,   0.30012,   0.30011,    36.359,    36.364,         0,      3.8732
simulation,        10,      0.45,   0.45009,   0.45009,    36.433,    36.438,         0,      3.8181
simulation,        11,       0.6,   0.59992,   0.59993,    37.598,    37.602,         0,      3.7924
simulation,        12,      0.75,   0.75006,   0.75009,    44.518,    44.524,         0,      3.8352
