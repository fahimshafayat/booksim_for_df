wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
internal_speedup = 4.0;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
global_latency = 40;
local_latency = 40;
perm_seed = 50;
routing_function = UGAL_L;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50008,    0.5001,    126.67,    122.81,         0,      3.7181
simulation,         2,      0.74,   0.67102,   0.68569,    772.85,     382.7,         1,         0.0
simulation,         3,      0.62,   0.61109,   0.61121,     524.7,    197.73,         1,         0.0
simulation,         4,      0.56,   0.55842,   0.55844,    381.07,    142.24,         0,      3.7204
simulation,         5,      0.59,   0.58571,   0.58574,    463.62,    162.42,         1,         0.0
simulation,         6,      0.57,   0.56783,   0.56784,     476.4,    144.29,         0,      3.7222
simulation,         7,      0.58,   0.57709,   0.57711,     380.6,    152.65,         1,         0.0
simulation,         8,       0.1,   0.10002,   0.10003,    119.61,    119.68,         0,      3.8157
simulation,         9,       0.2,   0.19998,   0.19998,    117.31,    117.38,         0,       3.755
simulation,        10,       0.3,   0.30012,   0.30011,    116.67,    116.73,         0,       3.733
simulation,        11,       0.4,   0.40002,   0.40002,     116.6,    116.66,         0,      3.7218
simulation,        12,       0.5,   0.50008,    0.5001,    126.67,    122.81,         0,      3.7181
