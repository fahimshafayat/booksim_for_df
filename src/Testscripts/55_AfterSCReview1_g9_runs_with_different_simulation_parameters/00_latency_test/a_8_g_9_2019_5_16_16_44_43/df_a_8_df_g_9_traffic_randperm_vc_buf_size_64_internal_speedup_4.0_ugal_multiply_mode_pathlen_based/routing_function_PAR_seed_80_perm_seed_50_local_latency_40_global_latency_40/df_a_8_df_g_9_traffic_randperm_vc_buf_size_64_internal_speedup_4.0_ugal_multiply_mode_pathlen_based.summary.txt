wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
internal_speedup = 4.0;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
global_latency = 40;
local_latency = 40;
perm_seed = 50;
routing_function = PAR;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50012,   0.50015,     118.3,    118.36,         0,      3.7397
simulation,         2,      0.74,   0.68445,   0.69806,    645.43,    344.56,         1,         0.0
simulation,         3,      0.62,   0.61626,   0.61631,    449.73,    171.73,         1,         0.0
simulation,         4,      0.56,   0.55983,   0.55984,    132.93,    124.26,         0,      3.7431
simulation,         5,      0.59,   0.58907,   0.58915,    230.13,    148.92,         0,      3.7504
simulation,         6,       0.6,   0.59834,   0.59834,    349.81,    152.38,         0,      3.7511
simulation,         7,      0.61,    0.6074,   0.60744,     482.6,    163.94,         0,      3.7524
simulation,         8,      0.15,   0.15004,   0.15006,    120.01,    120.09,         0,      3.8231
simulation,         9,       0.3,   0.30011,   0.30011,    117.84,    117.91,         0,      3.7608
simulation,        10,      0.45,   0.45006,   0.45009,    117.82,    117.87,         0,      3.7422
simulation,        11,       0.6,   0.59834,   0.59834,    349.81,    152.38,         0,      3.7511
