vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
df_g = 9;
routing_function = vlb;
shift_by_group = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010011,  0.010011,    64.551,     64.55,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.42699,   0.48034,    2878.5,    1195.1,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,   0.42805,   0.48077,    761.85,    737.79,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.25,   0.24987,   0.24987,    67.511,    67.512,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         5,      0.37,   0.36972,   0.36971,    75.201,      75.2,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         6,      0.43,   0.42973,   0.42981,    150.47,    150.56,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         7,      0.46,   0.42833,   0.45196,    711.97,    667.98,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.44,   0.43212,   0.43929,    574.03,    565.49,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39984,   0.39984,     83.57,    83.572,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,        10,      0.35,   0.34975,   0.34974,    72.611,    72.611,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,        11,       0.3,   0.29977,   0.29976,     69.23,     69.23,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
