vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
df_g = 13;
routing_function = vlb;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010038,   0.01004,    64.539,    64.541,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.34244,    0.3925,    3250.0,    1305.7,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,   0.34176,     0.391,    1550.1,    1099.8,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.25,   0.25004,   0.25003,    67.743,    67.744,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         5,      0.37,   0.34043,   0.36187,    783.01,    723.95,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.31,      0.31,   0.30999,    71.484,    71.483,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         7,      0.34,   0.33991,   0.34001,    103.82,    104.16,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         8,      0.35,   0.34103,   0.34868,    651.79,    639.26,         1,  0,  0,  ,  ,  ,  
simulation,        10,       0.3,   0.30005,   0.30004,    70.387,    70.387,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,        12,       0.2,   0.19995,   0.19995,    66.514,    66.515,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
