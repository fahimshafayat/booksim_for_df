routing_delay = 0;
alloc_iters = 1;
credit_delay = 2;
num_vcs = 7;
sim_count = 1;
output_speedup = 1;
vc_alloc_delay = 1;
input_speedup = 1;
sample_period = 10000;
packet_size = 1;
internal_speedup = 4.0;
sw_allocator = separable_input_first;
vc_buf_size = 64;
warmup_periods = 3;
injection_rate_uses_flits = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_alloc_delay = 1;
priority = none;
wait_for_tail_credit = 0;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 33;
routing_function = PAR;
seed = 8;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099833, 0.0099826,    44.295,    44.304,         0,  870327,  199767,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 13531 , |3-> 5043 , |4-> 57019 , |5-> 286541 , |6-> 508193 ,,  |1-> 0 , |2-> 3469 , |3-> 1194 , |4-> 13806 , |5-> 66624 , |6-> 114674 ,
simulation,         2,      0.99,   0.99006,   0.98999,     156.6,    156.68,         0,  112601008,  2188878,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 1667796 , |3-> 601919 , |4-> 7130687 , |5-> 36774072 , |6-> 66426534 ,,  |1-> 0 , |2-> 31204 , |3-> 54988 , |4-> 430747 , |5-> 1146493 , |6-> 525446 ,
