routing_delay = 0;
alloc_iters = 1;
credit_delay = 2;
num_vcs = 7;
sim_count = 1;
output_speedup = 1;
vc_alloc_delay = 1;
input_speedup = 1;
sample_period = 10000;
packet_size = 1;
internal_speedup = 4.0;
sw_allocator = separable_input_first;
vc_buf_size = 64;
warmup_periods = 3;
injection_rate_uses_flits = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_alloc_delay = 1;
priority = none;
wait_for_tail_credit = 0;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 33;
routing_function = PAR_multi_tiered;
seed = 8;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099833, 0.0099826,    38.892,    38.896,         0,  884522,  195617,  |4-> 870711 , |5-> 0 , |6-> 0 ,,  |4-> 192516 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 15657 , |3-> 79044 , |4-> 789821 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 3567 , |3-> 17657 , |4-> 174393 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.98903,   0.98994,    168.84,    169.01,         0,  113171068,  2232197,  |4-> 43260 , |5-> 26798015 , |6-> 84672580 ,,  |4-> 4269 , |5-> 20522 , |6-> 2179986 ,,  |1-> 0 , |2-> 1687732 , |3-> 848649 , |4-> 9794081 , |5-> 50093911 , |6-> 50746695 ,,  |1-> 0 , |2-> 29754 , |3-> 58024 , |4-> 458590 , |5-> 1192657 , |6-> 493172 ,
