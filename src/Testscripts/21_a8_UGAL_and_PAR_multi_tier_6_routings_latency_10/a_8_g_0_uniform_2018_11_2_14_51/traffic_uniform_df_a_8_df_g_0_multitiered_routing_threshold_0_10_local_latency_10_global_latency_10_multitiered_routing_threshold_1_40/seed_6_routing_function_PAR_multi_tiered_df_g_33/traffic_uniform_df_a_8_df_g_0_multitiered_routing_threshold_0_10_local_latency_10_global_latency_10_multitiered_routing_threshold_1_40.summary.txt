routing_delay = 0;
alloc_iters = 1;
credit_delay = 2;
num_vcs = 7;
sim_count = 1;
output_speedup = 1;
vc_alloc_delay = 1;
input_speedup = 1;
sample_period = 10000;
packet_size = 1;
internal_speedup = 4.0;
sw_allocator = separable_input_first;
vc_buf_size = 64;
warmup_periods = 3;
injection_rate_uses_flits = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_alloc_delay = 1;
priority = none;
wait_for_tail_credit = 0;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 33;
routing_function = PAR_multi_tiered;
seed = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010018,   0.01002,    38.902,    38.906,         0,  887279,  196082,  |4-> 873551 , |5-> 0 , |6-> 0 ,,  |4-> 193057 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 15593 , |3-> 79039 , |4-> 792647 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 3493 , |3-> 17731 , |4-> 174858 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.99005,   0.98999,    155.79,    155.85,         0,  112659648,  2199647,  |4-> 35559 , |5-> 31705247 , |6-> 79271624 ,,  |4-> 4152 , |5-> 18505 , |6-> 2147545 ,,  |1-> 0 , |2-> 1679515 , |3-> 890887 , |4-> 10257778 , |5-> 52369758 , |6-> 47461710 ,,  |1-> 0 , |2-> 31609 , |3-> 55260 , |4-> 436993 , |5-> 1152574 , |6-> 523211 ,
