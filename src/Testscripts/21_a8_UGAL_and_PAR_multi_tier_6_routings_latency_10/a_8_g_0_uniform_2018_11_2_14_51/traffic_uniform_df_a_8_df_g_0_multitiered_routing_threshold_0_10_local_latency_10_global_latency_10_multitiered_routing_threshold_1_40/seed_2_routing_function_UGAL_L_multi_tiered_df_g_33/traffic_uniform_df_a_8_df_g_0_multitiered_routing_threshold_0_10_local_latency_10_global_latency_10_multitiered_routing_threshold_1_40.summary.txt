routing_delay = 0;
alloc_iters = 1;
credit_delay = 2;
num_vcs = 7;
sim_count = 1;
output_speedup = 1;
vc_alloc_delay = 1;
input_speedup = 1;
sample_period = 10000;
packet_size = 1;
internal_speedup = 4.0;
sw_allocator = separable_input_first;
vc_buf_size = 64;
warmup_periods = 3;
injection_rate_uses_flits = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_alloc_delay = 1;
priority = none;
wait_for_tail_credit = 0;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 33;
routing_function = UGAL_L_multi_tiered;
seed = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099972, 0.0099974,    35.743,    35.745,         0,  529336,  103641,  |4-> 515325 , |5-> 0 , |6-> 0 ,,  |4-> 100685 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 15023 , |3-> 46923 , |4-> 467390 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 3181 , |3-> 9274 , |4-> 91186 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.98998,   0.98997,    145.15,    145.22,         0,  61030265,  1966650,  |4-> 21381 , |5-> 12395184 , |6-> 46971383 ,,  |4-> 3200 , |5-> 16405 , |6-> 1913168 ,,  |1-> 0 , |2-> 1657136 , |3-> 460758 , |4-> 5256150 , |5-> 25768388 , |6-> 27887833 ,,  |1-> 0 , |2-> 35380 , |3-> 41541 , |4-> 342979 , |5-> 959510 , |6-> 587240 ,
