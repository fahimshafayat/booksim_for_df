vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
df_g = 25;
perm_seed = 64;
routing_function = UGAL_L;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010033,  0.010034,    40.339,    40.345,         0,  363757,  116061,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 14954 , |3-> 2228 , |4-> 24188 , |5-> 117224 , |6-> 205163 ,,  |1-> 0 , |2-> 5451 , |3-> 753 , |4-> 8120 , |5-> 37754 , |6-> 63983 ,
simulation,         2,      0.99,   0.55756,   0.60598,    1547.2,    745.61,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,   0.49838,   0.49839,    384.88,    104.52,         0,  42039565,  23276912,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 2033155 , |3-> 260846 , |4-> 2720784 , |5-> 13290882 , |6-> 23733898 ,,  |1-> 0 , |2-> 778471 , |3-> 150490 , |4-> 1700630 , |5-> 7809028 , |6-> 12838293 ,
simulation,         4,      0.74,   0.55575,   0.59062,    830.61,    517.68,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.62,   0.54339,   0.56724,    458.36,     340.1,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.56,   0.53179,   0.53234,    920.56,    282.31,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.53,   0.52017,    0.5202,    495.27,    185.75,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.51,   0.50659,    0.5066,    411.93,    128.63,         1,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,   0.45014,   0.45015,    65.534,    64.858,         0,  15910256,  6763548,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 715149 , |3-> 98732 , |4-> 1037456 , |5-> 5057877 , |6-> 9001042 ,,  |1-> 0 , |2-> 257565 , |3-> 43750 , |4-> 497168 , |5-> 2268285 , |6-> 3696780 ,
simulation,        11,       0.4,   0.40012,   0.40011,    50.438,    50.461,         0,  14004767,  5182871,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 605536 , |3-> 86948 , |4-> 918561 , |5-> 4463100 , |6-> 7930622 ,,  |1-> 0 , |2-> 218213 , |3-> 33441 , |4-> 381800 , |5-> 1732125 , |6-> 2817292 ,
simulation,        12,      0.35,   0.35014,   0.35014,    44.677,    44.688,         0,  12412001,  4350435,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 528192 , |3-> 76595 , |4-> 815534 , |5-> 3962671 , |6-> 7029009 ,,  |1-> 0 , |2-> 191685 , |3-> 29170 , |4-> 320246 , |5-> 1450695 , |6-> 2358639 ,
