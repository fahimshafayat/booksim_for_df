vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
df_g = 31;
routing_function = PAR_multi_tiered;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099998,  0.010001,    46.752,    46.754,         0,  471422,  440158,  |4-> 471422 , |5-> 0 , |6-> 0 ,,  |4-> 440157 , |5-> 1 , |6-> 0 ,,  |1-> 0 , |2-> 85 , |3-> 68698 , |4-> 402639 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 92 , |3-> 52088 , |4-> 387977 , |5-> 1 , |6-> 0 ,
simulation,         2,      0.99,     0.331,    0.3782,    3189.3,    1234.9,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,   0.40322,   0.43896,    765.57,    596.78,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.25,   0.24999,   0.24999,    66.082,    66.086,         0,  8430530,  13041501,  |4-> 18340 , |5-> 7494368 , |6-> 917822 ,,  |4-> 6153 , |5-> 7796779 , |6-> 5238569 ,,  |1-> 0 , |2-> 13 , |3-> 297526 , |4-> 1688287 , |5-> 5854708 , |6-> 589996 ,,  |1-> 0 , |2-> 329 , |3-> 264535 , |4-> 1769727 , |5-> 8044339 , |6-> 2962571 ,
simulation,         5,      0.37,   0.37011,   0.37012,    86.625,    86.644,         0,  8493170,  20253158,  |4-> 3632 , |5-> 5179486 , |6-> 3310052 ,,  |4-> 5634 , |5-> 7655711 , |6-> 12591813 ,,  |1-> 0 , |2-> 1 , |3-> 277443 , |4-> 1509002 , |5-> 4696356 , |6-> 2010368 ,,  |1-> 0 , |2-> 264 , |3-> 362221 , |4-> 2289747 , |5-> 10368396 , |6-> 7232530 ,
simulation,         6,      0.43,   0.42365,   0.42372,    485.75,    190.16,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.4,   0.40006,    0.4001,    96.058,    96.085,         0,  7249605,  22061793,  |4-> 3548 , |5-> 1160389 , |6-> 6085668 ,,  |4-> 5801 , |5-> 1553994 , |6-> 20501998 ,,  |1-> 0 , |2-> 1 , |3-> 140087 , |4-> 780137 , |5-> 2747432 , |6-> 3581948 ,,  |1-> 0 , |2-> 232 , |3-> 291931 , |4-> 1875519 , |5-> 8001494 , |6-> 11892617 ,
simulation,         8,      0.41,   0.40992,   0.40997,    112.31,     105.7,         0,  7526086,  24676078,  |4-> 3727 , |5-> 655437 , |6-> 6866922 ,,  |4-> 5885 , |5-> 866292 , |6-> 23803901 ,,  |1-> 0 , |2-> 0 , |3-> 128999 , |4-> 723564 , |5-> 2639152 , |6-> 4034371 ,,  |1-> 0 , |2-> 181 , |3-> 321324 , |4-> 2034052 , |5-> 8508240 , |6-> 13812281 ,
simulation,         9,      0.42,   0.41769,   0.41774,     485.8,    148.65,         0,  14592776,  50816361,  |4-> 4389 , |5-> 787864 , |6-> 13800523 ,,  |4-> 6384 , |5-> 1042400 , |6-> 49767577 ,,  |1-> 0 , |2-> 1 , |3-> 232217 , |4-> 1318190 , |5-> 4935835 , |6-> 8106533 ,,  |1-> 0 , |2-> 404 , |3-> 662428 , |4-> 4142981 , |5-> 17120739 , |6-> 28889809 ,
simulation,        11,      0.35,   0.35009,   0.35009,    80.048,    80.052,         0,  9102856,  19065003,  |4-> 3691 , |5-> 6932131 , |6-> 2167034 ,,  |4-> 5576 , |5-> 9835777 , |6-> 9223650 ,,  |1-> 0 , |2-> 0 , |3-> 337250 , |4-> 1822096 , |5-> 5573048 , |6-> 1370462 ,,  |1-> 0 , |2-> 281 , |3-> 382542 , |4-> 2402092 , |5-> 11052309 , |6-> 5227779 ,
simulation,        12,       0.3,   0.30009,   0.30009,    71.695,    71.699,         0,  9361250,  16029065,  |4-> 4009 , |5-> 7498641 , |6-> 1858600 ,,  |4-> 5504 , |5-> 8158241 , |6-> 7865320 ,,  |1-> 0 , |2-> 2 , |3-> 351546 , |4-> 1888878 , |5-> 5924601 , |6-> 1196223 ,,  |1-> 0 , |2-> 286 , |3-> 299950 , |4-> 1971069 , |5-> 9343227 , |6-> 4414533 ,
