vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
df_g = 27;
routing_function = vlb;
shift_by_group = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010035,  0.010034,    64.595,      64.6,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.38407,   0.43594,    3077.0,    1249.8,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,   0.38449,   0.43728,    1175.4,    973.26,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.25,   0.25009,    0.2501,    67.618,    67.619,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         5,      0.37,   0.37016,   0.37016,     83.92,    83.925,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         6,      0.43,   0.38445,   0.42663,    554.19,    550.18,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.4,   0.38577,   0.39439,    923.53,    848.74,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.38,   0.38015,   0.38013,    98.577,    98.592,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         9,      0.39,   0.38485,   0.38877,    495.86,     475.5,         1,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35015,   0.35014,    75.428,    75.427,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,        11,       0.3,   0.30016,   0.30017,    69.717,    69.718,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,        13,       0.2,   0.20004,   0.20005,    66.466,    66.467,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
