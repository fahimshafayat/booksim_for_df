vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 9;
routing_function = PAR;
seed = 8;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099911, 0.0099928,    41.321,    41.334,         0,  231103,  49783,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 13556 , |3-> 1341 , |4-> 14732 , |5-> 72839 , |6-> 128635 ,,  |1-> 0 , |2-> 3368 , |3-> 318 , |4-> 3397 , |5-> 15797 , |6-> 26903 ,
simulation,         2,      0.99,   0.99038,   0.98997,    98.179,    98.256,         0,  34579106,  475079,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 1923344 , |3-> 195882 , |4-> 2221618 , |5-> 10900318 , |6-> 19337944 ,,  |1-> 0 , |2-> 32333 , |3-> 6932 , |4-> 57973 , |5-> 189646 , |6-> 188195 ,
