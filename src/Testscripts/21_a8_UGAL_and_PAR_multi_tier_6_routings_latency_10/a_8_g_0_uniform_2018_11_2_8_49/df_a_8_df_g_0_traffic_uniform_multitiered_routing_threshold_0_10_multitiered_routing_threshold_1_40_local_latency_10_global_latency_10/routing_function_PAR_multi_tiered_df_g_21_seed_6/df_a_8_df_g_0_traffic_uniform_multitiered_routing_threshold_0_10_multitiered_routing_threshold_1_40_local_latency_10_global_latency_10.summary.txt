vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 21;
routing_function = PAR_multi_tiered;
seed = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099965, 0.0099966,    38.529,    38.535,         0,  556513,  125790,  |4-> 542706 , |5-> 0 , |6-> 0 ,,  |4-> 122662 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 15480 , |3-> 60212 , |4-> 480821 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 3510 , |3-> 13632 , |4-> 108648 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.78477,   0.82732,     982.9,    453.04,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,   0.50009,   0.50008,    37.809,    37.813,         0,  34259972,  1864156,  |4-> 9445598 , |5-> 24008670 , |6-> 0 ,,  |4-> 36160 , |5-> 1791522 , |6-> 5 ,,  |1-> 0 , |2-> 856359 , |3-> 1638302 , |4-> 13918944 , |5-> 17846367 , |6-> 0 ,,  |1-> 0 , |2-> 38781 , |3-> 60343 , |4-> 494697 , |5-> 1270333 , |6-> 2 ,
simulation,         4,      0.74,   0.74003,   0.74003,    48.336,    48.342,         0,  50592570,  3216880,  |4-> 681039 , |5-> 45192391 , |6-> 3511776 ,,  |4-> 2535 , |5-> 1867143 , |6-> 1308302 ,,  |1-> 0 , |2-> 1251792 , |3-> 1248829 , |4-> 11473829 , |5-> 35154850 , |6-> 1463270 ,,  |1-> 0 , |2-> 41985 , |3-> 80451 , |4-> 677025 , |5-> 1873381 , |6-> 544038 ,
simulation,         5,      0.86,   0.79808,   0.81699,     620.0,    346.99,         1,  0,  0,  ,  ,  ,  
simulation,         6,       0.8,   0.80008,   0.80005,    69.716,    69.745,         0,  52330524,  5155705,  |4-> 93471 , |5-> 39025354 , |6-> 11941135 ,,  |4-> 2607 , |5-> 710208 , |6-> 4363012 ,,  |1-> 0 , |2-> 1311426 , |3-> 1132727 , |4-> 10550891 , |5-> 34188862 , |6-> 5146618 ,,  |1-> 0 , |2-> 83848 , |3-> 108162 , |4-> 910112 , |5-> 2476274 , |6-> 1577309 ,
simulation,         7,      0.83,   0.81092,    0.8142,    503.94,    257.28,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.81,   0.81008,   0.81008,    76.544,    76.581,         0,  52306679,  5574241,  |4-> 67168 , |5-> 36594738 , |6-> 14374682 ,,  |4-> 2639 , |5-> 520632 , |6-> 4953238 ,,  |1-> 0 , |2-> 1308874 , |3-> 1094252 , |4-> 10246054 , |5-> 33456270 , |6-> 6201229 ,,  |1-> 0 , |2-> 102115 , |3-> 118847 , |4-> 989480 , |5-> 2628817 , |6-> 1734982 ,
simulation,         9,      0.82,   0.82005,   0.82006,     86.96,    87.031,         0,  51878518,  6199358,  |4-> 50197 , |5-> 32608354 , |6-> 17962400 ,,  |4-> 2633 , |5-> 325029 , |6-> 5744684 ,,  |1-> 0 , |2-> 1294004 , |3-> 1028810 , |4-> 9717609 , |5-> 32060912 , |6-> 7777183 ,,  |1-> 0 , |2-> 132063 , |3-> 137756 , |4-> 1119775 , |5-> 2885207 , |6-> 1924557 ,
simulation,        11,      0.75,   0.75006,   0.75006,    50.694,    50.706,         0,  51002119,  3509131,  |4-> 497349 , |5-> 44187470 , |6-> 5094593 ,,  |4-> 2589 , |5-> 1611237 , |6-> 1853612 ,,  |1-> 0 , |2-> 1266182 , |3-> 1223753 , |4-> 11281542 , |5-> 35088519 , |6-> 2142123 ,,  |1-> 0 , |2-> 44891 , |3-> 82397 , |4-> 698388 , |5-> 1934171 , |6-> 749284 ,
simulation,        12,       0.7,   0.70006,   0.70005,    42.736,    42.741,         0,  48461151,  2442097,  |4-> 1667308 , |5-> 45253090 , |6-> 397542 ,,  |4-> 2548 , |5-> 2196251 , |6-> 208403 ,,  |1-> 0 , |2-> 1189268 , |3-> 1312839 , |4-> 11950589 , |5-> 33848316 , |6-> 160139 ,,  |1-> 0 , |2-> 37625 , |3-> 72826 , |4-> 598894 , |5-> 1640388 , |6-> 92364 ,
simulation,        13,      0.65,   0.64999,   0.64998,    40.062,    40.065,         0,  45127547,  2079576,  |4-> 3183820 , |5-> 40871318 , |6-> 13847 ,,  |4-> 3122 , |5-> 2028456 , |6-> 13560 ,,  |1-> 0 , |2-> 1104915 , |3-> 1366351 , |4-> 12238729 , |5-> 30412135 , |6-> 5417 ,,  |1-> 0 , |2-> 37004 , |3-> 66073 , |4-> 536577 , |5-> 1433907 , |6-> 6015 ,
