vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 9;
routing_function = PAR_multi_tiered;
seed = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010014,  0.010015,    36.404,    36.408,         0,  235452,  48209,  |4-> 221340 , |5-> 0 , |6-> 0 ,,  |4-> 45240 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 15316 , |3-> 32017 , |4-> 188119 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 3262 , |3-> 6523 , |4-> 38424 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.99006,   0.98999,    93.183,    93.257,         0,  29790635,  414863,  |4-> 12452 , |5-> 27510919 , |6-> 615525 ,,  |4-> 1115 , |5-> 260316 , |6-> 127051 ,,  |1-> 0 , |2-> 1750966 , |3-> 2352236 , |4-> 14733533 , |5-> 10918334 , |6-> 35566 ,,  |1-> 0 , |2-> 28469 , |3-> 44188 , |4-> 222672 , |5-> 115173 , |6-> 4361 ,
