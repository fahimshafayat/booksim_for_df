vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 1000;
df_a = 16;
df_arrangement = absolute_improved;
global_latency = 10;
local_latency = 1;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 35;
routing_function = UGAL_L_multi_tiered;
seed = 12;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099987, 0.0099998,    19.053,    19.055,         0,  254517,  15584,  |4-> 247625 , |5-> 0 , |6-> 0 ,,  |4-> 15116 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 7133 , |3-> 15815 , |4-> 231569 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 484 , |3-> 912 , |4-> 14188 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.84023,   0.85948,    514.45,    391.49,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,   0.49988,   0.49988,    21.739,    21.745,         0,  12024396,  1484511,  |4-> 11522182 , |5-> 200613 , |6-> 0 ,,  |4-> 990804 , |5-> 432591 , |6-> 0 ,,  |1-> 0 , |2-> 314052 , |3-> 773775 , |4-> 10822715 , |5-> 113854 , |6-> 0 ,,  |1-> 0 , |2-> 61138 , |3-> 34946 , |4-> 1044339 , |5-> 344088 , |6-> 0 ,
simulation,         4,      0.74,   0.74002,   0.73999,    26.736,    26.754,         0,  18204117,  1952751,  |4-> 16760825 , |5-> 967214 , |6-> 2739 ,,  |4-> 839465 , |5-> 1007601 , |6-> 38094 ,,  |1-> 0 , |2-> 491017 , |3-> 1118946 , |4-> 15976605 , |5-> 617549 , |6-> 0 ,,  |1-> 0 , |2-> 67610 , |3-> 33886 , |4-> 1014997 , |5-> 822497 , |6-> 13761 ,
simulation,         5,      0.86,   0.85391,   0.85932,    97.366,     101.6,         0,  35170407,  4462385,  |4-> 6786547 , |5-> 21550506 , |6-> 5910491 ,,  |4-> 81366 , |5-> 2356470 , |6-> 1883544 ,,  |1-> 0 , |2-> 938225 , |3-> 894309 , |4-> 13247863 , |5-> 18046950 , |6-> 2043060 ,,  |1-> 0 , |2-> 141574 , |3-> 47610 , |4-> 950115 , |5-> 2723010 , |6-> 600076 ,
simulation,         6,      0.92,   0.84945,   0.86948,    427.56,    308.27,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.89,    0.8605,   0.87524,    254.09,    222.13,         0,  46585574,  6424081,  |4-> 2747700 , |5-> 23762112 , |6-> 18827245 ,,  |4-> 34716 , |5-> 1281309 , |6-> 4931639 ,,  |1-> 0 , |2-> 1262228 , |3-> 788411 , |4-> 12063755 , |5-> 25825234 , |6-> 6645946 ,,  |1-> 0 , |2-> 178095 , |3-> 94847 , |4-> 1406881 , |5-> 3342763 , |6-> 1401495 ,
simulation,         8,       0.9,   0.85826,   0.87598,    328.12,    265.38,         0,  47717320,  6889451,  |4-> 2385435 , |5-> 22430961 , |6-> 21621230 ,,  |4-> 31135 , |5-> 1100930 , |6-> 5572537 ,,  |1-> 0 , |2-> 1292963 , |3-> 766422 , |4-> 11788503 , |5-> 26197802 , |6-> 7671630 ,,  |1-> 0 , |2-> 186862 , |3-> 107251 , |4-> 1530657 , |5-> 3525952 , |6-> 1538729 ,
simulation,         9,      0.91,   0.85427,   0.87415,    414.83,    305.35,         0,  46385773,  7030115,  |4-> 2275206 , |5-> 20643924 , |6-> 22220483 ,,  |4-> 28996 , |5-> 1002772 , |6-> 5811156 ,,  |1-> 0 , |2-> 1258746 , |3-> 730304 , |4-> 11288364 , |5-> 25197237 , |6-> 7911122 ,,  |1-> 0 , |2-> 189231 , |3-> 110644 , |4-> 1567895 , |5-> 3576208 , |6-> 1586137 ,
simulation,        11,      0.85,   0.84742,   0.85001,    72.486,    73.909,         0,  24711273,  3109918,  |4-> 6916908 , |5-> 14908701 , |6-> 2240029 ,,  |4-> 97198 , |5-> 1992690 , |6-> 917637 ,,  |1-> 0 , |2-> 658452 , |3-> 748199 , |4-> 10968020 , |5-> 11577256 , |6-> 759346 ,,  |1-> 0 , |2-> 102643 , |3-> 29324 , |4-> 676728 , |5-> 1987871 , |6-> 313352 ,
simulation,        12,       0.8,   0.80005,   0.79994,     37.57,    37.654,         0,  19659789,  2375222,  |4-> 14154665 , |5-> 4977509 , |6-> 15588 ,,  |4-> 391826 , |5-> 1703113 , |6-> 200982 ,,  |1-> 0 , |2-> 528626 , |3-> 1020277 , |4-> 14682227 , |5-> 3428464 , |6-> 195 ,,  |1-> 0 , |2-> 79340 , |3-> 25385 , |4-> 745761 , |5-> 1450398 , |6-> 74338 ,
simulation,        13,      0.75,   0.74997,   0.74998,    27.606,    27.627,         0,  18485889,  1983450,  |4-> 16897780 , |5-> 1104050 , |6-> 4035 ,,  |4-> 810574 , |5-> 1046701 , |6-> 57868 ,,  |1-> 0 , |2-> 498089 , |3-> 1131516 , |4-> 16143837 , |5-> 712446 , |6-> 1 ,,  |1-> 0 , |2-> 68326 , |3-> 33465 , |4-> 997436 , |5-> 863105 , |6-> 21118 ,
