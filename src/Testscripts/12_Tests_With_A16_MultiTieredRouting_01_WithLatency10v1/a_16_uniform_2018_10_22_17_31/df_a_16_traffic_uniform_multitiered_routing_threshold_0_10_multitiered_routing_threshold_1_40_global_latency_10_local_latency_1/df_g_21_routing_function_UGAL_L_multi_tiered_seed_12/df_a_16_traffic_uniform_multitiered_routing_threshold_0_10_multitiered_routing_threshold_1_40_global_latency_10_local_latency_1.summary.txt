vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 1000;
df_a = 16;
df_arrangement = absolute_improved;
global_latency = 10;
local_latency = 1;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 21;
routing_function = UGAL_L_multi_tiered;
seed = 12;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099918,  0.010003,    18.766,    18.769,         0,  153009,  9026,  |4-> 146029 , |5-> 0 , |6-> 0 ,,  |4-> 8627 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 7181 , |3-> 10714 , |4-> 135114 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 414 , |3-> 614 , |4-> 7998 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.94701,   0.96549,    314.23,    260.04,         0,  28831759,  2659342,  |4-> 502351 , |5-> 6759902 , |6-> 20236738 ,,  |4-> 9707 , |5-> 157013 , |6-> 2415710 ,,  |1-> 0 , |2-> 1348373 , |3-> 825546 , |4-> 11185827 , |5-> 12764930 , |6-> 2707083 ,,  |1-> 0 , |2-> 79730 , |3-> 126440 , |4-> 1303102 , |5-> 1016005 , |6-> 134065 ,
