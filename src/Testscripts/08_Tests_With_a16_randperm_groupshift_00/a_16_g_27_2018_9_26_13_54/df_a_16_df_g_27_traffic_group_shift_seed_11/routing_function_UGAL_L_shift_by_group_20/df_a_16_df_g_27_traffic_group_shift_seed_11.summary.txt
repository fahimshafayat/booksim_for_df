vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 27;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L;
shift_by_group = 20;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01,  0.010038,  0.010039,    15.308,         0
simulation,         2,      0.99,   0.16976,   0.41115,    643.27,         1
simulation,         3,       0.5,   0.19732,   0.43761,     376.5,         1
simulation,         4,      0.25,   0.15372,   0.24967,    512.12,         1
simulation,         5,      0.13,   0.13001,   0.13004,    74.637,         0
simulation,         6,      0.19,   0.15423,   0.18999,    460.39,         1
simulation,         7,      0.16,   0.15117,   0.16004,    700.52,         0
simulation,         8,      0.17,   0.15174,   0.17001,     426.2,         1
simulation,         9,      0.15,    0.1482,   0.15006,    178.78,         0
simulation,        10,       0.1,       0.1,   0.10001,    62.459,         0
simulation,        11,      0.05,  0.050122,  0.050113,    18.318,         0
