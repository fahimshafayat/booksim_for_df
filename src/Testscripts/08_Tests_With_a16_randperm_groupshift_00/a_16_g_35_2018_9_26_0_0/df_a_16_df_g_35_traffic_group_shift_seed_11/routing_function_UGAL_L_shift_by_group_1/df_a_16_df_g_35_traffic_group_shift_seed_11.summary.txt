vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01,  0.010046,  0.010042,     15.59,         0
simulation,         2,      0.99,   0.16506,   0.40148,     633.7,         1
simulation,         3,       0.5,   0.20456,   0.43717,    346.35,         1
simulation,         4,      0.25,   0.15385,   0.24972,    760.65,         1
simulation,         5,      0.13,   0.13003,   0.13012,    69.831,         0
simulation,         6,      0.19,   0.15379,   0.19006,    498.57,         1
simulation,         7,      0.16,   0.15136,   0.16004,    658.22,         0
simulation,         8,      0.17,   0.15207,   0.17001,    390.82,         1
simulation,         9,      0.15,   0.14699,   0.15008,    321.27,         0
simulation,        10,       0.1,   0.10012,   0.10014,    58.043,         0
simulation,        11,      0.05,  0.050092,  0.050139,    24.345,         0
