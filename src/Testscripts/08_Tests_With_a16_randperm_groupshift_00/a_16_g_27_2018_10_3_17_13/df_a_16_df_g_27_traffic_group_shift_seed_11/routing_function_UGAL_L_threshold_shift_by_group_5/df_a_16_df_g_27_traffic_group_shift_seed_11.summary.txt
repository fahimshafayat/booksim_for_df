vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 27;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L_threshold;
shift_by_group = 5;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01,  0.010035,  0.010039,    15.252,         0
simulation,         2,      0.99,   0.17007,   0.41128,    643.71,         1
simulation,         3,       0.5,   0.19811,   0.43869,    375.45,         1
simulation,         4,      0.25,   0.15777,   0.24958,    728.94,         1
simulation,         5,      0.13,   0.12989,   0.13004,    79.913,         0
simulation,         6,      0.19,   0.16726,   0.18989,    429.23,         1
simulation,         7,      0.16,   0.15694,   0.16004,    276.44,         0
simulation,         8,      0.17,   0.16268,   0.17001,    548.54,         0
simulation,         9,      0.18,   0.16594,   0.17996,    331.79,         1
simulation,        10,      0.15,   0.14916,   0.15007,    133.55,         0
simulation,        11,       0.1,       0.1,   0.10001,    61.362,         0
simulation,        12,      0.05,  0.050125,  0.050113,    18.166,         0
