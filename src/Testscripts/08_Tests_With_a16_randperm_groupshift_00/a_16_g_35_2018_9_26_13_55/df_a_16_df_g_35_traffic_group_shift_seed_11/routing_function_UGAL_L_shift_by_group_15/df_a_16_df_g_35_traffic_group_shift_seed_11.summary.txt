vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L;
shift_by_group = 15;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01,  0.010044,  0.010042,    15.559,         0
simulation,         2,      0.99,   0.16411,   0.40064,     635.5,         1
simulation,         3,       0.5,   0.20398,   0.43716,    350.06,         1
simulation,         4,      0.25,   0.16013,   0.24963,    436.07,         1
simulation,         5,      0.13,   0.13001,   0.13012,    71.281,         0
simulation,         6,      0.19,   0.15366,   0.19004,     500.1,         1
simulation,         7,      0.16,   0.15109,   0.16004,    236.67,         1
simulation,         8,      0.14,   0.13946,   0.14011,    116.75,         0
simulation,         9,      0.15,   0.14699,   0.15008,    311.43,         0
simulation,        10,      0.15,   0.14699,   0.15008,    311.43,         0
simulation,        11,       0.1,   0.10012,   0.10014,    58.213,         0
simulation,        12,      0.05,  0.050096,  0.050139,    24.586,         0
