vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 48;
routing_function = UGAL_L_threshold;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01,  0.010002,  0.010006,    13.952,         0
simulation,         2,      0.99,    0.6627,   0.68296,    586.02,         1
simulation,         3,       0.5,   0.49979,   0.49978,    18.604,         0
simulation,         4,      0.74,   0.66739,   0.67677,    330.86,         1
simulation,         5,      0.62,   0.61815,   0.61879,    48.098,         0
simulation,         6,      0.68,   0.65728,   0.66231,     186.6,         0
simulation,         7,      0.71,   0.66426,   0.67108,    248.91,         1
simulation,         8,      0.69,   0.66009,   0.66593,    188.53,         1
simulation,         9,      0.65,   0.64215,    0.6446,    99.071,         0
simulation,        10,       0.6,   0.59925,   0.59954,    36.362,         0
simulation,        11,      0.55,   0.54974,   0.54975,    22.665,         0
simulation,        12,       0.5,   0.49979,   0.49978,    18.604,         0
