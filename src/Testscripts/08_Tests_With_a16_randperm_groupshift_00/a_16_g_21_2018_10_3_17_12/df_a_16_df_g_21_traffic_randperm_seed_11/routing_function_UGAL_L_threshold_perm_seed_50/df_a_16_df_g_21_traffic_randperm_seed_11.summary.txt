vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 50;
routing_function = UGAL_L_threshold;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01,  0.010002,  0.010006,    13.984,         0
simulation,         2,      0.99,   0.65681,   0.67553,    596.49,         1
simulation,         3,       0.5,   0.49978,   0.49978,    18.995,         0
simulation,         4,      0.74,   0.66369,   0.67369,    333.95,         1
simulation,         5,      0.62,   0.61891,    0.6194,    45.397,         0
simulation,         6,      0.68,   0.65438,   0.66061,    177.84,         1
simulation,         7,      0.65,   0.64188,   0.64428,    103.95,         0
simulation,         8,      0.66,   0.64666,   0.65016,    138.88,         0
simulation,         9,      0.67,   0.65148,   0.65624,    178.12,         0
simulation,        10,      0.65,   0.64188,   0.64428,    103.95,         0
simulation,        11,       0.6,   0.59955,   0.59962,    34.331,         0
simulation,        12,      0.55,   0.54971,   0.54975,    23.037,         0
simulation,        13,       0.5,   0.49978,   0.49978,    18.995,         0
