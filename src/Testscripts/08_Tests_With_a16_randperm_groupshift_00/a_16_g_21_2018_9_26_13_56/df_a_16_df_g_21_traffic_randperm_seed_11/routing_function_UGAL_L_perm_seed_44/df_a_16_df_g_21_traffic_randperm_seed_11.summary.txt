vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 44;
routing_function = UGAL_L;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01,  0.010004,  0.010006,    13.959,         0
simulation,         2,      0.99,   0.66007,   0.67915,     591.8,         1
simulation,         3,       0.5,   0.49976,   0.49978,    18.991,         0
simulation,         4,      0.74,      0.66,   0.66938,    341.16,         1
simulation,         5,      0.62,   0.61774,   0.61823,    54.556,         0
simulation,         6,      0.68,   0.64977,   0.65629,    190.57,         1
simulation,         7,      0.65,   0.63906,   0.64219,    125.44,         0
simulation,         8,      0.66,   0.64319,   0.64746,     158.0,         0
simulation,         9,      0.67,   0.64723,   0.65261,     192.2,         0
simulation,        10,      0.65,   0.63906,   0.64219,    125.44,         0
simulation,        11,       0.6,   0.59894,   0.59952,    39.557,         0
simulation,        12,      0.55,   0.54979,   0.54975,    23.239,         0
simulation,        13,       0.5,   0.49976,   0.49978,    18.991,         0
