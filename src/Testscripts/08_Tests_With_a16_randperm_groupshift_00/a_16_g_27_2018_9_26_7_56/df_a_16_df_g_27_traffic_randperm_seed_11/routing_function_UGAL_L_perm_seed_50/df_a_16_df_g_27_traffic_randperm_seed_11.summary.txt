vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 27;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 50;
routing_function = UGAL_L;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01,  0.010039,  0.010039,    14.042,         0
simulation,         2,      0.99,   0.63562,   0.65425,    627.13,         1
simulation,         3,       0.5,   0.50014,   0.50016,    20.873,         0
simulation,         4,      0.74,   0.64587,   0.65801,    381.05,         1
simulation,         5,      0.62,   0.61709,   0.61831,     67.84,         0
simulation,         6,      0.68,   0.64352,    0.6501,    224.14,         1
simulation,         7,      0.65,   0.63537,   0.63914,     150.8,         0
simulation,         8,      0.66,   0.63837,   0.64327,    188.21,         0
simulation,         9,      0.67,   0.64122,   0.64699,    190.55,         1
simulation,        10,      0.65,   0.63537,   0.63914,     150.8,         0
simulation,        11,       0.6,   0.59947,   0.59991,    43.606,         0
simulation,        12,      0.55,   0.55008,   0.55012,    25.902,         0
simulation,        13,       0.5,   0.50014,   0.50016,    20.873,         0
