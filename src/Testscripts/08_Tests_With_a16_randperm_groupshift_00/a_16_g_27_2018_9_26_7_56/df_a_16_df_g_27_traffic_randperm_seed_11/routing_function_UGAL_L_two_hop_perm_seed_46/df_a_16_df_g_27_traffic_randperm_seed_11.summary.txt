vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 27;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 46;
routing_function = UGAL_L_two_hop;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01,  0.010039,  0.010039,    14.048,         0
simulation,         2,      0.99,   0.64551,    0.6643,    608.79,         1
simulation,         3,       0.5,   0.50018,   0.50016,    20.444,         0
simulation,         4,      0.74,   0.65277,   0.66407,    361.96,         1
simulation,         5,      0.62,   0.61792,    0.6191,    62.264,         0
simulation,         6,      0.68,   0.64756,   0.65398,    203.78,         1
simulation,         7,      0.65,   0.63753,   0.64079,    131.35,         0
simulation,         8,      0.66,   0.64231,   0.64678,    163.28,         0
simulation,         9,      0.67,   0.64541,   0.65084,    197.98,         0
simulation,        10,      0.65,   0.63753,   0.64079,    131.35,         0
simulation,        11,       0.6,   0.59969,   0.59999,    41.377,         0
simulation,        12,      0.55,   0.55012,   0.55012,    26.106,         0
simulation,        13,       0.5,   0.50018,   0.50016,    20.444,         0
