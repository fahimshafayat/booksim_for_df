vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L_threshold;
shift_by_group = 20;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01,  0.010002,  0.010006,    15.014,         0
simulation,         2,      0.99,   0.16916,   0.41658,    660.97,         1
simulation,         3,       0.5,   0.18828,   0.43519,     410.9,         1
simulation,         4,      0.25,    0.1512,   0.24999,    557.83,         1
simulation,         5,      0.13,    0.1299,   0.12993,    79.172,         0
simulation,         6,      0.19,   0.15826,   0.18988,     490.4,         1
simulation,         7,      0.16,    0.1565,   0.15994,    311.12,         0
simulation,         8,      0.17,   0.15913,   0.16992,    311.11,         1
simulation,         9,      0.15,   0.14922,   0.14993,    137.14,         0
simulation,        10,       0.1,  0.099878,  0.099889,    62.193,         0
simulation,        11,      0.05,  0.049981,  0.049976,    17.352,         0
