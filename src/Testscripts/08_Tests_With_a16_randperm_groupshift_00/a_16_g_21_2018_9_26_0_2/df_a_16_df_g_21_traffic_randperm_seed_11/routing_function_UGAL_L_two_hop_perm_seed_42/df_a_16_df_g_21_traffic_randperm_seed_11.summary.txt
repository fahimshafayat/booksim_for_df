vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 42;
routing_function = UGAL_L_two_hop;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01,  0.010002,  0.010006,    13.959,         0
simulation,         2,      0.99,   0.67349,   0.69856,    569.51,         1
simulation,         3,       0.5,   0.49978,   0.49978,    19.734,         0
simulation,         4,      0.74,   0.67909,   0.68719,    296.04,         1
simulation,         5,      0.62,   0.61852,   0.61923,     43.59,         0
simulation,         6,      0.68,   0.66451,   0.66788,    139.24,         0
simulation,         7,      0.71,   0.67482,   0.68167,    204.22,         1
simulation,         8,      0.69,   0.66888,   0.67287,    168.13,         0
simulation,         9,       0.7,   0.67252,   0.67783,    172.68,         1
simulation,        10,       0.7,   0.67252,   0.67783,    172.68,         1
simulation,        11,      0.65,   0.64501,   0.64623,    74.113,         0
simulation,        12,       0.6,   0.59936,   0.59966,    32.076,         0
simulation,        13,      0.55,   0.54979,   0.54975,    23.088,         0
