vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 27;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 50;
routing_function = UGAL_L_threshold;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01,  0.010038,  0.010039,    14.009,         0
simulation,         2,      0.99,   0.63939,   0.65844,    626.07,         1
simulation,         3,       0.5,   0.50015,   0.50016,    20.405,         0
simulation,         4,      0.74,   0.64834,   0.66145,    371.84,         1
simulation,         5,      0.62,   0.61852,   0.61946,    56.505,         0
simulation,         6,      0.68,   0.64772,    0.6537,    202.67,         1
simulation,         7,      0.65,   0.63871,   0.64184,    124.94,         0
simulation,         8,      0.66,   0.64288,   0.64679,    162.14,         0
simulation,         9,      0.67,   0.64562,   0.65121,    204.04,         0
simulation,        10,      0.65,   0.63871,   0.64184,    124.94,         0
simulation,        11,       0.6,   0.59978,   0.60002,    38.814,         0
simulation,        12,      0.55,   0.55011,   0.55012,    25.102,         0
simulation,        13,       0.5,   0.50015,   0.50016,    20.405,         0
