vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 9;
perm_seed = 72;
routing_function = UGAL_L_multi_tiered;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4998,   0.49978,    36.361,    36.365,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.73981,   0.73979,    90.332,    64.261,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,    0.7903,   0.80142,    618.43,    254.33,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.78094,   0.78085,    500.76,    153.68,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.76334,   0.76338,     484.8,    102.39,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.75,   0.74773,   0.74773,    356.99,    78.291,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.76,   0.75601,   0.75598,    286.49,    88.132,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.7,    0.7002,   0.70017,    49.434,    49.457,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.65,   0.65006,   0.65004,    43.275,    43.286,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.6,   0.59998,   0.59996,    40.089,    40.095,         0,  0,  0,  ,  ,  ,  
