vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_L_restricted;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.35545,   0.39076,     968.3,    680.71,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25007,   0.25008,    74.627,    74.688,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.35213,   0.35233,    820.08,    288.22,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31007,   0.31007,    79.131,    79.239,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,     0.338,     0.338,    324.56,     115.2,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.32,   0.31994,   0.31999,    87.908,    85.574,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.33,   0.32975,   0.32975,    143.59,    88.222,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.30005,   0.30006,    78.258,    78.357,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20009,   0.20009,    71.022,    71.057,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.15,   0.15006,   0.15007,    66.091,    66.105,         0,  0,  0,  ,  ,  ,  
