vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 0;
multitiered_routing_threshold_1 = 50;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_L_multi_tiered;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.31877,   0.35729,    1417.8,    916.96,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24966,   0.24974,    175.38,    128.77,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.32085,   0.34351,    503.59,    434.84,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.29699,   0.29743,    815.05,    377.72,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27603,   0.27645,     520.9,    273.54,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,   0.25914,   0.25917,    315.11,    146.93,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.27,   0.26816,   0.26828,    453.67,    203.74,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.2,   0.20009,   0.20009,    75.565,     75.62,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.15,   0.15007,   0.15007,    69.684,    69.703,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.1,   0.10007,   0.10007,    58.718,    58.726,         0,  0,  0,  ,  ,  ,  
