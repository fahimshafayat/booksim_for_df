vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 0;
multitiered_routing_threshold_1 = 50;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = UGAL_L_multi_tiered;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.44092,   0.46683,    512.32,    412.07,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24998,   0.24997,    69.917,    69.929,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36906,   0.36912,    256.34,    160.31,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,    0.4135,   0.41413,     831.3,     362.3,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.39331,   0.39324,    547.98,    263.15,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.38,     0.377,   0.37708,    501.67,    209.35,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.35,   0.34991,   0.34989,    93.268,    93.379,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.29991,   0.29991,    81.744,    81.781,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.19993,   0.19992,    42.895,    42.897,         0,  0,  0,  ,  ,  ,  
