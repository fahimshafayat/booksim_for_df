vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 0;
multitiered_routing_threshold_1 = 60;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = UGAL_L_multi_tiered;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.33189,   0.37218,    1424.4,    923.88,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25006,    74.067,    74.157,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.31522,   0.33593,    530.43,    434.57,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.29001,   0.29601,    463.38,    305.28,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27424,    0.2744,     475.4,    225.92,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,   0.25955,   0.25957,    185.97,    93.303,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.27,   0.26687,     0.267,    499.57,    166.22,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.2,   0.20009,   0.20009,    70.189,    70.232,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.15,   0.15012,   0.15011,     67.11,    67.132,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.1,   0.10012,   0.10012,    62.387,    62.392,         0,  0,  0,  ,  ,  ,  
