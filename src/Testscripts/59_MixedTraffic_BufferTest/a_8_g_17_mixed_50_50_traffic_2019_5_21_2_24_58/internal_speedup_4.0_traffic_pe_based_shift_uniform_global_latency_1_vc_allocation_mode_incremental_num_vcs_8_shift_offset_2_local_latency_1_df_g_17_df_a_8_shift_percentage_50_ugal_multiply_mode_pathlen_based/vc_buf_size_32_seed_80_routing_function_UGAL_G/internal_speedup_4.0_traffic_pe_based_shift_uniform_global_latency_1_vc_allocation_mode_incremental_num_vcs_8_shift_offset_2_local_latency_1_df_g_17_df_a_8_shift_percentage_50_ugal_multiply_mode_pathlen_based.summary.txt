injection_rate_uses_flits = 1;
vc_allocator = separable_input_first;
output_speedup = 1;
routing_delay = 0;
sample_period = 10000;
st_final_delay = 1;
wait_for_tail_credit = 0;
sw_alloc_delay = 1;
input_speedup = 1;
warmup_periods = 3;
alloc_iters = 1;
sw_allocator = separable_input_first;
priority = none;
credit_delay = 2;
packet_size = 1;
vc_alloc_delay = 1;
sim_count = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 1;
internal_speedup = 4.0;
local_latency = 1;
num_vcs = 8;
shift_offset = 2;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
routing_function = UGAL_G;
seed = 80;
vc_buf_size = 32;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50025,   0.50025,    17.828,     17.83,         0,      4.7709
simulation,         2,      0.74,   0.56379,   0.58437,    732.68,    306.39,         1,         0.0
simulation,         3,      0.62,   0.62007,    0.6201,    29.193,    29.196,         0,      4.8365
simulation,         4,      0.68,    0.5841,   0.59364,    730.98,    236.54,         1,         0.0
simulation,         5,      0.65,   0.62993,   0.63017,    576.34,    92.931,         1,         0.0
simulation,         6,      0.63,   0.62732,   0.62734,    343.39,    48.591,         0,      4.8469
simulation,         7,      0.64,   0.62777,   0.62791,    501.03,    77.871,         1,         0.0
simulation,         8,      0.15,   0.15021,   0.15021,    11.801,    11.802,         0,      4.5412
simulation,         9,       0.3,   0.30023,   0.30023,    13.291,    13.291,         0,      4.5958
simulation,        10,      0.45,   0.45022,   0.45022,    16.282,    16.283,         0,      4.7376
simulation,        11,       0.6,   0.60014,   0.60015,    24.734,    24.734,         0,      4.8261
