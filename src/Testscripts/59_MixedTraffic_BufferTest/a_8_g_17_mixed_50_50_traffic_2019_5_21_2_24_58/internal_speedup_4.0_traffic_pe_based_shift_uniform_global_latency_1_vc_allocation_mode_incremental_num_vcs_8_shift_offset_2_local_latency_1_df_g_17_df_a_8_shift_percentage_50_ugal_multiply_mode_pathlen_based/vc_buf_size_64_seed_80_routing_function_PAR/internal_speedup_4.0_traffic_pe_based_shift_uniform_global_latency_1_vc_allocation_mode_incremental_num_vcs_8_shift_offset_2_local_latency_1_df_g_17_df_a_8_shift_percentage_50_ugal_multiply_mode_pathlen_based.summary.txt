injection_rate_uses_flits = 1;
vc_allocator = separable_input_first;
output_speedup = 1;
routing_delay = 0;
sample_period = 10000;
st_final_delay = 1;
wait_for_tail_credit = 0;
sw_alloc_delay = 1;
input_speedup = 1;
warmup_periods = 3;
alloc_iters = 1;
sw_allocator = separable_input_first;
priority = none;
credit_delay = 2;
packet_size = 1;
vc_alloc_delay = 1;
sim_count = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 1;
internal_speedup = 4.0;
local_latency = 1;
num_vcs = 8;
shift_offset = 2;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
routing_function = PAR;
seed = 80;
vc_buf_size = 64;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50025,   0.50025,    23.155,    23.161,         0,      5.1612
simulation,         2,      0.74,   0.58313,   0.62177,    665.89,    474.07,         1,         0.0
simulation,         3,      0.62,   0.59553,   0.59509,    731.41,    216.84,         1,         0.0
simulation,         4,      0.56,   0.55932,   0.55933,    112.82,    47.316,         0,      5.1847
simulation,         5,      0.59,    0.5824,   0.58252,    510.71,    98.947,         1,         0.0
simulation,         6,      0.57,   0.56873,   0.56875,    231.84,    57.124,         0,      5.1942
simulation,         7,      0.58,    0.5759,   0.57593,    345.86,    73.753,         1,         0.0
simulation,         8,       0.1,   0.10017,   0.10017,    12.843,    12.843,         0,      5.1839
simulation,         9,       0.2,    0.2002,    0.2002,    13.294,    13.294,         0,      5.0619
simulation,        10,       0.3,   0.30023,   0.30023,    14.562,    14.563,         0,      5.0445
simulation,        11,       0.4,   0.40026,   0.40026,    17.139,     17.14,         0,      5.1177
simulation,        12,       0.5,   0.50025,   0.50025,    23.155,    23.161,         0,      5.1612
