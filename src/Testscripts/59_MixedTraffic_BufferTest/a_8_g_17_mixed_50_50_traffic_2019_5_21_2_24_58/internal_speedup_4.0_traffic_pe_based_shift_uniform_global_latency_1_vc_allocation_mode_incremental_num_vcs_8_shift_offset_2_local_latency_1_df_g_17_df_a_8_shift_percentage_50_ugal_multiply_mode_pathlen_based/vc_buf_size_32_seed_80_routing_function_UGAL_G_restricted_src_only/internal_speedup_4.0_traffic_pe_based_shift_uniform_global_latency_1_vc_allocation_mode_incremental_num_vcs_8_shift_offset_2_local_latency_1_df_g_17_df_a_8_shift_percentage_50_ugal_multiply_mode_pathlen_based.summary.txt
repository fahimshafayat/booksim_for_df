injection_rate_uses_flits = 1;
vc_allocator = separable_input_first;
output_speedup = 1;
routing_delay = 0;
sample_period = 10000;
st_final_delay = 1;
wait_for_tail_credit = 0;
sw_alloc_delay = 1;
input_speedup = 1;
warmup_periods = 3;
alloc_iters = 1;
sw_allocator = separable_input_first;
priority = none;
credit_delay = 2;
packet_size = 1;
vc_alloc_delay = 1;
sim_count = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 1;
internal_speedup = 4.0;
local_latency = 1;
num_vcs = 8;
shift_offset = 2;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
routing_function = UGAL_G_restricted_src_only;
seed = 80;
vc_buf_size = 32;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50025,   0.50025,    15.088,    15.088,         0,      4.3268
simulation,         2,      0.74,     0.548,   0.57202,    812.42,    334.93,         1,         0.0
simulation,         3,      0.62,    0.6201,    0.6201,    21.673,    21.675,         0,      4.3569
simulation,         4,      0.68,    0.6045,   0.61429,     497.3,    217.99,         1,         0.0
simulation,         5,      0.65,   0.65011,   0.65009,    31.337,    31.342,         0,      4.3659
simulation,         6,      0.66,   0.66017,   0.66016,    50.087,    50.136,         0,       4.371
simulation,         7,      0.67,   0.58218,   0.58712,    868.07,    265.95,         1,         0.0
simulation,         8,      0.15,   0.15021,   0.15021,    11.166,    11.166,         0,      4.2735
simulation,         9,       0.3,   0.30023,   0.30023,    12.151,    12.151,         0,      4.2731
simulation,        10,      0.45,   0.45022,   0.45022,    14.067,    14.068,         0,      4.3132
simulation,        11,       0.6,   0.60015,   0.60015,    19.467,    19.468,         0,      4.3517
