wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 33;
global_latency = 10;
local_latency = 10;
shift_by_group = 1;
topology = dragonflyfull;
vc_buf_size = 64;
df_wc_seed = 62;
routing_function = UGAL_L_restricted;
seed = 92;
traffic = df_wc;
ugal_multiply_mode = one_vs_two;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.30255,   0.36176,    542.25,    540.08,         1,         0.0
simulation,         2,      0.25,   0.24979,   0.24975,    78.372,    79.257,         0,      5.5019
simulation,         3,      0.37,   0.33973,   0.35281,    337.33,    318.21,         1,         0.0
simulation,         4,      0.31,   0.30941,   0.31004,    88.708,    90.704,         0,       5.545
simulation,         5,      0.34,   0.33316,   0.33847,    197.48,     235.9,         0,      5.5555
simulation,         6,      0.35,   0.33695,   0.34618,    302.83,    352.03,         0,      5.5572
simulation,         7,      0.36,   0.33719,   0.35028,    258.89,     249.4,         1,         0.0
simulation,         8,      0.05,  0.049962,  0.049936,    47.948,    47.994,         0,      4.9446
simulation,         9,       0.1,  0.099857,  0.099825,    68.712,    68.828,         0,      5.1524
simulation,        10,      0.15,   0.15011,   0.15005,    73.611,    73.953,         0,      5.3483
simulation,        11,       0.2,    0.1998,   0.19978,    76.315,    76.915,         0,      5.4436
simulation,        12,      0.25,   0.24979,   0.24975,    78.372,    79.257,         0,      5.5019
simulation,        13,       0.3,   0.30006,   0.30007,    81.681,    82.523,         0,      5.5399
simulation,        14,      0.35,   0.33695,   0.34618,    302.83,    352.03,         0,      5.5572
