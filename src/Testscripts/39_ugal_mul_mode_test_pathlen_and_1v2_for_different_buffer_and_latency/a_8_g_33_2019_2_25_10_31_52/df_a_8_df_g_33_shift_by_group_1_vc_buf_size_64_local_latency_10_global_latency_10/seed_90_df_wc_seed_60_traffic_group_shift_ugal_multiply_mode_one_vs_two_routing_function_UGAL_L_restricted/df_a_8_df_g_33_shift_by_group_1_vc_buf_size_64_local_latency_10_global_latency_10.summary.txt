wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 33;
global_latency = 10;
local_latency = 10;
shift_by_group = 1;
topology = dragonflyfull;
vc_buf_size = 64;
df_wc_seed = 60;
routing_function = UGAL_L_restricted;
seed = 90;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.36032,   0.39432,     469.2,    466.87,         1,         0.0
simulation,         2,      0.25,    0.2501,   0.25015,    74.507,    75.326,         0,       5.083
simulation,         3,      0.37,   0.34681,   0.35338,    303.37,    288.72,         1,         0.0
simulation,         4,      0.31,   0.31005,   0.31008,    78.067,    79.224,         0,      5.1083
simulation,         5,      0.34,   0.33345,   0.33858,    199.13,    207.46,         0,      5.1122
simulation,         6,      0.35,   0.33815,   0.34402,    322.79,    259.05,         0,      5.1117
simulation,         7,      0.36,   0.34185,   0.34803,    237.71,    234.28,         1,         0.0
simulation,         8,      0.05,  0.050066,  0.050075,    45.122,    45.151,         0,      4.6891
simulation,         9,       0.1,   0.10011,   0.10015,    65.769,    65.947,         0,      4.8506
simulation,        10,      0.15,   0.15003,   0.15008,    69.964,    70.375,         0,      4.9851
simulation,        11,       0.2,   0.20011,   0.20021,    72.526,    73.242,         0,      5.0486
simulation,        12,      0.25,    0.2501,   0.25015,    74.507,    75.326,         0,       5.083
simulation,        13,       0.3,   0.30008,   0.30006,    76.825,    77.905,         0,      5.1051
simulation,        14,      0.35,   0.33815,   0.34402,    322.79,    259.05,         0,      5.1117
