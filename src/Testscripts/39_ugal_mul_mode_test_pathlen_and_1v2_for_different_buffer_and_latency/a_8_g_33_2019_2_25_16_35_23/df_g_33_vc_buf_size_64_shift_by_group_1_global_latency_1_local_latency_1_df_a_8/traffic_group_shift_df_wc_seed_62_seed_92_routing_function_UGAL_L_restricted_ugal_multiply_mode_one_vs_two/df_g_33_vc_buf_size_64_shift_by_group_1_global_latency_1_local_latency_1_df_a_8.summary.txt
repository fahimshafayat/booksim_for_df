sample_period = 1000;
output_speedup = 1;
sim_count = 1;
routing_delay = 0;
priority = none;
warmup_periods = 3;
num_vcs = 7;
vc_allocator = separable_input_first;
vc_alloc_delay = 1;
sw_allocator = separable_input_first;
input_speedup = 1;
st_final_delay = 1;
internal_speedup = 4.0;
injection_rate_uses_flits = 1;
packet_size = 1;
credit_delay = 2;
sw_alloc_delay = 1;
wait_for_tail_credit = 0;
alloc_iters = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 33;
global_latency = 1;
local_latency = 1;
shift_by_group = 1;
topology = dragonflyfull;
vc_buf_size = 64;
df_wc_seed = 62;
routing_function = UGAL_L_restricted;
seed = 92;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.36209,   0.39549,    405.25,    402.92,         1,         0.0
simulation,         2,      0.25,   0.25003,      0.25,    28.114,    28.451,         0,      5.0833
simulation,         3,      0.37,   0.34855,   0.35505,    204.12,    199.84,         1,         0.0
simulation,         4,      0.31,   0.31004,   0.31004,    29.611,    30.051,         0,      5.1091
simulation,         5,      0.34,   0.33474,   0.33941,    123.09,    141.41,         0,      5.1153
simulation,         6,      0.35,   0.33948,   0.34574,    255.15,    200.63,         0,       5.114
simulation,         7,      0.36,   0.34428,   0.35037,    170.85,    165.41,         1,         0.0
simulation,         8,      0.05,  0.049932,  0.049936,    11.985,    11.986,         0,      4.5774
simulation,         9,       0.1,  0.099841,  0.099825,    33.258,     33.55,         0,      4.8359
simulation,        10,      0.15,   0.15006,   0.15005,    29.967,    30.308,         0,       4.977
simulation,        11,       0.2,   0.19978,   0.19978,    28.587,    28.971,         0,      5.0437
simulation,        12,      0.25,   0.25003,      0.25,    28.114,    28.451,         0,      5.0833
simulation,        13,       0.3,   0.30007,   0.30007,    28.841,    29.235,         0,      5.1056
simulation,        14,      0.35,   0.33948,   0.34574,    255.15,    200.63,         0,       5.114
