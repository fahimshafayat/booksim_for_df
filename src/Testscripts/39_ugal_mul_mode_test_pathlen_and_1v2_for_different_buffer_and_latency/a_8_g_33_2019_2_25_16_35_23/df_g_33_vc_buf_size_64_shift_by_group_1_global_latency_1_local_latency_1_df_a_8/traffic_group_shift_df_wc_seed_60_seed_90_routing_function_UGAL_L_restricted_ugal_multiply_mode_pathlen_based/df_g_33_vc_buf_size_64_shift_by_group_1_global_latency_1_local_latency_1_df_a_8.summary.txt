sample_period = 1000;
output_speedup = 1;
sim_count = 1;
routing_delay = 0;
priority = none;
warmup_periods = 3;
num_vcs = 7;
vc_allocator = separable_input_first;
vc_alloc_delay = 1;
sw_allocator = separable_input_first;
input_speedup = 1;
st_final_delay = 1;
internal_speedup = 4.0;
injection_rate_uses_flits = 1;
packet_size = 1;
credit_delay = 2;
sw_alloc_delay = 1;
wait_for_tail_credit = 0;
alloc_iters = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 33;
global_latency = 1;
local_latency = 1;
shift_by_group = 1;
topology = dragonflyfull;
vc_buf_size = 64;
df_wc_seed = 60;
routing_function = UGAL_L_restricted;
seed = 90;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35474,   0.39799,    414.11,    412.54,         1,         0.0
simulation,         2,      0.25,   0.25016,   0.25022,    25.533,    25.823,         0,      5.0609
simulation,         3,      0.37,   0.34878,   0.35615,    220.71,    206.38,         1,         0.0
simulation,         4,      0.31,   0.31005,   0.31008,    26.635,    26.896,         0,      5.0915
simulation,         5,      0.34,   0.33336,    0.3389,    135.14,     147.5,         0,         5.1
simulation,         6,      0.35,   0.33796,   0.34491,    251.87,    211.45,         0,      5.1009
simulation,         7,      0.36,   0.34352,   0.35001,     187.2,    176.32,         1,         0.0
simulation,         8,      0.05,  0.050046,  0.050075,    11.891,    11.892,         0,      4.5838
simulation,         9,       0.1,    0.1001,   0.10015,    31.387,    31.621,         0,      4.8194
simulation,        10,      0.15,   0.15005,   0.15008,    27.664,    27.918,         0,      4.9524
simulation,        11,       0.2,   0.20018,   0.20021,    26.169,    26.444,         0,      5.0175
simulation,        12,      0.25,   0.25016,   0.25022,    25.533,    25.823,         0,      5.0609
simulation,        13,       0.3,   0.30012,   0.30015,     26.12,    26.449,         0,      5.0872
simulation,        14,      0.35,   0.33796,   0.34491,    251.87,    211.45,         0,      5.1009
