wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 33;
global_latency = 10;
local_latency = 10;
shift_by_group = 1;
topology = dragonflyfull;
vc_buf_size = 256;
df_wc_seed = 62;
routing_function = UGAL_L;
seed = 92;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.42342,   0.49991,    433.08,    464.61,         1,         0.0
simulation,         2,      0.25,   0.25003,   0.25004,    113.47,    116.19,         0,      6.1586
simulation,         3,      0.37,   0.36955,   0.37006,    128.62,    133.04,         0,      6.2571
simulation,         4,      0.43,   0.42015,   0.42996,    228.99,    400.07,         0,      6.2606
simulation,         5,      0.46,   0.41751,   0.45996,     325.6,    335.65,         1,         0.0
simulation,         6,      0.44,   0.42156,   0.44002,    284.93,    607.41,         0,      6.2596
simulation,         7,      0.45,   0.42023,   0.44998,     258.3,    268.48,         1,         0.0
simulation,         8,      0.05,  0.049958,  0.049936,    52.056,    52.131,         0,      5.3049
simulation,         9,       0.1,  0.099903,  0.099957,    137.18,    139.26,         0,      5.6723
simulation,        10,      0.15,   0.14995,   0.14998,    123.11,     125.4,         0,      5.9444
simulation,        11,       0.2,   0.19987,   0.19991,    116.56,    119.02,         0,      6.0787
simulation,        12,      0.25,   0.25003,   0.25004,    113.47,    116.19,         0,      6.1586
simulation,        13,       0.3,   0.30013,   0.30016,    113.07,    116.15,         0,      6.2118
simulation,        14,      0.35,    0.3501,   0.35019,    118.38,    122.27,         0,      6.2479
simulation,        15,       0.4,   0.39737,   0.40004,    167.28,    179.47,         0,      6.2585
