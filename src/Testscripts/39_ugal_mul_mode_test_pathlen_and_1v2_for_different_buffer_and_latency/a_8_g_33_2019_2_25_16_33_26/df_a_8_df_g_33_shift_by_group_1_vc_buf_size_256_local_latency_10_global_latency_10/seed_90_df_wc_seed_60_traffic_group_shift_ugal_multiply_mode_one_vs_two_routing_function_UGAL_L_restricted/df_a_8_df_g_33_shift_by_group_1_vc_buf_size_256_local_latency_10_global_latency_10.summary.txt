wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 33;
global_latency = 10;
local_latency = 10;
shift_by_group = 1;
topology = dragonflyfull;
vc_buf_size = 256;
df_wc_seed = 60;
routing_function = UGAL_L_restricted;
seed = 90;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35621,   0.50013,    170.91,     496.1,         1,         0.0
simulation,         2,      0.25,   0.25012,   0.25012,    98.472,    101.17,         0,      5.0827
simulation,         3,      0.37,   0.35598,    0.3702,    201.75,    206.71,         1,         0.0
simulation,         4,      0.31,    0.3101,   0.31003,    97.257,    100.22,         0,       5.108
simulation,         5,      0.34,   0.33825,   0.34003,    129.17,    145.84,         0,      5.1145
simulation,         6,      0.35,   0.34514,   0.35006,    172.19,    251.91,         0,      5.1133
simulation,         7,      0.36,   0.35183,   0.36012,    164.72,    168.78,         1,         0.0
simulation,         8,      0.05,  0.050066,  0.050075,    45.122,    45.151,         0,      4.6891
simulation,         9,       0.1,       0.1,  0.099991,    125.74,    127.98,         0,      4.8516
simulation,        10,      0.15,   0.14997,   0.14996,    110.02,    112.42,         0,      4.9873
simulation,        11,       0.2,   0.20006,   0.20005,    102.53,    105.03,         0,      5.0482
simulation,        12,      0.25,   0.25012,   0.25012,    98.472,    101.17,         0,      5.0827
simulation,        13,       0.3,   0.30001,   0.29999,    96.916,      99.8,         0,      5.1051
simulation,        14,      0.35,   0.34514,   0.35006,    172.19,    251.91,         0,      5.1133
