wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 64;
perm_seed = 64;
routing_function = UGAL_L_restricted_src_only;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35491,   0.42106,    396.05,    371.84,         1,         0.0
simulation,         2,      0.25,   0.24981,   0.24981,    27.186,    27.256,         0,      5.4792
simulation,         3,      0.37,   0.35239,   0.35342,    356.67,    267.12,         1,         0.0
simulation,         4,      0.31,    0.3093,   0.30937,     119.7,    64.282,         0,      5.5254
simulation,         5,      0.34,   0.33326,   0.33391,    384.01,    144.13,         1,         0.0
simulation,         6,      0.32,   0.31827,   0.31832,     248.9,    69.615,         0,      5.5304
simulation,         7,      0.33,   0.32629,   0.32659,    248.33,    93.816,         1,         0.0
simulation,         8,       0.1,  0.099828,  0.099832,     32.13,     32.18,         0,      5.1338
simulation,         9,       0.2,   0.19983,   0.19983,    27.765,    27.827,         0,      5.4201
simulation,        10,       0.3,       0.3,   0.29997,    32.057,    31.996,         0,      5.5191
