wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 64;
perm_seed = 64;
routing_function = UGAL_L_multi_tiered_four_vs_five;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35921,   0.42377,    378.03,    353.39,         1,         0.0
simulation,         2,      0.25,   0.24981,   0.24981,    27.042,    27.114,         0,      5.4524
simulation,         3,      0.37,   0.35419,    0.3549,    404.16,    286.19,         1,         0.0
simulation,         4,      0.31,   0.30911,   0.30921,    128.95,    64.171,         0,      5.5244
simulation,         5,      0.34,   0.33305,    0.3334,    461.87,    165.55,         1,         0.0
simulation,         6,      0.32,   0.31827,   0.31833,     249.1,      68.7,         0,        5.53
simulation,         7,      0.33,   0.32592,   0.32663,    251.83,    104.26,         1,         0.0
simulation,         8,       0.1,  0.099829,  0.099832,    31.496,    31.549,         0,      4.6833
simulation,         9,       0.2,   0.19983,   0.19983,    27.097,    27.158,         0,      5.2471
simulation,        10,       0.3,   0.29987,   0.29985,    29.018,    29.201,         0,      5.5171
