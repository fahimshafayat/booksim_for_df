wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 64;
perm_seed = 64;
routing_function = UGAL_L_restricted_src_only;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.3239,   0.39679,    464.35,    440.84,         1,         0.0
simulation,         2,      0.25,   0.24996,   0.24996,    30.496,    30.573,         0,      5.3204
simulation,         3,      0.37,   0.35142,   0.35362,    363.82,    264.48,         1,         0.0
simulation,         4,      0.31,   0.31005,   0.31006,    31.346,    31.438,         0,      5.3831
simulation,         5,      0.34,    0.3401,   0.34014,    34.081,    34.205,         0,      5.4076
simulation,         6,      0.35,   0.34803,   0.34814,    202.08,    61.624,         0,      5.4123
simulation,         7,      0.36,   0.35183,   0.35236,    419.93,     161.0,         1,         0.0
simulation,         8,       0.1,  0.099974,  0.099975,    24.123,    24.157,         0,       4.938
simulation,         9,       0.2,   0.19991,   0.19991,    31.007,    31.073,         0,      5.2415
simulation,        10,       0.3,   0.29998,   0.29999,    31.034,    31.121,         0,      5.3746
