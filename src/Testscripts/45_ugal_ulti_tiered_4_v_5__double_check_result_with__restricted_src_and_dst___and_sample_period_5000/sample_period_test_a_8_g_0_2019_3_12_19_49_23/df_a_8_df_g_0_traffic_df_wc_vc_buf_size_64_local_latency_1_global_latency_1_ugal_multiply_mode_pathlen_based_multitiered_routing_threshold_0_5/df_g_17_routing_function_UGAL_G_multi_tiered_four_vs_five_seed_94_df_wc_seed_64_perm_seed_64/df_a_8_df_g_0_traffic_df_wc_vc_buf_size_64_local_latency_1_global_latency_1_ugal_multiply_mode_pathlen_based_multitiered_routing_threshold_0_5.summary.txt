wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 64;
perm_seed = 64;
routing_function = UGAL_G_multi_tiered_four_vs_five;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50027,   0.50021,     51.03,    51.057,         0,      5.0062
simulation,         2,      0.74,   0.47375,   0.55413,    724.11,    560.74,         1,         0.0
simulation,         3,      0.62,   0.49983,   0.53083,    734.79,    475.79,         1,         0.0
simulation,         4,      0.56,   0.51445,    0.5134,    742.74,    364.79,         1,         0.0
simulation,         5,      0.53,   0.52308,    0.5232,    366.21,    159.75,         1,         0.0
simulation,         6,      0.51,   0.51024,   0.51023,    55.921,    55.945,         0,      5.0477
simulation,         7,      0.52,   0.52022,   0.52018,    63.961,     63.97,         0,      5.1132
simulation,         8,       0.1,   0.10027,   0.10028,    11.556,    11.556,         0,      4.4342
simulation,         9,       0.2,    0.2001,   0.20011,    12.669,    12.669,         0,      4.5303
simulation,        10,       0.3,   0.30024,   0.30024,    14.276,    14.276,         0,      4.6302
simulation,        11,       0.4,   0.40038,   0.40036,    18.403,    18.405,         0,      4.7456
simulation,        12,       0.5,   0.50027,   0.50021,     51.03,    51.057,         0,      5.0062
