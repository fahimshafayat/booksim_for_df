We got results for UGAL_L_restricted_src_only and UGAL_L_multi_tiered_4_v_5 (Also Ugal_Gs)

Turns out, there is no performance difference between this two. (Folder 44).

However, results from folder 40 shows a different throughput (much lower) for UGAL_restricted.

The only differences we see are:

1. sample period (before 5K, now 10K), which shouldn't make a differnece.

2. Chances are, previous UGAL_restricted used UGAL_src_and_dst, which we know shows worse performance.


So we are doing two more runs.

traffic = df_wc, g = 9,17,25,33; everything else stays same.

1. Test 1 uses sample period of 5K

2. Test 2 uses UGAL_restricted_src_and_dst


