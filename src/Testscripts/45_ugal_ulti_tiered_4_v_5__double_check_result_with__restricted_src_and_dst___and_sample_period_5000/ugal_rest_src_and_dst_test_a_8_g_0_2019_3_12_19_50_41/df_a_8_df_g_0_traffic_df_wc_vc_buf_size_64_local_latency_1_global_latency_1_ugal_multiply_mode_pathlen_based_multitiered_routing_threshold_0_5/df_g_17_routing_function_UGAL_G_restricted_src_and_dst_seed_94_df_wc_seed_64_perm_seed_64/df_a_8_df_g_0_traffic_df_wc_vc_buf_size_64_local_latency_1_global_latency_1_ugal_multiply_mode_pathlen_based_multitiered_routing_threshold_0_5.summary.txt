wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 64;
perm_seed = 64;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49989,   0.49989,    26.699,      26.7,         0,       5.358
simulation,         2,      0.74,   0.48509,   0.52362,    1395.7,    671.86,         1,         0.0
simulation,         3,      0.62,   0.50878,    0.5365,    669.61,    441.45,         1,         0.0
simulation,         4,      0.56,   0.51825,   0.52613,     553.0,    278.71,         1,         0.0
simulation,         5,      0.53,   0.52371,   0.52371,    535.33,    147.64,         1,         0.0
simulation,         6,      0.51,   0.50992,   0.50992,    29.613,    29.614,         0,      5.3643
simulation,         7,      0.52,   0.51992,   0.51994,    35.404,    35.405,         0,      5.3715
simulation,         8,       0.1,  0.099814,  0.099819,    12.628,    12.628,         0,      4.9304
simulation,         9,       0.2,   0.19986,   0.19987,    13.872,    13.872,         0,      5.0484
simulation,        10,       0.3,   0.29984,   0.29985,     15.64,    15.641,         0,      5.1879
simulation,        11,       0.4,   0.39986,   0.39987,    18.368,    18.369,         0,      5.2899
simulation,        12,       0.5,   0.49989,   0.49989,    26.699,      26.7,         0,       5.358
