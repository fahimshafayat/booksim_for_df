wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 64;
perm_seed = 64;
routing_function = UGAL_L;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,     0.344,   0.38184,    1270.3,    830.06,         1,         0.0
simulation,         2,      0.25,   0.24997,   0.24998,    38.246,    38.329,         0,      6.0115
simulation,         3,      0.37,   0.33997,   0.34953,    653.92,     412.6,         1,         0.0
simulation,         4,      0.31,   0.30139,   0.30176,    588.93,    396.49,         1,         0.0
simulation,         5,      0.28,   0.27846,   0.27941,    329.19,    292.86,         0,      6.0619
simulation,         6,      0.29,   0.28558,   0.28666,    481.79,    385.32,         1,         0.0
simulation,         7,      0.05,  0.050004,  0.050002,    12.778,    12.778,         0,      5.0118
simulation,         8,       0.1,  0.099997,  0.099995,     26.76,    26.782,         0,      5.3705
simulation,         9,      0.15,   0.14998,   0.14998,    36.975,    37.012,         0,      5.6937
simulation,        10,       0.2,   0.20002,   0.20002,    36.708,    36.757,         0,      5.8908
simulation,        11,      0.25,   0.24997,   0.24998,    38.246,    38.329,         0,      6.0115
