wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 64;
perm_seed = 64;
routing_function = UGAL_G_multi_tiered_four_vs_five;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.40046,   0.43021,    688.98,    509.21,         1,         0.0
simulation,         2,      0.25,   0.24997,   0.24998,     13.94,     13.94,         0,      4.7386
simulation,         3,      0.37,   0.36991,   0.36992,    21.797,      21.8,         0,      4.8863
simulation,         4,      0.43,   0.40542,   0.40498,    916.83,    302.34,         1,         0.0
simulation,         5,       0.4,    0.3999,   0.39991,    37.606,    37.628,         0,       4.997
simulation,         6,      0.41,   0.40996,   0.40995,    49.131,     49.16,         0,      5.1196
simulation,         7,      0.42,   0.40723,   0.40698,    500.85,    201.28,         1,         0.0
simulation,         8,       0.1,  0.099997,  0.099995,     12.01,     12.01,         0,      4.5694
simulation,         9,       0.2,   0.20002,   0.20002,    13.128,    13.129,         0,      4.6834
simulation,        10,       0.3,   0.29995,   0.29995,    15.159,     15.16,         0,       4.795
simulation,        11,       0.4,    0.3999,   0.39991,    37.606,    37.628,         0,       4.997
