wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 64;
perm_seed = 64;
routing_function = UGAL_L;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.38175,   0.41723,    1007.0,    692.05,         1,         0.0
simulation,         2,      0.25,   0.24984,   0.24984,    42.658,    42.723,         0,      5.8005
simulation,         3,      0.37,   0.36074,   0.36819,    498.65,    476.25,         1,         0.0
simulation,         4,      0.31,   0.30989,    0.3099,    46.364,    46.466,         0,       5.936
simulation,         5,      0.34,   0.33988,   0.33989,    52.683,    52.853,         0,      5.9863
simulation,         6,      0.35,   0.34989,   0.34994,    65.314,     65.59,         0,      6.0007
simulation,         7,      0.36,   0.35785,   0.35929,     356.7,    319.46,         0,      6.0119
simulation,         8,       0.1,  0.099814,  0.099819,    13.614,    13.615,         0,      5.0069
simulation,         9,       0.2,   0.19986,   0.19987,    41.791,    41.839,         0,        5.63
simulation,        10,       0.3,   0.29985,   0.29985,    45.399,    45.493,         0,      5.9167
