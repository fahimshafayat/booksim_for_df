vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 62;
routing_function = UGAL_L_threshold;
routing_threshold = 60;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010042,  0.010042,    14.108,    14.111,         0
simulation,         2,      0.99,   0.61205,   0.62901,    762.69,    660.13,         1
simulation,         3,       0.5,   0.50024,   0.50027,    24.888,    24.994,         0
simulation,         4,      0.74,   0.61945,   0.63448,    352.03,    427.08,         1
simulation,         5,      0.62,   0.60984,   0.61255,    196.72,    126.48,         0
simulation,         6,      0.68,   0.61975,   0.62747,    425.21,    308.27,         1
simulation,         7,      0.65,   0.61798,   0.62401,    256.95,    205.39,         1
simulation,         8,      0.63,   0.61395,   0.61764,    277.15,    159.02,         0
simulation,         9,      0.64,   0.61669,   0.62143,    378.92,    200.44,         0
simulation,        10,       0.6,   0.59682,   0.59823,    90.995,    75.607,         0
simulation,        11,      0.55,   0.55027,   0.55032,    33.964,    34.203,         0
simulation,        13,      0.45,   0.45017,   0.45019,    20.133,    20.174,         0
