vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 66;
routing_function = UGAL_L_threshold;
routing_threshold = 40;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010044,  0.010042,    14.113,    14.116,         0
simulation,         2,      0.99,   0.60975,   0.62787,    766.29,    663.92,         1
simulation,         3,       0.5,   0.50026,   0.50027,    24.074,    24.181,         0
simulation,         4,      0.74,   0.61745,   0.63466,    369.61,    438.13,         1
simulation,         5,      0.62,   0.61117,   0.61429,    171.89,     122.0,         0
simulation,         6,      0.68,    0.6183,   0.62642,    438.63,    315.38,         1
simulation,         7,      0.65,   0.61733,   0.62387,    267.12,    214.41,         1
simulation,         8,      0.63,   0.61398,   0.61869,    263.76,    167.74,         0
simulation,         9,      0.64,   0.61611,   0.62187,    370.26,    206.78,         0
simulation,        10,       0.6,   0.59816,   0.59904,    73.117,    65.113,         0
simulation,        11,      0.55,   0.55036,   0.55032,    32.115,    32.307,         0
simulation,        13,      0.45,   0.45016,   0.45019,    19.966,    20.007,         0
