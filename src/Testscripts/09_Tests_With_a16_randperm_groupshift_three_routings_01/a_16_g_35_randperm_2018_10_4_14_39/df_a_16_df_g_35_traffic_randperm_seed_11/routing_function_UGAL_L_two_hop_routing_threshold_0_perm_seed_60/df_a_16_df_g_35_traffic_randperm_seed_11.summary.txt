vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 60;
routing_function = UGAL_L_two_hop;
routing_threshold = 0;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010044,  0.010042,    14.119,    14.121,         0
simulation,         2,      0.99,   0.61299,   0.63061,    744.24,    646.37,         1
simulation,         3,       0.5,   0.50026,   0.50027,    24.524,    24.628,         0
simulation,         4,      0.74,   0.62043,   0.63249,    457.65,    426.12,         1
simulation,         5,      0.62,   0.60983,   0.61268,    194.06,     122.6,         0
simulation,         6,      0.68,   0.62019,   0.62741,    417.24,    299.47,         1
simulation,         7,      0.65,   0.61842,   0.62396,    252.33,    200.37,         1
simulation,         8,      0.63,   0.61371,    0.6175,    277.73,    156.35,         0
simulation,         9,      0.64,   0.61673,   0.62141,    376.94,     191.3,         0
simulation,        10,       0.6,   0.59661,   0.59794,    92.432,    74.023,         0
simulation,        11,      0.55,   0.55021,   0.55032,     32.85,    33.084,         0
simulation,        13,      0.45,   0.45019,   0.45019,    20.232,    20.277,         0
