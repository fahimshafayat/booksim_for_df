vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 68;
routing_function = UGAL_L_threshold;
routing_threshold = 100;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010044,  0.010042,     14.11,    14.112,         0
simulation,         2,      0.99,   0.61945,    0.6371,    751.18,     650.5,         1
simulation,         3,       0.5,   0.50023,   0.50027,    23.609,    23.691,         0
simulation,         4,      0.74,   0.62544,   0.63933,    468.26,    433.17,         1
simulation,         5,      0.62,   0.61316,   0.61559,    141.32,    103.65,         0
simulation,         6,      0.68,   0.62608,   0.63455,    396.94,    293.92,         1
simulation,         7,      0.65,   0.62389,   0.62923,    226.27,     185.5,         1
simulation,         8,      0.63,    0.6181,   0.62155,    209.66,    137.53,         0
simulation,         9,      0.64,   0.62165,   0.62613,    297.48,    174.17,         0
simulation,        10,       0.6,   0.59871,   0.59932,    65.062,    58.872,         0
simulation,        11,      0.55,   0.55019,    0.5503,    31.966,    32.032,         0
simulation,        13,      0.45,   0.45017,   0.45019,    19.873,    19.914,         0
