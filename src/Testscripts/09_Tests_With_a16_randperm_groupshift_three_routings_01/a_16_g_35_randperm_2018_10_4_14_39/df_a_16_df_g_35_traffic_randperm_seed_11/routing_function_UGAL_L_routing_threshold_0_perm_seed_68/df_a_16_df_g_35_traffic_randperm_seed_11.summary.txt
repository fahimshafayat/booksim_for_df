vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 68;
routing_function = UGAL_L;
routing_threshold = 0;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010045,  0.010042,    14.149,    14.151,         0
simulation,         2,      0.99,   0.60845,   0.62511,    777.52,    671.15,         1
simulation,         3,       0.5,   0.50026,   0.50027,    24.704,    24.816,         0
simulation,         4,      0.74,   0.61502,   0.63172,     382.4,    452.54,         1
simulation,         5,      0.62,   0.60656,   0.61013,    238.55,     148.0,         0
simulation,         6,      0.68,    0.6155,   0.62408,    470.38,    338.26,         1
simulation,         7,      0.65,   0.61373,   0.62056,    290.18,    231.31,         1
simulation,         8,      0.63,   0.61046,   0.61462,    327.52,    180.87,         0
simulation,         9,      0.64,   0.61246,    0.6183,    237.52,    195.17,         1
simulation,        10,       0.6,   0.59558,   0.59736,    109.92,    87.136,         0
simulation,        11,      0.55,   0.55023,   0.55032,    34.472,    34.689,         0
simulation,        13,      0.45,   0.45018,   0.45019,     20.33,    20.382,         0
