sw_allocator = separable_input_first;
vc_buf_size = 64;
alloc_iters = 1;
sw_alloc_delay = 1;
priority = none;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
credit_delay = 2;
vc_allocator = separable_input_first;
sim_count = 1;
internal_speedup = 2.0;
num_vcs = 7;
wait_for_tail_credit = 0;
output_speedup = 1;
input_speedup = 1;
packet_size = 1;
sample_period = 1000;
injection_rate_uses_flits = 1;
st_final_delay = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L;
routing_threshold = 0;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010042,  0.010042,    15.559,    15.563,         0
simulation,         2,      0.99,   0.16398,   0.40061,    641.04,    637.73,         1
simulation,         3,       0.5,   0.20554,   0.43884,     349.3,    349.27,         1
simulation,         4,      0.25,   0.15528,   0.24976,    182.85,    757.31,         1
simulation,         5,      0.13,   0.13004,   0.13012,    67.577,    68.838,         0
simulation,         6,      0.19,   0.15487,   0.19006,    447.96,    481.53,         1
simulation,         7,      0.16,   0.15224,   0.16004,    307.23,    618.77,         0
simulation,         8,      0.17,   0.15288,      0.17,    368.85,    374.98,         1
simulation,         9,      0.15,   0.14775,   0.15008,    151.86,    241.11,         0
simulation,        10,       0.1,   0.10013,   0.10014,    57.597,     58.28,         0
simulation,        11,      0.05,  0.050096,  0.050139,    23.486,    23.538,         0
