sw_allocator = separable_input_first;
vc_buf_size = 64;
alloc_iters = 1;
sw_alloc_delay = 1;
priority = none;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
credit_delay = 2;
vc_allocator = separable_input_first;
sim_count = 1;
internal_speedup = 2.0;
num_vcs = 7;
wait_for_tail_credit = 0;
output_speedup = 1;
input_speedup = 1;
packet_size = 1;
sample_period = 1000;
injection_rate_uses_flits = 1;
st_final_delay = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L;
routing_threshold = 0;
shift_by_group = 9;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010042,  0.010042,    15.568,     15.57,         0
simulation,         2,      0.99,    0.1637,   0.40028,    640.41,    636.82,         1
simulation,         3,       0.5,   0.20374,   0.43704,    351.26,    351.24,         1
simulation,         4,      0.25,   0.15992,   0.24963,     437.1,     437.1,         1
simulation,         5,      0.13,   0.12995,   0.13012,     70.61,    73.343,         0
simulation,         6,      0.19,   0.15371,      0.19,    462.66,    500.61,         1
simulation,         7,      0.16,   0.15108,   0.16004,    235.68,    240.79,         1
simulation,         8,      0.14,   0.13926,   0.14011,    95.874,    126.59,         0
simulation,         9,      0.15,   0.14693,   0.15008,    178.05,    339.57,         0
simulation,        11,       0.1,   0.10006,   0.10005,    57.436,    58.316,         0
simulation,        12,      0.05,  0.050096,  0.050139,    24.177,    24.238,         0
