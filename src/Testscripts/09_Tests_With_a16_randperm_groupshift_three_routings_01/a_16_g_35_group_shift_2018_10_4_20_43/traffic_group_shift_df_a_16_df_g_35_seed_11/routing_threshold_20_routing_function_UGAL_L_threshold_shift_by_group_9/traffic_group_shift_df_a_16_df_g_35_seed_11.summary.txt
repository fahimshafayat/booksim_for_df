sw_allocator = separable_input_first;
vc_buf_size = 64;
alloc_iters = 1;
sw_alloc_delay = 1;
priority = none;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
credit_delay = 2;
vc_allocator = separable_input_first;
sim_count = 1;
internal_speedup = 2.0;
num_vcs = 7;
wait_for_tail_credit = 0;
output_speedup = 1;
input_speedup = 1;
packet_size = 1;
sample_period = 1000;
injection_rate_uses_flits = 1;
st_final_delay = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L_threshold;
routing_threshold = 20;
shift_by_group = 9;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010043,  0.010042,    15.429,    15.433,         0
simulation,         2,      0.99,   0.16396,    0.4004,     641.5,    638.06,         1
simulation,         3,       0.5,   0.20359,   0.43706,    351.18,    351.16,         1
simulation,         4,      0.25,   0.15332,   0.24975,    190.22,    759.74,         1
simulation,         5,      0.13,   0.13009,   0.13012,    64.681,    65.692,         0
simulation,         6,      0.19,    0.1603,   0.18951,    468.77,    480.26,         1
simulation,         7,      0.16,   0.15681,   0.16004,     157.8,    279.18,         0
simulation,         8,      0.17,   0.16114,   0.17001,    203.15,    207.19,         1
simulation,         9,      0.15,   0.14954,   0.15011,    85.339,    103.94,         0
simulation,        10,       0.1,   0.10013,   0.10014,    56.822,    57.486,         0
simulation,        11,      0.05,  0.050111,  0.050139,    22.592,    22.626,         0
