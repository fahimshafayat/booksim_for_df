sw_allocator = separable_input_first;
vc_buf_size = 64;
alloc_iters = 1;
sw_alloc_delay = 1;
priority = none;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
credit_delay = 2;
vc_allocator = separable_input_first;
sim_count = 1;
internal_speedup = 2.0;
num_vcs = 7;
wait_for_tail_credit = 0;
output_speedup = 1;
input_speedup = 1;
packet_size = 1;
sample_period = 1000;
injection_rate_uses_flits = 1;
st_final_delay = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L_threshold;
routing_threshold = 50;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010044,  0.010042,    15.415,    15.418,         0
simulation,         2,      0.99,   0.16527,   0.40178,    643.35,    639.68,         1
simulation,         3,       0.5,   0.20068,   0.43521,    362.98,    362.95,         1
simulation,         4,      0.25,   0.17226,    0.2499,    147.46,    547.51,         1
simulation,         5,      0.13,   0.12998,   0.13012,    72.255,    73.956,         0
simulation,         6,      0.19,    0.1718,   0.18935,     371.5,    381.97,         1
simulation,         7,      0.16,   0.15671,   0.16004,    181.39,    285.51,         0
simulation,         8,      0.17,   0.16315,   0.17001,     292.6,    511.45,         0
simulation,         9,      0.18,   0.16786,   0.17989,    274.68,    282.48,         1
simulation,        10,      0.15,   0.14901,   0.15008,    112.34,    143.32,         0
simulation,        11,       0.1,   0.10011,   0.10014,    57.077,    57.685,         0
simulation,        12,      0.05,  0.050111,  0.050139,    22.308,    22.338,         0
