sw_allocator = separable_input_first;
vc_buf_size = 64;
alloc_iters = 1;
sw_alloc_delay = 1;
priority = none;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
credit_delay = 2;
vc_allocator = separable_input_first;
sim_count = 1;
internal_speedup = 2.0;
num_vcs = 7;
wait_for_tail_credit = 0;
output_speedup = 1;
input_speedup = 1;
packet_size = 1;
sample_period = 1000;
injection_rate_uses_flits = 1;
st_final_delay = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L_threshold;
routing_threshold = 40;
shift_by_group = 9;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010043,  0.010042,    15.429,    15.433,         0
simulation,         2,      0.99,   0.16599,   0.40234,    642.54,     638.5,         1
simulation,         3,       0.5,   0.20241,   0.43628,    356.94,    356.92,         1
simulation,         4,      0.25,   0.16678,   0.24992,    168.34,    579.49,         1
simulation,         5,      0.13,      0.13,   0.13012,    72.612,    74.134,         0
simulation,         6,      0.19,    0.1732,    0.1894,    333.62,    342.89,         1
simulation,         7,      0.16,   0.15767,   0.16004,    150.97,    221.03,         0
simulation,         8,      0.17,   0.16429,   0.17001,    252.52,    459.37,         0
simulation,         9,      0.18,   0.16971,   0.17996,    236.74,    243.54,         1
simulation,        10,      0.15,   0.14933,   0.15011,    99.322,     123.2,         0
simulation,        11,       0.1,    0.1001,   0.10014,    57.321,    57.958,         0
simulation,        12,      0.05,  0.050108,  0.050139,    22.888,    22.921,         0
