sw_allocator = separable_input_first;
vc_buf_size = 64;
alloc_iters = 1;
sw_alloc_delay = 1;
priority = none;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
credit_delay = 2;
vc_allocator = separable_input_first;
sim_count = 1;
internal_speedup = 2.0;
num_vcs = 7;
wait_for_tail_credit = 0;
output_speedup = 1;
input_speedup = 1;
packet_size = 1;
sample_period = 1000;
injection_rate_uses_flits = 1;
st_final_delay = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L_threshold;
routing_threshold = 60;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010042,  0.010042,    15.444,    15.448,         0
simulation,         2,      0.99,   0.16613,   0.40252,    644.98,    641.45,         1
simulation,         3,       0.5,   0.20358,    0.4379,    358.96,    358.93,         1
simulation,         4,      0.25,     0.177,   0.24989,    133.04,    497.22,         1
simulation,         5,      0.13,   0.12991,   0.13012,    77.987,    79.574,         0
simulation,         6,      0.19,   0.17252,   0.18943,    361.42,    372.67,         1
simulation,         7,      0.16,   0.15724,   0.16004,    172.38,    255.63,         0
simulation,         8,      0.17,   0.16365,   0.17001,    282.29,    462.97,         0
simulation,         9,      0.18,   0.16868,   0.17994,    264.44,    273.23,         1
simulation,        10,      0.15,   0.14903,   0.15008,    116.45,    145.26,         0
simulation,        11,       0.1,    0.1001,   0.10014,    57.412,    57.984,         0
simulation,        12,      0.05,  0.050117,  0.050139,    22.293,    22.315,         0
