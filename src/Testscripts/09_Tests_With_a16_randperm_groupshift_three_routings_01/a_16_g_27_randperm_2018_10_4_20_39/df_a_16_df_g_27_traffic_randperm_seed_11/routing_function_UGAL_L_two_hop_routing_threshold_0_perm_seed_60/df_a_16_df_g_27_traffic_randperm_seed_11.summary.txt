vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 27;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 60;
routing_function = UGAL_L_two_hop;
routing_threshold = 0;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010039,  0.010039,     14.07,    14.072,         0
simulation,         2,      0.99,   0.63479,   0.65309,    726.45,    621.02,         1
simulation,         3,       0.5,   0.50013,   0.50016,    20.623,    20.658,         0
simulation,         4,      0.74,   0.64581,   0.65716,    459.21,    376.45,         1
simulation,         5,      0.62,   0.61824,    0.6194,    63.968,    61.515,         0
simulation,         6,      0.68,    0.6426,   0.64937,    289.73,    225.28,         1
simulation,         7,      0.65,   0.63636,   0.64033,    222.23,    144.42,         0
simulation,         8,      0.66,   0.63928,   0.64462,     310.0,    184.98,         0
simulation,         9,      0.67,   0.64119,   0.64762,    241.19,    194.97,         1
simulation,        11,       0.6,   0.59975,   0.60007,    40.534,    40.926,         0
simulation,        12,      0.55,   0.55013,   0.55012,    26.297,    26.419,         0
