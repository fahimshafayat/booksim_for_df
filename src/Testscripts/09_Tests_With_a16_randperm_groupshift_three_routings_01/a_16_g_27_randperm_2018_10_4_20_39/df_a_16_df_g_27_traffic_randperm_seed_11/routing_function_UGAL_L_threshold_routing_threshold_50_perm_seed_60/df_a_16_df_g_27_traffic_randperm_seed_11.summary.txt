vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 27;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 60;
routing_function = UGAL_L_threshold;
routing_threshold = 50;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010039,  0.010039,     14.07,    14.072,         0
simulation,         2,      0.99,   0.63432,   0.65381,    731.08,    622.63,         1
simulation,         3,       0.5,   0.50013,   0.50016,    20.544,    20.581,         0
simulation,         4,      0.74,   0.64411,   0.65545,    470.57,    384.91,         1
simulation,         5,      0.62,   0.61821,   0.61926,    65.635,    61.582,         0
simulation,         6,      0.68,   0.64204,   0.64866,    299.29,    230.49,         1
simulation,         7,      0.65,   0.63608,   0.64034,    223.22,     148.8,         0
simulation,         8,      0.66,   0.63836,    0.6441,    322.69,     197.7,         0
simulation,         9,      0.67,   0.64023,   0.64665,    248.07,    199.08,         1
simulation,        11,       0.6,   0.59994,   0.60007,    40.731,    41.098,         0
simulation,        12,      0.55,    0.5501,   0.55012,    26.392,    26.514,         0
