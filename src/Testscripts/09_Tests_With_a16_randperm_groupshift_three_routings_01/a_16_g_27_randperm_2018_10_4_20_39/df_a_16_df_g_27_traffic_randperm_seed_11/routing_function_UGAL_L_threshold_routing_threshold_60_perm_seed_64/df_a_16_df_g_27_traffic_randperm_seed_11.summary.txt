vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 27;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 64;
routing_function = UGAL_L_threshold;
routing_threshold = 60;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010038,  0.010039,    14.041,    14.045,         0
simulation,         2,      0.99,   0.64104,   0.66053,    722.97,    620.14,         1
simulation,         3,       0.5,   0.50015,   0.50016,    20.777,    20.823,         0
simulation,         4,      0.74,   0.64893,   0.66117,    451.31,    373.98,         1
simulation,         5,      0.62,   0.61773,   0.61902,    68.503,    63.482,         0
simulation,         6,      0.68,   0.64596,   0.65246,    273.13,    213.03,         1
simulation,         7,      0.65,   0.63691,   0.64063,    216.82,    142.13,         0
simulation,         8,      0.66,   0.64105,   0.64581,    289.03,    174.64,         0
simulation,         9,      0.67,   0.64393,    0.6495,    222.13,    179.87,         1
simulation,        11,       0.6,   0.59949,       0.6,    42.472,      43.1,         0
simulation,        12,      0.55,   0.55013,   0.55012,    26.662,    26.776,         0
