vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 27;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 68;
routing_function = UGAL_L_threshold;
routing_threshold = 50;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010038,  0.010039,    14.057,    14.059,         0
simulation,         2,      0.99,    0.6364,   0.65418,    724.76,    618.08,         1
simulation,         3,       0.5,   0.50015,   0.50016,    21.302,    21.359,         0
simulation,         4,      0.74,   0.64121,   0.65423,    398.34,    376.24,         1
simulation,         5,      0.62,    0.6154,   0.61739,    109.96,    88.983,         0
simulation,         6,      0.68,    0.6372,   0.64345,    319.56,    239.63,         1
simulation,         7,      0.65,   0.63069,   0.63486,    307.61,    173.42,         0
simulation,         8,      0.66,    0.6338,   0.63886,    218.69,    176.77,         1
simulation,        10,       0.6,   0.59915,   0.59978,    50.664,    50.639,         0
simulation,        11,      0.55,   0.55009,   0.55012,    28.903,    29.067,         0
