vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L_threshold;
routing_threshold = 60;
shift_by_group = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010003,  0.010006,    15.012,    15.016,         0
simulation,         2,      0.99,    0.1702,   0.41733,    656.72,    656.17,         1
simulation,         3,       0.5,   0.19009,   0.43642,    407.14,    407.14,         1
simulation,         4,      0.25,   0.15438,   0.24999,    531.81,    531.81,         1
simulation,         5,      0.13,    0.1299,   0.12993,    77.862,    78.958,         0
simulation,         6,      0.19,   0.16056,   0.18988,    448.89,    480.21,         1
simulation,         7,      0.16,   0.15619,   0.15994,    228.24,    330.09,         0
simulation,         8,      0.17,   0.15975,   0.16992,    308.13,     314.8,         1
simulation,         9,      0.15,   0.14896,   0.14993,    131.82,     143.5,         0
simulation,        10,       0.1,  0.099851,  0.099889,    61.742,    62.216,         0
simulation,        11,      0.05,   0.04998,  0.049976,    17.356,     17.36,         0
