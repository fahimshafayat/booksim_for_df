vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L_threshold;
routing_threshold = 40;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010002,  0.010006,    15.009,    15.013,         0
simulation,         2,      0.99,   0.17068,   0.41818,    655.91,    655.37,         1
simulation,         3,       0.5,    0.1888,   0.43572,    411.21,     411.2,         1
simulation,         4,      0.25,   0.15104,   0.24999,    555.46,    555.46,         1
simulation,         5,      0.13,   0.12992,   0.12993,    77.263,    78.371,         0
simulation,         6,      0.19,   0.16007,   0.18988,    439.57,    468.18,         1
simulation,         7,      0.16,   0.15688,   0.15994,    207.82,    291.77,         0
simulation,         8,      0.17,   0.16004,   0.16992,    287.42,    293.45,         1
simulation,         9,      0.15,   0.14921,   0.14993,    125.36,    136.46,         0
simulation,        10,       0.1,  0.099867,  0.099889,    61.514,    61.978,         0
simulation,        11,      0.05,  0.049983,  0.049976,    17.341,    17.346,         0
