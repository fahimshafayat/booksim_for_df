vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L_two_hop;
routing_threshold = 0;
shift_by_group = 19;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010002,  0.010006,    14.983,    14.985,         0
simulation,         2,      0.99,   0.16947,   0.41749,     676.7,    675.95,         1
simulation,         3,       0.5,   0.19369,   0.44015,    409.17,    409.16,         1
simulation,         4,      0.25,   0.15667,   0.24999,     542.0,     542.0,         1
simulation,         5,      0.13,   0.12992,   0.12993,    78.414,    79.489,         0
simulation,         6,      0.19,   0.15671,   0.18986,    499.99,    536.85,         1
simulation,         7,      0.16,   0.15358,   0.15993,    307.61,    502.71,         0
simulation,         8,      0.17,   0.15588,    0.1699,    369.04,    374.85,         1
simulation,         9,      0.15,    0.1478,   0.14995,    165.66,    236.86,         0
simulation,        10,       0.1,  0.099867,  0.099889,    61.449,    61.893,         0
simulation,        11,      0.05,   0.04998,  0.049976,    17.235,    17.239,         0
