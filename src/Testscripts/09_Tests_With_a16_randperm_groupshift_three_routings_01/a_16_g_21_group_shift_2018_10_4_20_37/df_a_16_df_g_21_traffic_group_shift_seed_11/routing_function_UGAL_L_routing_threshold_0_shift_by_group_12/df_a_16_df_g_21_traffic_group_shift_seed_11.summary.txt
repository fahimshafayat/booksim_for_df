vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L;
routing_threshold = 0;
shift_by_group = 12;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010003,  0.010006,    15.034,    15.036,         0
simulation,         2,      0.99,   0.17009,    0.4176,     659.7,    659.18,         1
simulation,         3,       0.5,   0.18839,   0.43532,    413.11,    413.11,         1
simulation,         4,      0.25,   0.14781,   0.24999,    599.36,    599.36,         1
simulation,         5,      0.13,   0.12994,   0.12993,    79.942,    81.188,         0
simulation,         6,      0.19,   0.15362,   0.18979,    446.01,    511.35,         1
simulation,         7,      0.16,   0.15129,   0.15994,    369.58,    662.05,         0
simulation,         8,      0.17,   0.15291,   0.16992,    436.67,    442.24,         1
simulation,         9,      0.15,   0.14765,   0.14995,    171.26,    220.39,         0
simulation,        10,       0.1,  0.099869,  0.099889,    62.601,      63.1,         0
simulation,        11,      0.05,  0.049975,  0.049976,    17.421,    17.425,         0
