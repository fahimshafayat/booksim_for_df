vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 70;
routing_function = UGAL_L_two_hop;
routing_threshold = 0;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010001,  0.010006,    13.982,    13.984,         0
simulation,         2,      0.99,   0.65116,   0.67071,    706.09,    596.36,         1
simulation,         3,       0.5,   0.49978,   0.49978,    20.554,    20.611,         0
simulation,         4,      0.74,   0.65499,   0.66477,    468.81,    354.75,         1
simulation,         5,      0.62,   0.61507,    0.6162,    109.17,    76.591,         0
simulation,         6,      0.68,   0.64692,   0.65231,    258.85,    198.25,         1
simulation,         7,      0.65,   0.63463,   0.63753,    262.91,    140.56,         0
simulation,         8,      0.66,   0.63926,    0.6432,    340.87,    169.65,         0
simulation,         9,      0.67,    0.6431,   0.64761,    217.39,    172.23,         1
simulation,        11,       0.6,   0.59813,   0.59911,    54.194,    50.684,         0
simulation,        12,      0.55,   0.54967,   0.54975,    25.939,    26.062,         0
