vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 70;
routing_function = UGAL_L_threshold;
routing_threshold = 50;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010001,  0.010006,    13.982,    13.984,         0
simulation,         2,      0.99,   0.65106,   0.66905,    706.62,    596.62,         1
simulation,         3,       0.5,   0.49974,   0.49978,     20.62,    20.674,         0
simulation,         4,      0.74,   0.65355,   0.66357,     474.4,     355.3,         1
simulation,         5,      0.62,   0.61468,   0.61613,    113.89,      79.8,         0
simulation,         6,      0.68,    0.6454,   0.65095,    265.42,    203.12,         1
simulation,         7,      0.65,   0.63383,   0.63711,     270.2,    143.28,         0
simulation,         8,      0.66,   0.63853,   0.64263,    346.29,    176.61,         0
simulation,         9,      0.67,    0.6422,   0.64709,    221.24,    175.56,         1
simulation,        11,       0.6,   0.59817,   0.59897,    57.421,    50.861,         0
simulation,        12,      0.55,   0.54971,   0.54975,    25.858,    26.006,         0
