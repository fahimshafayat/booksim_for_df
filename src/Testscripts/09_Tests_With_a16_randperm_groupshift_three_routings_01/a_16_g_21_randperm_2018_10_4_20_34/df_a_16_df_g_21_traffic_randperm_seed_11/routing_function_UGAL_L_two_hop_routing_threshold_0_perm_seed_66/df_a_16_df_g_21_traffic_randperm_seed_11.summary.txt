vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 66;
routing_function = UGAL_L_two_hop;
routing_threshold = 0;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010001,  0.010006,    13.949,    13.951,         0
simulation,         2,      0.99,   0.66356,   0.68385,    681.06,    576.88,         1
simulation,         3,       0.5,   0.49976,   0.49978,     19.35,    19.376,         0
simulation,         4,      0.74,   0.66983,   0.67876,    447.01,    319.96,         1
simulation,         5,      0.62,   0.61815,   0.61876,    57.279,    50.119,         0
simulation,         6,      0.68,   0.65825,    0.6628,    321.03,    182.91,         0
simulation,         7,      0.71,   0.66656,    0.6732,    329.38,    241.52,         1
simulation,         8,      0.69,   0.66152,   0.66657,    232.95,    182.28,         1
simulation,         9,      0.65,   0.64243,   0.64449,    139.36,    97.133,         0
simulation,        10,       0.6,   0.59915,   0.59969,    36.742,    37.907,         0
simulation,        11,      0.55,   0.54976,   0.54975,     23.46,    23.528,         0
