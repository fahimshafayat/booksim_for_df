vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 60;
routing_function = UGAL_L_threshold;
routing_threshold = 20;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010003,  0.010006,    13.999,    14.001,         0
simulation,         2,      0.99,   0.65461,   0.67402,    699.35,    593.18,         1
simulation,         3,       0.5,   0.49978,   0.49978,    20.022,    20.055,         0
simulation,         4,      0.74,   0.65756,   0.66709,    452.13,    342.05,         1
simulation,         5,      0.62,   0.61715,   0.61848,    72.202,     64.32,         0
simulation,         6,      0.68,   0.64777,   0.65331,    253.23,    195.29,         1
simulation,         7,      0.65,   0.63701,   0.64023,    211.88,    131.13,         0
simulation,         8,      0.66,   0.64123,   0.64541,    289.69,    166.29,         0
simulation,         9,      0.67,   0.64487,   0.64962,     209.8,    167.75,         1
simulation,        11,       0.6,   0.59917,   0.59969,    39.506,    40.899,         0
simulation,        12,      0.55,   0.54979,   0.54975,    24.849,     24.95,         0
