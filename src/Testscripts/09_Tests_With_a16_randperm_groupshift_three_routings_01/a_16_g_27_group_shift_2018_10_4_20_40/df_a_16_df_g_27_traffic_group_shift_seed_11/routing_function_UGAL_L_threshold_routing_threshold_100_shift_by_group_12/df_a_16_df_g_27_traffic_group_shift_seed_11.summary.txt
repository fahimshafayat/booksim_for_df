vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 27;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L_threshold;
routing_threshold = 100;
shift_by_group = 12;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010039,  0.010039,    15.187,     15.19,         0
simulation,         2,      0.99,   0.16942,   0.41029,     673.2,    669.94,         1
simulation,         3,       0.5,   0.21056,   0.44731,    358.94,    358.92,         1
simulation,         4,      0.25,   0.16626,   0.24787,    155.78,    664.08,         1
simulation,         5,      0.13,   0.12944,   0.13004,    89.447,    118.79,         0
simulation,         6,      0.19,   0.16197,   0.18943,    369.01,    423.27,         1
simulation,         7,      0.16,   0.15054,   0.15994,    254.15,    261.09,         1
simulation,         8,      0.14,   0.13773,   0.14005,    161.54,    254.02,         0
simulation,         9,      0.15,   0.14489,   0.15005,    269.71,    425.32,         0
simulation,        11,       0.1,   0.10001,   0.10001,    60.801,    61.478,         0
simulation,        12,      0.05,  0.050123,  0.050113,    18.054,     18.06,         0
