vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 27;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L_threshold;
routing_threshold = 100;
shift_by_group = 19;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010035,  0.010039,    15.249,    15.252,         0
simulation,         2,      0.99,   0.16876,   0.40946,    671.94,     668.6,         1
simulation,         3,       0.5,   0.21091,   0.44751,    357.06,    357.05,         1
simulation,         4,      0.25,   0.16696,   0.24768,    155.35,    659.38,         1
simulation,         5,      0.13,   0.12947,   0.13004,    89.494,     108.1,         0
simulation,         6,      0.19,   0.16204,    0.1884,    434.19,    473.12,         1
simulation,         7,      0.16,   0.15104,   0.15993,     246.0,    252.98,         1
simulation,         8,      0.14,   0.13769,   0.14005,    162.69,    241.85,         0
simulation,         9,      0.15,   0.14501,   0.15005,    271.76,    410.04,         0
simulation,        11,       0.1,  0.099998,   0.10001,    60.658,    61.308,         0
simulation,        12,      0.05,  0.050121,  0.050113,    18.115,    18.119,         0
