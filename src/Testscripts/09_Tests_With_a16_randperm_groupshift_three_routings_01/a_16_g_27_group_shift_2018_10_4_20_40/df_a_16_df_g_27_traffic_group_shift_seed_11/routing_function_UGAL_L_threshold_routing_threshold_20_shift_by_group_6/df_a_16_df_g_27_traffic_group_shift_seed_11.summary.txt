vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 27;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L_threshold;
routing_threshold = 20;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010038,  0.010039,     15.22,    15.222,         0
simulation,         2,      0.99,   0.17033,   0.41214,    643.13,    641.88,         1
simulation,         3,       0.5,   0.19798,   0.43864,    376.41,     376.4,         1
simulation,         4,      0.25,   0.15397,   0.24967,    495.94,    495.94,         1
simulation,         5,      0.13,   0.13001,   0.13004,    73.691,    74.829,         0
simulation,         6,      0.19,    0.1601,   0.18995,    455.95,    473.74,         1
simulation,         7,      0.16,   0.15666,   0.16004,    178.67,    290.94,         0
simulation,         8,      0.17,   0.16019,   0.17001,    247.06,    251.62,         1
simulation,         9,      0.15,   0.14952,   0.15001,    96.772,    103.16,         0
simulation,        10,       0.1,  0.099968,   0.10001,     61.07,     61.71,         0
simulation,        11,      0.05,  0.050117,  0.050113,    18.004,    18.008,         0
