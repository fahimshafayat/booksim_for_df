vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 25;
multitiered_routing_threshold_0 = 60;
routing_function = UGAL_G_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.39741,   0.42949,    746.75,    547.65,         1,         0.0
simulation,         2,      0.25,   0.25008,   0.25008,    13.707,    13.708,         0,      4.5991
simulation,         3,      0.37,   0.36955,   0.36954,     141.6,    56.938,         0,      4.7579
simulation,         4,      0.43,   0.40045,   0.40751,    471.52,    280.64,         1,         0.0
simulation,         5,       0.4,   0.39067,   0.39092,    451.42,    158.38,         1,         0.0
simulation,         6,      0.38,   0.37836,   0.37836,    458.36,      89.6,         0,      4.8109
simulation,         7,      0.39,   0.38455,   0.38457,    518.66,    130.23,         1,         0.0
simulation,         8,       0.1,   0.10007,   0.10007,    11.833,    11.833,         0,       4.493
simulation,         9,       0.2,   0.20009,   0.20009,    12.901,    12.901,         0,      4.5637
simulation,        10,       0.3,   0.30006,   0.30006,    15.109,     15.11,         0,      4.6326
