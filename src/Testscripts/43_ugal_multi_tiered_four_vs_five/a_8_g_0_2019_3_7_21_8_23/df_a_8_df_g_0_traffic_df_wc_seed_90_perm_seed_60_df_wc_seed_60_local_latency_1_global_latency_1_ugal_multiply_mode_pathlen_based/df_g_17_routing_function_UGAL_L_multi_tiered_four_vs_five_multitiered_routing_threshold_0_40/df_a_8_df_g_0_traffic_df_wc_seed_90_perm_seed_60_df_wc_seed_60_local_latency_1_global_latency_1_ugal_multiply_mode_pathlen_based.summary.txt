vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 17;
multitiered_routing_threshold_0 = 40;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.44004,   0.46074,     454.1,    335.55,         1,         0.0
simulation,         2,      0.25,   0.25006,   0.25005,    36.535,      36.6,         0,      4.6514
simulation,         3,      0.37,   0.35802,   0.35819,    606.96,    255.33,         1,         0.0
simulation,         4,      0.31,   0.30973,   0.30974,    104.62,    68.907,         0,      4.7322
simulation,         5,      0.34,   0.33591,   0.33593,    487.27,    145.29,         1,         0.0
simulation,         6,      0.32,   0.31835,   0.31838,    413.08,    100.63,         0,      4.7505
simulation,         7,      0.33,   0.32814,    0.3282,    406.14,    119.55,         0,      4.7726
simulation,         8,       0.1,  0.099945,  0.099944,    11.652,    11.652,         0,      4.3012
simulation,         9,       0.2,   0.20008,   0.20007,    35.004,    35.043,         0,      4.5885
simulation,        10,       0.3,   0.29994,   0.30001,    50.944,     51.12,         0,      4.7135
