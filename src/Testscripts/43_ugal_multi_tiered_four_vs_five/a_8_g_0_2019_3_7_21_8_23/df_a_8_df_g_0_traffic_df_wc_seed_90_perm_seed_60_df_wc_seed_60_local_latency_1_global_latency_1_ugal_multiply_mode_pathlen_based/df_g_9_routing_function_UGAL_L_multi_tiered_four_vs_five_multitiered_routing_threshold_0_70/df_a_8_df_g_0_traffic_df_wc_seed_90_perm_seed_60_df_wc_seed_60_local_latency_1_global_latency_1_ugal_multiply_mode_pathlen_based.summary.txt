vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 9;
multitiered_routing_threshold_0 = 70;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.41635,    0.4479,    668.08,    509.66,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24997,    39.066,    39.092,         0,      4.2549
simulation,         3,      0.37,   0.36685,   0.36698,    517.77,    166.22,         1,         0.0
simulation,         4,      0.31,   0.30991,    0.3099,    41.776,     41.82,         0,      4.3175
simulation,         5,      0.34,   0.33949,   0.33955,    171.95,    96.739,         0,      4.3398
simulation,         6,      0.35,   0.34907,   0.34907,    255.85,    101.19,         0,      4.3467
simulation,         7,      0.36,    0.3582,   0.35824,    409.62,     129.1,         0,      4.3524
simulation,         8,       0.1,  0.099934,  0.099933,    10.723,    10.723,         0,      4.1453
simulation,         9,       0.2,   0.19992,   0.19992,    12.147,    12.147,         0,      4.2044
simulation,        10,       0.3,   0.29992,   0.29991,    41.313,    41.348,         0,      4.3089
