vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 33;
multitiered_routing_threshold_0 = 50;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.3018,    0.3407,    1329.7,    847.02,         1,         0.0
simulation,         2,      0.25,   0.24986,   0.24986,    76.406,    47.218,         0,      4.9228
simulation,         3,      0.37,   0.32248,   0.33482,    698.82,    420.33,         1,         0.0
simulation,         4,      0.31,   0.30604,    0.3061,    511.76,    108.11,         1,         0.0
simulation,         5,      0.28,    0.2785,    0.2785,    471.94,      74.4,         0,      4.9679
simulation,         6,      0.29,   0.28771,   0.28773,    359.57,    81.068,         1,         0.0
simulation,         7,      0.05,  0.050089,   0.05009,    11.578,    11.578,         0,      4.3729
simulation,         8,       0.1,   0.10012,   0.10012,    31.764,     31.79,         0,      4.6783
simulation,         9,      0.15,   0.15011,   0.15011,    31.865,     31.93,         0,      4.7928
simulation,        10,       0.2,   0.20009,   0.20009,    31.529,    31.615,         0,       4.865
simulation,        11,      0.25,   0.24986,   0.24986,    76.406,    47.218,         0,      4.9228
