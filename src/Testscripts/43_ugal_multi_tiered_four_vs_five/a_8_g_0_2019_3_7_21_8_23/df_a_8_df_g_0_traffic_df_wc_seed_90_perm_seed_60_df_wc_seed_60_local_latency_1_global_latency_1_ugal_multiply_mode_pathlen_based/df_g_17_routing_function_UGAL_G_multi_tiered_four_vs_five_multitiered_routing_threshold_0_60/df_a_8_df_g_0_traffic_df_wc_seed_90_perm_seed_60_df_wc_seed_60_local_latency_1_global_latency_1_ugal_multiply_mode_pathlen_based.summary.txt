vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 17;
multitiered_routing_threshold_0 = 60;
routing_function = UGAL_G_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46504,   0.47218,    474.26,     261.0,         1,         0.0
simulation,         2,      0.25,   0.25006,   0.25005,    13.267,    13.267,         0,      4.5095
simulation,         3,      0.37,   0.36998,   0.36997,    17.321,    17.322,         0,      4.5921
simulation,         4,      0.43,   0.42853,   0.42856,    356.38,    94.058,         0,      4.7452
simulation,         5,      0.46,   0.44534,   0.44491,    448.19,    171.99,         1,         0.0
simulation,         6,      0.44,   0.43401,   0.43405,    482.45,    120.69,         1,         0.0
simulation,         7,       0.1,  0.099945,  0.099944,    11.515,    11.515,         0,      4.4137
simulation,         8,       0.2,   0.20008,   0.20007,    12.578,    12.578,         0,       4.479
simulation,         9,       0.3,   0.30002,   0.30001,    14.226,    14.226,         0,      4.5471
simulation,        10,       0.4,   0.39994,   0.39994,    29.324,    29.331,         0,      4.6283
