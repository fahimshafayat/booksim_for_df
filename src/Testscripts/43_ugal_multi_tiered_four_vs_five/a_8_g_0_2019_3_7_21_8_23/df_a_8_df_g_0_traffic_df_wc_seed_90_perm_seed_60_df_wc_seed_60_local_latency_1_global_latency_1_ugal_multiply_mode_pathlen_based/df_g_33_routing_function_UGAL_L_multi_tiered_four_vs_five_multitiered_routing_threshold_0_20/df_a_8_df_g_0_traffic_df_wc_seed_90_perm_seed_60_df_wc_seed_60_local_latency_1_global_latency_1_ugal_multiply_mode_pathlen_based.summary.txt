vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 33;
multitiered_routing_threshold_0 = 20;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.29314,   0.33263,    1311.7,    862.43,         1,         0.0
simulation,         2,      0.25,   0.24961,   0.24962,     124.1,    49.269,         0,      4.9312
simulation,         3,      0.37,   0.31303,   0.32376,    1578.7,    652.95,         1,         0.0
simulation,         4,      0.31,   0.30652,   0.30658,    476.29,    88.694,         1,         0.0
simulation,         5,      0.28,   0.27863,   0.27863,    463.12,    67.559,         0,      4.9827
simulation,         6,      0.29,     0.288,   0.28801,     327.6,    72.772,         1,         0.0
simulation,         7,      0.05,  0.050089,   0.05009,    11.578,    11.578,         0,      4.3729
simulation,         8,       0.1,   0.10012,   0.10012,    31.763,    31.789,         0,      4.6784
simulation,         9,      0.15,   0.15011,   0.15011,    29.229,    29.265,         0,      4.7942
simulation,        10,       0.2,   0.20009,   0.20009,    28.113,    28.157,         0,      4.8651
simulation,        11,      0.25,   0.24961,   0.24962,     124.1,    49.269,         0,      4.9312
