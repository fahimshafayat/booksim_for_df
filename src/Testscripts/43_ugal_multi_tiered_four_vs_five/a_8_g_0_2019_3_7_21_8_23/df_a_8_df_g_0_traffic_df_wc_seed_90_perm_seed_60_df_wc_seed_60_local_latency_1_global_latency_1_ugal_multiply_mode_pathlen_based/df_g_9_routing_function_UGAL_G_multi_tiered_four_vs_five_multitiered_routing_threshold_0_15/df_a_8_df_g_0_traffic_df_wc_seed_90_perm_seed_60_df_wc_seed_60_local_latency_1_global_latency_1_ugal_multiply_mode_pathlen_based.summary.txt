vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 9;
multitiered_routing_threshold_0 = 15;
routing_function = UGAL_G_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49987,   0.49978,    52.337,    52.346,         0,      4.4349
simulation,         2,      0.74,   0.53526,   0.57633,    1175.3,    613.63,         1,         0.0
simulation,         3,      0.62,   0.55084,   0.57793,    472.49,    355.18,         1,         0.0
simulation,         4,      0.56,   0.55729,   0.55734,    437.37,    127.69,         0,      4.6201
simulation,         5,      0.59,   0.55264,   0.56267,    531.25,    304.82,         1,         0.0
simulation,         6,      0.57,    0.5562,   0.55534,    541.67,     232.0,         1,         0.0
simulation,         7,       0.1,  0.099934,  0.099933,    10.826,    10.826,         0,      4.2097
simulation,         8,       0.2,   0.19992,   0.19992,    11.632,    11.633,         0,      4.2402
simulation,         9,       0.3,   0.29992,   0.29991,     12.76,     12.76,         0,      4.2648
simulation,        10,       0.4,   0.39985,   0.39984,    14.988,    14.988,         0,      4.3273
simulation,        11,       0.5,   0.49987,   0.49978,    52.337,    52.346,         0,      4.4349
