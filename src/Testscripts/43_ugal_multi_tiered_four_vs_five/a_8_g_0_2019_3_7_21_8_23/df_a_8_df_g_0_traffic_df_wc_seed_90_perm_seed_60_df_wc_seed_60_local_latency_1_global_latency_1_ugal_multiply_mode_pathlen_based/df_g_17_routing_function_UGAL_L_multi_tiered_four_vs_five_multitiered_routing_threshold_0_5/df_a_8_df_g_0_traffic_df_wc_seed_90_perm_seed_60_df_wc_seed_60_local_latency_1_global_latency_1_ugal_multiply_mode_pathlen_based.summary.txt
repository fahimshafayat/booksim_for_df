vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 17;
multitiered_routing_threshold_0 = 5;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46392,   0.46452,    1239.2,    338.51,         1,         0.0
simulation,         2,      0.25,   0.25006,   0.25005,    33.534,    33.574,         0,      4.9154
simulation,         3,      0.37,   0.36998,   0.36997,    35.024,    35.082,         0,      5.2153
simulation,         4,      0.43,   0.42993,   0.42991,    39.318,    39.401,         0,        5.26
simulation,         5,      0.46,   0.45405,   0.45418,    510.27,    120.34,         1,         0.0
simulation,         6,      0.44,   0.43979,   0.43984,    49.797,    47.394,         0,      5.2659
simulation,         7,      0.45,   0.44748,    0.4475,    354.56,    80.099,         0,      5.2704
simulation,         8,       0.1,  0.099944,  0.099944,    11.766,    11.766,         0,      4.3564
simulation,         9,       0.2,   0.20008,   0.20007,    34.212,    34.245,         0,      4.6754
simulation,        10,       0.3,   0.30002,   0.30001,    33.685,    33.732,         0,      5.1066
simulation,        11,       0.4,   0.39995,   0.39994,    36.374,    36.438,         0,      5.2398
