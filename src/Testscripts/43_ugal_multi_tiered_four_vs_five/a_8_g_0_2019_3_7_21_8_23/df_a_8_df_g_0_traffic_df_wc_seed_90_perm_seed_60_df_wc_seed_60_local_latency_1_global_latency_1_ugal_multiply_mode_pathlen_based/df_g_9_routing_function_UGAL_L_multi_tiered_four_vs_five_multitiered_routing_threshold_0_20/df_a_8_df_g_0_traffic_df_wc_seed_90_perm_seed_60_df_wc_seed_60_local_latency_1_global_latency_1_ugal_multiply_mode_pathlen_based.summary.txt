vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 9;
multitiered_routing_threshold_0 = 20;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.47105,   0.48003,    485.86,     285.9,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24997,     38.57,    38.593,         0,      4.3058
simulation,         3,      0.37,   0.36992,   0.36992,    45.991,    46.055,         0,      4.4243
simulation,         4,      0.43,   0.42589,   0.42598,    500.86,     124.8,         1,         0.0
simulation,         5,       0.4,   0.39874,   0.39875,    291.07,    68.534,         0,      4.4693
simulation,         6,      0.41,   0.40789,   0.40791,    462.42,    89.263,         0,      4.4879
simulation,         7,      0.42,   0.41718,   0.41719,    369.12,     106.9,         1,         0.0
simulation,         8,       0.1,  0.099934,  0.099933,    10.723,    10.723,         0,      4.1453
simulation,         9,       0.2,   0.19992,   0.19992,    12.154,    12.154,         0,       4.204
simulation,        10,       0.3,   0.29992,   0.29991,    41.296,    41.333,         0,      4.3608
simulation,        11,       0.4,   0.39874,   0.39875,    291.07,    68.534,         0,      4.4693
