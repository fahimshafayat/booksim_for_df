vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 62;
global_latency = 1;
local_latency = 1;
perm_seed = 62;
seed = 92;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 17;
multitiered_routing_threshold_0 = 15;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49985,   0.49985,    14.059,    14.062,         0,      3.9656
simulation,         2,      0.74,   0.68521,   0.69637,     551.7,    270.59,         1,         0.0
simulation,         3,      0.62,   0.61988,   0.61985,    32.654,    32.684,         0,      4.0257
simulation,         4,      0.68,    0.6731,   0.67312,    496.18,    100.59,         1,         0.0
simulation,         5,      0.65,   0.64823,   0.64822,    210.19,    57.439,         0,      4.0819
simulation,         6,      0.66,   0.65709,   0.65708,    403.49,    63.392,         0,      4.1076
simulation,         7,      0.67,   0.66593,   0.66593,    338.78,    78.494,         1,         0.0
simulation,         8,       0.1,  0.099984,  0.099982,    10.091,    10.091,         0,      3.9485
simulation,         9,       0.2,   0.19985,   0.19985,    10.427,    10.427,         0,      3.9783
simulation,        10,       0.3,   0.29975,   0.29975,    10.799,    10.799,         0,      3.9765
simulation,        11,       0.4,    0.3997,    0.3997,    11.338,    11.338,         0,       3.968
simulation,        12,       0.5,   0.49985,   0.49985,    14.059,    14.062,         0,      3.9656
simulation,        13,       0.6,   0.59984,   0.59984,    26.924,    26.948,         0,      4.0017
