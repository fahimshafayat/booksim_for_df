vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 62;
global_latency = 1;
local_latency = 1;
perm_seed = 62;
seed = 92;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 17;
multitiered_routing_threshold_0 = 10;
routing_function = UGAL_G_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49985,   0.49985,    11.818,    11.818,         0,      3.8833
simulation,         2,      0.74,   0.73986,   0.73986,    19.583,    19.584,         0,      3.9361
simulation,         3,      0.86,   0.77674,   0.78929,     699.3,    283.37,         1,         0.0
simulation,         4,       0.8,   0.78572,   0.78571,    523.73,    152.31,         1,         0.0
simulation,         5,      0.77,   0.76991,   0.76989,    26.652,    26.653,         0,      3.9798
simulation,         6,      0.78,   0.77992,    0.7799,    32.092,    32.095,         0,      3.9997
simulation,         7,      0.79,   0.78995,   0.78991,    43.842,    43.848,         0,      4.0259
simulation,         8,       0.1,  0.099983,  0.099982,    10.066,    10.066,         0,       3.934
simulation,         9,       0.2,   0.19985,   0.19985,    10.324,    10.325,         0,       3.926
simulation,        10,       0.3,   0.29975,   0.29975,    10.665,    10.665,         0,      3.9113
simulation,        11,       0.4,    0.3997,    0.3997,    11.135,    11.135,         0,      3.8968
simulation,        12,       0.5,   0.49985,   0.49985,    11.818,    11.818,         0,      3.8833
simulation,        13,       0.6,   0.59984,   0.59984,    12.984,    12.984,         0,      3.8768
simulation,        14,       0.7,   0.69994,   0.69993,    16.035,    16.035,         0,      3.9017
