vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 62;
global_latency = 1;
local_latency = 1;
perm_seed = 62;
seed = 92;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
multitiered_routing_threshold_0 = 50;
routing_function = UGAL_G_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49998,   0.49998,     12.87,     12.87,         0,      4.0312
simulation,         2,      0.74,   0.67134,    0.6827,    658.11,    293.46,         1,         0.0
simulation,         3,      0.62,   0.62001,   0.62001,    17.425,    17.425,         0,       4.062
simulation,         4,      0.68,   0.68004,   0.68006,    44.321,    44.325,         0,      4.1393
simulation,         5,      0.71,   0.67434,   0.67459,    894.68,    247.51,         1,         0.0
simulation,         6,      0.69,   0.67794,   0.67802,    488.31,    154.73,         1,         0.0
simulation,         7,       0.1,  0.099915,  0.099913,    10.316,    10.316,         0,      4.0511
simulation,         8,       0.2,   0.19995,   0.19995,    10.644,    10.644,         0,      4.0544
simulation,         9,       0.3,   0.29997,   0.29997,    11.096,    11.096,         0,      4.0468
simulation,        10,       0.4,       0.4,   0.40001,    11.748,    11.748,         0,      4.0368
simulation,        11,       0.5,   0.49998,   0.49998,     12.87,     12.87,         0,      4.0312
simulation,        12,       0.6,   0.60004,   0.60004,    15.964,    15.965,         0,      4.0517
