vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 62;
global_latency = 1;
local_latency = 1;
perm_seed = 62;
seed = 92;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
multitiered_routing_threshold_0 = 60;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49995,   0.49996,     33.12,    32.311,         0,      4.1632
simulation,         2,      0.74,    0.5784,   0.61503,    740.11,    463.23,         1,         0.0
simulation,         3,      0.62,   0.58661,   0.58671,     934.7,    254.98,         1,         0.0
simulation,         4,      0.56,   0.55769,   0.55772,    402.64,    75.783,         0,      4.2763
simulation,         5,      0.59,   0.57764,   0.57783,    516.85,    164.63,         1,         0.0
simulation,         6,      0.57,   0.56584,   0.56588,    384.61,    97.419,         1,         0.0
simulation,         7,       0.1,  0.099915,  0.099913,     10.34,     10.34,         0,      4.0647
simulation,         8,       0.2,   0.19995,   0.19995,    10.732,    10.732,         0,      4.1003
simulation,         9,       0.3,   0.29997,   0.29997,    11.448,    11.448,         0,      4.1027
simulation,        10,       0.4,       0.4,   0.40001,     14.52,    14.527,         0,      4.1063
simulation,        11,       0.5,   0.49995,   0.49996,     33.12,    32.311,         0,      4.1632
