vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 62;
global_latency = 1;
local_latency = 1;
perm_seed = 62;
seed = 92;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
multitiered_routing_threshold_0 = 70;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49956,   0.49956,    122.23,     32.56,         0,      4.1596
simulation,         2,      0.74,   0.59439,    0.6276,    668.69,     425.2,         1,         0.0
simulation,         3,      0.62,   0.58528,    0.5852,    960.11,    260.03,         1,         0.0
simulation,         4,      0.56,   0.55607,   0.55607,     375.6,    87.793,         1,         0.0
simulation,         5,      0.53,   0.52958,   0.52957,    102.86,    45.911,         0,      4.2017
simulation,         6,      0.54,   0.53899,   0.53899,    174.11,    57.927,         0,      4.2184
simulation,         7,      0.55,   0.54791,   0.54792,    354.39,    71.975,         0,      4.2361
simulation,         8,       0.1,  0.099915,  0.099913,     10.34,     10.34,         0,      4.0647
simulation,         9,       0.2,   0.19995,   0.19995,    10.732,    10.732,         0,      4.1003
simulation,        10,       0.3,   0.29997,   0.29997,    11.448,    11.448,         0,      4.1027
simulation,        11,       0.4,   0.39982,   0.39982,    51.213,    16.601,         0,      4.1057
simulation,        12,       0.5,   0.49956,   0.49956,    122.23,     32.56,         0,      4.1596
