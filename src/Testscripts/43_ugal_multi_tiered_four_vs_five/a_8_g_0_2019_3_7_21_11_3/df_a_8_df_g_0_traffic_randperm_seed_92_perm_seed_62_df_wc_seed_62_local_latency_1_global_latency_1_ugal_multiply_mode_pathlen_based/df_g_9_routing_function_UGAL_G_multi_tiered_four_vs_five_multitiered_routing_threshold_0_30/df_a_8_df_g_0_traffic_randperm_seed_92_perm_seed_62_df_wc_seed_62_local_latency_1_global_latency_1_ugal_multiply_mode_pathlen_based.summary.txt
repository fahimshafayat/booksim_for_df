vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 62;
global_latency = 1;
local_latency = 1;
perm_seed = 62;
seed = 92;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 9;
multitiered_routing_threshold_0 = 30;
routing_function = UGAL_G_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50012,   0.50012,    11.044,    11.044,         0,      3.7168
simulation,         2,      0.74,   0.73998,   0.73997,    13.376,    13.376,         0,      3.6858
simulation,         3,      0.86,   0.86007,   0.86008,    17.705,    17.706,         0,      3.6888
simulation,         4,      0.92,   0.92009,    0.9201,    28.861,    28.865,         0,      3.7037
simulation,         5,      0.95,   0.92544,   0.92512,     604.4,    160.75,         1,         0.0
simulation,         6,      0.93,    0.9301,   0.93008,    36.129,    36.133,         0,      3.7114
simulation,         7,      0.94,   0.94009,   0.94004,    55.262,    55.276,         0,      3.7303
simulation,         8,       0.1,   0.10007,   0.10007,    9.7352,    9.7353,         0,      3.7784
simulation,         9,       0.2,   0.20012,   0.20012,    9.9469,     9.947,         0,      3.7664
simulation,        10,       0.3,   0.30006,   0.30006,     10.21,    10.211,         0,      3.7491
simulation,        11,       0.4,   0.40003,   0.40003,    10.563,    10.563,         0,      3.7317
simulation,        12,       0.5,   0.50012,   0.50012,    11.044,    11.044,         0,      3.7168
simulation,        13,       0.6,   0.60001,       0.6,    11.707,    11.708,         0,      3.7018
simulation,        14,       0.7,   0.69991,   0.69991,    12.745,    12.745,         0,      3.6894
simulation,        15,       0.8,   0.80005,   0.80005,    14.813,    14.814,         0,      3.6842
simulation,        16,       0.9,   0.90008,   0.90007,    22.538,    22.541,         0,      3.6964
