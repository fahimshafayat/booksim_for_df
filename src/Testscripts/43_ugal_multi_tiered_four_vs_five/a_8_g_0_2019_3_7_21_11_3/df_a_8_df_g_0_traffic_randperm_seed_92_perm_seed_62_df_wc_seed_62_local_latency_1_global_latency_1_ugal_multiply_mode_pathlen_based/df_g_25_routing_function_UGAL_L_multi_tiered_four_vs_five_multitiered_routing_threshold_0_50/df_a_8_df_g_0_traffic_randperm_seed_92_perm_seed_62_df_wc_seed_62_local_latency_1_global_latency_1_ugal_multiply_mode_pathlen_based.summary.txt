vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 62;
global_latency = 1;
local_latency = 1;
perm_seed = 62;
seed = 92;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 25;
multitiered_routing_threshold_0 = 50;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49992,   0.49994,    29.532,    29.571,         0,      4.1314
simulation,         2,      0.74,   0.58735,   0.62131,    643.04,    404.19,         1,         0.0
simulation,         3,      0.62,   0.59084,   0.59101,     798.3,    220.19,         1,         0.0
simulation,         4,      0.56,   0.55773,   0.55774,    443.46,    73.899,         0,      4.2241
simulation,         5,      0.59,   0.57895,    0.5792,    516.12,    138.32,         1,         0.0
simulation,         6,      0.57,   0.56539,   0.56542,    381.35,    90.001,         1,         0.0
simulation,         7,       0.1,  0.099918,  0.099918,    10.295,    10.295,         0,      4.0412
simulation,         8,       0.2,   0.19988,   0.19988,    10.687,    10.687,         0,      4.0767
simulation,         9,       0.3,   0.29983,   0.29983,     11.21,     11.21,         0,      4.0788
simulation,        10,       0.4,   0.39989,   0.39989,    14.765,     14.77,         0,      4.0849
simulation,        11,       0.5,   0.49992,   0.49994,    29.532,    29.571,         0,      4.1314
