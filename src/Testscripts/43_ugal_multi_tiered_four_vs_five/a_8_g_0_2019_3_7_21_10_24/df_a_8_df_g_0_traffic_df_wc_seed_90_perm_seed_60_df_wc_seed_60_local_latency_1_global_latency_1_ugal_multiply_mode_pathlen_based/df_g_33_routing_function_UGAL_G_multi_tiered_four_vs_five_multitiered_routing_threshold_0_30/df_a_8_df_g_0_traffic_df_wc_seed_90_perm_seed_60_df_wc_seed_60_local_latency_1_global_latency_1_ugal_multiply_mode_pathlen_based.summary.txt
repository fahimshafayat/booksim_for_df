vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 33;
multitiered_routing_threshold_0 = 30;
routing_function = UGAL_G_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.42561,   0.44635,    404.22,    317.51,         1,         0.0
simulation,         2,      0.25,   0.25006,   0.25006,    13.857,    13.857,         0,      4.7268
simulation,         3,      0.37,   0.37007,   0.37006,    32.485,     32.52,         0,      4.8876
simulation,         4,      0.43,   0.41362,   0.41387,    460.98,    166.43,         1,         0.0
simulation,         5,       0.4,   0.39867,   0.39868,    381.16,    57.167,         0,      5.0088
simulation,         6,      0.41,   0.40745,   0.40746,    226.15,     71.98,         1,         0.0
simulation,         7,       0.1,   0.10012,   0.10012,    12.164,    12.164,         0,      4.6091
simulation,         8,       0.2,   0.20009,   0.20009,    13.148,    13.148,         0,      4.6913
simulation,         9,       0.3,   0.30005,   0.30005,    15.096,    15.096,         0,      4.7528
simulation,        10,       0.4,   0.39867,   0.39868,    381.16,    57.167,         0,      5.0088
