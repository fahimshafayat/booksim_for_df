vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 9;
multitiered_routing_threshold_0 = 60;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.45695,   0.46833,    725.46,    403.36,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24997,    39.084,    39.109,         0,      4.2934
simulation,         3,      0.37,   0.36995,   0.36992,    54.727,    54.889,         0,      4.4217
simulation,         4,      0.43,   0.42272,   0.42312,    508.77,    221.11,         1,         0.0
simulation,         5,       0.4,   0.39881,   0.39889,    275.07,    105.29,         0,      4.4623
simulation,         6,      0.41,   0.40726,   0.40737,    378.24,    137.03,         1,         0.0
simulation,         7,       0.1,  0.099934,  0.099933,    10.723,    10.723,         0,      4.1453
simulation,         8,       0.2,   0.19992,   0.19992,    12.147,    12.147,         0,      4.2044
simulation,         9,       0.3,   0.29991,   0.29991,    41.323,     41.36,         0,      4.3609
simulation,        10,       0.4,   0.39881,   0.39889,    275.07,    105.29,         0,      4.4623
