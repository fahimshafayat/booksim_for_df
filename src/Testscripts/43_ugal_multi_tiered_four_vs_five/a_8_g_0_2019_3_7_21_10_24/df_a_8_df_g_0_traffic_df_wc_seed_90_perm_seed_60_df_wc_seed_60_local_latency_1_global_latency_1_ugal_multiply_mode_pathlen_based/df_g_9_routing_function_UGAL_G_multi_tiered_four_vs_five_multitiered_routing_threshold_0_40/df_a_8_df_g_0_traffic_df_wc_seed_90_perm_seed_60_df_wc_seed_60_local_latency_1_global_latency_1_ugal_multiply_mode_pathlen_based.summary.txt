vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 9;
multitiered_routing_threshold_0 = 40;
routing_function = UGAL_G_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49985,   0.49978,    77.254,    77.342,         0,      4.4276
simulation,         2,      0.74,   0.52523,   0.56727,    1239.9,    636.21,         1,         0.0
simulation,         3,      0.62,   0.54161,   0.56932,    525.94,    385.38,         1,         0.0
simulation,         4,      0.56,     0.537,   0.53691,    719.82,    239.48,         1,         0.0
simulation,         5,      0.53,   0.52377,   0.52375,    523.39,    140.53,         1,         0.0
simulation,         6,      0.51,   0.50919,   0.50918,    148.39,    102.04,         0,      4.4527
simulation,         7,      0.52,   0.51595,   0.51597,    416.49,     123.9,         1,         0.0
simulation,         8,       0.1,  0.099934,  0.099933,    10.826,    10.826,         0,      4.2097
simulation,         9,       0.2,   0.19992,   0.19992,    11.633,    11.633,         0,      4.2406
simulation,        10,       0.3,   0.29992,   0.29991,    12.765,    12.765,         0,      4.2648
simulation,        11,       0.4,   0.39985,   0.39984,    15.045,    15.045,         0,      4.3175
simulation,        12,       0.5,   0.49985,   0.49978,    77.254,    77.342,         0,      4.4276
