vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 9;
multitiered_routing_threshold_0 = 5;
routing_function = UGAL_G_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49982,   0.49978,    22.593,    22.592,         0,      4.4673
simulation,         2,      0.74,   0.53387,   0.57541,    1183.5,    618.81,         1,         0.0
simulation,         3,      0.62,   0.55262,   0.57833,     451.5,    340.67,         1,         0.0
simulation,         4,      0.56,   0.55992,   0.55986,    75.176,    75.163,         0,       4.636
simulation,         5,      0.59,   0.55508,   0.56431,    494.65,    279.75,         1,         0.0
simulation,         6,      0.57,   0.55795,   0.55758,    595.45,    212.88,         1,         0.0
simulation,         7,       0.1,  0.099934,  0.099933,    10.834,    10.834,         0,      4.2141
simulation,         8,       0.2,   0.19992,   0.19992,    11.667,    11.667,         0,      4.2585
simulation,         9,       0.3,   0.29992,   0.29991,    12.784,    12.784,         0,      4.2967
simulation,        10,       0.4,   0.39986,   0.39984,    14.889,     14.89,         0,      4.3681
simulation,        11,       0.5,   0.49982,   0.49978,    22.593,    22.592,         0,      4.4673
