vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 25;
multitiered_routing_threshold_0 = 30;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.31273,      0.35,    1058.0,    747.44,         1,         0.0
simulation,         2,      0.25,   0.24962,   0.24962,    146.53,    60.404,         0,      4.7751
simulation,         3,      0.37,   0.33342,   0.34164,    548.21,    330.57,         1,         0.0
simulation,         4,      0.31,   0.30093,   0.30106,    487.15,    184.29,         1,         0.0
simulation,         5,      0.28,   0.27663,   0.27663,    512.11,    121.49,         1,         0.0
simulation,         6,      0.26,   0.25892,   0.25893,    399.21,    70.442,         0,      4.7872
simulation,         7,      0.27,    0.2679,   0.26794,    387.07,    98.142,         1,         0.0
simulation,         8,      0.05,  0.050054,  0.050053,    11.177,    11.177,         0,      4.3042
simulation,         9,       0.1,   0.10007,   0.10007,    22.456,    22.474,         0,       4.494
simulation,        10,      0.15,   0.15007,   0.15007,    31.339,    31.366,         0,      4.6384
simulation,        11,       0.2,   0.20009,   0.20009,    30.486,    30.523,         0,      4.7121
simulation,        12,      0.25,   0.24962,   0.24962,    146.53,    60.404,         0,      4.7751
