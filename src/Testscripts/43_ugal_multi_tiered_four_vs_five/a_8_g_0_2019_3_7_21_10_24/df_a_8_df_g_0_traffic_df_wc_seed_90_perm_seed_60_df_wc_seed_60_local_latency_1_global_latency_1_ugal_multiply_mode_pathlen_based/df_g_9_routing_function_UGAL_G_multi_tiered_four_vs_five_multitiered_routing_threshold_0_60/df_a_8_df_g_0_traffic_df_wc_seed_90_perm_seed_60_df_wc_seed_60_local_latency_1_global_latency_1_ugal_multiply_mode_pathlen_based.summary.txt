vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 9;
multitiered_routing_threshold_0 = 60;
routing_function = UGAL_G_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49805,   0.49807,    279.28,    119.01,         0,      4.4249
simulation,         2,      0.74,   0.51172,   0.55556,    1328.1,     678.0,         1,         0.0
simulation,         3,      0.62,   0.52544,   0.55602,     629.9,    445.91,         1,         0.0
simulation,         4,      0.56,   0.51969,   0.52943,    559.73,    318.21,         1,         0.0
simulation,         5,      0.53,   0.51016,   0.51009,    684.76,    246.58,         1,         0.0
simulation,         6,      0.51,   0.50413,   0.50409,    524.65,    146.31,         1,         0.0
simulation,         7,       0.1,  0.099934,  0.099933,    10.826,    10.826,         0,      4.2097
simulation,         8,       0.2,   0.19992,   0.19992,    11.633,    11.633,         0,      4.2406
simulation,         9,       0.3,   0.29992,   0.29991,    12.765,    12.765,         0,      4.2648
simulation,        10,       0.4,   0.39985,   0.39984,    15.045,    15.045,         0,      4.3175
simulation,        11,       0.5,   0.49805,   0.49807,    279.28,    119.01,         0,      4.4249
