vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 62;
global_latency = 1;
local_latency = 1;
perm_seed = 62;
seed = 92;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 33;
multitiered_routing_threshold_0 = 15;
routing_function = UGAL_G_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.41935,      0.43,    857.18,     396.4,         1,         0.0
simulation,         2,      0.25,   0.24997,   0.24997,    13.911,    13.911,         0,      4.7784
simulation,         3,      0.37,   0.36998,      0.37,    23.666,    23.673,         0,      4.8965
simulation,         4,      0.43,   0.42406,   0.42414,    445.29,    96.935,         1,         0.0
simulation,         5,       0.4,   0.39988,   0.39991,    49.886,    45.741,         0,       4.999
simulation,         6,      0.41,   0.40947,   0.40948,    143.31,    51.712,         0,      5.0616
simulation,         7,      0.42,   0.41928,   0.41928,    203.78,    58.392,         0,      5.1461
simulation,         8,       0.1,  0.099913,  0.099913,     12.19,     12.19,         0,      4.6205
simulation,         9,       0.2,   0.19995,   0.19995,    13.184,    13.184,         0,      4.7163
simulation,        10,       0.3,   0.29997,   0.29997,    15.063,    15.063,         0,      4.8404
simulation,        11,       0.4,   0.39988,   0.39991,    49.886,    45.741,         0,       4.999
