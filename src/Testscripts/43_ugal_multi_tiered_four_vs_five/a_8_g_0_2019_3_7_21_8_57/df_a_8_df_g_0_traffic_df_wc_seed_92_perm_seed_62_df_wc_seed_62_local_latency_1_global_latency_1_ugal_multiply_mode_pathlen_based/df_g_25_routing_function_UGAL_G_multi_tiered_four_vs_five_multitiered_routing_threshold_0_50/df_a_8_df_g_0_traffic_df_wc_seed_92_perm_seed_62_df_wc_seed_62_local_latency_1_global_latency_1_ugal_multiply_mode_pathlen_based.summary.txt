vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 62;
global_latency = 1;
local_latency = 1;
perm_seed = 62;
seed = 92;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 25;
multitiered_routing_threshold_0 = 50;
routing_function = UGAL_G_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.38093,   0.41338,    840.74,    605.76,         1,         0.0
simulation,         2,      0.25,   0.24986,   0.24986,    13.937,    13.938,         0,      4.6553
simulation,         3,      0.37,   0.36825,   0.36825,     467.2,     91.59,         0,      4.9217
simulation,         4,      0.43,   0.39158,   0.40061,    615.35,    357.96,         1,         0.0
simulation,         5,       0.4,   0.38804,     0.388,    476.23,    194.72,         1,         0.0
simulation,         6,      0.38,   0.37655,   0.37656,    406.87,    116.71,         1,         0.0
simulation,         7,       0.1,  0.099918,  0.099918,    12.012,    12.012,         0,      4.5453
simulation,         8,       0.2,   0.19988,   0.19988,    13.075,    13.075,         0,      4.6203
simulation,         9,       0.3,   0.29983,   0.29983,    16.145,    16.145,         0,      4.6849
