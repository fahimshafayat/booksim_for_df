vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 62;
global_latency = 1;
local_latency = 1;
perm_seed = 62;
seed = 92;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 17;
multitiered_routing_threshold_0 = 30;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.43557,   0.45631,    458.81,    334.89,         1,         0.0
simulation,         2,      0.25,   0.24982,   0.24982,    36.381,    36.437,         0,      4.6397
simulation,         3,      0.37,   0.36013,   0.36043,    611.26,    257.84,         1,         0.0
simulation,         4,      0.31,   0.30798,   0.30801,    423.48,    122.34,         0,      4.7335
simulation,         5,      0.34,   0.33482,   0.33482,    485.99,    181.89,         1,         0.0
simulation,         6,      0.32,   0.31784,   0.31784,    480.87,    134.14,         0,      4.7595
simulation,         7,      0.33,   0.32691,   0.32694,    507.32,    153.66,         1,         0.0
simulation,         8,       0.1,  0.099984,  0.099982,    11.616,    11.616,         0,      4.3046
simulation,         9,       0.2,   0.19985,   0.19985,    33.956,     33.99,         0,      4.5765
simulation,        10,       0.3,   0.29927,   0.29936,    148.11,    94.468,         0,      4.7162
