vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 62;
global_latency = 1;
local_latency = 1;
perm_seed = 62;
seed = 92;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 33;
multitiered_routing_threshold_0 = 15;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.28372,   0.32457,    1299.7,     883.0,         1,         0.0
simulation,         2,      0.25,   0.24934,   0.24934,    239.99,    48.684,         0,      4.9324
simulation,         3,      0.37,   0.32805,   0.34217,    655.27,    388.81,         1,         0.0
simulation,         4,      0.31,   0.30645,   0.30648,    486.96,    91.274,         1,         0.0
simulation,         5,      0.28,   0.27885,   0.27885,    398.54,     56.06,         0,      4.9924
simulation,         6,      0.29,   0.28799,   0.28802,    299.15,    69.095,         1,         0.0
simulation,         7,      0.05,  0.049996,  0.049995,    11.548,    11.548,         0,      4.3837
simulation,         8,       0.1,  0.099914,  0.099913,    31.497,    31.524,         0,      4.6776
simulation,         9,      0.15,   0.14992,   0.14991,    28.343,    28.376,         0,      4.7942
simulation,        10,       0.2,   0.19995,   0.19995,    27.059,    27.093,         0,      4.8635
simulation,        11,      0.25,   0.24934,   0.24934,    239.99,    48.684,         0,      4.9324
