vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 62;
global_latency = 1;
local_latency = 1;
perm_seed = 62;
seed = 92;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 25;
multitiered_routing_threshold_0 = 60;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49994,   0.49994,    12.059,     12.06,         0,      3.9222
simulation,         2,      0.74,      0.74,   0.74001,      36.0,    36.021,         0,      3.9528
simulation,         3,      0.86,   0.78342,   0.80066,     789.3,    351.98,         1,         0.0
simulation,         4,       0.8,   0.78199,    0.7828,    640.62,    252.54,         1,         0.0
simulation,         5,      0.77,   0.76988,   0.77004,    64.896,    65.014,         0,      3.9746
simulation,         6,      0.78,   0.77862,   0.77878,    187.54,    109.16,         0,      3.9751
simulation,         7,      0.79,   0.78163,   0.78224,    544.34,    197.76,         1,         0.0
simulation,         8,       0.1,  0.099919,  0.099918,    10.125,    10.125,         0,      3.9507
simulation,         9,       0.2,   0.19988,   0.19988,    10.477,    10.477,         0,      3.9751
simulation,        10,       0.3,   0.29983,   0.29983,    10.851,    10.851,         0,      3.9644
simulation,        11,       0.4,   0.39989,   0.39989,     11.34,     11.34,         0,      3.9437
simulation,        12,       0.5,   0.49994,   0.49994,    12.059,     12.06,         0,      3.9222
simulation,        13,       0.6,   0.59998,   0.59998,    13.308,    13.309,         0,      3.9047
simulation,        14,       0.7,   0.69999,       0.7,    18.072,    18.076,         0,       3.907
