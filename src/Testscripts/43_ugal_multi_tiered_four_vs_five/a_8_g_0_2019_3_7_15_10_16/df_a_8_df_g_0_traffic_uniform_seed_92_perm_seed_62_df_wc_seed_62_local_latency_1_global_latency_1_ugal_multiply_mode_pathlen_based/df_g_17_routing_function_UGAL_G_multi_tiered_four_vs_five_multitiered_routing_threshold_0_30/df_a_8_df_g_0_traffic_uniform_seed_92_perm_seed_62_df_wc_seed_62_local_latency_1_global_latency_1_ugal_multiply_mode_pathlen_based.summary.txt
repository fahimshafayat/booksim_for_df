vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 62;
global_latency = 1;
local_latency = 1;
perm_seed = 62;
seed = 92;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 17;
multitiered_routing_threshold_0 = 30;
routing_function = UGAL_G_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49985,   0.49985,    11.401,    11.401,         0,      3.7698
simulation,         2,      0.74,   0.73986,   0.73986,    14.025,    14.025,         0,      3.7235
simulation,         3,      0.86,   0.85996,   0.85996,    17.814,    17.815,         0,      3.7007
simulation,         4,      0.92,   0.91997,   0.91996,    22.892,    22.894,         0,      3.6873
simulation,         5,      0.95,   0.94997,   0.94998,    28.989,    28.991,         0,      3.6793
simulation,         6,      0.97,   0.96994,   0.96999,    37.997,    38.009,         0,      3.6729
simulation,         7,      0.98,   0.98003,   0.98001,    48.395,    48.421,         0,      3.6693
simulation,         8,       0.1,  0.099985,  0.099982,    9.9505,    9.9506,         0,      3.8678
simulation,         9,       0.2,   0.19985,   0.19985,    10.167,    10.167,         0,       3.841
simulation,        10,       0.3,   0.29975,   0.29975,    10.456,    10.457,         0,      3.8143
simulation,        11,       0.4,    0.3997,    0.3997,    10.856,    10.856,         0,      3.7912
simulation,        12,       0.5,   0.49985,   0.49985,    11.401,    11.401,         0,      3.7698
simulation,        13,       0.6,   0.59984,   0.59984,    12.172,    12.172,         0,      3.7496
simulation,        14,       0.7,   0.69993,   0.69993,    13.352,    13.352,         0,       3.731
simulation,        15,       0.8,   0.79992,   0.79991,    15.438,    15.438,         0,      3.7122
simulation,        16,       0.9,   0.90001,   0.90001,    20.656,    20.657,         0,      3.6919
