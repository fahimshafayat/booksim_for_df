vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 62;
global_latency = 1;
local_latency = 1;
perm_seed = 62;
seed = 92;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 9;
multitiered_routing_threshold_0 = 30;
routing_function = UGAL_G_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50012,   0.50012,    11.042,    11.042,         0,      3.6629
simulation,         2,      0.74,   0.73998,   0.73997,    13.364,    13.365,         0,       3.621
simulation,         3,      0.86,   0.86008,   0.86008,    16.562,    16.563,         0,      3.6009
simulation,         4,      0.92,   0.92007,    0.9201,    20.709,     20.71,         0,      3.5905
simulation,         5,      0.95,   0.95003,   0.95002,    25.395,    25.398,         0,       3.584
simulation,         6,      0.97,   0.96997,   0.96996,    33.195,    33.208,         0,      3.5806
simulation,         7,      0.98,   0.97999,   0.97994,    42.586,    42.608,         0,      3.5787
simulation,         8,       0.1,   0.10007,   0.10007,    9.6955,    9.6956,         0,      3.7475
simulation,         9,       0.2,   0.20011,   0.20012,    9.9067,    9.9068,         0,      3.7269
simulation,        10,       0.3,   0.30006,   0.30006,    10.175,    10.175,         0,       3.702
simulation,        11,       0.4,   0.40003,   0.40003,    10.544,    10.544,         0,      3.6822
simulation,        12,       0.5,   0.50012,   0.50012,    11.042,    11.042,         0,      3.6629
simulation,        13,       0.6,   0.60001,       0.6,    11.736,    11.736,         0,      3.6443
simulation,        14,       0.7,   0.69991,   0.69991,    12.784,    12.784,         0,      3.6276
simulation,        15,       0.8,   0.80005,   0.80005,    14.584,    14.585,         0,      3.6112
simulation,        16,       0.9,   0.90007,   0.90007,    18.867,    18.869,         0,      3.5938
