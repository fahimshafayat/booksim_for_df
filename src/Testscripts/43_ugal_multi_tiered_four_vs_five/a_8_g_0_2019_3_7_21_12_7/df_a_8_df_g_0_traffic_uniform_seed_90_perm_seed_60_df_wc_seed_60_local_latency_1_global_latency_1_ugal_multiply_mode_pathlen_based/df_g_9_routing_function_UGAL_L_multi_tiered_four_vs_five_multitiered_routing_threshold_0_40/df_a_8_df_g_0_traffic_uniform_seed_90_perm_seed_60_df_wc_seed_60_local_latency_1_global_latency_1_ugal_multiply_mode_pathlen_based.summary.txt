vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 9;
multitiered_routing_threshold_0 = 40;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49979,   0.49978,    11.268,    11.268,         0,      3.7483
simulation,         2,      0.74,   0.74029,   0.74028,    13.889,     13.89,         0,      3.7049
simulation,         3,      0.86,   0.86011,    0.8601,    17.992,    17.994,         0,      3.6906
simulation,         4,      0.92,   0.92007,   0.92006,    24.156,    24.158,         0,      3.6893
simulation,         5,      0.95,   0.95007,   0.95001,    36.217,    36.221,         0,      3.7089
simulation,         6,      0.97,   0.96997,   0.96995,    75.252,    75.288,         0,      3.7211
simulation,         7,      0.98,    0.9799,   0.97996,     93.53,    93.525,         0,      3.7013
simulation,         8,       0.1,  0.099933,  0.099933,    9.7427,    9.7428,         0,      3.7738
simulation,         9,       0.2,   0.19992,   0.19992,    10.045,    10.045,         0,      3.7938
simulation,        10,       0.3,   0.29991,   0.29991,    10.356,    10.356,         0,      3.7852
simulation,        11,       0.4,   0.39985,   0.39984,    10.742,    10.742,         0,      3.7674
simulation,        12,       0.5,   0.49979,   0.49978,    11.268,    11.268,         0,      3.7483
simulation,        13,       0.6,   0.59997,   0.59996,    12.013,    12.013,         0,      3.7282
simulation,        14,       0.7,   0.70018,   0.70017,    13.191,    13.192,         0,      3.7113
simulation,        15,       0.8,   0.80023,   0.80024,    15.369,     15.37,         0,      3.6969
simulation,        16,       0.9,   0.90009,   0.90005,    21.272,    21.273,         0,      3.6884
