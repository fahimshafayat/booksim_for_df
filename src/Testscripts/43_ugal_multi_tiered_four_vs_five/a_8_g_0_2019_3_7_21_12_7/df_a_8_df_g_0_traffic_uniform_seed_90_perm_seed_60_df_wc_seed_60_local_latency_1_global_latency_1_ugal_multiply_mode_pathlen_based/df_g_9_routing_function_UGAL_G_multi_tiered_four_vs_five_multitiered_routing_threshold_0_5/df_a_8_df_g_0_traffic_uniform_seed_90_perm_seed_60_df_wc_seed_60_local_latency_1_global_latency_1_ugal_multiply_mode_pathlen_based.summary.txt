vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 9;
multitiered_routing_threshold_0 = 5;
routing_function = UGAL_G_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49978,   0.49978,    11.053,    11.053,         0,      3.6677
simulation,         2,      0.74,   0.74028,   0.74028,    13.383,    13.384,         0,      3.6311
simulation,         3,      0.86,    0.8601,    0.8601,     16.57,    16.571,         0,      3.6142
simulation,         4,      0.92,   0.92005,   0.92006,    20.679,     20.68,         0,      3.6058
simulation,         5,      0.95,   0.95001,   0.95001,    25.599,    25.604,         0,      3.6022
simulation,         6,      0.97,   0.96994,   0.96995,    33.034,    33.044,         0,      3.5993
simulation,         7,      0.98,    0.9799,   0.97997,    41.816,    41.844,         0,      3.5976
simulation,         8,       0.1,  0.099934,  0.099933,    9.7011,    9.7012,         0,      3.7501
simulation,         9,       0.2,   0.19992,   0.19992,    9.9087,    9.9089,         0,      3.7271
simulation,        10,       0.3,   0.29992,   0.29991,     10.18,     10.18,         0,       3.704
simulation,        11,       0.4,   0.39984,   0.39984,    10.552,    10.552,         0,      3.6859
simulation,        12,       0.5,   0.49978,   0.49978,    11.053,    11.053,         0,      3.6677
simulation,        13,       0.6,   0.59997,   0.59996,    11.752,    11.752,         0,      3.6524
simulation,        14,       0.7,   0.70019,   0.70017,    12.794,    12.795,         0,      3.6367
simulation,        15,       0.8,   0.80024,   0.80024,     14.59,    14.591,         0,      3.6223
simulation,        16,       0.9,   0.90008,   0.90005,     18.85,    18.851,         0,      3.6092
