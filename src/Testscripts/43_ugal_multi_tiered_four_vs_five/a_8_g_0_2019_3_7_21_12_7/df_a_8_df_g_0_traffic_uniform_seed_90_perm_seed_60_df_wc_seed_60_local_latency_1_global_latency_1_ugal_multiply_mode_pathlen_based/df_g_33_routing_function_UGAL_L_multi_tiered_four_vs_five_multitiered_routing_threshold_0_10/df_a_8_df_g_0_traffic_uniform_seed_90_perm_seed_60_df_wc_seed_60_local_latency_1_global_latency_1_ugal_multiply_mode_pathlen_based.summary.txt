vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 33;
multitiered_routing_threshold_0 = 10;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49998,   0.49998,    11.917,    11.917,         0,      3.9356
simulation,         2,      0.74,   0.74003,   0.74003,    15.448,    15.448,         0,      3.8897
simulation,         3,      0.86,   0.85999,   0.85998,    22.888,    22.889,         0,      3.8849
simulation,         4,      0.92,   0.92005,   0.92004,    44.765,     44.77,         0,      3.9065
simulation,         5,      0.95,   0.95001,   0.95005,    81.915,    81.923,         0,       3.852
simulation,         6,      0.97,    0.9697,   0.97002,    113.93,    114.03,         0,      3.7941
simulation,         7,      0.98,   0.95882,   0.96269,    513.43,     292.1,         1,         0.0
simulation,         8,       0.1,   0.10012,   0.10012,    10.171,    10.171,         0,      3.9753
simulation,         9,       0.2,   0.20009,   0.20009,    10.511,    10.511,         0,      3.9981
simulation,        10,       0.3,   0.30005,   0.30005,    10.862,    10.862,         0,      3.9845
simulation,        11,       0.4,   0.40005,   0.40005,    11.307,    11.307,         0,      3.9615
simulation,        12,       0.5,   0.49998,   0.49998,    11.917,    11.917,         0,      3.9356
simulation,        13,       0.6,   0.60002,   0.60002,    12.842,    12.842,         0,      3.9127
simulation,        14,       0.7,   0.70001,       0.7,    14.428,    14.429,         0,       3.895
simulation,        15,       0.8,   0.79998,   0.79998,    17.869,     17.87,         0,      3.8841
simulation,        16,       0.9,   0.90001,   0.90001,    31.141,    31.144,         0,      3.8924
