vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 33;
multitiered_routing_threshold_0 = 60;
routing_function = UGAL_G_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49998,   0.49998,    11.629,    11.629,         0,      3.8403
simulation,         2,      0.74,   0.74003,   0.74003,    14.452,    14.452,         0,      3.7893
simulation,         3,      0.86,      0.86,   0.85998,    18.714,    18.714,         0,      3.7636
simulation,         4,      0.92,   0.92005,   0.92004,    24.786,    24.787,         0,      3.7478
simulation,         5,      0.95,   0.95005,   0.95005,    32.462,    32.465,         0,      3.7367
simulation,         6,      0.97,   0.97002,   0.97003,    44.332,    44.342,         0,      3.7269
simulation,         7,      0.98,   0.97999,   0.98001,    57.089,    57.114,         0,      3.7215
simulation,         8,       0.1,   0.10012,   0.10012,    10.126,    10.126,         0,      3.9503
simulation,         9,       0.2,   0.20009,   0.20009,    10.345,    10.345,         0,      3.9189
simulation,        10,       0.3,   0.30005,   0.30005,    10.645,    10.645,         0,      3.8898
simulation,        11,       0.4,   0.40005,   0.40005,     11.06,     11.06,         0,      3.8643
simulation,        12,       0.5,   0.49998,   0.49998,    11.629,    11.629,         0,      3.8403
simulation,        13,       0.6,   0.60002,   0.60002,    12.446,    12.447,         0,      3.8182
simulation,        14,       0.7,       0.7,       0.7,    13.712,    13.712,         0,      3.7974
simulation,        15,       0.8,   0.79998,   0.79998,    16.021,    16.021,         0,      3.7769
simulation,        16,       0.9,   0.89999,   0.90001,    22.029,     22.03,         0,      3.7537
