vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 9;
multitiered_routing_threshold_0 = 5;
routing_function = UGAL_L_multi_tiered_four_vs_five;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49979,   0.49978,    11.308,    11.308,         0,      3.7628
simulation,         2,      0.74,   0.74028,   0.74028,    13.979,    13.979,         0,      3.7365
simulation,         3,      0.86,    0.8601,    0.8601,    17.765,    17.766,         0,      3.7146
simulation,         4,      0.92,   0.92007,   0.92006,    22.834,    22.837,         0,      3.7007
simulation,         5,      0.95,   0.95004,   0.95001,    28.775,    28.782,         0,      3.6928
simulation,         6,      0.97,   0.96991,   0.96995,    37.939,    37.947,         0,      3.6886
simulation,         7,      0.98,   0.97993,   0.97997,    48.036,    48.055,         0,      3.6873
simulation,         8,       0.1,  0.099933,  0.099933,    9.7449,     9.745,         0,      3.7738
simulation,         9,       0.2,   0.19992,   0.19992,    10.048,    10.048,         0,      3.7957
simulation,        10,       0.3,   0.29991,   0.29991,    10.362,    10.362,         0,      3.7877
simulation,        11,       0.4,   0.39985,   0.39984,    10.764,    10.764,         0,      3.7753
simulation,        12,       0.5,   0.49979,   0.49978,    11.308,    11.308,         0,      3.7628
simulation,        13,       0.6,   0.59997,   0.59996,    12.085,    12.086,         0,      3.7515
simulation,        14,       0.7,   0.70018,   0.70017,    13.278,    13.278,         0,      3.7404
simulation,        15,       0.8,   0.80024,   0.80024,    15.409,    15.409,         0,      3.7266
simulation,        16,       0.9,   0.90005,   0.90005,     20.58,    20.581,         0,      3.7054
