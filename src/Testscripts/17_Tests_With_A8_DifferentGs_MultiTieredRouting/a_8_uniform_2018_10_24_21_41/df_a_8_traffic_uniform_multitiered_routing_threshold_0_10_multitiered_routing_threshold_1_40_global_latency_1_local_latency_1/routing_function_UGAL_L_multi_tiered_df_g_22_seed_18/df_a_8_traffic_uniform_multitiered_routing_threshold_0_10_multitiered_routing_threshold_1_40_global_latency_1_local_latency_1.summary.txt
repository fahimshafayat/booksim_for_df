vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 1000;
df_a = 8;
df_arrangement = absolute_improved;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 22;
routing_function = UGAL_L_multi_tiered;
seed = 18;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,      0.01, 0.0099991,    9.4601,    9.4608,         0,  39923,  2230,  |4-> 38356 , |5-> 0 , |6-> 0 ,,  |4-> 2132 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 1704 , |3-> 4321 , |4-> 33898 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 105 , |3-> 261 , |4-> 1864 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.70556,   0.73699,    611.35,    532.63,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,   0.49993,   0.49993,    11.725,    11.726,         0,  1773986,  332588,  |4-> 1703908 , |5-> 413 , |6-> 0 ,,  |4-> 315528 , |5-> 2522 , |6-> 0 ,,  |1-> 0 , |2-> 74997 , |3-> 192968 , |4-> 1505719 , |5-> 302 , |6-> 0 ,,  |1-> 0 , |2-> 15518 , |3-> 35169 , |4-> 280047 , |5-> 1854 , |6-> 0 ,
simulation,         4,      0.74,   0.71607,   0.72853,    218.25,    189.39,         0,  5553256,  1706256,  |4-> 2329612 , |5-> 2117600 , |6-> 869862 ,,  |4-> 197775 , |5-> 903985 , |6-> 550024 ,,  |1-> 0 , |2-> 244917 , |3-> 311049 , |4-> 2606672 , |5-> 1979032 , |6-> 411586 ,,  |1-> 0 , |2-> 57018 , |3-> 69258 , |4-> 510689 , |5-> 871240 , |6-> 198051 ,
simulation,         5,      0.86,   0.70881,   0.73258,    499.09,     468.5,         1,  0,  0,  ,  ,  ,  
simulation,         6,       0.8,   0.71049,   0.73167,     512.2,    376.03,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.77,   0.71432,    0.7317,    354.14,    274.85,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.75,   0.71645,   0.73136,    295.99,    233.43,         0,  5436869,  1722799,  |4-> 2062479 , |5-> 2081144 , |6-> 1062883 ,,  |4-> 165091 , |5-> 863650 , |6-> 637422 ,,  |1-> 0 , |2-> 238120 , |3-> 281808 , |4-> 2382599 , |5-> 2030967 , |6-> 503375 ,,  |1-> 0 , |2-> 58966 , |3-> 66013 , |4-> 491356 , |5-> 881609 , |6-> 224855 ,
simulation,         9,      0.76,   0.71572,   0.73261,    384.79,    277.36,         0,  5633794,  1849858,  |4-> 1941066 , |5-> 2149010 , |6-> 1307419 ,,  |4-> 145787 , |5-> 874137 , |6-> 767439 ,,  |1-> 0 , |2-> 243965 , |3-> 273075 , |4-> 2311969 , |5-> 2184280 , |6-> 620505 ,,  |1-> 0 , |2-> 64861 , |3-> 67226 , |4-> 502214 , |5-> 947313 , |6-> 268244 ,
simulation,        11,       0.7,   0.69947,   0.69991,    22.637,     22.71,         0,  2543709,  451612,  |4-> 2372443 , |5-> 67240 , |6-> 61 ,,  |4-> 291302 , |5-> 117948 , |6-> 26409 ,,  |1-> 0 , |2-> 111439 , |3-> 269711 , |4-> 2113962 , |5-> 48597 , |6-> 0 ,,  |1-> 0 , |2-> 17017 , |3-> 35795 , |4-> 285377 , |5-> 101820 , |6-> 11603 ,
simulation,        12,      0.65,   0.65016,   0.65015,    14.286,    14.288,         0,  2359643,  388350,  |4-> 2242938 , |5-> 22130 , |6-> 0 ,,  |4-> 320986 , |5-> 51815 , |6-> 70 ,,  |1-> 0 , |2-> 101456 , |3-> 254182 , |4-> 1987634 , |5-> 16371 , |6-> 0 ,,  |1-> 0 , |2-> 16513 , |3-> 36935 , |4-> 295365 , |5-> 39506 , |6-> 31 ,
simulation,        13,       0.6,   0.60001,   0.59997,    12.949,    12.951,         0,  2165798,  366455,  |4-> 2073273 , |5-> 6712 , |6-> 0 ,,  |4-> 330807 , |5-> 20296 , |6-> 0 ,,  |1-> 0 , |2-> 92197 , |3-> 235278 , |4-> 1833347 , |5-> 4976 , |6-> 0 ,,  |1-> 0 , |2-> 16368 , |3-> 37410 , |4-> 297325 , |5-> 15352 , |6-> 0 ,
