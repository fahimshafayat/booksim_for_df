vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 1000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 33;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = randperm;
df_g = 9;
perm_seed = 64;
routing_function = UGAL_L;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099514, 0.0099606,    9.4771,    9.4768,         0,  16117,  1012,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 1430 , |3-> 93 , |4-> 1064 , |5-> 5059 , |6-> 8471 ,,  |1-> 0 , |2-> 138 , |3-> 4 , |4-> 57 , |5-> 310 , |6-> 503 ,
simulation,         2,      0.99,   0.73635,   0.76632,    515.21,     473.8,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,   0.49953,   0.49948,    12.344,    12.349,         0,  697533,  164497,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 52652 , |3-> 4036 , |4-> 44854 , |5-> 215356 , |6-> 380635 ,,  |1-> 0 , |2-> 22932 , |3-> 1102 , |4-> 11559 , |5-> 50328 , |6-> 78576 ,
simulation,         4,      0.74,   0.72612,   0.72904,    194.28,    114.58,         0,  2903951,  1050500,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 197585 , |3-> 14742 , |4-> 170571 , |5-> 873601 , |6-> 1647452 ,,  |1-> 0 , |2-> 154214 , |3-> 9291 , |4-> 89527 , |5-> 342077 , |6-> 455391 ,
simulation,         5,      0.86,   0.73633,    0.7531,    438.14,    377.83,         1,  0,  0,  ,  ,  ,  
simulation,         6,       0.8,   0.73836,   0.74823,    379.06,    262.55,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.77,   0.73434,   0.74043,    426.59,    203.84,         0,  3094061,  1219504,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 205010 , |3-> 14599 , |4-> 175346 , |5-> 923371 , |6-> 1775735 ,,  |1-> 0 , |2-> 184351 , |3-> 11106 , |4-> 106543 , |5-> 400853 , |6-> 516651 ,
simulation,         8,      0.78,    0.7366,   0.74353,    287.08,    206.03,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.75,   0.72993,   0.73438,     250.2,     142.3,         0,  3052986,  1146691,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 203939 , |3-> 15226 , |4-> 176779 , |5-> 916745 , |6-> 1740297 ,,  |1-> 0 , |2-> 169701 , |3-> 10203 , |4-> 98264 , |5-> 374031 , |6-> 494492 ,
simulation,        10,       0.7,   0.69742,   0.69865,    54.388,    47.329,         0,  1906940,  569164,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 134771 , |3-> 10597 , |4-> 118287 , |5-> 583395 , |6-> 1059890 ,,  |1-> 0 , |2-> 82415 , |3-> 4502 , |4-> 44411 , |5-> 179126 , |6-> 258710 ,
simulation,        11,      0.65,   0.64991,   0.64995,    21.938,     22.09,         0,  922754,  232256,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 67670 , |3-> 5267 , |4-> 58504 , |5-> 285211 , |6-> 506102 ,,  |1-> 0 , |2-> 33587 , |3-> 1718 , |4-> 17184 , |5-> 71861 , |6-> 107906 ,
simulation,        12,       0.6,   0.59964,   0.59973,     16.59,    16.641,         0,  848456,  199110,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 63291 , |3-> 4881 , |4-> 54440 , |5-> 261501 , |6-> 464343 ,,  |1-> 0 , |2-> 28531 , |3-> 1398 , |4-> 14497 , |5-> 61116 , |6-> 93568 ,
