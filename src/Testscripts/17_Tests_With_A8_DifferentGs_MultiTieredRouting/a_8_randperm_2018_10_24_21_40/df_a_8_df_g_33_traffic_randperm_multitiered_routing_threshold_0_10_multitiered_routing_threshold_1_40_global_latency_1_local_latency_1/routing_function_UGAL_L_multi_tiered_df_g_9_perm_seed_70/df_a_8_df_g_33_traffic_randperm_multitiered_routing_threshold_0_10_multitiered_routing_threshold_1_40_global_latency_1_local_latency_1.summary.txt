vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 1000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 33;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = randperm;
df_g = 9;
perm_seed = 70;
routing_function = UGAL_L_multi_tiered;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099514, 0.0099606,    9.2125,    9.2128,         0,  16115,  953,  |4-> 14498 , |5-> 0 , |6-> 0 ,,  |4-> 813 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 1689 , |3-> 2088 , |4-> 12338 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 142 , |3-> 104 , |4-> 707 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,    0.8142,   0.82486,    505.94,    418.11,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,   0.49952,   0.49948,    11.147,    11.149,         0,  682728,  175203,  |4-> 623334 , |5-> 178 , |6-> 0 ,,  |4-> 142376 , |5-> 1334 , |6-> 0 ,,  |1-> 0 , |2-> 62154 , |3-> 94035 , |4-> 526479 , |5-> 60 , |6-> 0 ,,  |1-> 0 , |2-> 32168 , |3-> 21497 , |4-> 121123 , |5-> 415 , |6-> 0 ,
simulation,         4,      0.74,   0.73947,   0.73989,    24.403,    24.608,         0,  1050601,  298317,  |4-> 871171 , |5-> 91918 , |6-> 2991 ,,  |4-> 130582 , |5-> 99349 , |6-> 10785 ,,  |1-> 0 , |2-> 89239 , |3-> 138094 , |4-> 786362 , |5-> 36724 , |6-> 182 ,,  |1-> 0 , |2-> 58482 , |3-> 30746 , |4-> 169556 , |5-> 38642 , |6-> 891 ,
simulation,         5,      0.86,     0.823,   0.82896,    400.97,     179.7,         0,  3259765,  1452592,  |4-> 879191 , |5-> 1442783 , |6-> 694629 ,,  |4-> 41662 , |5-> 596441 , |6-> 552758 ,,  |1-> 0 , |2-> 253438 , |3-> 302503 , |4-> 1844290 , |5-> 811990 , |6-> 47544 ,,  |1-> 0 , |2-> 265673 , |3-> 125210 , |4-> 659288 , |5-> 382573 , |6-> 19848 ,
simulation,         6,      0.92,   0.81311,   0.82603,     468.0,    318.25,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.89,   0.81879,    0.8268,    373.53,    245.46,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.87,   0.82368,   0.83116,    270.43,    186.56,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.85,   0.82295,   0.82643,    310.18,    143.84,         0,  2921952,  1273344,  |4-> 899578 , |5-> 1299902 , |6-> 504107 ,,  |4-> 46568 , |5-> 571922 , |6-> 422552 ,,  |1-> 0 , |2-> 228069 , |3-> 278607 , |4-> 1693971 , |5-> 685895 , |6-> 35410 ,,  |1-> 0 , |2-> 235723 , |3-> 109541 , |4-> 580603 , |5-> 332433 , |6-> 15044 ,
simulation,        10,       0.8,   0.79558,   0.79636,    74.385,    55.673,         0,  2325272,  825935,  |4-> 1419939 , |5-> 669206 , |6-> 55000 ,,  |4-> 132123 , |5-> 446383 , |6-> 95590 ,,  |1-> 0 , |2-> 190472 , |3-> 270498 , |4-> 1583712 , |5-> 276578 , |6-> 4012 ,,  |1-> 0 , |2-> 154105 , |3-> 75265 , |4-> 405374 , |5-> 186162 , |6-> 5029 ,
simulation,        11,      0.75,   0.74924,   0.75008,    27.843,    28.412,         0,  1183602,  343982,  |4-> 962529 , |5-> 120651 , |6-> 6052 ,,  |4-> 137082 , |5-> 124651 , |6-> 15826 ,,  |1-> 0 , |2-> 99664 , |3-> 154705 , |4-> 880586 , |5-> 48266 , |6-> 381 ,,  |1-> 0 , |2-> 67410 , |3-> 34804 , |4-> 191529 , |5-> 49115 , |6-> 1124 ,
simulation,        12,       0.7,       0.7,   0.70028,    19.368,    19.477,         0,  972433,  254696,  |4-> 855064 , |5-> 38044 , |6-> 0 ,,  |4-> 148026 , |5-> 50325 , |6-> 6486 ,,  |1-> 0 , |2-> 83508 , |3-> 131499 , |4-> 742866 , |5-> 14560 , |6-> 0 ,,  |1-> 0 , |2-> 50577 , |3-> 28054 , |4-> 155457 , |5-> 19885 , |6-> 723 ,
