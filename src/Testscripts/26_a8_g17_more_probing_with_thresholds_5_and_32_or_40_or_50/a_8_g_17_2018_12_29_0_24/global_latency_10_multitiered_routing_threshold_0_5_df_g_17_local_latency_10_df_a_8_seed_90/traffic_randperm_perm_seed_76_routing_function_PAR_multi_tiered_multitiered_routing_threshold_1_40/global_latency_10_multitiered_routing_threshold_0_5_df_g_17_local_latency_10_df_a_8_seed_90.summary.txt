alloc_iters = 1;
sw_alloc_delay = 1;
injection_rate_uses_flits = 1;
vc_buf_size = 64;
sim_count = 1;
num_vcs = 7;
priority = none;
sample_period = 10000;
wait_for_tail_credit = 0;
output_speedup = 1;
input_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
packet_size = 1;
routing_delay = 0;
st_final_delay = 1;
credit_delay = 2;
sw_allocator = separable_input_first;
vc_alloc_delay = 1;
vc_allocator = separable_input_first;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 5;
seed = 90;
topology = dragonflyfull;
multitiered_routing_threshold_1 = 40;
perm_seed = 76;
routing_function = PAR_multi_tiered;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49997,   0.49997,    42.928,    42.935,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.64465,   0.67746,    517.95,    391.69,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.61995,   0.61995,    55.139,    55.161,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.68,    0.6501,   0.65072,    828.04,    312.58,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.65,   0.65002,   0.65001,    72.938,    72.972,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.66,   0.65777,   0.65772,    321.26,    134.05,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.67,   0.65336,   0.65373,    497.72,    253.09,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.6,   0.59994,   0.59993,     51.06,    51.074,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.55,   0.54992,   0.54993,     45.52,    45.529,         0,  0,  0,  ,  ,  ,  
