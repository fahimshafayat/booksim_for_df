alloc_iters = 1;
sw_alloc_delay = 1;
injection_rate_uses_flits = 1;
vc_buf_size = 64;
sim_count = 1;
num_vcs = 7;
priority = none;
sample_period = 10000;
wait_for_tail_credit = 0;
output_speedup = 1;
input_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
packet_size = 1;
routing_delay = 0;
st_final_delay = 1;
credit_delay = 2;
sw_allocator = separable_input_first;
vc_alloc_delay = 1;
vc_allocator = separable_input_first;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 5;
seed = 90;
topology = dragonflyfull;
multitiered_routing_threshold_1 = 40;
routing_function = PAR_multi_tiered;
shift_by_group = 4;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.46717,   0.47793,    548.54,    346.75,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,     57.83,    57.834,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36998,   0.36997,      74.9,    74.903,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42991,   0.42991,     93.22,    93.236,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45579,   0.45574,    509.75,    180.09,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.43987,   0.43992,    99.905,    99.979,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.45,   0.44811,   0.44813,    395.59,    149.42,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39992,   0.39994,    83.228,    83.236,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35001,   0.35001,    71.333,    71.335,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,       0.3,   0.30001,    63.236,    63.241,         0,  0,  0,  ,  ,  ,  
