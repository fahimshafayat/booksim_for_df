alloc_iters = 1;
sw_alloc_delay = 1;
injection_rate_uses_flits = 1;
vc_buf_size = 64;
sim_count = 1;
num_vcs = 7;
priority = none;
sample_period = 10000;
wait_for_tail_credit = 0;
output_speedup = 1;
input_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
packet_size = 1;
routing_delay = 0;
st_final_delay = 1;
credit_delay = 2;
sw_allocator = separable_input_first;
vc_alloc_delay = 1;
vc_allocator = separable_input_first;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 5;
seed = 90;
topology = dragonflyfull;
multitiered_routing_threshold_1 = 32;
routing_function = PAR_multi_tiered;
shift_by_group = 9;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4654,   0.47697,    591.16,     376.0,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,    59.509,    59.515,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36998,   0.36997,    75.611,    75.613,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,    0.4299,   0.42991,    97.092,    97.102,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45566,   0.45573,    507.76,    203.44,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.43982,   0.43985,     130.4,    121.36,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.45,   0.44811,   0.44819,    406.16,    159.61,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39993,   0.39994,    85.232,    85.233,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35002,   0.35001,    71.887,    71.889,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30001,   0.30001,     65.57,    65.574,         0,  0,  0,  ,  ,  ,  
