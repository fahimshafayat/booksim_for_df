alloc_iters = 1;
sw_alloc_delay = 1;
injection_rate_uses_flits = 1;
vc_buf_size = 64;
sim_count = 1;
num_vcs = 7;
priority = none;
sample_period = 10000;
wait_for_tail_credit = 0;
output_speedup = 1;
input_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
packet_size = 1;
routing_delay = 0;
st_final_delay = 1;
credit_delay = 2;
sw_allocator = separable_input_first;
vc_alloc_delay = 1;
vc_allocator = separable_input_first;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 5;
seed = 90;
topology = dragonflyfull;
multitiered_routing_threshold_1 = -1;
routing_function = PAR;
shift_by_group = 4;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.43928,   0.46812,    532.15,    448.18,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,    67.392,    67.398,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36998,   0.36997,    93.606,    93.613,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42696,   0.42696,    487.12,    173.76,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.39994,   0.39994,    105.11,     105.1,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.41,   0.40964,   0.40969,    136.82,    122.14,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.42,   0.41878,   0.41877,    327.14,    147.41,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.35,      0.35,   0.35001,    85.519,    85.523,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.3,   0.30001,   0.30001,    74.254,    74.255,         0,  0,  0,  ,  ,  ,  
