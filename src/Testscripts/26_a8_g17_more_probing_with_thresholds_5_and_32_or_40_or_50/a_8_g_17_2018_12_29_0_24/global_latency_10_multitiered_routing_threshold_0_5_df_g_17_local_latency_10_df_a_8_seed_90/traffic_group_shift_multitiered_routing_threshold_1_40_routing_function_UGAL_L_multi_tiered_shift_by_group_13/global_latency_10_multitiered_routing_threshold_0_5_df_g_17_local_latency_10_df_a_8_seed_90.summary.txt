alloc_iters = 1;
sw_alloc_delay = 1;
injection_rate_uses_flits = 1;
vc_buf_size = 64;
sim_count = 1;
num_vcs = 7;
priority = none;
sample_period = 10000;
wait_for_tail_credit = 0;
output_speedup = 1;
input_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
packet_size = 1;
routing_delay = 0;
st_final_delay = 1;
credit_delay = 2;
sw_allocator = separable_input_first;
vc_alloc_delay = 1;
vc_allocator = separable_input_first;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 5;
seed = 90;
topology = dragonflyfull;
multitiered_routing_threshold_1 = 40;
routing_function = UGAL_L_multi_tiered;
shift_by_group = 13;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.38869,   0.42435,    921.86,    674.41,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,    89.369,    89.488,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.34813,   0.34992,    947.84,    651.58,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31001,   0.31001,    98.808,    99.039,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,   0.33892,   0.33928,    249.76,    205.46,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.35,    0.3436,   0.34502,    478.97,     390.1,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,       0.3,   0.30001,    96.185,    96.385,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20007,   0.20007,    83.452,     83.51,         0,  0,  0,  ,  ,  ,  
