alloc_iters = 1;
sw_alloc_delay = 1;
injection_rate_uses_flits = 1;
vc_buf_size = 64;
sim_count = 1;
num_vcs = 7;
priority = none;
sample_period = 10000;
wait_for_tail_credit = 0;
output_speedup = 1;
input_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
packet_size = 1;
routing_delay = 0;
st_final_delay = 1;
credit_delay = 2;
sw_allocator = separable_input_first;
vc_alloc_delay = 1;
vc_allocator = separable_input_first;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 5;
seed = 90;
topology = dragonflyfull;
multitiered_routing_threshold_1 = -1;
routing_function = PAR;
shift_by_group = 13;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.44546,   0.47295,    470.72,    416.77,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,    67.306,    67.311,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36998,   0.36997,    90.946,     90.95,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42939,   0.42939,    187.12,    124.95,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,    0.4471,   0.44786,    477.96,    272.08,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.43828,   0.43832,    396.61,     147.2,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.45,   0.44528,   0.44551,    495.77,     187.3,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39994,   0.39994,    100.95,    100.95,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35002,   0.35001,     84.36,    84.363,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,       0.3,   0.30001,    74.025,    74.027,         0,  0,  0,  ,  ,  ,  
