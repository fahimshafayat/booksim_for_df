alloc_iters = 1;
sw_alloc_delay = 1;
injection_rate_uses_flits = 1;
vc_buf_size = 64;
sim_count = 1;
num_vcs = 7;
priority = none;
sample_period = 10000;
wait_for_tail_credit = 0;
output_speedup = 1;
input_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
packet_size = 1;
routing_delay = 0;
st_final_delay = 1;
credit_delay = 2;
sw_allocator = separable_input_first;
vc_alloc_delay = 1;
vc_allocator = separable_input_first;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 5;
seed = 90;
topology = dragonflyfull;
multitiered_routing_threshold_1 = 50;
routing_function = PAR_multi_tiered;
shift_by_group = 1;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.47041,   0.47876,    491.03,    292.48,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,    57.623,    57.627,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36997,   0.36997,    73.395,    73.397,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42994,   0.42991,    92.121,    92.169,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45779,   0.45783,    358.68,    143.65,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.43952,   0.43959,    134.53,    105.45,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.45,   0.44886,   0.44886,     273.2,    113.37,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39993,   0.39994,     82.16,    82.168,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35002,   0.35001,    68.726,    68.729,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30001,   0.30001,    61.246,    61.249,         0,  0,  0,  ,  ,  ,  
