wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 51;
perm_seed = 51;
routing_function = UGAL_L_three_hop_restricted;
seed = 81;
shift_by_group = 51;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49989,   0.49989,    49.849,    49.891,         0,      3.9288
simulation,         2,      0.74,   0.58654,   0.64024,    931.54,    661.24,         1,         0.0
simulation,         3,      0.62,   0.62013,   0.62015,    83.425,    83.558,         0,      3.9419
simulation,         4,      0.68,    0.6017,   0.65383,    502.45,    467.54,         1,         0.0
simulation,         5,      0.65,   0.60409,   0.62889,    549.04,    475.55,         1,         0.0
simulation,         6,      0.63,   0.60387,   0.60571,    656.39,    426.16,         1,         0.0
simulation,         7,      0.15,   0.15003,   0.15003,    10.134,    10.134,         0,      3.8716
simulation,         8,       0.3,   0.30009,   0.30009,    11.064,    11.064,         0,      3.8837
simulation,         9,      0.45,   0.44992,   0.44991,    47.619,    47.654,         0,      3.9193
simulation,        10,       0.6,   0.60011,    0.6001,    59.925,    60.007,         0,      3.9404
