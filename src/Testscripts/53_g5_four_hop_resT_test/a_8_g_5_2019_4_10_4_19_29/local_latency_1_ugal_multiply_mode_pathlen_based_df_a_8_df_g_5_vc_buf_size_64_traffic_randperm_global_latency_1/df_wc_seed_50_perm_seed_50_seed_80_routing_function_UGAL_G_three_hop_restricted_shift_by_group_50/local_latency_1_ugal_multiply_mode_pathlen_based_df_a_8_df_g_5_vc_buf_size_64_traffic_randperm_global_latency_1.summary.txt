injection_rate_uses_flits = 1;
credit_delay = 2;
internal_speedup = 4.0;
packet_size = 1;
wait_for_tail_credit = 0;
vc_alloc_delay = 1;
priority = none;
warmup_periods = 3;
st_final_delay = 1;
output_speedup = 1;
vc_allocator = separable_input_first;
num_vcs = 8;
alloc_iters = 1;
routing_delay = 0;
sim_count = 1;
sw_alloc_delay = 1;
sw_allocator = separable_input_first;
input_speedup = 1;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_G_three_hop_restricted;
seed = 80;
shift_by_group = 50;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4996,    0.4996,    10.412,    10.412,         0,      3.4927
simulation,         2,      0.74,   0.73946,   0.73946,    12.188,    12.188,         0,      3.4786
simulation,         3,      0.86,    0.8595,    0.8595,    14.197,    14.197,         0,      3.4769
simulation,         4,      0.92,   0.91969,   0.91968,    16.268,    16.269,         0,      3.4811
simulation,         5,      0.95,   0.94972,   0.94972,    18.035,    18.035,         0,      3.4858
simulation,         6,      0.97,   0.96986,   0.96987,     19.81,    19.811,         0,        3.49
simulation,         7,      0.98,   0.97991,   0.97992,     21.02,    21.021,         0,       3.492
simulation,         8,       0.2,   0.19965,   0.19966,     9.378,    9.3782,         0,      3.5104
simulation,         9,       0.4,   0.39959,   0.39959,    9.9699,    9.9701,         0,      3.4985
simulation,        10,       0.6,   0.59936,   0.59936,    10.986,    10.986,         0,      3.4864
simulation,        11,       0.8,   0.79956,   0.79956,    12.996,    12.996,         0,      3.4766
