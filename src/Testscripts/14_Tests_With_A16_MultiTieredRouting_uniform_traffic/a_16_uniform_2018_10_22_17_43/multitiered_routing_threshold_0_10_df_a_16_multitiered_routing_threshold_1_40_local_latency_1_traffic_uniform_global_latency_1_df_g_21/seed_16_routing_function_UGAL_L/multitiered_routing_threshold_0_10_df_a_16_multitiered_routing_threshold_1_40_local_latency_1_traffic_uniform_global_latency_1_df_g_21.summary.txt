priority = none;
sw_alloc_delay = 1;
wait_for_tail_credit = 0;
vc_buf_size = 64;
injection_rate_uses_flits = 1;
input_speedup = 1;
packet_size = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sample_period = 1000;
num_vcs = 7;
routing_delay = 0;
internal_speedup = 4.0;
sim_count = 1;
alloc_iters = 1;
st_final_delay = 1;
warmup_periods = 3;
output_speedup = 1;
credit_delay = 2;
vc_alloc_delay = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
routing_function = UGAL_L;
seed = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099722, 0.0099747,    9.8889,    9.8899,         0,  152468,  8675,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 6670 , |3-> 131 , |4-> 2989 , |5-> 29891 , |6-> 112787 ,,  |1-> 0 , |2-> 424 , |3-> 2 , |4-> 159 , |5-> 1719 , |6-> 6371 ,
simulation,         2,      0.99,   0.93828,   0.95923,    349.25,     278.6,         0,  29251242,  2164175,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 1341380 , |3-> 16814 , |4-> 454681 , |5-> 5275086 , |6-> 22163281 ,,  |1-> 0 , |2-> 66752 , |3-> 9553 , |4-> 156758 , |5-> 919851 , |6-> 1011261 ,
