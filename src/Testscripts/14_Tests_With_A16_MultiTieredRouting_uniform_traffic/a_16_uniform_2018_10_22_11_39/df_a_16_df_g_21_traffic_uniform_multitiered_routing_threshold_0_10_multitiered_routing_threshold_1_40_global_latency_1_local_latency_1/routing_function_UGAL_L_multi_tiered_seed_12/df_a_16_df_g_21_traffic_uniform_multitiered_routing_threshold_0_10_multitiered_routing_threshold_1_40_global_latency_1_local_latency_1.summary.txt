vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 1000;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
routing_function = UGAL_L_multi_tiered;
seed = 12;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099965,  0.010003,    9.6937,    9.6937,         0,  152986,  8567,  |4-> 146052 , |5-> 0 , |6-> 0 ,,  |4-> 8189 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 7132 , |3-> 10823 , |4-> 135031 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 385 , |3-> 594 , |4-> 7588 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.95074,   0.97012,    282.96,    244.72,         0,  28753158,  3177165,  |4-> 769725 , |5-> 7800046 , |6-> 18831963 ,,  |4-> 14904 , |5-> 448229 , |6-> 2636141 ,,  |1-> 0 , |2-> 1366528 , |3-> 818155 , |4-> 11253389 , |5-> 12749878 , |6-> 2565208 ,,  |1-> 0 , |2-> 81737 , |3-> 163215 , |4-> 1623998 , |5-> 1185675 , |6-> 122540 ,
