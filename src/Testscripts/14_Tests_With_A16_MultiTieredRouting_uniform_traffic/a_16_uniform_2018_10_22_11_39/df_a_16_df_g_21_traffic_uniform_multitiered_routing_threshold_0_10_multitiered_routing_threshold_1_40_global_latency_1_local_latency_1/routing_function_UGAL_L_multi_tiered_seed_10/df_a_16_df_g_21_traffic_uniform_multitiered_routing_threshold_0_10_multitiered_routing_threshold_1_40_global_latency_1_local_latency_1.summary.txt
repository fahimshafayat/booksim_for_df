vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 1000;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
routing_function = UGAL_L_multi_tiered;
seed = 10;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010015,  0.010013,    9.6918,    9.6919,         0,  152533,  8612,  |4-> 145698 , |5-> 0 , |6-> 0 ,,  |4-> 8209 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 7042 , |3-> 10454 , |4-> 135037 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 416 , |3-> 630 , |4-> 7566 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.95149,   0.97122,     276.9,     243.7,         0,  28557545,  3138282,  |4-> 768934 , |5-> 7736985 , |6-> 18708074 ,,  |4-> 15032 , |5-> 446062 , |6-> 2600217 ,,  |1-> 0 , |2-> 1358864 , |3-> 814473 , |4-> 11175224 , |5-> 12661617 , |6-> 2547367 ,,  |1-> 0 , |2-> 80780 , |3-> 161225 , |4-> 1604109 , |5-> 1171847 , |6-> 120321 ,
