vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 1000;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
routing_function = UGAL_L_multi_tiered;
seed = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099728, 0.0099747,    9.6843,    9.6845,         0,  152673,  8420,  |4-> 145789 , |5-> 0 , |6-> 0 ,,  |4-> 7998 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 7079 , |3-> 10521 , |4-> 135073 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 430 , |3-> 581 , |4-> 7409 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.95108,   0.97012,    281.94,    243.87,         0,  28383424,  3129530,  |4-> 765961 , |5-> 7724429 , |6-> 18560070 ,,  |4-> 14802 , |5-> 441915 , |6-> 2595576 ,,  |1-> 0 , |2-> 1348120 , |3-> 809917 , |4-> 11114572 , |5-> 12583438 , |6-> 2527377 ,,  |1-> 0 , |2-> 81095 , |3-> 160006 , |4-> 1599151 , |5-> 1167990 , |6-> 121288 ,
