vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 4;
df_arrangement = absolute_improved;
seed = 11;
topology = dragonflyfull;
df_g = 5;
perm_seed = 42;
routing_function = UGAL_L_two_hop;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01, 0.0098063,  0.009775,    12.012,         0
simulation,         2,      0.99,   0.73088,    0.7644,     500.0,         1
simulation,         3,       0.5,   0.50076,   0.50092,    14.722,         0
simulation,         4,      0.74,   0.73126,   0.73904,    123.49,         0
simulation,         5,      0.86,   0.74497,     0.766,    420.51,         1
simulation,         6,       0.8,   0.74282,   0.74962,    290.71,         1
simulation,         7,      0.77,    0.7375,   0.75126,    261.14,         0
simulation,         8,      0.78,   0.73942,   0.75018,    258.08,         1
simulation,         9,      0.75,   0.73724,    0.7481,    159.62,         0
simulation,        10,       0.7,   0.70157,   0.70214,    29.666,         0
simulation,        11,      0.65,   0.65104,   0.65114,    18.088,         0
simulation,        12,       0.6,   0.60022,   0.60037,    16.203,         0
