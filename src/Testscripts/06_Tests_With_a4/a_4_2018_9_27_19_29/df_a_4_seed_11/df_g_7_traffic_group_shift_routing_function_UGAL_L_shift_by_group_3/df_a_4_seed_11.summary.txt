vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 4;
df_arrangement = absolute_improved;
seed = 11;
topology = dragonflyfull;
df_g = 7;
routing_function = UGAL_L;
shift_by_group = 3;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01, 0.0097976, 0.0098095,    14.099,         0
simulation,         2,      0.99,   0.42755,   0.67215,    459.75,         1
simulation,         3,       0.5,   0.42722,   0.44931,    495.22,         1
simulation,         4,      0.25,    0.2492,    0.2493,    20.772,         0
simulation,         5,      0.37,   0.37042,   0.37055,    49.221,         0
simulation,         6,      0.43,   0.41979,   0.42908,    238.06,         0
simulation,         7,      0.46,    0.4225,   0.43599,    397.66,         1
simulation,         8,      0.44,     0.422,   0.43459,    319.48,         0
simulation,         9,      0.45,   0.42233,   0.43747,    345.55,         1
simulation,        10,      0.45,   0.42233,   0.43747,    345.55,         1
simulation,        11,       0.4,   0.40041,   0.40026,    59.604,         0
simulation,        12,      0.35,   0.35083,   0.35097,    48.199,         0
simulation,        13,       0.3,   0.30002,   0.30023,    45.712,         0
