vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 4;
df_arrangement = absolute_improved;
seed = 11;
topology = dragonflyfull;
df_g = 5;
routing_function = UGAL_L_threshold;
shift_by_group = 3;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01, 0.0098063,  0.009775,    13.363,         0
simulation,         2,      0.99,   0.53373,   0.56112,     867.1,         1
simulation,         3,       0.5,   0.50044,   0.50092,    60.322,         0
simulation,         4,      0.74,   0.56558,   0.60795,    492.28,         1
simulation,         5,      0.62,   0.55972,   0.57348,    403.12,         1
simulation,         6,      0.56,   0.54891,   0.55901,    197.39,         0
simulation,         7,      0.59,   0.55498,   0.56606,    304.05,         1
simulation,         8,      0.57,   0.55154,   0.56085,    234.67,         0
simulation,         9,      0.58,   0.55427,   0.56299,    290.23,         0
simulation,        10,      0.55,   0.54558,   0.55036,    121.05,         0
simulation,        11,       0.5,   0.50044,   0.50092,    60.322,         0
simulation,        12,      0.45,   0.45129,   0.45132,    55.246,         0
simulation,        13,       0.4,   0.40166,   0.40158,     20.72,         0
