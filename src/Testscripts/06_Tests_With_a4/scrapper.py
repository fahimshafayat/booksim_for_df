from os import listdir
from os.path import isfile, join
import re
import operator

g_features_we_care = [ "df_a", "df_g", "topology", "df_arrangement", "vc_buf_size", "internal_speedup",  "seed",  "traffic",  "perm_seed", "shift_by_group", "routing_function"]
    

def processFile(fileName):
    
    fp = open(fileName)
    #print(fileName)
    
    '''
    Fields to grab (and how the values look):
        
        vc_buf_size = 64;
        internal_speedup = 2.0;
        df_a = 16;
        df_arrangement = absolute_improved;
        seed = 11;
        topology = dragonflyfull;
        traffic = randperm;
        df_g = 21;
        perm_seed = 42;
        routing_function = UGAL_L;
        saturation point (i_rate in config file, i_rate injected, i_rate accepted, flit latency, deadlock)
            
    '''    
    
    
    values = {}
    
    for feature in g_features_we_care:
        values[feature] = ""
    values["saturationPoint"] = 0.0
    values["irate_vs_latency"] = []
    
    results = []
    
    for line in fp:
        line = line.strip()
        
        for feature in g_features_we_care:
            if line.startswith(feature + " = "):
                values[feature] = line[line.find(" = ") + len(" = ") : line.find(";")]
            
        if line.startswith("simulation,"):
            temp = line.split(",")
            temp = temp[2:]
            temp = [float(t) for t in temp]
            results.append(temp)
    
    fp.close()
    
    #find the saturation point
    results.sort()
    results.sort(key = operator.itemgetter(4), reverse = True)
        #inefficinet, to be honest. Sorting twice is unnecessary. 
        #But python sort is stable, so taking advantage of the trick to keep the code short.
    saturationPoint = results[len(results)-1][0]
    values["saturationPoint"] = saturationPoint
    
    #save the latencies when the network is not saturated
        #the resutls are already sorted with all the saturated points coming first
        #so just find the first occurance of unsaturated point and save from there
    for idx in range(len(results)):
        if results[idx][4] == 0:
            break
    
    for ii in range(idx,len(results)):
        values["irate_vs_latency"].append((results[ii][0], results[ii][3]))
    
    #print(values)
    #print(results)
    
    #for result in results:
    #    print(result)    
    
    return values
        
    pass


def get_files():
    dirs = [d for d in listdir() if not isfile(d)]

    files = []

    for d in dirs:
        folders = [x for x in listdir(d) if not isfile(x)]
        for folder in folders:
            tempFiles = listdir(join(d,folder)) 
            files.extend(join(d,folder,f) for f in tempFiles if ".out" not in f)
    
    #    for idx,f in enumerate(files):
    #        print(idx,f)
    #    
    
    return files
    

def get_files_three_layers():
    dirs = [d for d in listdir() if not isfile(d)]

    files = []

    for d in dirs:
        dir2s = [x for x in listdir(d) if not isfile(x)]
        for dir2 in dir2s:
            dir3s = [y for y in listdir(join(d,dir2)) if not isfile(join(d,dir2))]
            for dir3 in dir3s:
                tempFiles = listdir(join(d,dir2,dir3)) 
                files.extend(join(d,dir2,dir3,f) for f in tempFiles if ".out" not in f)

    #    for idx,f in enumerate(files):
    #        print(idx,f)
    #    
    
    return files

def grab_results(files):
    results = []
    for f in files:
        values = processFile(f)
        results.append(values)
    return results
    
def get_index_of_feature(feature, feature_list = None):
    if feature_list == None:
        feature_list = g_features_we_care
    for idx in range(len(g_features_we_care)):
        if g_features_we_care[idx] == feature:
            return idx
    return -1   #feature not found
    
    
def pretty_print(results):
    #results is a list of dict
    #let's make it a list of list for easy sorting
    #because we are discarding dict keys, so the order becomes important

    resultList = []
    
    for values in results:
        
        listValues = []
         
        for feature in g_features_we_care:
            listValues.append(values[feature])
        listValues.append(values["saturationPoint"])
        listValues.append(values["irate_vs_latency"])
        
        resultList.append(listValues)

    #now sort according to our preference

    #first by traffic, then by df_g, then by seed, then by perm_seed, then by routing_function
    
    #Implementation detail: need to follow the sorting feature order in reverse.(because python sort is stable!)
    
    #the correct index of each feature is important. Get that from g_features_we_care up top.


    #sort based on routings. Order should be UGAL_L, UGAL_L_two_hop 
    order = {"UGAL_L":5, "UGAL_L_two_hop":6, "UGAL_L_threshold":7 }
    resultList.sort(key = lambda x:order[x[get_index_of_feature("routing_function")]])

    #by perm_seed
    resultList.sort(key=operator.itemgetter(get_index_of_feature("perm_seed")))
    
    #by shift_by_group
    resultList.sort(key=operator.itemgetter(get_index_of_feature("shift_by_group")))    

    #by seed
    resultList.sort(key=operator.itemgetter(get_index_of_feature("seed")))

    #by df_g
    resultList.sort(key=operator.itemgetter(get_index_of_feature("df_g")))

    #by traffic
    resultList.sort(key=operator.itemgetter(get_index_of_feature("traffic")))

    #print header
    for feature in g_features_we_care:
        print(feature, end = " , ")
    print("saturationPoint", end = " , ") 
    print("irate_vs_latency")
    
    for idx,result in enumerate(resultList):
        #print(result)
        #only the last two columns (saturationPoint, and irate_vs_latency) needs careful formatting.
        #the rest can be printed as is.
        for ii in range(len(result)-2):
            print(result[ii], end=" , ")
        #saturationPoint
        print("{:.2f}".format(result[len(result)-2]), end=" , ")
        #irate_vs_latency
        for tup in result[len(result)-1]:
            print(tup, end=" , ")
        print()

        if (idx+1) % len(order) == 0 :
            print("")
        
    pass

if __name__ == "__main__":

    files = get_files_three_layers()
        
    results = grab_results(files)
    
#    for row in results:
#        print(row)
#        
    pretty_print(results)


