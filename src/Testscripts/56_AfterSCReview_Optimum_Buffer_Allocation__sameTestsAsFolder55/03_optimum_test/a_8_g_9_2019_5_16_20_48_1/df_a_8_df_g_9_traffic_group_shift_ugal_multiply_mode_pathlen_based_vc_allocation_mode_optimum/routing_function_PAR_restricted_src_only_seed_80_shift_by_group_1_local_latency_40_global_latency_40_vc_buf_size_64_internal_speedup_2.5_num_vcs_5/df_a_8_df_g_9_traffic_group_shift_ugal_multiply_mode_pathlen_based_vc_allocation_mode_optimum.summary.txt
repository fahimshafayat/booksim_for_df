wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
global_latency = 40;
internal_speedup = 2.5;
local_latency = 40;
num_vcs = 5;
routing_function = PAR_restricted_src_only;
seed = 80;
shift_by_group = 1;
vc_buf_size = 64;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.32876,   0.35924,    1294.2,    757.35,         1,         0.0
simulation,         2,      0.25,   0.24996,   0.24998,    163.14,    163.21,         0,      4.7051
simulation,         3,      0.37,   0.33533,   0.34259,    697.93,    377.11,         1,         0.0
simulation,         4,      0.31,   0.31015,   0.31018,    168.96,    169.03,         0,      4.7246
simulation,         5,      0.34,   0.32851,   0.32819,    729.75,    290.52,         1,         0.0
simulation,         6,      0.32,   0.31921,   0.31921,     350.1,    188.04,         0,      4.7303
simulation,         7,      0.33,   0.32511,   0.32574,    528.14,    252.49,         1,         0.0
simulation,         8,       0.1,   0.10002,   0.10003,    159.86,    159.94,         0,      4.6935
simulation,         9,       0.2,   0.19996,   0.19998,    161.06,    161.14,         0,      4.6952
simulation,        10,       0.3,    0.3001,   0.30011,    166.71,    166.78,         0,      4.7201
