wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 64;
internal_speedup = 1.0;
num_vcs = 5;
routing_function = PAR_restricted_src_only;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.26617,   0.30055,    2087.2,    1027.1,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24998,    33.226,    33.227,         0,      4.8916
simulation,         3,      0.37,   0.32465,   0.33306,    917.58,    383.65,         1,         0.0
simulation,         4,      0.31,   0.29333,   0.29372,    419.15,    201.79,         1,         0.0
simulation,         5,      0.28,   0.28003,   0.28003,    38.536,    38.539,         0,       4.943
simulation,         6,      0.29,   0.29004,   0.29006,     39.91,    39.918,         0,      4.9617
simulation,         7,       0.3,   0.30009,   0.30009,    44.687,    44.721,         0,      4.9709
simulation,         8,      0.05,  0.049884,  0.049886,    21.633,    21.633,         0,      4.7785
simulation,         9,       0.1,   0.10003,   0.10003,    22.353,    22.353,         0,      4.7704
simulation,        10,      0.15,   0.15006,   0.15006,     23.71,    23.711,         0,      4.7534
simulation,        11,       0.2,   0.19999,   0.19998,    27.329,    27.332,         0,      4.7907
simulation,        12,      0.25,   0.24998,   0.24998,    33.226,    33.227,         0,      4.8916
simulation,        13,       0.3,   0.30009,   0.30009,    44.687,    44.721,         0,      4.9709
