wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
internal_speedup = 4.0;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 64;
global_latency = 1;
local_latency = 1;
num_vcs = 5;
routing_function = PAR;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46018,   0.46951,    612.52,    294.89,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24998,    15.074,    15.074,         0,      5.3961
simulation,         3,      0.37,   0.37018,   0.37017,    21.722,    21.723,         0,      5.7889
simulation,         4,      0.43,      0.43,   0.43003,    32.193,    32.193,         0,      5.9432
simulation,         5,      0.46,   0.45734,   0.45749,    290.18,    91.361,         1,         0.0
simulation,         6,      0.44,   0.44005,   0.44008,    37.935,    37.944,         0,      5.9606
simulation,         7,      0.45,   0.44875,   0.44876,    324.16,    55.705,         0,      5.9683
simulation,         8,       0.1,   0.10003,   0.10003,    13.385,    13.385,         0,      5.4284
simulation,         9,       0.2,   0.19998,   0.19998,    14.152,    14.152,         0,      5.3603
simulation,        10,       0.3,   0.30012,   0.30011,    16.982,    16.983,         0,      5.5244
simulation,        11,       0.4,   0.40001,   0.40002,    25.032,    25.032,         0,      5.8751
