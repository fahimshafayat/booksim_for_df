wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
internal_speedup = 4.0;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
num_vcs = 4;
routing_function = UGAL_G;
seed = 80;
shift_by_group = 1;
vc_buf_size = 8;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.24224,   0.24586,    2494.7,     147.2,         1,         0.0
simulation,         2,      0.25,   0.24571,   0.24581,    405.88,    37.249,         1,         0.0
simulation,         3,      0.13,   0.13007,   0.13007,    12.746,    12.746,         0,      4.9683
simulation,         4,      0.19,   0.19001,   0.19001,    13.508,    13.508,         0,      4.9994
simulation,         5,      0.22,   0.21997,   0.21997,    14.077,    14.077,         0,      5.0078
simulation,         6,      0.23,   0.22997,   0.22997,    14.379,    14.379,         0,      5.0103
simulation,         7,      0.24,   0.23998,   0.23997,    14.883,    14.883,         0,      5.0113
simulation,         8,      0.05,  0.049884,  0.049886,    12.004,    12.004,         0,      4.8889
simulation,         9,       0.1,   0.10003,   0.10003,    12.442,    12.442,         0,       4.946
simulation,        10,      0.15,   0.15006,   0.15006,     12.97,    12.971,         0,      4.9807
simulation,        11,       0.2,   0.19998,   0.19998,    13.667,    13.667,         0,      5.0022
