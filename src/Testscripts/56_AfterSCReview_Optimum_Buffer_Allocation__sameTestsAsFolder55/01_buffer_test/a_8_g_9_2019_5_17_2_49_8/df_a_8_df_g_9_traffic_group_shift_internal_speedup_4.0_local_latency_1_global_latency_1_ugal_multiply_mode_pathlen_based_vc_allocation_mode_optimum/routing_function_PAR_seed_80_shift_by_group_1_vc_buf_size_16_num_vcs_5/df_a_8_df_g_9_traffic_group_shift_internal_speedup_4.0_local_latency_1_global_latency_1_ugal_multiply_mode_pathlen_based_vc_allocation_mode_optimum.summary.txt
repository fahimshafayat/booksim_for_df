wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
internal_speedup = 4.0;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
num_vcs = 5;
routing_function = PAR;
seed = 80;
shift_by_group = 1;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.40661,   0.41327,    762.69,    150.98,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24998,    15.072,    15.072,         0,      5.3957
simulation,         3,      0.37,   0.37017,   0.37017,    21.911,    21.912,         0,      5.7846
simulation,         4,      0.43,   0.40758,   0.40772,    1080.7,    93.544,         1,         0.0
simulation,         5,       0.4,   0.39779,   0.39783,    453.18,    38.779,         0,      5.8166
simulation,         6,      0.41,   0.40382,   0.40386,    546.19,    47.386,         1,         0.0
simulation,         7,       0.1,   0.10003,   0.10003,    13.385,    13.385,         0,      5.4284
simulation,         8,       0.2,   0.19998,   0.19998,    14.152,    14.152,         0,      5.3603
simulation,         9,       0.3,   0.30012,   0.30011,    16.981,    16.981,         0,      5.5236
simulation,        10,       0.4,   0.39779,   0.39783,    453.18,    38.779,         0,      5.8166
