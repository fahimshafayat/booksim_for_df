vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 100;
latency_thres = 1000.0;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 25;
perm_seed = 72;
routing_function = UGAL_L;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.28598,   0.33096,    1572.7,    1190.2,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.23733,   0.23763,    1069.3,    607.49,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.13009,   0.13009,    160.33,    160.46,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.19,   0.18955,   0.18958,    362.87,    227.43,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.22,   0.21686,   0.21688,    886.84,    352.09,         1,  0,  0,  ,  ,  ,  
simulation,         6,       0.2,   0.19922,   0.19922,    551.35,    235.43,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.21,   0.20832,   0.20838,    903.99,    309.34,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.15,   0.15006,   0.15007,    164.36,    164.49,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.1,   0.10006,   0.10007,    157.18,    157.31,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.05,   0.05005,  0.050053,     157.6,    157.73,         0,  0,  0,  ,  ,  ,  
