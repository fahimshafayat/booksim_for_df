vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 100;
latency_thres = 1000.0;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 25;
perm_seed = 70;
routing_function = UGAL_L;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.27324,   0.31888,    1674.3,    1258.5,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.23338,   0.23376,    1359.0,    738.04,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.13009,   0.13009,    160.36,    160.48,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.19,   0.18943,   0.18946,    519.88,    228.86,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.22,   0.21535,   0.21542,    980.54,     429.8,         1,  0,  0,  ,  ,  ,  
simulation,         6,       0.2,   0.19873,   0.19876,    867.74,    275.96,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.21,   0.20748,   0.20752,    734.63,    345.14,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.15,   0.15006,   0.15007,    166.23,    166.36,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.1,   0.10006,   0.10007,    157.28,     157.4,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.05,  0.050052,  0.050053,     158.1,    158.23,         0,  0,  0,  ,  ,  ,  
