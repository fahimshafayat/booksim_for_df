vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 100;
latency_thres = 1000.0;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 17;
routing_function = UGAL_L_restricted;
shift_by_group = 9;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.084278,   0.13241,    3879.3,    3335.4,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.081834,   0.13026,    2863.2,    2750.5,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.087117,   0.12452,    1467.9,    1467.9,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.069953,   0.06996,    479.56,    484.42,         0,  0,  0,  ,  ,  ,  
simulation,         5,       0.1,  0.084282,  0.099027,    1543.0,    1542.6,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.08,  0.079619,  0.079814,    741.96,    682.99,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.09,  0.082577,   0.09008,    951.23,    951.23,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.05,  0.049958,  0.049959,    389.19,    390.45,         0,  0,  0,  ,  ,  ,  
