vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 100;
latency_thres = 1000.0;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 9;
routing_function = UGAL_L;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.1124,   0.16353,    3723.8,    3046.5,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.11325,   0.16457,    2397.1,    2336.7,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.10503,   0.13011,    876.16,    876.16,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.069968,  0.069959,    234.01,    233.93,         0,  0,  0,  ,  ,  ,  
simulation,         5,       0.1,  0.093921,  0.099528,    1664.6,    1664.5,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.08,  0.079957,  0.079967,    327.13,    327.36,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.09,  0.089288,  0.089797,     956.8,    952.43,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.05,  0.049932,   0.04993,    165.62,    165.72,         0,  0,  0,  ,  ,  ,  
