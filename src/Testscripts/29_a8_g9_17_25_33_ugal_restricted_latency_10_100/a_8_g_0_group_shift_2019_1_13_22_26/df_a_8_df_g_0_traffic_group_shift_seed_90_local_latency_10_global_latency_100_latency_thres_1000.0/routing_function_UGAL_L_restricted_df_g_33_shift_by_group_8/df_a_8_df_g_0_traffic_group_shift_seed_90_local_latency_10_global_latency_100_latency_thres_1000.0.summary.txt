vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 100;
latency_thres = 1000.0;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 33;
routing_function = UGAL_L_restricted;
shift_by_group = 8;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.099657,   0.14422,    2525.5,    2344.8,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.08142,   0.12311,    2192.2,    2167.9,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.075066,   0.11692,    1861.5,    1861.2,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.070088,  0.070094,    396.66,    401.48,         0,  0,  0,  ,  ,  ,  
simulation,         5,       0.1,  0.074129,   0.10004,    1206.2,    1206.2,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.08,  0.073958,  0.079812,    1869.7,    1869.5,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.05,  0.050089,   0.05009,    369.57,    371.86,         0,  0,  0,  ,  ,  ,  
