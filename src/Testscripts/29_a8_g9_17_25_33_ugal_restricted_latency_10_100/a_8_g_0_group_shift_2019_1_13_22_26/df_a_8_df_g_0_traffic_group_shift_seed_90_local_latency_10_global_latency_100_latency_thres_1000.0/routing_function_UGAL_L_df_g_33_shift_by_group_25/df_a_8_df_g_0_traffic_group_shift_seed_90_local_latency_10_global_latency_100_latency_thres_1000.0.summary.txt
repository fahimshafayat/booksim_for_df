vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 100;
latency_thres = 1000.0;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 33;
routing_function = UGAL_L;
shift_by_group = 25;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.11313,   0.15606,    1993.2,    1944.2,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.093505,   0.13576,    2275.1,    2228.2,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.083114,   0.12388,    1514.8,    1514.8,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.070093,  0.070094,    387.87,    391.76,         0,  0,  0,  ,  ,  ,  
simulation,         5,       0.1,  0.080215,   0.10005,    885.99,    885.99,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.08,  0.078266,  0.080057,    1093.0,    1157.7,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.05,  0.050091,   0.05009,    298.85,    299.33,         0,  0,  0,  ,  ,  ,  
