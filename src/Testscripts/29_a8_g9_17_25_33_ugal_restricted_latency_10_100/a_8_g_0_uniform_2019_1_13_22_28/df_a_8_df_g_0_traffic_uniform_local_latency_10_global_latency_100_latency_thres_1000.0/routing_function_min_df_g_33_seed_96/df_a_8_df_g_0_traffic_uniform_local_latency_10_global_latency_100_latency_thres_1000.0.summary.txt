vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 100;
latency_thres = 1000.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
df_g = 33;
routing_function = min;
seed = 96;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.33657,   0.38945,    1645.2,    1193.5,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25001,      0.25,    121.45,    121.47,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.33814,   0.34043,    2172.4,    1513.4,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30999,   0.30999,    124.56,    124.57,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,    0.3381,   0.33975,    526.75,    524.97,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.35,   0.33825,   0.34496,    1224.8,    1139.8,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.30001,   0.29999,    122.97,    122.98,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.19995,   0.19994,     121.3,    121.31,         0,  0,  0,  ,  ,  ,  
