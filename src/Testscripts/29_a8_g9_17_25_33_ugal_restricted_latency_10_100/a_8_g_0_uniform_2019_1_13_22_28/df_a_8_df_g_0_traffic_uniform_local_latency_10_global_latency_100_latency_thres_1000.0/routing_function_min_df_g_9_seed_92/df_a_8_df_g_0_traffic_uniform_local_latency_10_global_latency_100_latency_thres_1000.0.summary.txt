vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 100;
latency_thres = 1000.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
df_g = 9;
routing_function = min;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.36833,   0.42093,    1322.8,    1043.1,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25012,   0.25011,    112.49,    112.53,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36821,   0.36976,    472.82,    469.13,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,    0.3684,   0.39447,    1450.8,    1072.1,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.36909,   0.37217,    1965.8,    1367.3,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.38,   0.36973,   0.37436,    1071.7,    991.51,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.35,   0.35013,   0.35011,    120.28,    120.36,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.30006,   0.30006,    112.74,    112.78,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20011,   0.20012,    112.35,    112.39,         0,  0,  0,  ,  ,  ,  
