vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 100;
latency_thres = 1000.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
df_g = 9;
routing_function = UGAL_L_restricted;
seed = 96;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.36078,   0.41476,    1405.6,    1101.2,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24991,   0.24992,    122.31,    122.38,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36559,   0.36769,     971.8,    822.53,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31016,   0.31016,    123.51,    123.58,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,   0.34012,   0.34015,    140.87,    140.97,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.35,   0.35008,   0.35009,    204.53,    204.67,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.36,   0.36001,   0.36012,    265.66,    265.86,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.30008,   0.30008,     122.6,    122.67,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.2,   0.19993,   0.19992,    122.86,    122.94,         0,  0,  0,  ,  ,  ,  
