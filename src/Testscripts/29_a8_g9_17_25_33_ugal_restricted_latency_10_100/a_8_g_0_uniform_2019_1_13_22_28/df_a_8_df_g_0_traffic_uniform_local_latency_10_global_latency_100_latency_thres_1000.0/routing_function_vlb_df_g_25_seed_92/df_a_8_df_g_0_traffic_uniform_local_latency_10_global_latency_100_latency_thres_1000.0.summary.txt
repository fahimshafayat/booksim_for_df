vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 100;
latency_thres = 1000.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
df_g = 25;
routing_function = vlb;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.25514,   0.30867,    2474.8,    1660.6,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24988,   0.24986,    251.38,    251.53,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.25521,   0.30868,    1580.5,    1401.0,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.25587,   0.28235,    1763.2,    1457.8,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.25552,   0.26249,    2113.3,    1830.9,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,   0.25566,   0.25882,    1067.7,    995.66,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.19989,   0.19988,    237.66,    237.72,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.15,   0.14987,   0.14986,    236.84,    236.91,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.1,  0.099926,  0.099918,    236.36,    236.42,         0,  0,  0,  ,  ,  ,  
