vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 100;
latency_thres = 1000.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
df_g = 25;
routing_function = vlb;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.25571,   0.30929,    2460.3,    1655.8,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25008,    251.59,    251.69,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.25481,   0.30827,    1574.2,    1399.5,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.25544,   0.28191,    1769.2,    1460.7,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.25616,   0.26314,    2107.4,    1825.0,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,   0.25584,   0.25905,    1008.5,    951.83,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,    0.2001,   0.20009,    237.62,    237.69,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.15,   0.15007,   0.15007,    236.81,    236.87,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.1,   0.10007,   0.10007,    236.33,    236.39,         0,  0,  0,  ,  ,  ,  
