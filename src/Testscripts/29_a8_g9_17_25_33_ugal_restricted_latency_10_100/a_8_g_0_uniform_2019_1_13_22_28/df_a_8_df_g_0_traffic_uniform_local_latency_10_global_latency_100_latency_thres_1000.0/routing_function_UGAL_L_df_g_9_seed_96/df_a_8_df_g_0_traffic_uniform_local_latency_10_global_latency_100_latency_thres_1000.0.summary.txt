vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 100;
latency_thres = 1000.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
df_g = 9;
routing_function = UGAL_L;
seed = 96;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.35477,   0.40762,    1430.0,    1106.7,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24991,   0.24992,    131.44,    131.55,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36328,    0.3656,    1059.6,    852.28,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31014,   0.31016,    135.18,    135.29,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,   0.34014,   0.34015,    230.84,     231.0,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.35,   0.35013,   0.35009,    257.69,    257.96,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.36,   0.36001,   0.36012,    309.01,    309.26,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.30008,   0.30008,    132.77,    132.88,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.2,   0.19991,   0.19992,    132.79,     132.9,         0,  0,  0,  ,  ,  ,  
