vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 100;
latency_thres = 1000.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
df_g = 17;
routing_function = UGAL_L;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.33417,   0.38716,    1623.6,    1199.0,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24983,   0.24982,    138.41,     138.5,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.33645,   0.33726,    2001.6,    1413.5,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30981,   0.30974,    183.08,     183.2,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,   0.33974,   0.33975,    323.88,    324.52,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.35,   0.34325,   0.34616,    1020.7,    900.57,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.29977,   0.29975,    146.34,    146.43,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.19985,   0.19985,    139.65,    139.75,         0,  0,  0,  ,  ,  ,  
