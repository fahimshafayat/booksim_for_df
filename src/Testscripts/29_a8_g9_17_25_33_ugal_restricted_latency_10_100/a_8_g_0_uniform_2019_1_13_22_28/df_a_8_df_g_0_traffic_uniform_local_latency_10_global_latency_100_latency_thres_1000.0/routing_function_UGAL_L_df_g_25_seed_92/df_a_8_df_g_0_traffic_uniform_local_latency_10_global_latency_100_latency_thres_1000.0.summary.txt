vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 100;
latency_thres = 1000.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
df_g = 25;
routing_function = UGAL_L;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.28699,   0.33948,    2069.7,    1413.1,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24986,   0.24986,    182.38,    182.53,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,    0.2906,   0.34173,    1060.6,    1017.7,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.29204,    0.2941,    1362.4,    1038.8,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27888,   0.27886,    497.23,    330.48,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.29,   0.28571,   0.28633,    984.75,    629.21,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.19988,   0.19988,    142.98,    143.07,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.15,   0.14987,   0.14986,    144.49,    144.58,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.1,   0.09992,  0.099918,    147.15,    147.25,         0,  0,  0,  ,  ,  ,  
