wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
five_hop_percentage = 60;
perm_seed = 50;
routing_function = PAR_four_hop_some_five_hop_restricted;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50015,    12.183,    12.183,         0,      4.0382
simulation,         2,      0.74,    0.7401,   0.74012,     18.34,     18.34,         0,       4.041
simulation,         3,      0.86,   0.79578,   0.81125,    619.87,    326.02,         1,         0.0
simulation,         4,       0.8,   0.78246,   0.78241,     477.4,    212.37,         1,         0.0
simulation,         5,      0.77,   0.76992,      0.77,    29.172,    29.194,         0,      4.0974
simulation,         6,      0.78,   0.77869,   0.77868,    168.85,    45.591,         0,      4.1545
simulation,         7,      0.79,   0.77909,   0.77953,    490.95,    190.43,         1,         0.0
simulation,         8,      0.15,   0.15006,   0.15006,    10.935,    10.935,         0,      4.2623
simulation,         9,       0.3,   0.30011,   0.30011,    11.272,    11.272,         0,      4.1545
simulation,        10,      0.45,   0.45009,   0.45009,    11.866,    11.867,         0,      4.0614
simulation,        11,       0.6,   0.59993,   0.59993,    13.171,    13.171,         0,      4.0049
simulation,        12,      0.75,   0.75007,   0.75009,    21.329,    21.337,         0,      4.0537
