wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
five_hop_percentage = 70;
perm_seed = 51;
routing_function = UGAL_L_four_hop_some_five_hop_restricted;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50002,   0.50001,    11.386,    11.386,         0,      3.8245
simulation,         2,      0.74,   0.74001,   0.74001,    22.967,    22.984,         0,      3.8198
simulation,         3,      0.86,   0.82614,    0.8266,    772.49,    231.06,         1,         0.0
simulation,         4,       0.8,   0.79956,   0.79954,    75.081,    49.123,         0,      3.8689
simulation,         5,      0.83,   0.81769,   0.81774,    544.25,    124.66,         1,         0.0
simulation,         6,      0.81,   0.80716,   0.80722,    318.51,    73.035,         0,      3.8787
simulation,         7,      0.82,   0.81322,   0.81325,    482.27,    106.55,         1,         0.0
simulation,         8,      0.15,   0.14984,   0.14984,    10.114,    10.114,         0,       3.902
simulation,         9,       0.3,   0.29995,   0.29995,    10.527,    10.527,         0,      3.8819
simulation,        10,      0.45,   0.44997,   0.44996,    11.096,    11.096,         0,      3.8373
simulation,        11,       0.6,   0.60002,   0.60001,    12.688,     12.69,         0,      3.8062
simulation,        12,      0.75,   0.75004,   0.75003,    24.607,    24.627,         0,      3.8239
