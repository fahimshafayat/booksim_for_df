wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
five_hop_percentage = 50;
perm_seed = 50;
routing_function = UGAL_G_four_hop_some_five_hop_restricted;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50015,   0.50015,    11.032,    11.032,         0,      3.7386
simulation,         2,      0.74,   0.74011,   0.74012,    13.422,    13.422,         0,      3.7049
simulation,         3,      0.86,   0.86005,   0.86005,    18.332,    18.333,         0,      3.7188
simulation,         4,      0.92,   0.92001,   0.92002,    31.646,     31.65,         0,      3.7461
simulation,         5,      0.95,   0.93284,   0.93263,     505.9,    142.72,         1,         0.0
simulation,         6,      0.93,      0.93,   0.93004,    40.327,    40.334,         0,      3.7521
simulation,         7,      0.94,   0.93433,   0.93446,    464.42,    106.95,         0,      3.7608
simulation,         8,       0.2,   0.19998,   0.19998,    10.055,    10.055,         0,       3.819
simulation,         9,       0.4,   0.40003,   0.40002,    10.593,    10.593,         0,      3.7613
simulation,        10,       0.6,   0.59993,   0.59993,    11.672,    11.673,         0,      3.7203
simulation,        11,       0.8,   0.79996,   0.79997,    15.008,    15.009,         0,      3.7069
