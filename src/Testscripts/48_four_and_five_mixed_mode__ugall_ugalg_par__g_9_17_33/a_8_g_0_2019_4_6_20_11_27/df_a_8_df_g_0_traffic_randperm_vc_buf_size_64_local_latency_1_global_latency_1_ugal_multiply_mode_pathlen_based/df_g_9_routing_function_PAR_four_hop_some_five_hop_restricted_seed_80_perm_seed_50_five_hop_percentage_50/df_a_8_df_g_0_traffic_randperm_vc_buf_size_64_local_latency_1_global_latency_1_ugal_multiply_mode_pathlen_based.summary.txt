wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
five_hop_percentage = 50;
perm_seed = 50;
routing_function = PAR_four_hop_some_five_hop_restricted;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50015,   0.50015,    12.166,    12.167,         0,      4.0337
simulation,         2,      0.74,   0.74011,   0.74012,    18.311,    18.313,         0,      4.0397
simulation,         3,      0.86,   0.79339,   0.80845,    653.52,    330.36,         1,         0.0
simulation,         4,       0.8,   0.77917,   0.78147,    566.76,    247.66,         1,         0.0
simulation,         5,      0.77,   0.76989,   0.76997,    29.413,     29.48,         0,      4.0994
simulation,         6,      0.78,   0.77389,   0.77415,    439.71,    169.59,         1,         0.0
simulation,         7,      0.15,   0.15006,   0.15006,    10.906,    10.906,         0,        4.25
simulation,         8,       0.3,   0.30011,   0.30011,    11.252,    11.253,         0,      4.1471
simulation,         9,      0.45,    0.4501,   0.45009,    11.855,    11.855,         0,      4.0569
simulation,        10,       0.6,   0.59993,   0.59993,    13.155,    13.155,         0,      4.0016
simulation,        11,      0.75,   0.75007,   0.75009,    20.544,    20.549,         0,       4.053
