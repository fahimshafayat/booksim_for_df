wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
five_hop_percentage = 60;
perm_seed = 52;
routing_function = PAR_four_hop_some_five_hop_restricted;
seed = 82;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4999,   0.49989,    12.149,    12.149,         0,      4.0246
simulation,         2,      0.74,    0.7399,   0.73991,    17.903,    17.904,         0,      4.0289
simulation,         3,      0.86,   0.79165,   0.80807,    672.75,    343.62,         1,         0.0
simulation,         4,       0.8,   0.77449,   0.77535,     621.2,    279.56,         1,         0.0
simulation,         5,      0.77,   0.77001,   0.76999,    24.994,     25.01,         0,      4.0773
simulation,         6,      0.78,   0.77929,   0.77936,     78.52,    43.555,         0,      4.1178
simulation,         7,      0.79,   0.77217,   0.77358,    561.53,    237.54,         1,         0.0
simulation,         8,      0.15,   0.15005,   0.15005,     10.87,     10.87,         0,      4.2332
simulation,         9,       0.3,   0.30011,    0.3001,    11.219,    11.219,         0,      4.1329
simulation,        10,      0.45,   0.44994,   0.44993,    11.831,    11.832,         0,      4.0466
simulation,        11,       0.6,   0.59987,   0.59986,     13.16,     13.16,         0,      3.9963
simulation,        12,      0.75,    0.7499,    0.7499,    19.361,    19.364,         0,      4.0396
