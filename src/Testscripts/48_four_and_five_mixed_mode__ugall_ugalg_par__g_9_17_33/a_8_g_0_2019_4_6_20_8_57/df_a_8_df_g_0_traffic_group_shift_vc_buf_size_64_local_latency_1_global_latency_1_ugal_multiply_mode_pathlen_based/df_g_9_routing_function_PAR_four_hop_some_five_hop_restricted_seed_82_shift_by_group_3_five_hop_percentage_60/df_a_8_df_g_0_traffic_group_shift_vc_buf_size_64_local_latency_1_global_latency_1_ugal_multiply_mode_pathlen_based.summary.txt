wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
five_hop_percentage = 60;
routing_function = PAR_four_hop_some_five_hop_restricted;
seed = 82;
shift_by_group = 3;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4999,   0.49989,    25.884,    25.903,         0,      5.1644
simulation,         2,      0.74,   0.54469,    0.5915,    1129.3,    693.73,         1,         0.0
simulation,         3,      0.62,   0.54898,   0.58012,    481.61,    399.09,         1,         0.0
simulation,         4,      0.56,   0.54868,   0.54832,     534.6,    234.69,         1,         0.0
simulation,         5,      0.53,   0.52989,   0.52989,    38.779,    38.812,         0,      5.2247
simulation,         6,      0.54,   0.53996,   0.53988,    71.541,    71.894,         0,      5.2427
simulation,         7,      0.55,   0.54537,   0.54541,    479.59,    157.54,         1,         0.0
simulation,         8,       0.1,  0.099989,  0.099984,    12.119,    12.119,         0,      4.8519
simulation,         9,       0.2,    0.2001,   0.20009,    12.673,    12.673,         0,      4.8305
simulation,        10,       0.3,   0.30011,    0.3001,    13.728,    13.728,         0,      4.8524
simulation,        11,       0.4,   0.40005,   0.40004,    16.278,    16.278,         0,      4.9815
simulation,        12,       0.5,    0.4999,   0.49989,    25.884,    25.903,         0,      5.1644
