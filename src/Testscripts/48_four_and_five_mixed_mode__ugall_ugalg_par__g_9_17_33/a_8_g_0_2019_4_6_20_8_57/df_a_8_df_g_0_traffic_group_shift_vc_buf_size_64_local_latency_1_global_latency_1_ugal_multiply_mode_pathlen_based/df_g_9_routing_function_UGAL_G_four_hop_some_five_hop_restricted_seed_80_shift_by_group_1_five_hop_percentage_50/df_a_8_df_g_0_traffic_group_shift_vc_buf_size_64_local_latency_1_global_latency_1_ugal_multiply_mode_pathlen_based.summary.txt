wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
five_hop_percentage = 50;
routing_function = UGAL_G_four_hop_some_five_hop_restricted;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50015,    19.254,    19.254,         0,      4.6478
simulation,         2,      0.74,   0.54156,   0.58354,    1093.0,    617.38,         1,         0.0
simulation,         3,      0.62,   0.54914,    0.5766,    464.94,    363.56,         1,         0.0
simulation,         4,      0.56,   0.55988,      0.56,    36.815,    36.815,         0,      4.7029
simulation,         5,      0.59,   0.55268,    0.5633,    530.84,     319.9,         1,         0.0
simulation,         6,      0.57,   0.55528,   0.55532,    503.48,    255.74,         1,         0.0
simulation,         7,       0.1,   0.10003,   0.10003,    11.232,    11.232,         0,      4.4038
simulation,         8,       0.2,   0.19998,   0.19998,    12.036,    12.036,         0,      4.4437
simulation,         9,       0.3,   0.30012,   0.30011,     13.13,     13.13,         0,      4.4782
simulation,        10,       0.4,   0.40003,   0.40002,    15.151,    15.151,         0,      4.5621
simulation,        11,       0.5,   0.50014,   0.50015,    19.254,    19.254,         0,      4.6478
