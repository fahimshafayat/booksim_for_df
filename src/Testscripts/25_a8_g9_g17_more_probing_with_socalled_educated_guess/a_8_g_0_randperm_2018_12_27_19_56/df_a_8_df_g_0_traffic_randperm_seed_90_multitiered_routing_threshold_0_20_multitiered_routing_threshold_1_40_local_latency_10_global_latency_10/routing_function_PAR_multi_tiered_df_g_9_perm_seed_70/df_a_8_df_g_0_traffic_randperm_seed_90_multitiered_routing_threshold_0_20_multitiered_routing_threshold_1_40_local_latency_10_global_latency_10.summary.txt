vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 20;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 9;
perm_seed = 70;
routing_function = PAR_multi_tiered;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49981,   0.49978,    36.908,    36.912,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,    0.7403,   0.74028,    46.053,     46.06,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.79886,    0.8131,     564.0,    294.53,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.80022,   0.80024,    67.065,     67.09,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.83,   0.80551,   0.80715,    549.41,    243.28,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.81,   0.80904,   0.80901,     151.9,    83.403,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.82,   0.81114,    0.8111,     524.4,    153.53,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.75,    0.7503,   0.75028,    47.538,    47.544,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.7,   0.70019,   0.70017,     42.22,    42.226,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.65,   0.65006,   0.65004,    39.712,    39.717,         0,  0,  0,  ,  ,  ,  
