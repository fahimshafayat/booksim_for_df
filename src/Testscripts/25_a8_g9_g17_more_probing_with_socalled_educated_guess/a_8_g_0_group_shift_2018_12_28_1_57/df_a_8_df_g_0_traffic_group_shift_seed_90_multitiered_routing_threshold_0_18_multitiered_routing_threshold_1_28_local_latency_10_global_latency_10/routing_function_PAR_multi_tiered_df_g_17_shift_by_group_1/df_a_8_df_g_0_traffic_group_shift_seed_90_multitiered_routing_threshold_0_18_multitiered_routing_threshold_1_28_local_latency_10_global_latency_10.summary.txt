vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 18;
multitiered_routing_threshold_1 = 28;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 17;
routing_function = PAR_multi_tiered;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.46885,   0.47781,    514.92,    308.66,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,    58.849,    58.856,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36998,   0.36997,    77.343,    77.348,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42967,   0.42967,     136.4,    105.48,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,    0.4563,   0.45633,     494.9,    174.32,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.43921,   0.43921,    235.44,    119.21,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.45,   0.44803,   0.44806,    451.22,     141.2,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39992,   0.39994,    88.223,    88.233,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35002,   0.35001,    72.559,    72.564,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30001,   0.30001,    65.123,    65.127,         0,  0,  0,  ,  ,  ,  
