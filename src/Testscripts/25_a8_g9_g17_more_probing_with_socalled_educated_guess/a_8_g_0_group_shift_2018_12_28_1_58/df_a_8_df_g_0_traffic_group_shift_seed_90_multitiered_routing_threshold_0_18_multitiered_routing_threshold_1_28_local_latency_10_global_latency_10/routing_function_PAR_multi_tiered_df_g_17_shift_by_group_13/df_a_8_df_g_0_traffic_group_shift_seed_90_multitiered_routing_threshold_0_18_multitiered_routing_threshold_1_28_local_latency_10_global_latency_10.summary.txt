vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 18;
multitiered_routing_threshold_1 = 28;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 17;
routing_function = PAR_multi_tiered;
shift_by_group = 13;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,      0.47,    0.4721,    1107.3,    400.44,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,    58.705,    58.712,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36997,   0.36997,    76.814,    76.816,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42991,   0.42991,    94.907,    94.914,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45896,   0.45895,    261.71,     142.0,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,      0.47,    0.4697,    530.13,    218.15,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.46649,   0.46654,    499.64,    171.45,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44976,   0.44983,    115.97,    111.22,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39993,   0.39994,    86.089,    86.094,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35002,   0.35001,     72.23,    72.233,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30001,   0.30001,    64.951,    64.957,         0,  0,  0,  ,  ,  ,  
