vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 20;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 9;
routing_function = UGAL_L;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.33594,   0.38549,    1498.2,    1083.3,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24997,   0.24997,    110.25,    110.32,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.33026,     0.358,    498.63,    485.88,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30874,   0.30891,     326.8,     221.9,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,    0.3219,   0.33484,    524.73,    490.62,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.32,   0.31524,    0.3161,     493.5,     408.6,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.3,   0.29992,   0.29991,    139.75,    139.98,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.2,   0.19993,   0.19992,    83.166,     83.17,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.15,   0.14992,   0.14992,      46.4,    46.405,         0,  0,  0,  ,  ,  ,  
