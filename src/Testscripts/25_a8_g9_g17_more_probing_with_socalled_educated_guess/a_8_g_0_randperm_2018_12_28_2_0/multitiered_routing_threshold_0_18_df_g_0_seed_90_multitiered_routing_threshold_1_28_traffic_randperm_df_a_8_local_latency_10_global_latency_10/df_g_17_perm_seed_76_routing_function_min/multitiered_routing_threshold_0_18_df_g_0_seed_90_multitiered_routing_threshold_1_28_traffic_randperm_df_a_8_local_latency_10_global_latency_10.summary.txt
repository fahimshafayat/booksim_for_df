packet_size = 1;
injection_rate_uses_flits = 1;
wait_for_tail_credit = 0;
vc_alloc_delay = 1;
output_speedup = 1;
credit_delay = 2;
vc_allocator = separable_input_first;
vc_buf_size = 64;
input_speedup = 1;
sw_allocator = separable_input_first;
sim_count = 1;
internal_speedup = 4.0;
warmup_periods = 3;
st_final_delay = 1;
num_vcs = 7;
sample_period = 10000;
routing_delay = 0;
sw_alloc_delay = 1;
priority = none;
alloc_iters = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 18;
multitiered_routing_threshold_1 = 28;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 17;
perm_seed = 76;
routing_function = min;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.45258,   0.46112,    625.61,     312.1,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25005,    33.638,    33.639,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36519,   0.36524,    506.61,    130.68,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31001,   0.31001,    34.127,    34.129,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,   0.33824,   0.33835,    258.66,    113.51,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.32,   0.32003,   0.32003,    34.421,    34.423,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.33,   0.33002,   0.33002,    35.607,    35.608,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.30001,   0.30001,    33.987,    33.989,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20008,   0.20007,    33.452,    33.453,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.15,   0.15003,   0.15003,    33.327,    33.328,         0,  0,  0,  ,  ,  ,  
