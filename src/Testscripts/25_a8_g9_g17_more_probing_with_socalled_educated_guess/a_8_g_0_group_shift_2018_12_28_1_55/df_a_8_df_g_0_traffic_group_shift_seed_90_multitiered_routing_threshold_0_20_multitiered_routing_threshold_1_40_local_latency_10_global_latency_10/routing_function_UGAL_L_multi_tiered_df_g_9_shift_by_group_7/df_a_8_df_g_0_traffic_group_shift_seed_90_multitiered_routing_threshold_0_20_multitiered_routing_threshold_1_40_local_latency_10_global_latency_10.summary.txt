vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 20;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 9;
routing_function = UGAL_L_multi_tiered;
shift_by_group = 7;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.42244,   0.45069,    636.98,    498.77,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24998,   0.24997,    84.729,    84.769,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36342,   0.36357,    476.73,    332.24,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,    0.3099,    0.3099,     104.1,    104.22,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,   0.33908,   0.33934,    249.55,    204.68,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.35,   0.34713,   0.34734,    501.69,    275.24,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.29993,   0.29991,    100.86,    100.98,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.19994,   0.19992,    59.868,    59.867,         0,  0,  0,  ,  ,  ,  
