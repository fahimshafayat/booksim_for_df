vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_L_restricted_src_and_dst;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49992,   0.49994,    15.993,    16.008,         0,       4.161
simulation,         2,      0.74,   0.66975,    0.6702,    864.92,    363.81,         1,         0.0
simulation,         3,      0.62,   0.61987,   0.61983,    44.699,    40.436,         0,      4.2579
simulation,         4,      0.68,   0.66283,   0.66309,    481.34,    168.46,         1,         0.0
simulation,         5,      0.65,   0.64685,   0.64686,    242.55,    74.019,         0,      4.3335
simulation,         6,      0.66,   0.65368,   0.65364,    475.54,    99.155,         0,      4.3617
simulation,         7,      0.67,   0.65942,   0.65957,    416.54,    125.55,         1,         0.0
simulation,         8,       0.1,  0.099911,  0.099915,    10.657,    10.658,         0,      4.2139
simulation,         9,       0.2,   0.19997,   0.19998,    11.043,    11.043,         0,      4.2431
simulation,        10,       0.3,   0.29996,   0.29997,    11.391,    11.391,         0,      4.2062
simulation,        11,       0.4,   0.39991,   0.39992,    12.005,    12.006,         0,      4.1697
simulation,        12,       0.5,   0.49992,   0.49994,    15.993,    16.008,         0,       4.161
simulation,        13,       0.6,       0.6,   0.60001,    29.235,    29.288,         0,      4.2271
