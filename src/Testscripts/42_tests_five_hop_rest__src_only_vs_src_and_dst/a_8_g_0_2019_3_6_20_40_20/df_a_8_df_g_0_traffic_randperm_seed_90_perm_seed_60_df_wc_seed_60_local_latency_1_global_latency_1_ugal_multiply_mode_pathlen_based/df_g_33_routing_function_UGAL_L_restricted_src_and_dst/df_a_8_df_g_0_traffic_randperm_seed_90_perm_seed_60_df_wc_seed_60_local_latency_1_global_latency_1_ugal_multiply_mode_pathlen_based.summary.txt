vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = UGAL_L_restricted_src_and_dst;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49965,   0.49966,    59.387,    35.745,         0,      4.5013
simulation,         2,      0.74,   0.58313,   0.61624,    708.09,    454.75,         1,         0.0
simulation,         3,      0.62,   0.57988,   0.58072,    569.39,    285.34,         1,         0.0
simulation,         4,      0.56,   0.55504,   0.55511,    457.69,    99.252,         0,      4.6732
simulation,         5,      0.59,   0.57396,   0.57415,    489.28,    175.86,         1,         0.0
simulation,         6,      0.57,   0.56243,   0.56247,    347.33,    119.56,         1,         0.0
simulation,         7,       0.1,  0.099895,  0.099898,    11.061,    11.061,         0,      4.4024
simulation,         8,       0.2,       0.2,   0.20001,    11.503,    11.503,         0,      4.4342
simulation,         9,       0.3,   0.29997,   0.29998,    12.043,    12.044,         0,      4.4035
simulation,        10,       0.4,   0.39988,   0.39989,    16.348,    16.361,         0,      4.4002
simulation,        11,       0.5,   0.49965,   0.49966,    59.387,    35.745,         0,      4.5013
