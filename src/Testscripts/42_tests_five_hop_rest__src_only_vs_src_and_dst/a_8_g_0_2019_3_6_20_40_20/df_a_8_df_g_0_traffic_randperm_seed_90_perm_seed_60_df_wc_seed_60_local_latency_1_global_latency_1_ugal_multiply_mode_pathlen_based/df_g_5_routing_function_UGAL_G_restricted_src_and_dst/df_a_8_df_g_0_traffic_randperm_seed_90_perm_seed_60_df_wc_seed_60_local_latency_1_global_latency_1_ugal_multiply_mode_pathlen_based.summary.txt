vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_G_restricted_src_and_dst;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50007,   0.50007,    10.281,    10.281,         0,      3.5061
simulation,         2,      0.74,    0.7402,   0.74021,    11.733,    11.734,         0,      3.4743
simulation,         3,      0.86,   0.86028,   0.86027,    13.383,    13.384,         0,      3.4682
simulation,         4,      0.92,   0.92017,   0.92021,    14.844,    14.845,         0,      3.4693
simulation,         5,      0.95,   0.95005,   0.95007,    15.955,    15.956,         0,       3.471
simulation,         6,      0.97,   0.97012,   0.97012,    16.962,    16.963,         0,      3.4715
simulation,         7,      0.98,   0.98012,   0.98012,    17.649,    17.651,         0,      3.4723
simulation,         8,       0.1,   0.10047,   0.10047,    9.2895,    9.2894,         0,      3.5667
simulation,         9,       0.2,   0.20032,   0.20032,    9.4679,    9.4681,         0,      3.5577
simulation,        10,       0.3,   0.30002,   0.30002,    9.6699,    9.6701,         0,       3.539
simulation,        11,       0.4,   0.40013,   0.40013,    9.9339,    9.9342,         0,      3.5216
simulation,        12,       0.5,   0.50007,   0.50007,    10.281,    10.281,         0,      3.5061
simulation,        13,       0.6,   0.60026,   0.60026,    10.742,    10.743,         0,      3.4912
simulation,        14,       0.7,   0.70011,   0.70014,    11.386,    11.386,         0,      3.4783
simulation,        15,       0.8,   0.80031,   0.80034,    12.416,    12.417,         0,      3.4702
simulation,        16,       0.9,   0.90028,   0.90029,    14.278,    14.279,         0,      3.4689
