vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = vlb_restricted_src_only;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.38043,    0.4185,    839.18,    636.93,         1,         0.0
simulation,         2,      0.25,   0.25001,   0.25002,     14.93,     14.93,         0,      5.5947
simulation,         3,      0.37,   0.36412,   0.36411,    333.42,    108.53,         1,         0.0
simulation,         4,      0.31,   0.30997,   0.30998,    15.949,    15.949,         0,      5.5942
simulation,         5,      0.34,   0.33999,   0.34001,    16.875,    16.877,         0,      5.5951
simulation,         6,      0.35,   0.34936,   0.34959,    79.288,    50.671,         0,      5.5945
simulation,         7,      0.36,   0.35816,   0.35825,     187.9,    56.573,         0,      5.5943
simulation,         8,       0.1,  0.099896,  0.099898,    13.645,    13.645,         0,      5.5941
simulation,         9,       0.2,       0.2,   0.20001,    14.375,    14.375,         0,      5.5947
simulation,        10,       0.3,   0.29997,   0.29998,    15.738,    15.738,         0,       5.595
