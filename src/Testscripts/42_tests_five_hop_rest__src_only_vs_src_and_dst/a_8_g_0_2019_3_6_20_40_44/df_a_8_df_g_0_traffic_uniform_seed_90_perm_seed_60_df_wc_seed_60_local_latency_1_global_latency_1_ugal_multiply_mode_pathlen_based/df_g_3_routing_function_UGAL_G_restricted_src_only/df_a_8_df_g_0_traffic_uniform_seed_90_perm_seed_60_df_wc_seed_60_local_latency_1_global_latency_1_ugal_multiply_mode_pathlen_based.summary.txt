vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = UGAL_G_restricted_src_only;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50013,    9.6803,    9.6808,         0,      3.2292
simulation,         2,      0.74,    0.7401,   0.74014,    11.227,    11.227,         0,      3.1953
simulation,         3,      0.86,   0.86027,   0.86025,    13.362,    13.362,         0,      3.1808
simulation,         4,      0.92,   0.92023,   0.92019,    16.291,    16.294,         0,      3.1721
simulation,         5,      0.95,   0.95018,   0.95025,    20.414,    20.418,         0,      3.1679
simulation,         6,      0.97,   0.97023,   0.97029,    27.193,    27.197,         0,      3.1664
simulation,         7,      0.98,   0.98021,   0.98018,    35.472,    35.499,         0,      3.1672
simulation,         8,       0.1,   0.10056,   0.10056,    8.7336,    8.7335,         0,      3.2899
simulation,         9,       0.2,   0.20054,   0.20056,    8.8944,    8.8946,         0,      3.2761
simulation,        10,       0.3,   0.30021,   0.30022,    9.0945,    9.0948,         0,      3.2627
simulation,        11,       0.4,    0.3999,    0.3999,    9.3432,    9.3438,         0,      3.2457
simulation,        12,       0.5,   0.50014,   0.50013,    9.6803,    9.6808,         0,      3.2292
simulation,        13,       0.6,    0.6001,    0.6001,    10.146,    10.147,         0,      3.2119
simulation,        14,       0.7,   0.70002,   0.70001,    10.842,    10.842,         0,         3.2
simulation,        15,       0.8,   0.80009,    0.8001,    12.025,    12.026,         0,       3.188
simulation,        16,       0.9,   0.90014,   0.90014,    14.989,    14.991,         0,      3.1749
