vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_G_restricted_src_and_dst;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49993,   0.49994,    11.325,    11.326,         0,      3.7814
simulation,         2,      0.74,   0.74027,   0.74027,    13.862,    13.862,         0,      3.7248
simulation,         3,      0.86,    0.8601,   0.86008,    17.716,    17.717,         0,      3.7076
simulation,         4,      0.92,   0.92005,   0.92006,    22.819,    22.822,         0,      3.7014
simulation,         5,      0.95,      0.95,      0.95,    28.665,    28.672,         0,      3.6972
simulation,         6,      0.97,   0.96987,   0.96992,    37.781,    37.812,         0,      3.6944
simulation,         7,      0.98,   0.97995,   0.97993,    47.373,     47.41,         0,      3.6924
simulation,         8,       0.1,  0.099913,  0.099915,    10.196,    10.196,         0,      3.9871
simulation,         9,       0.2,   0.19997,   0.19998,    10.313,    10.313,         0,       3.914
simulation,        10,       0.3,   0.29996,   0.29997,    10.524,    10.524,         0,      3.8581
simulation,        11,       0.4,   0.39991,   0.39992,    10.847,    10.848,         0,      3.8152
simulation,        12,       0.5,   0.49993,   0.49994,    11.325,    11.326,         0,      3.7814
simulation,        13,       0.6,       0.6,   0.60001,     12.04,     12.04,         0,      3.7543
simulation,        14,       0.7,   0.70019,   0.70018,    13.187,    13.188,         0,      3.7318
simulation,        15,       0.8,   0.80021,   0.80021,    15.299,      15.3,         0,      3.7154
simulation,        16,       0.9,   0.90005,   0.90004,    20.534,    20.536,         0,      3.7029
