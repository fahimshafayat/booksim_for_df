vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = UGAL_G_restricted_src_only;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50013,   0.50013,    11.055,    11.055,         0,      3.6872
simulation,         2,      0.74,   0.73994,   0.73995,    13.338,    13.339,         0,      3.6384
simulation,         3,      0.86,   0.85986,   0.85987,    16.515,    16.517,         0,       3.619
simulation,         4,      0.92,   0.91999,   0.91998,    20.638,    20.643,         0,      3.6101
simulation,         5,      0.95,   0.94987,   0.94994,    25.362,     25.37,         0,      3.6045
simulation,         6,      0.97,   0.96981,   0.96987,    33.168,    33.181,         0,      3.6017
simulation,         7,      0.98,   0.97983,    0.9799,    42.945,    42.979,         0,      3.5995
simulation,         8,       0.1,  0.099926,   0.09993,    9.8483,    9.8485,         0,       3.821
simulation,         9,       0.2,   0.20005,   0.20005,    10.013,    10.013,         0,      3.7778
simulation,        10,       0.3,   0.29995,   0.29995,    10.254,    10.254,         0,      3.7429
simulation,        11,       0.4,   0.40021,   0.40022,    10.581,    10.581,         0,      3.7108
simulation,        12,       0.5,   0.50013,   0.50013,    11.055,    11.055,         0,      3.6872
simulation,        13,       0.6,   0.59988,   0.59988,    11.728,    11.729,         0,       3.665
simulation,        14,       0.7,   0.69979,   0.69979,    12.749,    12.749,         0,      3.6454
simulation,        15,       0.8,   0.80003,   0.80002,    14.562,    14.563,         0,      3.6284
simulation,        16,       0.9,   0.89994,   0.89993,    18.834,    18.837,         0,       3.613
