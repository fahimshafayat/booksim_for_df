vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = vlb_restricted_src_and_dst;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.33349,   0.41442,    372.73,    358.21,         1,         0.0
simulation,         2,      0.25,   0.25001,   0.25002,    15.634,    15.634,         0,      5.8131
simulation,         3,      0.37,    0.2958,   0.29401,    711.14,    521.03,         1,         0.0
simulation,         4,      0.31,   0.30996,   0.30998,    17.119,    17.119,         0,      5.8133
simulation,         5,      0.34,   0.31152,   0.31418,    484.47,    230.48,         1,         0.0
simulation,         6,      0.32,   0.31982,   0.31993,    26.546,    24.989,         0,      5.8134
simulation,         7,      0.33,   0.31342,   0.31546,    287.53,    137.89,         1,         0.0
simulation,         8,       0.1,  0.099897,  0.099898,    14.121,    14.121,         0,      5.8133
simulation,         9,       0.2,       0.2,   0.20001,    14.959,    14.959,         0,      5.8134
simulation,        10,       0.3,   0.29997,   0.29998,    16.752,    16.753,         0,      5.8132
