vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = vlb_restricted_src_and_dst;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.21942,   0.32349,    1330.6,    1292.4,         1,         0.0
simulation,         2,      0.25,   0.24264,   0.24621,    449.21,    384.57,         1,         0.0
simulation,         3,      0.13,   0.13074,   0.13074,     12.15,    12.151,         0,      4.5005
simulation,         4,      0.19,   0.19053,   0.19054,    14.157,    14.157,         0,      4.4996
simulation,         5,      0.22,   0.22051,   0.22051,    18.147,    18.147,         0,      4.4991
simulation,         6,      0.23,   0.23045,   0.23047,    22.237,    22.246,         0,      4.5007
simulation,         7,      0.24,   0.24044,    0.2404,      34.3,    34.286,         0,      4.4996
simulation,         8,      0.05,  0.050314,  0.050321,    11.278,    11.278,         0,      4.5028
simulation,         9,       0.1,   0.10055,   0.10056,    11.709,     11.71,         0,      4.4984
simulation,        10,      0.15,   0.15065,   0.15065,    12.562,    12.562,         0,      4.4991
simulation,        11,       0.2,   0.20054,   0.20056,    15.002,    15.002,         0,      4.5013
