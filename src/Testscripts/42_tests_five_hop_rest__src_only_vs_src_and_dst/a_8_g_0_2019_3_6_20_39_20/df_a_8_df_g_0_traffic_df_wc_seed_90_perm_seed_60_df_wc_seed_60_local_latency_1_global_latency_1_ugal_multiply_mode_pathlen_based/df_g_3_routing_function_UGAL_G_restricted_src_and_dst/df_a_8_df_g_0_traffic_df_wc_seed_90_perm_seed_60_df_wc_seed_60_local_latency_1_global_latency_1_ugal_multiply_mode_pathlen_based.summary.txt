vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = UGAL_G_restricted_src_and_dst;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50012,   0.50013,    11.902,    11.903,         0,      3.8779
simulation,         2,      0.74,    0.7401,   0.74014,    20.312,    20.313,         0,      3.8766
simulation,         3,      0.86,   0.72001,   0.76596,    748.23,     486.0,         1,         0.0
simulation,         4,       0.8,   0.73508,   0.74109,    846.12,    480.44,         1,         0.0
simulation,         5,      0.77,    0.7435,   0.75441,    496.77,    411.13,         1,         0.0
simulation,         6,      0.75,    0.7498,   0.75016,    38.696,    38.715,         0,      3.8856
simulation,         7,      0.76,   0.74823,    0.7508,     484.2,    313.26,         1,         0.0
simulation,         8,       0.1,   0.10056,   0.10056,    10.078,    10.078,         0,      3.9333
simulation,         9,       0.2,   0.20055,   0.20056,    10.321,    10.321,         0,      3.9145
simulation,        10,       0.3,   0.30022,   0.30022,    10.681,    10.681,         0,      3.9007
simulation,        11,       0.4,   0.39989,    0.3999,    11.179,     11.18,         0,      3.8876
simulation,        12,       0.5,   0.50012,   0.50013,    11.902,    11.903,         0,      3.8779
simulation,        13,       0.6,   0.60009,    0.6001,    13.025,    13.025,         0,      3.8674
simulation,        14,       0.7,       0.7,   0.70001,    15.698,    15.699,         0,      3.8673
