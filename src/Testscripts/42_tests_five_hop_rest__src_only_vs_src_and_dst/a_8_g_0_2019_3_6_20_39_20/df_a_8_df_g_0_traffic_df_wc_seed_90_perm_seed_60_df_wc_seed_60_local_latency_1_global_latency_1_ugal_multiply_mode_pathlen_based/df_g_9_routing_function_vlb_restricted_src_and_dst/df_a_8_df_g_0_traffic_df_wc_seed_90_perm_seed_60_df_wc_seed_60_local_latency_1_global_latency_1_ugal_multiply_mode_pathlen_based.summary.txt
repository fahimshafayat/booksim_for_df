vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
df_wc_seed = 60;
global_latency = 1;
local_latency = 1;
perm_seed = 60;
seed = 90;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = vlb_restricted_src_and_dst;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.38699,   0.42295,    743.77,    585.39,         1,         0.0
simulation,         2,      0.25,   0.25018,   0.25019,    14.574,    14.574,         0,      5.2748
simulation,         3,      0.37,   0.37015,   0.37019,    19.898,    19.901,         0,      5.2744
simulation,         4,      0.43,   0.39307,   0.40448,    608.96,    465.03,         1,         0.0
simulation,         5,       0.4,   0.39485,   0.39585,    391.65,    108.87,         0,      5.2744
simulation,         6,      0.41,   0.39706,   0.39588,    488.15,    178.38,         1,         0.0
simulation,         7,       0.1,  0.099925,   0.09993,    13.032,    13.032,         0,      5.2759
simulation,         8,       0.2,   0.20004,   0.20005,    13.863,    13.863,         0,      5.2754
simulation,         9,       0.3,   0.29993,   0.29995,    15.698,    15.699,         0,       5.274
simulation,        10,       0.4,   0.39485,   0.39585,    391.65,    108.87,         0,      5.2744
