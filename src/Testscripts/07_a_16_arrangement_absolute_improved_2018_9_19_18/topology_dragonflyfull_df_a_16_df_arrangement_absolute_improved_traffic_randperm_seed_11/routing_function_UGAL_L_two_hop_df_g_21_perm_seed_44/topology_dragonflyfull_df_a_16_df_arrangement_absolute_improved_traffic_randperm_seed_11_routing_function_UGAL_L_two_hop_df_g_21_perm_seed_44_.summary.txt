vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
df_g = 21;
perm_seed = 44;
routing_function = UGAL_L_two_hop;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01,  0.010005,  0.010006,    13.961,         0
simulation,         2,      0.99,   0.66125,   0.68155,    586.19,         1
simulation,         3,       0.5,   0.49977,   0.49978,    18.765,         0
simulation,         4,      0.74,   0.66499,   0.67422,    331.37,         1
simulation,         5,      0.62,   0.61844,   0.61904,    49.108,         0
simulation,         6,      0.68,   0.65561,   0.66034,    162.17,         1
simulation,         7,      0.65,   0.64182,   0.64412,    104.74,         0
simulation,         8,      0.66,   0.64692,   0.65022,    132.59,         0
simulation,         9,      0.67,   0.65198,   0.65561,    159.35,         0
