vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 5;
multitiered_routing_threshold_1 = 25;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 17;
routing_function = PAR_multi_tiered;
shift_by_group = 9;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.46545,   0.47664,    582.91,     369.2,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,    60.789,    60.795,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36997,   0.36997,    77.527,    77.531,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42992,   0.42991,    97.504,     97.51,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45518,   0.45529,    497.16,    206.82,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.43979,   0.43989,    116.29,    116.41,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.45,   0.44814,   0.44815,    404.92,    153.16,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39994,   0.39994,    86.998,    86.999,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35002,   0.35001,    73.135,    73.137,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30001,   0.30001,    65.969,    65.974,         0,  0,  0,  ,  ,  ,  
