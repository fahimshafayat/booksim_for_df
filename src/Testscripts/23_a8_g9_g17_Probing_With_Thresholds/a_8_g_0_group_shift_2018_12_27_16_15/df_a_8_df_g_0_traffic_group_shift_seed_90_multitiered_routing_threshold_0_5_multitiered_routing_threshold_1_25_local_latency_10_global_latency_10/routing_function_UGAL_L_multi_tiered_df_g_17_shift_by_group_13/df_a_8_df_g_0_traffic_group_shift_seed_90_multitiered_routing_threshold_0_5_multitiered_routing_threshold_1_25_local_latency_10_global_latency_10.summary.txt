vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 5;
multitiered_routing_threshold_1 = 25;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 17;
routing_function = UGAL_L_multi_tiered;
shift_by_group = 13;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.39144,   0.42729,    927.03,    673.98,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,    95.064,    95.188,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.34757,   0.34942,    996.14,    666.32,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30999,   0.31001,    106.65,    106.92,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,   0.33762,   0.33811,    430.59,    277.65,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.35,   0.34466,   0.34598,    512.39,    423.63,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.30002,   0.30001,    104.38,    104.61,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20007,   0.20007,    83.599,    83.659,         0,  0,  0,  ,  ,  ,  
