vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 9;
routing_function = vlb;
shift_by_group = 7;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.42568,   0.47714,    770.61,    737.15,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24998,   0.24997,    67.514,    67.514,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36994,   0.36992,    75.161,     75.16,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.43005,   0.42991,     153.6,    153.54,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.42675,   0.44967,    711.79,    660.05,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,    0.4305,   0.43827,    585.51,     576.6,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.4,   0.39989,   0.39984,    83.576,    83.569,         0,  0,  0,  ,  ,  ,  
simulation,         8,      0.35,   0.34991,   0.34989,    72.632,    72.631,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.29992,   0.29991,     69.23,    69.231,         0,  0,  0,  ,  ,  ,  
