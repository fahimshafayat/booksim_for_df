vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 9;
routing_function = PAR_multi_tiered;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4998,   0.49978,    88.516,    88.518,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.52681,    0.5735,    1318.5,    736.04,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.53531,   0.57471,     626.5,    504.73,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.54329,   0.54399,    705.25,    383.26,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.52957,   0.52962,    142.57,    136.83,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.54,   0.53882,   0.53887,    238.32,    162.41,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.55,   0.54421,   0.54495,    496.23,    252.09,         1,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,   0.44998,   0.44994,    69.378,     69.38,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.4,   0.39986,   0.39984,    61.464,    61.466,         0,  0,  0,  ,  ,  ,  
