vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 9;
perm_seed = 72;
routing_function = min;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49629,   0.49627,    362.59,     83.71,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24997,   0.24997,    32.514,    32.517,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36952,   0.36952,    110.58,    42.014,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42886,   0.42886,    272.11,    41.559,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45805,   0.45804,    449.95,    56.527,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,   0.47699,   0.47717,    267.36,    68.393,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,    0.4676,    0.4676,    247.46,    56.682,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44846,   0.44848,    371.22,     56.81,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39917,   0.39917,    191.28,    41.604,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.34968,   0.34967,    66.165,    42.431,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.29992,   0.29991,    32.679,    32.681,         0,  0,  0,  ,  ,  ,  
