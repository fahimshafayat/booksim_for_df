vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 9;
perm_seed = 72;
routing_function = PAR_multi_tiered;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4998,   0.49978,    37.861,    37.867,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.74031,   0.74028,    49.566,    49.573,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.77361,   0.79036,    810.36,    372.67,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.78263,   0.78344,    563.01,    212.81,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.76988,   0.76989,    79.246,     65.65,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.78,   0.77881,    0.7788,    174.15,     77.56,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.79,   0.78698,     0.787,    354.19,    89.807,         0,  0,  0,  ,  ,  ,  
simulation,         8,      0.75,   0.75033,   0.75028,    52.071,    52.081,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.7,   0.70021,   0.70017,    44.155,     44.16,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.65,   0.65006,   0.65004,    40.967,    40.972,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.6,   0.59999,   0.59996,    39.342,    39.347,         0,  0,  0,  ,  ,  ,  
