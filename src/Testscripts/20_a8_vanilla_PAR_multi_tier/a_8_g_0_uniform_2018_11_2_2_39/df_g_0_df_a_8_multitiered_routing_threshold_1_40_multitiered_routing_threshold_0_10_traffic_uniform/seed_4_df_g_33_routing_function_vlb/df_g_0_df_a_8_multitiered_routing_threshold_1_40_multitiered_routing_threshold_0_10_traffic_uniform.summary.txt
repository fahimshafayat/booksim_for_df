input_speedup = 1;
warmup_periods = 3;
alloc_iters = 1;
st_final_delay = 1;
sw_allocator = separable_input_first;
injection_rate_uses_flits = 1;
vc_alloc_delay = 1;
vc_allocator = separable_input_first;
credit_delay = 2;
priority = none;
sim_count = 1;
output_speedup = 1;
wait_for_tail_credit = 0;
sw_alloc_delay = 1;
routing_delay = 0;
internal_speedup = 4.0;
packet_size = 1;
num_vcs = 7;
sample_period = 1000;
vc_buf_size = 64;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 33;
routing_function = vlb;
seed = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010059,  0.010056,    14.813,    14.814,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.44592,   0.81656,    662.55,    662.38,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,   0.49689,   0.50001,    120.14,    120.79,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         4,      0.74,   0.53368,   0.62233,    919.37,    919.09,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.62,   0.51969,   0.62038,    651.38,    651.38,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.56,   0.51171,   0.56005,    576.92,    569.74,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.53,   0.50343,   0.52937,    491.52,    539.67,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         8,      0.54,   0.50652,   0.53975,     515.2,    506.09,         1,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,   0.44972,   0.44977,    33.806,    33.824,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,        11,       0.4,   0.39959,    0.3996,    24.059,    24.061,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,        12,      0.35,   0.34973,   0.34973,    20.452,    20.451,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
