input_speedup = 1;
warmup_periods = 3;
alloc_iters = 1;
st_final_delay = 1;
sw_allocator = separable_input_first;
injection_rate_uses_flits = 1;
vc_alloc_delay = 1;
vc_allocator = separable_input_first;
credit_delay = 2;
priority = none;
sim_count = 1;
output_speedup = 1;
wait_for_tail_credit = 0;
sw_alloc_delay = 1;
routing_delay = 0;
internal_speedup = 4.0;
packet_size = 1;
num_vcs = 7;
sample_period = 1000;
vc_buf_size = 64;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 33;
routing_function = UGAL_L;
seed = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010062,  0.010056,    9.6926,    9.6928,         0,  60320,  3321,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 1596 , |3-> 381 , |4-> 4149 , |5-> 19847 , |6-> 34347 ,,  |1-> 0 , |2-> 89 , |3-> 18 , |4-> 229 , |5-> 1084 , |6-> 1901 ,
simulation,         2,      0.99,   0.98201,   0.99004,    134.81,     135.7,         0,  7366295,  367002,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 200487 , |3-> 38798 , |4-> 467730 , |5-> 2377025 , |6-> 4282255 ,,  |1-> 0 , |2-> 6314 , |3-> 7487 , |4-> 62821 , |5-> 177411 , |6-> 112969 ,
