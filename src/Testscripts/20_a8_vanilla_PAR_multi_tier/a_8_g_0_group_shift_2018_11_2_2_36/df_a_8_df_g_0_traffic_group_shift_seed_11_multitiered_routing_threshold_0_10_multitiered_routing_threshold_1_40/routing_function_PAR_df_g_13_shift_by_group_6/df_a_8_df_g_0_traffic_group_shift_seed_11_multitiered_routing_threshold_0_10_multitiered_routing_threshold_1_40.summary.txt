vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 1000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
df_g = 13;
routing_function = PAR;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099752, 0.0099688,    12.174,    12.178,         0,  34851,  9753,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 13 , |3-> 236 , |4-> 2430 , |5-> 11495 , |6-> 20677 ,,  |1-> 0 , |2-> 1 , |3-> 85 , |4-> 644 , |5-> 3226 , |6-> 5797 ,
simulation,         2,      0.99,   0.39409,   0.63497,    554.82,     542.5,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,   0.40085,   0.43897,    426.78,    614.14,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.25,   0.24999,   0.24994,    17.185,    17.189,         0,  572475,  437963,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 166 , |3-> 3224 , |4-> 37112 , |5-> 179936 , |6-> 352037 ,,  |1-> 0 , |2-> 228 , |3-> 3583 , |4-> 34495 , |5-> 150821 , |6-> 248836 ,
simulation,         5,      0.37,    0.3697,    0.3699,    30.757,    30.871,         0,  764475,  745087,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 195 , |3-> 4392 , |4-> 49579 , |5-> 238330 , |6-> 471979 ,,  |1-> 0 , |2-> 423 , |3-> 5886 , |4-> 57129 , |5-> 256181 , |6-> 425468 ,
simulation,         6,      0.43,    0.4098,   0.42267,    408.11,    374.45,         0,  1955220,  2406783,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 437 , |3-> 9470 , |4-> 117316 , |5-> 593700 , |6-> 1234297 ,,  |1-> 0 , |2-> 1428 , |3-> 20453 , |4-> 194399 , |5-> 831881 , |6-> 1358622 ,
simulation,         7,      0.46,   0.40568,   0.42966,    455.11,    469.85,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.44,   0.40759,   0.42394,    411.19,     388.8,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,    0.3984,    0.4001,    81.198,    82.732,         0,  1097631,  1161044,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 292 , |3-> 5964 , |4-> 69618 , |5-> 339595 , |6-> 682162 ,,  |1-> 0 , |2-> 641 , |3-> 9252 , |4-> 91331 , |5-> 398551 , |6-> 661269 ,
simulation,        10,      0.35,   0.34978,   0.34981,    23.838,    23.841,         0,  730974,  681609,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 219 , |3-> 4198 , |4-> 47337 , |5-> 228387 , |6-> 450833 ,,  |1-> 0 , |2-> 387 , |3-> 5351 , |4-> 52561 , |5-> 234091 , |6-> 389219 ,
simulation,        11,       0.3,    0.2997,   0.29964,    19.468,    19.473,         0,  659317,  555122,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 201 , |3-> 3669 , |4-> 42834 , |5-> 206972 , |6-> 405641 ,,  |1-> 0 , |2-> 263 , |3-> 4574 , |4-> 43363 , |5-> 190431 , |6-> 316491 ,
