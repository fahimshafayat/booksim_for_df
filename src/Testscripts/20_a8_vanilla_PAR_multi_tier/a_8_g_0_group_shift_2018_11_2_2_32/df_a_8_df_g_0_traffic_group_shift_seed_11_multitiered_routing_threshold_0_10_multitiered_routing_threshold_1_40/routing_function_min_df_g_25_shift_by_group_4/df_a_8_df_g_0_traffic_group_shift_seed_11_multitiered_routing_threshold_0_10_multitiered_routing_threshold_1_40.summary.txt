vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 1000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
df_g = 25;
routing_function = min;
shift_by_group = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099671, 0.0099642,    9.6902,    9.6907,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,  0.043414,   0.51454,    468.32,    468.32,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,  0.043577,   0.27894,    865.02,    865.02,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.25,  0.043562,   0.24848,    728.43,    728.43,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.13,  0.043549,   0.13052,    502.66,    502.66,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.07,   0.04375,  0.069436,    651.78,    651.78,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.04,  0.034793,  0.040205,    422.73,    498.09,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.02,  0.020004,  0.020002,    10.132,    10.133,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         9,      0.03,  0.030041,  0.030062,    17.245,    17.267,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
