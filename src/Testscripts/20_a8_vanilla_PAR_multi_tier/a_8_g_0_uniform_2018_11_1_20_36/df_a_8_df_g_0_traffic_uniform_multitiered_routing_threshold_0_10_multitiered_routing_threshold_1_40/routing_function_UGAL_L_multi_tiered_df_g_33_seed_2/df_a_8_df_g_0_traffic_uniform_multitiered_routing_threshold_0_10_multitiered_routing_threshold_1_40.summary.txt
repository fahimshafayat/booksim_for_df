vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 1000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 33;
routing_function = UGAL_L_multi_tiered;
seed = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010018,  0.010027,    9.5163,    9.5173,         0,  59954,  3247,  |4-> 58409 , |5-> 0 , |6-> 0 ,,  |4-> 3170 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 1656 , |3-> 5302 , |4-> 52996 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 81 , |3-> 292 , |4-> 2874 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.98206,   0.99006,    136.38,    137.16,         0,  8328678,  427497,  |4-> 193038 , |5-> 3937927 , |6-> 3972706 ,,  |4-> 4036 , |5-> 157478 , |6-> 258172 ,,  |1-> 0 , |2-> 228055 , |3-> 100118 , |4-> 1101775 , |5-> 4516532 , |6-> 2382198 ,,  |1-> 0 , |2-> 8132 , |3-> 9255 , |4-> 80537 , |5-> 247581 , |6-> 81992 ,
