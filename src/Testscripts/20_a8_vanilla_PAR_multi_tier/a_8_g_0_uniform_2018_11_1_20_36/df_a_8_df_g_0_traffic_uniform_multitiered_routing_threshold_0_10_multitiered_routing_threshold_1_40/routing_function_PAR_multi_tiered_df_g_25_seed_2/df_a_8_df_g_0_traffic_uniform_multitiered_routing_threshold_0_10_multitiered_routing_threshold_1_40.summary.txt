vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 1000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 25;
routing_function = PAR_multi_tiered;
seed = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099492, 0.0099483,    9.6708,     9.672,         0,  80958,  4863,  |4-> 79375 , |5-> 0 , |6-> 0 ,,  |4-> 4759 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 1783 , |3-> 8193 , |4-> 70982 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 121 , |3-> 501 , |4-> 4241 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.76375,   0.77551,    621.29,    570.52,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,    0.4999,   0.49994,    12.627,     12.63,         0,  3516596,  645446,  |4-> 3444149 , |5-> 2055 , |6-> 0 ,,  |4-> 623176 , |5-> 8420 , |6-> 0 ,,  |1-> 0 , |2-> 79690 , |3-> 364215 , |4-> 3071152 , |5-> 1539 , |6-> 0 ,,  |1-> 0 , |2-> 15334 , |3-> 61114 , |4-> 562435 , |5-> 6563 , |6-> 0 ,
simulation,         4,      0.74,    0.7401,   0.74016,     20.38,    20.394,         0,  5437053,  848329,  |4-> 4122191 , |5-> 1204243 , |6-> 0 ,,  |4-> 314474 , |5-> 518585 , |6-> 0 ,,  |1-> 0 , |2-> 122609 , |3-> 455061 , |4-> 3929592 , |5-> 929791 , |6-> 0 ,,  |1-> 0 , |2-> 16402 , |3-> 40585 , |4-> 382364 , |5-> 408978 , |6-> 0 ,
simulation,         5,      0.86,   0.76929,   0.80251,    510.12,    433.07,         1,  0,  0,  ,  ,  ,  
simulation,         6,       0.8,   0.77981,   0.79703,    208.91,    222.33,         0,  11538921,  2211822,  |4-> 2122322 , |5-> 4014695 , |6-> 5165973 ,,  |4-> 57537 , |5-> 766237 , |6-> 1341194 ,,  |1-> 0 , |2-> 245577 , |3-> 332016 , |4-> 3182741 , |5-> 5152872 , |6-> 2625715 ,,  |1-> 0 , |2-> 48529 , |3-> 47435 , |4-> 420544 , |5-> 1151154 , |6-> 544160 ,
simulation,         7,      0.83,   0.77316,   0.79887,    398.94,    345.72,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.81,   0.77966,   0.80082,    282.29,    280.01,         0,  11551589,  2204161,  |4-> 1878972 , |5-> 3623403 , |6-> 5814279 ,,  |4-> 51026 , |5-> 630235 , |6-> 1473772 ,,  |1-> 0 , |2-> 243605 , |3-> 303368 , |4-> 2939656 , |5-> 5097993 , |6-> 2966967 ,,  |1-> 0 , |2-> 50878 , |3-> 47554 , |4-> 411259 , |5-> 1111964 , |6-> 582506 ,
simulation,         9,      0.82,   0.77662,   0.80094,     376.0,     342.5,         0,  12015778,  2330461,  |4-> 1817497 , |5-> 3526195 , |6-> 6429066 ,,  |4-> 49756 , |5-> 566940 , |6-> 1660392 ,,  |1-> 0 , |2-> 251560 , |3-> 298959 , |4-> 2915588 , |5-> 5256421 , |6-> 3293250 ,,  |1-> 0 , |2-> 55176 , |3-> 50151 , |4-> 427884 , |5-> 1150614 , |6-> 646636 ,
simulation,        11,      0.75,   0.75012,   0.75023,    22.546,    22.573,         0,  5480434,  890562,  |4-> 3735316 , |5-> 1633622 , |6-> 0 ,,  |4-> 246516 , |5-> 628685 , |6-> 0 ,,  |1-> 0 , |2-> 122949 , |3-> 423132 , |4-> 3671995 , |5-> 1262358 , |6-> 0 ,,  |1-> 0 , |2-> 16333 , |3-> 36039 , |4-> 343033 , |5-> 495157 , |6-> 0 ,
simulation,        12,       0.7,   0.70013,   0.70012,    16.558,    16.566,         0,  5152833,  763981,  |4-> 4667052 , |5-> 382697 , |6-> 0 ,,  |4-> 493006 , |5-> 255899 , |6-> 0 ,,  |1-> 0 , |2-> 116302 , |3-> 497447 , |4-> 4245010 , |5-> 294074 , |6-> 0 ,,  |1-> 0 , |2-> 16383 , |3-> 51959 , |4-> 493551 , |5-> 202088 , |6-> 0 ,
simulation,        13,      0.65,   0.65011,   0.65018,     14.78,    14.785,         0,  4758196,  720980,  |4-> 4561335 , |5-> 101866 , |6-> 0 ,,  |4-> 594757 , |5-> 111352 , |6-> 0 ,,  |1-> 0 , |2-> 107320 , |3-> 482138 , |4-> 4090889 , |5-> 77849 , |6-> 0 ,,  |1-> 0 , |2-> 16358 , |3-> 59656 , |4-> 557326 , |5-> 87640 , |6-> 0 ,
