vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 1000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 33;
routing_function = vlb;
seed = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010022,  0.010027,    14.809,    14.811,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.44647,   0.81595,    662.01,    661.81,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,   0.49689,   0.49971,    121.42,    121.76,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         4,      0.74,   0.53562,   0.62565,    920.17,     919.9,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.62,   0.52205,   0.61928,    646.53,    646.53,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.56,   0.51033,   0.56023,    584.13,    575.29,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.53,   0.50369,   0.52955,    494.01,    546.27,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         8,      0.54,   0.50627,   0.53948,    517.57,     508.3,         1,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,   0.45003,   0.45002,    33.947,    33.942,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,        11,       0.4,   0.39996,   0.39995,    24.153,    24.152,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,        12,      0.35,   0.34966,   0.34964,     20.45,     20.45,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
