vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = one_vs_two;
df_g = 33;
df_wc_seed = 60;
routing_function = UGAL_G_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.43278,   0.43273,    742.13,     345.5,         1,         0.0
simulation,         2,      0.25,   0.25001,   0.25002,    15.478,    15.478,         0,      5.4403
simulation,         3,      0.37,   0.36992,   0.36993,     18.58,     18.58,         0,      5.5259
simulation,         4,      0.43,   0.42982,   0.42985,    23.476,    23.478,         0,      5.5572
simulation,         5,      0.46,   0.45994,   0.45992,    36.098,    36.128,         0,      5.5754
simulation,         6,      0.48,   0.44096,   0.44053,    383.23,    219.97,         1,         0.0
simulation,         7,      0.47,   0.44588,   0.44592,    492.83,    162.16,         1,         0.0
simulation,         8,       0.1,  0.099897,  0.099898,    13.542,    13.543,         0,      5.2326
simulation,         9,       0.2,       0.2,   0.20001,    14.717,    14.717,         0,      5.3789
simulation,        10,       0.3,   0.29997,   0.29998,    16.448,    16.448,         0,      5.4833
simulation,        11,       0.4,   0.39986,   0.39989,     20.26,     20.26,         0,      5.5413
