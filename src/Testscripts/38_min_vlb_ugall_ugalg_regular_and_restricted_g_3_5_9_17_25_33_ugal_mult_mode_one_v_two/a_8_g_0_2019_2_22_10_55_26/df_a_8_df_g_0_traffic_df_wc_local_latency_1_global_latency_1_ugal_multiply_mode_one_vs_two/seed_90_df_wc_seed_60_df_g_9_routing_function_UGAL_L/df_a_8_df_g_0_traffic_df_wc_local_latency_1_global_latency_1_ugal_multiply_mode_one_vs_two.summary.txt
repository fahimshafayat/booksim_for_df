vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = one_vs_two;
df_g = 9;
df_wc_seed = 60;
routing_function = UGAL_L;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35824,   0.44938,    576.48,    562.97,         1,         0.0
simulation,         2,      0.25,   0.25017,   0.25019,    54.516,     54.61,         0,      5.1544
simulation,         3,      0.37,   0.35602,   0.36256,    568.15,    627.42,         1,         0.0
simulation,         4,      0.31,   0.30997,   0.30999,    64.935,    65.155,         0,      5.3985
simulation,         5,      0.34,    0.3396,   0.34003,    99.212,    101.03,         0,      5.4885
simulation,         6,      0.35,   0.34597,   0.34941,    464.59,    461.75,         0,      5.5045
simulation,         7,      0.36,   0.35059,   0.35753,    527.68,    538.12,         1,         0.0
simulation,         8,       0.1,  0.099924,   0.09993,    12.047,    12.048,         0,      4.7392
simulation,         9,       0.2,       0.2,   0.20005,    36.022,    36.043,         0,      4.8449
simulation,        10,       0.3,   0.29993,   0.29995,    62.306,    62.496,         0,      5.3644
