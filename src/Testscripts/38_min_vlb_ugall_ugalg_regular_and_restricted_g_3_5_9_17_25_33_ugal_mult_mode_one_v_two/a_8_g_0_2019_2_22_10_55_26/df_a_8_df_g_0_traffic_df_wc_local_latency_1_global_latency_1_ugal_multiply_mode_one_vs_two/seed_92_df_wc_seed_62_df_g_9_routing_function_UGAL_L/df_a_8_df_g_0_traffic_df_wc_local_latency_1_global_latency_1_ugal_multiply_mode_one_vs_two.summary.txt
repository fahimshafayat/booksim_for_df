vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = one_vs_two;
df_g = 9;
df_wc_seed = 62;
routing_function = UGAL_L;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35787,   0.44898,    571.27,    558.37,         1,         0.0
simulation,         2,      0.25,   0.25011,   0.25012,    54.215,    54.305,         0,      5.1544
simulation,         3,      0.37,   0.35484,   0.36816,     487.2,    483.47,         1,         0.0
simulation,         4,      0.31,   0.31021,   0.31023,    64.449,    64.645,         0,      5.4023
simulation,         5,      0.34,   0.33982,   0.34031,    102.71,    104.02,         0,      5.4915
simulation,         6,      0.35,   0.34567,   0.34954,    448.82,    444.96,         0,      5.5059
simulation,         7,      0.36,   0.35101,   0.35837,    484.13,    494.16,         1,         0.0
simulation,         8,       0.1,  0.099965,  0.099966,    12.056,    12.057,         0,      4.7457
simulation,         9,       0.2,    0.1999,   0.19992,    33.489,    33.517,         0,       4.846
simulation,        10,       0.3,   0.30025,   0.30028,    61.877,     62.05,         0,      5.3681
