vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = one_vs_two;
df_g = 5;
perm_seed = 60;
routing_function = vlb_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4824,   0.48401,    492.04,    224.09,         1,         0.0
simulation,         2,      0.25,    0.2502,   0.25021,    11.419,    11.419,         0,      4.2916
simulation,         3,      0.37,   0.37024,   0.37022,    12.354,    12.355,         0,      4.2909
simulation,         4,      0.43,   0.43003,   0.43003,    13.455,    13.456,         0,      4.2916
simulation,         5,      0.46,   0.45902,   0.45966,    57.039,    48.625,         0,      4.2916
simulation,         6,      0.48,   0.47614,   0.47611,    288.37,    68.714,         0,      4.2924
simulation,         7,      0.49,    0.4756,   0.47549,    502.61,    167.42,         1,         0.0
simulation,         8,       0.1,   0.10048,   0.10047,    10.831,    10.831,         0,      4.2923
simulation,         9,       0.2,   0.20032,   0.20032,    11.178,    11.178,         0,      4.2909
simulation,        10,       0.3,   0.30003,   0.30002,    11.721,    11.721,         0,       4.292
simulation,        11,       0.4,   0.40014,   0.40013,    12.771,    12.772,         0,      4.2921
