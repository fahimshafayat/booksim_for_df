vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = one_vs_two;
df_g = 25;
perm_seed = 62;
routing_function = vlb_restricted;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35232,   0.43407,    528.39,    514.25,         1,         0.0
simulation,         2,      0.25,   0.24986,   0.24986,    14.878,    14.879,         0,      5.5166
simulation,         3,      0.37,   0.32667,     0.328,    562.37,    432.21,         1,         0.0
simulation,         4,      0.31,   0.30981,   0.30981,    16.214,    16.216,         0,      5.5168
simulation,         5,      0.34,   0.33776,   0.33832,     135.9,    100.64,         0,      5.5168
simulation,         6,      0.35,   0.33193,   0.33488,    542.25,    354.06,         1,         0.0
simulation,         7,       0.1,  0.099979,  0.099978,    13.497,    13.497,         0,      5.5161
simulation,         8,       0.2,   0.19988,   0.19988,    14.262,    14.262,         0,      5.5174
simulation,         9,       0.3,   0.29977,   0.29978,    15.909,     15.91,         0,      5.5167
