vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = one_vs_two;
df_g = 5;
perm_seed = 62;
routing_function = UGAL_L_restricted;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49983,   0.49982,    10.375,    10.376,         0,      3.5562
simulation,         2,      0.74,   0.73952,   0.73949,     12.04,    12.041,         0,      3.5322
simulation,         3,      0.86,   0.85974,   0.85973,     14.65,    14.653,         0,      3.5409
simulation,         4,      0.92,   0.91994,   0.91994,    18.979,     18.99,         0,      3.5564
simulation,         5,      0.95,    0.9499,   0.94991,    22.537,     22.55,         0,      3.5677
simulation,         6,      0.97,   0.97005,   0.96994,    28.177,    28.276,         0,      3.5756
simulation,         7,      0.98,   0.97954,   0.97958,    43.777,    35.128,         0,      3.5803
simulation,         8,       0.1,  0.099934,  0.099931,     9.392,    9.3921,         0,      3.6181
simulation,         9,       0.2,   0.19974,   0.19973,    9.5856,    9.5858,         0,      3.6157
simulation,        10,       0.3,   0.29977,   0.29977,    9.7725,    9.7728,         0,      3.5922
simulation,        11,       0.4,   0.39985,   0.39985,    10.029,    10.029,         0,      3.5726
simulation,        12,       0.5,   0.49983,   0.49982,    10.375,    10.376,         0,      3.5562
simulation,        13,       0.6,   0.59907,   0.59907,    10.854,    10.854,         0,      3.5431
simulation,        14,       0.7,   0.69946,   0.69942,    11.601,    11.602,         0,       3.534
simulation,        15,       0.8,   0.79937,   0.79936,    12.953,    12.954,         0,      3.5333
simulation,        16,       0.9,   0.89997,   0.89997,    16.942,    16.945,         0,      3.5515
