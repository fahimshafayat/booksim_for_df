vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = one_vs_two;
df_g = 3;
routing_function = min;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50013,   0.50013,    9.3284,    9.3286,         0,      3.1263
simulation,         2,      0.74,   0.74012,   0.74014,    10.928,    10.928,         0,      3.1261
simulation,         3,      0.86,   0.86028,   0.86025,    13.202,    13.203,         0,      3.1246
simulation,         4,      0.92,   0.92013,   0.92019,    16.468,    16.469,         0,      3.1257
simulation,         5,      0.95,   0.95028,   0.95025,    20.514,    20.521,         0,      3.1255
simulation,         6,      0.97,   0.97023,   0.97029,    27.424,    27.441,         0,      3.1253
simulation,         7,      0.98,   0.97986,   0.98018,    34.965,    35.044,         0,      3.1258
simulation,         8,       0.1,   0.10056,   0.10056,    8.3814,     8.382,         0,      3.1261
simulation,         9,       0.2,   0.20055,   0.20056,    8.5327,    8.5331,         0,      3.1235
simulation,        10,       0.3,    0.3002,   0.30022,    8.7306,    8.7308,         0,      3.1242
simulation,        11,       0.4,   0.39991,    0.3999,    8.9839,    8.9843,         0,      3.1238
simulation,        12,       0.5,   0.50013,   0.50013,    9.3284,    9.3286,         0,      3.1263
simulation,        13,       0.6,   0.60011,    0.6001,     9.798,    9.7983,         0,      3.1257
simulation,        14,       0.7,   0.70001,   0.70001,    10.515,    10.515,         0,      3.1251
simulation,        15,       0.8,    0.8001,    0.8001,    11.786,    11.787,         0,      3.1256
simulation,        16,       0.9,   0.90015,   0.90014,    14.962,    14.964,         0,      3.1251
