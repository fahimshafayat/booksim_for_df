vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = one_vs_two;
df_g = 33;
routing_function = UGAL_G;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49989,   0.49989,    11.331,    11.331,         0,      3.7919
simulation,         2,      0.74,   0.73989,   0.73991,     14.07,     14.07,         0,      3.7457
simulation,         3,      0.86,   0.85984,   0.85984,    18.547,    18.548,         0,      3.7378
simulation,         4,      0.92,   0.91989,   0.91989,    24.797,    24.802,         0,      3.7372
simulation,         5,      0.95,    0.9499,   0.94991,    32.212,    32.225,         0,      3.7371
simulation,         6,      0.97,   0.96997,   0.96991,    43.404,    43.431,         0,      3.7368
simulation,         7,      0.98,   0.97993,      0.98,    55.645,    55.693,         0,      3.7353
simulation,         8,       0.1,  0.099898,  0.099898,    10.402,    10.402,         0,       4.086
simulation,         9,       0.2,   0.20001,   0.20001,    10.411,    10.411,         0,      3.9618
simulation,        10,       0.3,   0.29997,   0.29998,    10.564,    10.565,         0,      3.8816
simulation,        11,       0.4,   0.39988,   0.39989,    10.857,    10.857,         0,      3.8282
simulation,        12,       0.5,   0.49989,   0.49989,    11.331,    11.331,         0,      3.7919
simulation,        13,       0.6,   0.59986,   0.59986,    12.075,    12.075,         0,      3.7668
simulation,        14,       0.7,   0.69991,   0.69992,     13.32,     13.32,         0,      3.7502
simulation,        15,       0.8,   0.79979,    0.7998,    15.702,    15.703,         0,      3.7406
simulation,        16,       0.9,   0.89986,   0.89987,    22.019,    22.021,         0,      3.7372
