vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = one_vs_two;
df_g = 9;
routing_function = UGAL_L_restricted;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50008,   0.50011,    11.184,    11.184,         0,      3.7447
simulation,         2,      0.74,   0.74023,   0.74028,    13.586,    13.587,         0,       3.692
simulation,         3,      0.86,   0.86033,   0.86034,    17.126,    17.127,         0,       3.673
simulation,         4,      0.92,   0.92034,   0.92037,    21.828,    21.833,         0,      3.6665
simulation,         5,      0.95,   0.95028,   0.95029,    27.178,    27.188,         0,      3.6646
simulation,         6,      0.97,   0.97021,   0.97024,    35.841,    35.862,         0,      3.6634
simulation,         7,      0.98,   0.98016,   0.98017,    45.409,    45.454,         0,      3.6632
simulation,         8,       0.1,  0.099964,  0.099966,    9.9198,    9.9201,         0,      3.8572
simulation,         9,       0.2,   0.19991,   0.19992,    10.152,    10.152,         0,      3.8442
simulation,        10,       0.3,   0.30028,   0.30028,    10.381,    10.381,         0,      3.8039
simulation,        11,       0.4,   0.40008,   0.40009,    10.721,    10.721,         0,      3.7736
simulation,        12,       0.5,   0.50008,   0.50011,    11.184,    11.184,         0,      3.7447
simulation,        13,       0.6,   0.59994,   0.59996,    11.872,    11.872,         0,      3.7215
simulation,        14,       0.7,   0.70017,    0.7002,    12.959,    12.959,         0,      3.6993
simulation,        15,       0.8,   0.80023,   0.80024,    14.904,    14.905,         0,      3.6822
simulation,        16,       0.9,   0.90029,   0.90029,     19.72,    19.724,         0,      3.6681
