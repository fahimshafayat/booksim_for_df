vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = one_vs_two;
df_g = 33;
routing_function = min;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49986,   0.49986,    11.011,    11.011,         0,      3.6932
simulation,         2,      0.74,   0.73988,   0.73987,    13.927,    13.928,         0,      3.6932
simulation,         3,      0.86,   0.85999,   0.85998,    18.806,    18.808,         0,      3.6933
simulation,         4,      0.92,   0.91999,   0.91997,    26.042,    26.047,         0,      3.6932
simulation,         5,      0.95,   0.94996,      0.95,    34.846,    34.859,         0,      3.6929
simulation,         6,      0.97,   0.96999,   0.96999,    48.525,     48.55,         0,      3.6933
simulation,         7,      0.98,   0.98004,      0.98,    62.685,     62.75,         0,      3.6931
simulation,         8,       0.1,  0.099993,  0.099997,    9.5654,    9.5656,         0,      3.6933
simulation,         9,       0.2,   0.19985,   0.19986,    9.7906,    9.7907,         0,      3.6931
simulation,        10,       0.3,   0.29977,   0.29977,    10.081,    10.081,         0,       3.693
simulation,        11,       0.4,    0.3997,    0.3997,    10.469,    10.469,         0,      3.6933
simulation,        12,       0.5,   0.49986,   0.49986,    11.011,    11.011,         0,      3.6932
simulation,        13,       0.6,   0.59987,   0.59986,    11.813,    11.813,         0,       3.693
simulation,        14,       0.7,   0.69993,   0.69993,     13.13,    13.131,         0,       3.693
simulation,        15,       0.8,   0.79995,   0.79994,    15.676,    15.677,         0,       3.693
simulation,        16,       0.9,   0.90003,   0.90002,    22.752,    22.755,         0,      3.6932
