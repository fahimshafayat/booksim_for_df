vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = one_vs_two;
df_g = 33;
routing_function = vlb_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4999,   0.49989,    48.216,    48.269,         0,      5.6072
simulation,         2,      0.74,   0.49039,   0.58517,    834.43,    732.68,         1,         0.0
simulation,         3,      0.62,   0.49522,   0.58834,    499.89,    493.91,         1,         0.0
simulation,         4,      0.56,   0.49188,   0.53373,     545.2,    499.52,         1,         0.0
simulation,         5,      0.53,   0.48222,   0.50224,    849.32,    629.48,         1,         0.0
simulation,         6,      0.51,   0.47982,   0.49317,    514.62,    431.54,         1,         0.0
simulation,         7,       0.1,  0.099896,  0.099898,    13.693,    13.693,         0,      5.6077
simulation,         8,       0.2,       0.2,   0.20001,    14.424,    14.424,         0,      5.6076
simulation,         9,       0.3,   0.29997,   0.29998,     15.69,     15.69,         0,      5.6075
simulation,        10,       0.4,   0.39987,   0.39989,    18.577,    18.577,         0,      5.6075
simulation,        11,       0.5,    0.4999,   0.49989,    48.216,    48.269,         0,      5.6072
