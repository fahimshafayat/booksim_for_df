vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = one_vs_two;
df_g = 25;
routing_function = UGAL_G_restricted;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49987,   0.49988,    11.641,    11.641,         0,      3.8363
simulation,         2,      0.74,   0.73991,   0.73993,     15.22,     15.22,         0,       3.795
simulation,         3,      0.86,   0.86007,   0.86008,    26.145,    26.149,         0,      3.8232
simulation,         4,      0.92,   0.85632,   0.85817,     597.3,    365.03,         1,         0.0
simulation,         5,      0.89,   0.89008,   0.89007,    38.891,      38.9,         0,      3.8529
simulation,         6,       0.9,   0.90004,    0.9001,    48.297,    48.318,         0,      3.8635
simulation,         7,      0.91,   0.86354,   0.86561,    542.38,    339.54,         1,         0.0
simulation,         8,       0.1,  0.099979,  0.099978,    10.305,    10.305,         0,       4.035
simulation,         9,       0.2,   0.19987,   0.19988,     10.44,     10.44,         0,      3.9621
simulation,        10,       0.3,   0.29978,   0.29978,    10.684,    10.685,         0,       3.908
simulation,        11,       0.4,   0.39975,   0.39976,    11.063,    11.063,         0,      3.8669
simulation,        12,       0.5,   0.49987,   0.49988,    11.641,    11.641,         0,      3.8363
simulation,        13,       0.6,   0.59992,   0.59992,    12.537,    12.537,         0,       3.812
simulation,        14,       0.7,   0.69998,   0.69998,    14.156,    14.157,         0,      3.7977
simulation,        15,       0.8,   0.79997,   0.79998,     18.03,    18.031,         0,      3.7981
simulation,        16,       0.9,   0.90004,    0.9001,    48.297,    48.318,         0,      3.8635
