vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = one_vs_two;
df_g = 25;
routing_function = UGAL_L;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4998,    0.4998,    12.598,    12.599,         0,      4.0654
simulation,         2,      0.74,    0.7401,   0.74005,    49.035,    49.098,         0,      4.1329
simulation,         3,      0.86,   0.75378,   0.75523,    1308.6,    520.18,         1,         0.0
simulation,         4,       0.8,   0.76179,   0.76447,    638.58,    357.71,         1,         0.0
simulation,         5,      0.77,    0.7619,   0.76259,    377.18,    166.21,         0,      4.1388
simulation,         6,      0.78,   0.76271,   0.76462,    497.17,    240.38,         1,         0.0
simulation,         7,       0.1,    0.0999,  0.099902,    10.902,    10.902,         0,      4.3156
simulation,         8,       0.2,   0.19997,   0.19997,    11.128,    11.129,         0,      4.2571
simulation,         9,       0.3,   0.29999,       0.3,    11.401,    11.402,         0,      4.1798
simulation,        10,       0.4,   0.39987,   0.39987,    11.854,    11.854,         0,      4.1172
simulation,        11,       0.5,    0.4998,    0.4998,    12.598,    12.599,         0,      4.0654
simulation,        12,       0.6,   0.59989,   0.59989,    14.033,    14.034,         0,      4.0307
simulation,        13,       0.7,   0.70005,   0.70003,    19.793,    19.801,         0,       4.045
