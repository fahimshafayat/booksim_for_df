vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = one_vs_two;
df_g = 17;
routing_function = UGAL_L_restricted;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50011,   0.50012,    11.679,     11.68,         0,      3.8924
simulation,         2,      0.74,   0.74001,   0.74001,    14.476,    14.476,         0,      3.8225
simulation,         3,      0.86,   0.86015,   0.86014,    19.018,     19.02,         0,      3.8004
simulation,         4,      0.92,   0.92016,   0.92012,    25.551,    25.553,         0,      3.7932
simulation,         5,      0.95,   0.95005,   0.95004,    33.524,    33.533,         0,      3.7898
simulation,         6,      0.97,   0.96999,   0.96999,     46.46,     46.49,         0,      3.7838
simulation,         7,      0.98,   0.97998,   0.97996,    59.195,    59.238,         0,       3.777
simulation,         8,       0.1,   0.10011,   0.10011,     10.32,     10.32,         0,      4.0467
simulation,         9,       0.2,   0.20018,   0.20018,    10.549,    10.549,         0,      4.0193
simulation,        10,       0.3,   0.30014,   0.30013,    10.795,    10.795,         0,      3.9698
simulation,        11,       0.4,   0.40011,    0.4001,    11.156,    11.156,         0,      3.9275
simulation,        12,       0.5,   0.50011,   0.50012,    11.679,     11.68,         0,      3.8924
simulation,        13,       0.6,       0.6,   0.60001,    12.461,    12.462,         0,      3.8602
simulation,        14,       0.7,   0.69988,    0.6999,     13.72,     13.72,         0,      3.8321
simulation,        15,       0.8,   0.80003,   0.80004,    16.111,    16.112,         0,      3.8099
simulation,        16,       0.9,    0.9001,   0.90012,    22.648,    22.649,         0,      3.7957
