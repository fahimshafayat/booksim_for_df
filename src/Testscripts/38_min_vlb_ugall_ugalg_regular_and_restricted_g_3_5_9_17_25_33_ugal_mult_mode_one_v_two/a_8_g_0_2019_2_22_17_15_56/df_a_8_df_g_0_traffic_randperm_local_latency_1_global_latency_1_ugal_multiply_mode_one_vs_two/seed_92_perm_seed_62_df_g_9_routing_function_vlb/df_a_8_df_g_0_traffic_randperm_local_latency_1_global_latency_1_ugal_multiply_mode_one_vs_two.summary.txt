vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = one_vs_two;
df_g = 9;
perm_seed = 62;
routing_function = vlb;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.48883,   0.49176,    489.87,    335.35,         1,         0.0
simulation,         2,      0.25,   0.25011,   0.25012,    16.221,    16.222,         0,       6.054
simulation,         3,      0.37,   0.37019,   0.37022,    19.502,    19.502,         0,      6.0546
simulation,         4,      0.43,   0.42995,      0.43,    24.159,    24.159,         0,      6.0542
simulation,         5,      0.46,   0.46002,   0.46005,    30.576,    30.586,         0,      6.0537
simulation,         6,      0.48,   0.47867,   0.47909,    126.33,    83.887,         0,      6.0525
simulation,         7,      0.49,   0.48443,   0.48642,    334.13,    207.02,         0,      6.0524
simulation,         8,       0.1,   0.09996,  0.099966,    14.654,    14.655,         0,      6.0523
simulation,         9,       0.2,   0.19992,   0.19992,    15.548,    15.549,         0,      6.0538
simulation,        10,       0.3,   0.30027,   0.30028,    17.176,    17.177,         0,      6.0534
simulation,        11,       0.4,   0.40004,   0.40009,    21.237,    21.237,         0,      6.0539
