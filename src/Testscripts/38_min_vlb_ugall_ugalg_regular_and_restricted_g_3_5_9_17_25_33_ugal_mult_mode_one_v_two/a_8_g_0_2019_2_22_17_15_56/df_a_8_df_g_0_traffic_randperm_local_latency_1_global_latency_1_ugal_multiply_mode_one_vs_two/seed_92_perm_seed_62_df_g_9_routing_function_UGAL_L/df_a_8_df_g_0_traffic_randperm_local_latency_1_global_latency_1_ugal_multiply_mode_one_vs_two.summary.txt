vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = one_vs_two;
df_g = 9;
perm_seed = 62;
routing_function = UGAL_L;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50009,   0.50011,    12.046,    12.047,         0,      4.0066
simulation,         2,      0.74,   0.73631,   0.73636,    247.19,    52.944,         0,      4.1555
simulation,         3,      0.86,   0.75812,   0.75881,    1075.9,     404.0,         1,         0.0
simulation,         4,       0.8,     0.767,    0.7676,    460.62,    181.38,         1,         0.0
simulation,         5,      0.77,     0.757,     0.757,    416.49,    95.902,         1,         0.0
simulation,         6,      0.75,   0.74345,   0.74353,    397.51,    72.825,         0,      4.1689
simulation,         7,      0.76,   0.75054,   0.75052,    328.44,    78.893,         1,         0.0
simulation,         8,       0.1,  0.099965,  0.099966,    10.589,     10.59,         0,      4.1826
simulation,         9,       0.2,   0.19991,   0.19992,    10.806,    10.807,         0,      4.1493
simulation,        10,       0.3,   0.30028,   0.30028,    11.038,    11.039,         0,      4.0895
simulation,        11,       0.4,   0.40008,   0.40009,     11.41,    11.411,         0,      4.0423
simulation,        12,       0.5,   0.50009,   0.50011,    12.046,    12.047,         0,      4.0066
simulation,        13,       0.6,   0.59995,   0.59996,    14.307,    14.311,         0,      3.9964
simulation,        14,       0.7,   0.70017,    0.7002,    25.892,    25.927,         0,      4.0812
