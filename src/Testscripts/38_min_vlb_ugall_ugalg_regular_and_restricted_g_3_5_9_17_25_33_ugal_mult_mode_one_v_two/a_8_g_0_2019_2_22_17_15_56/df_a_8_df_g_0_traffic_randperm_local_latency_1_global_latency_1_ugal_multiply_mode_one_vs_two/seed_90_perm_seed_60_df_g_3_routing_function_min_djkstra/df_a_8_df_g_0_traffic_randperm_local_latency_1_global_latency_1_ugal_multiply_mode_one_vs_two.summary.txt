vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = one_vs_two;
df_g = 3;
perm_seed = 60;
routing_function = min_djkstra;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49084,    0.4909,    380.49,    59.486,         1,         0.0
simulation,         2,      0.25,   0.25054,   0.25054,    6.9424,    6.9425,         0,      2.3844
simulation,         3,      0.37,    0.3681,    0.3681,     237.2,    47.888,         0,      2.3852
simulation,         4,      0.43,   0.42506,   0.42506,    273.43,    64.716,         1,         0.0
simulation,         5,       0.4,   0.39692,   0.39694,    361.11,    46.808,         0,      2.3848
simulation,         6,      0.41,   0.40631,   0.40652,    414.96,    64.974,         0,      2.3843
simulation,         7,      0.42,    0.4157,   0.41578,    256.91,    65.304,         1,         0.0
simulation,         8,       0.1,   0.10056,   0.10056,    6.8081,    6.8084,         0,      2.3836
simulation,         9,       0.2,   0.20055,   0.20056,    6.8766,    6.8768,         0,      2.3842
simulation,        10,       0.3,   0.30015,   0.30017,    7.3393,    7.3391,         0,       2.385
simulation,        11,       0.4,   0.39692,   0.39694,    361.11,    46.808,         0,      2.3848
