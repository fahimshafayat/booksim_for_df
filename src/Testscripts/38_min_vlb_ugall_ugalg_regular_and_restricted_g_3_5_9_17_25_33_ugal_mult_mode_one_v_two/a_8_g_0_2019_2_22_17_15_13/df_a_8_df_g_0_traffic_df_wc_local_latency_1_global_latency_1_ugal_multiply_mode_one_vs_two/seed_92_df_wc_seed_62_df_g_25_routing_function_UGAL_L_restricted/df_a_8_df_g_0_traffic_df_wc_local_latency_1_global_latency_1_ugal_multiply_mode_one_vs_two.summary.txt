vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = one_vs_two;
df_g = 25;
df_wc_seed = 62;
routing_function = UGAL_L_restricted;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.32554,   0.39622,    490.31,    462.06,         1,         0.0
simulation,         2,      0.25,   0.24986,   0.24986,    30.459,    30.551,         0,      5.3923
simulation,         3,      0.37,   0.34332,   0.34748,    506.68,    331.53,         1,         0.0
simulation,         4,      0.31,   0.30981,   0.30981,    31.667,    31.781,         0,      5.4423
simulation,         5,      0.34,   0.33947,   0.33947,    60.475,    42.639,         0,      5.4603
simulation,         6,      0.35,   0.34362,   0.34374,     362.2,     127.8,         1,         0.0
simulation,         7,       0.1,  0.099979,  0.099978,    30.172,    30.224,         0,      5.0269
simulation,         8,       0.2,   0.19988,   0.19988,     30.78,    30.859,         0,      5.3281
simulation,         9,       0.3,   0.29978,   0.29978,     31.19,    31.297,         0,      5.4359
