vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = one_vs_two;
df_g = 5;
df_wc_seed = 62;
routing_function = UGAL_L_restricted;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49986,   0.49982,     73.95,    74.159,         0,      4.2739
simulation,         2,      0.74,   0.57099,    0.6746,     539.0,    506.37,         1,         0.0
simulation,         3,      0.62,   0.56654,   0.58284,    910.61,    814.72,         1,         0.0
simulation,         4,      0.56,   0.55949,   0.55948,    89.945,    90.305,         0,      4.3153
simulation,         5,      0.59,    0.5727,   0.58387,    549.53,    489.62,         1,         0.0
simulation,         6,      0.57,   0.56928,   0.56928,    95.164,    95.594,         0,      4.3207
simulation,         7,      0.58,   0.57932,   0.57924,     104.6,    105.15,         0,      4.3219
simulation,         8,       0.1,   0.09993,  0.099931,    10.416,    10.416,         0,      4.0714
simulation,         9,       0.2,   0.19975,   0.19973,    10.903,    10.903,         0,      4.0923
simulation,        10,       0.3,   0.29977,   0.29977,    12.065,    12.065,         0,      4.1027
simulation,        11,       0.4,   0.39986,   0.39985,    55.953,    56.023,         0,      4.1783
simulation,        12,       0.5,   0.49986,   0.49982,     73.95,    74.159,         0,      4.2739
