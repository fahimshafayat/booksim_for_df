vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = one_vs_two;
df_g = 5;
df_wc_seed = 60;
routing_function = UGAL_G_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50007,   0.50007,    14.543,    14.543,         0,      4.1865
simulation,         2,      0.74,    0.5596,   0.60881,    941.52,    633.61,         1,         0.0
simulation,         3,      0.62,   0.62026,   0.62025,     26.79,    26.792,         0,      4.2701
simulation,         4,      0.68,   0.55484,   0.56614,    1507.5,    847.67,         1,         0.0
simulation,         5,      0.65,    0.6177,    0.6273,    538.81,    469.45,         1,         0.0
simulation,         6,      0.63,   0.62436,   0.62628,    314.67,    233.48,         0,       4.266
simulation,         7,      0.64,   0.62288,   0.62319,    497.17,    366.78,         1,         0.0
simulation,         8,       0.1,   0.10047,   0.10047,    10.545,    10.545,         0,      4.1337
simulation,         9,       0.2,   0.20032,   0.20032,    10.983,    10.983,         0,      4.1436
simulation,        10,       0.3,   0.30002,   0.30002,    11.627,    11.628,         0,      4.1477
simulation,        11,       0.4,   0.40013,   0.40013,    12.603,    12.603,         0,      4.1561
simulation,        12,       0.5,   0.50007,   0.50007,    14.543,    14.543,         0,      4.1865
simulation,        13,       0.6,   0.60027,   0.60026,     20.55,    20.551,         0,      4.2559
