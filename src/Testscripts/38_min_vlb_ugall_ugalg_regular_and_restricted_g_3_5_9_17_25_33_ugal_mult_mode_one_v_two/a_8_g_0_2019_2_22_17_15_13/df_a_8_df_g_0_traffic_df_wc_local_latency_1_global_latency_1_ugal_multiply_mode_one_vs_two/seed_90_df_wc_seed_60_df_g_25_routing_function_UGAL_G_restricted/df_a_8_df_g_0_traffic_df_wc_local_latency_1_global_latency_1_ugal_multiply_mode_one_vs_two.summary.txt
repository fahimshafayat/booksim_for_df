vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = one_vs_two;
df_g = 25;
df_wc_seed = 60;
routing_function = UGAL_G_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.3981,   0.42917,    715.94,    529.67,         1,         0.0
simulation,         2,      0.25,   0.25003,   0.25004,    15.133,    15.133,         0,      5.2364
simulation,         3,      0.37,    0.3699,   0.36992,    19.327,    19.327,         0,      5.3526
simulation,         4,      0.43,   0.40276,   0.40314,    509.04,    328.54,         1,         0.0
simulation,         5,       0.4,   0.39988,   0.39987,    25.753,    25.756,         0,      5.3764
simulation,         6,      0.41,     0.407,   0.40726,    357.46,    100.58,         0,      5.3694
simulation,         7,      0.42,   0.40559,   0.40546,    499.35,     222.4,         1,         0.0
simulation,         8,       0.1,  0.099899,  0.099902,    13.027,    13.027,         0,      5.0406
simulation,         9,       0.2,   0.19997,   0.19997,     14.27,     14.27,         0,      5.1684
simulation,        10,       0.3,   0.29999,       0.3,    16.281,    16.282,         0,      5.2944
simulation,        11,       0.4,   0.39988,   0.39987,    25.753,    25.756,         0,      5.3764
