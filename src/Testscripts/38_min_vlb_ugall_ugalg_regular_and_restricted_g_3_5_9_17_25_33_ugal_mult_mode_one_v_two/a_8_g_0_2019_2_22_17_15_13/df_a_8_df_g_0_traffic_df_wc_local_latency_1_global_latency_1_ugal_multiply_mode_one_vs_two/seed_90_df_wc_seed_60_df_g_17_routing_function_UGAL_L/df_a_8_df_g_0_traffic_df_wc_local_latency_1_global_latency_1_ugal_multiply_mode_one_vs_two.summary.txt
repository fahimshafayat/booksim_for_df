vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = one_vs_two;
df_g = 17;
df_wc_seed = 60;
routing_function = UGAL_L;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.39589,   0.46693,     433.7,    425.98,         1,         0.0
simulation,         2,      0.25,   0.25005,   0.25006,    43.422,    43.561,         0,      5.8236
simulation,         3,      0.37,    0.3621,   0.36843,    533.56,    510.05,         1,         0.0
simulation,         4,      0.31,   0.30993,   0.30995,    47.077,    47.283,         0,        5.95
simulation,         5,      0.34,   0.33991,   0.33995,    53.203,    53.517,         0,      5.9957
simulation,         6,      0.35,   0.34989,   0.34992,    59.882,    60.394,         0,      6.0097
simulation,         7,      0.36,   0.35905,   0.35983,    132.99,    135.85,         0,      6.0206
simulation,         8,       0.1,  0.099914,  0.099915,    13.933,    13.933,         0,      5.0156
simulation,         9,       0.2,   0.19997,   0.19998,    42.605,    42.704,         0,      5.6598
simulation,        10,       0.3,   0.29995,   0.29997,    46.074,    46.269,         0,      5.9329
