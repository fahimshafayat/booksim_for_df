vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = one_vs_two;
df_g = 25;
df_wc_seed = 62;
routing_function = UGAL_G_restricted;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.3918,   0.45519,    401.04,    389.37,         1,         0.0
simulation,         2,      0.25,   0.24987,   0.24986,    15.369,     15.37,         0,      5.3301
simulation,         3,      0.37,   0.36975,   0.36977,    20.334,    20.335,         0,      5.4269
simulation,         4,      0.43,    0.3886,   0.38762,    773.63,    454.53,         1,         0.0
simulation,         5,       0.4,   0.39062,   0.39065,    489.96,    181.33,         1,         0.0
simulation,         6,      0.38,   0.37977,   0.37976,    22.268,     22.27,         0,      5.4341
simulation,         7,      0.39,   0.38984,   0.38977,    36.199,    36.209,         0,      5.4427
simulation,         8,       0.1,  0.099978,  0.099978,    13.306,    13.306,         0,      5.1307
simulation,         9,       0.2,   0.19988,   0.19988,    14.511,    14.512,         0,      5.2655
simulation,        10,       0.3,   0.29978,   0.29978,    16.517,    16.518,         0,      5.3778
