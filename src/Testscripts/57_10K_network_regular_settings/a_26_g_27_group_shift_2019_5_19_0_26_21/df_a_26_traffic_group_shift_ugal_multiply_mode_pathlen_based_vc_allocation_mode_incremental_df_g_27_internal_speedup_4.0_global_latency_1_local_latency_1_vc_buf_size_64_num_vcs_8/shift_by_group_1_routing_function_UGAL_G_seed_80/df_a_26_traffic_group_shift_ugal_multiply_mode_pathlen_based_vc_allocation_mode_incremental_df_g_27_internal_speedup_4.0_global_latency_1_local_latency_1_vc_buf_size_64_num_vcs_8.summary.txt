alloc_iters = 1;
output_speedup = 1;
input_speedup = 1;
routing_delay = 0;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
vc_alloc_delay = 1;
st_final_delay = 1;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
sim_count = 1;
injection_rate_uses_flits = 1;
warmup_periods = 3;
credit_delay = 2;
priority = none;
sample_period = 10000;
packet_size = 1;
df_a = 26;
df_arrangement = absolute_improved;
df_g = 27;
global_latency = 1;
internal_speedup = 4.0;
local_latency = 1;
num_vcs = 8;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
vc_buf_size = 64;
routing_function = UGAL_G;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.2481,   0.29992,    1775.1,    1242.4,         1,         0.0
simulation,         2,      0.25,      0.25,      0.25,    18.636,    18.636,         0,       6.371
simulation,         3,      0.37,      0.37,      0.37,    23.261,    23.261,         0,      6.5187
simulation,         4,      0.43,   0.42999,   0.42999,    28.585,    28.585,         0,      6.5625
simulation,         5,      0.46,   0.45999,   0.45999,    34.172,    34.173,         0,      6.5805
simulation,         6,      0.48,   0.48001,   0.48001,    41.185,    41.185,         0,      6.5917
simulation,         7,      0.49,      0.49,   0.49001,    47.113,    47.112,         0,      6.5969
