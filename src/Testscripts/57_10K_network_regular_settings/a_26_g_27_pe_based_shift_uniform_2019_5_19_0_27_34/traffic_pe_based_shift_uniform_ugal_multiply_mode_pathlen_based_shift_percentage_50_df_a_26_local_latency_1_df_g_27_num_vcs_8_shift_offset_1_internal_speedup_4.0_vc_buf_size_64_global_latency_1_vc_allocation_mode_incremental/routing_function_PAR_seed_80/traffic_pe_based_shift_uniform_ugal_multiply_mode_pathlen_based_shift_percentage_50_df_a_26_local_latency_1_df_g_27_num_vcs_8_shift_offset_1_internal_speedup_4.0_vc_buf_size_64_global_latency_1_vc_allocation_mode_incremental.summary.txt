packet_size = 1;
output_speedup = 1;
priority = none;
injection_rate_uses_flits = 1;
input_speedup = 1;
vc_alloc_delay = 1;
vc_allocator = separable_input_first;
sim_count = 1;
st_final_delay = 1;
credit_delay = 2;
routing_delay = 0;
sw_alloc_delay = 1;
alloc_iters = 1;
sw_allocator = separable_input_first;
sample_period = 10000;
warmup_periods = 3;
wait_for_tail_credit = 0;
df_a = 26;
df_arrangement = absolute_improved;
df_g = 27;
global_latency = 1;
internal_speedup = 4.0;
local_latency = 1;
num_vcs = 8;
shift_offset = 1;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
vc_buf_size = 64;
routing_function = PAR;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4689,   0.46956,    1090.9,    354.93,         1,         0.0
simulation,         2,      0.25,      0.25,      0.25,    15.974,    15.974,         0,      5.6146
simulation,         3,      0.37,      0.37,      0.37,    21.618,    21.625,         0,      5.7092
simulation,         4,      0.43,   0.42999,   0.42999,    27.582,      27.6,         0,      5.7375
simulation,         5,      0.46,   0.45897,     0.459,    202.84,    51.183,         0,      5.7564
simulation,         6,      0.48,   0.46756,   0.46779,    451.16,    189.27,         1,         0.0
simulation,         7,      0.47,    0.4645,   0.46465,    491.11,    115.26,         1,         0.0
