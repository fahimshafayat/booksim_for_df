wait_for_tail_credit = 0;
credit_delay = 2;
vc_alloc_delay = 1;
sw_allocator = separable_input_first;
alloc_iters = 1;
output_speedup = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
packet_size = 1;
sample_period = 10000;
sw_alloc_delay = 1;
routing_delay = 0;
sim_count = 1;
warmup_periods = 3;
vc_allocator = separable_input_first;
priority = none;
df_a = 26;
df_arrangement = absolute_improved;
df_g = 27;
global_latency = 1;
internal_speedup = 4.0;
local_latency = 1;
num_vcs = 8;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
vc_buf_size = 64;
routing_function = UGAL_L_restricted_src_only;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,      0.05,   0.05001,   0.05001,    13.213,    13.214,         0,      4.3175
simulation,         2,      0.08,  0.080018,  0.080017,    45.706,    45.746,         0,      4.5605
simulation,         3,       0.1,   0.10001,   0.10001,    45.407,    45.463,         0,      4.6659
simulation,         4,      0.12,   0.12001,   0.12001,     47.36,    47.441,         0,       4.733
simulation,         5,      0.13,   0.13001,   0.13001,    49.285,    49.388,         0,      4.7588
