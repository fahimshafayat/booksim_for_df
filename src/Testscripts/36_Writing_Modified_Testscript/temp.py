#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 18 21:03:04 2019

@author: rahman
"""

def extract_value_pairs(text):
    '''
    Sample input: |0-> 0 , |1-> 0 , |2-> 1 , |3-> 4 , |4-> 33 , |5-> 60 , |6-> 26 , |7-> 0 , |8-> 0 , |9-> 0 , 
    Sample output: [ (0, 0) , (1,0) , (2, 1), (3, 4) , (4 , 33) , (5, 60) , (6, 26) , (7, 0) , (8, 0) , (9, 0) ]
    '''
    results = []
    
    print(text)
    
    text = text.strip()
    
    if text == "":
        return results
    
    
    splitted = text.split("|")
    print(splitted)
    
    #drop the first entry. It was created because there is an empty | at the very beginning.
    
    splitted = splitted[1:]
    print(splitted)

    for element in splitted:
        temp = element.split(":")
        val = int(temp[0])
        freq = int(temp[1])
        results.append((val, freq))
    
    print(results)
    return results
    
    pass

if __name__ == "__main__":
    #text = "|0-> 0 , |1-> 0 , |2-> 1 , |3-> 4 , |4-> 33 , |5-> 60 , |6-> 26 , |7-> 0 , |8-> 0 , |9-> 0 , "
    text = " | 0:0| 1:0| 2:563| 3:1384| 4:0| 5:0| 6:0| 7:0| 8:0| 9:0"
    extract_value_pairs(text)

