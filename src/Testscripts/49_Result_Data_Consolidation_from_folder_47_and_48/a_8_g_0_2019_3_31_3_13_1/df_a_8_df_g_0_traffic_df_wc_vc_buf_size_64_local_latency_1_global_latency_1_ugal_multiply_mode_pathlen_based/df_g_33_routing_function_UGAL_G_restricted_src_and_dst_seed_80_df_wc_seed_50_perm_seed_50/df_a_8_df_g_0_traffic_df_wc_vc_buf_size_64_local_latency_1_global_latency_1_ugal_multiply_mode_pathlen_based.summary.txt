wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.48149,   0.48147,    576.73,    198.83,         1,         0.0
simulation,         2,      0.25,      0.25,   0.24999,    15.648,    15.649,         0,      5.5133
simulation,         3,      0.37,   0.37008,   0.37008,    18.513,    18.513,         0,      5.6008
simulation,         4,      0.43,      0.43,   0.42999,    22.031,    22.031,         0,       5.629
simulation,         5,      0.46,   0.45995,   0.45995,    26.459,     26.46,         0,      5.6427
simulation,         6,      0.48,   0.47995,   0.47997,    34.405,    34.408,         0,       5.653
simulation,         7,      0.49,   0.48809,   0.48808,     314.3,    76.814,         0,      5.6552
simulation,         8,       0.1,  0.099977,  0.099975,    13.675,    13.675,         0,      5.2948
simulation,         9,       0.2,   0.20003,   0.20003,    14.874,    14.874,         0,       5.449
simulation,        10,       0.3,   0.29997,   0.29997,    16.583,    16.583,         0,      5.5576
simulation,        11,       0.4,   0.40001,   0.40001,    19.866,    19.867,         0,      5.6154
