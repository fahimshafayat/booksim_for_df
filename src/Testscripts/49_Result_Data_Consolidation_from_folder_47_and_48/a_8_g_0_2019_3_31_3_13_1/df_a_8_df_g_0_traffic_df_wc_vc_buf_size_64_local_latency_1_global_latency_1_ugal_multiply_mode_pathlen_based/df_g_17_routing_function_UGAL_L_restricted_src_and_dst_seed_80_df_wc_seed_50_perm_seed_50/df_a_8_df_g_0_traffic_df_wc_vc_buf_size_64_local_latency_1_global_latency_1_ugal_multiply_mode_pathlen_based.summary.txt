wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.44055,   0.46729,    473.16,    390.45,         1,         0.0
simulation,         2,      0.25,   0.25024,   0.25024,    36.406,    36.451,         0,      5.1972
simulation,         3,      0.37,    0.3645,   0.36466,    504.25,    231.69,         1,         0.0
simulation,         4,      0.31,   0.30949,    0.3095,    194.98,    75.288,         0,      5.2848
simulation,         5,      0.34,   0.33836,   0.33859,    410.18,    128.53,         0,      5.3163
simulation,         6,      0.35,   0.34728,   0.34743,     471.3,    182.52,         1,         0.0
simulation,         7,       0.1,   0.10017,   0.10017,    12.545,    12.545,         0,      4.7261
simulation,         8,       0.2,   0.20019,    0.2002,    36.689,    36.724,         0,      5.0852
simulation,         9,       0.3,   0.29975,   0.29978,    131.35,    75.531,         0,      5.2725
