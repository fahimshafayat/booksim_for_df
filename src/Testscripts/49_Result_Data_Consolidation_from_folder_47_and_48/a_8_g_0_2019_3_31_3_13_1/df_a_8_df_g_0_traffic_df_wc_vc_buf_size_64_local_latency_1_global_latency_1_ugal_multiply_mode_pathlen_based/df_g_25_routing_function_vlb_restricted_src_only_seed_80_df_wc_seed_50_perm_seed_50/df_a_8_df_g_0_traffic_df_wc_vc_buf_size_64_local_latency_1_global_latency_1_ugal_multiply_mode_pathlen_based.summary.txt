wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 50;
perm_seed = 50;
routing_function = vlb_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.24457,   0.29663,    1858.0,    1408.9,         1,         0.0
simulation,         2,      0.25,   0.24072,   0.24071,    399.29,    186.98,         1,         0.0
simulation,         3,      0.13,   0.13015,   0.13015,     14.01,     14.01,         0,      5.6386
simulation,         4,      0.19,   0.19014,   0.19014,    14.633,    14.633,         0,      5.6381
simulation,         5,      0.22,    0.2202,    0.2202,    15.128,    15.128,         0,       5.638
simulation,         6,      0.23,    0.2302,    0.2302,    15.425,    15.425,         0,      5.6384
simulation,         7,      0.24,   0.23243,   0.23289,    488.52,    164.62,         1,         0.0
simulation,         8,      0.05,  0.050014,  0.050014,    13.499,    13.499,         0,       5.639
simulation,         9,       0.1,    0.1001,    0.1001,     13.79,     13.79,         0,       5.639
simulation,        10,      0.15,   0.15015,   0.15015,    14.186,    14.186,         0,       5.639
simulation,        11,       0.2,   0.20016,   0.20016,    14.776,    14.776,         0,      5.6383
