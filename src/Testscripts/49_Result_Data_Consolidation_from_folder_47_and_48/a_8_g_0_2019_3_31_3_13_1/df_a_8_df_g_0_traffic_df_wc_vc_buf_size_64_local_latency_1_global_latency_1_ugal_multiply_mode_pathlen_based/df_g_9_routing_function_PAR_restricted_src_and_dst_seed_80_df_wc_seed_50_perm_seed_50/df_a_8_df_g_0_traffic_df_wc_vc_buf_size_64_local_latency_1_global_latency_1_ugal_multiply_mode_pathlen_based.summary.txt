wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 50;
perm_seed = 50;
routing_function = PAR_restricted_src_and_dst;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50013,   0.50015,    23.409,    23.411,         0,      5.1983
simulation,         2,      0.74,   0.54302,   0.59329,    1146.2,    690.78,         1,         0.0
simulation,         3,      0.62,   0.55084,   0.58243,    472.92,    373.52,         1,         0.0
simulation,         4,      0.56,   0.55357,   0.55459,    527.96,     267.4,         1,         0.0
simulation,         5,      0.53,   0.53006,   0.53011,    34.798,    34.836,         0,      5.2496
simulation,         6,      0.54,   0.53994,   0.54002,    47.243,    47.323,         0,      5.2689
simulation,         7,      0.55,   0.54988,   0.55001,    104.64,    105.04,         0,      5.2641
simulation,         8,       0.1,   0.10003,   0.10003,    12.164,    12.164,         0,       4.871
simulation,         9,       0.2,   0.19998,   0.19998,    12.729,    12.729,         0,      4.8461
simulation,        10,       0.3,   0.30012,   0.30011,    13.862,    13.862,         0,      4.8765
simulation,        11,       0.4,   0.40003,   0.40002,    16.582,    16.582,         0,      5.0221
simulation,        12,       0.5,   0.50013,   0.50015,    23.409,    23.411,         0,      5.1983
