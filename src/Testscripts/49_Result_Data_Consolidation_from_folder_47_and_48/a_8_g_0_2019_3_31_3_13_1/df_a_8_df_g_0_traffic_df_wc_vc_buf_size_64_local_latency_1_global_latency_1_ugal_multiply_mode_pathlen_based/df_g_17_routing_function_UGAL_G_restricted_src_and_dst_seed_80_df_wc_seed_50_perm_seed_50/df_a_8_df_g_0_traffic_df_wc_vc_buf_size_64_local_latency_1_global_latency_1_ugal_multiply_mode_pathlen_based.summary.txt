wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50023,   0.50025,    26.977,    26.978,         0,      5.3802
simulation,         2,      0.74,   0.47739,   0.52197,    1482.6,    757.78,         1,         0.0
simulation,         3,      0.62,   0.51215,   0.54258,    656.65,    462.54,         1,         0.0
simulation,         4,      0.56,    0.5191,   0.52767,    545.33,    297.33,         1,         0.0
simulation,         5,      0.53,    0.5261,   0.52609,    489.79,    140.86,         0,      5.3916
simulation,         6,      0.54,     0.523,   0.52266,     629.8,    218.58,         1,         0.0
simulation,         7,       0.1,   0.10017,   0.10017,    12.661,    12.661,         0,      4.9441
simulation,         8,       0.2,    0.2002,    0.2002,    13.923,    13.923,         0,      5.0638
simulation,         9,       0.3,   0.30023,   0.30023,     15.74,     15.74,         0,      5.2082
simulation,        10,       0.4,   0.40026,   0.40026,    18.525,    18.525,         0,       5.312
simulation,        11,       0.5,   0.50023,   0.50025,    26.977,    26.978,         0,      5.3802
