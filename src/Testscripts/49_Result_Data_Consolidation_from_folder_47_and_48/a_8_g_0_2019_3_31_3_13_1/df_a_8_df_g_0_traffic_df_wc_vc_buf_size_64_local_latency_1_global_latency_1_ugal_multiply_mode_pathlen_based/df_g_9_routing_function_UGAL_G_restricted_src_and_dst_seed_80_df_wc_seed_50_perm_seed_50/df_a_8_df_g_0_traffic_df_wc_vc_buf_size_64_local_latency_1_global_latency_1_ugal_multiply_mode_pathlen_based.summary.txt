wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50015,    20.079,    20.079,         0,      4.7643
simulation,         2,      0.74,   0.53749,   0.58154,    1145.1,    656.19,         1,         0.0
simulation,         3,      0.62,    0.5536,   0.56586,    821.61,    389.21,         1,         0.0
simulation,         4,      0.56,   0.55988,      0.56,    37.268,    37.266,         0,      4.8174
simulation,         5,      0.59,   0.55738,    0.5577,    1074.5,    294.86,         1,         0.0
simulation,         6,      0.57,   0.55909,   0.55934,    529.44,    202.68,         1,         0.0
simulation,         7,       0.1,   0.10003,   0.10003,    11.402,    11.402,         0,      4.4786
simulation,         8,       0.2,   0.19998,   0.19998,    12.252,    12.252,         0,      4.5237
simulation,         9,       0.3,   0.30012,   0.30011,    13.437,    13.438,         0,      4.5676
simulation,        10,       0.4,   0.40004,   0.40002,    15.695,    15.695,         0,      4.6705
simulation,        11,       0.5,   0.50014,   0.50015,    20.079,    20.079,         0,      4.7643
