wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
five_hop_percentage = 50;
routing_function = UGAL_L_four_hop_some_five_hop_restricted;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46994,   0.48055,    520.35,    336.58,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24998,    39.457,    39.479,         0,      4.4493
simulation,         3,      0.37,   0.36981,   0.36981,    124.11,    66.285,         0,      4.6117
simulation,         4,      0.43,   0.42515,   0.42527,    472.44,    202.47,         1,         0.0
simulation,         5,       0.4,   0.39833,   0.39853,    420.08,    181.11,         0,      4.6384
simulation,         6,      0.41,   0.40742,   0.40747,    493.27,    188.19,         1,         0.0
simulation,         7,       0.1,   0.10003,   0.10003,    11.061,    11.061,         0,      4.3076
simulation,         8,       0.2,   0.19998,   0.19998,    12.509,    12.509,         0,      4.3723
simulation,         9,       0.3,   0.30012,   0.30011,    42.047,    42.084,         0,      4.5292
simulation,        10,       0.4,   0.39833,   0.39853,    420.08,    181.11,         0,      4.6384
