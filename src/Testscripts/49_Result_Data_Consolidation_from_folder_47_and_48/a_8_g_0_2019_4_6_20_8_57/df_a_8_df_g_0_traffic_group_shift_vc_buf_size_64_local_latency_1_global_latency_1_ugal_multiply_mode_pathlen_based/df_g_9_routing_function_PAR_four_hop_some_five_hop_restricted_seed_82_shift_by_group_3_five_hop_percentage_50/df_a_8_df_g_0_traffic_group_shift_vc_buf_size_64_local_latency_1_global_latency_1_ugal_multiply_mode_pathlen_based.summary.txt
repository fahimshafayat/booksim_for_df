wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
five_hop_percentage = 50;
routing_function = PAR_four_hop_some_five_hop_restricted;
seed = 82;
shift_by_group = 3;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49989,   0.49989,     26.85,    26.863,         0,      5.1418
simulation,         2,      0.74,    0.5415,   0.58736,    1136.8,    681.48,         1,         0.0
simulation,         3,      0.62,   0.54767,   0.57991,    492.58,    400.57,         1,         0.0
simulation,         4,      0.56,   0.54778,   0.54826,    506.46,    255.79,         1,         0.0
simulation,         5,      0.53,   0.52996,   0.52989,    51.904,    51.997,         0,      5.2072
simulation,         6,      0.54,   0.53882,   0.53882,    215.97,    98.666,         0,      5.2178
simulation,         7,      0.55,   0.54398,   0.54438,    504.78,    179.57,         1,         0.0
simulation,         8,       0.1,  0.099988,  0.099984,     12.08,     12.08,         0,      4.8344
simulation,         9,       0.2,    0.2001,   0.20009,    12.631,    12.631,         0,      4.8134
simulation,        10,       0.3,   0.30011,    0.3001,    13.664,    13.664,         0,      4.8338
simulation,        11,       0.4,   0.40005,   0.40004,    16.156,    16.156,         0,      4.9575
simulation,        12,       0.5,   0.49989,   0.49989,     26.85,    26.863,         0,      5.1418
