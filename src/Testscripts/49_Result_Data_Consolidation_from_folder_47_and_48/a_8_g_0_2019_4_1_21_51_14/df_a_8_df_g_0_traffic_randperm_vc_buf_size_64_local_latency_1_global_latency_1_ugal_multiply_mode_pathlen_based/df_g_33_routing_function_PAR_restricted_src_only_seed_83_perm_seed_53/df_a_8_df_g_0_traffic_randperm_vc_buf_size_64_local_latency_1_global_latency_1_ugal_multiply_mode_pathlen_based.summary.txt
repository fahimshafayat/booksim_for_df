wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
perm_seed = 53;
routing_function = PAR_restricted_src_only;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49998,   0.49998,    17.123,    17.126,         0,      4.6715
simulation,         2,      0.74,   0.56132,   0.60694,    898.67,    597.81,         1,         0.0
simulation,         3,      0.62,   0.57843,   0.58103,    1060.4,    445.23,         1,         0.0
simulation,         4,      0.56,   0.55996,   0.55996,    25.322,    25.335,         0,      4.8157
simulation,         5,      0.59,   0.58968,   0.58969,    81.522,    64.046,         0,      5.0608
simulation,         6,       0.6,   0.59029,    0.5905,     526.5,    234.79,         1,         0.0
simulation,         7,       0.1,  0.099998,  0.099999,    11.983,    11.984,         0,      4.8234
simulation,         8,       0.2,   0.19999,   0.19999,    12.303,    12.304,         0,      4.7607
simulation,         9,       0.3,   0.30001,   0.30001,    12.723,    12.724,         0,      4.6743
simulation,        10,       0.4,   0.39995,   0.39995,    13.625,    13.626,         0,      4.6294
simulation,        11,       0.5,   0.49998,   0.49998,    17.123,    17.126,         0,      4.6715
