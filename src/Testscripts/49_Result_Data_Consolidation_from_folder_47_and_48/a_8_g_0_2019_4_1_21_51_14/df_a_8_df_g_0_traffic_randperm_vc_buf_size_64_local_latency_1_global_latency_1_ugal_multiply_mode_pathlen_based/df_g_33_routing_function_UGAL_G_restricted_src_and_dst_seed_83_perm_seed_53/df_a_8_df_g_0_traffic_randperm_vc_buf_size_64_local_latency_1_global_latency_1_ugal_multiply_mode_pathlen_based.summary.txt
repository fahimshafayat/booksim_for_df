wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
perm_seed = 53;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49998,   0.49998,     13.78,    13.781,         0,      4.2276
simulation,         2,      0.74,   0.66657,   0.67939,     703.6,     332.5,         1,         0.0
simulation,         3,      0.62,   0.61999,      0.62,    21.832,    21.834,         0,      4.3651
simulation,         4,      0.68,   0.67454,   0.67457,    386.13,    122.86,         1,         0.0
simulation,         5,      0.65,   0.64996,   0.64997,    30.039,    30.043,         0,      4.4374
simulation,         6,      0.66,   0.65998,      0.66,    36.109,    36.114,         0,      4.4691
simulation,         7,      0.67,   0.66994,   0.66997,    47.959,    47.971,         0,       4.508
simulation,         8,      0.15,   0.14995,   0.14995,    10.947,    10.947,         0,      4.2804
simulation,         9,       0.3,   0.30001,   0.30001,    11.499,    11.499,         0,      4.2355
simulation,        10,      0.45,   0.45001,   0.45001,     12.79,    12.791,         0,      4.2136
simulation,        11,       0.6,   0.60001,   0.60002,    19.165,    19.165,         0,      4.3285
