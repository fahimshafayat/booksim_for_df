wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
perm_seed = 52;
routing_function = UGAL_G;
seed = 82;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4999,   0.49989,    11.055,    11.055,         0,       3.739
simulation,         2,      0.74,   0.73991,   0.73991,    15.074,    15.075,         0,      3.7636
simulation,         3,      0.86,   0.85992,   0.85995,    31.071,    31.075,         0,      3.8833
simulation,         4,      0.92,   0.87515,   0.87575,    829.13,    221.51,         1,         0.0
simulation,         5,      0.89,   0.88403,   0.88405,    338.17,    96.673,         1,         0.0
simulation,         6,      0.87,   0.86996,   0.86996,    37.124,    37.129,         0,      3.9023
simulation,         7,      0.88,      0.88,   0.88001,    48.576,    48.584,         0,      3.9249
simulation,         8,      0.15,   0.15005,   0.15005,    10.135,    10.135,         0,      3.9171
simulation,         9,       0.3,   0.30011,    0.3001,    10.317,    10.317,         0,      3.8115
simulation,        10,      0.45,   0.44993,   0.44993,    10.794,    10.794,         0,      3.7525
simulation,        11,       0.6,   0.59987,   0.59986,    11.916,    11.916,         0,      3.7297
simulation,        12,      0.75,    0.7499,    0.7499,    15.509,     15.51,         0,      3.7687
