wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
perm_seed = 53;
routing_function = PAR_four_hop_restricted;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50001,       0.5,     13.03,    13.031,         0,      4.1769
simulation,         2,      0.74,   0.63971,   0.67429,     532.6,     395.8,         1,         0.0
simulation,         3,      0.62,      0.62,   0.62001,     18.91,    18.915,         0,      4.2914
simulation,         4,      0.68,   0.63325,   0.64535,    523.75,    313.18,         1,         0.0
simulation,         5,      0.65,   0.62691,    0.6263,    582.71,    274.84,         1,         0.0
simulation,         6,      0.63,   0.62995,   0.62997,    24.033,    24.036,         0,      4.3569
simulation,         7,      0.64,   0.62368,   0.62366,    508.26,    230.13,         1,         0.0
simulation,         8,      0.15,   0.14989,   0.14989,     10.92,     10.92,         0,      4.2536
simulation,         9,       0.3,   0.29985,   0.29985,    11.455,    11.455,         0,      4.2076
simulation,        10,      0.45,   0.44987,   0.44987,    12.443,    12.443,         0,       4.177
simulation,        11,       0.6,   0.59999,   0.59999,    16.463,    16.464,         0,      4.2389
