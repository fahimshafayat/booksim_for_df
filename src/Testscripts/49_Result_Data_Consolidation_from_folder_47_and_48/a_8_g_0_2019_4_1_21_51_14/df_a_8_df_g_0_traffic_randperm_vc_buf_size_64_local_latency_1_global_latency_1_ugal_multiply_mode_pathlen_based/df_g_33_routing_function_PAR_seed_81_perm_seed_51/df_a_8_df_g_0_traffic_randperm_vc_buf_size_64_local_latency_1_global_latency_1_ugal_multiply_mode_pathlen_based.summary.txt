wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
perm_seed = 51;
routing_function = PAR;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49991,   0.49991,    24.906,    24.924,         0,      4.9794
simulation,         2,      0.74,   0.55204,   0.60059,    955.71,    671.36,         1,         0.0
simulation,         3,      0.62,   0.55609,   0.57148,    768.95,    455.36,         1,         0.0
simulation,         4,      0.56,   0.55698,   0.55698,    331.97,    124.36,         1,         0.0
simulation,         5,      0.53,   0.52993,   0.52991,    34.454,    34.484,         0,      5.1442
simulation,         6,      0.54,   0.53991,    0.5399,    42.965,    42.799,         0,      5.2298
simulation,         7,      0.55,   0.54972,    0.5497,    79.866,    60.457,         0,       5.349
simulation,         8,       0.1,   0.10003,   0.10003,    12.594,    12.595,         0,      5.1056
simulation,         9,       0.2,   0.20005,   0.20005,    12.754,    12.755,         0,      4.9481
simulation,        10,       0.3,   0.30009,   0.30009,    13.228,    13.228,         0,      4.8406
simulation,        11,       0.4,       0.4,       0.4,    14.933,    14.934,         0,      4.8111
simulation,        12,       0.5,   0.49991,   0.49991,    24.906,    24.924,         0,      4.9794
