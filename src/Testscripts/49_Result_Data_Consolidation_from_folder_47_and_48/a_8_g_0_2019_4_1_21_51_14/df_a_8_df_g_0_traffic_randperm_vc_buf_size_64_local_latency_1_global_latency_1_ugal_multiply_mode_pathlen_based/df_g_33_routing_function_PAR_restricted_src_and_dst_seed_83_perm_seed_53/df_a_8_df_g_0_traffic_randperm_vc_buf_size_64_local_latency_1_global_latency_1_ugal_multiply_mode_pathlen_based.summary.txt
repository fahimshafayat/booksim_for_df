wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
perm_seed = 53;
routing_function = PAR_restricted_src_and_dst;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49998,   0.49998,    17.843,    17.848,         0,      4.7555
simulation,         2,      0.74,   0.56949,   0.61555,    890.96,    606.16,         1,         0.0
simulation,         3,      0.62,   0.57404,   0.58854,    599.66,    392.05,         1,         0.0
simulation,         4,      0.56,   0.55996,   0.55996,    27.499,    27.509,         0,      4.9706
simulation,         5,      0.59,   0.57488,   0.57614,    560.98,    330.44,         1,         0.0
simulation,         6,      0.57,   0.56994,   0.56996,    32.154,    32.171,         0,      5.0648
simulation,         7,      0.58,   0.57427,   0.57453,    524.83,    243.79,         1,         0.0
simulation,         8,       0.1,  0.099998,  0.099999,    12.091,    12.091,         0,      4.8744
simulation,         9,       0.2,   0.19999,   0.19999,    12.436,    12.436,         0,      4.8165
simulation,        10,       0.3,   0.30001,   0.30001,    12.876,    12.876,         0,      4.7297
simulation,        11,       0.4,   0.39995,   0.39995,    13.862,    13.862,         0,      4.6897
simulation,        12,       0.5,   0.49998,   0.49998,    17.843,    17.848,         0,      4.7555
