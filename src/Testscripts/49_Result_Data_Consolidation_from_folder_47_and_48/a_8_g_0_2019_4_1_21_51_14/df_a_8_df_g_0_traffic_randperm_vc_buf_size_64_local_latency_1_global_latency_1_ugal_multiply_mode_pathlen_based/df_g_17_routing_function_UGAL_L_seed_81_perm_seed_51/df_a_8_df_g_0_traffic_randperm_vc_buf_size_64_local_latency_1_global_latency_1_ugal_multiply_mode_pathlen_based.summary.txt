wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
perm_seed = 51;
routing_function = UGAL_L;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49997,   0.49997,    16.947,    16.952,         0,      4.2943
simulation,         2,      0.74,   0.63479,   0.66685,    523.67,    373.85,         1,         0.0
simulation,         3,      0.62,   0.61552,   0.61557,    359.91,     95.22,         1,         0.0
simulation,         4,      0.56,   0.56001,   0.56002,    26.243,    26.261,         0,      4.3576
simulation,         5,      0.59,      0.59,   0.59004,     44.85,    38.495,         0,      4.4307
simulation,         6,       0.6,    0.5991,    0.5991,    167.89,     47.53,         0,      4.4642
simulation,         7,      0.61,   0.60807,   0.60807,    334.72,    59.482,         0,      4.5044
simulation,         8,      0.15,   0.15003,   0.15003,    11.259,     11.26,         0,      4.4194
simulation,         9,       0.3,   0.30005,   0.30005,    11.722,    11.722,         0,      4.3309
simulation,        10,      0.45,   0.45002,   0.45002,    13.881,    13.883,         0,      4.2826
simulation,        11,       0.6,    0.5991,    0.5991,    167.89,     47.53,         0,      4.4642
