wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
five_hop_percentage = 60;
perm_seed = 50;
routing_function = UGAL_G_four_hop_some_five_hop_restricted;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50015,    11.039,    11.039,         0,      3.7412
simulation,         2,      0.74,   0.74012,   0.74012,    13.447,    13.447,         0,      3.7077
simulation,         3,      0.86,   0.86003,   0.86005,    18.464,    18.464,         0,      3.7225
simulation,         4,      0.92,      0.92,   0.92002,    32.306,    32.309,         0,       3.751
simulation,         5,      0.95,   0.93162,   0.93136,    525.51,    147.83,         1,         0.0
simulation,         6,      0.93,   0.93001,   0.93004,    41.867,    41.875,         0,       3.758
simulation,         7,      0.94,    0.9336,   0.93359,    372.81,    109.54,         1,         0.0
simulation,         8,       0.2,   0.19998,   0.19998,    10.063,    10.063,         0,      3.8226
simulation,         9,       0.4,   0.40003,   0.40002,    10.597,    10.597,         0,      3.7637
simulation,        10,       0.6,   0.59993,   0.59993,    11.678,    11.678,         0,      3.7219
simulation,        11,       0.8,   0.79996,   0.79997,    15.069,    15.069,         0,      3.7101
