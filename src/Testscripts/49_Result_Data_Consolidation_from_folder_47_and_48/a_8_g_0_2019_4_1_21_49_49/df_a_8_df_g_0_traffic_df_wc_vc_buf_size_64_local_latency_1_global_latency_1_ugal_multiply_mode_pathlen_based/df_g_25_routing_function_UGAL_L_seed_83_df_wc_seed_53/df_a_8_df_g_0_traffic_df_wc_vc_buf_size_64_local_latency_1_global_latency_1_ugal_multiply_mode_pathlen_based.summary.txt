wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 53;
routing_function = UGAL_L;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.3369,   0.38026,    1387.2,    952.71,         1,         0.0
simulation,         2,      0.25,   0.24996,   0.24995,    39.175,    39.248,         0,      6.0137
simulation,         3,      0.37,   0.33073,   0.34562,    883.93,     616.0,         1,         0.0
simulation,         4,      0.31,   0.29881,   0.29947,    767.52,    564.72,         1,         0.0
simulation,         5,      0.28,   0.27796,   0.27941,    432.14,    405.22,         0,      6.0637
simulation,         6,      0.29,   0.28477,   0.28724,    489.84,    494.04,         1,         0.0
simulation,         7,      0.05,  0.049971,  0.049969,    12.801,    12.802,         0,      5.0143
simulation,         8,       0.1,  0.099949,  0.099941,    26.907,    26.931,         0,      5.3739
simulation,         9,      0.15,   0.14987,   0.14986,    37.179,    37.218,         0,      5.6953
simulation,        10,       0.2,    0.1999,   0.19989,    37.102,    37.158,         0,       5.893
simulation,        11,      0.25,   0.24996,   0.24995,    39.175,    39.248,         0,      6.0137
