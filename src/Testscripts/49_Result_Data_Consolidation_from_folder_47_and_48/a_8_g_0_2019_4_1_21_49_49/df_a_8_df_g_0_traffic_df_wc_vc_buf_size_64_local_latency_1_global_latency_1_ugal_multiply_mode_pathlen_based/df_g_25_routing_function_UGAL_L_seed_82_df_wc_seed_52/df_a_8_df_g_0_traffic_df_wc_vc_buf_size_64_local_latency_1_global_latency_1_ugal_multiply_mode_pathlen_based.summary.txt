wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 52;
routing_function = UGAL_L;
seed = 82;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.34387,    0.3867,    1292.6,    904.28,         1,         0.0
simulation,         2,      0.25,    0.2501,    0.2501,     37.48,    37.546,         0,      6.0416
simulation,         3,      0.37,   0.34127,   0.35232,    628.17,    443.71,         1,         0.0
simulation,         4,      0.31,   0.30246,   0.30284,    528.88,    386.72,         1,         0.0
simulation,         5,      0.28,   0.27858,   0.27954,    319.85,     291.1,         0,      6.0895
simulation,         6,      0.29,   0.28639,   0.28711,    536.64,     382.9,         1,         0.0
simulation,         7,      0.05,  0.050022,  0.050023,    12.874,    12.874,         0,      5.0429
simulation,         8,       0.1,   0.10002,   0.10002,    28.483,    28.505,         0,      5.4235
simulation,         9,      0.15,   0.14999,      0.15,    36.251,    36.288,         0,      5.7427
simulation,        10,       0.2,   0.20005,   0.20005,    35.926,    35.975,         0,      5.9283
simulation,        11,      0.25,    0.2501,    0.2501,     37.48,    37.546,         0,      6.0416
