wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 51;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.41172,   0.43614,    549.16,    409.36,         1,         0.0
simulation,         2,      0.25,   0.24669,   0.24668,     473.6,    139.97,         1,         0.0
simulation,         3,      0.13,   0.13005,   0.13005,    30.836,    30.866,         0,      5.3424
simulation,         4,      0.19,    0.1893,    0.1893,    330.74,    93.052,         0,      5.4877
simulation,         5,      0.22,   0.21826,    0.2183,    406.86,    122.91,         1,         0.0
simulation,         6,       0.2,   0.19903,   0.19906,    454.02,    103.51,         0,      5.5031
simulation,         7,      0.21,   0.20867,   0.20874,    332.12,    112.95,         1,         0.0
simulation,         8,      0.05,  0.050014,  0.050012,    12.506,    12.506,         0,       4.861
simulation,         9,       0.1,   0.10003,   0.10003,     32.74,    32.765,         0,      5.2041
simulation,        10,      0.15,   0.15003,   0.15003,    30.382,    30.415,         0,      5.4042
simulation,        11,       0.2,   0.19903,   0.19906,    454.02,    103.51,         0,      5.5031
