wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 51;
routing_function = UGAL_L_restricted_src_only;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.31059,   0.35445,    1074.0,    800.34,         1,         0.0
simulation,         2,      0.25,   0.25011,   0.25011,    28.921,    28.957,         0,       5.325
simulation,         3,      0.37,   0.35117,   0.35182,    818.78,    320.99,         1,         0.0
simulation,         4,      0.31,   0.31015,   0.31015,    29.876,     29.92,         0,      5.3801
simulation,         5,      0.34,   0.33964,   0.33963,    134.32,    43.342,         0,      5.4006
simulation,         6,      0.35,   0.34464,   0.34477,    524.86,    138.62,         1,         0.0
simulation,         7,       0.1,   0.10008,   0.10008,    27.128,    27.148,         0,      4.9743
simulation,         8,       0.2,   0.20015,   0.20016,    29.437,     29.47,         0,      5.2559
simulation,         9,       0.3,   0.30015,   0.30015,     29.51,    29.554,         0,      5.3725
