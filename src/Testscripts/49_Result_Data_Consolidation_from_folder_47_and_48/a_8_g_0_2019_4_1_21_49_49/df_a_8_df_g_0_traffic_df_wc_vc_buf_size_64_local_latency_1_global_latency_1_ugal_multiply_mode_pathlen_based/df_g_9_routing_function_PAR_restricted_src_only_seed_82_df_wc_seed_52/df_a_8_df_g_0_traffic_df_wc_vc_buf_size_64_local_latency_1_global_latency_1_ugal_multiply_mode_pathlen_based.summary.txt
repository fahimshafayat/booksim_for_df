wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 52;
routing_function = PAR_restricted_src_only;
seed = 82;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49989,   0.49989,    22.511,    22.512,         0,      5.0877
simulation,         2,      0.74,   0.54005,   0.58928,    1193.5,    711.41,         1,         0.0
simulation,         3,      0.62,   0.54533,   0.57696,    503.16,    393.29,         1,         0.0
simulation,         4,      0.56,    0.5454,   0.54676,    636.98,    293.92,         1,         0.0
simulation,         5,      0.53,   0.52987,   0.52989,    44.254,    44.345,         0,      5.1498
simulation,         6,      0.54,   0.53949,   0.53976,    122.31,    117.32,         0,      5.1471
simulation,         7,      0.55,   0.54123,   0.54105,     477.7,    265.13,         1,         0.0
simulation,         8,       0.1,  0.099987,  0.099984,    12.013,    12.013,         0,      4.8034
simulation,         9,       0.2,    0.2001,   0.20009,    12.561,    12.561,         0,       4.785
simulation,        10,       0.3,   0.30011,    0.3001,    13.573,    13.573,         0,      4.8071
simulation,        11,       0.4,   0.40005,   0.40004,    15.964,    15.964,         0,      4.9202
simulation,        12,       0.5,   0.49989,   0.49989,    22.511,    22.512,         0,      5.0877
