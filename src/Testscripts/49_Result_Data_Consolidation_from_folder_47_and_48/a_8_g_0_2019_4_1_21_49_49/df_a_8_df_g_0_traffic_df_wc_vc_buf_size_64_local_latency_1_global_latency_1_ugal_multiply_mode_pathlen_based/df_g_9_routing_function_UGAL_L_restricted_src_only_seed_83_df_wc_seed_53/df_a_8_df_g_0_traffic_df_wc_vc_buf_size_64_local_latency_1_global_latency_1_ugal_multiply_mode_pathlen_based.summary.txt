wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 53;
routing_function = UGAL_L_restricted_src_only;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.48468,   0.48559,    704.77,    315.69,         1,         0.0
simulation,         2,      0.25,   0.24999,   0.24999,    39.201,    39.222,         0,      4.4718
simulation,         3,      0.37,      0.37,   0.36999,    42.794,    42.842,         0,       4.667
simulation,         4,      0.43,   0.42999,      0.43,    46.013,    46.082,         0,      4.7273
simulation,         5,      0.46,   0.45997,   0.45997,    51.764,    51.865,         0,      4.7523
simulation,         6,      0.48,   0.47419,   0.47433,     528.2,    176.88,         1,         0.0
simulation,         7,      0.47,   0.46857,   0.46859,    238.16,    88.719,         0,      4.7593
simulation,         8,       0.1,  0.099856,  0.099855,    11.117,    11.117,         0,      4.3373
simulation,         9,       0.2,   0.19995,   0.19995,    12.476,    12.476,         0,      4.4061
simulation,        10,       0.3,   0.29998,   0.29998,    41.435,    41.471,         0,      4.5693
simulation,        11,       0.4,   0.39988,   0.39988,    43.989,    44.046,         0,      4.6991
