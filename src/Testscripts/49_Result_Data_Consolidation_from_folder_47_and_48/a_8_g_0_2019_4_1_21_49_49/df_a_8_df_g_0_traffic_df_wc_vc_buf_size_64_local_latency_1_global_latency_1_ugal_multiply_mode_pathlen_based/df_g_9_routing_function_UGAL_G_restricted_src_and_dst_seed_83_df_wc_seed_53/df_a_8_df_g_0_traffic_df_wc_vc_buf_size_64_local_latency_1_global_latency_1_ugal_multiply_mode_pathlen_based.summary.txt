wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 53;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49986,   0.49988,    20.154,    20.154,         0,      4.7864
simulation,         2,      0.74,   0.54281,   0.58245,    1069.2,     603.2,         1,         0.0
simulation,         3,      0.62,    0.5527,   0.57891,    439.07,    345.67,         1,         0.0
simulation,         4,      0.56,   0.55988,   0.55987,    37.576,    37.584,         0,      4.8391
simulation,         5,      0.59,    0.5545,   0.56398,    499.42,    292.31,         1,         0.0
simulation,         6,      0.57,    0.5558,   0.55595,    524.14,     249.6,         1,         0.0
simulation,         7,       0.1,  0.099856,  0.099855,    11.427,    11.427,         0,      4.4914
simulation,         8,       0.2,   0.19995,   0.19995,    12.289,    12.289,         0,      4.5396
simulation,         9,       0.3,   0.29998,   0.29998,     13.49,     13.49,         0,       4.585
simulation,        10,       0.4,   0.39989,   0.39988,    15.781,    15.781,         0,      4.6916
simulation,        11,       0.5,   0.49986,   0.49988,    20.154,    20.154,         0,      4.7864
