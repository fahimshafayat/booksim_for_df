wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 52;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 82;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35441,   0.39354,    1022.5,    742.83,         1,         0.0
simulation,         2,      0.25,   0.24749,   0.24755,     475.6,    141.14,         1,         0.0
simulation,         3,      0.13,   0.13002,   0.13001,    31.723,    31.751,         0,      5.1399
simulation,         4,      0.19,   0.18974,   0.18976,    131.11,     54.75,         0,      5.3247
simulation,         5,      0.22,   0.21879,   0.21881,     470.6,    126.36,         0,      5.3793
simulation,         6,      0.23,    0.2284,    0.2284,    374.63,    122.24,         1,         0.0
simulation,         7,      0.05,  0.050023,  0.050023,    12.155,    12.155,         0,      4.7559
simulation,         8,       0.1,   0.10002,   0.10002,    26.186,    26.207,         0,      5.0198
simulation,         9,      0.15,   0.14999,      0.15,     33.42,    33.484,         0,      5.2174
simulation,        10,       0.2,   0.19953,   0.19961,    211.34,    97.763,         0,      5.3449
