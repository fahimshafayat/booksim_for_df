wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 52;
routing_function = UGAL_G;
seed = 82;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50001,   0.50002,    38.303,    38.305,         0,      6.0809
simulation,         2,      0.74,   0.40771,   0.45615,    1774.9,    969.42,         1,         0.0
simulation,         3,      0.62,     0.461,   0.50188,    958.19,    660.54,         1,         0.0
simulation,         4,      0.56,   0.48918,   0.51454,    423.69,    360.93,         1,         0.0
simulation,         5,      0.53,   0.50786,   0.50771,    821.92,    262.93,         1,         0.0
simulation,         6,      0.51,   0.51001,   0.51001,    43.974,    43.977,         0,      6.0899
simulation,         7,      0.52,   0.52005,   0.52004,    53.938,    53.945,         0,      6.0979
simulation,         8,       0.1,    0.1001,    0.1001,    13.651,    13.651,         0,      5.3714
simulation,         9,       0.2,   0.20002,   0.20002,    15.469,    15.469,         0,      5.5735
simulation,        10,       0.3,   0.30018,   0.30017,    18.301,    18.301,         0,      5.8361
simulation,        11,       0.4,   0.40012,   0.40011,    22.566,    22.567,         0,      5.9845
simulation,        12,       0.5,   0.50001,   0.50002,    38.303,    38.305,         0,      6.0809
