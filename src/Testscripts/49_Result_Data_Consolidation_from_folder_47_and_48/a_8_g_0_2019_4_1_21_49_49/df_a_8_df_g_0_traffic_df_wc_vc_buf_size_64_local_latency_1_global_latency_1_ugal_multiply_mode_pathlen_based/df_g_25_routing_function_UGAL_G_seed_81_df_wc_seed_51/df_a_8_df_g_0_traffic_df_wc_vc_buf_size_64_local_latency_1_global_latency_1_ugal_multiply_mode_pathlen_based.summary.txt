wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 51;
routing_function = UGAL_G;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.36641,   0.40737,    950.43,    753.54,         1,         0.0
simulation,         2,      0.25,   0.25011,   0.25011,    17.785,    17.785,         0,        6.01
simulation,         3,      0.37,   0.37008,   0.37009,    24.201,    24.202,         0,      6.1575
simulation,         4,      0.43,   0.39375,   0.40569,    619.68,    448.71,         1,         0.0
simulation,         5,       0.4,   0.40007,   0.40006,    35.085,     35.09,         0,      6.1851
simulation,         6,      0.41,   0.39989,    0.4001,    473.85,    292.03,         1,         0.0
simulation,         7,       0.1,   0.10008,   0.10008,    14.514,    14.514,         0,      5.6569
simulation,         8,       0.2,   0.20015,   0.20016,    16.444,    16.444,         0,      5.9024
simulation,         9,       0.3,   0.30014,   0.30015,    19.488,    19.488,         0,      6.0843
simulation,        10,       0.4,   0.40007,   0.40006,    35.085,     35.09,         0,      6.1851
