wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 53;
routing_function = UGAL_G_restricted_src_only;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.40372,   0.43716,    696.06,    545.42,         1,         0.0
simulation,         2,      0.25,   0.24995,   0.24995,    15.114,    15.114,         0,      5.2415
simulation,         3,      0.37,   0.36988,   0.36986,    19.129,     19.13,         0,      5.3536
simulation,         4,      0.43,   0.40634,   0.40691,    1025.0,    376.11,         1,         0.0
simulation,         5,       0.4,   0.39987,   0.39985,    24.617,    24.617,         0,      5.3769
simulation,         6,      0.41,   0.40714,   0.40704,     333.1,    118.55,         1,         0.0
simulation,         7,       0.1,  0.099944,  0.099941,    13.053,    13.053,         0,      5.0492
simulation,         8,       0.2,   0.19989,   0.19989,     14.26,    14.261,         0,      5.1745
simulation,         9,       0.3,    0.2999,   0.29989,    16.214,    16.215,         0,      5.2964
simulation,        10,       0.4,   0.39987,   0.39985,    24.617,    24.617,         0,      5.3769
