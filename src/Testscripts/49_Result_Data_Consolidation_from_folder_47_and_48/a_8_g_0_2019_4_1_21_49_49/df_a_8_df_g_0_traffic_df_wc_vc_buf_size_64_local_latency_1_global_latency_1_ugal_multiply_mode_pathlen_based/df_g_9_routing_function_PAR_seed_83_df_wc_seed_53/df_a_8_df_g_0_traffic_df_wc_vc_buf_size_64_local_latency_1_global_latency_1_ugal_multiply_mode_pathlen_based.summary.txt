wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 53;
routing_function = PAR;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46372,   0.47694,    595.49,    394.31,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24999,    15.101,    15.101,         0,      5.3976
simulation,         3,      0.37,   0.36999,   0.36999,    21.731,     21.73,         0,      5.8002
simulation,         4,      0.43,   0.42997,      0.43,    30.064,    30.063,         0,      5.9564
simulation,         5,      0.46,   0.45996,   0.45997,    51.388,    51.416,         0,      6.0031
simulation,         6,      0.48,   0.46524,    0.4657,    617.41,    312.63,         1,         0.0
simulation,         7,      0.47,   0.46561,   0.46565,    487.99,    168.32,         1,         0.0
simulation,         8,       0.1,  0.099857,  0.099855,    13.383,    13.384,         0,      5.4266
simulation,         9,       0.2,   0.19995,   0.19995,     14.15,    14.151,         0,      5.3583
simulation,        10,       0.3,   0.29998,   0.29998,    17.046,    17.046,         0,      5.5322
simulation,        11,       0.4,   0.39988,   0.39988,    24.783,    24.782,         0,       5.889
