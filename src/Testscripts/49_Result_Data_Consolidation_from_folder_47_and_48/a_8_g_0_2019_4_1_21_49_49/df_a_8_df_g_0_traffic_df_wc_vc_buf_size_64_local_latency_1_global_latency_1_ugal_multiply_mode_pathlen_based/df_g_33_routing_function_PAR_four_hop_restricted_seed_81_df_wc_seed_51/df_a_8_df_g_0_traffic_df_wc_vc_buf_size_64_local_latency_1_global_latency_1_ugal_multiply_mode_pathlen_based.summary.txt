wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 51;
routing_function = PAR_four_hop_restricted;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.40506,   0.43302,    607.96,    470.25,         1,         0.0
simulation,         2,      0.25,   0.25006,   0.25006,    18.363,    18.386,         0,      4.9735
simulation,         3,      0.37,   0.35447,   0.35473,    742.46,    286.76,         1,         0.0
simulation,         4,      0.31,   0.30905,   0.30906,    250.61,    89.924,         0,       5.009
simulation,         5,      0.34,   0.33277,   0.33292,    436.17,    186.25,         1,         0.0
simulation,         6,      0.32,   0.31778,   0.31778,    394.06,    123.45,         1,         0.0
simulation,         7,       0.1,   0.10003,   0.10003,    12.398,    12.398,         0,      4.8966
simulation,         8,       0.2,   0.20005,   0.20005,    14.437,    14.443,         0,      4.9341
simulation,         9,       0.3,   0.30001,   0.30001,    52.097,    43.441,         0,      5.0069
