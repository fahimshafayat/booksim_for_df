wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 51;
routing_function = UGAL_L_four_hop_restricted;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.30497,   0.34642,    1299.9,    946.76,         1,         0.0
simulation,         2,      0.25,   0.23976,   0.24025,    674.03,    337.48,         1,         0.0
simulation,         3,      0.13,   0.12995,   0.12995,    98.942,    50.117,         0,      4.5258
simulation,         4,      0.19,   0.18809,   0.18816,    471.19,    150.18,         1,         0.0
simulation,         5,      0.16,   0.15931,   0.15931,     461.4,     97.32,         0,      4.5771
simulation,         6,      0.17,     0.169,   0.16906,    307.37,    121.34,         1,         0.0
simulation,         7,      0.05,  0.050046,  0.050046,    11.289,    11.289,         0,      4.3107
simulation,         8,       0.1,   0.10008,   0.10008,    26.377,      26.4,         0,       4.462
simulation,         9,      0.15,   0.14955,   0.14956,    321.77,    100.73,         0,      4.5623
