wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 53;
routing_function = PAR;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.44918,   0.45823,    636.07,    327.08,         1,         0.0
simulation,         2,      0.25,   0.25005,   0.25004,    19.709,     19.71,         0,      6.3715
simulation,         3,      0.37,   0.36848,   0.36848,    473.22,    57.107,         0,      6.3802
simulation,         4,      0.43,   0.42156,   0.42156,    484.27,    118.89,         1,         0.0
simulation,         5,       0.4,    0.3976,   0.39759,    254.51,    60.288,         1,         0.0
simulation,         6,      0.38,   0.37811,   0.37811,    229.24,    57.937,         1,         0.0
simulation,         7,       0.1,  0.099999,  0.099999,    14.953,    14.953,         0,      6.0024
simulation,         8,       0.2,       0.2,   0.19999,    17.808,    17.809,         0,      6.2782
simulation,         9,       0.3,   0.30001,   0.30001,    22.521,    22.521,         0,      6.4116
