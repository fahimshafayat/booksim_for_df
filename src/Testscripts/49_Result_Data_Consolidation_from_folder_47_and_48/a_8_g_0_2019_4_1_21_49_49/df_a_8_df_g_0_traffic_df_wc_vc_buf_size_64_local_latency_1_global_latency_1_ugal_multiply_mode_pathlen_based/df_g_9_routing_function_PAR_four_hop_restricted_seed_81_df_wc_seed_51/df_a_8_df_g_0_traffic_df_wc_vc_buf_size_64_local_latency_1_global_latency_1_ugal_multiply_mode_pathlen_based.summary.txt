wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 51;
routing_function = PAR_four_hop_restricted;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46699,   0.47634,     483.2,    310.28,         1,         0.0
simulation,         2,      0.25,   0.24984,   0.24984,    12.556,    12.556,         0,      4.5751
simulation,         3,      0.37,   0.36998,   0.36999,    24.576,    24.608,         0,      4.6296
simulation,         4,      0.43,   0.42741,   0.42742,    318.29,    92.136,         1,         0.0
simulation,         5,       0.4,   0.39996,   0.39997,    38.518,    38.682,         0,      4.6711
simulation,         6,      0.41,   0.40974,    0.4098,    72.014,    58.715,         0,      4.6904
simulation,         7,      0.42,    0.4188,    0.4188,    253.77,    67.826,         0,      4.7096
simulation,         8,       0.1,  0.099918,  0.099918,    11.518,    11.518,         0,      4.5671
simulation,         9,       0.2,    0.1998,    0.1998,    12.092,    12.092,         0,      4.5667
simulation,        10,       0.3,   0.29995,   0.29995,    16.831,    16.848,         0,      4.5885
simulation,        11,       0.4,   0.39996,   0.39997,    38.518,    38.682,         0,      4.6711
