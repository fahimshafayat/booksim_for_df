wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 50;
perm_seed = 50;
routing_function = PAR;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49998,   0.49998,    25.491,    25.512,         0,      4.9883
simulation,         2,      0.74,   0.55077,   0.59913,     956.6,    674.31,         1,         0.0
simulation,         3,      0.62,   0.55385,   0.56968,    786.08,    467.24,         1,         0.0
simulation,         4,      0.56,   0.55635,   0.55642,    402.53,    164.93,         1,         0.0
simulation,         5,      0.53,   0.53002,   0.53003,    34.928,    34.964,         0,      5.1554
simulation,         6,      0.54,   0.54003,   0.54004,     41.53,    41.568,         0,      5.2468
simulation,         7,      0.55,      0.55,   0.55002,    56.317,    56.381,         0,      5.3829
simulation,         8,       0.1,  0.099976,  0.099975,    12.596,    12.596,         0,      5.1068
simulation,         9,       0.2,   0.20002,   0.20003,    12.761,    12.762,         0,      4.9517
simulation,        10,       0.3,   0.29998,   0.29997,    13.234,    13.234,         0,      4.8446
simulation,        11,       0.4,   0.40001,   0.40001,     14.79,    14.791,         0,      4.8162
simulation,        12,       0.5,   0.49998,   0.49998,    25.491,    25.512,         0,      4.9883
