wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 50;
perm_seed = 50;
routing_function = vlb;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.47524,   0.48014,    904.89,     673.2,         1,         0.0
simulation,         2,      0.25,   0.24999,   0.24999,    17.333,    17.333,         0,      6.4066
simulation,         3,      0.37,   0.37009,   0.37008,    21.768,    21.768,         0,       6.407
simulation,         4,      0.43,   0.43001,   0.42999,    29.249,    29.249,         0,      6.4071
simulation,         5,      0.46,   0.45997,   0.45995,    41.233,    41.235,         0,      6.4069
simulation,         6,      0.48,   0.47731,    0.4781,    356.11,    207.69,         0,      6.4072
simulation,         7,      0.49,   0.47684,   0.48206,    594.36,    472.14,         1,         0.0
simulation,         8,       0.1,  0.099976,  0.099975,    15.447,    15.447,         0,      6.4061
simulation,         9,       0.2,   0.20002,   0.20003,    16.502,    16.502,         0,      6.4068
simulation,        10,       0.3,   0.29997,   0.29997,    18.555,    18.555,         0,       6.407
simulation,        11,       0.4,   0.40002,   0.40001,    24.438,    24.438,         0,      6.4073
