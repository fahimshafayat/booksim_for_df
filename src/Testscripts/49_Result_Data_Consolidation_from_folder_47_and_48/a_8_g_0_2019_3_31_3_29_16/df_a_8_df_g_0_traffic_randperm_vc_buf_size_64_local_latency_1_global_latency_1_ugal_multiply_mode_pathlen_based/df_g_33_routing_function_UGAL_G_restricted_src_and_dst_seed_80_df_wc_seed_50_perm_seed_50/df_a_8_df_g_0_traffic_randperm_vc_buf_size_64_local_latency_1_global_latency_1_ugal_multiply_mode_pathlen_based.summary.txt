wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49998,   0.49998,    13.915,    13.915,         0,      4.2558
simulation,         2,      0.74,   0.66262,   0.67606,     731.2,     347.8,         1,         0.0
simulation,         3,      0.62,   0.61997,   0.61997,    22.118,    22.119,         0,      4.3957
simulation,         4,      0.68,   0.67276,   0.67282,     518.9,    144.16,         1,         0.0
simulation,         5,      0.65,   0.64999,   0.64998,    30.636,    30.638,         0,      4.4673
simulation,         6,      0.66,   0.65996,   0.65998,    36.942,    36.945,         0,      4.4989
simulation,         7,      0.67,   0.66995,   0.66997,    49.267,    49.275,         0,       4.537
simulation,         8,       0.1,  0.099976,  0.099975,    10.873,    10.873,         0,      4.3166
simulation,         9,       0.2,   0.20003,   0.20003,     11.15,     11.15,         0,      4.2891
simulation,        10,       0.3,   0.29997,   0.29997,    11.563,    11.563,         0,      4.2597
simulation,        11,       0.4,   0.40002,   0.40001,    12.265,    12.265,         0,      4.2387
simulation,        12,       0.5,   0.49998,   0.49998,    13.915,    13.915,         0,      4.2558
simulation,        13,       0.6,       0.6,   0.59999,    19.409,     19.41,         0,      4.3595
