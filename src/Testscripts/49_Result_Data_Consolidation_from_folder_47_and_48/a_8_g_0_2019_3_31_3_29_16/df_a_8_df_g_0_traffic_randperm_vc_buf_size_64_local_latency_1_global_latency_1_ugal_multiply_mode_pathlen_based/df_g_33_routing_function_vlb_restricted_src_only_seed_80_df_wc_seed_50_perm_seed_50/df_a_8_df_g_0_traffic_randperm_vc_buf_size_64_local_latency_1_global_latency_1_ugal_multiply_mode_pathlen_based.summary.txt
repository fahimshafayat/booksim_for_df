wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 50;
perm_seed = 50;
routing_function = vlb_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35518,   0.40159,    1062.3,    790.29,         1,         0.0
simulation,         2,      0.25,      0.25,   0.24999,    15.012,    15.012,         0,       5.623
simulation,         3,      0.37,   0.36202,    0.3621,    507.33,    238.49,         1,         0.0
simulation,         4,      0.31,   0.31002,   0.31002,    16.049,    16.049,         0,      5.6227
simulation,         5,      0.34,   0.34003,   0.34003,     16.88,     16.88,         0,      5.6229
simulation,         6,      0.35,   0.35004,   0.35003,    17.443,    17.444,         0,       5.623
simulation,         7,      0.36,   0.35632,   0.35646,    396.39,    106.02,         1,         0.0
simulation,         8,       0.1,  0.099976,  0.099975,    13.707,    13.707,         0,      5.6223
simulation,         9,       0.2,   0.20002,   0.20003,    14.449,    14.449,         0,      5.6227
simulation,        10,       0.3,   0.29998,   0.29997,    15.832,    15.832,         0,      5.6229
