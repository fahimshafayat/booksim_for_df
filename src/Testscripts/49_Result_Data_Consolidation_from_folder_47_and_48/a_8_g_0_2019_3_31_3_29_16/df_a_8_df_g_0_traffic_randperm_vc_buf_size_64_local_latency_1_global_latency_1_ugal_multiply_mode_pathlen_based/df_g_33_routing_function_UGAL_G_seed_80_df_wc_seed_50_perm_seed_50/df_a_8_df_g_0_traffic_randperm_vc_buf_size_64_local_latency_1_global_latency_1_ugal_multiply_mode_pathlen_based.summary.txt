wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_G;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49998,   0.49998,    15.011,    15.012,         0,      4.3323
simulation,         2,      0.74,   0.62844,    0.6603,     540.7,    387.74,         1,         0.0
simulation,         3,      0.62,   0.61997,   0.61997,    32.806,    32.809,         0,        4.62
simulation,         4,      0.68,   0.63634,    0.6462,    470.05,     260.7,         1,         0.0
simulation,         5,      0.65,   0.64524,   0.64526,    406.71,     130.6,         1,         0.0
simulation,         6,      0.63,   0.62993,   0.62994,    39.306,    39.312,         0,       4.665
simulation,         7,      0.64,   0.63995,   0.63996,     50.89,      50.9,         0,      4.7193
simulation,         8,       0.1,  0.099976,  0.099975,    11.051,    11.051,         0,      4.4021
simulation,         9,       0.2,   0.20003,   0.20003,    11.244,    11.245,         0,      4.3363
simulation,        10,       0.3,   0.29998,   0.29997,     11.62,     11.62,         0,      4.2871
simulation,        11,       0.4,   0.40001,   0.40001,    12.469,    12.469,         0,      4.2685
simulation,        12,       0.5,   0.49998,   0.49998,    15.011,    15.012,         0,      4.3323
simulation,        13,       0.6,       0.6,   0.59999,    25.705,    25.706,         0,      4.5474
