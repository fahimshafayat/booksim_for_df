wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 50;
perm_seed = 50;
routing_function = PAR_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49998,   0.49998,    17.586,    17.592,         0,      4.6928
simulation,         2,      0.74,   0.56328,   0.60999,    879.64,    600.66,         1,         0.0
simulation,         3,      0.62,   0.57351,   0.57726,    1161.2,    470.46,         1,         0.0
simulation,         4,      0.56,   0.56001,   0.56001,    26.358,    26.373,         0,      4.8375
simulation,         5,      0.59,    0.5898,   0.58981,    70.797,    61.769,         0,      5.0636
simulation,         6,       0.6,   0.59246,   0.59267,    518.56,    188.68,         1,         0.0
simulation,         7,       0.1,  0.099975,  0.099975,    12.016,    12.016,         0,      4.8395
simulation,         8,       0.2,   0.20003,   0.20003,    12.351,    12.351,         0,      4.7812
simulation,         9,       0.3,   0.29998,   0.29997,    12.774,    12.775,         0,      4.6947
simulation,        10,       0.4,   0.40001,   0.40001,    13.682,    13.682,         0,      4.6498
simulation,        11,       0.5,   0.49998,   0.49998,    17.586,    17.592,         0,      4.6928
