internal_speedup = 4.0;
wait_for_tail_credit = 0;
num_vcs = 8;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
injection_rate_uses_flits = 1;
output_speedup = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
vc_alloc_delay = 1;
credit_delay = 2;
warmup_periods = 3;
sample_period = 10000;
packet_size = 1;
priority = none;
sw_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 50;
perm_seed = 50;
routing_function = PAR;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50025,   0.50025,    12.729,    12.729,         0,      4.1728
simulation,         2,      0.74,   0.74011,   0.74012,    15.841,    15.841,         0,      3.9936
simulation,         3,      0.86,   0.85996,   0.85998,    21.621,    21.622,         0,      3.9339
simulation,         4,      0.92,   0.91989,    0.9199,    31.319,    31.322,         0,      3.9023
simulation,         5,      0.95,   0.94995,   0.94994,    44.288,    44.291,         0,      3.8653
simulation,         6,      0.97,   0.96994,   0.96996,     63.66,    63.673,         0,       3.823
simulation,         7,      0.98,   0.97992,   0.97995,    80.829,    80.862,         0,      3.7987
simulation,         8,       0.1,   0.10017,   0.10017,    11.971,    11.972,         0,      4.8128
simulation,         9,       0.2,    0.2002,    0.2002,    11.952,    11.952,         0,        4.61
simulation,        10,       0.3,   0.30023,   0.30023,    12.008,    12.008,         0,      4.4223
simulation,        11,       0.4,   0.40026,   0.40026,    12.251,    12.252,         0,      4.2801
simulation,        12,       0.5,   0.50025,   0.50025,    12.729,    12.729,         0,      4.1728
simulation,        13,       0.6,   0.60016,   0.60015,    13.531,    13.531,         0,      4.0865
simulation,        14,       0.7,   0.70013,   0.70014,    14.954,    14.954,         0,      4.0176
simulation,        15,       0.8,       0.8,   0.79999,    17.843,    17.844,         0,      3.9613
simulation,        16,       0.9,   0.89992,   0.89992,    26.704,    26.704,         0,      3.9148
