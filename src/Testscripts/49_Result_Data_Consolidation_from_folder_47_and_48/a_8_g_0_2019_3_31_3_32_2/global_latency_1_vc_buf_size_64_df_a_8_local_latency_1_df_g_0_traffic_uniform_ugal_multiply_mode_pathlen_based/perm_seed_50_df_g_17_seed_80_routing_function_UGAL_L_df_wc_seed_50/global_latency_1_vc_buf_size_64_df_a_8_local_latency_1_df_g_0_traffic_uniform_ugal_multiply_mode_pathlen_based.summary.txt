internal_speedup = 4.0;
wait_for_tail_credit = 0;
num_vcs = 8;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
injection_rate_uses_flits = 1;
output_speedup = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
vc_alloc_delay = 1;
credit_delay = 2;
warmup_periods = 3;
sample_period = 10000;
packet_size = 1;
priority = none;
sw_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_L;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50025,   0.50025,    12.097,    12.097,         0,      4.0103
simulation,         2,      0.74,   0.74012,   0.74012,    15.139,    15.139,         0,      3.9048
simulation,         3,      0.86,   0.85997,   0.85998,    20.438,    20.439,         0,      3.8679
simulation,         4,      0.92,   0.91988,    0.9199,    28.791,    28.793,         0,      3.8523
simulation,         5,      0.95,    0.9499,   0.94994,    40.262,    40.267,         0,      3.8386
simulation,         6,      0.97,    0.9699,   0.96996,    57.281,    57.298,         0,      3.8133
simulation,         7,      0.98,   0.97997,   0.97995,    73.516,    73.537,         0,      3.7937
simulation,         8,       0.1,   0.10017,   0.10017,    10.835,    10.836,         0,      4.2893
simulation,         9,       0.2,    0.2002,    0.2002,    11.058,    11.058,         0,      4.2396
simulation,        10,       0.3,   0.30023,   0.30023,    11.257,    11.257,         0,      4.1496
simulation,        11,       0.4,   0.40026,   0.40026,    11.582,    11.582,         0,      4.0726
simulation,        12,       0.5,   0.50025,   0.50025,    12.097,    12.097,         0,      4.0103
simulation,        13,       0.6,   0.60015,   0.60015,    12.911,    12.911,         0,      3.9601
simulation,        14,       0.7,   0.70013,   0.70014,    14.299,      14.3,         0,      3.9197
simulation,        15,       0.8,   0.79999,   0.79999,    17.017,    17.018,         0,      3.8849
simulation,        16,       0.9,   0.89992,   0.89992,    24.891,    24.892,         0,      3.8577
