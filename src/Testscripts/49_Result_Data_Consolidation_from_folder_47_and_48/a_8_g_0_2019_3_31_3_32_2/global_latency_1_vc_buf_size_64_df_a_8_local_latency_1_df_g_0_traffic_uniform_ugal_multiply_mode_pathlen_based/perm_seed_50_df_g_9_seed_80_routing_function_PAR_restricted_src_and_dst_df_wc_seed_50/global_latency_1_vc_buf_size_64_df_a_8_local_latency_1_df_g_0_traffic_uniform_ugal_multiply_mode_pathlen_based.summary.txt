internal_speedup = 4.0;
wait_for_tail_credit = 0;
num_vcs = 8;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
injection_rate_uses_flits = 1;
output_speedup = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
vc_alloc_delay = 1;
credit_delay = 2;
warmup_periods = 3;
sample_period = 10000;
packet_size = 1;
priority = none;
sw_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 50;
perm_seed = 50;
routing_function = PAR_restricted_src_and_dst;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50015,    12.104,    12.105,         0,      3.9876
simulation,         2,      0.74,   0.74011,   0.74012,    14.811,    14.811,         0,      3.8664
simulation,         3,      0.86,   0.86006,   0.86005,    19.073,    19.073,         0,      3.8192
simulation,         4,      0.92,   0.92001,   0.92002,    25.738,     25.74,         0,      3.8067
simulation,         5,      0.95,   0.95063,   0.94997,    171.82,    171.35,         0,      3.8549
simulation,         6,      0.97,   0.97011,   0.96991,    162.51,    161.41,         0,      3.8021
simulation,         7,      0.98,   0.98032,   0.97994,    153.55,    153.34,         0,      3.7762
simulation,         8,       0.1,   0.10003,   0.10003,     10.81,    10.811,         0,      4.2699
simulation,         9,       0.2,   0.19998,   0.19998,    11.065,    11.065,         0,      4.2272
simulation,        10,       0.3,   0.30011,   0.30011,    11.284,    11.284,         0,      4.1371
simulation,        11,       0.4,   0.40003,   0.40002,    11.607,    11.608,         0,      4.0548
simulation,        12,       0.5,   0.50014,   0.50015,    12.104,    12.105,         0,      3.9876
simulation,        13,       0.6,   0.59993,   0.59993,    12.857,    12.857,         0,      3.9316
simulation,        14,       0.7,   0.69993,   0.69993,    14.087,    14.087,         0,      3.8843
simulation,        15,       0.8,   0.79997,   0.79997,    16.363,    16.363,         0,      3.8413
simulation,        16,       0.9,   0.90005,   0.90005,    22.576,    22.578,         0,      3.8075
