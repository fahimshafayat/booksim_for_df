internal_speedup = 4.0;
wait_for_tail_credit = 0;
num_vcs = 8;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
injection_rate_uses_flits = 1;
output_speedup = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
vc_alloc_delay = 1;
credit_delay = 2;
warmup_periods = 3;
sample_period = 10000;
packet_size = 1;
priority = none;
sw_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50008,   0.50008,    11.635,    11.635,         0,      3.8346
simulation,         2,      0.74,   0.74007,   0.74006,    15.355,    15.356,         0,      3.7963
simulation,         3,      0.86,   0.86006,   0.86005,    28.194,    28.197,         0,      3.8378
simulation,         4,      0.92,   0.84772,    0.8659,     633.2,    331.12,         1,         0.0
simulation,         5,      0.89,   0.88999,   0.89002,    45.077,    45.089,         0,      3.8763
simulation,         6,       0.9,   0.85168,   0.85112,    826.17,     373.6,         1,         0.0
simulation,         7,       0.1,    0.1001,    0.1001,    10.332,    10.332,         0,      4.0477
simulation,         8,       0.2,   0.20016,   0.20016,    10.455,    10.455,         0,      3.9689
simulation,         9,       0.3,    0.3002,    0.3002,    10.692,    10.692,         0,       3.911
simulation,        10,       0.4,   0.40011,   0.40011,    11.066,    11.066,         0,      3.8674
simulation,        11,       0.5,   0.50008,   0.50008,    11.635,    11.635,         0,      3.8346
simulation,        12,       0.6,   0.60002,   0.60001,    12.551,    12.551,         0,      3.8109
simulation,        13,       0.7,   0.70012,   0.70011,    14.219,     14.22,         0,      3.7976
simulation,        14,       0.8,   0.80005,   0.80005,    18.431,    18.432,         0,      3.8026
