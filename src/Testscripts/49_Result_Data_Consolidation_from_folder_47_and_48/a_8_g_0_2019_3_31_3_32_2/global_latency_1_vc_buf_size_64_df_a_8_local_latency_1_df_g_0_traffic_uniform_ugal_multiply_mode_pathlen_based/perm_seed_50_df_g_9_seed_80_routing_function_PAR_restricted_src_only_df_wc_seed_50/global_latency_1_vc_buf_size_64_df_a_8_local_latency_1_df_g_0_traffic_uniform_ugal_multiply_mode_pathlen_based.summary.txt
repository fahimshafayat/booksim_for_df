internal_speedup = 4.0;
wait_for_tail_credit = 0;
num_vcs = 8;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
injection_rate_uses_flits = 1;
output_speedup = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
vc_alloc_delay = 1;
credit_delay = 2;
warmup_periods = 3;
sample_period = 10000;
packet_size = 1;
priority = none;
sw_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 50;
perm_seed = 50;
routing_function = PAR_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50015,   0.50015,    12.046,    12.047,         0,        3.97
simulation,         2,      0.74,   0.74012,   0.74012,    14.746,    14.746,         0,      3.8567
simulation,         3,      0.86,   0.86006,   0.86005,    19.004,    19.005,         0,      3.8118
simulation,         4,      0.92,   0.91999,   0.92002,    26.359,     26.36,         0,      3.8101
simulation,         5,      0.95,   0.95174,      0.95,     211.8,    214.05,         0,      3.8269
simulation,         6,      0.97,   0.96846,   0.96995,    225.02,    225.09,         0,       3.779
simulation,         7,      0.98,   0.98019,   0.97991,    223.84,    223.14,         0,      3.7614
simulation,         8,       0.1,   0.10003,   0.10003,    10.703,    10.703,         0,      4.2207
simulation,         9,       0.2,   0.19998,   0.19998,    10.959,    10.959,         0,      4.1819
simulation,        10,       0.3,   0.30011,   0.30011,    11.197,    11.197,         0,      4.1033
simulation,        11,       0.4,   0.40003,   0.40002,    11.548,    11.548,         0,      4.0328
simulation,        12,       0.5,   0.50015,   0.50015,    12.046,    12.047,         0,        3.97
simulation,        13,       0.6,   0.59993,   0.59993,    12.804,    12.805,         0,      3.9183
simulation,        14,       0.7,   0.69993,   0.69993,    14.023,    14.023,         0,       3.873
simulation,        15,       0.8,   0.79996,   0.79997,    16.279,    16.279,         0,       3.832
simulation,        16,       0.9,   0.90006,   0.90005,    22.573,    22.575,         0,      3.8035
