internal_speedup = 4.0;
wait_for_tail_credit = 0;
num_vcs = 8;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
injection_rate_uses_flits = 1;
output_speedup = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
vc_alloc_delay = 1;
credit_delay = 2;
warmup_periods = 3;
sample_period = 10000;
packet_size = 1;
priority = none;
sw_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 50;
perm_seed = 50;
routing_function = PAR;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50015,   0.50015,    12.303,    12.303,         0,      4.0579
simulation,         2,      0.74,   0.74011,   0.74012,    14.949,    14.949,         0,      3.8882
simulation,         3,      0.86,   0.86006,   0.86005,    19.282,    19.282,         0,       3.827
simulation,         4,      0.92,   0.91999,   0.92002,    25.329,    25.331,         0,      3.8005
simulation,         5,      0.95,   0.94998,      0.95,    32.431,    32.435,         0,      3.7837
simulation,         6,      0.97,   0.96996,   0.96997,    43.021,    43.034,         0,       3.768
simulation,         7,      0.98,   0.97994,   0.97994,    54.274,    54.283,         0,      3.7568
simulation,         8,       0.1,   0.10003,   0.10003,    11.615,    11.615,         0,      4.6475
simulation,         9,       0.2,   0.19998,   0.19998,    11.629,     11.63,         0,      4.4733
simulation,        10,       0.3,   0.30012,   0.30011,    11.677,    11.678,         0,      4.2975
simulation,        11,       0.4,   0.40003,   0.40002,    11.888,    11.889,         0,      4.1615
simulation,        12,       0.5,   0.50015,   0.50015,    12.303,    12.303,         0,      4.0579
simulation,        13,       0.6,   0.59993,   0.59993,    13.008,    13.009,         0,      3.9775
simulation,        14,       0.7,   0.69993,   0.69993,    14.217,    14.218,         0,      3.9118
simulation,        15,       0.8,   0.79997,   0.79997,    16.517,    16.518,         0,      3.8551
simulation,        16,       0.9,   0.90004,   0.90005,    22.602,    22.604,         0,       3.809
