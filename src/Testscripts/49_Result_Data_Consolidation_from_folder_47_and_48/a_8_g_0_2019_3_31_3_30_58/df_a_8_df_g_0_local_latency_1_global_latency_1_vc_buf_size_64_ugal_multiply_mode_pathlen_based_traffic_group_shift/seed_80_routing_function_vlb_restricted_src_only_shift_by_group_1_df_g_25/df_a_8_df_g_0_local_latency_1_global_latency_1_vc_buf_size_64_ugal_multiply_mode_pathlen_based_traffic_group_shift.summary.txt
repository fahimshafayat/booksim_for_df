wait_for_tail_credit = 0;
injection_rate_uses_flits = 1;
sim_count = 1;
vc_alloc_delay = 1;
input_speedup = 1;
sw_allocator = separable_input_first;
internal_speedup = 4.0;
st_final_delay = 1;
num_vcs = 8;
warmup_periods = 3;
packet_size = 1;
sample_period = 10000;
alloc_iters = 1;
output_speedup = 1;
routing_delay = 0;
vc_allocator = separable_input_first;
priority = none;
sw_alloc_delay = 1;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = vlb_restricted_src_only;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.24093,   0.29281,    1926.1,    1442.5,         1,         0.0
simulation,         2,      0.25,   0.25011,    0.2501,    15.405,    15.406,         0,      5.4254
simulation,         3,      0.37,   0.27149,    0.3167,    847.83,    768.52,         1,         0.0
simulation,         4,      0.31,   0.27598,   0.28608,    499.18,    329.57,         1,         0.0
simulation,         5,      0.28,   0.27266,   0.27187,    778.82,    365.99,         1,         0.0
simulation,         6,      0.26,   0.23321,   0.23612,     375.4,    291.46,         1,         0.0
simulation,         7,      0.05,  0.050013,  0.050014,     13.06,     13.06,         0,      5.4253
simulation,         8,       0.1,    0.1001,    0.1001,    13.335,    13.335,         0,      5.4254
simulation,         9,      0.15,   0.15015,   0.15015,    13.711,    13.711,         0,      5.4254
simulation,        10,       0.2,   0.20016,   0.20016,    14.257,    14.257,         0,      5.4252
simulation,        11,      0.25,   0.25011,    0.2501,    15.405,    15.406,         0,      5.4254
