wait_for_tail_credit = 0;
injection_rate_uses_flits = 1;
sim_count = 1;
vc_alloc_delay = 1;
input_speedup = 1;
sw_allocator = separable_input_first;
internal_speedup = 4.0;
st_final_delay = 1;
num_vcs = 8;
warmup_periods = 3;
packet_size = 1;
sample_period = 10000;
alloc_iters = 1;
output_speedup = 1;
routing_delay = 0;
vc_allocator = separable_input_first;
priority = none;
sw_alloc_delay = 1;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = vlb_restricted_src_only;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.37824,   0.42655,    904.64,    769.41,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24998,    13.978,    13.978,         0,      5.0786
simulation,         3,      0.37,   0.37017,   0.37017,    19.427,    19.428,         0,      5.0788
simulation,         4,      0.43,   0.36238,   0.37729,     987.6,    538.05,         1,         0.0
simulation,         5,       0.4,   0.37509,   0.37524,    997.35,    396.22,         1,         0.0
simulation,         6,      0.38,   0.37474,    0.3769,    459.03,    280.29,         0,      5.0785
simulation,         7,      0.39,   0.37395,   0.37835,    479.55,    321.94,         1,         0.0
simulation,         8,       0.1,   0.10003,   0.10003,    12.596,    12.596,         0,      5.0792
simulation,         9,       0.2,   0.19998,   0.19998,    13.349,    13.349,         0,      5.0792
simulation,        10,       0.3,   0.30012,   0.30011,    15.016,    15.016,         0,      5.0793
