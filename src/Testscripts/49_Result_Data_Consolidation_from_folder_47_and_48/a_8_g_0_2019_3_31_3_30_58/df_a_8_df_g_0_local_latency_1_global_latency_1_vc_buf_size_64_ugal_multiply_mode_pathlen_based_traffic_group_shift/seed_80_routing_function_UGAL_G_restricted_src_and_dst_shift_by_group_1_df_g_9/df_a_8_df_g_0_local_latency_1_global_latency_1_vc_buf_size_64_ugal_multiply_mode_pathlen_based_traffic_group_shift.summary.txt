wait_for_tail_credit = 0;
injection_rate_uses_flits = 1;
sim_count = 1;
vc_alloc_delay = 1;
input_speedup = 1;
sw_allocator = separable_input_first;
internal_speedup = 4.0;
st_final_delay = 1;
num_vcs = 8;
warmup_periods = 3;
packet_size = 1;
sample_period = 10000;
alloc_iters = 1;
output_speedup = 1;
routing_delay = 0;
vc_allocator = separable_input_first;
priority = none;
sw_alloc_delay = 1;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50015,    19.742,    19.742,         0,      4.7305
simulation,         2,      0.74,   0.53559,   0.57888,    1175.4,    650.95,         1,         0.0
simulation,         3,      0.62,   0.54964,   0.57808,    469.61,    375.14,         1,         0.0
simulation,         4,      0.56,   0.55988,      0.56,     37.41,     37.41,         0,      4.7778
simulation,         5,      0.59,   0.55259,   0.56327,    525.77,    325.11,         1,         0.0
simulation,         6,      0.57,   0.55423,   0.55448,    550.67,    257.61,         1,         0.0
simulation,         7,       0.1,   0.10003,   0.10003,    11.348,    11.348,         0,      4.4571
simulation,         8,       0.2,   0.19998,   0.19998,    12.183,    12.183,         0,      4.5006
simulation,         9,       0.3,   0.30011,   0.30011,    13.348,    13.348,         0,      4.5416
simulation,        10,       0.4,   0.40003,   0.40002,    15.532,    15.532,         0,      4.6404
simulation,        11,       0.5,   0.50014,   0.50015,    19.742,    19.742,         0,      4.7305
