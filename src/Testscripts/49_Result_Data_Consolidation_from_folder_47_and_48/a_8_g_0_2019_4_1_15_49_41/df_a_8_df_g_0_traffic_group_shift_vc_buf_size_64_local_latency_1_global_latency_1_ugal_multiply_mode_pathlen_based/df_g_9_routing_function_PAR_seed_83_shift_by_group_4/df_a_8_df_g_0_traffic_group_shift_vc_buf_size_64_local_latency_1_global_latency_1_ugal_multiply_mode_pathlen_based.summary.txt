wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = PAR;
seed = 83;
shift_by_group = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46255,   0.47426,    576.43,    385.29,         1,         0.0
simulation,         2,      0.25,   0.24999,   0.24999,    15.082,    15.082,         0,      5.4027
simulation,         3,      0.37,   0.36999,   0.36999,    21.569,    21.568,         0,      5.7881
simulation,         4,      0.43,   0.42999,      0.43,    29.728,    29.727,         0,      5.9424
simulation,         5,      0.46,   0.45998,   0.45997,    50.686,    50.687,         0,      5.9911
simulation,         6,      0.48,   0.46709,     0.466,    604.23,     304.8,         1,         0.0
simulation,         7,      0.47,   0.46572,   0.46576,    487.52,    173.15,         1,         0.0
simulation,         8,       0.1,  0.099859,  0.099855,    13.399,    13.399,         0,      5.4347
simulation,         9,       0.2,   0.19995,   0.19995,    14.166,    14.166,         0,      5.3681
simulation,        10,       0.3,   0.29998,   0.29998,    16.948,    16.948,         0,       5.527
simulation,        11,       0.4,   0.39988,   0.39988,    24.599,    24.598,         0,      5.8754
