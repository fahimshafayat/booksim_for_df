wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = UGAL_L_restricted_src_only;
seed = 83;
shift_by_group = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.31723,   0.36012,    1170.3,    841.85,         1,         0.0
simulation,         2,      0.25,   0.24996,   0.24995,    30.098,    30.135,         0,      5.2608
simulation,         3,      0.37,   0.34397,   0.34433,    1082.2,    413.73,         1,         0.0
simulation,         4,      0.31,   0.30989,   0.30988,    31.233,    31.277,         0,      5.3187
simulation,         5,      0.34,   0.33788,   0.33795,    255.52,    73.573,         1,         0.0
simulation,         6,      0.32,   0.31986,   0.31984,    32.091,    32.142,         0,      5.3261
simulation,         7,      0.33,    0.3294,   0.32944,    135.76,     47.63,         0,      5.3331
simulation,         8,       0.1,  0.099946,  0.099941,    23.983,    24.001,         0,      4.9083
simulation,         9,       0.2,   0.19989,   0.19989,    30.595,    30.627,         0,      5.1881
simulation,        10,       0.3,    0.2999,   0.29989,    30.761,    30.805,         0,      5.3102
