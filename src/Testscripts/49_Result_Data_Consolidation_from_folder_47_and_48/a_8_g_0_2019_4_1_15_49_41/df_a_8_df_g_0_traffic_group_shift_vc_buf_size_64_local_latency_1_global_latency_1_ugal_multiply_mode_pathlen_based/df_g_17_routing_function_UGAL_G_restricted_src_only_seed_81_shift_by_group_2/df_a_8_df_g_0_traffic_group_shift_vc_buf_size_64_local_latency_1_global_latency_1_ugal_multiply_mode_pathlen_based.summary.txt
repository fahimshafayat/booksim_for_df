wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_G_restricted_src_only;
seed = 81;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49996,   0.49997,    24.237,    24.237,         0,      5.0531
simulation,         2,      0.74,   0.46702,   0.51319,    1582.8,    797.21,         1,         0.0
simulation,         3,      0.62,   0.49989,   0.53771,     753.6,    544.07,         1,         0.0
simulation,         4,      0.56,   0.51825,   0.52826,    558.12,    318.88,         1,         0.0
simulation,         5,      0.53,   0.52994,   0.52994,    77.278,    77.271,         0,      5.0754
simulation,         6,      0.54,   0.52385,   0.52387,    563.75,    233.14,         1,         0.0
simulation,         7,       0.1,  0.099992,  0.099991,    12.166,    12.166,         0,      4.7282
simulation,         8,       0.2,   0.20005,   0.20005,    13.236,    13.236,         0,      4.8147
simulation,         9,       0.3,   0.30005,   0.30005,    14.632,    14.632,         0,      4.9093
simulation,        10,       0.4,   0.39999,   0.39999,    16.943,    16.943,         0,      4.9923
simulation,        11,       0.5,   0.49996,   0.49997,    24.237,    24.237,         0,      5.0531
