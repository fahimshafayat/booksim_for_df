wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_G_restricted_src_only;
seed = 83;
shift_by_group = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49987,   0.49988,    19.126,    19.126,         0,      4.6449
simulation,         2,      0.74,   0.53315,   0.58069,    1176.8,    676.21,         1,         0.0
simulation,         3,      0.62,   0.55482,   0.56795,    815.14,    397.49,         1,         0.0
simulation,         4,      0.56,   0.55987,   0.55987,    37.292,    37.301,         0,      4.6958
simulation,         5,      0.59,   0.55803,    0.5574,    1080.7,     312.7,         1,         0.0
simulation,         6,      0.57,   0.55988,   0.55971,    517.57,    206.72,         1,         0.0
simulation,         7,       0.1,  0.099857,  0.099855,    11.239,    11.239,         0,      4.4079
simulation,         8,       0.2,   0.19995,   0.19995,    12.049,    12.049,         0,      4.4492
simulation,         9,       0.3,   0.29998,   0.29998,    13.121,    13.121,         0,      4.4839
simulation,        10,       0.4,   0.39988,   0.39988,    15.099,    15.099,         0,      4.5634
simulation,        11,       0.5,   0.49987,   0.49988,    19.126,    19.126,         0,      4.6449
