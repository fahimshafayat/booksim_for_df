wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_L_restricted_src_only;
seed = 83;
shift_by_group = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.48672,   0.48695,    597.28,    238.03,         1,         0.0
simulation,         2,      0.25,      0.25,   0.24999,    38.294,    38.318,         0,      4.4574
simulation,         3,      0.37,      0.37,   0.36999,    42.141,    42.188,         0,      4.6385
simulation,         4,      0.43,      0.43,      0.43,    44.809,    44.875,         0,       4.694
simulation,         5,      0.46,   0.45994,   0.45997,    48.419,    48.501,         0,      4.7164
simulation,         6,      0.48,   0.47894,   0.47913,    159.57,    93.088,         0,      4.7311
simulation,         7,      0.49,   0.48424,   0.48423,    523.82,     162.1,         1,         0.0
simulation,         8,       0.1,  0.099857,  0.099855,    11.085,    11.085,         0,      4.3215
simulation,         9,       0.2,   0.19994,   0.19995,    12.412,    12.412,         0,      4.3891
simulation,        10,       0.3,   0.29998,   0.29998,    41.109,    41.144,         0,      4.5484
simulation,        11,       0.4,   0.39988,   0.39988,    43.161,    43.215,         0,      4.6679
