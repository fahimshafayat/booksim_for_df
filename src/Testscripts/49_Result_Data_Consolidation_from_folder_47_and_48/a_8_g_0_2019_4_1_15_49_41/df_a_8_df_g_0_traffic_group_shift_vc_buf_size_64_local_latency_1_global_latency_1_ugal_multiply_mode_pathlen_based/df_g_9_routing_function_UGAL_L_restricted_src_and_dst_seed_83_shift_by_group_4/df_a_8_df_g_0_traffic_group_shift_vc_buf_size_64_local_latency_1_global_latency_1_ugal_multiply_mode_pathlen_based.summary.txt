wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 83;
shift_by_group = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46177,   0.47497,    678.16,    428.71,         1,         0.0
simulation,         2,      0.25,   0.24999,   0.24999,    40.733,    40.758,         0,      4.5249
simulation,         3,      0.37,   0.36983,    0.3699,    75.727,    73.772,         0,      4.7208
simulation,         4,      0.43,   0.42376,   0.42478,    504.86,    256.91,         1,         0.0
simulation,         5,       0.4,   0.39759,   0.39764,     483.6,    193.69,         0,      4.7512
simulation,         6,      0.41,   0.40646,   0.40665,    488.66,    203.97,         1,         0.0
simulation,         7,       0.1,  0.099857,  0.099855,    11.194,    11.194,         0,      4.3687
simulation,         8,       0.2,   0.19995,   0.19995,     12.72,     12.72,         0,      4.4377
simulation,         9,       0.3,   0.29997,   0.29998,    42.908,    42.944,         0,      4.6229
simulation,        10,       0.4,   0.39759,   0.39764,     483.6,    193.69,         0,      4.7512
