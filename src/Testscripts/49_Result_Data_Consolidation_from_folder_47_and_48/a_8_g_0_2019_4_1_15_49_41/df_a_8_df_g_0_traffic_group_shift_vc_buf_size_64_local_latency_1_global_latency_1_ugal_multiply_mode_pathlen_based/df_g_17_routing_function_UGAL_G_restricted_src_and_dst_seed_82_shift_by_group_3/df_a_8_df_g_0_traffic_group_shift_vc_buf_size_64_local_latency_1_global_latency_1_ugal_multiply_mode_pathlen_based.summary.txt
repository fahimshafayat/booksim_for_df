wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 82;
shift_by_group = 3;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50002,   0.50002,    27.318,    27.319,         0,      5.2732
simulation,         2,      0.74,   0.44971,   0.49689,    1708.6,    853.65,         1,         0.0
simulation,         3,      0.62,   0.50153,   0.53624,    734.36,     515.3,         1,         0.0
simulation,         4,      0.56,    0.5159,   0.52577,    589.32,    331.67,         1,         0.0
simulation,         5,      0.53,   0.52272,   0.52256,    490.47,    151.91,         1,         0.0
simulation,         6,      0.51,   0.51002,   0.51001,    30.973,    30.976,         0,      5.2801
simulation,         7,      0.52,   0.52005,   0.52004,    39.239,    39.244,         0,      5.2876
simulation,         8,       0.1,    0.1001,    0.1001,    12.492,    12.492,         0,      4.8701
simulation,         9,       0.2,   0.20002,   0.20002,    13.691,    13.691,         0,      4.9756
simulation,        10,       0.3,   0.30017,   0.30017,    15.412,    15.412,         0,      5.1064
simulation,        11,       0.4,   0.40011,   0.40011,    18.141,    18.142,         0,      5.2051
simulation,        12,       0.5,   0.50002,   0.50002,    27.318,    27.319,         0,      5.2732
