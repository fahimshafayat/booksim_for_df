wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_G;
seed = 83;
shift_by_group = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49987,   0.49988,    28.805,    28.806,         0,      5.6759
simulation,         2,      0.74,   0.47311,   0.52195,    1569.8,    842.28,         1,         0.0
simulation,         3,      0.62,   0.50769,   0.54155,    701.24,    503.44,         1,         0.0
simulation,         4,      0.56,   0.55987,   0.55987,    61.108,    61.122,         0,      5.7587
simulation,         5,      0.59,   0.51919,   0.54831,    449.76,    364.98,         1,         0.0
simulation,         6,      0.57,     0.529,   0.53888,    531.54,    302.59,         1,         0.0
simulation,         7,       0.1,  0.099858,  0.099855,    12.446,    12.446,         0,       4.949
simulation,         8,       0.2,   0.19995,   0.19995,    13.665,    13.665,         0,      5.0326
simulation,         9,       0.3,   0.29998,   0.29998,    16.204,    16.204,         0,      5.2329
simulation,        10,       0.4,   0.39988,   0.39988,    20.444,    20.444,         0,       5.502
simulation,        11,       0.5,   0.49987,   0.49988,    28.805,    28.806,         0,      5.6759
