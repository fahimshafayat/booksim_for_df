wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_G;
seed = 81;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49996,   0.49997,     38.12,    38.119,         0,      6.0761
simulation,         2,      0.74,   0.41277,   0.46114,    1782.4,    943.85,         1,         0.0
simulation,         3,      0.62,   0.46034,   0.50233,    982.98,    665.04,         1,         0.0
simulation,         4,      0.56,   0.49145,   0.51653,    421.55,    352.24,         1,         0.0
simulation,         5,      0.53,   0.50898,   0.50859,    811.48,    256.38,         1,         0.0
simulation,         6,      0.51,   0.50998,      0.51,    43.726,    43.725,         0,      6.0848
simulation,         7,      0.52,   0.51997,      0.52,    53.657,    53.654,         0,      6.0945
simulation,         8,       0.1,  0.099992,  0.099991,     13.63,     13.63,         0,      5.3636
simulation,         9,       0.2,   0.20005,   0.20005,    15.438,    15.438,         0,      5.5626
simulation,        10,       0.3,   0.30005,   0.30005,    18.279,    18.279,         0,      5.8283
simulation,        11,       0.4,   0.39999,   0.39999,    22.531,    22.531,         0,      5.9778
simulation,        12,       0.5,   0.49996,   0.49997,     38.12,    38.119,         0,      6.0761
