wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = PAR_four_hop_restricted;
seed = 83;
shift_by_group = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46044,    0.4701,    592.06,    347.27,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24999,    12.491,    12.491,         0,      4.5548
simulation,         3,      0.37,      0.37,   0.36999,    28.838,    28.919,         0,      4.6054
simulation,         4,      0.43,   0.42998,      0.43,    64.724,    64.936,         0,       4.691
simulation,         5,      0.46,   0.45037,   0.45033,    455.93,     199.6,         1,         0.0
simulation,         6,      0.44,   0.43997,   0.43994,    82.422,    82.712,         0,      4.6957
simulation,         7,      0.45,   0.44627,   0.44638,    427.93,    149.81,         1,         0.0
simulation,         8,       0.1,  0.099857,  0.099855,    11.482,    11.482,         0,      4.5511
simulation,         9,       0.2,   0.19995,   0.19995,    12.043,    12.043,         0,       4.546
simulation,        10,       0.3,   0.29997,   0.29998,    14.349,    14.353,         0,      4.5677
simulation,        11,       0.4,   0.39988,   0.39988,    39.135,    39.226,         0,      4.6485
