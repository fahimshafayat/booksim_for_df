wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 50;
five_hop_percentage = 70;
routing_function = UGAL_L_four_hop_some_five_hop_restricted;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46764,   0.47722,    548.37,    327.55,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24998,    40.874,    40.899,         0,      4.5017
simulation,         3,      0.37,   0.36976,   0.37001,    117.63,     101.8,         0,      4.6954
simulation,         4,      0.43,   0.42016,   0.42052,    563.99,    286.73,         1,         0.0
simulation,         5,       0.4,   0.39607,   0.39623,    526.19,    265.73,         1,         0.0
simulation,         6,      0.38,   0.37905,   0.37949,    242.15,    180.09,         0,      4.7066
simulation,         7,      0.39,   0.38825,    0.3885,    449.77,    257.32,         0,      4.7167
simulation,         8,       0.1,   0.10003,   0.10003,    11.153,    11.153,         0,      4.3479
simulation,         9,       0.2,   0.19998,   0.19998,    12.691,    12.692,         0,       4.419
simulation,        10,       0.3,   0.30012,   0.30011,    42.893,    42.933,         0,      4.5977
