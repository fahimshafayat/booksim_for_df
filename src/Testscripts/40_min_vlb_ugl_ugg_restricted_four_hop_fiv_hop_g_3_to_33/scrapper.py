from os import listdir
from os.path import isfile, join
import re
import operator

g_features_we_care = [ "df_a", "df_g", "topology", "df_arrangement", "vc_buf_size", "internal_speedup", "warmup_periods", "sample_period" , "seed",  "traffic",  "perm_seed", "df_wc_seed", "shift_by_group", "routing_function", "multitiered_routing_threshold_0", "multitiered_routing_threshold_1", "ugal_multiply_mode", "global_latency", "local_latency"]
    
g_result_features_we_care = ["i-rate in config file", "i-rate injected", "i-rate accepted", "packet latency", "flit latency", "deadlock?"]

g_int_features = {"df_a", "df_g","vc_buf_size",  "warmup_periods", "sample_period" , "seed", "perm_seed", "df_wc_seed", "shift_by_group", "routing_threshold", "multitiered_routing_threshold_0", "multitiered_routing_threshold_1", "global_latency", "local_latency"}

def find_index_in_list(item, lst):
    for idx,element in enumerate(lst):
        if element == item:
            return idx

def processFile(fileName):
    
    fp = open(fileName)
    #print(fileName)
    
    '''
    Fields to grab (and how the values look):
        
        vc_buf_size = 64;
        internal_speedup = 2.0;
        df_a = 16;
        df_arrangement = absolute_improved;
        seed = 11;
        topology = dragonflyfull;
        traffic = randperm;
        df_g = 21;
        perm_seed = 42;
        routing_function = UGAL_L;
        saturation point (i_rate in config file, i_rate injected, i_rate accepted, flit latency, deadlock)
    '''    
    
    
    values = {}
    
    for feature in g_features_we_care:
        values[feature] = ""
        if feature in g_int_features:
            values[feature] = -1
        
    values["saturationPoint"] = 0.0
    values["irate_vs_packet_latency"] = []
    values["irate_vs_flit_latency"] = []
    
    results = []
    
    for line in fp:
        line = line.strip()
        
        for feature in g_features_we_care:
            if line.startswith(feature + " = "):
                values[feature] = line[line.find(" = ") + len(" = ") : line.find(";")]
            if feature in g_int_features:
                values[feature] = int(values[feature])
            
        if line.startswith("simulation,"):
            temp = line.split(",")
            temp = temp[2:8]
            temp = [float(t) for t in temp]
            results.append(temp)
    
    fp.close()
    
    #find the saturation point
    #    logic: this is kinda tricky, but works.
    #    First we sort the array, by default it will be sorted based on i-rate.
    #    Then we sort again based on deadlock, in reverse. So all deadlocked configs will be on top.
    #    and non-deadlock i-rates will be on the bottom. Becasue python sort is stable, it will
    #    still be sorted based on i-rate. So the last one will be the largest i-rate that is not deadloacked.
    #    Hence, this is the saturation point.
    
    results.sort()
    results.sort(key = operator.itemgetter( find_index_in_list("deadlock?", g_result_features_we_care) ), reverse = True)
        #inefficinet, to be honest. Sorting twice is unnecessary. 
        #But python sort is stable, so taking advantage of the trick to keep the code short.
    saturationPoint = results[len(results)-1][0]
    values["saturationPoint"] = saturationPoint
    
    #save the latencies when the network is not saturated
        #the resutls are already sorted with all the saturated points coming first
        #so just find the first occurance of unsaturated point and save from there
    for idx in range(len(results)):
        if results[idx][find_index_in_list("deadlock?", g_result_features_we_care)] == 0:
            break
    
    for ii in range(idx,len(results)):
        values["irate_vs_packet_latency"].append((results[ii][find_index_in_list("i-rate in config file", g_result_features_we_care)], results[ii][find_index_in_list("packet latency", g_result_features_we_care)]))
    
    for ii in range(idx,len(results)):
        values["irate_vs_flit_latency"].append((results[ii][find_index_in_list("i-rate in config file", g_result_features_we_care)], results[ii][find_index_in_list("flit latency", g_result_features_we_care)]))
    
    
    #print(values)
    #print(results)
    
    #for result in results:
    #    print(result)    
    
    return values
        
    pass


def get_files():
    dirs = [d for d in listdir() if not isfile(d)]

    files = []

    for d in dirs:
        folders = [x for x in listdir(d) if not isfile(x)]
        for folder in folders:
            tempFiles = listdir(join(d,folder)) 
            files.extend(join(d,folder,f) for f in tempFiles if ".out" not in f)
    
    #    for idx,f in enumerate(files):
    #        print(idx,f)
    #    
    
    return files
    

def get_files_two_layers():
    dirs = [d for d in listdir() if not isfile(d)]

    files = []

    for d in dirs:
        dir2s = [x for x in listdir(d) if not isfile(x)]
        for dir2 in dir2s:
            tempFiles = listdir(join(d,dir2)) 
            files.extend(join(d,dir2,f) for f in tempFiles if ".out" not in f)

    #    for idx,f in enumerate(files):
    #        print(idx,f)
    #    
    
    return files


def get_files_three_layers():
    ToIgnore = set(["Ignore"])
    
    dirs = [d for d in listdir() if not isfile(d) and d not in ToIgnore]

    files = []

    for d in dirs:
        dir2s = [x for x in listdir(d) if not isfile(x)]
        for dir2 in dir2s:
            dir3s = [y for y in listdir(join(d,dir2)) if not isfile(join(d,dir2))]
            for dir3 in dir3s:
                tempFiles = listdir(join(d,dir2,dir3)) 
                #files.extend(join(d,dir2,dir3,f) for f in tempFiles if ".out" not in f)
                files.extend(join(d,dir2,dir3,f) for f in tempFiles if "summary.txt" in f)

    #    for idx,f in enumerate(files):
    #        print(idx,f)
    #    
    
    return files

def grab_results(files):
    results = []
    for f in files:
        values = processFile(f)
        results.append(values)
    return results
    
def get_index_of_feature(feature, feature_list = None):
    if feature_list == None:
        feature_list = g_features_we_care
    for idx in range(len(g_features_we_care)):
        if g_features_we_care[idx] == feature:
            return idx
    return -1   #feature not found
    
    
def pretty_print(results):
    #results is a list of dict
    #let's make it a list of list for easy sorting
    #because we are discarding dict keys, so the order becomes important

    resultList = []
    
    for values in results:
        
        listValues = []
         
        for feature in g_features_we_care:
            listValues.append(values[feature])
        listValues.append(values["saturationPoint"])
        listValues.append(values["irate_vs_packet_latency"])
        listValues.append(values["irate_vs_flit_latency"])
        
        resultList.append(listValues)

    #now sort according to our preference

    #first by traffic, then by df_g, then by seed, then by perm_seed, then by routing_function, then by routing_threshold
    
    #Implementation detail: need to follow the sorting feature order in reverse.(because python sort is stable!)
    
    #the correct index of each feature is important. Get that from g_features_we_care up top.

    
    #sort based on routing_threshold
    resultList.sort(key=operator.itemgetter(get_index_of_feature("routing_threshold")))
    
    #sort based on routing_threshold_1
    resultList.sort(key=operator.itemgetter(get_index_of_feature("multitiered_routing_threshold_1")))
    
    #sort based on routings. Order should be UGAL_L, UGAL_L_two_hop 
    order = {"min": 0, "min_djkstra": 1, "vlb":3, "vlb_restricted" : 4, "vlb_four_hop_restricted" : 5,  "UGAL_L":11, "UGAL_L_two_hop":12, "UGAL_L_threshold":13, "UGAL_L_multi_tiered" : 14, "UGAL_L_restricted": 15,  "UGAL_L_four_hop_restricted" : 16,  "PAR" : 20, "PAR_multi_tiered" : 21, "UGAL_G" : 30, "UGAL_G_restricted" : 31, "UGAL_G_four_hop_restricted" : 32 }
    resultList.sort(key = lambda x:order[x[get_index_of_feature("routing_function")]])

    #by perm_seed
    resultList.sort(key=operator.itemgetter(get_index_of_feature("perm_seed")))
    
    #by df_wc_seed
    resultList.sort(key=operator.itemgetter(get_index_of_feature("df_wc_seed")))    

    #by shift_by_group
    resultList.sort(key=operator.itemgetter(get_index_of_feature("shift_by_group")))   

    #by seed
    resultList.sort(key=operator.itemgetter(get_index_of_feature("seed")))

    #by df_g
    resultList.sort(key=operator.itemgetter(get_index_of_feature("df_g")))

    #by traffic
    resultList.sort(key=operator.itemgetter(get_index_of_feature("traffic")))

    resultList.sort(key=operator.itemgetter(get_index_of_feature("global_latency")))
    
    resultList.sort(key=operator.itemgetter(get_index_of_feature("local_latency")))


    #print header
    for feature in g_features_we_care:
        print(feature, end = " , ")
    print("saturationPoint", end = " , ") 
    print("irate_vs_packet_latency", end = " , ")
    print("irate_vs_flit_latency")
    

    for idx,result in enumerate(resultList):
        #print(result)
        #only the last three columns (saturationPoint, and irate_vs_packet_latency, irate_vs_flit_latency) needs careful formatting.
        
        #the rest can be printed as is.
        for ii in range(len(result)-3):
            print(result[ii], end=" , ")
        
        #saturationPoint
        print("{:.2f}".format(result[len(result)-3]), end=" , ")
        
        #irate_vs_packet_latency
        temp_list = ["->".join([str(x) for x in tup]) for tup in result[len(result)-2]]
        irate_vs_packet_latency = " | ".join(temp_list)
        print(irate_vs_packet_latency, end = " , ")
        
        #irate_vs_flit_latency
        temp_list = ["->".join([str(x) for x in tup]) for tup in result[len(result)-1]]
        irate_vs_flit_latency = " | ".join(temp_list)
        print(irate_vs_flit_latency)
        
        #set here how you want your indentation.
        if (idx+1) % 11 == 0 :
            print("")
        
    pass

if __name__ == "__main__":

    files = get_files_three_layers()
    #files = get_files_two_layers()
        
    results = grab_results(files)
    
#    for row in results:
#        print(row)
#        
    pretty_print(results)


