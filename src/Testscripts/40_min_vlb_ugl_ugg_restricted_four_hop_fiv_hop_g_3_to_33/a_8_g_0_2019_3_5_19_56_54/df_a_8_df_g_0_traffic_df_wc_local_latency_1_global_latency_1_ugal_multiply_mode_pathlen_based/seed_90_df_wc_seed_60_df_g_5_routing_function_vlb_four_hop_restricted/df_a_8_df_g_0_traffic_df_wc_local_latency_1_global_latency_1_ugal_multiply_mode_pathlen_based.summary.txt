vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 5;
df_wc_seed = 60;
routing_function = vlb_four_hop_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.36768,    0.4773,    693.21,    691.93,         1,         0.0
simulation,         2,      0.25,   0.25021,   0.25021,    13.708,    13.709,         0,      4.7503
simulation,         3,      0.37,   0.36998,   0.37022,     79.68,    79.862,         0,      4.7506
simulation,         4,      0.43,   0.37014,   0.42058,     719.3,    705.69,         1,         0.0
simulation,         5,       0.4,   0.37201,   0.39402,    891.15,    872.01,         1,         0.0
simulation,         6,      0.38,   0.37158,   0.37985,    555.71,    547.61,         1,         0.0
simulation,         7,       0.1,   0.10047,   0.10047,    11.944,    11.944,         0,      4.7487
simulation,         8,       0.2,   0.20031,   0.20032,    12.815,    12.815,         0,      4.7498
simulation,         9,       0.3,   0.30003,   0.30002,    15.656,    15.658,         0,      4.7496
