vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 5;
df_wc_seed = 60;
routing_function = UGAL_L_four_hop_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50005,   0.50007,     54.02,    54.109,         0,      4.2699
simulation,         2,      0.74,   0.58656,   0.68047,     492.1,    453.99,         1,         0.0
simulation,         3,      0.62,   0.58654,   0.60615,    620.02,    601.83,         1,         0.0
simulation,         4,      0.56,   0.56022,   0.56022,    63.102,    63.265,         0,      4.3119
simulation,         5,      0.59,   0.59021,   0.59024,    74.494,    74.746,         0,       4.329
simulation,         6,       0.6,   0.60029,   0.60026,    83.691,    83.987,         0,      4.3332
simulation,         7,      0.61,   0.61007,   0.61035,    107.86,    108.37,         0,      4.3342
simulation,         8,       0.1,   0.10047,   0.10047,    10.447,    10.447,         0,      4.0884
simulation,         9,       0.2,   0.20031,   0.20032,    10.917,    10.917,         0,      4.1243
simulation,        10,       0.3,   0.30003,   0.30002,    11.644,    11.644,         0,      4.1369
simulation,        11,       0.4,   0.40014,   0.40013,    16.183,    16.186,         0,      4.1706
simulation,        12,       0.5,   0.50005,   0.50007,     54.02,    54.109,         0,      4.2699
simulation,        13,       0.6,   0.60029,   0.60026,    83.691,    83.987,         0,      4.3332
