vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 5;
df_wc_seed = 60;
routing_function = UGAL_G_four_hop_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50009,   0.50007,    14.535,    14.535,         0,      4.1863
simulation,         2,      0.74,   0.57446,   0.62707,     954.2,    632.98,         1,         0.0
simulation,         3,      0.62,   0.62025,   0.62025,    26.642,    26.644,         0,      4.2699
simulation,         4,      0.68,   0.60088,   0.64763,    480.71,    434.77,         1,         0.0
simulation,         5,      0.65,    0.6179,   0.62636,    546.52,    482.58,         1,         0.0
simulation,         6,      0.63,   0.62445,    0.6259,    314.14,    223.45,         0,      4.2654
simulation,         7,      0.64,   0.62223,    0.6261,    504.56,    375.91,         1,         0.0
simulation,         8,       0.1,   0.10047,   0.10047,    10.545,    10.545,         0,      4.1337
simulation,         9,       0.2,   0.20032,   0.20032,    10.982,    10.982,         0,      4.1421
simulation,        10,       0.3,   0.30002,   0.30002,    11.625,    11.626,         0,      4.1491
simulation,        11,       0.4,   0.40013,   0.40013,    12.596,    12.596,         0,      4.1562
simulation,        12,       0.5,   0.50009,   0.50007,    14.535,    14.535,         0,      4.1863
simulation,        13,       0.6,   0.60026,   0.60026,     20.51,    20.511,         0,      4.2557
