vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 25;
df_wc_seed = 60;
routing_function = UGAL_G_four_hop_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35938,   0.43003,    500.84,     477.4,         1,         0.0
simulation,         2,      0.25,   0.25002,   0.25004,    13.708,    13.708,         0,      4.5993
simulation,         3,      0.37,   0.35178,   0.35211,    378.07,    291.55,         1,         0.0
simulation,         4,      0.31,   0.30997,   0.30998,    15.614,    15.615,         0,      4.6393
simulation,         5,      0.34,   0.33997,   0.33998,    20.759,    20.763,         0,      4.6585
simulation,         6,      0.35,   0.34694,   0.34694,    391.89,    99.761,         0,      4.6576
simulation,         7,      0.36,     0.349,   0.34918,    470.41,    214.05,         1,         0.0
simulation,         8,       0.1,    0.0999,  0.099902,    11.836,    11.836,         0,      4.4943
simulation,         9,       0.2,   0.19996,   0.19997,    12.896,    12.896,         0,      4.5632
simulation,        10,       0.3,   0.29998,       0.3,    15.105,    15.106,         0,       4.633
