vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 25;
df_wc_seed = 60;
routing_function = min;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,  0.044928,   0.13911,    2166.4,    2166.1,         1,         0.0
simulation,         2,      0.25,  0.044922,   0.13913,    1828.3,    1828.3,         1,         0.0
simulation,         3,      0.13,  0.044917,   0.12095,    1227.8,    1227.8,         1,         0.0
simulation,         4,      0.07,   0.04489,  0.070201,    610.03,    610.03,         1,         0.0
simulation,         5,      0.04,  0.035074,  0.040037,    520.11,    520.11,         1,         0.0
simulation,         6,      0.02,   0.01997,   0.01997,    10.095,    10.095,         0,      3.7496
simulation,         7,      0.03,  0.029983,  0.029986,    16.503,    16.503,         0,      3.7502
simulation,         8,      0.02,   0.01997,   0.01997,    10.095,    10.095,         0,      3.7496
