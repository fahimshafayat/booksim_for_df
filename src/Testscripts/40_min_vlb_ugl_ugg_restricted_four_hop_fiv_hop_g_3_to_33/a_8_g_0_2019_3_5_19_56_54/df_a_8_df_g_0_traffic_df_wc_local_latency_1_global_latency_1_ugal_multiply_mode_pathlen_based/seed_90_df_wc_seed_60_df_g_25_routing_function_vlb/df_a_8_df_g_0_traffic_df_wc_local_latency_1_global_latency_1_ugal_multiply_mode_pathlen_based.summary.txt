vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 25;
df_wc_seed = 60;
routing_function = vlb;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35601,   0.45874,    707.21,    701.74,         1,         0.0
simulation,         2,      0.25,   0.25003,   0.25004,    18.425,    18.425,         0,         6.5
simulation,         3,      0.37,   0.35598,   0.36873,    604.87,     633.7,         1,         0.0
simulation,         4,      0.31,   0.30996,   0.30998,    22.467,    22.467,         0,      6.4999
simulation,         5,      0.34,   0.33997,   0.33998,    31.255,    31.262,         0,         6.5
simulation,         6,      0.35,   0.34987,   0.34991,    45.414,    45.443,         0,      6.4998
simulation,         7,      0.36,   0.35618,   0.35984,    361.84,    372.66,         0,      6.5002
simulation,         8,       0.1,  0.099896,  0.099902,    15.709,    15.709,         0,      6.4992
simulation,         9,       0.2,   0.19996,   0.19997,    17.082,    17.082,         0,      6.4996
simulation,        10,       0.3,   0.29999,       0.3,    21.347,    21.347,         0,      6.4998
