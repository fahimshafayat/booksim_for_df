vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 3;
df_wc_seed = 60;
routing_function = min_djkstra;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.40374,   0.42351,    618.37,    387.86,         1,         0.0
simulation,         2,      0.25,   0.25006,   0.25027,    51.903,     52.06,         0,      2.6251
simulation,         3,      0.37,   0.32469,   0.34222,    470.49,    385.94,         1,         0.0
simulation,         4,      0.31,    0.2874,   0.28753,    795.21,    606.66,         1,         0.0
simulation,         5,      0.28,   0.26869,   0.27909,    472.25,    471.83,         1,         0.0
simulation,         6,      0.26,   0.25655,   0.25971,    488.82,     487.0,         0,      2.6253
simulation,         7,      0.27,   0.26251,   0.26863,    496.38,    525.86,         1,         0.0
simulation,         8,      0.05,  0.050315,  0.050321,    7.3144,    7.3143,         0,      2.6246
simulation,         9,       0.1,   0.10056,   0.10056,    7.4096,    7.4097,         0,      2.6242
simulation,        10,      0.15,   0.15066,   0.15065,    7.5782,    7.5784,         0,      2.6247
simulation,        11,       0.2,   0.20055,   0.20056,    7.9929,    7.9928,         0,      2.6252
simulation,        12,      0.25,   0.25006,   0.25027,    51.903,     52.06,         0,      2.6251
