vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 9;
df_wc_seed = 60;
routing_function = min;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.12477,   0.22529,    1867.4,    1862.8,         1,         0.0
simulation,         2,      0.25,   0.12476,   0.22461,    1226.8,    1226.8,         1,         0.0
simulation,         3,      0.13,     0.125,   0.13013,    515.92,    515.92,         1,         0.0
simulation,         4,      0.07,   0.06986,  0.069864,    10.167,    10.167,         0,      3.7495
simulation,         5,       0.1,  0.099918,   0.09993,    11.511,    11.512,         0,      3.7493
simulation,         6,      0.11,   0.10993,   0.10994,    13.145,    13.146,         0,      3.7521
simulation,         7,      0.12,   0.11996,   0.11997,    20.725,    20.724,         0,      3.7504
simulation,         8,      0.05,  0.049844,  0.049849,     9.858,    9.8581,         0,      3.7501
simulation,         9,       0.1,  0.099918,   0.09993,    11.511,    11.512,         0,      3.7493
