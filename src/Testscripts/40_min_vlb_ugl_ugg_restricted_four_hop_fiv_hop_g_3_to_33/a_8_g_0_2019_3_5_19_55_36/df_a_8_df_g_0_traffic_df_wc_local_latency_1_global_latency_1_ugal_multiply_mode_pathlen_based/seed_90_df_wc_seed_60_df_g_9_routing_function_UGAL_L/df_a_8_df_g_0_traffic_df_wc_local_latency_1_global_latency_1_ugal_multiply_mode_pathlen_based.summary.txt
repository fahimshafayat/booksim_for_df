vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 9;
df_wc_seed = 60;
routing_function = UGAL_L;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35608,   0.44847,    592.44,    577.45,         1,         0.0
simulation,         2,      0.25,   0.25016,   0.25019,    52.279,    52.359,         0,       5.119
simulation,         3,      0.37,   0.35319,   0.36627,    521.11,    512.29,         1,         0.0
simulation,         4,      0.31,   0.30997,   0.30999,    62.701,    62.891,         0,       5.375
simulation,         5,      0.34,   0.33953,    0.3399,    127.55,    128.57,         0,      5.4677
simulation,         6,      0.35,   0.34504,    0.3489,    469.34,     440.8,         1,         0.0
simulation,         7,       0.1,  0.099925,   0.09993,    12.009,    12.009,         0,      4.7252
simulation,         8,       0.2,   0.20004,   0.20005,    19.157,    19.164,         0,      4.8245
simulation,         9,       0.3,   0.29994,   0.29995,    60.093,    60.254,         0,      5.3378
