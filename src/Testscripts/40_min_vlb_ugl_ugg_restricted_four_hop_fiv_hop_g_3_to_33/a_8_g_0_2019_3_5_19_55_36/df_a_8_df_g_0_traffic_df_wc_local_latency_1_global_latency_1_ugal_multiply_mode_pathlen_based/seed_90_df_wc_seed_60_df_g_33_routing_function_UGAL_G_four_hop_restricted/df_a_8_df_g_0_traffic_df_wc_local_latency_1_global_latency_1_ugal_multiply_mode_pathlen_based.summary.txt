vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
df_g = 33;
df_wc_seed = 60;
routing_function = UGAL_G_four_hop_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.37155,   0.43189,    402.28,     381.6,         1,         0.0
simulation,         2,      0.25,   0.25001,   0.25002,    13.855,    13.855,         0,      4.7263
simulation,         3,      0.37,   0.35662,   0.35694,    431.45,     200.3,         1,         0.0
simulation,         4,      0.31,   0.30996,   0.30998,    15.527,    15.528,         0,      4.7567
simulation,         5,      0.34,   0.34001,   0.34001,    19.018,    19.021,         0,       4.771
simulation,         6,      0.35,   0.34933,    0.3493,    97.201,    42.251,         0,      4.7764
simulation,         7,      0.36,   0.35499,   0.35509,    304.09,    115.63,         1,         0.0
simulation,         8,       0.1,  0.099898,  0.099898,    12.162,    12.162,         0,      4.6086
simulation,         9,       0.2,       0.2,   0.20001,    13.148,    13.149,         0,      4.6913
simulation,        10,       0.3,   0.29997,   0.29998,    15.097,    15.098,         0,      4.7522
