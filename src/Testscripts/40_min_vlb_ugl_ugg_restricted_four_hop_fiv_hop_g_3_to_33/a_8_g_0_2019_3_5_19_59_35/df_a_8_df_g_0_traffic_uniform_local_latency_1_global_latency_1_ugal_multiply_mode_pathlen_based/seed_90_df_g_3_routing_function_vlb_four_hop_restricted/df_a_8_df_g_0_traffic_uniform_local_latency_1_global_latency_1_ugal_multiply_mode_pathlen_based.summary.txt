vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = vlb_four_hop_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50012,   0.50013,    12.234,    12.235,         0,      3.9165
simulation,         2,      0.74,   0.74017,   0.74014,    60.733,      60.9,         0,      3.9169
simulation,         3,      0.86,   0.74567,   0.79224,    650.26,    467.06,         1,         0.0
simulation,         4,       0.8,   0.74227,   0.75202,    853.77,     578.5,         1,         0.0
simulation,         5,      0.77,    0.7432,   0.74945,    557.76,    487.77,         1,         0.0
simulation,         6,      0.75,   0.74415,   0.74862,    255.66,    238.69,         0,      3.9167
simulation,         7,      0.76,   0.74301,   0.75038,     502.1,    399.31,         1,         0.0
simulation,         8,       0.1,   0.10056,   0.10056,    10.052,    10.053,         0,      3.9147
simulation,         9,       0.2,   0.20056,   0.20056,    10.347,    10.347,         0,      3.9153
simulation,        10,       0.3,   0.30022,   0.30022,    10.743,    10.743,         0,      3.9169
simulation,        11,       0.4,    0.3999,    0.3999,    11.316,    11.316,         0,      3.9165
simulation,        12,       0.5,   0.50012,   0.50013,    12.234,    12.235,         0,      3.9165
simulation,        13,       0.6,   0.60009,    0.6001,    14.075,    14.076,         0,      3.9164
simulation,        14,       0.7,   0.69998,   0.70001,    21.145,    21.151,         0,      3.9161
