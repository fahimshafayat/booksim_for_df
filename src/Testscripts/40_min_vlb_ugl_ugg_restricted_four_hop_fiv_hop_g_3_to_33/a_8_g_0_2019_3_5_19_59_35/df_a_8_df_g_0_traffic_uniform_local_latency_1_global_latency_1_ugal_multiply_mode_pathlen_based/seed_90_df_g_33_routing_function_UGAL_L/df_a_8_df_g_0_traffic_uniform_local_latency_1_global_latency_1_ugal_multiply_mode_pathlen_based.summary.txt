vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = UGAL_L;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49988,   0.49989,    12.304,    12.304,         0,      4.0675
simulation,         2,      0.74,    0.7399,   0.73991,    15.661,    15.662,         0,      3.9602
simulation,         3,      0.86,   0.85986,   0.85984,    22.064,    22.066,         0,      3.9237
simulation,         4,      0.92,    0.9199,   0.91989,    34.988,    34.995,         0,        3.91
simulation,         5,      0.95,   0.94998,   0.94991,    59.196,    59.223,         0,      3.8798
simulation,         6,      0.97,   0.97001,   0.96991,    84.144,    84.186,         0,      3.8261
simulation,         7,      0.98,   0.97966,   0.97999,    107.51,    107.67,         0,      3.7957
simulation,         8,       0.1,  0.099896,  0.099898,    10.983,    10.984,         0,      4.3587
simulation,         9,       0.2,       0.2,   0.20001,    11.211,    11.211,         0,      4.3056
simulation,        10,       0.3,   0.29997,   0.29998,    11.416,    11.417,         0,      4.2109
simulation,        11,       0.4,   0.39988,   0.39989,    11.761,    11.762,         0,      4.1319
simulation,        12,       0.5,   0.49988,   0.49989,    12.304,    12.304,         0,      4.0675
simulation,        13,       0.6,   0.59986,   0.59986,    13.186,    13.187,         0,      4.0169
simulation,        14,       0.7,   0.69991,   0.69992,    14.707,    14.708,         0,      3.9751
simulation,        15,       0.8,   0.79979,    0.7998,    17.827,    17.828,         0,      3.9402
simulation,        16,       0.9,    0.8999,   0.89987,    28.445,     28.45,         0,       3.915
