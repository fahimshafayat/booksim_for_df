vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = UGAL_G_four_hop_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50012,   0.50013,     11.04,     11.04,         0,      3.6622
simulation,         2,      0.74,   0.73996,   0.73995,    13.374,    13.375,         0,      3.6209
simulation,         3,      0.86,   0.85988,   0.85987,    16.559,    16.561,         0,      3.6008
simulation,         4,      0.92,   0.91998,   0.91998,    20.638,    20.642,         0,      3.5907
simulation,         5,      0.95,   0.94991,   0.94994,    25.502,    25.512,         0,       3.585
simulation,         6,      0.97,    0.9699,   0.96987,    33.307,     33.32,         0,      3.5803
simulation,         7,      0.98,   0.97986,    0.9799,    42.444,    42.492,         0,      3.5776
simulation,         8,       0.1,  0.099927,   0.09993,    9.6969,     9.697,         0,      3.7477
simulation,         9,       0.2,   0.20005,   0.20005,    9.9068,    9.9071,         0,      3.7263
simulation,        10,       0.3,   0.29995,   0.29995,    10.175,    10.175,         0,      3.7026
simulation,        11,       0.4,   0.40021,   0.40022,    10.546,    10.546,         0,      3.6825
simulation,        12,       0.5,   0.50012,   0.50013,     11.04,     11.04,         0,      3.6622
simulation,        13,       0.6,   0.59987,   0.59988,    11.735,    11.736,         0,      3.6444
simulation,        14,       0.7,    0.6998,   0.69979,    12.783,    12.784,         0,      3.6278
simulation,        15,       0.8,       0.8,   0.80002,    14.571,    14.572,         0,      3.6111
simulation,        16,       0.9,   0.89993,   0.89993,    18.832,    18.835,         0,      3.5932
