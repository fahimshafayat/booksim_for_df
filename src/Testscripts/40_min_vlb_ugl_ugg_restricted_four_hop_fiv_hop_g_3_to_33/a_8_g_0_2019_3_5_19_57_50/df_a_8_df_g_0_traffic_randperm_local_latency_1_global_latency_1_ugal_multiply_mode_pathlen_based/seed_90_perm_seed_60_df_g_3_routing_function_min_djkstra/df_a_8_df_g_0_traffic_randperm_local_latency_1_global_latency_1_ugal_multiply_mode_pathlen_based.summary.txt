vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 60;
routing_function = min_djkstra;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49094,   0.49096,    378.78,    58.969,         1,         0.0
simulation,         2,      0.25,   0.25054,   0.25054,    6.9396,    6.9397,         0,      2.3844
simulation,         3,      0.37,   0.36807,   0.36807,    241.04,    47.894,         0,      2.3843
simulation,         4,      0.43,   0.42514,   0.42514,    272.99,     64.72,         1,         0.0
simulation,         5,       0.4,    0.3968,   0.39686,     366.5,    51.173,         0,      2.3847
simulation,         6,      0.41,   0.40638,   0.40653,    414.09,    64.155,         0,      2.3847
simulation,         7,      0.42,   0.41578,   0.41586,    475.32,    66.738,         0,      2.3844
simulation,         8,       0.1,   0.10056,   0.10056,    6.8083,    6.8085,         0,      2.3836
simulation,         9,       0.2,   0.20055,   0.20056,    6.8761,    6.8763,         0,      2.3842
simulation,        10,       0.3,   0.30015,   0.30017,    7.3302,      7.33,         0,       2.385
simulation,        11,       0.4,    0.3968,   0.39686,     366.5,    51.173,         0,      2.3847
