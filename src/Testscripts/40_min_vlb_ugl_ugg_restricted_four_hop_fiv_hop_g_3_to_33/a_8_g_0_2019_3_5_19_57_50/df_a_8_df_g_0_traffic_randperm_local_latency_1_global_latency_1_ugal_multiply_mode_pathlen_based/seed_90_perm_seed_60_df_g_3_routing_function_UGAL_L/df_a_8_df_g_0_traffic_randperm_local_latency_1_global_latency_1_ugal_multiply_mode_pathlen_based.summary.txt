vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 60;
routing_function = UGAL_L;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50013,    10.534,    10.535,         0,      3.5915
simulation,         2,      0.74,    0.7401,   0.74014,    12.268,    12.268,         0,      3.5284
simulation,         3,      0.86,   0.86026,   0.86025,    14.949,     14.95,         0,      3.5324
simulation,         4,      0.92,   0.92029,   0.92019,    18.679,    18.686,         0,      3.5508
simulation,         5,      0.95,   0.95017,   0.95025,    25.086,    25.086,         0,      3.5667
simulation,         6,      0.97,   0.96383,   0.96382,    248.01,    71.745,         0,      3.5891
simulation,         7,      0.98,   0.96929,   0.96952,     455.3,     96.69,         0,      3.5908
simulation,         8,       0.1,   0.10056,   0.10056,    9.6749,    9.6757,         0,      3.7502
simulation,         9,       0.2,   0.20055,   0.20056,    9.8735,    9.8742,         0,      3.7402
simulation,        10,       0.3,   0.30021,   0.30022,    10.022,    10.023,         0,      3.6851
simulation,        11,       0.4,    0.3999,    0.3999,    10.222,    10.223,         0,      3.6323
simulation,        12,       0.5,   0.50014,   0.50013,    10.534,    10.535,         0,      3.5915
simulation,        13,       0.6,   0.60009,    0.6001,    11.006,    11.007,         0,      3.5564
simulation,        14,       0.7,       0.7,   0.70001,    11.796,    11.797,         0,      3.5343
simulation,        15,       0.8,   0.80013,    0.8001,    13.286,    13.286,         0,      3.5272
simulation,        16,       0.9,   0.90011,   0.90014,    16.986,    16.988,         0,      3.5421
