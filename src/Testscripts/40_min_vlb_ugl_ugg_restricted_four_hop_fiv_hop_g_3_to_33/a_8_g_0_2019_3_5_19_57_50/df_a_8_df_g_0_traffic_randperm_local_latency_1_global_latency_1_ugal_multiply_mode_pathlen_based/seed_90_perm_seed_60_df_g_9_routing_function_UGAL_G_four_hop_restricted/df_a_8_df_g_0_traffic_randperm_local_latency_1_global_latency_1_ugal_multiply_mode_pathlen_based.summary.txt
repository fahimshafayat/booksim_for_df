vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 9;
perm_seed = 60;
routing_function = UGAL_G_four_hop_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50013,   0.50013,    11.048,    11.048,         0,       3.715
simulation,         2,      0.74,   0.73994,   0.73995,    13.439,     13.44,         0,      3.6841
simulation,         3,      0.86,   0.85988,   0.85987,    18.545,    18.547,         0,      3.6889
simulation,         4,      0.92,   0.91983,   0.91998,    50.441,    50.485,         0,       3.715
simulation,         5,      0.95,   0.90893,   0.90975,    522.41,    199.43,         1,         0.0
simulation,         6,      0.93,    0.9097,   0.90983,    491.98,    160.78,         1,         0.0
simulation,         7,       0.1,   0.09993,   0.09993,     9.742,    9.7422,         0,       3.781
simulation,         8,       0.2,   0.20005,   0.20005,     9.953,    9.9532,         0,      3.7681
simulation,         9,       0.3,   0.29995,   0.29995,    10.212,    10.212,         0,      3.7482
simulation,        10,       0.4,   0.40021,   0.40022,    10.576,    10.576,         0,      3.7318
simulation,        11,       0.5,   0.50013,   0.50013,    11.048,    11.048,         0,       3.715
simulation,        12,       0.6,   0.59987,   0.59988,    11.713,    11.714,         0,      3.7001
simulation,        13,       0.7,   0.69979,   0.69979,    12.778,    12.779,         0,      3.6881
simulation,        14,       0.8,   0.80002,   0.80002,      15.0,    15.001,         0,       3.683
simulation,        15,       0.9,   0.89994,   0.89993,    26.915,    26.921,         0,      3.7012
