vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 60;
routing_function = vlb_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.37744,    0.4053,     527.4,    416.58,         1,         0.0
simulation,         2,      0.25,   0.25001,   0.25002,    15.151,    15.151,         0,      5.6866
simulation,         3,      0.37,   0.36992,   0.36993,    17.949,    17.949,         0,      5.6857
simulation,         4,      0.43,   0.33914,   0.34152,    884.71,    551.19,         1,         0.0
simulation,         5,       0.4,   0.37322,   0.37343,    397.51,    240.27,         1,         0.0
simulation,         6,      0.38,   0.37989,   0.37992,    18.426,    18.427,         0,      5.6862
simulation,         7,      0.39,    0.3795,   0.38125,    183.49,    101.85,         1,         0.0
simulation,         8,       0.1,  0.099897,  0.099898,    13.842,    13.842,         0,      5.6856
simulation,         9,       0.2,       0.2,   0.20001,    14.592,    14.592,         0,      5.6861
simulation,        10,       0.3,   0.29997,   0.29998,    15.935,    15.935,         0,      5.6865
