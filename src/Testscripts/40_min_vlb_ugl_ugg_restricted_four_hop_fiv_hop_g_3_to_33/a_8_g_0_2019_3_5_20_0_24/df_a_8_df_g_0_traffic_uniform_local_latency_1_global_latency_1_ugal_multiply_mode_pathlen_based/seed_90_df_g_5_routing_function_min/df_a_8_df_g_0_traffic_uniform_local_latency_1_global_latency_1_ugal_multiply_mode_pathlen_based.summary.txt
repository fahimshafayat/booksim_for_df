vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = min;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50006,   0.50007,    10.023,    10.023,         0,      3.3744
simulation,         2,      0.74,   0.74022,   0.74021,    12.021,    12.022,         0,      3.3742
simulation,         3,      0.86,   0.86029,   0.86027,    14.916,    14.917,         0,      3.3747
simulation,         4,      0.92,   0.92017,   0.92021,    18.681,    18.685,         0,      3.3754
simulation,         5,      0.95,      0.95,   0.95007,    23.324,    23.336,         0,       3.374
simulation,         6,      0.97,   0.97024,   0.97012,    31.126,    31.152,         0,      3.3749
simulation,         7,      0.98,   0.98014,   0.98012,     39.33,    39.376,         0,      3.3751
simulation,         8,       0.1,   0.10047,   0.10047,    8.9006,    8.9008,         0,      3.3756
simulation,         9,       0.2,   0.20031,   0.20032,    9.0847,     9.085,         0,      3.3754
simulation,        10,       0.3,   0.30002,   0.30002,    9.3179,    9.3182,         0,      3.3759
simulation,        11,       0.4,   0.40013,   0.40013,    9.6155,    9.6158,         0,      3.3735
simulation,        12,       0.5,   0.50006,   0.50007,    10.023,    10.023,         0,      3.3744
simulation,        13,       0.6,   0.60026,   0.60026,    10.612,    10.612,         0,      3.3759
simulation,        14,       0.7,   0.70012,   0.70014,    11.497,    11.497,         0,      3.3749
simulation,        15,       0.8,   0.80032,   0.80034,    13.115,    13.116,         0,      3.3754
simulation,        16,       0.9,   0.90026,   0.90029,      17.1,    17.101,         0,       3.374
