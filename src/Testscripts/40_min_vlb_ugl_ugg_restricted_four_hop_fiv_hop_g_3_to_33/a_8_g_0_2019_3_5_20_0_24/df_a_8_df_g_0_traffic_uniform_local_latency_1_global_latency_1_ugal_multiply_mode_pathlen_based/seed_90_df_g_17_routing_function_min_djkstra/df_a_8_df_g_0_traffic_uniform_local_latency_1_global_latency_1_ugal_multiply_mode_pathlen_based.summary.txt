vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = min_djkstra;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49993,   0.49994,    10.282,    10.282,         0,      3.4336
simulation,         2,      0.74,   0.74028,   0.74027,    12.715,    12.716,         0,      3.4341
simulation,         3,      0.86,   0.86009,   0.86008,    16.552,    16.553,         0,      3.4338
simulation,         4,      0.92,   0.92009,   0.92006,    21.931,    21.936,         0,      3.4337
simulation,         5,      0.95,   0.94998,      0.95,    28.331,    28.343,         0,      3.4339
simulation,         6,      0.97,    0.9699,   0.96992,    38.141,     38.16,         0,      3.4336
simulation,         7,      0.98,   0.98013,   0.97993,    48.767,    48.807,         0,      3.4343
simulation,         8,       0.1,   0.09991,  0.099915,    9.0267,    9.0269,         0,      3.4339
simulation,         9,       0.2,   0.19997,   0.19998,    9.2286,    9.2287,         0,      3.4342
simulation,        10,       0.3,   0.29996,   0.29997,    9.4804,    9.4806,         0,      3.4331
simulation,        11,       0.4,   0.39991,   0.39992,    9.8187,    9.8189,         0,      3.4341
simulation,        12,       0.5,   0.49993,   0.49994,    10.282,    10.282,         0,      3.4336
simulation,        13,       0.6,   0.60001,   0.60001,    10.963,    10.963,         0,      3.4337
simulation,        14,       0.7,   0.70018,   0.70018,     12.06,    12.061,         0,      3.4337
simulation,        15,       0.8,   0.80022,   0.80021,    14.126,    14.126,         0,      3.4339
simulation,        16,       0.9,   0.90004,   0.90004,    19.491,    19.494,         0,      3.4338
