vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_L_four_hop_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50007,   0.50007,    10.729,     10.73,         0,      3.5917
simulation,         2,      0.74,   0.74021,   0.74021,    12.772,    12.772,         0,      3.5473
simulation,         3,      0.86,   0.86028,   0.86027,    15.563,    15.564,         0,      3.5262
simulation,         4,      0.92,   0.92019,   0.92021,    19.217,     19.22,         0,      3.5147
simulation,         5,      0.95,   0.94999,   0.95007,    23.765,    23.773,         0,      3.5098
simulation,         6,      0.97,   0.97014,   0.97012,    31.006,    31.024,         0,      3.5068
simulation,         7,      0.98,   0.98012,   0.98012,    38.786,     38.82,         0,      3.5035
simulation,         8,       0.1,   0.10047,   0.10047,    9.4179,     9.418,         0,      3.6186
simulation,         9,       0.2,   0.20031,   0.20032,    9.7133,    9.7136,         0,      3.6478
simulation,        10,       0.3,   0.30001,   0.30002,    9.9734,    9.9737,         0,      3.6334
simulation,        11,       0.4,   0.40014,   0.40013,    10.299,    10.299,         0,      3.6127
simulation,        12,       0.5,   0.50007,   0.50007,    10.729,     10.73,         0,      3.5917
simulation,        13,       0.6,   0.60025,   0.60026,    11.347,    11.348,         0,      3.5734
simulation,        14,       0.7,    0.7001,   0.70014,    12.257,    12.257,         0,      3.5549
simulation,        15,       0.8,   0.80031,   0.80034,    13.855,    13.857,         0,      3.5358
simulation,        16,       0.9,   0.90028,   0.90029,    17.632,    17.634,         0,      3.5179
