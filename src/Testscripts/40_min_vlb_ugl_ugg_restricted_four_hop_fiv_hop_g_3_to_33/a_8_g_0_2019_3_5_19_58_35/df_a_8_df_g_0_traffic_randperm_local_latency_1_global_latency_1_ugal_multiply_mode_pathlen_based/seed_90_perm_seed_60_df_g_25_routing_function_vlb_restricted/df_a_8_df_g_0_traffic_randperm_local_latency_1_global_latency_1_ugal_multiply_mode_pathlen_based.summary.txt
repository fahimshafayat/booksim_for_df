vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 25;
perm_seed = 60;
routing_function = vlb_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.36683,   0.43488,    395.66,    386.84,         1,         0.0
simulation,         2,      0.25,   0.25003,   0.25004,    15.116,    15.116,         0,       5.599
simulation,         3,      0.37,   0.33322,   0.33644,    381.54,    300.05,         1,         0.0
simulation,         4,      0.31,   0.30997,   0.30998,    16.447,    16.447,         0,      5.5995
simulation,         5,      0.34,   0.33998,   0.33998,    18.293,    18.295,         0,       5.599
simulation,         6,      0.35,   0.34154,   0.34284,     312.4,    142.79,         1,         0.0
simulation,         7,       0.1,  0.099898,  0.099902,    13.677,    13.677,         0,      5.5983
simulation,         8,       0.2,   0.19996,   0.19997,    14.475,    14.475,         0,      5.5995
simulation,         9,       0.3,   0.29999,       0.3,    16.147,    16.147,         0,      5.5994
