vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 17;
perm_seed = 60;
routing_function = UGAL_G_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49992,   0.49994,     12.03,    12.031,         0,      3.9619
simulation,         2,      0.74,   0.74029,   0.74027,    23.278,    23.281,         0,      4.0614
simulation,         3,      0.86,   0.76307,   0.76477,    946.76,    337.57,         1,         0.0
simulation,         4,       0.8,   0.77608,   0.77645,    477.78,    177.93,         1,         0.0
simulation,         5,      0.77,   0.77033,   0.77026,    35.396,    35.402,         0,      4.1123
simulation,         6,      0.78,   0.78023,   0.78025,    47.371,    47.387,         0,      4.1335
simulation,         7,      0.79,   0.78019,   0.78017,    309.23,    129.17,         1,         0.0
simulation,         8,       0.1,  0.099911,  0.099915,    10.406,    10.406,         0,       4.096
simulation,         9,       0.2,   0.19997,   0.19998,    10.591,    10.592,         0,      4.0519
simulation,        10,       0.3,   0.29996,   0.29997,    10.873,    10.873,         0,      4.0134
simulation,        11,       0.4,   0.39991,   0.39992,    11.302,    11.302,         0,      3.9817
simulation,        12,       0.5,   0.49992,   0.49994,     12.03,    12.031,         0,      3.9619
simulation,        13,       0.6,   0.60001,   0.60001,    13.544,    13.545,         0,      3.9644
simulation,        14,       0.7,   0.70018,   0.70018,    17.992,    17.993,         0,       4.017
