vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 25;
perm_seed = 60;
routing_function = UGAL_G_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4998,    0.4998,     13.34,     13.34,         0,       4.162
simulation,         2,      0.74,   0.69388,   0.69448,     543.6,     244.5,         1,         0.0
simulation,         3,      0.62,   0.61996,   0.61995,    18.936,    18.937,         0,      4.2504
simulation,         4,      0.68,   0.68003,   0.68001,    34.363,    34.368,         0,      4.3679
simulation,         5,      0.71,   0.69439,   0.69437,    488.59,    158.83,         1,         0.0
simulation,         6,      0.69,   0.69003,   0.69004,    44.206,    44.216,         0,      4.3991
simulation,         7,       0.7,    0.6961,   0.69612,    289.01,    100.91,         0,      4.4462
simulation,         8,       0.1,  0.099898,  0.099902,    10.721,    10.721,         0,      4.2418
simulation,         9,       0.2,   0.19997,   0.19997,    10.984,    10.984,         0,      4.2113
simulation,        10,       0.3,       0.3,       0.3,    11.377,    11.377,         0,      4.1817
simulation,        11,       0.4,   0.39987,   0.39987,    12.017,    12.018,         0,      4.1597
simulation,        12,       0.5,    0.4998,    0.4998,     13.34,     13.34,         0,       4.162
simulation,        13,       0.6,    0.5999,   0.59989,    17.232,    17.233,         0,      4.2261
simulation,        14,       0.7,    0.6961,   0.69612,    289.01,    100.91,         0,      4.4462
