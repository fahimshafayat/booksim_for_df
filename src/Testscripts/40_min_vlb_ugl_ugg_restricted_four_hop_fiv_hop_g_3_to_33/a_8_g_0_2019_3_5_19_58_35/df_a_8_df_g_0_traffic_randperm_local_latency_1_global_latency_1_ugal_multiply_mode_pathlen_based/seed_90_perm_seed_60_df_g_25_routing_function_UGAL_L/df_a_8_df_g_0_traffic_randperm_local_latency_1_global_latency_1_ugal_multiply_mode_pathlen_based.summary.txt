vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 25;
perm_seed = 60;
routing_function = UGAL_L;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49953,   0.49959,    63.727,    52.379,         0,      4.6953
simulation,         2,      0.74,   0.56331,   0.62878,    419.04,    380.19,         1,         0.0
simulation,         3,      0.62,   0.55112,   0.55194,    945.66,    399.56,         1,         0.0
simulation,         4,      0.56,   0.53739,   0.53777,    417.22,    221.48,         1,         0.0
simulation,         5,      0.53,   0.52287,   0.52291,    359.54,    130.56,         1,         0.0
simulation,         6,      0.51,   0.50846,   0.50852,    170.45,    71.831,         0,      4.7417
simulation,         7,      0.52,    0.5164,   0.51647,    337.53,    104.53,         0,      4.7855
simulation,         8,       0.1,    0.0999,  0.099902,    11.314,    11.315,         0,      4.5152
simulation,         9,       0.2,   0.19996,   0.19997,    11.676,    11.677,         0,      4.4934
simulation,        10,       0.3,   0.29999,       0.3,    12.333,    12.333,         0,      4.4538
simulation,        11,       0.4,   0.39987,   0.39987,    20.346,     20.38,         0,      4.4745
simulation,        12,       0.5,   0.49953,   0.49959,    63.727,    52.379,         0,      4.6953
