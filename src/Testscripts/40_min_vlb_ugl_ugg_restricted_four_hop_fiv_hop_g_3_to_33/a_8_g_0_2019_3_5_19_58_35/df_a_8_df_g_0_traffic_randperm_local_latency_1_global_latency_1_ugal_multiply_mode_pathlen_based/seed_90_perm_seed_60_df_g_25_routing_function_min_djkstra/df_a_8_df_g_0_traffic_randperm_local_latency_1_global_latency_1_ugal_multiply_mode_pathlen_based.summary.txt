vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 5000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 25;
perm_seed = 60;
routing_function = min_djkstra;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.41605,   0.43778,    533.86,    390.49,         1,         0.0
simulation,         2,      0.25,   0.24969,   0.24976,    103.13,    28.332,         0,      3.6114
simulation,         3,      0.37,   0.34436,   0.34717,    614.31,    407.56,         1,         0.0
simulation,         4,      0.31,   0.30245,   0.30246,    475.27,    184.48,         1,         0.0
simulation,         5,      0.28,    0.2767,   0.27683,    306.73,    138.76,         1,         0.0
simulation,         6,      0.26,    0.2586,   0.25901,    253.13,    123.53,         0,      3.6115
simulation,         7,      0.27,   0.26762,   0.26785,    396.77,    140.41,         0,      3.6121
simulation,         8,      0.05,  0.049932,  0.049935,    9.2869,     9.287,         0,      3.6119
simulation,         9,       0.1,  0.099898,  0.099902,    9.3708,    9.3709,         0,      3.6111
simulation,        10,      0.15,   0.14992,   0.14993,    9.5006,    9.5007,         0,      3.6113
simulation,        11,       0.2,   0.19995,   0.19997,    10.277,     10.28,         0,      3.6115
simulation,        12,      0.25,   0.24969,   0.24976,    103.13,    28.332,         0,      3.6114
