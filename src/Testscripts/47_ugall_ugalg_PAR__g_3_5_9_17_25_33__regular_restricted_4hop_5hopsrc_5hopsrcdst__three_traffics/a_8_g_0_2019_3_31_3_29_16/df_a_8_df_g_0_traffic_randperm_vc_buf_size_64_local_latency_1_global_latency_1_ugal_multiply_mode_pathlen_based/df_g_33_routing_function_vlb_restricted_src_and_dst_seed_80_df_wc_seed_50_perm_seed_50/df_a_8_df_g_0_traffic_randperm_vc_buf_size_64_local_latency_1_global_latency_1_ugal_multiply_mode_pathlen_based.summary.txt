wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 50;
perm_seed = 50;
routing_function = vlb_restricted_src_and_dst;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.40129,   0.43313,     462.9,    399.84,         1,         0.0
simulation,         2,      0.25,   0.24999,   0.24999,    15.251,    15.251,         0,      5.7194
simulation,         3,      0.37,   0.37008,   0.37008,    18.076,    18.076,         0,      5.7196
simulation,         4,      0.43,   0.40987,   0.41058,    478.38,    198.25,         1,         0.0
simulation,         5,       0.4,   0.40005,   0.40006,    22.989,    22.519,         0,      5.7199
simulation,         6,      0.41,   0.40981,   0.40981,    110.28,    33.376,         0,      5.7199
simulation,         7,      0.42,   0.40512,   0.40545,     497.1,    147.46,         1,         0.0
simulation,         8,       0.1,  0.099976,  0.099975,    13.917,    13.917,         0,      5.7188
simulation,         9,       0.2,   0.20003,   0.20003,     14.68,     14.68,         0,      5.7197
simulation,        10,       0.3,   0.29998,   0.29997,     16.06,     16.06,         0,      5.7198
simulation,        11,       0.4,   0.40005,   0.40006,    22.989,    22.519,         0,      5.7199
