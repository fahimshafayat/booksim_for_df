wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_L_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49942,   0.49942,    174.41,     41.69,         0,      4.4303
simulation,         2,      0.74,    0.5462,   0.58615,    804.09,    525.99,         1,         0.0
simulation,         3,      0.62,    0.5517,    0.5642,    646.34,    346.38,         1,         0.0
simulation,         4,      0.56,   0.55117,   0.55123,    485.42,    123.84,         1,         0.0
simulation,         5,      0.53,   0.52741,   0.52742,    238.92,    67.204,         1,         0.0
simulation,         6,      0.51,   0.50875,   0.50875,    325.97,    48.663,         0,      4.4449
simulation,         7,      0.52,   0.51823,   0.51823,    170.02,    57.952,         1,         0.0
simulation,         8,       0.1,    0.1001,    0.1001,    10.935,    10.935,         0,      4.3373
simulation,         9,       0.2,   0.20016,   0.20016,    11.382,    11.383,         0,      4.3693
simulation,        10,       0.3,    0.3002,    0.3002,    12.234,    12.235,         0,      4.3418
simulation,        11,       0.4,   0.40012,   0.40011,     18.48,    18.493,         0,      4.3428
simulation,        12,       0.5,   0.49942,   0.49942,    174.41,     41.69,         0,      4.4303
