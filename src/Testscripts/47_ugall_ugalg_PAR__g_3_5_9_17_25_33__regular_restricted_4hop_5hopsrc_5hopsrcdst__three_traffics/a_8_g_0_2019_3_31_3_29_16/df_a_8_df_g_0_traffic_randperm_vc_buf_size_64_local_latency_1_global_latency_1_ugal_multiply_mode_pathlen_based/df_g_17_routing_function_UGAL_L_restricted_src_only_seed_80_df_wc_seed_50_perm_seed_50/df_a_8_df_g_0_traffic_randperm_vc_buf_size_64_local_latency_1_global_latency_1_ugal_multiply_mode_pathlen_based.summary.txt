wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_L_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50024,   0.50025,    16.272,    16.279,         0,      4.1623
simulation,         2,      0.74,   0.67701,   0.68907,    621.13,    298.85,         1,         0.0
simulation,         3,      0.62,   0.62008,    0.6201,     35.31,    35.351,         0,      4.2351
simulation,         4,      0.68,   0.66663,   0.66691,    518.99,    165.99,         1,         0.0
simulation,         5,      0.65,   0.64915,   0.64919,    148.91,    63.351,         0,      4.2882
simulation,         6,      0.66,   0.65638,   0.65641,    487.51,    100.49,         0,      4.3084
simulation,         7,      0.67,   0.66219,   0.66228,    485.62,    124.69,         1,         0.0
simulation,         8,       0.1,   0.10017,   0.10017,    10.632,    10.632,         0,      4.2001
simulation,         9,       0.2,    0.2002,    0.2002,    11.019,    11.019,         0,      4.2299
simulation,        10,       0.3,   0.30023,   0.30023,    11.388,    11.388,         0,      4.1994
simulation,        11,       0.4,   0.40026,   0.40026,    12.284,    12.285,         0,      4.1671
simulation,        12,       0.5,   0.50024,   0.50025,    16.272,    16.279,         0,      4.1623
simulation,        13,       0.6,   0.60016,   0.60015,    28.623,    28.656,         0,       4.212
