wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_G_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49998,   0.49998,    13.782,    13.783,         0,      4.2315
simulation,         2,      0.74,   0.66321,    0.6768,    726.63,    348.87,         1,         0.0
simulation,         3,      0.62,   0.61998,   0.61997,    21.596,    21.597,         0,      4.3567
simulation,         4,      0.68,   0.67167,   0.67168,    514.75,    146.41,         1,         0.0
simulation,         5,      0.65,   0.64999,   0.64998,    29.923,    29.925,         0,      4.4232
simulation,         6,      0.66,   0.65996,   0.65998,    36.441,    36.445,         0,      4.4529
simulation,         7,      0.67,   0.66998,   0.66997,    49.927,    49.933,         0,      4.4897
simulation,         8,       0.1,  0.099975,  0.099975,     10.82,     10.82,         0,      4.2913
simulation,         9,       0.2,   0.20002,   0.20003,    11.103,    11.103,         0,      4.2671
simulation,        10,       0.3,   0.29997,   0.29997,    11.519,     11.52,         0,      4.2395
simulation,        11,       0.4,   0.40001,   0.40001,    12.213,    12.213,         0,      4.2195
simulation,        12,       0.5,   0.49998,   0.49998,    13.782,    13.783,         0,      4.2315
simulation,        13,       0.6,   0.59999,   0.59999,    18.973,    18.974,         0,      4.3233
