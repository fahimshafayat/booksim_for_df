internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = PAR;
seed = 86;
shift_by_group = 7;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.43962,   0.45062,    742.65,    380.52,         1,         0.0
simulation,         2,      0.25,   0.24981,   0.24981,    19.646,    19.646,         0,      6.3585
simulation,         3,      0.37,   0.36762,   0.36762,    312.84,    67.831,         1,         0.0
simulation,         4,      0.31,   0.30981,   0.30981,    23.521,    23.523,         0,      6.4021
simulation,         5,      0.34,   0.33949,   0.33949,    117.74,    53.078,         0,      6.3972
simulation,         6,      0.35,   0.34897,   0.34896,    252.27,    55.153,         0,      6.3863
simulation,         7,      0.36,   0.35825,   0.35825,    440.91,      67.5,         0,      6.3731
simulation,         8,       0.1,  0.099956,  0.099959,     14.94,    14.941,         0,      6.0015
simulation,         9,       0.2,   0.19986,   0.19986,    17.754,    17.754,         0,      6.2674
simulation,        10,       0.3,    0.2998,    0.2998,    22.522,    22.523,         0,      6.3986
