internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = UGAL_G_four_hop_restricted;
seed = 84;
shift_by_group = 5;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.36439,   0.40322,    960.27,    710.85,         1,         0.0
simulation,         2,      0.25,   0.24986,   0.24986,    13.364,    13.364,         0,      4.4859
simulation,         3,      0.37,   0.36146,   0.36116,    474.66,     223.0,         1,         0.0
simulation,         4,      0.31,   0.30988,   0.30988,    14.884,    14.884,         0,      4.5166
simulation,         5,      0.34,   0.33986,   0.33986,    17.083,    17.084,         0,      4.5306
simulation,         6,      0.35,   0.34987,   0.34987,    19.179,    19.181,         0,       4.536
simulation,         7,      0.36,   0.35857,   0.35846,    362.45,    61.791,         0,      4.5393
simulation,         8,       0.1,  0.099987,  0.099986,    11.656,    11.657,         0,      4.3989
simulation,         9,       0.2,    0.1999,    0.1999,    12.627,    12.627,         0,      4.4566
simulation,        10,       0.3,   0.29988,   0.29988,    14.519,     14.52,         0,      4.5118
