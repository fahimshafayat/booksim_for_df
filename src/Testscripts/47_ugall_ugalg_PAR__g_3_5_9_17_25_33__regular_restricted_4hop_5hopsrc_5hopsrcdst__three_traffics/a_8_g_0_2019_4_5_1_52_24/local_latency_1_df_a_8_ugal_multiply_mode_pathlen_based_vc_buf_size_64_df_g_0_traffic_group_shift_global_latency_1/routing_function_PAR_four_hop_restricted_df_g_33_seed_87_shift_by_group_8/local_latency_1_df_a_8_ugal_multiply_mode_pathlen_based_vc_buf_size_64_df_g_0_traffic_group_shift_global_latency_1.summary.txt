internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = PAR_four_hop_restricted;
seed = 87;
shift_by_group = 8;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.37582,   0.40936,    829.35,    627.87,         1,         0.0
simulation,         2,      0.25,   0.24944,   0.24945,    264.87,    67.085,         0,      4.7809
simulation,         3,      0.37,   0.34125,   0.34857,    520.84,    329.63,         1,         0.0
simulation,         4,      0.31,   0.30202,   0.30208,     457.0,    206.87,         1,         0.0
simulation,         5,      0.28,   0.27652,   0.27651,    472.71,    158.17,         1,         0.0
simulation,         6,      0.26,   0.25853,   0.25848,    319.58,    109.36,         1,         0.0
simulation,         7,      0.05,  0.049989,  0.049989,    11.806,    11.807,         0,      4.7524
simulation,         8,       0.1,  0.099913,  0.099913,    12.113,    12.113,         0,      4.7461
simulation,         9,      0.15,   0.14992,   0.14992,    12.556,    12.556,         0,      4.7523
simulation,        10,       0.2,   0.19994,   0.19995,    17.341,    17.354,         0,      4.7668
simulation,        11,      0.25,   0.24944,   0.24945,    264.87,    67.085,         0,      4.7809
