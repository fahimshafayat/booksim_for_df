internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = UGAL_G_four_hop_restricted;
seed = 86;
shift_by_group = 7;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.3441,    0.3847,    1089.4,    780.68,         1,         0.0
simulation,         2,      0.25,   0.24981,   0.24981,    13.175,    13.176,         0,       4.402
simulation,         3,      0.37,   0.34536,   0.35289,    476.28,    326.36,         1,         0.0
simulation,         4,      0.31,   0.30981,   0.30981,    15.422,    15.423,         0,      4.4253
simulation,         5,      0.34,   0.33837,   0.33839,    399.71,    89.136,         0,      4.4374
simulation,         6,      0.35,   0.34116,   0.34112,    435.24,     199.5,         1,         0.0
simulation,         7,       0.1,  0.099955,  0.099959,    11.581,    11.581,         0,      4.3285
simulation,         8,       0.2,   0.19986,   0.19986,    12.462,    12.462,         0,       4.378
simulation,         9,       0.3,    0.2998,    0.2998,    14.718,    14.719,         0,      4.4215
