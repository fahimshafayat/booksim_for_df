internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = UGAL_G_restricted_src_only;
seed = 85;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4089,   0.44013,    647.14,    511.64,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24998,    14.663,    14.663,         0,      5.1036
simulation,         3,      0.37,      0.37,      0.37,    18.426,    18.427,         0,      5.1989
simulation,         4,      0.43,   0.40532,    0.4053,    897.35,    333.71,         1,         0.0
simulation,         5,       0.4,   0.40005,   0.40004,    24.608,    24.609,         0,       5.221
simulation,         6,      0.41,   0.40621,   0.40604,    469.63,    136.18,         1,         0.0
simulation,         7,       0.1,   0.10003,   0.10003,    12.835,    12.835,         0,      4.9421
simulation,         8,       0.2,   0.19996,   0.19996,    13.917,    13.917,         0,      5.0495
simulation,         9,       0.3,   0.29997,   0.29997,    15.674,    15.674,         0,      5.1502
simulation,        10,       0.4,   0.40005,   0.40004,    24.608,    24.609,         0,       5.221
