internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = PAR_restricted_src_only;
seed = 86;
shift_by_group = 7;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50022,   0.50023,    21.777,    21.778,         0,      5.0295
simulation,         2,      0.74,   0.53581,   0.58426,    1186.7,    687.93,         1,         0.0
simulation,         3,      0.62,   0.54089,   0.56959,    471.54,    368.06,         1,         0.0
simulation,         4,      0.56,    0.5382,   0.53913,    741.95,    296.57,         1,         0.0
simulation,         5,      0.53,   0.53015,   0.53015,     47.01,    47.053,         0,      5.0998
simulation,         6,      0.54,   0.53307,   0.53311,    505.31,    149.68,         1,         0.0
simulation,         7,       0.1,   0.10012,   0.10012,    11.898,    11.898,         0,      4.7494
simulation,         8,       0.2,   0.20018,   0.20019,    12.432,    12.432,         0,      4.7314
simulation,         9,       0.3,   0.30016,   0.30016,    13.405,    13.405,         0,      4.7517
simulation,        10,       0.4,   0.40022,   0.40022,    15.683,    15.684,         0,      4.8593
simulation,        11,       0.5,   0.50022,   0.50023,    21.777,    21.778,         0,      5.0295
