internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_G;
seed = 86;
shift_by_group = 7;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49984,   0.49984,    38.122,    38.122,         0,      6.0763
simulation,         2,      0.74,   0.41931,     0.468,    1765.1,    942.56,         1,         0.0
simulation,         3,      0.62,   0.46003,   0.50033,     962.9,    660.76,         1,         0.0
simulation,         4,      0.56,   0.49135,   0.52005,    454.91,    382.29,         1,         0.0
simulation,         5,      0.53,   0.50904,   0.50891,    773.61,    277.99,         1,         0.0
simulation,         6,      0.51,   0.50982,   0.50983,    43.629,    43.629,         0,      6.0846
simulation,         7,      0.52,   0.51982,   0.51982,     53.36,    53.363,         0,      6.0931
simulation,         8,       0.1,  0.099957,  0.099952,    13.635,    13.635,         0,       5.367
simulation,         9,       0.2,   0.19988,   0.19988,    15.436,    15.436,         0,      5.5659
simulation,        10,       0.3,    0.2999,    0.2999,     18.28,     18.28,         0,      5.8292
simulation,        11,       0.4,   0.39983,   0.39984,    22.507,    22.507,         0,      5.9776
simulation,        12,       0.5,   0.49984,   0.49984,    38.122,    38.122,         0,      6.0763
