internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = UGAL_G_restricted_src_only;
seed = 86;
shift_by_group = 7;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.39198,    0.4276,    777.77,    603.17,         1,         0.0
simulation,         2,      0.25,   0.24984,   0.24984,    15.088,    15.088,         0,       5.212
simulation,         3,      0.37,   0.36972,   0.36973,    19.852,    19.854,         0,      5.3174
simulation,         4,      0.43,   0.39596,   0.40529,    547.07,    357.78,         1,         0.0
simulation,         5,       0.4,   0.39621,   0.39618,    473.54,    120.62,         1,         0.0
simulation,         6,      0.38,   0.37971,   0.37971,    21.381,    21.382,         0,      5.3246
simulation,         7,      0.39,   0.38974,   0.38973,     24.99,    24.993,         0,      5.3329
simulation,         8,       0.1,  0.099921,  0.099925,    13.038,    13.038,         0,      5.0266
simulation,         9,       0.2,   0.19987,   0.19988,    14.237,    14.237,         0,      5.1485
simulation,        10,       0.3,   0.29978,   0.29979,    16.248,    16.248,         0,      5.2635
