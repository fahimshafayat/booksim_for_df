internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = PAR;
seed = 85;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.45547,   0.46506,    622.38,    345.19,         1,         0.0
simulation,         2,      0.25,   0.24979,   0.24979,     18.04,     18.04,         0,      6.0271
simulation,         3,      0.37,   0.36993,   0.36992,    28.802,    28.805,         0,      6.2775
simulation,         4,      0.43,   0.42536,   0.42541,     491.1,    119.75,         1,         0.0
simulation,         5,       0.4,   0.39974,   0.39973,    123.48,    71.543,         0,      6.2817
simulation,         6,      0.41,   0.40912,   0.40913,    231.18,    78.636,         0,      6.2705
simulation,         7,      0.42,   0.41776,   0.41776,    482.03,    107.39,         0,      6.2565
simulation,         8,       0.1,  0.099932,   0.09993,    14.194,    14.195,         0,      5.7432
simulation,         9,       0.2,   0.19983,   0.19983,    15.994,    15.994,         0,      5.8494
simulation,        10,       0.3,   0.29981,   0.29981,    20.753,    20.754,         0,      6.1688
simulation,        11,       0.4,   0.39974,   0.39973,    123.48,    71.543,         0,      6.2817
