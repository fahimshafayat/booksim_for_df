internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_G_four_hop_restricted;
seed = 84;
shift_by_group = 5;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.39085,   0.42851,    781.54,    619.29,         1,         0.0
simulation,         2,      0.25,    0.2499,    0.2499,    12.817,    12.817,         0,      4.2991
simulation,         3,      0.37,   0.36987,   0.36987,    17.913,    17.915,         0,      4.3581
simulation,         4,      0.43,   0.39295,   0.40271,    616.14,    394.09,         1,         0.0
simulation,         5,       0.4,   0.38908,    0.3882,    451.53,    215.13,         1,         0.0
simulation,         6,      0.38,   0.37984,   0.37984,    20.466,    20.468,         0,       4.363
simulation,         7,      0.39,   0.38987,   0.38989,    30.671,    30.677,         0,       4.369
simulation,         8,       0.1,       0.1,  0.099998,    11.148,    11.149,         0,       4.238
simulation,         9,       0.2,   0.19993,   0.19992,    12.149,    12.149,         0,      4.2792
simulation,        10,       0.3,   0.29986,   0.29986,    13.806,    13.806,         0,      4.3261
