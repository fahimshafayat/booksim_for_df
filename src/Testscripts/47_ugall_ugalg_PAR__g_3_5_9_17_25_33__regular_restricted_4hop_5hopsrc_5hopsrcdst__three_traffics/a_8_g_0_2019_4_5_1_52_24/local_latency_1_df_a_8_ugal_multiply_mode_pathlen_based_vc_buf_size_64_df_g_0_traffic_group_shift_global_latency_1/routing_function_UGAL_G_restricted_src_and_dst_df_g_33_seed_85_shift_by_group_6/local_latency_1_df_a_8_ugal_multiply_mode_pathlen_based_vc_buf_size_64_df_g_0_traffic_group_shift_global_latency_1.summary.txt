internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 85;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.42861,   0.45696,    523.28,    423.08,         1,         0.0
simulation,         2,      0.25,   0.25011,   0.25012,    15.492,    15.492,         0,      5.4174
simulation,         3,      0.37,   0.37005,   0.37006,    18.963,    18.964,         0,      5.5036
simulation,         4,      0.43,   0.43002,   0.43005,    31.597,    31.605,         0,      5.5368
simulation,         5,      0.46,   0.43014,   0.43951,    475.83,    318.44,         1,         0.0
simulation,         6,      0.44,   0.42987,   0.43022,    501.07,    248.32,         1,         0.0
simulation,         7,       0.1,   0.10004,   0.10004,    13.494,    13.494,         0,      5.2119
simulation,         8,       0.2,   0.20003,   0.20003,     14.69,     14.69,         0,      5.3548
simulation,         9,       0.3,   0.30011,   0.30011,    16.521,    16.521,         0,      5.4607
simulation,        10,       0.4,   0.40008,   0.40008,    21.415,    21.416,         0,      5.5195
