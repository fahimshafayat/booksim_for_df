internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = PAR;
seed = 86;
shift_by_group = 7;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46602,   0.47967,    579.32,    410.09,         1,         0.0
simulation,         2,      0.25,   0.25015,   0.25015,    15.081,    15.082,         0,      5.4028
simulation,         3,      0.37,   0.37013,   0.37015,     21.56,     21.56,         0,        5.79
simulation,         4,      0.43,   0.43017,   0.43017,    29.683,    29.684,         0,      5.9471
simulation,         5,      0.46,   0.46023,   0.46021,    52.662,    52.678,         0,      5.9946
simulation,         6,      0.48,   0.46747,   0.46769,    532.13,    303.26,         1,         0.0
simulation,         7,      0.47,   0.46849,   0.46877,    294.95,    155.58,         0,      5.9895
simulation,         8,       0.1,   0.10012,   0.10012,    13.396,    13.396,         0,      5.4324
simulation,         9,       0.2,   0.20018,   0.20019,    14.163,    14.163,         0,      5.3672
simulation,        10,       0.3,   0.30015,   0.30016,    16.965,    16.965,         0,      5.5286
simulation,        11,       0.4,   0.40021,   0.40022,    24.607,    24.607,         0,      5.8784
