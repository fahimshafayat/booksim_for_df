internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = PAR_restricted_src_and_dst;
seed = 87;
shift_by_group = 8;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50017,   0.50017,    23.046,    23.046,         0,       5.191
simulation,         2,      0.74,   0.54641,   0.59616,    1123.5,    703.06,         1,         0.0
simulation,         3,      0.62,   0.55323,   0.58735,    480.59,    402.64,         1,         0.0
simulation,         4,      0.56,   0.55832,   0.55861,    318.34,    216.33,         0,        5.27
simulation,         5,      0.59,   0.55558,   0.57095,    536.55,    393.42,         1,         0.0
simulation,         6,      0.57,   0.55741,   0.56089,    584.21,    410.96,         1,         0.0
simulation,         7,       0.1,    0.1001,    0.1001,    12.146,    12.146,         0,      4.8633
simulation,         8,       0.2,   0.19999,   0.19999,    12.691,    12.692,         0,      4.8357
simulation,         9,       0.3,   0.29995,   0.29995,    13.786,    13.786,         0,      4.8632
simulation,        10,       0.4,   0.39993,   0.39993,     16.46,     16.46,         0,      5.0066
simulation,        11,       0.5,   0.50017,   0.50017,    23.046,    23.046,         0,       5.191
