internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 86;
shift_by_group = 7;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4647,   0.47715,    620.33,    398.26,         1,         0.0
simulation,         2,      0.25,   0.25015,   0.25015,    39.208,    39.234,         0,      4.4654
simulation,         3,      0.37,      0.37,   0.37006,    82.339,    77.592,         0,      4.6495
simulation,         4,      0.43,   0.42465,   0.42476,    512.62,    204.99,         1,         0.0
simulation,         5,       0.4,   0.39819,   0.39827,    409.57,     158.5,         0,      4.6779
simulation,         6,      0.41,   0.40695,   0.40706,    506.81,    193.43,         1,         0.0
simulation,         7,       0.1,   0.10012,   0.10012,    11.099,    11.099,         0,      4.3251
simulation,         8,       0.2,   0.20018,   0.20019,    12.533,    12.533,         0,      4.3904
simulation,         9,       0.3,   0.30016,   0.30016,    42.072,    42.111,         0,      4.5582
simulation,        10,       0.4,   0.39819,   0.39827,    409.57,     158.5,         0,      4.6779
