internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_L_four_hop_restricted;
seed = 87;
shift_by_group = 8;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.33639,   0.38038,    1239.4,    911.38,         1,         0.0
simulation,         2,      0.25,   0.24903,   0.24904,     388.9,    65.692,         0,      4.4133
simulation,         3,      0.37,   0.32394,   0.34753,    444.41,    407.05,         1,         0.0
simulation,         4,      0.31,   0.29782,   0.29884,    704.37,    375.84,         1,         0.0
simulation,         5,      0.28,   0.27624,    0.2763,     497.6,    148.85,         1,         0.0
simulation,         6,      0.26,   0.25858,    0.2586,    254.77,    92.271,         1,         0.0
simulation,         7,      0.05,  0.050081,   0.05008,     10.63,     10.63,         0,      4.1431
simulation,         8,       0.1,   0.10017,   0.10017,    11.498,    11.498,         0,       4.218
simulation,         9,      0.15,   0.15018,   0.15018,    36.041,    36.069,         0,      4.2992
simulation,        10,       0.2,   0.20018,   0.20018,    33.628,    33.663,         0,      4.3732
simulation,        11,      0.25,   0.24903,   0.24904,     388.9,    65.692,         0,      4.4133
