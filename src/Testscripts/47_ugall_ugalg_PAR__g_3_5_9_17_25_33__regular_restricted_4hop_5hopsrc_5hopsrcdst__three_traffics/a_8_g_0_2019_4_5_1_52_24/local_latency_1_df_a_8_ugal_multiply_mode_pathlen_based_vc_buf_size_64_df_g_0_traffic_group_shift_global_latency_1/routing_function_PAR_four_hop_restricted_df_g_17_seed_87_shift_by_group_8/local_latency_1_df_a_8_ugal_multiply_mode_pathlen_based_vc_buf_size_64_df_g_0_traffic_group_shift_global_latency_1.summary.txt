internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = PAR_four_hop_restricted;
seed = 87;
shift_by_group = 8;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.40493,   0.43929,    686.23,    563.79,         1,         0.0
simulation,         2,      0.25,   0.25002,   0.25002,    14.767,    14.775,         0,      4.7157
simulation,         3,      0.37,   0.36876,   0.36876,    307.18,    102.03,         0,      4.8168
simulation,         4,      0.43,   0.39644,   0.40504,    543.21,    352.72,         1,         0.0
simulation,         5,       0.4,   0.38697,   0.38733,    536.94,    240.99,         1,         0.0
simulation,         6,      0.38,   0.37676,   0.37673,    464.96,    139.83,         1,         0.0
simulation,         7,       0.1,   0.10017,   0.10017,    11.848,    11.848,         0,      4.6728
simulation,         8,       0.2,   0.20018,   0.20018,    12.703,    12.703,         0,       4.694
simulation,         9,       0.3,   0.29995,   0.29995,    30.188,    30.275,         0,      4.7599
