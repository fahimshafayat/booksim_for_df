internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 85;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4586,    0.4721,    726.85,    454.47,         1,         0.0
simulation,         2,      0.25,    0.2503,    0.2503,    41.416,     41.44,         0,      4.5351
simulation,         3,      0.37,   0.36942,    0.3696,    164.43,    111.48,         0,      4.7293
simulation,         4,      0.43,   0.42081,   0.42228,    545.36,    359.03,         1,         0.0
simulation,         5,       0.4,    0.3975,   0.39764,    432.96,     170.8,         1,         0.0
simulation,         6,      0.38,   0.37911,   0.37918,    262.83,    137.89,         0,      4.7401
simulation,         7,      0.39,   0.38839,   0.38843,     382.2,     145.8,         0,      4.7502
simulation,         8,       0.1,   0.10008,   0.10008,    11.216,    11.216,         0,      4.3801
simulation,         9,       0.2,   0.20023,   0.20023,    12.769,    12.769,         0,        4.45
simulation,        10,       0.3,    0.3003,    0.3003,    43.144,    43.181,         0,      4.6332
