internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_G;
seed = 85;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50003,   0.50004,    38.256,    38.258,         0,       6.079
simulation,         2,      0.74,   0.41056,   0.45881,    1770.3,    964.08,         1,         0.0
simulation,         3,      0.62,   0.46072,   0.50168,    984.54,    667.19,         1,         0.0
simulation,         4,      0.56,   0.49083,   0.51844,    442.09,    376.54,         1,         0.0
simulation,         5,      0.53,   0.50875,   0.50843,    789.29,     265.4,         1,         0.0
simulation,         6,      0.51,   0.51006,   0.51007,    43.922,    43.923,         0,      6.0876
simulation,         7,      0.52,   0.52007,   0.52009,    53.887,    53.886,         0,      6.0963
simulation,         8,       0.1,  0.099932,   0.09993,    13.641,    13.642,         0,      5.3692
simulation,         9,       0.2,   0.19983,   0.19983,    15.438,    15.438,         0,      5.5688
simulation,        10,       0.3,   0.29981,   0.29981,    18.288,    18.288,         0,      5.8325
simulation,        11,       0.4,   0.39997,   0.39997,    22.536,    22.537,         0,       5.981
simulation,        12,       0.5,   0.50003,   0.50004,    38.256,    38.258,         0,       6.079
