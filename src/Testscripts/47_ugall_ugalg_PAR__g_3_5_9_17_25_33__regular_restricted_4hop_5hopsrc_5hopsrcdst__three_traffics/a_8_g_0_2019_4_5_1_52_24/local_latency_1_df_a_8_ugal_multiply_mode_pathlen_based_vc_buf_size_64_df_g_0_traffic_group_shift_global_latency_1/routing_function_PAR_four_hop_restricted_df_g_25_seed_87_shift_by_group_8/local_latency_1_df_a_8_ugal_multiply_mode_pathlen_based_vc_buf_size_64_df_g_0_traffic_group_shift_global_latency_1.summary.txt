internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = PAR_four_hop_restricted;
seed = 87;
shift_by_group = 8;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.36199,   0.40064,    972.84,    744.79,         1,         0.0
simulation,         2,      0.25,   0.24997,   0.24997,    24.605,    24.652,         0,      4.8658
simulation,         3,      0.37,   0.34047,    0.3484,    521.49,    352.21,         1,         0.0
simulation,         4,      0.31,   0.30633,   0.30636,    503.91,    146.63,         1,         0.0
simulation,         5,      0.28,   0.27993,   0.27993,    40.571,     40.69,         0,      4.8803
simulation,         6,      0.29,    0.2897,   0.28971,    109.44,    56.066,         0,      4.8831
simulation,         7,       0.3,   0.29862,   0.29862,    210.51,    78.223,         1,         0.0
simulation,         8,      0.05,  0.049998,  0.049999,    11.879,     11.88,         0,      4.7957
simulation,         9,       0.1,  0.099988,  0.099988,    12.205,    12.205,         0,      4.8006
simulation,        10,      0.15,      0.15,      0.15,     12.66,     12.66,         0,      4.8108
simulation,        11,       0.2,   0.20005,   0.20004,    16.168,    16.184,         0,      4.8291
simulation,        12,      0.25,   0.24997,   0.24997,    24.605,    24.652,         0,      4.8658
