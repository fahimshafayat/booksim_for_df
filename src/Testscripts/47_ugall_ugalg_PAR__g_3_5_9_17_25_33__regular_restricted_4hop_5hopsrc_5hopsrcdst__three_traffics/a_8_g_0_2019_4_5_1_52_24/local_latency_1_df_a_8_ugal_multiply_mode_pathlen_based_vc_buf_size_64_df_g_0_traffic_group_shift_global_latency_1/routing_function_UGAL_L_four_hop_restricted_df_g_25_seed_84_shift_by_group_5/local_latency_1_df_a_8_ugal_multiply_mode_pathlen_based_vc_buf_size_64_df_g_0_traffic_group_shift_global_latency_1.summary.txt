internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = UGAL_L_four_hop_restricted;
seed = 84;
shift_by_group = 5;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.33454,   0.37288,    1081.2,    787.28,         1,         0.0
simulation,         2,      0.25,   0.24389,   0.24399,    364.77,    197.79,         1,         0.0
simulation,         3,      0.13,   0.12992,   0.12994,    64.944,    47.335,         0,      4.4121
simulation,         4,      0.19,   0.18858,   0.18861,    370.26,    136.51,         1,         0.0
simulation,         5,      0.16,   0.15967,    0.1597,    226.07,    79.012,         0,      4.4588
simulation,         6,      0.17,   0.16941,   0.16947,    343.48,    106.27,         0,       4.471
simulation,         7,      0.18,   0.17904,   0.17906,    283.76,     117.1,         1,         0.0
simulation,         8,      0.05,  0.049972,  0.049971,    11.064,    11.064,         0,      4.2411
simulation,         9,       0.1,  0.099987,  0.099986,    23.757,    23.776,         0,      4.3588
simulation,        10,      0.15,    0.1498,   0.14981,    151.53,    51.846,         0,      4.4454
