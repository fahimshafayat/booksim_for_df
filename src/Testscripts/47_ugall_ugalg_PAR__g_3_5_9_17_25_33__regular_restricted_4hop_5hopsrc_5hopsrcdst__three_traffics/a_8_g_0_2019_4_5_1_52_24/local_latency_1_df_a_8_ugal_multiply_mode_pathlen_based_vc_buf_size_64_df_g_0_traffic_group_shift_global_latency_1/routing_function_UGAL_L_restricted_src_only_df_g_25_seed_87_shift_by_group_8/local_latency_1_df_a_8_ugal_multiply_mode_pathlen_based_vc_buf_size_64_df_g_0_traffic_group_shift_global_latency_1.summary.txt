internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = UGAL_L_restricted_src_only;
seed = 87;
shift_by_group = 8;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.34958,    0.3877,    971.29,    698.47,         1,         0.0
simulation,         2,      0.25,   0.24997,   0.24997,     29.76,    29.798,         0,         5.3
simulation,         3,      0.37,   0.34752,   0.34854,    986.91,    374.08,         1,         0.0
simulation,         4,      0.31,   0.30991,    0.3099,    30.945,    30.992,         0,      5.3587
simulation,         5,      0.34,   0.33797,     0.338,     262.0,    70.707,         1,         0.0
simulation,         6,      0.32,   0.31998,   0.31997,    32.058,    32.111,         0,      5.3666
simulation,         7,      0.33,   0.32925,   0.32926,    189.04,    55.196,         0,       5.374
simulation,         8,       0.1,  0.099988,  0.099988,    25.575,    25.594,         0,      4.9352
simulation,         9,       0.2,   0.20004,   0.20004,     30.23,    30.262,         0,      5.2274
simulation,        10,       0.3,   0.29991,   0.29991,    30.466,    30.509,         0,      5.3501
