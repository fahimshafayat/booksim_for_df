internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_G_four_hop_restricted;
seed = 84;
shift_by_group = 5;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.48073,   0.48031,    756.45,    332.86,         1,         0.0
simulation,         2,      0.25,      0.25,   0.24999,    12.098,    12.098,         0,      4.2193
simulation,         3,      0.37,    0.3698,   0.36979,    14.207,    14.207,         0,        4.26
simulation,         4,      0.43,   0.42974,   0.42973,    16.683,    16.683,         0,      4.2961
simulation,         5,      0.46,   0.45971,   0.45971,    20.368,    20.369,         0,      4.3127
simulation,         6,      0.48,   0.47611,   0.47619,    334.15,    147.61,         1,         0.0
simulation,         7,      0.47,    0.4697,    0.4697,    23.855,    23.856,         0,      4.3189
simulation,         8,       0.1,   0.10006,   0.10006,    10.773,    10.773,         0,      4.1823
simulation,         9,       0.2,   0.20012,   0.20012,    11.578,    11.578,         0,      4.2098
simulation,        10,       0.3,   0.29989,   0.29987,    12.719,    12.719,         0,      4.2292
simulation,        11,       0.4,    0.3998,    0.3998,    15.155,    15.155,         0,      4.2784
