internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_G_four_hop_restricted;
seed = 86;
shift_by_group = 7;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.3983,   0.43178,    742.23,    562.92,         1,         0.0
simulation,         2,      0.25,   0.24989,   0.24989,     12.83,     12.83,         0,      4.3317
simulation,         3,      0.37,   0.36983,   0.36984,    17.354,    17.355,         0,      4.3932
simulation,         4,      0.43,   0.39519,    0.4048,    600.39,    380.92,         1,         0.0
simulation,         5,       0.4,   0.38838,   0.38759,    599.97,    276.95,         1,         0.0
simulation,         6,      0.38,   0.37985,   0.37984,    20.844,    20.847,         0,      4.3981
simulation,         7,      0.39,   0.38509,   0.38482,     516.3,    169.23,         1,         0.0
simulation,         8,       0.1,  0.099957,  0.099952,    11.198,    11.198,         0,      4.2653
simulation,         9,       0.2,   0.19988,   0.19988,    12.186,    12.186,         0,      4.3098
simulation,        10,       0.3,    0.2999,    0.2999,    13.774,    13.774,         0,        4.36
