internal_speedup = 4.0;
output_speedup = 1;
routing_delay = 0;
priority = none;
num_vcs = 8;
sw_allocator = separable_input_first;
vc_allocator = separable_input_first;
sample_period = 10000;
sim_count = 1;
alloc_iters = 1;
warmup_periods = 3;
vc_alloc_delay = 1;
input_speedup = 1;
injection_rate_uses_flits = 1;
st_final_delay = 1;
sw_alloc_delay = 1;
packet_size = 1;
wait_for_tail_credit = 0;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_L_four_hop_restricted;
seed = 85;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.41803,   0.45217,     654.1,    530.92,         1,         0.0
simulation,         2,      0.25,   0.25029,    0.2503,    36.748,    36.772,         0,      4.2145
simulation,         3,      0.37,    0.3654,   0.36559,    468.42,    221.54,         1,         0.0
simulation,         4,      0.31,   0.31029,    0.3103,    40.714,    40.751,         0,      4.2685
simulation,         5,      0.34,   0.33953,   0.33956,    218.65,    98.186,         0,      4.2883
simulation,         6,      0.35,   0.34883,   0.34887,    349.25,    131.75,         0,      4.2936
simulation,         7,      0.36,   0.35756,   0.35762,    472.02,    177.97,         1,         0.0
simulation,         8,       0.1,   0.10008,   0.10008,    10.659,    10.659,         0,      4.1186
simulation,         9,       0.2,   0.20023,   0.20023,    11.981,    11.981,         0,      4.1687
simulation,        10,       0.3,    0.3003,    0.3003,    40.397,    40.431,         0,      4.2613
