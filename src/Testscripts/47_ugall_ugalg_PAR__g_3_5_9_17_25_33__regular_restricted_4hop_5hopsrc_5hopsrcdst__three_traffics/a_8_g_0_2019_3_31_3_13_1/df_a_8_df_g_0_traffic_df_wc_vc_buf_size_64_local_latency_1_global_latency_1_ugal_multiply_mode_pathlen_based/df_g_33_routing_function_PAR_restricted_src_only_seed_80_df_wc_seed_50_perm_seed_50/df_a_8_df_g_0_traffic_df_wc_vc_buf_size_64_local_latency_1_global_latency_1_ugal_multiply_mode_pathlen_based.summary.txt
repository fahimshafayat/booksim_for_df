wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 50;
perm_seed = 50;
routing_function = PAR_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35764,    0.4045,    884.01,    704.98,         1,         0.0
simulation,         2,      0.25,   0.24999,   0.24999,    16.363,    16.363,         0,      5.6666
simulation,         3,      0.37,   0.37007,   0.37008,    29.375,    29.402,         0,      5.7264
simulation,         4,      0.43,   0.37446,   0.38977,    658.46,    443.68,         1,         0.0
simulation,         5,       0.4,   0.37463,   0.37835,     549.1,    282.55,         1,         0.0
simulation,         6,      0.38,   0.38011,   0.38009,    34.705,    34.858,         0,      5.7252
simulation,         7,      0.39,   0.38164,   0.38247,    454.45,    164.04,         1,         0.0
simulation,         8,       0.1,  0.099974,  0.099975,    13.718,    13.718,         0,      5.5032
simulation,         9,       0.2,   0.20003,   0.20003,    15.209,    15.209,         0,       5.597
simulation,        10,       0.3,   0.29998,   0.29997,    17.838,    17.838,         0,      5.7066
