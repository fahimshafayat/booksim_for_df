wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4132,   0.44669,     658.7,    541.73,         1,         0.0
simulation,         2,      0.25,    0.2502,    0.2502,    15.451,    15.451,         0,      5.3496
simulation,         3,      0.37,   0.37022,   0.37021,    19.337,    19.337,         0,       5.471
simulation,         4,      0.43,   0.42175,   0.42144,    455.34,    210.08,         1,         0.0
simulation,         5,       0.4,   0.40012,   0.40011,    22.418,    22.419,         0,      5.4926
simulation,         6,      0.41,   0.41014,   0.41012,    24.878,    24.879,         0,      5.4995
simulation,         7,      0.42,   0.42014,   0.42012,    32.708,    32.711,         0,      5.5074
simulation,         8,       0.1,    0.1001,    0.1001,    13.235,    13.235,         0,      5.1336
simulation,         9,       0.2,   0.20016,   0.20016,    14.532,    14.532,         0,       5.274
simulation,        10,       0.3,   0.30021,    0.3002,    16.605,    16.605,         0,      5.4109
simulation,        11,       0.4,   0.40012,   0.40011,    22.418,    22.419,         0,      5.4926
