wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 50;
perm_seed = 50;
routing_function = vlb_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.3658,    0.4145,    965.21,    792.93,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24998,    14.101,    14.101,         0,      5.1113
simulation,         3,      0.37,   0.37017,   0.37017,    21.595,    21.601,         0,      5.1117
simulation,         4,      0.43,   0.36756,   0.38702,    1087.2,    665.81,         1,         0.0
simulation,         5,       0.4,   0.37452,   0.38904,    480.73,    365.81,         1,         0.0
simulation,         6,      0.38,   0.35613,   0.35841,    561.13,    310.76,         1,         0.0
simulation,         7,       0.1,   0.10003,   0.10003,    12.672,    12.673,         0,      5.1118
simulation,         8,       0.2,   0.19998,   0.19998,    13.446,    13.446,         0,      5.1119
simulation,         9,       0.3,   0.30012,   0.30011,    15.205,    15.206,         0,      5.1114
