wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_L_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.29144,   0.33658,    1352.1,    968.11,         1,         0.0
simulation,         2,      0.25,   0.24999,   0.24999,    27.233,    27.267,         0,      5.4709
simulation,         3,      0.37,   0.31103,    0.3288,    908.49,    581.39,         1,         0.0
simulation,         4,      0.31,   0.30436,   0.30491,    479.27,    212.11,         1,         0.0
simulation,         5,      0.28,   0.28001,   0.28001,    27.493,     27.53,         0,      5.4966
simulation,         6,      0.29,   0.29001,   0.29001,    28.458,    28.494,         0,       5.504
simulation,         7,       0.3,    0.2996,   0.29973,    98.681,    58.703,         0,      5.5103
simulation,         8,      0.05,  0.049959,   0.04996,    12.373,    12.374,         0,      4.8253
simulation,         9,       0.1,  0.099977,  0.099975,    32.131,    32.155,         0,      5.1285
simulation,        10,      0.15,      0.15,      0.15,    29.137,    29.167,         0,      5.3162
simulation,        11,       0.2,   0.20003,   0.20003,    27.778,    27.811,         0,      5.4119
simulation,        12,      0.25,   0.24999,   0.24999,    27.233,    27.267,         0,      5.4709
simulation,        13,       0.3,    0.2996,   0.29973,    98.681,    58.703,         0,      5.5103
