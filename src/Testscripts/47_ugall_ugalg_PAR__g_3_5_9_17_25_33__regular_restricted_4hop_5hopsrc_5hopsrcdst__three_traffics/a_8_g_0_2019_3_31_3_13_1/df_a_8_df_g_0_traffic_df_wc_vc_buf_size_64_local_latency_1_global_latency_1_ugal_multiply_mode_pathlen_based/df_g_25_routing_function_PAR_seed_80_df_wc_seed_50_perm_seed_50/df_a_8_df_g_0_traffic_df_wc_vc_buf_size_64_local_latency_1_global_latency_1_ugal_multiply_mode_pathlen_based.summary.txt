wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 50;
perm_seed = 50;
routing_function = PAR;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.36466,   0.41031,    1061.6,    837.76,         1,         0.0
simulation,         2,      0.25,    0.2502,    0.2502,    19.488,    19.489,         0,       6.228
simulation,         3,      0.37,   0.36763,   0.36762,    349.08,    102.94,         1,         0.0
simulation,         4,      0.31,   0.30983,   0.30983,    101.38,    35.059,         0,      6.3208
simulation,         5,      0.34,   0.33913,   0.33914,     295.8,    48.222,         0,       6.329
simulation,         6,      0.35,   0.34898,   0.34897,     350.5,    54.232,         0,      6.3306
simulation,         7,      0.36,   0.35884,   0.35884,    418.83,    65.498,         0,      6.3323
simulation,         8,       0.1,    0.1001,    0.1001,    14.637,    14.637,         0,      5.8826
simulation,         9,       0.2,   0.20016,   0.20016,    17.214,    17.215,         0,      6.0912
simulation,        10,       0.3,   0.29992,   0.29992,    65.198,    33.169,         0,      6.3121
