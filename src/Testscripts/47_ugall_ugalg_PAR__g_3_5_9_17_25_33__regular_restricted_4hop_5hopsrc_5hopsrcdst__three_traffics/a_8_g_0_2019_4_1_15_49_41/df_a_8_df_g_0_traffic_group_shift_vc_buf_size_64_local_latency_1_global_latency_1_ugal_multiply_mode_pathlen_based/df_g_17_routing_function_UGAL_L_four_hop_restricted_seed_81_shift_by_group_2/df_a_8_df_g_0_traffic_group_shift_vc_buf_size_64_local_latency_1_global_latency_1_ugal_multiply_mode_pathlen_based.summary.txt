wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_L_four_hop_restricted;
seed = 81;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.34652,     0.388,    1130.2,    831.66,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24999,    32.186,    32.226,         0,      4.2937
simulation,         3,      0.37,   0.33354,   0.34476,    717.09,    474.68,         1,         0.0
simulation,         4,      0.31,   0.30515,   0.30534,    486.52,    202.45,         1,         0.0
simulation,         5,      0.28,   0.27936,   0.27937,    228.72,    90.093,         0,      4.3071
simulation,         6,      0.29,   0.28893,   0.28895,    373.68,    96.619,         0,      4.3109
simulation,         7,       0.3,   0.29771,   0.29776,    410.95,    135.46,         1,         0.0
simulation,         8,      0.05,  0.049957,  0.049961,    10.495,    10.495,         0,      4.0806
simulation,         9,       0.1,   0.09999,  0.099991,    11.292,    11.292,         0,      4.1411
simulation,        10,      0.15,   0.15003,   0.15003,    35.462,    35.488,         0,      4.2058
simulation,        11,       0.2,   0.20005,   0.20005,    32.938,    32.972,         0,       4.264
simulation,        12,      0.25,   0.24998,   0.24999,    32.186,    32.226,         0,      4.2937
