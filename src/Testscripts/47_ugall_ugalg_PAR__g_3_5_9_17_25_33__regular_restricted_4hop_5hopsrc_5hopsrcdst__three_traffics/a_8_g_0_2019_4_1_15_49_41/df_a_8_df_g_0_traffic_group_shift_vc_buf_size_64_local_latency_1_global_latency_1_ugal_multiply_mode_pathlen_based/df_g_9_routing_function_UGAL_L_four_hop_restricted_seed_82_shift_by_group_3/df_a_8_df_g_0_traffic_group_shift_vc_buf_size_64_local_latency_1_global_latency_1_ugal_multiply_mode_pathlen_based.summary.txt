wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_L_four_hop_restricted;
seed = 82;
shift_by_group = 3;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.39831,   0.43621,    784.13,    624.26,         1,         0.0
simulation,         2,      0.25,   0.25013,   0.25012,    39.286,    39.312,         0,      4.2068
simulation,         3,      0.37,    0.3636,   0.36538,    476.92,    292.75,         1,         0.0
simulation,         4,      0.31,   0.31003,    0.3101,    59.093,    59.456,         0,      4.2553
simulation,         5,      0.34,   0.33952,   0.33961,    206.39,    116.15,         0,      4.2705
simulation,         6,      0.35,   0.34828,   0.34841,    452.63,    191.67,         0,      4.2739
simulation,         7,      0.36,    0.3567,   0.35684,    495.88,    248.64,         1,         0.0
simulation,         8,       0.1,  0.099987,  0.099984,    10.646,    10.646,         0,      4.1095
simulation,         9,       0.2,    0.2001,   0.20009,    12.044,    12.044,         0,      4.1604
simulation,        10,       0.3,   0.30009,    0.3001,    42.968,    43.004,         0,      4.2495
