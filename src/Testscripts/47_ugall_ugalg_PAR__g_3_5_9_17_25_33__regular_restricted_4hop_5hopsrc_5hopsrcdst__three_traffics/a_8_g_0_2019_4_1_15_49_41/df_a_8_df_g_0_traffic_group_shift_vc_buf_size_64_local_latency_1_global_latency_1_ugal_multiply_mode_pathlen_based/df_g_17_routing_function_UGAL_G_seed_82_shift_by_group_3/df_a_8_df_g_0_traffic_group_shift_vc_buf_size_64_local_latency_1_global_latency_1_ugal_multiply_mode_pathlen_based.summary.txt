wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_G;
seed = 82;
shift_by_group = 3;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,       0.5,   0.50002,    38.176,    38.178,         0,      6.0758
simulation,         2,      0.74,   0.41382,   0.46051,    1736.4,    929.84,         1,         0.0
simulation,         3,      0.62,   0.45774,   0.49766,    952.14,     657.2,         1,         0.0
simulation,         4,      0.56,   0.48984,   0.51859,    457.43,     387.2,         1,         0.0
simulation,         5,      0.53,   0.50745,   0.50717,    810.42,    282.54,         1,         0.0
simulation,         6,      0.51,   0.51001,   0.51001,    43.794,    43.796,         0,      6.0848
simulation,         7,      0.52,   0.52006,   0.52004,    53.638,    53.641,         0,      6.0932
simulation,         8,       0.1,    0.1001,    0.1001,     13.63,     13.63,         0,      5.3648
simulation,         9,       0.2,   0.20002,   0.20002,    15.435,    15.435,         0,      5.5639
simulation,        10,       0.3,   0.30017,   0.30017,    18.305,    18.305,         0,      5.8286
simulation,        11,       0.4,   0.40011,   0.40011,    22.536,    22.537,         0,      5.9772
simulation,        12,       0.5,       0.5,   0.50002,    38.176,    38.178,         0,      6.0758
