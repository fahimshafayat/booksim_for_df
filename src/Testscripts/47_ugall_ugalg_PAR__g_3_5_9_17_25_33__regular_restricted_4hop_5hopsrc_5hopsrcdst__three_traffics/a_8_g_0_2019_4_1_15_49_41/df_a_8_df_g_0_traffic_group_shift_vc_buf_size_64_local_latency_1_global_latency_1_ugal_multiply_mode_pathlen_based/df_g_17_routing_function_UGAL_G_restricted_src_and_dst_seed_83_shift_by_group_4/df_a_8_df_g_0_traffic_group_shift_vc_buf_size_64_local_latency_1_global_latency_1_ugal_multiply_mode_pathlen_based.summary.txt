wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 83;
shift_by_group = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50001,       0.5,    27.594,    27.594,         0,      5.1796
simulation,         2,      0.74,   0.45563,   0.50158,    1645.1,    830.41,         1,         0.0
simulation,         3,      0.62,   0.49694,   0.53206,     779.7,    539.25,         1,         0.0
simulation,         4,      0.56,   0.50911,   0.51998,    694.47,    373.94,         1,         0.0
simulation,         5,      0.53,   0.51342,   0.51216,    579.48,    224.17,         1,         0.0
simulation,         6,      0.51,   0.51004,   0.51002,    33.908,    33.911,         0,      5.1865
simulation,         7,      0.52,   0.51593,   0.51593,    410.65,    107.46,         1,         0.0
simulation,         8,       0.1,  0.099956,  0.099954,    12.356,    12.356,         0,      4.8119
simulation,         9,       0.2,   0.19986,   0.19985,    13.491,    13.491,         0,      4.9071
simulation,        10,       0.3,   0.29985,   0.29985,    15.068,    15.068,         0,      5.0209
simulation,        11,       0.4,   0.39975,   0.39975,    17.697,    17.698,         0,      5.1139
simulation,        12,       0.5,   0.50001,       0.5,    27.594,    27.594,         0,      5.1796
