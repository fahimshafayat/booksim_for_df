wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_L_four_hop_restricted;
seed = 81;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.41842,   0.45081,    613.36,    499.12,         1,         0.0
simulation,         2,      0.25,   0.24984,   0.24984,    36.237,    36.258,         0,      4.2345
simulation,         3,      0.37,   0.36374,   0.36383,    444.06,    196.54,         1,         0.0
simulation,         4,      0.31,   0.30943,   0.30944,    196.92,    105.94,         0,      4.2927
simulation,         5,      0.34,   0.33793,   0.33794,    381.82,    107.33,         1,         0.0
simulation,         6,      0.32,   0.31905,   0.31905,    320.58,    109.82,         0,       4.301
simulation,         7,      0.33,   0.32859,   0.32862,    398.36,    109.63,         0,      4.3084
simulation,         8,       0.1,   0.09992,  0.099918,    10.688,    10.688,         0,      4.1316
simulation,         9,       0.2,    0.1998,    0.1998,    12.034,    12.034,         0,      4.1856
simulation,        10,       0.3,    0.2998,   0.29985,    123.89,    95.842,         0,      4.2836
