wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = PAR_restricted_src_and_dst;
seed = 82;
shift_by_group = 3;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.36517,   0.41133,    1084.0,    852.72,         1,         0.0
simulation,         2,      0.25,   0.25007,   0.25006,    16.496,    16.496,         0,      5.6748
simulation,         3,      0.37,   0.35619,   0.35712,    707.85,    381.45,         1,         0.0
simulation,         4,      0.31,   0.31006,   0.31006,      20.8,    20.805,         0,      5.7213
simulation,         5,      0.34,   0.34004,   0.34006,    78.195,    75.536,         0,      5.7783
simulation,         6,      0.35,   0.34818,   0.34823,    306.95,    144.04,         1,         0.0
simulation,         7,       0.1,  0.099916,  0.099915,    13.731,    13.731,         0,      5.5127
simulation,         8,       0.2,   0.20003,   0.20002,     15.23,     15.23,         0,      5.6029
simulation,         9,       0.3,   0.30006,   0.30006,    18.772,    18.772,         0,      5.7164
