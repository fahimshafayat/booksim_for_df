wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_G_four_hop_restricted;
seed = 83;
shift_by_group = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.47623,    0.4755,    922.09,    367.09,         1,         0.0
simulation,         2,      0.25,   0.24999,   0.24999,    12.095,    12.095,         0,      4.2188
simulation,         3,      0.37,   0.36999,   0.36999,    14.221,    14.221,         0,      4.2603
simulation,         4,      0.43,      0.43,      0.43,    16.693,    16.692,         0,      4.2961
simulation,         5,      0.46,   0.45996,   0.45997,    20.394,    20.395,         0,      4.3129
simulation,         6,      0.48,   0.47853,   0.47828,    292.02,    107.58,         0,      4.3202
simulation,         7,      0.49,   0.47792,   0.47699,    536.82,     282.5,         1,         0.0
simulation,         8,       0.1,  0.099856,  0.099855,    10.774,    10.774,         0,      4.1838
simulation,         9,       0.2,   0.19995,   0.19995,    11.577,    11.577,         0,      4.2087
simulation,        10,       0.3,   0.29998,   0.29998,    12.719,    12.719,         0,       4.229
simulation,        11,       0.4,   0.39989,   0.39988,    15.149,    15.149,         0,      4.2778
