wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = PAR_restricted_src_and_dst;
seed = 83;
shift_by_group = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.38404,   0.42573,    882.66,    709.92,         1,         0.0
simulation,         2,      0.25,   0.24996,   0.24995,    16.301,    16.301,         0,      5.6207
simulation,         3,      0.37,   0.36905,   0.36906,    233.48,    100.24,         0,      5.7579
simulation,         4,      0.43,    0.3852,   0.39744,    745.46,    480.56,         1,         0.0
simulation,         5,       0.4,   0.38161,   0.38179,    808.44,    343.43,         1,         0.0
simulation,         6,      0.38,   0.37464,   0.37468,    499.02,    201.62,         1,         0.0
simulation,         7,       0.1,  0.099945,  0.099941,    13.625,    13.625,         0,       5.471
simulation,         8,       0.2,   0.19989,   0.19989,    15.049,    15.049,         0,      5.5469
simulation,         9,       0.3,    0.2999,   0.29989,    18.167,    18.167,         0,      5.6879
