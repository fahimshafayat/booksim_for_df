wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = PAR_restricted_src_and_dst;
seed = 81;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.3633,   0.41209,    1132.6,    879.96,         1,         0.0
simulation,         2,      0.25,   0.25006,   0.25006,    16.338,    16.338,         0,      5.6187
simulation,         3,      0.37,   0.35612,   0.35622,    741.36,    392.35,         1,         0.0
simulation,         4,      0.31,   0.31008,   0.31009,    32.075,    32.139,         0,      5.6791
simulation,         5,      0.34,   0.33907,   0.33909,    279.98,     131.9,         0,      5.7208
simulation,         6,      0.35,   0.34633,   0.34636,    508.03,    218.68,         1,         0.0
simulation,         7,       0.1,   0.10003,   0.10003,    13.633,    13.633,         0,       5.468
simulation,         8,       0.2,   0.20005,   0.20005,    15.079,    15.079,         0,      5.5492
simulation,         9,       0.3,   0.30009,   0.30009,    20.892,    20.894,         0,      5.6609
