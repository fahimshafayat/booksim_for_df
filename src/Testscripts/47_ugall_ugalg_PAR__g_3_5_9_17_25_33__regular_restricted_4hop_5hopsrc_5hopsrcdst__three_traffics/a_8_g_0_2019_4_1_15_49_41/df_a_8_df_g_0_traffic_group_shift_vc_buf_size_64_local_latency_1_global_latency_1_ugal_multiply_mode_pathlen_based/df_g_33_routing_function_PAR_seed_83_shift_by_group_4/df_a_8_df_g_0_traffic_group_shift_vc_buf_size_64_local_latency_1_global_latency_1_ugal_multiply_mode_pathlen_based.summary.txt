wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = PAR;
seed = 83;
shift_by_group = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.44488,   0.45448,    714.63,    357.39,         1,         0.0
simulation,         2,      0.25,   0.25005,   0.25004,    19.581,    19.581,         0,      6.3572
simulation,         3,      0.37,   0.36878,   0.36878,    361.16,    50.311,         0,      6.3713
simulation,         4,      0.43,   0.42067,   0.42067,    433.89,    133.79,         1,         0.0
simulation,         5,       0.4,    0.3976,   0.39759,    267.72,    64.047,         1,         0.0
simulation,         6,      0.38,   0.37844,   0.37847,    465.05,    55.444,         0,       6.362
simulation,         7,      0.39,   0.38799,   0.38801,    240.89,    62.136,         1,         0.0
simulation,         8,       0.1,  0.099999,  0.099999,    14.931,    14.931,         0,      6.0011
simulation,         9,       0.2,       0.2,   0.19999,    17.718,    17.718,         0,      6.2663
simulation,        10,       0.3,   0.30001,   0.30001,     22.25,     22.25,         0,       6.397
