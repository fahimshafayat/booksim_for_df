wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = UGAL_G;
seed = 82;
shift_by_group = 3;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35466,    0.3949,    929.62,    727.65,         1,         0.0
simulation,         2,      0.25,    0.2501,    0.2501,    17.722,    17.722,         0,      5.9828
simulation,         3,      0.37,   0.37012,   0.37012,    23.859,    23.859,         0,       6.138
simulation,         4,      0.43,    0.3846,    0.3959,    668.96,    433.12,         1,         0.0
simulation,         5,       0.4,   0.40013,   0.40011,    31.448,    31.449,         0,      6.1673
simulation,         6,      0.41,   0.40134,   0.40105,    459.13,    273.95,         1,         0.0
simulation,         7,       0.1,   0.10002,   0.10002,    14.422,    14.422,         0,      5.6263
simulation,         8,       0.2,   0.20005,   0.20005,    16.363,    16.363,         0,      5.8713
simulation,         9,       0.3,   0.30017,   0.30017,    19.388,    19.388,         0,      6.0606
simulation,        10,       0.4,   0.40013,   0.40011,    31.448,    31.449,         0,      6.1673
