internal_speedup = 4.0;
wait_for_tail_credit = 0;
num_vcs = 8;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
injection_rate_uses_flits = 1;
output_speedup = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
vc_alloc_delay = 1;
credit_delay = 2;
warmup_periods = 3;
sample_period = 10000;
packet_size = 1;
priority = none;
sw_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50015,   0.50015,    11.037,    11.037,         0,      3.6853
simulation,         2,      0.74,   0.74012,   0.74012,    13.301,    13.301,         0,      3.6351
simulation,         3,      0.86,   0.86005,   0.86005,    16.499,    16.499,         0,      3.6161
simulation,         4,      0.92,      0.92,   0.92002,    20.671,    20.672,         0,      3.6082
simulation,         5,      0.95,      0.95,      0.95,    25.526,    25.529,         0,      3.6035
simulation,         6,      0.97,   0.96996,   0.96997,     33.31,    33.322,         0,      3.6005
simulation,         7,      0.98,   0.97993,   0.97994,    42.099,    42.117,         0,      3.5997
simulation,         8,       0.1,   0.10003,   0.10003,    9.8791,    9.8793,         0,      3.8355
simulation,         9,       0.2,   0.19998,   0.19998,    10.032,    10.032,         0,      3.7876
simulation,        10,       0.3,   0.30011,   0.30011,    10.252,    10.252,         0,      3.7443
simulation,        11,       0.4,   0.40003,   0.40002,    10.574,    10.574,         0,      3.7114
simulation,        12,       0.5,   0.50015,   0.50015,    11.037,    11.037,         0,      3.6853
simulation,        13,       0.6,   0.59993,   0.59993,    11.697,    11.697,         0,      3.6626
simulation,        14,       0.7,   0.69994,   0.69993,    12.723,    12.723,         0,      3.6427
simulation,        15,       0.8,   0.79997,   0.79997,    14.503,    14.504,         0,      3.6248
simulation,        16,       0.9,   0.90004,   0.90005,    18.809,    18.811,         0,      3.6107
