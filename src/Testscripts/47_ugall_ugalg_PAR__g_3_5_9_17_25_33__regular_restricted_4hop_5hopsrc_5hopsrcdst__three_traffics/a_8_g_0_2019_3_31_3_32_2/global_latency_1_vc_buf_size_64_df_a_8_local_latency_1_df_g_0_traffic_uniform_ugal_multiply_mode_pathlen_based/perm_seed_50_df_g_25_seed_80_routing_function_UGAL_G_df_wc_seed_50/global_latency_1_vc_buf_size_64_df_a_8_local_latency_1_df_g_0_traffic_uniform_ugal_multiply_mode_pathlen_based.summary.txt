internal_speedup = 4.0;
wait_for_tail_credit = 0;
num_vcs = 8;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
injection_rate_uses_flits = 1;
output_speedup = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
vc_alloc_delay = 1;
credit_delay = 2;
warmup_periods = 3;
sample_period = 10000;
packet_size = 1;
priority = none;
sw_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_G;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50008,   0.50008,    11.513,    11.513,         0,       3.795
simulation,         2,      0.74,   0.74006,   0.74006,    15.831,    15.831,         0,      3.7767
simulation,         3,      0.86,   0.86006,   0.86005,    41.908,     41.92,         0,      3.8956
simulation,         4,      0.92,   0.80124,   0.82272,    1076.4,    448.09,         1,         0.0
simulation,         5,      0.89,   0.82424,   0.84376,     610.1,    357.02,         1,         0.0
simulation,         6,      0.87,   0.82403,   0.82479,    722.57,    431.45,         1,         0.0
simulation,         7,       0.1,    0.1001,    0.1001,    10.374,    10.374,         0,       4.069
simulation,         8,       0.2,   0.20016,   0.20016,    10.404,    10.405,         0,      3.9495
simulation,         9,       0.3,    0.3002,    0.3002,     10.59,    10.591,         0,      3.8748
simulation,        10,       0.4,   0.40011,   0.40011,     10.94,     10.94,         0,       3.827
simulation,        11,       0.5,   0.50008,   0.50008,    11.513,    11.513,         0,       3.795
simulation,        12,       0.6,   0.60001,   0.60001,    12.491,    12.491,         0,      3.7768
simulation,        13,       0.7,   0.70011,   0.70011,    14.408,    14.408,         0,      3.7722
simulation,        14,       0.8,   0.80006,   0.80005,    20.434,    20.436,         0,       3.801
