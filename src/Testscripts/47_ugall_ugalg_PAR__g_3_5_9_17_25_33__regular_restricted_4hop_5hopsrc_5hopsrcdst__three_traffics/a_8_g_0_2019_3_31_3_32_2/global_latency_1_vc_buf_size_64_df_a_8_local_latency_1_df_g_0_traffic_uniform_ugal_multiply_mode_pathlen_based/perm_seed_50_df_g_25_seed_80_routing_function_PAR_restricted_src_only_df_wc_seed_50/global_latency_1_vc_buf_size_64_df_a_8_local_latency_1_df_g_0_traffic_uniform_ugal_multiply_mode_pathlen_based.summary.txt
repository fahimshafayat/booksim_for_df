internal_speedup = 4.0;
wait_for_tail_credit = 0;
num_vcs = 8;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
injection_rate_uses_flits = 1;
output_speedup = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
vc_alloc_delay = 1;
credit_delay = 2;
warmup_periods = 3;
sample_period = 10000;
packet_size = 1;
priority = none;
sw_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 50;
perm_seed = 50;
routing_function = PAR_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50008,   0.50008,    13.208,    13.209,         0,      4.2418
simulation,         2,      0.74,   0.74006,   0.74006,    21.015,    21.016,         0,      4.1971
simulation,         3,      0.86,   0.76756,   0.79256,    965.94,    510.18,         1,         0.0
simulation,         4,       0.8,   0.76501,   0.76729,    1045.0,    477.44,         1,         0.0
simulation,         5,      0.77,   0.75613,   0.75812,    537.43,     352.3,         1,         0.0
simulation,         6,      0.75,   0.75009,   0.75005,    23.148,    23.148,         0,      4.2288
simulation,         7,      0.76,   0.75362,   0.75661,    374.76,    266.54,         0,      4.4943
simulation,         8,       0.1,    0.1001,    0.1001,    11.611,    11.611,         0,      4.6408
simulation,         9,       0.2,   0.20016,   0.20016,    11.869,    11.869,         0,       4.561
simulation,        10,       0.3,    0.3002,    0.3002,    12.112,    12.112,         0,       4.435
simulation,        11,       0.4,   0.40012,   0.40011,    12.514,    12.514,         0,      4.3259
simulation,        12,       0.5,   0.50008,   0.50008,    13.208,    13.209,         0,      4.2418
simulation,        13,       0.6,   0.60001,   0.60001,    14.468,    14.469,         0,      4.1817
simulation,        14,       0.7,   0.70011,   0.70011,    17.471,    17.472,         0,      4.1606
