internal_speedup = 4.0;
wait_for_tail_credit = 0;
num_vcs = 8;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
injection_rate_uses_flits = 1;
output_speedup = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
vc_alloc_delay = 1;
credit_delay = 2;
warmup_periods = 3;
sample_period = 10000;
packet_size = 1;
priority = none;
sw_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 50;
perm_seed = 50;
routing_function = PAR_restricted_src_and_dst;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50008,   0.50008,    13.294,    13.295,         0,      4.2565
simulation,         2,      0.74,   0.74007,   0.74006,    25.996,    25.997,         0,      4.3353
simulation,         3,      0.86,   0.77038,   0.79317,    908.61,    466.88,         1,         0.0
simulation,         4,       0.8,    0.7569,   0.75877,    1098.3,    479.28,         1,         0.0
simulation,         5,      0.77,   0.75191,   0.75398,    608.76,    352.81,         1,         0.0
simulation,         6,      0.75,   0.74645,   0.74694,    405.44,    250.67,         0,      4.5613
simulation,         7,      0.76,   0.75264,   0.75386,    495.17,    298.44,         1,         0.0
simulation,         8,       0.1,    0.1001,    0.1001,    11.695,    11.695,         0,      4.6797
simulation,         9,       0.2,   0.20016,   0.20016,     11.95,     11.95,         0,      4.5944
simulation,        10,       0.3,    0.3002,    0.3002,    12.184,    12.184,         0,        4.46
simulation,        11,       0.4,   0.40011,   0.40011,    12.584,    12.585,         0,      4.3442
simulation,        12,       0.5,   0.50008,   0.50008,    13.294,    13.295,         0,      4.2565
simulation,        13,       0.6,   0.60002,   0.60001,     14.64,    14.641,         0,      4.1981
simulation,        14,       0.7,   0.70011,   0.70011,    18.259,     18.26,         0,      4.2001
