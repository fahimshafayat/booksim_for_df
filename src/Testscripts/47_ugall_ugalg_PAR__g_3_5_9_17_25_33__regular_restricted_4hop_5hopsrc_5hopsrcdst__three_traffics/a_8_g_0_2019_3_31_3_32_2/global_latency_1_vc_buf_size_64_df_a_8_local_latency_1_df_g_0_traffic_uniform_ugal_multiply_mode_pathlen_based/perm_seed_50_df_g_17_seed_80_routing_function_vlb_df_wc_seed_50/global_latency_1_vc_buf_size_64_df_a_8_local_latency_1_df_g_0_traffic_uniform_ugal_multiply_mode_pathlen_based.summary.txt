internal_speedup = 4.0;
wait_for_tail_credit = 0;
num_vcs = 8;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
injection_rate_uses_flits = 1;
output_speedup = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
vc_alloc_delay = 1;
credit_delay = 2;
warmup_periods = 3;
sample_period = 10000;
packet_size = 1;
priority = none;
sw_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 50;
perm_seed = 50;
routing_function = vlb;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50021,   0.50025,    83.581,    83.609,         0,      6.2798
simulation,         2,      0.74,    0.5053,   0.56958,    1656.7,    1096.3,         1,         0.0
simulation,         3,      0.62,   0.50562,   0.56997,    991.26,    895.07,         1,         0.0
simulation,         4,      0.56,   0.50565,   0.55814,     549.8,    549.12,         1,         0.0
simulation,         5,      0.53,   0.50803,   0.51852,    1036.6,    920.63,         1,         0.0
simulation,         6,      0.51,   0.50896,   0.51006,    294.09,    292.58,         0,      6.2791
simulation,         7,      0.52,   0.51042,    0.5193,    574.77,    563.48,         1,         0.0
simulation,         8,       0.1,   0.10017,   0.10017,    15.183,    15.183,         0,      6.2807
simulation,         9,       0.2,    0.2002,    0.2002,    16.186,    16.186,         0,      6.2797
simulation,        10,       0.3,   0.30023,   0.30023,    18.087,    18.087,         0,      6.2806
simulation,        11,       0.4,   0.40025,   0.40026,    23.056,    23.056,         0,      6.2792
simulation,        12,       0.5,   0.50021,   0.50025,    83.581,    83.609,         0,      6.2798
