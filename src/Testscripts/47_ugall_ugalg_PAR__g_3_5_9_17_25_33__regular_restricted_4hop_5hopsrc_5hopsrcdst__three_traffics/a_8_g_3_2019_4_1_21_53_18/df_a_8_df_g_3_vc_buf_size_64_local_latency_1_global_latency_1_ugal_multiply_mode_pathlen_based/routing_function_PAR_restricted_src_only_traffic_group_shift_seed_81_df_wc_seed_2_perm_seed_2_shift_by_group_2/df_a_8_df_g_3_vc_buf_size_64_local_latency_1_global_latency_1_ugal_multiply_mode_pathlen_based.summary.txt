wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 3;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 2;
perm_seed = 2;
routing_function = PAR_restricted_src_only;
seed = 81;
shift_by_group = 2;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49981,    0.4998,    11.991,    11.992,         0,      3.9247
simulation,         2,      0.74,   0.74042,   0.74041,    20.836,    20.837,         0,      4.0165
simulation,         3,      0.86,   0.74693,    0.7929,    627.68,    458.41,         1,         0.0
simulation,         4,       0.8,   0.74771,   0.76908,    618.41,    420.37,         1,         0.0
simulation,         5,      0.77,    0.7496,   0.75124,     679.0,    423.06,         1,         0.0
simulation,         6,      0.75,   0.74978,    0.7502,    69.396,    69.407,         0,      4.1974
simulation,         7,      0.76,   0.74708,   0.75057,    512.07,    378.38,         1,         0.0
simulation,         8,      0.15,   0.15004,   0.15004,    10.517,    10.517,         0,      4.0603
simulation,         9,       0.3,   0.30023,   0.30022,    10.936,    10.936,         0,      3.9883
simulation,        10,      0.45,   0.44972,   0.44972,     11.65,     11.65,         0,      3.9386
simulation,        11,       0.6,   0.60002,   0.60001,    13.034,    13.034,         0,      3.9073
simulation,        12,      0.75,   0.74978,    0.7502,    69.396,    69.407,         0,      4.1974
