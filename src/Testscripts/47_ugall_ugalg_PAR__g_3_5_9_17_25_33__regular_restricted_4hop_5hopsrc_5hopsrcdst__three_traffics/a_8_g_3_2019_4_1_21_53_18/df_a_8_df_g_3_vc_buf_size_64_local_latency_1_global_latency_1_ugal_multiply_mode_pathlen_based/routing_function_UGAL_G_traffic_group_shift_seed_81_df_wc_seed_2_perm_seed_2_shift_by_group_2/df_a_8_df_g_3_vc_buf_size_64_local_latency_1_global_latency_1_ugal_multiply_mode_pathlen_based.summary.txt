wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 3;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 2;
perm_seed = 2;
routing_function = UGAL_G;
seed = 81;
shift_by_group = 2;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49981,    0.4998,    13.132,    13.133,         0,      4.1544
simulation,         2,      0.74,   0.74043,   0.74041,    37.132,    37.131,         0,      4.4693
simulation,         3,      0.86,   0.67559,   0.72952,    1001.6,    608.24,         1,         0.0
simulation,         4,       0.8,   0.68359,   0.73657,    629.59,    499.44,         1,         0.0
simulation,         5,      0.77,   0.69366,   0.71838,    852.48,    504.69,         1,         0.0
simulation,         6,      0.75,   0.73455,   0.74012,     293.0,    211.99,         0,      4.4518
simulation,         7,      0.76,   0.69937,   0.72417,     583.7,    422.79,         1,         0.0
simulation,         8,      0.15,   0.15004,   0.15004,    10.933,    10.933,         0,      4.2617
simulation,         9,       0.3,   0.30023,   0.30022,    11.421,    11.422,         0,       4.188
simulation,        10,      0.45,   0.44972,   0.44972,    12.485,    12.486,         0,      4.1554
simulation,        11,       0.6,   0.60001,   0.60001,    15.729,    15.729,         0,      4.1974
simulation,        12,      0.75,   0.73455,   0.74012,     293.0,    211.99,         0,      4.4518
