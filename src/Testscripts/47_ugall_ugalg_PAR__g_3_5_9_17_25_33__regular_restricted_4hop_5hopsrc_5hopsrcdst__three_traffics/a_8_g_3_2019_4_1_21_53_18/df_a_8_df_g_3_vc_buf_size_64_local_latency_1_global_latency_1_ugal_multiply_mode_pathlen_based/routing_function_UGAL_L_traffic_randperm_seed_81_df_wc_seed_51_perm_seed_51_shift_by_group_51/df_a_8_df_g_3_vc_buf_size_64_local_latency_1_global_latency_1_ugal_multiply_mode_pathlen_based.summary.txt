wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 3;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 51;
perm_seed = 51;
routing_function = UGAL_L;
seed = 81;
shift_by_group = 51;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49981,    0.4998,    10.442,    10.443,         0,      3.5586
simulation,         2,      0.74,   0.74042,   0.74041,    12.171,    12.172,         0,      3.5045
simulation,         3,      0.86,   0.86033,   0.86032,     14.95,     14.95,         0,      3.5194
simulation,         4,      0.92,   0.91995,   0.91996,    18.506,    18.508,         0,      3.5437
simulation,         5,      0.95,   0.94689,   0.94682,    196.26,    45.412,         0,      3.5608
simulation,         6,      0.97,   0.96099,   0.96091,    534.61,    84.305,         1,         0.0
simulation,         7,      0.96,   0.95532,   0.95522,     347.8,    61.289,         0,      3.5704
simulation,         8,       0.2,   0.20005,   0.20005,    9.7875,    9.7879,         0,      3.7013
simulation,         9,       0.4,   0.40001,       0.4,    10.137,    10.137,         0,      3.5981
simulation,        10,       0.6,   0.60002,   0.60001,    10.906,    10.907,         0,      3.5262
simulation,        11,       0.8,   0.80031,   0.80031,    13.224,    13.225,         0,      3.5079
