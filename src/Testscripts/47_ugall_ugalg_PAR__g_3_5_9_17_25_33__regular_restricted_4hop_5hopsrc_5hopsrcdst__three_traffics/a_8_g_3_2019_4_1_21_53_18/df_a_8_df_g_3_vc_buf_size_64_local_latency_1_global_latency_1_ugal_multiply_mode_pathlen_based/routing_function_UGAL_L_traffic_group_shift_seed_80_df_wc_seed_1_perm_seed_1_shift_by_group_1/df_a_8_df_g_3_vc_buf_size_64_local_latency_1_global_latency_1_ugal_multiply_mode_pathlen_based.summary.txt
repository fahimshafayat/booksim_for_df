wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 3;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 1;
perm_seed = 1;
routing_function = UGAL_L;
seed = 80;
shift_by_group = 1;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4999,   0.49991,    13.407,    13.407,         0,      4.2066
simulation,         2,      0.74,   0.61512,   0.67173,    860.14,    655.22,         1,         0.0
simulation,         3,      0.62,   0.61965,   0.61964,    21.533,    21.534,         0,      4.2792
simulation,         4,      0.68,   0.62424,   0.65236,    739.77,    576.74,         1,         0.0
simulation,         5,      0.65,   0.64963,   0.64961,    32.733,    32.737,         0,      4.3837
simulation,         6,      0.66,   0.65956,   0.65958,    40.555,    40.573,         0,      4.4234
simulation,         7,      0.67,   0.66946,   0.66957,    51.057,     51.07,         0,      4.4475
simulation,         8,      0.15,   0.14998,   0.14998,    11.277,    11.277,         0,      4.4236
simulation,         9,       0.3,   0.30008,   0.30008,    11.668,    11.668,         0,      4.3054
simulation,        10,      0.45,   0.44975,   0.44976,    12.665,    12.665,         0,        4.22
simulation,        11,       0.6,   0.59967,   0.59967,    18.295,    18.296,         0,      4.2397
