wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 3;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 1;
perm_seed = 1;
routing_function = PAR_four_hop_restricted;
seed = 82;
shift_by_group = 1;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49996,   0.49996,    11.991,    11.991,         0,       3.924
simulation,         2,      0.74,   0.74014,   0.74014,    20.712,    20.713,         0,      4.0146
simulation,         3,      0.86,   0.74649,    0.7927,     629.0,    447.35,         1,         0.0
simulation,         4,       0.8,   0.74796,   0.76712,    628.18,    399.57,         1,         0.0
simulation,         5,      0.77,   0.74833,   0.75531,    705.59,    467.48,         1,         0.0
simulation,         6,      0.75,   0.74964,   0.74992,    70.691,    70.678,         0,      4.1892
simulation,         7,      0.76,   0.74788,   0.75165,    533.32,    402.82,         1,         0.0
simulation,         8,      0.15,   0.14962,   0.14963,    10.518,    10.518,         0,      4.0614
simulation,         9,       0.3,   0.29984,   0.29986,    10.933,    10.933,         0,      3.9896
simulation,        10,      0.45,   0.44994,   0.44994,    11.637,    11.637,         0,      3.9355
simulation,        11,       0.6,   0.59988,   0.59988,    13.015,    13.015,         0,      3.9057
simulation,        12,      0.75,   0.74964,   0.74992,    70.691,    70.678,         0,      4.1892
