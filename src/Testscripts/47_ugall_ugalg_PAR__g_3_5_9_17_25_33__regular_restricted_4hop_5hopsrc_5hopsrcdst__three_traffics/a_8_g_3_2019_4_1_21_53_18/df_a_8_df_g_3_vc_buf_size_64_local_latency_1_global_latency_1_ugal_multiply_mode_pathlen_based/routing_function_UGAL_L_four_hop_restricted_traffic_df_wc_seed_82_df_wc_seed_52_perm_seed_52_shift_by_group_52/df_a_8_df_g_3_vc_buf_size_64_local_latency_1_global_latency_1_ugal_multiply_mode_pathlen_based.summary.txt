wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 3;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 52;
perm_seed = 52;
routing_function = UGAL_L_four_hop_restricted;
seed = 82;
shift_by_group = 52;
traffic = df_wc;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49995,   0.49996,    11.919,    11.919,         0,      3.9234
simulation,         2,      0.74,   0.74014,   0.74014,    52.386,    52.413,         0,       4.015
simulation,         3,      0.86,   0.73815,   0.78995,    684.72,    507.12,         1,         0.0
simulation,         4,       0.8,   0.73858,   0.76449,    716.55,    506.19,         1,         0.0
simulation,         5,      0.77,   0.73798,   0.74483,    820.79,    612.95,         1,         0.0
simulation,         6,      0.75,   0.74876,   0.75004,    173.05,    174.54,         0,      3.8667
simulation,         7,      0.76,    0.7426,   0.74736,    652.95,    516.47,         1,         0.0
simulation,         8,      0.15,   0.14962,   0.14963,    10.247,    10.247,         0,      3.9592
simulation,         9,       0.3,   0.29984,   0.29986,    10.735,    10.735,         0,      3.9472
simulation,        10,      0.45,   0.44993,   0.44994,    11.518,    11.518,         0,      3.9289
simulation,        11,       0.6,   0.59987,   0.59988,    13.369,     13.37,         0,      3.9217
simulation,        12,      0.75,   0.74876,   0.75004,    173.05,    174.54,         0,      3.8667
