wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 3;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 50;
perm_seed = 50;
routing_function = PAR_four_hop_restricted;
seed = 80;
shift_by_group = 50;
traffic = df_wc;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4999,   0.49991,    12.106,    12.106,         0,       3.959
simulation,         2,      0.74,   0.73959,   0.73956,    21.097,    21.097,         0,      4.0432
simulation,         3,      0.86,   0.74616,   0.79445,    621.28,    462.07,         1,         0.0
simulation,         4,       0.8,   0.74803,   0.76982,    636.71,    420.06,         1,         0.0
simulation,         5,      0.77,   0.74881,   0.75448,    712.84,    428.26,         1,         0.0
simulation,         6,      0.75,   0.74964,   0.74948,    66.447,    66.417,         0,      4.1806
simulation,         7,      0.76,    0.7482,   0.75121,    536.59,    385.69,         1,         0.0
simulation,         8,      0.15,   0.14998,   0.14998,    10.577,    10.577,         0,      4.0888
simulation,         9,       0.3,   0.30007,   0.30008,    10.998,    10.998,         0,      4.0172
simulation,        10,      0.45,   0.44975,   0.44976,     11.74,     11.74,         0,      3.9703
simulation,        11,       0.6,   0.59967,   0.59967,    13.183,    13.183,         0,      3.9439
simulation,        12,      0.75,   0.74964,   0.74948,    66.447,    66.417,         0,      4.1806
