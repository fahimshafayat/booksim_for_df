wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 3;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 1;
perm_seed = 1;
routing_function = PAR_four_hop_restricted;
seed = 80;
shift_by_group = 1;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4999,   0.49991,    11.989,    11.989,         0,      3.9243
simulation,         2,      0.74,    0.7396,   0.73956,    20.502,    20.502,         0,      4.0112
simulation,         3,      0.86,   0.74638,    0.7909,    609.67,    447.48,         1,         0.0
simulation,         4,       0.8,    0.7475,   0.76605,    616.39,    412.41,         1,         0.0
simulation,         5,      0.77,   0.74956,   0.75238,    700.19,    435.01,         1,         0.0
simulation,         6,      0.75,    0.7497,   0.74951,     49.13,     49.13,         0,      4.1663
simulation,         7,      0.76,    0.7487,   0.75149,    527.83,    404.92,         1,         0.0
simulation,         8,      0.15,   0.14998,   0.14998,    10.519,     10.52,         0,      4.0614
simulation,         9,       0.3,   0.30007,   0.30008,    10.929,    10.929,         0,      3.9879
simulation,        10,      0.45,   0.44974,   0.44976,    11.645,    11.645,         0,      3.9377
simulation,        11,       0.6,   0.59967,   0.59967,    13.028,    13.028,         0,       3.907
simulation,        12,      0.75,    0.7497,   0.74951,     49.13,     49.13,         0,      4.1663
