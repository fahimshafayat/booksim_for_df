wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 3;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 53;
perm_seed = 53;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 83;
shift_by_group = 53;
traffic = df_wc;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50016,   0.50016,    11.938,    11.938,         0,      3.9287
simulation,         2,      0.74,   0.74006,   0.74006,    52.173,    52.202,         0,      4.0173
simulation,         3,      0.86,     0.736,   0.78717,    690.58,    507.44,         1,         0.0
simulation,         4,       0.8,    0.7379,   0.76306,    717.51,    510.21,         1,         0.0
simulation,         5,      0.77,   0.73971,   0.74751,    814.04,    578.17,         1,         0.0
simulation,         6,      0.75,   0.74812,   0.74983,    226.73,    226.66,         0,      3.8715
simulation,         7,      0.76,   0.74186,    0.7467,    629.96,    521.49,         1,         0.0
simulation,         8,      0.15,   0.15028,   0.15028,    10.254,    10.254,         0,      3.9615
simulation,         9,       0.3,   0.30041,   0.30042,    10.745,    10.745,         0,      3.9508
simulation,        10,      0.45,   0.45034,   0.45034,    11.539,     11.54,         0,      3.9332
simulation,        11,       0.6,   0.59991,   0.59989,    13.381,    13.381,         0,      3.9265
simulation,        12,      0.75,   0.74812,   0.74983,    226.73,    226.66,         0,      3.8715
