wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 3;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 1;
perm_seed = 1;
routing_function = PAR;
seed = 80;
shift_by_group = 1;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49991,   0.49991,     13.47,     13.47,         0,      4.3294
simulation,         2,      0.74,   0.73961,   0.73956,    44.069,    44.069,         0,      4.6274
simulation,         3,      0.86,   0.73706,   0.78752,    691.79,    480.48,         1,         0.0
simulation,         4,       0.8,   0.74072,    0.7605,    700.98,    421.01,         1,         0.0
simulation,         5,      0.77,   0.74197,    0.7409,    849.14,    450.48,         1,         0.0
simulation,         6,      0.75,   0.74965,   0.74948,    117.68,    117.63,         0,      4.6655
simulation,         7,      0.76,   0.74337,   0.74181,     501.1,    352.29,         1,         0.0
simulation,         8,      0.15,   0.14997,   0.14998,    12.088,    12.088,         0,       4.776
simulation,         9,       0.3,   0.30007,   0.30008,    12.201,    12.201,         0,       4.505
simulation,        10,      0.45,   0.44974,   0.44976,    12.954,    12.954,         0,      4.3585
simulation,        11,       0.6,   0.59966,   0.59967,    15.621,    15.621,         0,      4.3212
simulation,        12,      0.75,   0.74965,   0.74948,    117.68,    117.63,         0,      4.6655
