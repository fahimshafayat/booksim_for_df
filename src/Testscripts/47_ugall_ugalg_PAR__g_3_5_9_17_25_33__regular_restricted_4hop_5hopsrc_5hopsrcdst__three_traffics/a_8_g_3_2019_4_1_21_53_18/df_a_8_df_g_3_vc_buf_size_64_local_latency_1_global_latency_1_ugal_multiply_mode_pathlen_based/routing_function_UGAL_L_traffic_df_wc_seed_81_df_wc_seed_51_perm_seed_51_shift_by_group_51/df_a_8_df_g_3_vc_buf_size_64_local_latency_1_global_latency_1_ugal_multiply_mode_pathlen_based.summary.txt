wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 3;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 51;
perm_seed = 51;
routing_function = UGAL_L;
seed = 81;
shift_by_group = 51;
traffic = df_wc;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49981,    0.4998,    13.417,    13.417,         0,      4.2105
simulation,         2,      0.74,   0.61846,   0.67512,    821.03,    640.33,         1,         0.0
simulation,         3,      0.62,   0.62012,   0.62008,     21.14,    21.143,         0,      4.2825
simulation,         4,      0.68,   0.64144,   0.66102,    500.52,    391.34,         1,         0.0
simulation,         5,      0.65,   0.65017,   0.65013,     31.84,    31.847,         0,      4.3814
simulation,         6,      0.66,   0.66024,   0.66017,    39.719,    39.727,         0,      4.4221
simulation,         7,      0.67,    0.6702,   0.67018,    50.516,    50.529,         0,      4.4475
simulation,         8,      0.15,   0.15004,   0.15004,    11.273,    11.274,         0,      4.4224
simulation,         9,       0.3,   0.30023,   0.30022,    11.675,    11.675,         0,       4.307
simulation,        10,      0.45,   0.44973,   0.44972,    12.677,    12.677,         0,      4.2247
simulation,        11,       0.6,   0.60001,   0.60001,     18.13,    18.131,         0,      4.2445
