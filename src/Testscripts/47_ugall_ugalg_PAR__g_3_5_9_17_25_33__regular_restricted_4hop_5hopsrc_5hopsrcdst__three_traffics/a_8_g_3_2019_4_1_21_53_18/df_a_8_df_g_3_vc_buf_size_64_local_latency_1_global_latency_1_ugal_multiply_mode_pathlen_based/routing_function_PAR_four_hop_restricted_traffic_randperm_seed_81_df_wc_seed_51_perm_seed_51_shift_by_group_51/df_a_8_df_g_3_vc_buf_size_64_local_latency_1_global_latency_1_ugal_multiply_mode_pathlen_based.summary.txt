wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 3;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 51;
perm_seed = 51;
routing_function = PAR_four_hop_restricted;
seed = 81;
shift_by_group = 51;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49981,    0.4998,    10.145,    10.146,         0,      3.4442
simulation,         2,      0.74,   0.74043,   0.74041,    11.445,    11.446,         0,      3.3913
simulation,         3,      0.86,   0.86033,   0.86032,    12.901,    12.902,         0,       3.378
simulation,         4,      0.92,   0.91997,   0.91996,    14.254,    14.255,         0,      3.3767
simulation,         5,      0.95,   0.94999,      0.95,    15.322,    15.322,         0,      3.3795
simulation,         6,      0.97,   0.96993,   0.96993,    16.345,    16.346,         0,      3.3832
simulation,         7,      0.98,   0.97993,   0.97994,    17.008,    17.009,         0,      3.3841
simulation,         8,       0.2,   0.20006,   0.20005,    9.4425,    9.4428,         0,      3.5376
simulation,         9,       0.4,       0.4,       0.4,    9.8427,    9.8429,         0,      3.4716
simulation,        10,       0.6,   0.60002,   0.60001,    10.551,    10.551,         0,       3.419
simulation,        11,       0.8,   0.80031,   0.80031,    12.045,    12.046,         0,      3.3834
