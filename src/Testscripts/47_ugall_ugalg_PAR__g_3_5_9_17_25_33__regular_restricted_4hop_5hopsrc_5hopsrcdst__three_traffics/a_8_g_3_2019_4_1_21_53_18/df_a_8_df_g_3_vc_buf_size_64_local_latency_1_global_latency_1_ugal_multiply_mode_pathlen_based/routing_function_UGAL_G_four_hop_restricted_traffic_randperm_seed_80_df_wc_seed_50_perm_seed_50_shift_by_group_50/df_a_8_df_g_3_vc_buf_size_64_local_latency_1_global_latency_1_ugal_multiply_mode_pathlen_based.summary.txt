wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 3;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_G_four_hop_restricted;
seed = 80;
shift_by_group = 50;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4999,   0.49991,    9.5973,    9.5974,         0,      3.2812
simulation,         2,      0.74,   0.73956,   0.73956,    10.705,    10.705,         0,      3.2552
simulation,         3,      0.86,    0.8597,    0.8597,    11.893,    11.893,         0,      3.2496
simulation,         4,      0.92,   0.91982,   0.91981,     12.97,    12.971,         0,      3.2519
simulation,         5,      0.95,   0.94975,   0.94974,    13.736,    13.737,         0,      3.2549
simulation,         6,      0.97,   0.96996,   0.96996,    14.444,    14.445,         0,      3.2574
simulation,         7,      0.98,   0.97991,   0.97991,     14.93,    14.931,         0,      3.2593
simulation,         8,       0.2,       0.2,       0.2,    8.9325,    8.9328,         0,      3.3215
simulation,         9,       0.4,   0.39994,   0.39996,    9.3202,    9.3204,         0,      3.2941
simulation,        10,       0.6,   0.59967,   0.59967,    9.9559,    9.9561,         0,      3.2698
simulation,        11,       0.8,   0.79969,    0.7997,     11.19,     11.19,         0,      3.2511
