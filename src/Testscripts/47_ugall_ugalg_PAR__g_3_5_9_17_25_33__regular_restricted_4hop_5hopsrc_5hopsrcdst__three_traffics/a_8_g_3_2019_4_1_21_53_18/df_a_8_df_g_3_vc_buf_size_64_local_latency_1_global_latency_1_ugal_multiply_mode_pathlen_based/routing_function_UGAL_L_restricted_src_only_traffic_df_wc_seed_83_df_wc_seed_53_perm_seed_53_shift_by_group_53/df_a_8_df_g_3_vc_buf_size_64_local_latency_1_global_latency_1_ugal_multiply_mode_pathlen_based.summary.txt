wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 3;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 53;
perm_seed = 53;
routing_function = UGAL_L_restricted_src_only;
seed = 83;
shift_by_group = 53;
traffic = df_wc;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50015,   0.50016,    11.939,    11.939,         0,      3.9292
simulation,         2,      0.74,   0.74008,   0.74006,    52.196,    52.226,         0,       4.016
simulation,         3,      0.86,   0.73838,   0.79032,    683.65,    506.45,         1,         0.0
simulation,         4,       0.8,    0.7382,   0.76425,    709.26,    506.32,         1,         0.0
simulation,         5,      0.77,   0.74115,   0.74594,    804.59,    579.35,         1,         0.0
simulation,         6,      0.75,   0.74729,   0.74944,    254.41,    251.03,         0,      3.8611
simulation,         7,      0.76,   0.74341,   0.74843,    654.22,     514.3,         1,         0.0
simulation,         8,      0.15,   0.15028,   0.15028,    10.255,    10.255,         0,      3.9625
simulation,         9,       0.3,   0.30041,   0.30042,    10.747,    10.747,         0,      3.9511
simulation,        10,      0.45,   0.45034,   0.45034,     11.54,     11.54,         0,       3.934
simulation,        11,       0.6,   0.59989,   0.59989,    13.375,    13.376,         0,      3.9276
simulation,        12,      0.75,   0.74729,   0.74944,    254.41,    251.03,         0,      3.8611
