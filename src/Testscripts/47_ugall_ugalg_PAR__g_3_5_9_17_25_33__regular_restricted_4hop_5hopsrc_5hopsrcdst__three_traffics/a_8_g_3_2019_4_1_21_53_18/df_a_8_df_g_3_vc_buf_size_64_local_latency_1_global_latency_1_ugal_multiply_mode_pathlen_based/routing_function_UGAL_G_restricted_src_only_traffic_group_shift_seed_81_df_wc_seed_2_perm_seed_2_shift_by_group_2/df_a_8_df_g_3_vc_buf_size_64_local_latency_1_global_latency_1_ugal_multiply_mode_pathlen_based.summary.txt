wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 3;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 2;
perm_seed = 2;
routing_function = UGAL_G_restricted_src_only;
seed = 81;
shift_by_group = 2;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4998,    0.4998,    11.665,    11.665,         0,      3.8125
simulation,         2,      0.74,    0.7404,   0.74041,      19.9,      19.9,         0,      3.7975
simulation,         3,      0.86,   0.71769,   0.76971,    756.24,    526.69,         1,         0.0
simulation,         4,       0.8,   0.73209,   0.75597,     746.0,    474.52,         1,         0.0
simulation,         5,      0.77,   0.74322,   0.74459,    711.94,    449.51,         1,         0.0
simulation,         6,      0.75,   0.74993,    0.7502,    53.833,    53.825,         0,      3.8144
simulation,         7,      0.76,   0.74612,   0.74805,    511.24,    408.03,         1,         0.0
simulation,         8,      0.15,   0.15004,   0.15004,    10.116,    10.116,         0,       3.888
simulation,         9,       0.3,   0.30022,   0.30022,    10.555,    10.555,         0,        3.85
simulation,        10,      0.45,   0.44972,   0.44972,    11.306,    11.306,         0,      3.8218
simulation,        11,       0.6,   0.60001,   0.60001,    12.706,    12.706,         0,      3.7971
simulation,        12,      0.75,   0.74993,    0.7502,    53.833,    53.825,         0,      3.8144
