wait_for_tail_credit = 0;
injection_rate_uses_flits = 1;
sim_count = 1;
vc_alloc_delay = 1;
input_speedup = 1;
sw_allocator = separable_input_first;
internal_speedup = 4.0;
st_final_delay = 1;
num_vcs = 8;
warmup_periods = 3;
packet_size = 1;
sample_period = 10000;
alloc_iters = 1;
output_speedup = 1;
routing_delay = 0;
vc_allocator = separable_input_first;
priority = none;
sw_alloc_delay = 1;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = PAR_restricted_src_only;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.38338,   0.42583,    901.16,    728.67,         1,         0.0
simulation,         2,      0.25,      0.25,   0.24999,    15.652,    15.652,         0,      5.3647
simulation,         3,      0.37,   0.36019,    0.3609,    573.96,    317.47,         1,         0.0
simulation,         4,      0.31,   0.31002,   0.31002,    20.416,    20.433,         0,      5.4149
simulation,         5,      0.34,   0.33919,   0.33921,    294.43,     102.8,         0,      5.4521
simulation,         6,      0.35,   0.34723,   0.34724,    504.87,    190.98,         1,         0.0
simulation,         7,       0.1,  0.099977,  0.099975,    13.148,    13.148,         0,      5.2367
simulation,         8,       0.2,   0.20002,   0.20003,    14.478,    14.478,         0,      5.3014
simulation,         9,       0.3,   0.29997,   0.29997,    17.747,    17.748,         0,      5.4066
