wait_for_tail_credit = 0;
injection_rate_uses_flits = 1;
sim_count = 1;
vc_alloc_delay = 1;
input_speedup = 1;
sw_allocator = separable_input_first;
internal_speedup = 4.0;
st_final_delay = 1;
num_vcs = 8;
warmup_periods = 3;
packet_size = 1;
sample_period = 10000;
alloc_iters = 1;
output_speedup = 1;
routing_delay = 0;
vc_allocator = separable_input_first;
priority = none;
sw_alloc_delay = 1;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_G;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50013,   0.50015,    28.779,     28.78,         0,      5.6723
simulation,         2,      0.74,   0.47381,   0.52388,    1592.6,    856.02,         1,         0.0
simulation,         3,      0.62,   0.50825,   0.54559,    708.86,    521.53,         1,         0.0
simulation,         4,      0.56,   0.55986,      0.56,    60.895,     60.89,         0,      5.7547
simulation,         5,      0.59,   0.51914,   0.54808,    455.99,    371.32,         1,         0.0
simulation,         6,      0.57,   0.52636,   0.53705,    563.92,    322.25,         1,         0.0
simulation,         7,       0.1,   0.10003,   0.10003,    12.441,    12.442,         0,       4.947
simulation,         8,       0.2,   0.19998,   0.19998,    13.661,    13.661,         0,      5.0302
simulation,         9,       0.3,   0.30012,   0.30011,    16.191,    16.191,         0,       5.229
simulation,        10,       0.4,   0.40003,   0.40002,    20.423,    20.423,         0,      5.4993
simulation,        11,       0.5,   0.50013,   0.50015,    28.779,     28.78,         0,      5.6723
