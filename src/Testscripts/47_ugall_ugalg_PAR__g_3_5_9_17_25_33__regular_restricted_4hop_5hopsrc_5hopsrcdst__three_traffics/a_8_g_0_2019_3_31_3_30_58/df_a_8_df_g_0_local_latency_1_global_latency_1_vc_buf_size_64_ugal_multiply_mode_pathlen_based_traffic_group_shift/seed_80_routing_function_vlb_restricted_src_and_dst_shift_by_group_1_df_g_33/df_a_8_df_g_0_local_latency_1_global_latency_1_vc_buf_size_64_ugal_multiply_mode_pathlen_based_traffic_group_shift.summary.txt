wait_for_tail_credit = 0;
injection_rate_uses_flits = 1;
sim_count = 1;
vc_alloc_delay = 1;
input_speedup = 1;
sw_allocator = separable_input_first;
internal_speedup = 4.0;
st_final_delay = 1;
num_vcs = 8;
warmup_periods = 3;
packet_size = 1;
sample_period = 10000;
alloc_iters = 1;
output_speedup = 1;
routing_delay = 0;
vc_allocator = separable_input_first;
priority = none;
sw_alloc_delay = 1;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = vlb_restricted_src_and_dst;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.24176,   0.29332,    2050.9,    1499.6,         1,         0.0
simulation,         2,      0.25,   0.23295,   0.24488,    426.88,    395.88,         1,         0.0
simulation,         3,      0.13,   0.13002,   0.13001,    13.627,    13.627,         0,      5.4336
simulation,         4,      0.19,   0.18999,   0.18999,    14.519,    14.519,         0,      5.4335
simulation,         5,      0.22,    0.2191,   0.21937,    247.28,    130.74,         0,      5.4331
simulation,         6,      0.23,   0.22443,   0.22616,    393.55,    337.75,         1,         0.0
simulation,         7,      0.05,  0.049959,   0.04996,    13.083,    13.083,         0,      5.4332
simulation,         8,       0.1,  0.099976,  0.099975,    13.381,    13.381,         0,      5.4331
simulation,         9,      0.15,      0.15,      0.15,    13.834,    13.834,         0,      5.4336
simulation,        10,       0.2,   0.20002,   0.20003,    14.838,    14.838,         0,      5.4333
