wait_for_tail_credit = 0;
injection_rate_uses_flits = 1;
sim_count = 1;
vc_alloc_delay = 1;
input_speedup = 1;
sw_allocator = separable_input_first;
internal_speedup = 4.0;
st_final_delay = 1;
num_vcs = 8;
warmup_periods = 3;
packet_size = 1;
sample_period = 10000;
alloc_iters = 1;
output_speedup = 1;
routing_delay = 0;
vc_allocator = separable_input_first;
priority = none;
sw_alloc_delay = 1;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = PAR_restricted_src_and_dst;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.34832,   0.39946,    1252.0,     958.4,         1,         0.0
simulation,         2,      0.25,   0.24999,   0.24999,    16.219,    16.219,         0,      5.5209
simulation,         3,      0.37,   0.34264,   0.35356,    569.91,    445.34,         1,         0.0
simulation,         4,      0.31,   0.30993,   0.30991,    104.84,    92.229,         0,      5.6626
simulation,         5,      0.34,     0.332,   0.33228,    499.68,    302.73,         1,         0.0
simulation,         6,      0.32,   0.31896,   0.31899,    378.83,    150.24,         0,      5.6528
simulation,         7,      0.33,   0.32576,   0.32583,    464.19,    247.06,         1,         0.0
simulation,         8,       0.1,  0.099976,  0.099975,     13.45,    13.451,         0,      5.3834
simulation,         9,       0.2,   0.20003,   0.20003,    14.843,    14.843,         0,      5.4527
simulation,        10,       0.3,   0.30001,   0.29997,    49.461,    49.636,         0,      5.6234
