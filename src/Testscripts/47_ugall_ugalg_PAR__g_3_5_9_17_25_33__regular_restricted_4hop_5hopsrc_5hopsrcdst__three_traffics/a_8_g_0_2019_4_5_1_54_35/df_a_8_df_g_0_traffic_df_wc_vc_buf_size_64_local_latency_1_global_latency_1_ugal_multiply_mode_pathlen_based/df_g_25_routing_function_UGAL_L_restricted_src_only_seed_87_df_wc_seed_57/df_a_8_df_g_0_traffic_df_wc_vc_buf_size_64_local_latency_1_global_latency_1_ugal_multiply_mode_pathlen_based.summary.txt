wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 57;
routing_function = UGAL_L_restricted_src_only;
seed = 87;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.29375,   0.33974,    1181.7,     889.3,         1,         0.0
simulation,         2,      0.25,   0.24997,   0.24997,    29.499,    29.538,         0,       5.334
simulation,         3,      0.37,   0.34281,   0.35253,    451.89,    317.64,         1,         0.0
simulation,         4,      0.31,   0.30983,   0.30983,    44.321,    39.968,         0,      5.3898
simulation,         5,      0.34,   0.33326,   0.33405,     539.2,    176.04,         1,         0.0
simulation,         6,      0.32,   0.31921,   0.31919,    243.74,    65.618,         0,      5.3971
simulation,         7,      0.33,   0.32761,   0.32766,    327.86,    85.897,         1,         0.0
simulation,         8,       0.1,  0.099989,  0.099988,    26.433,    26.453,         0,       4.969
simulation,         9,       0.2,   0.20004,   0.20004,    29.931,    29.964,         0,      5.2627
simulation,        10,       0.3,   0.29991,   0.29991,    30.394,    30.439,         0,      5.3819
