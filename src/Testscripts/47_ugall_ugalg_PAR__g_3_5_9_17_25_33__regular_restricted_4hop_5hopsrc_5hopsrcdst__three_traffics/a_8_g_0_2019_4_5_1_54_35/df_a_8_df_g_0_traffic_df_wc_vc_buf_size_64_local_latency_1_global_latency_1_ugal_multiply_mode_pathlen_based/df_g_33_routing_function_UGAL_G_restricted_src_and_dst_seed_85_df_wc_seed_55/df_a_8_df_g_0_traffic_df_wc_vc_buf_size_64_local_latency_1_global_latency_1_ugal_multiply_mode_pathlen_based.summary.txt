wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 55;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 85;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.47677,    0.4764,    632.89,    235.61,         1,         0.0
simulation,         2,      0.25,   0.25011,   0.25012,    15.689,    15.689,         0,      5.5273
simulation,         3,      0.37,   0.37005,   0.37006,    18.553,    18.553,         0,      5.6153
simulation,         4,      0.43,   0.43005,   0.43005,    22.038,    22.038,         0,      5.6442
simulation,         5,      0.46,   0.46001,   0.46002,    26.364,    26.364,         0,      5.6581
simulation,         6,      0.48,   0.47996,   0.47998,    33.944,    33.946,         0,       5.668
simulation,         7,      0.49,   0.48971,   0.48966,    66.568,     53.47,         0,       5.673
simulation,         8,       0.1,   0.10004,   0.10004,     13.71,     13.71,         0,      5.3076
simulation,         9,       0.2,   0.20003,   0.20003,     14.91,     14.91,         0,      5.4629
simulation,        10,       0.3,   0.30011,   0.30011,    16.626,    16.626,         0,      5.5719
simulation,        11,       0.4,   0.40008,   0.40008,    19.904,    19.904,         0,      5.6302
