wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 54;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 84;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49984,   0.49984,    26.431,    26.431,         0,      5.3415
simulation,         2,      0.74,   0.47378,    0.5179,    1478.1,    764.41,         1,         0.0
simulation,         3,      0.62,   0.50938,   0.54099,    676.27,     490.1,         1,         0.0
simulation,         4,      0.56,   0.52041,   0.52917,    543.34,    302.14,         1,         0.0
simulation,         5,      0.53,   0.52574,   0.52566,    423.75,    150.26,         1,         0.0
simulation,         6,      0.51,   0.50988,   0.50986,    29.252,    29.251,         0,      5.3479
simulation,         7,      0.52,   0.51986,   0.51986,    34.892,    34.891,         0,      5.3545
simulation,         8,       0.1,       0.1,  0.099998,    12.611,    12.611,         0,      4.9226
simulation,         9,       0.2,   0.19993,   0.19992,    13.836,    13.837,         0,      5.0377
simulation,        10,       0.3,   0.29987,   0.29986,    15.569,    15.569,         0,      5.1733
simulation,        11,       0.4,   0.39989,   0.39988,    18.263,    18.263,         0,      5.2749
simulation,        12,       0.5,   0.49984,   0.49984,    26.431,    26.431,         0,      5.3415
