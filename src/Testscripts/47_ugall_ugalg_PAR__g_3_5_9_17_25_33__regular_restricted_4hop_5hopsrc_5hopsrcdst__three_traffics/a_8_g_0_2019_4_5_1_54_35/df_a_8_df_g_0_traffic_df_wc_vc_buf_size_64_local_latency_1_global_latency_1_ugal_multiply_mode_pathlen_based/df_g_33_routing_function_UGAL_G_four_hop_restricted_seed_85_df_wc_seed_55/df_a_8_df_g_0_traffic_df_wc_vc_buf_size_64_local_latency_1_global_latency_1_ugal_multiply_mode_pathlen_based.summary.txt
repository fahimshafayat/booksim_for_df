wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 55;
routing_function = UGAL_G_four_hop_restricted;
seed = 85;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35543,   0.39291,    971.24,    698.52,         1,         0.0
simulation,         2,      0.25,   0.25011,   0.25012,    13.966,    13.966,         0,      4.7366
simulation,         3,      0.37,   0.34885,   0.34928,    948.15,     361.0,         1,         0.0
simulation,         4,      0.31,   0.31012,   0.31012,    16.008,    16.008,         0,       4.768
simulation,         5,      0.34,   0.34007,   0.34007,    24.513,    24.519,         0,      4.7852
simulation,         6,      0.35,   0.34446,   0.34436,    512.86,    165.73,         1,         0.0
simulation,         7,       0.1,   0.10004,   0.10004,     12.19,     12.19,         0,       4.616
simulation,         8,       0.2,   0.20003,   0.20003,    13.212,    13.212,         0,      4.7005
simulation,         9,       0.3,   0.30011,   0.30011,    15.419,    15.419,         0,      4.7632
