wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 56;
routing_function = UGAL_L;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.38096,   0.42127,    1018.7,    754.62,         1,         0.0
simulation,         2,      0.25,    0.2499,   0.24989,    42.591,    42.657,         0,      5.8036
simulation,         3,      0.37,   0.36022,   0.36864,    567.83,    553.62,         1,         0.0
simulation,         4,      0.31,    0.3099,   0.30989,    46.471,     46.58,         0,      5.9371
simulation,         5,      0.34,   0.33982,   0.33983,    53.539,    53.708,         0,      5.9872
simulation,         6,      0.35,   0.34976,   0.34975,    65.726,    65.999,         0,      6.0017
simulation,         7,      0.36,   0.35707,   0.35855,    482.08,    409.56,         1,         0.0
simulation,         8,       0.1,  0.099955,  0.099952,    13.656,    13.656,         0,      5.0106
simulation,         9,       0.2,   0.19988,   0.19988,    41.675,    41.721,         0,      5.6346
simulation,        10,       0.3,   0.29991,    0.2999,    45.402,    45.501,         0,      5.9191
