wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 56;
routing_function = PAR_four_hop_restricted;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.39227,   0.42561,    723.62,    567.06,         1,         0.0
simulation,         2,      0.25,   0.24981,   0.24981,    18.184,    18.205,         0,      4.9653
simulation,         3,      0.37,   0.35412,   0.35477,    714.91,    306.95,         1,         0.0
simulation,         4,      0.31,   0.30877,   0.30877,    342.71,    69.267,         0,       5.002
simulation,         5,      0.34,   0.33502,   0.33503,    466.79,    148.88,         1,         0.0
simulation,         6,      0.32,   0.31813,   0.31813,    272.92,    79.612,         1,         0.0
simulation,         7,       0.1,  0.099955,  0.099959,    12.386,    12.387,         0,      4.8906
simulation,         8,       0.2,   0.19986,   0.19986,    14.469,    14.478,         0,      4.9262
simulation,         9,       0.3,   0.29967,   0.29967,    98.159,    45.575,         0,      4.9987
