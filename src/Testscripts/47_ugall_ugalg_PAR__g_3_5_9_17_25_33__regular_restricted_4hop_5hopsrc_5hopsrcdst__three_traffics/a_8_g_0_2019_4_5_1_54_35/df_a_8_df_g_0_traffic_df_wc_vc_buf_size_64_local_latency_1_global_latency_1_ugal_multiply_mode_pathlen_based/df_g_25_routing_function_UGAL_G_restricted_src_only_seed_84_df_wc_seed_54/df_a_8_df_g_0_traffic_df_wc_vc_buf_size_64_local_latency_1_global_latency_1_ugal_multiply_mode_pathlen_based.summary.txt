wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 54;
routing_function = UGAL_G_restricted_src_only;
seed = 84;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.3946,   0.43033,    728.96,    574.72,         1,         0.0
simulation,         2,      0.25,   0.24986,   0.24986,    15.299,    15.299,         0,      5.3042
simulation,         3,      0.37,   0.36991,    0.3699,    19.844,    19.844,         0,      5.4073
simulation,         4,      0.43,   0.39888,   0.40732,    491.65,     322.1,         1,         0.0
simulation,         5,       0.4,   0.39729,   0.39728,    333.35,    103.55,         1,         0.0
simulation,         6,      0.38,   0.37988,   0.37988,    21.246,    21.246,         0,      5.4146
simulation,         7,      0.39,   0.38988,   0.38989,    24.254,    24.255,         0,      5.4225
simulation,         8,       0.1,  0.099989,  0.099986,    13.227,    13.227,         0,      5.1064
simulation,         9,       0.2,    0.1999,    0.1999,    14.446,    14.446,         0,      5.2389
simulation,        10,       0.3,   0.29988,   0.29988,    16.444,    16.444,         0,      5.3553
