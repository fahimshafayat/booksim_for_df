wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 56;
routing_function = UGAL_L_four_hop_restricted;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4176,   0.44836,    634.37,    499.23,         1,         0.0
simulation,         2,      0.25,   0.25015,   0.25015,    38.998,    39.025,         0,      4.2687
simulation,         3,      0.37,   0.36715,   0.36729,    487.89,    229.49,         1,         0.0
simulation,         4,      0.31,   0.31011,   0.31015,    44.149,    44.186,         0,       4.336
simulation,         5,      0.34,   0.33956,   0.33959,    181.01,    115.02,         0,      4.3597
simulation,         6,      0.35,   0.34912,   0.34923,     290.3,    152.85,         0,      4.3664
simulation,         7,      0.36,   0.35839,   0.35845,    442.48,     205.8,         1,         0.0
simulation,         8,       0.1,   0.10012,   0.10012,    10.732,    10.732,         0,      4.1491
simulation,         9,       0.2,   0.20019,   0.20019,     12.18,     12.18,         0,      4.2089
simulation,        10,       0.3,   0.30016,   0.30016,    41.415,    41.453,         0,      4.3266
