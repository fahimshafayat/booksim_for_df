wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 56;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49983,   0.49984,    26.541,    26.542,         0,      5.3571
simulation,         2,      0.74,   0.48031,   0.52472,    1446.4,     758.5,         1,         0.0
simulation,         3,      0.62,   0.51067,   0.54211,    672.83,     475.3,         1,         0.0
simulation,         4,      0.56,   0.51959,   0.52865,    558.54,    311.14,         1,         0.0
simulation,         5,      0.53,   0.52552,   0.52521,    498.14,    140.26,         1,         0.0
simulation,         6,      0.51,   0.50983,   0.50983,    29.298,    29.299,         0,      5.3633
simulation,         7,      0.52,   0.51982,   0.51982,    34.673,    34.674,         0,      5.3703
simulation,         8,       0.1,  0.099955,  0.099952,    12.628,    12.628,         0,      4.9313
simulation,         9,       0.2,   0.19988,   0.19988,     13.87,     13.87,         0,      5.0487
simulation,        10,       0.3,    0.2999,    0.2999,    15.642,    15.642,         0,      5.1875
simulation,        11,       0.4,   0.39983,   0.39984,    18.368,    18.369,         0,      5.2896
simulation,        12,       0.5,   0.49983,   0.49984,    26.541,    26.542,         0,      5.3571
