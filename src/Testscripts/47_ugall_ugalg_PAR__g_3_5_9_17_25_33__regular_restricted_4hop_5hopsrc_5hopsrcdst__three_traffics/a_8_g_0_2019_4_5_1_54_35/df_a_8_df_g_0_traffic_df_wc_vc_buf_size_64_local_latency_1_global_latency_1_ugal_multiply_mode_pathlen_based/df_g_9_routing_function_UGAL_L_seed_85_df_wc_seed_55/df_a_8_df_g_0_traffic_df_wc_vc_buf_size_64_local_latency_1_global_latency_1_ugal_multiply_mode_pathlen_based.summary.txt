wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 55;
routing_function = UGAL_L;
seed = 85;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35078,   0.40585,    1331.4,    1042.7,         1,         0.0
simulation,         2,      0.25,    0.2503,    0.2503,    52.049,    52.088,         0,      5.1215
simulation,         3,      0.37,   0.35518,    0.3563,    1012.8,    784.14,         1,         0.0
simulation,         4,      0.31,    0.3103,    0.3103,    61.917,    62.009,         0,      5.3764
simulation,         5,      0.34,   0.34002,   0.34014,    129.28,    130.03,         0,      5.4714
simulation,         6,      0.35,   0.34542,   0.34922,    515.51,    516.04,         1,         0.0
simulation,         7,       0.1,   0.10008,   0.10008,    12.002,    12.002,         0,      4.7228
simulation,         8,       0.2,   0.20023,   0.20023,    18.894,    18.895,         0,      4.8258
simulation,         9,       0.3,    0.3003,    0.3003,    59.542,    59.619,         0,      5.3411
