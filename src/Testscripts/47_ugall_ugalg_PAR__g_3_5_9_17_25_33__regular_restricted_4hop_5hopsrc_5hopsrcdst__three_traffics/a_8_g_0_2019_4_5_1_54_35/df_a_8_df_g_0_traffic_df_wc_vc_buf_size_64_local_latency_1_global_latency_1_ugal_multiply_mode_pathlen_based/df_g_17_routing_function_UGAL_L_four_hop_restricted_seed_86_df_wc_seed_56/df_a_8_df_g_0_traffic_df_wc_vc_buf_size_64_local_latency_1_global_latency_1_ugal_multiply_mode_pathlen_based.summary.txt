wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 56;
routing_function = UGAL_L_four_hop_restricted;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.37116,   0.40723,    934.03,    686.19,         1,         0.0
simulation,         2,      0.25,    0.2497,   0.24977,    80.712,    69.276,         0,      4.5508
simulation,         3,      0.37,   0.33998,   0.34949,    588.84,    397.77,         1,         0.0
simulation,         4,      0.31,   0.30302,   0.30304,    428.81,    246.55,         1,         0.0
simulation,         5,      0.28,   0.27795,   0.27798,    355.95,    117.92,         1,         0.0
simulation,         6,      0.26,   0.25908,    0.2591,    226.29,    84.816,         0,      4.5584
simulation,         7,      0.27,   0.26859,   0.26864,    408.87,    100.75,         0,      4.5656
simulation,         8,      0.05,  0.049969,  0.049967,    10.782,    10.782,         0,      4.2247
simulation,         9,       0.1,  0.099956,  0.099952,     11.59,     11.59,         0,       4.311
simulation,        10,      0.15,   0.14998,   0.14998,    35.878,    35.904,         0,      4.3994
simulation,        11,       0.2,   0.19988,   0.19988,    33.524,    33.557,         0,      4.4953
simulation,        12,      0.25,    0.2497,   0.24977,    80.712,    69.276,         0,      4.5508
