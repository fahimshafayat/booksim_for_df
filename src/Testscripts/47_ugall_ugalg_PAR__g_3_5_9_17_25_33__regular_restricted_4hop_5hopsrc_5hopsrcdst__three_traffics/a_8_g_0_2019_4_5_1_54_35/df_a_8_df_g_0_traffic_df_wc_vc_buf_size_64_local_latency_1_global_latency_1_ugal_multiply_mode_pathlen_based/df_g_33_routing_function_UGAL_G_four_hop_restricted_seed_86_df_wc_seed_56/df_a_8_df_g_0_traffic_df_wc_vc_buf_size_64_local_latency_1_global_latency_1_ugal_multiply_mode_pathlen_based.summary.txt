wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 56;
routing_function = UGAL_G_four_hop_restricted;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.36698,   0.40157,    849.88,     619.8,         1,         0.0
simulation,         2,      0.25,   0.24981,   0.24981,    13.797,    13.797,         0,      4.7153
simulation,         3,      0.37,   0.35657,   0.35636,     634.6,    263.77,         1,         0.0
simulation,         4,      0.31,    0.3098,   0.30981,    15.419,     15.42,         0,       4.745
simulation,         5,      0.34,   0.33939,   0.33935,    152.86,    43.852,         0,       4.758
simulation,         6,      0.35,   0.34708,   0.34707,    362.76,    81.757,         1,         0.0
simulation,         7,       0.1,  0.099955,  0.099959,    12.139,    12.139,         0,      4.6008
simulation,         8,       0.2,   0.19986,   0.19986,      13.1,      13.1,         0,      4.6803
simulation,         9,       0.3,   0.29979,    0.2998,    14.992,    14.993,         0,      4.7408
