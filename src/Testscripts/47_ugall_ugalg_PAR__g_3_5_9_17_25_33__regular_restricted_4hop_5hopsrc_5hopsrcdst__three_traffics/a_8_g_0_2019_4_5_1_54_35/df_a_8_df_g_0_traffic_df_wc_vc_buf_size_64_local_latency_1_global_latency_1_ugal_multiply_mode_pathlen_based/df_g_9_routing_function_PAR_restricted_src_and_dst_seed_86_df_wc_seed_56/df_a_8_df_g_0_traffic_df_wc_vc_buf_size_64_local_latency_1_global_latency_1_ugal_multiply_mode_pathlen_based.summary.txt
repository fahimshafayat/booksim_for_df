wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 56;
routing_function = PAR_restricted_src_and_dst;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50023,   0.50023,     23.18,    23.181,         0,      5.2082
simulation,         2,      0.74,   0.54171,   0.59192,    1158.8,    720.53,         1,         0.0
simulation,         3,      0.62,   0.55207,   0.58593,    479.06,    401.41,         1,         0.0
simulation,         4,      0.56,   0.55575,   0.55616,    514.47,    276.86,         1,         0.0
simulation,         5,      0.53,   0.53018,   0.53015,      29.6,    29.602,         0,      5.2626
simulation,         6,      0.54,   0.54013,   0.54015,    37.585,    37.605,         0,       5.284
simulation,         7,      0.55,   0.55017,   0.55013,    67.037,    67.083,         0,      5.3124
simulation,         8,       0.1,   0.10012,   0.10012,    12.171,    12.171,         0,      4.8741
simulation,         9,       0.2,   0.20018,   0.20019,    12.741,    12.741,         0,      4.8502
simulation,        10,       0.3,   0.30016,   0.30016,    13.862,    13.862,         0,      4.8788
simulation,        11,       0.4,   0.40021,   0.40022,    16.588,    16.588,         0,      5.0273
simulation,        12,       0.5,   0.50023,   0.50023,     23.18,    23.181,         0,      5.2082
