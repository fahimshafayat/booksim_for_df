wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 55;
routing_function = UGAL_G_restricted_src_only;
seed = 85;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.43928,   0.44849,    627.71,     324.4,         1,         0.0
simulation,         2,      0.25,   0.25012,   0.25012,    15.512,    15.512,         0,      5.4509
simulation,         3,      0.37,   0.37005,   0.37006,    18.641,    18.641,         0,      5.5373
simulation,         4,      0.43,   0.43005,   0.43005,    23.624,    23.625,         0,      5.5697
simulation,         5,      0.46,   0.45929,   0.45929,    155.83,    40.248,         0,      5.5865
simulation,         6,      0.48,   0.44352,    0.4432,    942.58,    254.82,         1,         0.0
simulation,         7,      0.47,   0.44778,   0.44748,    504.75,    161.39,         1,         0.0
simulation,         8,       0.1,   0.10004,   0.10004,    13.567,    13.567,         0,      5.2412
simulation,         9,       0.2,   0.20003,   0.20003,    14.739,     14.74,         0,      5.3888
simulation,        10,       0.3,   0.30011,   0.30011,    16.495,    16.495,         0,       5.494
simulation,        11,       0.4,   0.40008,   0.40008,    20.363,    20.364,         0,      5.5538
