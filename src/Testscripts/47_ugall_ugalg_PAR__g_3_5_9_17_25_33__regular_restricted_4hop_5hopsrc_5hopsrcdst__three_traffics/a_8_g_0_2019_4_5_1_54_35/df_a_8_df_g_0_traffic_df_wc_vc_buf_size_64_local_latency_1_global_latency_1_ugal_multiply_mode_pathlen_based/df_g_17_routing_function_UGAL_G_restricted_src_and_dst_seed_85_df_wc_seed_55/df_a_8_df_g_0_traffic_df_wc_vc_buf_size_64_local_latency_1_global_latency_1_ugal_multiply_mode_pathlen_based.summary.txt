wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 55;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 85;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50003,   0.50004,    26.893,    26.894,         0,      5.3876
simulation,         2,      0.74,   0.47394,   0.51924,    1481.3,    774.72,         1,         0.0
simulation,         3,      0.62,   0.50961,   0.54056,    660.81,    470.44,         1,         0.0
simulation,         4,      0.56,   0.51952,   0.52827,    546.48,     298.4,         1,         0.0
simulation,         5,      0.53,   0.52498,   0.52491,    505.67,    163.39,         1,         0.0
simulation,         6,      0.51,   0.51007,   0.51007,    29.778,     29.78,         0,      5.3938
simulation,         7,      0.52,   0.52009,   0.52009,    35.242,    35.243,         0,      5.4001
simulation,         8,       0.1,  0.099932,   0.09993,    12.674,    12.674,         0,      4.9505
simulation,         9,       0.2,   0.19983,   0.19983,    13.941,    13.941,         0,      5.0712
simulation,        10,       0.3,   0.29981,   0.29981,     15.75,     15.75,         0,      5.2166
simulation,        11,       0.4,   0.39997,   0.39997,    18.516,    18.516,         0,      5.3197
simulation,        12,       0.5,   0.50003,   0.50004,    26.893,    26.894,         0,      5.3876
