wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 55;
routing_function = PAR_restricted_src_only;
seed = 85;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50025,   0.50025,    22.247,    22.248,         0,      5.0759
simulation,         2,      0.74,   0.53975,   0.58781,    1179.5,    724.59,         1,         0.0
simulation,         3,      0.62,   0.54596,   0.58075,    498.65,     402.1,         1,         0.0
simulation,         4,      0.56,   0.54797,   0.54731,    482.86,    274.85,         1,         0.0
simulation,         5,      0.53,   0.53024,   0.53023,    34.889,    34.914,         0,       5.133
simulation,         6,      0.54,   0.54012,   0.54024,    66.182,    66.431,         0,      5.1627
simulation,         7,      0.55,   0.54529,   0.54553,     497.5,    198.18,         1,         0.0
simulation,         8,       0.1,   0.10008,   0.10008,    11.984,    11.984,         0,      4.7886
simulation,         9,       0.2,   0.20023,   0.20023,    12.537,    12.537,         0,      4.7721
simulation,        10,       0.3,    0.3003,    0.3003,    13.549,    13.549,         0,      4.7932
simulation,        11,       0.4,   0.40025,   0.40025,    15.925,    15.925,         0,      4.9105
simulation,        12,       0.5,   0.50025,   0.50025,    22.247,    22.248,         0,      5.0759
