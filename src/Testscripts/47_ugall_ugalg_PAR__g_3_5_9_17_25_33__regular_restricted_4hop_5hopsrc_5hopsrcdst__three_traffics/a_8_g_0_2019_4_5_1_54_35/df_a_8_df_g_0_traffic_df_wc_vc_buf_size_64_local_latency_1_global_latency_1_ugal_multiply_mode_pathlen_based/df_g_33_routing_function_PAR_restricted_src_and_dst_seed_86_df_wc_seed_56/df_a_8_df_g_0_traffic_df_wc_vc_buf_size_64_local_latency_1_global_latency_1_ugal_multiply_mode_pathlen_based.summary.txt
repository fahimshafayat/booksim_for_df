wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 56;
routing_function = PAR_restricted_src_and_dst;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46441,   0.47395,    507.38,    308.73,         1,         0.0
simulation,         2,      0.25,   0.24981,   0.24981,    16.605,    16.605,         0,      5.7746
simulation,         3,      0.37,   0.36959,   0.36959,    101.99,    27.463,         0,      5.8387
simulation,         4,      0.43,   0.42755,    0.4276,    242.17,    67.523,         1,         0.0
simulation,         5,       0.4,   0.39943,   0.39944,    153.74,    30.939,         0,      5.8359
simulation,         6,      0.41,   0.40931,   0.40933,    197.82,    37.944,         0,      5.8319
simulation,         7,      0.42,   0.41863,   0.41864,     323.9,    56.569,         0,      5.8276
simulation,         8,       0.1,  0.099956,  0.099959,    13.909,    13.909,         0,      5.5961
simulation,         9,       0.2,   0.19986,   0.19986,    15.443,    15.443,         0,      5.7005
simulation,        10,       0.3,   0.29979,    0.2998,    18.082,    18.082,         0,       5.817
simulation,        11,       0.4,   0.39943,   0.39944,    153.74,    30.939,         0,      5.8359
