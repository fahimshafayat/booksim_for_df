wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 54;
routing_function = UGAL_G_restricted_src_only;
seed = 84;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.43159,   0.44177,    649.93,     347.3,         1,         0.0
simulation,         2,      0.25,   0.24992,   0.24992,    15.444,    15.444,         0,      5.4332
simulation,         3,      0.37,   0.36993,   0.36994,    18.537,    18.538,         0,      5.5188
simulation,         4,      0.43,      0.43,      0.43,    23.517,    23.518,         0,      5.5503
simulation,         5,      0.46,   0.45601,   0.45614,    188.98,     57.81,         1,         0.0
simulation,         6,      0.44,   0.43999,   0.43999,    25.492,    25.494,         0,      5.5559
simulation,         7,      0.45,   0.44999,   0.44999,    28.696,    28.699,         0,      5.5619
simulation,         8,       0.1,  0.099976,  0.099979,     13.53,     13.53,         0,      5.2276
simulation,         9,       0.2,   0.19992,   0.19993,    14.683,    14.683,         0,      5.3716
simulation,        10,       0.3,   0.29997,   0.29997,    16.409,     16.41,         0,      5.4761
simulation,        11,       0.4,   0.39994,   0.39995,    20.264,    20.265,         0,      5.5345
