wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 56;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35881,   0.39463,    959.75,    691.91,         1,         0.0
simulation,         2,      0.25,   0.24656,   0.24666,    499.06,    182.96,         1,         0.0
simulation,         3,      0.13,   0.12987,   0.12987,    31.148,    31.174,         0,      5.1724
simulation,         4,      0.19,   0.18958,    0.1896,    203.19,    57.808,         0,      5.3478
simulation,         5,      0.22,   0.21853,    0.2186,    328.92,    143.84,         1,         0.0
simulation,         6,       0.2,   0.19943,   0.19948,    262.22,    76.547,         0,      5.3671
simulation,         7,      0.21,   0.20905,   0.20916,    385.71,    127.12,         0,      5.3841
simulation,         8,      0.05,  0.049903,  0.049904,    12.222,    12.222,         0,      4.7666
simulation,         9,       0.1,   0.09992,  0.099925,     27.79,    27.813,         0,      5.0514
simulation,        10,      0.15,   0.14987,   0.14988,    72.821,     49.44,         0,      5.2459
simulation,        11,       0.2,   0.19943,   0.19948,    262.22,    76.547,         0,      5.3671
