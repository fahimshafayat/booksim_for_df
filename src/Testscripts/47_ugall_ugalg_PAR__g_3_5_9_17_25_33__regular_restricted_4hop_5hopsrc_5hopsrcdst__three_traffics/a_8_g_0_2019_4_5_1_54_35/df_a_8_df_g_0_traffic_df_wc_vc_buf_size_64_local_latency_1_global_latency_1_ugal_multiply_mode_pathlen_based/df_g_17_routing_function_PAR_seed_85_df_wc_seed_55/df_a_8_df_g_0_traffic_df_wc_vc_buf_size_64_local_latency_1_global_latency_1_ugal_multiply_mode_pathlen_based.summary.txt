wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 55;
routing_function = PAR;
seed = 85;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.45661,   0.46655,    615.84,    350.58,         1,         0.0
simulation,         2,      0.25,   0.24979,   0.24979,    18.122,    18.122,         0,      6.0339
simulation,         3,      0.37,   0.36992,   0.36992,    28.872,    28.875,         0,      6.2851
simulation,         4,      0.43,   0.42821,   0.42821,     389.5,    92.058,         0,      6.2584
simulation,         5,      0.46,   0.44849,   0.44859,    461.83,    167.39,         1,         0.0
simulation,         6,      0.44,   0.43689,   0.43687,    380.66,    95.731,         1,         0.0
simulation,         7,       0.1,  0.099932,   0.09993,    14.182,    14.183,         0,      5.7346
simulation,         8,       0.2,   0.19983,   0.19983,    16.053,    16.053,         0,      5.8513
simulation,         9,       0.3,   0.29981,   0.29981,    20.798,    20.799,         0,      6.1776
simulation,        10,       0.4,   0.39991,   0.39991,    56.865,    53.547,         0,      6.2912
