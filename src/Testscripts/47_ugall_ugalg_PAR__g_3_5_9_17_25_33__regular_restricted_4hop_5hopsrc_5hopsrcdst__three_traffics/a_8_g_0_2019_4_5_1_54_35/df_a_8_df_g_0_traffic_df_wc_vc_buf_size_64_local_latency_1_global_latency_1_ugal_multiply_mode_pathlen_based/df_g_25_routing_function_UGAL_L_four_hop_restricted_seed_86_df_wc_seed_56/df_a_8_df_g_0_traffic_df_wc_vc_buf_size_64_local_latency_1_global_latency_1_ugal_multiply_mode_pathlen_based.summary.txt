wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 56;
routing_function = UGAL_L_four_hop_restricted;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.30325,   0.34383,    1281.1,    925.05,         1,         0.0
simulation,         2,      0.25,    0.2386,   0.23875,    715.39,    338.15,         1,         0.0
simulation,         3,      0.13,   0.12988,   0.12994,    55.254,    54.875,         0,      4.5283
simulation,         4,      0.19,   0.18811,   0.18817,    477.99,    174.98,         1,         0.0
simulation,         5,      0.16,   0.15948,   0.15951,    272.52,    84.326,         0,      4.5804
simulation,         6,      0.17,   0.16915,   0.16924,    418.17,    129.52,         0,      4.5933
simulation,         7,      0.18,   0.17872,   0.17873,    361.82,    145.47,         1,         0.0
simulation,         8,      0.05,  0.049903,  0.049904,    11.284,    11.284,         0,      4.3176
simulation,         9,       0.1,   0.09992,  0.099925,    26.289,    26.311,         0,       4.465
simulation,        10,      0.15,   0.14969,   0.14969,     179.9,    65.862,         0,      4.5658
