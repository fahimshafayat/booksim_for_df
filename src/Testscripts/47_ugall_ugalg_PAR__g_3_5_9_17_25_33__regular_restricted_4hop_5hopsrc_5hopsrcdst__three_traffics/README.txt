1 APR 2019. Trying to scrap together some results for SC.

We'll consider: 

three traffics: df_wc, group_shift, randperm
We'll skip uniform this time as the result isn't very useful.

Three base routings: ugal_l, ugal_g, par
    Each base routings have four variations: regular, restricted_src, restricted_src_and_dst, restricted_4hop

So, total 12 routings.

Six networks: a 8 g 3 5 9 17 25 33

g 3 and 5 runs in their own servers, with three traffics.

For g 9 17 25 33, 
Each traffic gets a server.

Each server runs 4 networks sequentially, each routing goes parallel.
So 4 rounds of 12 parallel process. 

seeds: 81-83, trafficseeds: 51-53. [Seed 80 is in folder 46.]
For shift, shift by [2,3,4]. Needs to use a spearate seed in each case.

For g 3,5, go with seeds 80-83, 50-53, 1-4.

