wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 52;
perm_seed = 52;
routing_function = PAR_restricted_src_only;
seed = 82;
shift_by_group = 52;
traffic = df_wc;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49978,   0.49979,    15.021,    15.021,         0,      4.4456
simulation,         2,      0.74,   0.58312,    0.6384,    1097.2,     735.0,         1,         0.0
simulation,         3,      0.62,   0.61961,   0.61957,    37.647,    37.654,         0,      4.7401
simulation,         4,      0.68,   0.57956,   0.63512,    718.54,     604.0,         1,         0.0
simulation,         5,      0.65,   0.58699,   0.61378,     922.4,    649.79,         1,         0.0
simulation,         6,      0.63,   0.57107,   0.60897,    691.37,    630.99,         1,         0.0
simulation,         7,      0.15,   0.14993,   0.14994,    11.309,    11.309,         0,      4.4166
simulation,         8,       0.3,       0.3,       0.3,    12.003,    12.003,         0,       4.379
simulation,         9,      0.45,    0.4498,   0.44981,    13.664,    13.664,         0,      4.3983
simulation,        10,       0.6,   0.59967,   0.59967,    23.926,    23.927,         0,      4.6639
