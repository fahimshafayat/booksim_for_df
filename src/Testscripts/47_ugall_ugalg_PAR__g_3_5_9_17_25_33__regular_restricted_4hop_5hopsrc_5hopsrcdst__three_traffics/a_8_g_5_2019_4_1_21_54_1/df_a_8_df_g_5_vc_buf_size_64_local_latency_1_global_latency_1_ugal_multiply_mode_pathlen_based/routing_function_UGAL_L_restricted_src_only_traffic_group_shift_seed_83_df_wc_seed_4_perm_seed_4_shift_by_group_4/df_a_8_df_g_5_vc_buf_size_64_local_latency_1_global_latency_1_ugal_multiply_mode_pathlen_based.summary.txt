wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 4;
perm_seed = 4;
routing_function = UGAL_L_restricted_src_only;
seed = 83;
shift_by_group = 4;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50018,    0.5002,    50.802,    50.841,         0,       4.275
simulation,         2,      0.74,   0.58231,   0.64006,    1042.6,    736.84,         1,         0.0
simulation,         3,      0.62,   0.58266,   0.58466,    1354.7,    967.93,         1,         0.0
simulation,         4,      0.56,   0.56042,   0.56042,    60.246,     60.31,         0,      4.3053
simulation,         5,      0.59,   0.59039,   0.59037,    81.938,    82.062,         0,      4.2685
simulation,         6,       0.6,   0.58423,   0.59789,    601.49,    592.53,         1,         0.0
simulation,         7,       0.1,   0.10017,   0.10017,    10.431,    10.431,         0,      4.0812
simulation,         8,       0.2,   0.20017,   0.20017,    10.801,    10.801,         0,      4.0732
simulation,         9,       0.3,   0.30025,   0.30026,    11.514,    11.514,         0,      4.0735
simulation,        10,       0.4,   0.40023,   0.40024,    15.098,    15.098,         0,      4.1326
simulation,        11,       0.5,   0.50018,    0.5002,    50.802,    50.841,         0,       4.275
