wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 53;
perm_seed = 53;
routing_function = UGAL_L_restricted_src_only;
seed = 83;
shift_by_group = 53;
traffic = df_wc;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50018,    0.5002,    53.968,    54.009,         0,      4.2702
simulation,         2,      0.74,   0.58682,   0.63857,    1011.4,    669.32,         1,         0.0
simulation,         3,      0.62,   0.58525,   0.59418,    1026.2,    804.67,         1,         0.0
simulation,         4,      0.56,   0.56041,   0.56042,    63.134,    63.202,         0,      4.3112
simulation,         5,      0.59,   0.59038,   0.59037,    74.636,    74.742,         0,       4.329
simulation,         6,       0.6,   0.60034,   0.60035,    83.619,    83.749,         0,      4.3338
simulation,         7,      0.61,   0.61025,   0.61031,    105.64,    105.84,         0,      4.3354
simulation,         8,      0.15,   0.15008,   0.15008,    10.672,    10.672,         0,      4.1135
simulation,         9,       0.3,   0.30025,   0.30026,    11.648,    11.648,         0,      4.1372
simulation,        10,      0.45,   0.45029,   0.45031,     48.96,    48.992,         0,      4.2219
simulation,        11,       0.6,   0.60034,   0.60035,    83.619,    83.749,         0,      4.3338
