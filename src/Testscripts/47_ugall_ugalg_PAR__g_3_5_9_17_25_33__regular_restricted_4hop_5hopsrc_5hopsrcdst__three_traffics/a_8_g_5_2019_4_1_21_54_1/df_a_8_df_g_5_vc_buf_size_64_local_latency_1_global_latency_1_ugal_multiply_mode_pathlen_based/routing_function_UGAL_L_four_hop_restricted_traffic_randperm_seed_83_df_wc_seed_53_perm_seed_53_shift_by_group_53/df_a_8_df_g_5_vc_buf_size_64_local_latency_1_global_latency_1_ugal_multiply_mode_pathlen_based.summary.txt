wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 53;
perm_seed = 53;
routing_function = UGAL_L_four_hop_restricted;
seed = 83;
shift_by_group = 53;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50019,    0.5002,    10.304,    10.304,         0,      3.5199
simulation,         2,      0.74,   0.74008,   0.74008,    12.334,    12.335,         0,      3.4913
simulation,         3,      0.86,   0.85991,   0.85991,    16.947,    16.954,         0,      3.4925
simulation,         4,      0.92,      0.92,      0.92,    22.052,    22.064,         0,      3.5034
simulation,         5,      0.95,    0.9459,   0.94592,    337.38,    57.181,         0,      3.5073
simulation,         6,      0.97,   0.96311,   0.96305,    419.43,    62.284,         1,         0.0
simulation,         7,      0.96,   0.95435,   0.95435,     474.9,    59.924,         0,      3.5109
simulation,         8,       0.2,   0.20017,   0.20017,    9.4498,    9.4498,         0,      3.5536
simulation,         9,       0.4,   0.40024,   0.40024,    9.9567,    9.9568,         0,       3.536
simulation,        10,       0.6,   0.60034,   0.60035,     10.78,    10.781,         0,      3.5047
simulation,        11,       0.8,   0.80003,   0.80005,     14.46,    14.464,         0,      3.4887
