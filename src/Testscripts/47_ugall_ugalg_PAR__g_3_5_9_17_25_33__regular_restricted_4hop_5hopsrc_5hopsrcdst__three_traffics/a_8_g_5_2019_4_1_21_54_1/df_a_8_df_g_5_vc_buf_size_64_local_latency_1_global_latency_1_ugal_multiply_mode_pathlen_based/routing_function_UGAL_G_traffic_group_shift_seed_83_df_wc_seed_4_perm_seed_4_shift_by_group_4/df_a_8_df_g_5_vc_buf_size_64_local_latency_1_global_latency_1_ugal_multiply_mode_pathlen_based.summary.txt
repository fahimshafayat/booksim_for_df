wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 4;
perm_seed = 4;
routing_function = UGAL_G;
seed = 83;
shift_by_group = 4;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50018,    0.5002,    20.809,    20.809,         0,        4.92
simulation,         2,      0.74,   0.47377,   0.52681,    1986.2,    941.18,         1,         0.0
simulation,         3,      0.62,   0.62026,   0.62025,    45.278,    45.271,         0,      5.1684
simulation,         4,      0.68,   0.47414,   0.52611,    1573.0,    867.69,         1,         0.0
simulation,         5,      0.65,    0.4852,    0.5375,    1161.8,     736.3,         1,         0.0
simulation,         6,      0.63,   0.52898,   0.55575,    1130.1,    574.79,         1,         0.0
simulation,         7,      0.15,   0.15008,   0.15008,    11.674,    11.674,         0,      4.5404
simulation,         8,       0.3,   0.30024,   0.30026,    12.962,    12.962,         0,       4.545
simulation,         9,      0.45,   0.45028,   0.45031,    17.757,    17.757,         0,      4.7873
simulation,        10,       0.6,   0.60032,   0.60035,    34.007,    34.005,         0,      5.1317
