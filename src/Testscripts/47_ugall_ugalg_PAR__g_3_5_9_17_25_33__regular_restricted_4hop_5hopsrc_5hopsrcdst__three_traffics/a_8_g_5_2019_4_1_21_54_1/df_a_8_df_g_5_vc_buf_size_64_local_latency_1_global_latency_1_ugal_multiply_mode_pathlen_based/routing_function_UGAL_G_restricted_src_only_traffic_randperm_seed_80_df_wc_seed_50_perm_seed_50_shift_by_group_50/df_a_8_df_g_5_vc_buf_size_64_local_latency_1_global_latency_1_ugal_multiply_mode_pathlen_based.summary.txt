wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_G_restricted_src_only;
seed = 80;
shift_by_group = 50;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4996,    0.4996,    10.704,    10.704,         0,      3.6341
simulation,         2,      0.74,   0.73946,   0.73946,    12.456,    12.456,         0,      3.5986
simulation,         3,      0.86,    0.8595,    0.8595,    14.607,    14.608,         0,       3.592
simulation,         4,      0.92,   0.91969,   0.91968,    16.781,    16.781,         0,      3.5934
simulation,         5,      0.95,   0.94973,   0.94972,    18.488,    18.489,         0,      3.5947
simulation,         6,      0.97,   0.96987,   0.96987,    20.274,    20.275,         0,       3.596
simulation,         7,      0.98,   0.97992,   0.97992,    21.531,    21.532,         0,       3.597
simulation,         8,       0.2,   0.19965,   0.19966,    9.7706,    9.7708,         0,      3.6905
simulation,         9,       0.4,   0.39959,   0.39959,    10.307,    10.307,         0,      3.6515
simulation,        10,       0.6,   0.59936,   0.59936,    11.243,    11.243,         0,      3.6174
simulation,        11,       0.8,   0.79955,   0.79956,    13.322,    13.322,         0,      3.5936
