wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_G;
seed = 80;
shift_by_group = 50;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4996,    0.4996,    10.556,    10.556,         0,      3.6179
simulation,         2,      0.74,   0.73946,   0.73946,    12.541,    12.542,         0,      3.5872
simulation,         3,      0.86,    0.8595,    0.8595,    15.835,    15.835,         0,      3.6086
simulation,         4,      0.92,   0.91968,   0.91968,     19.96,    19.961,         0,      3.6341
simulation,         5,      0.95,   0.94972,   0.94972,     24.44,    24.442,         0,      3.6511
simulation,         6,      0.97,   0.96986,   0.96987,    30.902,    30.905,         0,      3.6632
simulation,         7,      0.98,   0.97991,   0.97992,    37.849,    37.851,         0,       3.669
simulation,         8,       0.2,   0.19965,   0.19966,     9.907,    9.9074,         0,      3.7622
simulation,         9,       0.4,   0.39959,   0.39959,    10.223,    10.223,         0,      3.6503
simulation,        10,       0.6,   0.59936,   0.59936,    11.073,    11.073,         0,      3.5965
simulation,        11,       0.8,   0.79956,   0.79956,    13.771,    13.771,         0,      3.5938
