wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 1;
perm_seed = 1;
routing_function = UGAL_G_restricted_src_only;
seed = 80;
shift_by_group = 1;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49959,    0.4996,    14.211,    14.211,         0,      4.0917
simulation,         2,      0.74,   0.52439,   0.57918,    1127.1,     766.1,         1,         0.0
simulation,         3,      0.62,   0.61935,   0.61937,    25.721,    25.721,         0,      4.1736
simulation,         4,      0.68,   0.53844,   0.59603,    622.27,    558.92,         1,         0.0
simulation,         5,      0.65,   0.53825,   0.56769,    1154.6,    685.04,         1,         0.0
simulation,         6,      0.63,   0.61418,   0.62342,    504.97,    488.69,         1,         0.0
simulation,         7,      0.15,   0.14979,    0.1498,    10.607,    10.607,         0,      4.0782
simulation,         8,       0.3,   0.29978,   0.29978,    11.407,    11.407,         0,      4.0678
simulation,         9,      0.45,   0.44953,   0.44954,    13.036,    13.036,         0,      4.0739
simulation,        10,       0.6,   0.59934,   0.59936,    20.017,    20.018,         0,      4.1586
