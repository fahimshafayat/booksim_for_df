wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 52;
perm_seed = 52;
routing_function = PAR_restricted_src_only;
seed = 82;
shift_by_group = 52;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49978,   0.49979,    10.825,    10.826,         0,       3.668
simulation,         2,      0.74,   0.73969,   0.73969,    12.662,    12.663,         0,      3.6149
simulation,         3,      0.86,   0.85989,   0.85989,    15.648,    15.649,         0,       3.623
simulation,         4,      0.92,   0.91994,   0.91995,    20.431,    20.435,         0,      3.6578
simulation,         5,      0.95,   0.95009,   0.95007,     26.02,    26.028,         0,      3.6896
simulation,         6,      0.97,   0.96996,   0.96999,    36.855,    36.873,         0,      3.7327
simulation,         7,      0.98,   0.97996,   0.97999,    58.643,    58.701,         0,      3.7489
simulation,         8,       0.2,   0.19999,   0.19999,    9.9684,    9.9688,         0,       3.773
simulation,         9,       0.4,   0.39981,   0.39981,    10.448,    10.448,         0,       3.699
simulation,        10,       0.6,   0.59967,   0.59967,    11.353,    11.353,         0,      3.6414
simulation,        11,       0.8,   0.79989,   0.79989,    13.749,     13.75,         0,      3.6132
