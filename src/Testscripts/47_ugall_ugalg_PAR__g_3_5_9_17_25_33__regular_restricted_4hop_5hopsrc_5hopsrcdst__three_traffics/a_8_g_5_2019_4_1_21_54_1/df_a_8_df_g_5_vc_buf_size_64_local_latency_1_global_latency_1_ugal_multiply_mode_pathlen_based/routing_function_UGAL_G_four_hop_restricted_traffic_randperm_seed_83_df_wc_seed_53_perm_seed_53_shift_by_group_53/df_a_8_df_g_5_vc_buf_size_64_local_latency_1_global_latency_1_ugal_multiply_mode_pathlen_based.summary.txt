wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 53;
perm_seed = 53;
routing_function = UGAL_G_four_hop_restricted;
seed = 83;
shift_by_group = 53;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50019,    0.5002,    10.123,    10.124,         0,      3.4543
simulation,         2,      0.74,   0.74008,   0.74008,    11.489,    11.489,         0,      3.4232
simulation,         3,      0.86,   0.85991,   0.85991,    13.018,    13.019,         0,      3.4163
simulation,         4,      0.92,   0.91999,      0.92,    14.394,    14.394,         0,      3.4169
simulation,         5,      0.95,   0.94991,   0.94992,    15.435,    15.436,         0,      3.4187
simulation,         6,      0.97,   0.96994,   0.96994,    16.377,    16.378,         0,      3.4205
simulation,         7,      0.98,   0.97994,   0.97994,    17.027,    17.029,         0,      3.4211
simulation,         8,       0.2,   0.20017,   0.20017,    9.3406,    9.3407,         0,      3.5019
simulation,         9,       0.4,   0.40023,   0.40024,    9.7916,    9.7918,         0,      3.4689
simulation,        10,       0.6,   0.60034,   0.60035,    10.559,     10.56,         0,      3.4399
simulation,        11,       0.8,   0.80005,   0.80005,    12.123,    12.123,         0,      3.4185
