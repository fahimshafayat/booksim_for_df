wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 51;
perm_seed = 51;
routing_function = UGAL_L;
seed = 81;
shift_by_group = 51;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4999,   0.49989,    10.982,    10.982,         0,      3.7325
simulation,         2,      0.74,   0.74004,   0.74004,    13.608,    13.609,         0,      3.6667
simulation,         3,      0.86,   0.85924,   0.85924,    89.344,    35.437,         0,      3.7008
simulation,         4,      0.92,   0.90359,   0.90369,    490.56,    82.887,         1,         0.0
simulation,         5,      0.89,   0.88305,     0.883,    281.02,    51.059,         1,         0.0
simulation,         6,      0.87,   0.86856,   0.86859,    146.92,    36.832,         0,      3.7079
simulation,         7,      0.88,   0.87794,   0.87792,    208.41,    40.218,         0,      3.7123
simulation,         8,      0.15,   0.15003,   0.15003,    10.194,    10.195,         0,      3.9411
simulation,         9,       0.3,   0.30009,   0.30009,    10.409,     10.41,         0,      3.8431
simulation,        10,      0.45,   0.44992,   0.44991,    10.795,    10.796,         0,      3.7584
simulation,        11,       0.6,   0.60011,    0.6001,    11.558,    11.558,         0,      3.6951
simulation,        12,      0.75,   0.74999,      0.75,    14.081,    14.082,         0,      3.6656
