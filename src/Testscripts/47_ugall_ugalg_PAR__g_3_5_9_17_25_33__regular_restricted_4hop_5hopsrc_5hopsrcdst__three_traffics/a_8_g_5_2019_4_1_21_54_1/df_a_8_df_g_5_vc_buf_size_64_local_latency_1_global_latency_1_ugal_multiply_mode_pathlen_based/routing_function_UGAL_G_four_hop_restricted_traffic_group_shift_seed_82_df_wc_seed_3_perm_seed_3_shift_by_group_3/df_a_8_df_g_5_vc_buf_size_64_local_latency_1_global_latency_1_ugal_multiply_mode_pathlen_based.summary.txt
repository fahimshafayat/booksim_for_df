wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 3;
perm_seed = 3;
routing_function = UGAL_G_four_hop_restricted;
seed = 82;
shift_by_group = 3;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49978,   0.49979,    14.213,    14.213,         0,      4.0911
simulation,         2,      0.74,    0.5274,   0.58391,    1167.9,    787.27,         1,         0.0
simulation,         3,      0.62,   0.61958,   0.61957,    25.925,    25.926,         0,      4.1735
simulation,         4,      0.68,   0.57416,   0.63197,    593.52,     534.4,         1,         0.0
simulation,         5,      0.65,   0.55156,   0.58137,    892.13,    617.13,         1,         0.0
simulation,         6,      0.63,   0.61707,   0.62166,    517.46,    450.89,         1,         0.0
simulation,         7,      0.15,   0.14993,   0.14994,    10.605,    10.605,         0,      4.0777
simulation,         8,       0.3,       0.3,       0.3,    11.414,    11.414,         0,      4.0691
simulation,         9,      0.45,    0.4498,   0.44981,    13.042,    13.042,         0,      4.0735
simulation,        10,       0.6,   0.59967,   0.59967,    20.032,    20.033,         0,      4.1574
