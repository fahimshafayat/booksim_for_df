wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 51;
perm_seed = 51;
routing_function = UGAL_G_restricted_src_only;
seed = 81;
shift_by_group = 51;
traffic = df_wc;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49989,   0.49989,    14.518,    14.518,         0,      4.1836
simulation,         2,      0.74,   0.57584,   0.63117,    926.77,    668.42,         1,         0.0
simulation,         3,      0.62,   0.62015,   0.62015,    26.504,    26.504,         0,      4.2675
simulation,         4,      0.68,   0.58263,   0.61139,    1119.9,     680.4,         1,         0.0
simulation,         5,      0.65,   0.61854,   0.61923,    1079.5,    552.09,         1,         0.0
simulation,         6,      0.63,   0.62417,   0.62465,    497.95,    294.92,         1,         0.0
simulation,         7,      0.15,   0.15003,   0.15003,    10.743,    10.743,         0,      4.1392
simulation,         8,       0.3,   0.30009,   0.30009,    11.623,    11.623,         0,      4.1473
simulation,         9,      0.45,   0.44992,   0.44991,    13.335,    13.335,         0,      4.1641
simulation,        10,       0.6,    0.6001,    0.6001,    20.387,    20.387,         0,      4.2533
