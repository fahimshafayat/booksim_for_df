wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 2;
perm_seed = 2;
routing_function = UGAL_G;
seed = 81;
shift_by_group = 2;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49989,   0.49989,    20.797,    20.797,         0,      4.9193
simulation,         2,      0.74,   0.47365,   0.52658,    1976.5,    939.75,         1,         0.0
simulation,         3,      0.62,   0.62014,   0.62015,    45.078,    45.075,         0,       5.168
simulation,         4,      0.68,   0.47173,   0.52523,    1588.9,    879.74,         1,         0.0
simulation,         5,      0.65,   0.48145,    0.5335,    1206.2,    755.12,         1,         0.0
simulation,         6,      0.63,   0.52504,   0.55158,    1323.7,    636.97,         1,         0.0
simulation,         7,      0.15,   0.15003,   0.15003,    11.675,    11.675,         0,        4.54
simulation,         8,       0.3,   0.30009,   0.30009,    12.962,    12.962,         0,      4.5454
simulation,         9,      0.45,   0.44992,   0.44991,    17.712,    17.712,         0,      4.7853
simulation,        10,       0.6,   0.60009,    0.6001,    33.953,    33.954,         0,      5.1319
