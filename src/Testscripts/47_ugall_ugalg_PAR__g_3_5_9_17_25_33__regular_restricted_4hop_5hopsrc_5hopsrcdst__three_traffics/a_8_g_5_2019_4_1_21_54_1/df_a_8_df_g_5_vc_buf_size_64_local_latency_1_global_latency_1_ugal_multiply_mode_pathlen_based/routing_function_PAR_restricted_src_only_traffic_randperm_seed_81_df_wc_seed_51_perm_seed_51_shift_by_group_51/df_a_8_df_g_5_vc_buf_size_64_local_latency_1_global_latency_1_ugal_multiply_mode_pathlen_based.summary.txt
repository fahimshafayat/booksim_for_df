wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 51;
perm_seed = 51;
routing_function = PAR_restricted_src_only;
seed = 81;
shift_by_group = 51;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49989,   0.49989,    11.117,    11.118,         0,      3.7449
simulation,         2,      0.74,   0.74006,   0.74004,    13.528,    13.529,         0,      3.6977
simulation,         3,      0.86,   0.85984,   0.85982,    20.609,    20.616,         0,      3.7552
simulation,         4,      0.92,   0.89848,   0.90009,    560.59,    211.42,         1,         0.0
simulation,         5,      0.89,   0.88916,   0.88927,    106.12,    78.455,         0,      3.8842
simulation,         6,       0.9,   0.89819,    0.8981,    290.54,    102.63,         0,       3.841
simulation,         7,      0.91,   0.90144,   0.90178,    494.35,    162.42,         1,         0.0
simulation,         8,      0.15,   0.15003,   0.15003,    10.078,    10.078,         0,      3.8784
simulation,         9,       0.3,   0.30009,   0.30009,    10.396,    10.396,         0,      3.8186
simulation,        10,      0.45,   0.44992,   0.44991,     10.89,     10.89,         0,      3.7619
simulation,        11,       0.6,    0.6001,    0.6001,    11.732,    11.732,         0,      3.7163
simulation,        12,      0.75,   0.75002,      0.75,     13.75,    13.751,         0,      3.6984
simulation,        13,       0.9,   0.89819,    0.8981,    290.54,    102.63,         0,       3.841
