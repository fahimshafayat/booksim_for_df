wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 51;
perm_seed = 51;
routing_function = UGAL_L_four_hop_restricted;
seed = 81;
shift_by_group = 51;
traffic = df_wc;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49989,   0.49989,    53.943,     53.99,         0,      4.2703
simulation,         2,      0.74,   0.58599,    0.6368,    1016.0,    669.14,         1,         0.0
simulation,         3,      0.62,    0.5864,   0.59246,    1037.1,    808.58,         1,         0.0
simulation,         4,      0.56,   0.56006,   0.56007,    62.989,    63.059,         0,      4.3116
simulation,         5,      0.59,    0.5901,   0.59011,    74.311,    74.399,         0,      4.3289
simulation,         6,       0.6,    0.6001,    0.6001,    83.176,    83.311,         0,      4.3335
simulation,         7,      0.61,   0.61025,   0.61017,    106.97,    107.19,         0,      4.3357
simulation,         8,      0.15,   0.15003,   0.15003,    10.672,    10.672,         0,      4.1143
simulation,         9,       0.3,   0.30009,   0.30009,    11.651,    11.651,         0,      4.1383
simulation,        10,      0.45,   0.44993,   0.44991,    49.017,     49.05,         0,      4.2222
simulation,        11,       0.6,    0.6001,    0.6001,    83.176,    83.311,         0,      4.3335
