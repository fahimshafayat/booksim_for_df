wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 4;
perm_seed = 4;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 83;
shift_by_group = 4;
traffic = group_shift;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50018,    0.5002,    50.802,    50.839,         0,       4.275
simulation,         2,      0.74,   0.58257,   0.63999,    1040.5,    737.91,         1,         0.0
simulation,         3,      0.62,   0.58379,   0.58826,    1319.6,    951.89,         1,         0.0
simulation,         4,      0.56,   0.56041,   0.56042,    60.253,    60.311,         0,      4.3062
simulation,         5,      0.59,   0.59035,   0.59037,    81.889,    82.042,         0,      4.2689
simulation,         6,       0.6,   0.58367,   0.59823,    565.29,    567.03,         1,         0.0
simulation,         7,       0.1,   0.10017,   0.10017,     10.43,     10.43,         0,      4.0808
simulation,         8,       0.2,   0.20017,   0.20017,    10.798,    10.798,         0,      4.0725
simulation,         9,       0.3,   0.30025,   0.30026,    11.506,    11.506,         0,      4.0741
simulation,        10,       0.4,   0.40023,   0.40024,    15.121,    15.121,         0,       4.133
simulation,        11,       0.5,   0.50018,    0.5002,    50.802,    50.839,         0,       4.275
