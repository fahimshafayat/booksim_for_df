wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 51;
perm_seed = 51;
routing_function = UGAL_G_restricted_src_only;
seed = 81;
shift_by_group = 51;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49989,   0.49989,     10.35,     10.35,         0,      3.5253
simulation,         2,      0.74,   0.74005,   0.74004,     11.85,     11.85,         0,      3.4911
simulation,         3,      0.86,   0.85983,   0.85982,    13.581,    13.581,         0,      3.4832
simulation,         4,      0.92,   0.91998,   0.91997,    15.198,    15.198,         0,      3.4841
simulation,         5,      0.95,    0.9501,   0.95008,    16.462,    16.463,         0,      3.4858
simulation,         6,      0.97,   0.97012,   0.97011,    17.658,    17.658,         0,      3.4872
simulation,         7,      0.98,   0.98013,   0.98012,    18.395,    18.396,         0,      3.4875
simulation,         8,       0.2,   0.20017,   0.20017,    9.5193,    9.5194,         0,      3.5797
simulation,         9,       0.4,   0.40001,       0.4,    9.9961,    9.9963,         0,      3.5417
simulation,        10,       0.6,   0.60011,    0.6001,    10.822,    10.823,         0,      3.5091
simulation,        11,       0.8,   0.79994,   0.79994,    12.555,    12.555,         0,      3.4861
