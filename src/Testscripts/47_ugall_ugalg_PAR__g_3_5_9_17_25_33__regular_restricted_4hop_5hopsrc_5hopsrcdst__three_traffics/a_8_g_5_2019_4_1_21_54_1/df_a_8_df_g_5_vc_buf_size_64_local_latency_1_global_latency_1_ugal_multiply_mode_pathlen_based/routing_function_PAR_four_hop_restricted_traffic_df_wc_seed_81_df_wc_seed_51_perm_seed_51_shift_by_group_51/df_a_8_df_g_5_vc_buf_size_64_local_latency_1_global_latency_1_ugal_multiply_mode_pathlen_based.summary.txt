wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 51;
perm_seed = 51;
routing_function = PAR_four_hop_restricted;
seed = 81;
shift_by_group = 51;
traffic = df_wc;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4999,   0.49989,    15.025,    15.026,         0,      4.4455
simulation,         2,      0.74,    0.5814,   0.63698,    1115.7,    738.59,         1,         0.0
simulation,         3,      0.62,   0.62016,   0.62015,     38.83,    38.829,         0,      4.7421
simulation,         4,      0.68,   0.57765,   0.63179,    728.43,    611.36,         1,         0.0
simulation,         5,      0.65,   0.58945,   0.61584,    897.03,    639.37,         1,         0.0
simulation,         6,      0.63,   0.57235,    0.6092,    658.05,    609.91,         1,         0.0
simulation,         7,      0.15,   0.15003,   0.15003,    11.308,    11.309,         0,      4.4162
simulation,         8,       0.3,   0.30009,   0.30009,    12.003,    12.003,         0,      4.3783
simulation,         9,      0.45,   0.44992,   0.44991,    13.659,    13.659,         0,       4.398
simulation,        10,       0.6,    0.6001,    0.6001,    24.055,    24.056,         0,      4.6646
