wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 5;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_wc_seed = 53;
perm_seed = 53;
routing_function = UGAL_L;
seed = 83;
shift_by_group = 53;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50019,    0.5002,    10.739,    10.739,         0,      3.6553
simulation,         2,      0.74,   0.74008,   0.74008,    14.641,    14.645,         0,      3.5959
simulation,         3,      0.86,   0.85194,   0.85194,    493.55,      63.6,         1,         0.0
simulation,         4,       0.8,   0.80001,   0.80005,    18.155,    18.163,         0,      3.6077
simulation,         5,      0.83,   0.82522,   0.82517,     432.3,    62.535,         0,      3.6095
simulation,         6,      0.84,   0.83428,   0.83427,    440.26,    62.502,         1,         0.0
simulation,         7,      0.15,   0.15008,   0.15008,    9.9842,    9.9845,         0,      3.8453
simulation,         8,       0.3,   0.30025,   0.30026,    10.189,    10.189,         0,      3.7555
simulation,         9,      0.45,    0.4503,   0.45031,    10.554,    10.554,         0,      3.6758
simulation,        10,       0.6,   0.60034,   0.60035,    11.291,    11.292,         0,      3.6202
simulation,        11,      0.75,   0.75003,   0.75004,    15.139,    15.145,         0,      3.5968
