wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
perm_seed = 51;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49998,   0.49997,    15.162,    15.167,         0,      4.1716
simulation,         2,      0.74,   0.67269,   0.68522,    670.43,    316.43,         1,         0.0
simulation,         3,      0.62,   0.62004,   0.62005,    36.226,    36.263,         0,      4.2598
simulation,         4,      0.68,   0.66291,   0.66353,    479.66,    177.85,         1,         0.0
simulation,         5,      0.65,   0.64678,   0.64679,    484.86,    73.481,         0,      4.3263
simulation,         6,      0.66,   0.65416,   0.65418,    453.18,    96.965,         1,         0.0
simulation,         7,      0.15,   0.15003,   0.15003,    10.911,    10.911,         0,       4.261
simulation,         8,       0.3,   0.30005,   0.30005,    11.435,    11.435,         0,      4.2233
simulation,         9,      0.45,   0.45002,   0.45002,    12.809,     12.81,         0,       4.174
simulation,        10,       0.6,   0.60003,   0.60005,    30.349,    30.375,         0,      4.2282
