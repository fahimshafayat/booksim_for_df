wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
perm_seed = 51;
routing_function = UGAL_L_four_hop_restricted;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49972,   0.49972,    55.085,    36.179,         0,      4.1487
simulation,         2,      0.74,   0.61192,   0.64215,    555.25,    380.95,         1,         0.0
simulation,         3,      0.62,   0.58578,   0.58582,    835.43,    235.94,         1,         0.0
simulation,         4,      0.56,   0.55225,   0.55227,    509.09,    100.85,         1,         0.0
simulation,         5,      0.53,   0.52787,   0.52787,    403.78,    64.486,         0,      4.1701
simulation,         6,      0.54,   0.53653,   0.53653,    325.33,     75.59,         1,         0.0
simulation,         7,       0.1,   0.10008,   0.10008,    10.339,    10.339,         0,      4.0605
simulation,         8,       0.2,   0.20015,   0.20016,    10.744,    10.745,         0,      4.0958
simulation,         9,       0.3,   0.30014,   0.30015,    11.573,    11.573,         0,      4.0996
simulation,        10,       0.4,   0.40005,   0.40006,    17.523,    17.535,         0,      4.1083
simulation,        11,       0.5,   0.49972,   0.49972,    55.085,    36.179,         0,      4.1487
