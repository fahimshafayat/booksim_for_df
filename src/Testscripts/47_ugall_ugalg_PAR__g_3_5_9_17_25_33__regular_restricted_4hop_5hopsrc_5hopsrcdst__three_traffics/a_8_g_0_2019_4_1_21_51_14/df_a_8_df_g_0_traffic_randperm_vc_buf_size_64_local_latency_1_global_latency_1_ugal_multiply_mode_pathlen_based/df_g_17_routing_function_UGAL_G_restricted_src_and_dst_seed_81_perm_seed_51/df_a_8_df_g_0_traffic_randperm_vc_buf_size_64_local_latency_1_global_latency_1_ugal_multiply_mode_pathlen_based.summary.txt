wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
perm_seed = 51;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49997,   0.49997,    12.128,    12.128,         0,      3.9927
simulation,         2,      0.74,   0.73993,   0.73993,    23.673,    23.674,         0,      4.0862
simulation,         3,      0.86,   0.76406,   0.77817,    793.95,    325.11,         1,         0.0
simulation,         4,       0.8,   0.77252,   0.77264,    617.71,     211.4,         1,         0.0
simulation,         5,      0.77,   0.76995,   0.76995,    36.638,    36.641,         0,      4.1368
simulation,         6,      0.78,   0.77991,   0.77992,    49.758,    49.766,         0,      4.1594
simulation,         7,      0.79,   0.77563,   0.77553,    517.12,    161.34,         1,         0.0
simulation,         8,      0.15,   0.15002,   0.15003,    10.559,    10.559,         0,      4.1043
simulation,         9,       0.3,   0.30005,   0.30005,    10.954,    10.954,         0,      4.0442
simulation,        10,      0.45,   0.45002,   0.45002,     11.71,     11.71,         0,      4.0017
simulation,        11,       0.6,   0.60005,   0.60005,    13.653,    13.653,         0,      3.9932
simulation,        12,      0.75,   0.74992,   0.74994,    26.252,    26.253,         0,      4.0998
