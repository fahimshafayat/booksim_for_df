wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
perm_seed = 53;
routing_function = UGAL_G_restricted_src_only;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49998,   0.49997,    13.269,    13.269,         0,      4.1394
simulation,         2,      0.74,   0.69182,   0.70168,    463.23,    243.19,         1,         0.0
simulation,         3,      0.62,   0.61996,   0.61995,    18.636,    18.637,         0,      4.2198
simulation,         4,      0.68,   0.67995,   0.67993,    33.984,    33.988,         0,      4.3281
simulation,         5,      0.71,   0.69373,   0.69376,    554.81,    168.43,         1,         0.0
simulation,         6,      0.69,   0.68992,   0.68991,    44.823,    44.828,         0,      4.3578
simulation,         7,       0.7,   0.69451,   0.69454,    385.09,    109.43,         1,         0.0
simulation,         8,      0.15,   0.14986,   0.14986,    10.785,    10.785,         0,      4.2019
simulation,         9,       0.3,   0.29989,   0.29989,    11.319,     11.32,         0,       4.158
simulation,        10,      0.45,   0.44994,   0.44994,    12.476,    12.476,         0,      4.1334
simulation,        11,       0.6,   0.60001,   0.59999,    17.009,     17.01,         0,      4.1972
