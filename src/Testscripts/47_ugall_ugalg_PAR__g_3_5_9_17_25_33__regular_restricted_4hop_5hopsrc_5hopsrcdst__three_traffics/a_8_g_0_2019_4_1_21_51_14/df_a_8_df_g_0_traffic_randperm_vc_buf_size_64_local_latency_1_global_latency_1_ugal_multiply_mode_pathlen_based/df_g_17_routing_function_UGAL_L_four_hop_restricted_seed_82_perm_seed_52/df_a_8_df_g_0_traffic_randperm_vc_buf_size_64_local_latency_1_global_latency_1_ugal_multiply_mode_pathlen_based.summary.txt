wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
perm_seed = 52;
routing_function = UGAL_L_four_hop_restricted;
seed = 82;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50002,   0.50002,    14.802,    14.807,         0,      3.9561
simulation,         2,      0.74,   0.68296,   0.69394,     573.9,    276.38,         1,         0.0
simulation,         3,      0.62,   0.61936,   0.61936,    125.17,    36.253,         0,       3.995
simulation,         4,      0.68,   0.66903,   0.66894,    492.56,    144.05,         1,         0.0
simulation,         5,      0.65,   0.64886,   0.64885,    261.82,    54.279,         0,      4.0232
simulation,         6,      0.66,   0.65787,    0.6579,     416.5,    76.374,         0,      4.0352
simulation,         7,      0.67,   0.66421,   0.66417,    467.97,    117.35,         1,         0.0
simulation,         8,      0.15,   0.15003,   0.15003,    10.242,    10.242,         0,      3.9599
simulation,         9,       0.3,   0.30017,   0.30017,    10.782,    10.782,         0,       3.966
simulation,        10,      0.45,   0.45008,   0.45008,     12.28,    12.281,         0,      3.9553
simulation,        11,       0.6,   0.59996,   0.59996,    29.807,    29.764,         0,      3.9832
