wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
perm_seed = 51;
routing_function = PAR_restricted_src_and_dst;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49997,   0.49997,    13.833,    13.833,         0,      4.3912
simulation,         2,      0.74,   0.66672,   0.68424,    788.12,    431.94,         1,         0.0
simulation,         3,      0.62,   0.62005,   0.62005,    18.846,    18.847,         0,      4.4596
simulation,         4,      0.68,   0.66137,   0.66095,    588.13,    297.15,         1,         0.0
simulation,         5,      0.65,   0.64999,      0.65,    24.462,    24.465,         0,      4.5558
simulation,         6,      0.66,      0.66,      0.66,    30.533,    30.544,         0,      4.6294
simulation,         7,      0.67,   0.65872,   0.65911,    530.04,    247.58,         1,         0.0
simulation,         8,      0.15,   0.15003,   0.15003,     11.81,    11.811,         0,      4.6518
simulation,         9,       0.3,   0.30005,   0.30005,    12.231,    12.231,         0,       4.508
simulation,        10,      0.45,   0.45002,   0.45002,    13.186,    13.187,         0,      4.4058
simulation,        11,       0.6,   0.60005,   0.60005,    17.099,      17.1,         0,      4.4282
