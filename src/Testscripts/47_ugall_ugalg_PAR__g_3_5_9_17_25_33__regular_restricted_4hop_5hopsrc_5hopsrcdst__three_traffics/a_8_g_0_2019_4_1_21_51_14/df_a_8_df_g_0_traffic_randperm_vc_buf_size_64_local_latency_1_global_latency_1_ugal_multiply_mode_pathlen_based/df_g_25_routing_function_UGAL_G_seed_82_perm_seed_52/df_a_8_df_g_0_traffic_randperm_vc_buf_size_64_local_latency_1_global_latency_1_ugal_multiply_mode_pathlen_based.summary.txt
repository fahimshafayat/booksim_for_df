wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
perm_seed = 52;
routing_function = UGAL_G;
seed = 82;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50012,   0.50012,    14.819,     14.82,         0,      4.2545
simulation,         2,      0.74,    0.6366,   0.66448,    489.56,    341.47,         1,         0.0
simulation,         3,      0.62,   0.62008,   0.62007,    32.604,    32.609,         0,      4.5258
simulation,         4,      0.68,    0.6401,   0.64021,    1013.1,    270.57,         1,         0.0
simulation,         5,      0.65,   0.64096,   0.64101,    522.86,    140.65,         1,         0.0
simulation,         6,      0.63,   0.63004,   0.63005,    39.628,    39.636,         0,      4.5699
simulation,         7,      0.64,   0.63997,   0.64002,    54.913,    54.929,         0,      4.6278
simulation,         8,      0.15,      0.15,      0.15,    10.982,    10.982,         0,       4.292
simulation,         9,       0.3,   0.30017,   0.30017,    11.473,    11.473,         0,      4.2103
simulation,        10,      0.45,   0.45014,   0.45014,    13.229,    13.229,         0,      4.2109
simulation,        11,       0.6,   0.60008,   0.60007,    25.227,     25.23,         0,      4.4549
