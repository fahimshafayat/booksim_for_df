wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
perm_seed = 53;
routing_function = UGAL_G_four_hop_restricted;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49988,   0.49988,    10.938,    10.938,         0,      3.6812
simulation,         2,      0.74,   0.73992,   0.73992,    13.243,    13.244,         0,      3.6512
simulation,         3,      0.86,   0.85993,   0.85992,    17.727,    17.728,         0,      3.6553
simulation,         4,      0.92,   0.91998,   0.91999,    31.258,    31.262,         0,      3.6746
simulation,         5,      0.95,   0.91567,   0.91612,    571.58,    192.23,         1,         0.0
simulation,         6,      0.93,      0.93,      0.93,    41.039,    41.049,         0,      3.6814
simulation,         7,      0.94,   0.91995,   0.92013,    519.19,    149.93,         1,         0.0
simulation,         8,       0.2,   0.19995,   0.19995,    9.8647,    9.8648,         0,      3.7296
simulation,         9,       0.4,   0.39988,   0.39988,    10.475,    10.475,         0,      3.6968
simulation,        10,       0.6,   0.60001,   0.60002,    11.598,    11.598,         0,      3.6669
simulation,        11,       0.8,    0.7999,   0.79991,    14.681,    14.682,         0,        3.65
