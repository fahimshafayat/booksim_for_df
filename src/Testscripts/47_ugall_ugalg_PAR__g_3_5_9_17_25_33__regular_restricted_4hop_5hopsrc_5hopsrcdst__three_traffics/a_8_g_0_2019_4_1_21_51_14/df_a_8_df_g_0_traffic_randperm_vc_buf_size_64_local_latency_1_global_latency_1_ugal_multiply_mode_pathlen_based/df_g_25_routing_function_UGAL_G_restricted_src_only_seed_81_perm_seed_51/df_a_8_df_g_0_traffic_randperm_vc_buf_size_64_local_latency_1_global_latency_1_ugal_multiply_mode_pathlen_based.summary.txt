wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
perm_seed = 51;
routing_function = UGAL_G_restricted_src_only;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49986,   0.49987,    13.584,    13.585,         0,      4.1793
simulation,         2,      0.74,   0.67747,   0.68842,    574.02,    281.81,         1,         0.0
simulation,         3,      0.62,   0.61987,   0.61988,    19.978,    19.979,         0,      4.2757
simulation,         4,      0.68,   0.67981,   0.67978,    47.471,    47.482,         0,      4.4032
simulation,         5,      0.71,   0.67709,   0.67713,    780.97,    236.77,         1,         0.0
simulation,         6,      0.69,     0.678,   0.67799,    509.44,    158.31,         1,         0.0
simulation,         7,      0.15,    0.1501,    0.1501,    10.857,    10.857,         0,        4.23
simulation,         8,       0.3,   0.30015,   0.30015,    11.425,    11.425,         0,      4.1872
simulation,         9,      0.45,   0.44995,   0.44996,    12.694,    12.695,         0,      4.1688
simulation,        10,       0.6,   0.59991,   0.59991,    17.956,    17.957,         0,      4.2498
