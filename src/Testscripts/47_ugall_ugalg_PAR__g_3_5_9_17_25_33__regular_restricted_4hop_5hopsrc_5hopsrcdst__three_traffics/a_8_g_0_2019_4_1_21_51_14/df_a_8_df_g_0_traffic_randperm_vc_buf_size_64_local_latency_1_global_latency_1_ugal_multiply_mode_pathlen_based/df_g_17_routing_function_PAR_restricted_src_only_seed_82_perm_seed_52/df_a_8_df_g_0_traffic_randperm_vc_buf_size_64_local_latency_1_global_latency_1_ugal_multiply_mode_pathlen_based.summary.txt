wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
perm_seed = 52;
routing_function = PAR_restricted_src_only;
seed = 82;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50002,   0.50002,    13.501,    13.502,         0,      4.3206
simulation,         2,      0.74,   0.67291,   0.68889,    729.79,    397.37,         1,         0.0
simulation,         3,      0.62,   0.61991,    0.6199,    17.386,    17.387,         0,      4.3672
simulation,         4,      0.68,   0.66717,   0.66732,    542.82,     242.2,         1,         0.0
simulation,         5,      0.65,   0.64992,   0.64991,     20.87,    20.872,         0,      4.4272
simulation,         6,      0.66,    0.6599,   0.65989,     23.63,    23.633,         0,      4.4647
simulation,         7,      0.67,   0.66993,   0.66992,     29.07,    29.077,         0,      4.5293
simulation,         8,      0.15,   0.15003,   0.15003,    11.594,    11.594,         0,       4.556
simulation,         9,       0.3,   0.30017,   0.30017,    12.007,    12.008,         0,      4.4258
simulation,        10,      0.45,   0.45008,   0.45008,    12.909,    12.909,         0,      4.3336
simulation,        11,       0.6,   0.59996,   0.59996,    16.167,    16.168,         0,      4.3449
