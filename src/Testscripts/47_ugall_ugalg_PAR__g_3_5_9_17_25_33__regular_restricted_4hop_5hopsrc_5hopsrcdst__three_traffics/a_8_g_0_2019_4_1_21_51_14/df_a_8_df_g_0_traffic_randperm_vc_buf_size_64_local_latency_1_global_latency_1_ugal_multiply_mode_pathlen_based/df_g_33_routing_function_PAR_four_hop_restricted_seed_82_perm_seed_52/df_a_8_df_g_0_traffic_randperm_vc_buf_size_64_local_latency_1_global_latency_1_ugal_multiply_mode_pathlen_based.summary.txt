wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
perm_seed = 52;
routing_function = PAR_four_hop_restricted;
seed = 82;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50013,   0.50012,    15.192,    15.193,         0,      4.4521
simulation,         2,      0.74,   0.57784,   0.62147,    803.61,    566.29,         1,         0.0
simulation,         3,      0.62,   0.57043,   0.58275,    564.26,    345.85,         1,         0.0
simulation,         4,      0.56,   0.55986,   0.55993,    51.644,    42.419,         0,       4.642
simulation,         5,      0.59,   0.56661,   0.56754,    659.28,    283.31,         1,         0.0
simulation,         6,      0.57,   0.56407,   0.56407,    515.81,     154.9,         1,         0.0
simulation,         7,       0.1,  0.099915,  0.099915,    11.153,    11.153,         0,       4.437
simulation,         8,       0.2,   0.20003,   0.20002,    11.537,    11.537,         0,       4.431
simulation,         9,       0.3,   0.30006,   0.30006,    12.049,     12.05,         0,      4.4112
simulation,        10,       0.4,   0.40012,   0.40011,    12.916,    12.917,         0,      4.4056
simulation,        11,       0.5,   0.50013,   0.50012,    15.192,    15.193,         0,      4.4521
