wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
perm_seed = 52;
routing_function = UGAL_G_four_hop_restricted;
seed = 82;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49989,   0.49989,    10.994,    10.994,         0,      3.6949
simulation,         2,      0.74,   0.73991,   0.73991,    13.379,    13.379,         0,      3.6663
simulation,         3,      0.86,   0.85994,   0.85995,    18.114,    18.115,         0,      3.6727
simulation,         4,      0.92,      0.92,   0.91998,    34.933,    34.937,         0,      3.6937
simulation,         5,      0.95,   0.91644,   0.91722,    579.35,    191.18,         1,         0.0
simulation,         6,      0.93,   0.92999,   0.93002,    51.814,    51.829,         0,      3.7018
simulation,         7,      0.94,   0.92146,   0.92135,    473.17,    156.57,         1,         0.0
simulation,         8,       0.2,   0.20009,   0.20009,    9.9022,    9.9023,         0,      3.7439
simulation,         9,       0.4,   0.40005,   0.40004,    10.521,    10.522,         0,      3.7111
simulation,        10,       0.6,   0.59987,   0.59986,    11.661,    11.661,         0,      3.6801
simulation,        11,       0.8,   0.80005,   0.80004,    14.898,    14.899,         0,      3.6661
