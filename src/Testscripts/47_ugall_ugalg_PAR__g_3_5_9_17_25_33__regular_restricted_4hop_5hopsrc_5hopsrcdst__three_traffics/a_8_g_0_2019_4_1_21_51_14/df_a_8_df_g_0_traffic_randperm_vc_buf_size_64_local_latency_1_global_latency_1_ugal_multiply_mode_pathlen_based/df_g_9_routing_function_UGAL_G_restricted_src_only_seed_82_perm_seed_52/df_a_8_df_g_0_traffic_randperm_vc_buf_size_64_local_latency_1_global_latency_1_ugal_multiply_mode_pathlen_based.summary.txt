wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
perm_seed = 52;
routing_function = UGAL_G_restricted_src_only;
seed = 82;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4999,   0.49989,    11.065,    11.065,         0,      3.7345
simulation,         2,      0.74,   0.73991,   0.73991,    13.625,    13.626,         0,      3.7085
simulation,         3,      0.86,   0.85993,   0.85995,    19.025,    19.026,         0,      3.7289
simulation,         4,      0.92,   0.91999,   0.91998,    37.839,    37.844,         0,       3.761
simulation,         5,      0.95,   0.92134,   0.92108,    551.59,    174.96,         1,         0.0
simulation,         6,      0.93,      0.93,   0.93002,    58.644,    58.669,         0,      3.7704
simulation,         7,      0.94,   0.92384,   0.92415,    502.35,    142.46,         1,         0.0
simulation,         8,       0.2,    0.2001,   0.20009,    10.033,    10.033,         0,       3.806
simulation,         9,       0.4,   0.40005,   0.40004,    10.604,    10.604,         0,       3.755
simulation,        10,       0.6,   0.59987,   0.59986,    11.748,    11.748,         0,      3.7183
simulation,        11,       0.8,   0.80004,   0.80004,    15.362,    15.362,         0,      3.7142
