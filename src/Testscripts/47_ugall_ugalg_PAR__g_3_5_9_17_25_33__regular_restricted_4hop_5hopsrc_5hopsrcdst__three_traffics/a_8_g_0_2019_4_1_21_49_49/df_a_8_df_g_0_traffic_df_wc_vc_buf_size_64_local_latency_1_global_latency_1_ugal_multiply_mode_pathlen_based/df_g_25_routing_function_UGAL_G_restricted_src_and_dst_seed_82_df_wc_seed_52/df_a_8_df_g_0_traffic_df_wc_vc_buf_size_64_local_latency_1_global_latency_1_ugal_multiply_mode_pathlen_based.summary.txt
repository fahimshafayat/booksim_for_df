wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 52;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 82;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.40903,   0.44348,    697.51,     557.0,         1,         0.0
simulation,         2,      0.25,    0.2501,    0.2501,    15.503,    15.504,         0,      5.3717
simulation,         3,      0.37,   0.37012,   0.37012,    19.599,    19.599,         0,      5.4841
simulation,         4,      0.43,   0.41428,   0.41353,    660.19,    320.38,         1,         0.0
simulation,         5,       0.4,   0.40012,   0.40011,    23.634,    23.633,         0,      5.5045
simulation,         6,      0.41,   0.41012,   0.41011,    28.115,    28.117,         0,      5.5119
simulation,         7,      0.42,   0.41601,   0.41591,    505.45,    178.46,         1,         0.0
simulation,         8,       0.1,   0.10002,   0.10002,    13.317,    13.317,         0,      5.1573
simulation,         9,       0.2,   0.20005,   0.20005,    14.598,    14.599,         0,      5.2989
simulation,        10,       0.3,   0.30017,   0.30017,    16.658,    16.658,         0,      5.4283
simulation,        11,       0.4,   0.40012,   0.40011,    23.634,    23.633,         0,      5.5045
