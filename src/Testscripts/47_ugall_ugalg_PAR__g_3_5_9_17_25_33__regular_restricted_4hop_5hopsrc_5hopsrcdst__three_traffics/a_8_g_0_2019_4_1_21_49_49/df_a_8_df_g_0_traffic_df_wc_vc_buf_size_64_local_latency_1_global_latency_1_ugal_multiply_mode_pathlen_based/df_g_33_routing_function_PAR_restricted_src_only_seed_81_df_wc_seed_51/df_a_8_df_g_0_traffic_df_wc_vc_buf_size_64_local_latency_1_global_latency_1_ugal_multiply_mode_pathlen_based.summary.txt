wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 51;
routing_function = PAR_restricted_src_only;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.37904,    0.4222,    733.65,    581.61,         1,         0.0
simulation,         2,      0.25,   0.25006,   0.25006,    16.387,    16.387,         0,      5.6831
simulation,         3,      0.37,   0.36986,   0.36986,    70.913,    27.237,         0,      5.7394
simulation,         4,      0.43,   0.42712,   0.42712,    354.62,    79.717,         1,         0.0
simulation,         5,       0.4,   0.39899,   0.39902,     279.8,    47.672,         0,      5.7322
simulation,         6,      0.41,    0.4084,   0.40841,    411.88,    57.525,         0,      5.7259
simulation,         7,      0.42,   0.41783,   0.41784,    274.08,    70.912,         1,         0.0
simulation,         8,       0.1,   0.10003,   0.10003,    13.753,    13.753,         0,       5.521
simulation,         9,       0.2,   0.20005,   0.20005,    15.238,    15.239,         0,      5.6132
simulation,        10,       0.3,   0.30009,   0.30009,    17.834,    17.834,         0,      5.7226
simulation,        11,       0.4,   0.39899,   0.39902,     279.8,    47.672,         0,      5.7322
