wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 52;
routing_function = UGAL_G_four_hop_restricted;
seed = 82;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.48704,   0.48751,    510.63,    264.28,         1,         0.0
simulation,         2,      0.25,   0.25012,   0.25012,    12.202,    12.203,         0,      4.2748
simulation,         3,      0.37,   0.37005,   0.37004,     14.26,     14.26,         0,       4.324
simulation,         4,      0.43,   0.42995,   0.42995,    16.434,    16.434,         0,      4.3651
simulation,         5,      0.46,   0.45994,   0.45995,    18.906,    18.907,         0,      4.3845
simulation,         6,      0.48,   0.47989,   0.47989,    23.837,    23.838,         0,       4.399
simulation,         7,      0.49,    0.4898,   0.48988,    43.619,    43.644,         0,        4.41
simulation,         8,       0.1,  0.099989,  0.099984,    10.867,    10.867,         0,       4.229
simulation,         9,       0.2,    0.2001,   0.20009,    11.685,    11.685,         0,      4.2622
simulation,        10,       0.3,   0.30011,    0.3001,    12.817,    12.817,         0,      4.2882
simulation,        11,       0.4,   0.40005,   0.40004,    15.114,    15.114,         0,      4.3448
