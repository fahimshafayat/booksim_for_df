wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 51;
routing_function = UGAL_G;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50003,   0.50001,    28.896,    28.896,         0,      5.6818
simulation,         2,      0.74,   0.47214,   0.52022,    1549.1,    826.18,         1,         0.0
simulation,         3,      0.62,    0.5055,   0.54047,    707.89,    512.36,         1,         0.0
simulation,         4,      0.56,   0.56008,   0.56014,    62.447,    62.447,         0,       5.764
simulation,         5,      0.59,   0.51721,   0.54692,    458.01,    372.66,         1,         0.0
simulation,         6,      0.57,   0.52696,   0.53772,    550.89,    319.61,         1,         0.0
simulation,         7,       0.1,  0.099917,  0.099918,    12.453,    12.453,         0,      4.9524
simulation,         8,       0.2,    0.1998,    0.1998,    13.672,    13.672,         0,       5.036
simulation,         9,       0.3,   0.29994,   0.29995,    16.218,    16.218,         0,       5.237
simulation,        10,       0.4,   0.39997,   0.39997,    20.449,    20.449,         0,      5.5076
simulation,        11,       0.5,   0.50003,   0.50001,    28.896,    28.896,         0,      5.6818
