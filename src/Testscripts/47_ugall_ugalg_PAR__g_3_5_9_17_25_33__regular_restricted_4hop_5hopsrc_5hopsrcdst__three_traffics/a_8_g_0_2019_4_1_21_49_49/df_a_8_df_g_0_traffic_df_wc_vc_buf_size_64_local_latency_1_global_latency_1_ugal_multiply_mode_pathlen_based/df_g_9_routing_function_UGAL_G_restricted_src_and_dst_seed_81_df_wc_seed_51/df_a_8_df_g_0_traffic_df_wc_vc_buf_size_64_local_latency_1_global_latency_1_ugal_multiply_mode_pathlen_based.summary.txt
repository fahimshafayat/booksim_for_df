wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 51;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50002,   0.50001,    20.137,    20.137,         0,      4.7746
simulation,         2,      0.74,   0.53759,   0.58267,    1139.3,    661.59,         1,         0.0
simulation,         3,      0.62,   0.55335,   0.57993,    446.26,    353.85,         1,         0.0
simulation,         4,      0.56,   0.56009,   0.56014,     38.49,    38.492,         0,      4.8277
simulation,         5,      0.59,   0.55622,   0.56596,    467.13,    288.65,         1,         0.0
simulation,         6,      0.57,   0.55861,   0.55832,    568.38,    228.53,         1,         0.0
simulation,         7,       0.1,  0.099918,  0.099918,    11.413,    11.413,         0,      4.4859
simulation,         8,       0.2,    0.1998,    0.1998,    12.266,    12.266,         0,      4.5317
simulation,         9,       0.3,   0.29994,   0.29995,    13.464,    13.464,         0,      4.5761
simulation,        10,       0.4,   0.39997,   0.39997,    15.745,    15.745,         0,      4.6807
simulation,        11,       0.5,   0.50002,   0.50001,    20.137,    20.137,         0,      4.7746
