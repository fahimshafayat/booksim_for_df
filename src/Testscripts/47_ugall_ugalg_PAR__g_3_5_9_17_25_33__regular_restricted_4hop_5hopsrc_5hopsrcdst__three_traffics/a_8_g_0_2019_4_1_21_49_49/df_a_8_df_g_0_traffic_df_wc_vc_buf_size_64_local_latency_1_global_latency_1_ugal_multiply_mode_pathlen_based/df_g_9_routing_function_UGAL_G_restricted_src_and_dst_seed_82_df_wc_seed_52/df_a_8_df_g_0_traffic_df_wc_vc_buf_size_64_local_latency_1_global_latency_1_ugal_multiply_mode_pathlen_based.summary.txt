wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 52;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 82;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49989,   0.49989,     20.16,     20.16,         0,      4.7797
simulation,         2,      0.74,   0.54149,   0.58354,    1079.0,    616.17,         1,         0.0
simulation,         3,      0.62,   0.55271,   0.57762,    442.24,    342.32,         1,         0.0
simulation,         4,      0.56,   0.55992,   0.55993,    37.533,    37.531,         0,      4.8326
simulation,         5,      0.59,   0.55475,   0.56473,     496.1,    304.16,         1,         0.0
simulation,         6,      0.57,   0.55607,   0.55533,    473.12,    244.51,         1,         0.0
simulation,         7,       0.1,  0.099989,  0.099984,    11.421,    11.421,         0,      4.4885
simulation,         8,       0.2,    0.2001,   0.20009,    12.282,    12.282,         0,      4.5352
simulation,         9,       0.3,    0.3001,    0.3001,    13.472,    13.472,         0,      4.5805
simulation,        10,       0.4,   0.40005,   0.40004,    15.772,    15.772,         0,      4.6859
simulation,        11,       0.5,   0.49989,   0.49989,     20.16,     20.16,         0,      4.7797
