wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 51;
routing_function = PAR;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35896,   0.40551,    1118.0,    875.14,         1,         0.0
simulation,         2,      0.25,   0.25011,   0.25011,    19.953,    19.953,         0,      6.2905
simulation,         3,      0.37,   0.36734,   0.36738,    390.54,    114.03,         1,         0.0
simulation,         4,      0.31,   0.31016,   0.31015,    28.255,    28.263,         0,      6.3591
simulation,         5,      0.34,   0.34008,   0.34007,    35.807,    35.827,         0,      6.3575
simulation,         6,      0.35,   0.35008,   0.35008,    43.569,    43.518,         0,      6.3556
simulation,         7,      0.36,   0.35924,   0.35925,    223.43,    73.246,         0,      6.3553
simulation,         8,       0.1,   0.10008,   0.10008,     14.79,    14.791,         0,      5.9279
simulation,         9,       0.2,   0.20015,   0.20016,    17.653,    17.653,         0,      6.1728
simulation,        10,       0.3,   0.30015,   0.30015,    25.695,    25.701,         0,      6.3547
