wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 53;
routing_function = UGAL_G_four_hop_restricted;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.38172,     0.414,    749.87,    562.24,         1,         0.0
simulation,         2,      0.25,   0.25005,   0.25004,    13.691,    13.691,         0,      4.7111
simulation,         3,      0.37,   0.36207,   0.36227,    445.94,    169.21,         1,         0.0
simulation,         4,      0.31,   0.31001,      0.31,     15.06,    15.061,         0,      4.7403
simulation,         5,      0.34,      0.34,      0.34,    17.326,    17.328,         0,      4.7535
simulation,         6,      0.35,   0.34996,   0.34996,    20.157,    20.162,         0,      4.7586
simulation,         7,      0.36,   0.35676,   0.35677,    420.55,    98.237,         1,         0.0
simulation,         8,       0.1,  0.099999,  0.099999,    12.123,    12.123,         0,      4.5991
simulation,         9,       0.2,   0.19999,   0.19999,    13.034,    13.034,         0,      4.6772
simulation,        10,       0.3,   0.30001,   0.30001,    14.723,    14.723,         0,      4.7358
