wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 51;
routing_function = UGAL_G_four_hop_restricted;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.47967,   0.47918,    804.79,     290.5,         1,         0.0
simulation,         2,      0.25,   0.24984,   0.24984,     12.18,     12.18,         0,      4.2617
simulation,         3,      0.37,   0.36999,   0.36999,    14.254,    14.254,         0,      4.3088
simulation,         4,      0.43,   0.42996,   0.42997,    16.607,    16.607,         0,       4.349
simulation,         5,      0.46,   0.45999,   0.45998,    20.416,    20.415,         0,      4.3687
simulation,         6,      0.48,   0.47568,   0.47561,    492.53,    114.32,         1,         0.0
simulation,         7,      0.47,      0.47,      0.47,    26.703,    26.704,         0,      4.3777
simulation,         8,       0.1,  0.099919,  0.099918,    10.843,    10.843,         0,      4.2174
simulation,         9,       0.2,    0.1998,    0.1998,     11.66,     11.66,         0,      4.2494
simulation,        10,       0.3,   0.29995,   0.29995,    12.797,    12.797,         0,      4.2737
simulation,        11,       0.4,   0.39997,   0.39997,    15.139,    15.139,         0,      4.3295
