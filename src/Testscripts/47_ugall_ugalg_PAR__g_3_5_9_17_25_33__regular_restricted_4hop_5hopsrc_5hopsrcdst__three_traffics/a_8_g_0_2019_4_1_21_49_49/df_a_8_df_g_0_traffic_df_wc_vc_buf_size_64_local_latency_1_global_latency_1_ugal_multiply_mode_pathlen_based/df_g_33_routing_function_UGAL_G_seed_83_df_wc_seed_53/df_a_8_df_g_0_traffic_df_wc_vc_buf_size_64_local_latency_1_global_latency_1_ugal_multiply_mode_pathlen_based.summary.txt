wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 53;
routing_function = UGAL_G;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49996,   0.49998,    55.403,    55.405,         0,      6.2901
simulation,         2,      0.74,   0.38145,   0.42818,    1925.0,    955.43,         1,         0.0
simulation,         3,      0.62,   0.40977,   0.45609,    1310.0,    808.19,         1,         0.0
simulation,         4,      0.56,   0.43713,   0.47856,    740.57,    580.54,         1,         0.0
simulation,         5,      0.53,   0.43925,   0.45593,    1032.4,    554.28,         1,         0.0
simulation,         6,      0.51,   0.47602,   0.47654,    954.37,    266.12,         1,         0.0
simulation,         7,       0.1,  0.099999,  0.099999,    14.775,    14.775,         0,       5.766
simulation,         8,       0.2,   0.19999,   0.19999,    16.476,    16.476,         0,      6.0071
simulation,         9,       0.3,   0.30002,   0.30001,    18.875,    18.875,         0,      6.1564
simulation,        10,       0.4,   0.39995,   0.39995,    23.413,    23.413,         0,      6.2351
simulation,        11,       0.5,   0.49996,   0.49998,    55.403,    55.405,         0,      6.2901
