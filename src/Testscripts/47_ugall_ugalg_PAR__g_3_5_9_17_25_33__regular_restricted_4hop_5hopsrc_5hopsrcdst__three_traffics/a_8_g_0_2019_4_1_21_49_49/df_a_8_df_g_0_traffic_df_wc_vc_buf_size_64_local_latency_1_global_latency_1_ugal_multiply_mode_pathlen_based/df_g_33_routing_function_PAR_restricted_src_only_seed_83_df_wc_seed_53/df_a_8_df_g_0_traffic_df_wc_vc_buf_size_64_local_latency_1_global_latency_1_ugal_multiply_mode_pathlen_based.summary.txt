wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 53;
routing_function = PAR_restricted_src_only;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.38129,     0.424,    745.28,    606.23,         1,         0.0
simulation,         2,      0.25,   0.25005,   0.25004,    16.369,    16.369,         0,      5.6709
simulation,         3,      0.37,   0.36994,   0.36996,    28.097,    28.098,         0,       5.733
simulation,         4,      0.43,    0.3985,   0.40015,    682.73,    319.53,         1,         0.0
simulation,         5,       0.4,   0.39759,   0.39792,    314.25,    97.832,         1,         0.0
simulation,         6,      0.38,   0.37993,   0.37995,    31.889,    31.797,         0,      5.7297
simulation,         7,      0.39,   0.38962,   0.38961,    113.81,    57.738,         0,      5.7215
simulation,         8,       0.1,  0.099999,  0.099999,    13.723,    13.723,         0,      5.5054
simulation,         9,       0.2,       0.2,   0.19999,    15.215,    15.215,         0,      5.6004
simulation,        10,       0.3,   0.30001,   0.30001,    17.971,    17.971,         0,       5.711
