wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 53;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4448,   0.46903,    433.62,    355.22,         1,         0.0
simulation,         2,      0.25,   0.24994,   0.24993,    36.316,    36.362,         0,      5.1749
simulation,         3,      0.37,   0.36347,   0.36354,    423.92,    190.64,         1,         0.0
simulation,         4,      0.31,   0.30869,    0.3088,    307.67,    101.11,         0,      5.2603
simulation,         5,      0.34,   0.33688,   0.33698,     514.2,    156.94,         1,         0.0
simulation,         6,      0.32,   0.31828,   0.31834,    416.05,    120.76,         0,      5.2712
simulation,         7,      0.33,   0.32752,   0.32758,    407.76,    133.25,         1,         0.0
simulation,         8,       0.1,  0.099956,  0.099954,    12.506,    12.506,         0,      4.7157
simulation,         9,       0.2,   0.19986,   0.19985,    36.473,    36.509,         0,      5.0632
simulation,        10,       0.3,   0.29904,   0.29913,     223.9,    80.767,         0,      5.2484
