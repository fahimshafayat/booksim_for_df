wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 51;
routing_function = UGAL_L_restricted_src_only;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.27728,   0.32245,    1235.9,    932.21,         1,         0.0
simulation,         2,      0.25,   0.25006,   0.25006,    27.221,    27.255,         0,       5.487
simulation,         3,      0.37,   0.33904,   0.34625,    1008.2,    482.05,         1,         0.0
simulation,         4,      0.31,   0.31009,   0.31009,    27.804,    27.846,         0,       5.533
simulation,         5,      0.34,   0.33836,    0.3384,    424.42,    113.61,         0,       5.549
simulation,         6,      0.35,   0.34249,     0.343,    455.05,    194.18,         1,         0.0
simulation,         7,       0.1,   0.10003,   0.10003,    32.134,    32.159,         0,      5.1428
simulation,         8,       0.2,   0.20005,   0.20005,    27.785,    27.817,         0,      5.4278
simulation,         9,       0.3,   0.30009,   0.30009,    27.532,     27.57,         0,      5.5265
