wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 52;
routing_function = UGAL_G;
seed = 82;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.36915,   0.41121,    955.92,    752.27,         1,         0.0
simulation,         2,      0.25,    0.2501,    0.2501,    17.752,    17.752,         0,      5.9771
simulation,         3,      0.37,   0.37012,   0.37012,    24.055,    24.055,         0,      6.1341
simulation,         4,      0.43,   0.40096,   0.41113,    516.35,     373.5,         1,         0.0
simulation,         5,       0.4,   0.40011,   0.40011,    33.001,    33.002,         0,      6.1633
simulation,         6,      0.41,   0.40438,   0.40381,    520.39,    207.72,         1,         0.0
simulation,         7,       0.1,   0.10002,   0.10002,    14.408,    14.409,         0,      5.6203
simulation,         8,       0.2,   0.20005,   0.20005,    16.366,    16.366,         0,      5.8642
simulation,         9,       0.3,   0.30017,   0.30017,    19.456,    19.456,         0,      6.0561
simulation,        10,       0.4,   0.40011,   0.40011,    33.001,    33.002,         0,      6.1633
