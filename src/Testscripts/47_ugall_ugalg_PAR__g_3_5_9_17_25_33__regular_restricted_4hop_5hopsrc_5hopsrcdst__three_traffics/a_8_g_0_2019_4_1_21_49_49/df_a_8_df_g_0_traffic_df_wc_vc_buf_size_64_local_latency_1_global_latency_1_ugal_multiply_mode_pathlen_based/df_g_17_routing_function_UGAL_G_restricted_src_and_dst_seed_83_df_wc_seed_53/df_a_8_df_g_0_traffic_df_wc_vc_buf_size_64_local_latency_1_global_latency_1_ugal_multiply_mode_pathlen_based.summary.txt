wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 53;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,       0.5,       0.5,    26.442,    26.442,         0,      5.3523
simulation,         2,      0.74,   0.47813,   0.52231,    1464.6,    766.89,         1,         0.0
simulation,         3,      0.62,   0.50727,   0.53891,    665.66,    478.46,         1,         0.0
simulation,         4,      0.56,   0.51839,   0.52704,    530.75,    295.75,         1,         0.0
simulation,         5,      0.53,   0.52547,   0.52549,    415.39,    141.57,         1,         0.0
simulation,         6,      0.51,   0.51002,   0.51002,    29.246,    29.247,         0,      5.3586
simulation,         7,      0.52,   0.52002,   0.52003,    34.684,    34.684,         0,      5.3655
simulation,         8,       0.1,  0.099957,  0.099954,    12.617,    12.618,         0,      4.9259
simulation,         9,       0.2,   0.19986,   0.19985,    13.853,    13.853,         0,      5.0431
simulation,        10,       0.3,   0.29985,   0.29985,    15.611,    15.611,         0,      5.1822
simulation,        11,       0.4,   0.39975,   0.39975,    18.305,    18.305,         0,      5.2846
simulation,        12,       0.5,       0.5,       0.5,    26.442,    26.442,         0,      5.3523
