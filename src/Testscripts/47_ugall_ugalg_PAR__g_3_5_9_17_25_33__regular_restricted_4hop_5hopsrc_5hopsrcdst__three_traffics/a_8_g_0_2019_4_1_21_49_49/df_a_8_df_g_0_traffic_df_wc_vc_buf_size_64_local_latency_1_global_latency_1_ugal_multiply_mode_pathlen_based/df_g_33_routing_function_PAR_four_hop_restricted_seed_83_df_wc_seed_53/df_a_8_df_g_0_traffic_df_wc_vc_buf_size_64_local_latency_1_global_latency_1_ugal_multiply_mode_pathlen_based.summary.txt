wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 53;
routing_function = PAR_four_hop_restricted;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.40393,   0.43312,    634.57,    494.21,         1,         0.0
simulation,         2,      0.25,   0.25004,   0.25004,    18.362,    18.383,         0,        4.96
simulation,         3,      0.37,   0.35578,   0.35589,    590.17,    250.12,         1,         0.0
simulation,         4,      0.31,   0.30883,   0.30882,     414.2,    51.965,         0,      4.9903
simulation,         5,      0.34,   0.33406,   0.33401,    496.83,    139.83,         1,         0.0
simulation,         6,      0.32,   0.31792,   0.31794,    276.92,    75.249,         1,         0.0
simulation,         7,       0.1,  0.099998,  0.099999,    12.371,    12.371,         0,      4.8832
simulation,         8,       0.2,   0.19999,   0.19999,    13.467,    13.467,         0,      4.9217
simulation,         9,       0.3,   0.29938,   0.29938,    176.82,    49.619,         0,      4.9905
