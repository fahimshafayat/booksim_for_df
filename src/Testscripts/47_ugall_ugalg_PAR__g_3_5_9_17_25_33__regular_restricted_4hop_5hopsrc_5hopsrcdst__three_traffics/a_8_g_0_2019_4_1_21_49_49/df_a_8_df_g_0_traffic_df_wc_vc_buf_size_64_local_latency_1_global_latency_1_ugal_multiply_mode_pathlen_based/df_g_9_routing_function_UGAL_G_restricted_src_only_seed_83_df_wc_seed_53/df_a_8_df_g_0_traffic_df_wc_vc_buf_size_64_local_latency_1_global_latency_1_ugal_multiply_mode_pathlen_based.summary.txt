wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 53;
routing_function = UGAL_G_restricted_src_only;
seed = 83;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49986,   0.49988,    19.432,    19.432,         0,      4.6805
simulation,         2,      0.74,   0.54137,   0.58308,    1090.4,    611.84,         1,         0.0
simulation,         3,      0.62,   0.55219,   0.58039,    449.73,    355.88,         1,         0.0
simulation,         4,      0.56,   0.55987,   0.55987,    38.153,    38.164,         0,       4.731
simulation,         5,      0.59,   0.55456,   0.56498,    499.29,    298.23,         1,         0.0
simulation,         6,      0.57,   0.55694,    0.5573,    502.49,    247.45,         1,         0.0
simulation,         7,       0.1,  0.099857,  0.099855,    11.286,    11.286,         0,      4.4282
simulation,         8,       0.2,   0.19995,   0.19995,    12.113,    12.113,         0,      4.4717
simulation,         9,       0.3,   0.29998,   0.29998,    13.224,    13.224,         0,      4.5105
simulation,        10,       0.4,   0.39988,   0.39988,     15.27,     15.27,         0,       4.596
simulation,        11,       0.5,   0.49986,   0.49988,    19.432,    19.432,         0,      4.6805
