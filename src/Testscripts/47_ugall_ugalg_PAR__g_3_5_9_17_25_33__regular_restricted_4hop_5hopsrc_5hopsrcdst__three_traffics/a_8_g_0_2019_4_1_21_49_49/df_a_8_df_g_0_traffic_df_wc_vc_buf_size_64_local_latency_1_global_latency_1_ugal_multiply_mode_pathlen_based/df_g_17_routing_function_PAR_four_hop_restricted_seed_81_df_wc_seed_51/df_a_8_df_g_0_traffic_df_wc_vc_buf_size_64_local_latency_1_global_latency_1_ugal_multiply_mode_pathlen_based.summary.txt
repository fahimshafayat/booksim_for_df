wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 51;
routing_function = PAR_four_hop_restricted;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.38797,    0.4253,     842.2,    657.16,         1,         0.0
simulation,         2,      0.25,   0.24999,   0.24999,    18.257,    18.282,         0,      4.7877
simulation,         3,      0.37,   0.35917,   0.35928,    606.85,    297.06,         1,         0.0
simulation,         4,      0.31,   0.31003,   0.31005,    45.567,    45.729,         0,      4.8426
simulation,         5,      0.34,   0.33851,   0.33851,     257.2,    93.793,         1,         0.0
simulation,         6,      0.32,   0.32004,   0.32007,     53.59,    53.952,         0,      4.8533
simulation,         7,      0.33,   0.32917,   0.32922,     312.0,    78.125,         0,      4.8685
simulation,         8,       0.1,  0.099989,  0.099991,    11.989,    11.989,         0,      4.7442
simulation,         9,       0.2,   0.20005,   0.20005,    12.875,    12.875,         0,      4.7684
simulation,        10,       0.3,   0.30004,   0.30005,     38.57,    38.715,         0,      4.8294
