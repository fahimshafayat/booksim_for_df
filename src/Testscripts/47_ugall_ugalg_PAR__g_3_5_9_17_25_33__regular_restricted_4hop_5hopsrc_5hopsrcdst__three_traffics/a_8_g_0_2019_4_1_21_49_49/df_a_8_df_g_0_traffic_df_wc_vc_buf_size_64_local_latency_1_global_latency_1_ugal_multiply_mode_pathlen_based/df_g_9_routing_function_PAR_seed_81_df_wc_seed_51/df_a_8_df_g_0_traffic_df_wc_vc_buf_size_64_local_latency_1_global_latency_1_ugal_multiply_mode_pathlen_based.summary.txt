wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 51;
routing_function = PAR;
seed = 81;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.47249,   0.48194,    469.47,    318.24,         1,         0.0
simulation,         2,      0.25,   0.24984,   0.24984,    15.085,    15.085,         0,      5.4084
simulation,         3,      0.37,      0.37,   0.36999,    21.514,    21.514,         0,      5.7956
simulation,         4,      0.43,   0.42997,   0.42997,    29.632,    29.632,         0,      5.9518
simulation,         5,      0.46,   0.45939,   0.45939,    155.05,    68.433,         0,      5.9951
simulation,         6,      0.48,   0.46942,   0.46976,    491.85,    239.64,         1,         0.0
simulation,         7,      0.47,   0.46492,   0.46496,    519.46,     133.3,         1,         0.0
simulation,         8,       0.1,  0.099919,  0.099918,    13.415,    13.415,         0,      5.4409
simulation,         9,       0.2,    0.1998,    0.1998,    14.175,    14.175,         0,      5.3741
simulation,        10,       0.3,   0.29995,   0.29995,    16.942,    16.942,         0,      5.5332
simulation,        11,       0.4,   0.39998,   0.39997,    24.535,    24.536,         0,       5.883
