alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
perm_seed = 56;
routing_function = PAR_restricted_src_and_dst;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50021,   0.50023,    12.359,     12.36,         0,      4.0731
simulation,         2,      0.74,      0.74,   0.74001,    22.033,    22.044,         0,      4.1117
simulation,         3,      0.86,   0.76062,   0.79744,    474.45,    349.46,         1,         0.0
simulation,         4,       0.8,   0.76048,   0.76157,     985.3,    355.33,         1,         0.0
simulation,         5,      0.77,   0.75926,   0.75945,    510.25,    216.41,         1,         0.0
simulation,         6,      0.75,   0.75004,   0.75001,    24.819,    24.828,         0,      4.1374
simulation,         7,      0.76,   0.75959,   0.75958,     68.03,    33.228,         0,       4.171
simulation,         8,      0.15,   0.15013,   0.15013,    11.026,    11.026,         0,      4.3029
simulation,         9,       0.3,   0.30015,   0.30016,    11.379,     11.38,         0,      4.1919
simulation,        10,      0.45,   0.45022,   0.45023,    12.012,    12.012,         0,      4.0956
simulation,        11,       0.6,   0.60021,   0.60021,    13.507,    13.507,         0,      4.0454
simulation,        12,      0.75,   0.75004,   0.75001,    24.819,    24.828,         0,      4.1374
