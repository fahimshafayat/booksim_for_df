alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
perm_seed = 54;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 84;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49988,   0.49987,    48.213,    36.833,         0,      4.5311
simulation,         2,      0.74,   0.58056,   0.61827,    737.99,    500.79,         1,         0.0
simulation,         3,      0.62,   0.57528,   0.58517,    526.14,    288.41,         1,         0.0
simulation,         4,      0.56,   0.55619,   0.55622,    366.79,    109.53,         1,         0.0
simulation,         5,      0.53,   0.52955,   0.52956,    114.92,     51.88,         0,      4.6076
simulation,         6,      0.54,   0.53931,   0.53931,    163.38,    59.953,         0,      4.6413
simulation,         7,      0.55,   0.54842,   0.54843,    317.45,    78.072,         0,      4.6765
simulation,         8,       0.1,  0.099976,  0.099979,    11.112,    11.112,         0,      4.4253
simulation,         9,       0.2,   0.19993,   0.19993,    11.571,    11.571,         0,      4.4611
simulation,        10,       0.3,   0.29997,   0.29997,     12.66,    12.661,         0,      4.4318
simulation,        11,       0.4,   0.39995,   0.39995,    17.447,     17.46,         0,      4.4324
simulation,        12,       0.5,   0.49988,   0.49987,    48.213,    36.833,         0,      4.5311
