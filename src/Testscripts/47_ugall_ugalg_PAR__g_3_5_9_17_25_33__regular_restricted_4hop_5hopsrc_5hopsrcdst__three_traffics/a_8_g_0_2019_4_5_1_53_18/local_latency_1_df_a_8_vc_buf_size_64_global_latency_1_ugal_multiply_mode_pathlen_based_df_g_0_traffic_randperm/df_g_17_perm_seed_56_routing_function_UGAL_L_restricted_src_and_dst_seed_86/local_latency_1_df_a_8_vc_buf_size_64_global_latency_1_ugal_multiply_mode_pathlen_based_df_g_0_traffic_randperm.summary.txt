alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
perm_seed = 56;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49983,   0.49984,    14.386,     14.39,         0,      4.1271
simulation,         2,      0.74,   0.68614,   0.69769,    562.21,    284.59,         1,         0.0
simulation,         3,      0.62,   0.61985,   0.61986,    31.591,    31.624,         0,      4.2086
simulation,         4,      0.68,   0.67288,   0.67285,    521.07,    125.48,         1,         0.0
simulation,         5,      0.65,   0.64913,   0.64914,    124.04,    50.191,         0,      4.2663
simulation,         6,      0.66,   0.65859,   0.65861,     197.7,     61.98,         0,      4.2925
simulation,         7,      0.67,   0.66643,   0.66641,     477.7,    88.347,         0,      4.3178
simulation,         8,      0.15,   0.14998,   0.14998,    10.796,    10.797,         0,      4.2108
simulation,         9,       0.3,    0.2999,    0.2999,    11.299,      11.3,         0,      4.1736
simulation,        10,      0.45,   0.44989,    0.4499,    12.796,    12.797,         0,      4.1263
simulation,        11,       0.6,   0.59988,   0.59987,    26.778,    26.799,         0,      4.1833
