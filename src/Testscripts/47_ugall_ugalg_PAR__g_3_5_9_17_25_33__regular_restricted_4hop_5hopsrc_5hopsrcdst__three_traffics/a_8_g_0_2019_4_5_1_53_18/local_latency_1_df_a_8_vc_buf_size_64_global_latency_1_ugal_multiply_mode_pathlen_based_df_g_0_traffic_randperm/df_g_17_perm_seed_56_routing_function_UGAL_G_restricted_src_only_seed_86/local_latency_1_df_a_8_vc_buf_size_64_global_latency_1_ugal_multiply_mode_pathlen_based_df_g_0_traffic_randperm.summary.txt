alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
perm_seed = 56;
routing_function = UGAL_G_restricted_src_only;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49983,   0.49984,    11.848,    11.848,         0,      3.9211
simulation,         2,      0.74,   0.73994,   0.73993,    20.197,    20.198,         0,      3.9832
simulation,         3,      0.86,    0.7882,    0.8009,    612.29,    275.37,         1,         0.0
simulation,         4,       0.8,   0.79971,   0.79968,     73.36,    65.443,         0,      4.0762
simulation,         5,      0.83,    0.7898,   0.79015,    853.83,    242.74,         1,         0.0
simulation,         6,      0.81,   0.79514,   0.79536,    513.11,    152.36,         1,         0.0
simulation,         7,      0.15,   0.14998,   0.14998,    10.376,    10.376,         0,      4.0217
simulation,         8,       0.3,    0.2999,    0.2999,    10.764,    10.764,         0,        3.97
simulation,         9,      0.45,   0.44989,    0.4499,    11.469,    11.469,         0,      3.9301
simulation,        10,       0.6,   0.59987,   0.59987,    13.138,    13.138,         0,      3.9167
simulation,        11,      0.75,   0.74988,   0.74988,    21.675,    21.676,         0,      3.9937
