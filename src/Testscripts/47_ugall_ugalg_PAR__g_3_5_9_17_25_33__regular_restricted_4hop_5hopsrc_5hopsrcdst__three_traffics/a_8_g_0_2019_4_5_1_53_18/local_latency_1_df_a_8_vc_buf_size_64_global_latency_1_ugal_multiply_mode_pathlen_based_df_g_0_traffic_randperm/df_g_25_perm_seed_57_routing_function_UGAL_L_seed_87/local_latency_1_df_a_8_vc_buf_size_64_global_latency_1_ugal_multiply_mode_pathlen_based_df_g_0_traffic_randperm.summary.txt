alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
perm_seed = 57;
routing_function = UGAL_L;
seed = 87;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49905,   0.49903,    207.07,    62.759,         0,      4.7533
simulation,         2,      0.74,   0.55261,   0.59152,    808.97,    548.55,         1,         0.0
simulation,         3,      0.62,   0.54562,   0.55814,    787.07,    388.78,         1,         0.0
simulation,         4,      0.56,   0.53476,   0.53499,    762.08,    252.45,         1,         0.0
simulation,         5,      0.53,   0.52239,   0.52243,    495.36,    128.54,         1,         0.0
simulation,         6,      0.51,   0.50789,   0.50788,    443.62,    76.397,         0,      4.7961
simulation,         7,      0.52,   0.51552,   0.51553,    426.93,    98.596,         1,         0.0
simulation,         8,       0.1,  0.099988,  0.099988,    11.363,    11.363,         0,      4.5373
simulation,         9,       0.2,   0.20004,   0.20004,    11.724,    11.725,         0,      4.5118
simulation,        10,       0.3,   0.29991,   0.29991,    12.684,    12.684,         0,      4.4745
simulation,        11,       0.4,   0.39999,   0.39998,    20.426,    20.445,         0,      4.5076
simulation,        12,       0.5,   0.49905,   0.49903,    207.07,    62.759,         0,      4.7533
