alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
perm_seed = 57;
routing_function = PAR;
seed = 87;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49988,   0.49988,    26.735,    26.759,         0,      5.0317
simulation,         2,      0.74,   0.54753,   0.59664,    994.61,    695.15,         1,         0.0
simulation,         3,      0.62,    0.5474,    0.5799,    464.41,    404.49,         1,         0.0
simulation,         4,      0.56,   0.55189,   0.55196,    520.78,    214.51,         1,         0.0
simulation,         5,      0.53,   0.52979,   0.52979,    52.197,    39.248,         0,      5.2161
simulation,         6,      0.54,   0.53962,   0.53963,    85.699,    50.395,         0,      5.3252
simulation,         7,      0.55,   0.54929,   0.54928,    163.52,    81.307,         0,      5.4998
simulation,         8,       0.1,  0.099913,  0.099913,    12.623,    12.624,         0,      5.1187
simulation,         9,       0.2,   0.19994,   0.19995,    12.798,    12.798,         0,      4.9634
simulation,        10,       0.3,   0.29988,   0.29989,    13.305,    13.306,         0,      4.8587
simulation,        11,       0.4,   0.39993,   0.39993,    15.546,    15.549,         0,      4.8404
simulation,        12,       0.5,   0.49988,   0.49988,    26.735,    26.759,         0,      5.0317
