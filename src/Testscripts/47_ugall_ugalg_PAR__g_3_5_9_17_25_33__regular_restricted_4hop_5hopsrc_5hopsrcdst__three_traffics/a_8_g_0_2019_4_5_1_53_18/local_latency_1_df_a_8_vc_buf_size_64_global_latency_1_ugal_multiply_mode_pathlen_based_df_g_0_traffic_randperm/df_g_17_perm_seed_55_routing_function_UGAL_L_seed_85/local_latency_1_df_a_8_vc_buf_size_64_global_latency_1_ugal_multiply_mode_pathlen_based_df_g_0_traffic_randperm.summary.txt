alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
perm_seed = 55;
routing_function = UGAL_L;
seed = 85;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50003,   0.50004,    19.776,    19.785,         0,      4.3193
simulation,         2,      0.74,   0.62307,   0.65688,    581.32,    411.21,         1,         0.0
simulation,         3,      0.62,   0.60737,   0.60713,    467.34,    142.38,         1,         0.0
simulation,         4,      0.56,   0.56007,   0.56005,    32.602,    32.644,         0,      4.4151
simulation,         5,      0.59,   0.58631,   0.58632,    306.14,    73.835,         1,         0.0
simulation,         6,      0.57,   0.57003,   0.57005,    38.816,    38.916,         0,      4.4437
simulation,         7,      0.58,   0.57776,   0.57773,    358.46,     56.17,         0,      4.4729
simulation,         8,       0.1,  0.099932,   0.09993,    11.106,    11.106,         0,      4.4219
simulation,         9,       0.2,   0.19983,   0.19983,    11.389,    11.389,         0,      4.3882
simulation,        10,       0.3,   0.29981,   0.29981,    11.751,    11.751,         0,       4.328
simulation,        11,       0.4,   0.39997,   0.39997,    13.263,    13.264,         0,      4.2938
simulation,        12,       0.5,   0.50003,   0.50004,    19.776,    19.785,         0,      4.3193
