alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
perm_seed = 54;
routing_function = UGAL_G_four_hop_restricted;
seed = 84;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50001,   0.50002,    13.007,    13.007,         0,      4.0619
simulation,         2,      0.74,   0.67292,   0.68475,    620.05,    303.41,         1,         0.0
simulation,         3,      0.62,   0.61999,   0.61999,    17.682,    17.683,         0,       4.092
simulation,         4,      0.68,   0.67995,   0.67994,    50.744,    50.724,         0,      4.1551
simulation,         5,      0.71,   0.67508,   0.67518,    831.16,     249.8,         1,         0.0
simulation,         6,      0.69,    0.6779,   0.67793,    498.26,    158.47,         1,         0.0
simulation,         7,      0.15,   0.14995,   0.14995,     10.53,     10.53,         0,      4.0822
simulation,         8,       0.3,   0.29997,   0.29997,    11.174,    11.174,         0,      4.0753
simulation,         9,      0.45,   0.44999,   0.44999,    12.326,    12.326,         0,      4.0626
simulation,        10,       0.6,   0.59999,       0.6,    16.156,    16.156,         0,      4.0816
