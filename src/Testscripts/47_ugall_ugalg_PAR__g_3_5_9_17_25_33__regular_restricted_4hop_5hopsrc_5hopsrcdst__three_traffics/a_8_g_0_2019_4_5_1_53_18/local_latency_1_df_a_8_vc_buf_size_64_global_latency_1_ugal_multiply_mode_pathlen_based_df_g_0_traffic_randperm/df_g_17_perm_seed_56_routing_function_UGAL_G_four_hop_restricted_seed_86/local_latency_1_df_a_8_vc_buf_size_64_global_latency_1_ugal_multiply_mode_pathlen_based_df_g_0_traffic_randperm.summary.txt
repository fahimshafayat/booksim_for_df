alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
perm_seed = 56;
routing_function = UGAL_G_four_hop_restricted;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49983,   0.49984,    11.649,    11.649,         0,       3.841
simulation,         2,      0.74,   0.73993,   0.73993,    17.553,    17.554,         0,      3.8415
simulation,         3,      0.86,   0.79605,   0.80768,    534.69,    249.21,         1,         0.0
simulation,         4,       0.8,   0.79991,    0.7999,     37.74,    37.748,         0,      3.8743
simulation,         5,      0.83,   0.79859,   0.79864,    687.52,     199.1,         1,         0.0
simulation,         6,      0.81,   0.80078,   0.80081,    527.55,    124.61,         1,         0.0
simulation,         7,      0.15,   0.14998,   0.14998,    10.101,    10.101,         0,      3.8919
simulation,         8,       0.3,    0.2999,    0.2999,    10.557,    10.557,         0,      3.8704
simulation,         9,      0.45,    0.4499,    0.4499,    11.297,    11.297,         0,      3.8485
simulation,        10,       0.6,   0.59987,   0.59987,    12.709,    12.709,         0,        3.83
simulation,        11,      0.75,   0.74988,   0.74988,     18.52,    18.521,         0,      3.8451
