alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
perm_seed = 57;
routing_function = UGAL_L_four_hop_restricted;
seed = 87;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49998,   0.49998,    13.238,    13.241,         0,       3.901
simulation,         2,      0.74,   0.70227,   0.70197,    913.34,    246.67,         1,         0.0
simulation,         3,      0.62,   0.62008,   0.62008,    27.876,    27.903,         0,      3.9312
simulation,         4,      0.68,   0.67532,   0.67537,     372.6,    83.214,         1,         0.0
simulation,         5,      0.65,   0.64937,   0.64939,    98.883,    47.603,         0,      3.9541
simulation,         6,      0.66,   0.65841,   0.65841,    235.12,    59.379,         0,       3.962
simulation,         7,      0.67,   0.66707,   0.66707,    413.42,    67.459,         0,      3.9711
simulation,         8,      0.15,   0.15018,   0.15018,     10.13,    10.131,         0,      3.9103
simulation,         9,       0.3,   0.29995,   0.29995,    10.632,    10.632,         0,      3.9143
simulation,        10,      0.45,   0.45005,   0.45005,    11.573,    11.574,         0,      3.9015
simulation,        11,       0.6,   0.60003,   0.60004,    23.609,    23.628,         0,      3.9211
