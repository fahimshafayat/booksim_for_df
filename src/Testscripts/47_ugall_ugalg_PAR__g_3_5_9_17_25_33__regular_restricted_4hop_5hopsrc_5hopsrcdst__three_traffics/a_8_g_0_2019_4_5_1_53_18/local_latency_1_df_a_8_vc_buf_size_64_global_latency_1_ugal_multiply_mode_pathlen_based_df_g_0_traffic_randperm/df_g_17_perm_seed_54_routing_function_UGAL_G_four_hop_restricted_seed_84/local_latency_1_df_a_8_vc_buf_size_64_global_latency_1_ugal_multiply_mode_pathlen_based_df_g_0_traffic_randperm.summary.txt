alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
perm_seed = 54;
routing_function = UGAL_G_four_hop_restricted;
seed = 84;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49984,   0.49984,    11.653,    11.653,         0,      3.8522
simulation,         2,      0.74,   0.73997,   0.73997,    17.235,    17.236,         0,       3.846
simulation,         3,      0.86,   0.79624,   0.80779,    535.65,    247.52,         1,         0.0
simulation,         4,       0.8,    0.8001,    0.8001,    34.785,    34.788,         0,      3.8748
simulation,         5,      0.83,    0.7966,   0.79719,    721.84,    218.72,         1,         0.0
simulation,         6,      0.81,   0.80249,   0.80258,    393.19,    116.77,         1,         0.0
simulation,         7,      0.15,   0.15001,   0.15001,    10.133,    10.133,         0,      3.9074
simulation,         8,       0.3,   0.29987,   0.29986,    10.581,    10.581,         0,      3.8839
simulation,         9,      0.45,   0.44988,   0.44988,    11.308,    11.308,         0,        3.86
simulation,        10,       0.6,   0.59993,   0.59993,    12.679,    12.679,         0,        3.84
simulation,        11,      0.75,   0.74998,   0.74998,    18.127,    18.128,         0,      3.8487
