alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
perm_seed = 55;
routing_function = UGAL_G_restricted_src_and_dst;
seed = 85;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50004,   0.50004,    12.221,    12.221,         0,      3.9921
simulation,         2,      0.74,   0.74003,   0.74003,    26.796,    26.798,         0,      4.1113
simulation,         3,      0.86,   0.75267,   0.78248,    457.34,    307.64,         1,         0.0
simulation,         4,       0.8,   0.75836,   0.75866,    879.74,    243.18,         1,         0.0
simulation,         5,      0.77,      0.77,   0.77002,    54.692,    54.699,         0,      4.1775
simulation,         6,      0.78,   0.76331,   0.76329,    469.23,    158.07,         1,         0.0
simulation,         7,      0.15,   0.14992,   0.14992,    10.554,    10.554,         0,      4.0997
simulation,         8,       0.3,   0.29981,   0.29981,    10.965,    10.965,         0,      4.0408
simulation,         9,      0.45,   0.44999,   0.44998,    11.767,    11.767,         0,      3.9998
simulation,        10,       0.6,       0.6,       0.6,    13.922,    13.923,         0,      3.9986
simulation,        11,      0.75,   0.75002,   0.75001,     30.94,    30.944,         0,      4.1296
