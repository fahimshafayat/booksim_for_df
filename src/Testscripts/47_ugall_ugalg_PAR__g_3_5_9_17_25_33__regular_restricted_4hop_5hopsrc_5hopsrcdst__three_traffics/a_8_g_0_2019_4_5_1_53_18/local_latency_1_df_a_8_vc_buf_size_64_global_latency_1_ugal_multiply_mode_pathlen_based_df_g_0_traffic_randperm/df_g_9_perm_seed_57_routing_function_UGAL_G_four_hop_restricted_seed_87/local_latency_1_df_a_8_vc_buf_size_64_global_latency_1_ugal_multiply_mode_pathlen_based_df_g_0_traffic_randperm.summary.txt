alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
perm_seed = 57;
routing_function = UGAL_G_four_hop_restricted;
seed = 87;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50017,   0.50017,    10.725,    10.725,         0,      3.6265
simulation,         2,      0.74,    0.7402,    0.7402,    12.711,    12.711,         0,      3.5968
simulation,         3,      0.86,   0.86017,   0.86017,    15.775,    15.776,         0,      3.5978
simulation,         4,      0.92,    0.9201,   0.92008,    20.484,    20.484,         0,      3.6091
simulation,         5,      0.95,   0.95007,   0.95007,    27.261,    27.264,         0,      3.6189
simulation,         6,      0.97,    0.9701,   0.97007,    42.042,     42.05,         0,      3.6304
simulation,         7,      0.98,   0.96491,     0.965,    504.74,    132.86,         1,         0.0
simulation,         8,       0.2,   0.19999,   0.19999,    9.7303,    9.7305,         0,      3.6727
simulation,         9,       0.4,   0.39993,   0.39993,    10.295,    10.295,         0,      3.6414
simulation,        10,       0.6,    0.6001,   0.60011,    11.311,    11.311,         0,      3.6125
simulation,        11,       0.8,   0.80024,   0.80023,    13.829,    13.829,         0,      3.5945
