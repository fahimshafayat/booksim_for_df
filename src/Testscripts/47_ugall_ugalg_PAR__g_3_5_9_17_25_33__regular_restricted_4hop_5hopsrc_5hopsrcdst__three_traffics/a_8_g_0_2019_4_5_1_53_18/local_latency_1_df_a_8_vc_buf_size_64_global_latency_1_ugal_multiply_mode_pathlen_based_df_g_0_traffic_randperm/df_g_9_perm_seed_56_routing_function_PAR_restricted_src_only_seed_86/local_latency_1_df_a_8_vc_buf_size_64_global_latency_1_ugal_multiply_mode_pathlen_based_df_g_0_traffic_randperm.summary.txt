alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
perm_seed = 56;
routing_function = PAR_restricted_src_only;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50021,   0.50023,    12.263,    12.264,         0,      4.0502
simulation,         2,      0.74,   0.74002,   0.74001,    18.933,    18.935,         0,      4.0743
simulation,         3,      0.86,   0.76388,    0.7986,    456.79,    340.32,         1,         0.0
simulation,         4,       0.8,   0.76464,   0.76523,    896.14,    312.81,         1,         0.0
simulation,         5,      0.77,   0.77004,   0.76998,      31.2,    31.215,         0,      4.1692
simulation,         6,      0.78,   0.75966,   0.76043,     489.3,    254.01,         1,         0.0
simulation,         7,      0.15,   0.15013,   0.15013,    10.914,    10.915,         0,      4.2524
simulation,         8,       0.3,   0.30015,   0.30016,     11.28,     11.28,         0,      4.1543
simulation,         9,      0.45,   0.45022,   0.45023,    11.927,    11.927,         0,      4.0714
simulation,        10,       0.6,   0.60021,   0.60021,    13.336,    13.336,         0,      4.0234
simulation,        11,      0.75,   0.75003,   0.75001,    20.517,    20.519,         0,      4.0934
