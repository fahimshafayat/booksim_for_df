alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
perm_seed = 57;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 87;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50017,   0.50017,    11.218,    11.218,         0,      3.7881
simulation,         2,      0.74,   0.74018,    0.7402,    18.617,    18.627,         0,      3.7736
simulation,         3,      0.86,    0.8452,   0.84542,    479.86,    135.22,         1,         0.0
simulation,         4,       0.8,   0.79866,   0.79865,    166.47,    35.362,         0,      3.8029
simulation,         5,      0.83,   0.82577,   0.82585,    289.27,    58.613,         1,         0.0
simulation,         6,      0.81,   0.80806,   0.80806,    244.52,    39.064,         0,      3.8117
simulation,         7,      0.82,   0.81731,   0.81731,    343.92,    47.752,         0,      3.8223
simulation,         8,      0.15,   0.15008,   0.15008,    10.031,    10.031,         0,       3.865
simulation,         9,       0.3,   0.29995,   0.29995,    10.431,    10.432,         0,      3.8462
simulation,        10,      0.45,   0.45006,   0.45005,    10.963,    10.963,         0,      3.8014
simulation,        11,       0.6,    0.6001,   0.60011,    12.014,    12.015,         0,      3.7687
simulation,        12,      0.75,   0.75023,   0.75023,    19.802,    19.811,         0,      3.7763
