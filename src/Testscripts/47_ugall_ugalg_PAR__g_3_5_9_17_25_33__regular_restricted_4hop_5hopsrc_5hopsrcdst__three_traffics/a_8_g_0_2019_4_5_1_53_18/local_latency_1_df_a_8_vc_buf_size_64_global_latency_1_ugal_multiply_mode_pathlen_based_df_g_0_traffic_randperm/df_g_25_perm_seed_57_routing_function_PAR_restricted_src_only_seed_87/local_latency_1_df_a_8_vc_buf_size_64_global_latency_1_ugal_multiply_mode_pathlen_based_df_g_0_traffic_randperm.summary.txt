alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
perm_seed = 57;
routing_function = PAR_restricted_src_only;
seed = 87;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49999,   0.49999,    17.496,      17.5,         0,      4.6247
simulation,         2,      0.74,   0.54555,    0.5894,     887.0,    573.21,         1,         0.0
simulation,         3,      0.62,   0.55363,   0.56967,    627.22,    374.24,         1,         0.0
simulation,         4,      0.56,   0.55991,   0.55991,    24.635,    24.649,         0,      4.7449
simulation,         5,      0.59,   0.58902,   0.58904,    196.85,    59.751,         0,      4.9194
simulation,         6,       0.6,   0.56063,   0.56416,    756.81,    399.45,         1,         0.0
simulation,         7,       0.1,  0.099987,  0.099988,    11.868,    11.868,         0,      4.7662
simulation,         8,       0.2,   0.20004,   0.20004,    12.207,    12.207,         0,      4.7097
simulation,         9,       0.3,   0.29991,   0.29991,    12.651,    12.651,         0,      4.6283
simulation,        10,       0.4,   0.39998,   0.39998,     13.62,    13.621,         0,      4.5858
simulation,        11,       0.5,   0.49999,   0.49999,    17.496,      17.5,         0,      4.6247
