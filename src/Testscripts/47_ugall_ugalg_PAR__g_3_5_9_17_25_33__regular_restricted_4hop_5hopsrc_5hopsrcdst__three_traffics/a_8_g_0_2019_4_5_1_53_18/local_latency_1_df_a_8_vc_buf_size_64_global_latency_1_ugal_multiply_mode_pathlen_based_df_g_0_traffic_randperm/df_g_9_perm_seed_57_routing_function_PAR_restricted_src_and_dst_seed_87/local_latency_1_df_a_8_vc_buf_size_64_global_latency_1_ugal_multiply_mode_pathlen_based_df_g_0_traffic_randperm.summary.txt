alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
perm_seed = 57;
routing_function = PAR_restricted_src_and_dst;
seed = 87;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50017,   0.50017,    11.863,    11.863,         0,      3.9554
simulation,         2,      0.74,    0.7402,    0.7402,    16.138,    16.139,         0,      3.9326
simulation,         3,      0.86,   0.80876,   0.82403,    493.05,    285.85,         1,         0.0
simulation,         4,       0.8,   0.80022,   0.80023,    24.881,    24.885,         0,      4.0251
simulation,         5,      0.83,   0.80825,   0.81023,    621.97,     241.1,         1,         0.0
simulation,         6,      0.81,   0.81018,   0.81024,    37.688,    37.712,         0,      4.0932
simulation,         7,      0.82,   0.80786,   0.80815,    534.08,    213.72,         1,         0.0
simulation,         8,      0.15,   0.15008,   0.15008,    10.728,    10.728,         0,      4.1701
simulation,         9,       0.3,   0.29995,   0.29995,    11.043,    11.043,         0,      4.0685
simulation,        10,      0.45,   0.45006,   0.45005,    11.583,    11.583,         0,      3.9781
simulation,        11,       0.6,    0.6001,   0.60011,    12.723,    12.723,         0,      3.9226
simulation,        12,      0.75,   0.75024,   0.75023,    16.713,    16.714,         0,        3.94
