alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
perm_seed = 54;
routing_function = PAR_restricted_src_only;
seed = 84;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49994,   0.49994,    15.949,    15.951,         0,      4.5223
simulation,         2,      0.74,   0.57189,   0.61507,    773.94,    516.22,         1,         0.0
simulation,         3,      0.62,   0.57608,   0.58361,    753.76,    419.89,         1,         0.0
simulation,         4,      0.56,   0.55988,   0.55988,    20.904,    20.912,         0,       4.601
simulation,         5,      0.59,    0.5899,    0.5899,    27.277,     27.29,         0,      4.6913
simulation,         6,       0.6,   0.59992,   0.59993,    33.707,     33.73,         0,      4.7444
simulation,         7,      0.61,   0.60948,   0.60955,    96.519,    62.098,         0,      4.8277
simulation,         8,      0.15,   0.14999,   0.14998,      11.9,      11.9,         0,      4.6876
simulation,         9,       0.3,   0.29988,   0.29988,     12.43,     12.43,         0,      4.5609
simulation,        10,      0.45,   0.44996,   0.44995,    14.064,    14.065,         0,      4.5028
simulation,        11,       0.6,   0.59992,   0.59993,    33.707,     33.73,         0,      4.7444
