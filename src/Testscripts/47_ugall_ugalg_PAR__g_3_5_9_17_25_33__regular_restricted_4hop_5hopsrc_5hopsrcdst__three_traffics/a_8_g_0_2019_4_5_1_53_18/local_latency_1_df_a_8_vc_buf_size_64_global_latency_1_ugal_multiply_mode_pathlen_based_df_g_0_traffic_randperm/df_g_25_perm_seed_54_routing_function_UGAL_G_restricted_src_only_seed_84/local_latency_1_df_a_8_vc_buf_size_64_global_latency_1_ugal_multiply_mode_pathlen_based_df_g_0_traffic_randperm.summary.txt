alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
perm_seed = 54;
routing_function = UGAL_G_restricted_src_only;
seed = 84;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49994,   0.49994,    12.836,    12.836,         0,      4.0765
simulation,         2,      0.74,   0.70868,   0.70887,    739.58,    221.73,         1,         0.0
simulation,         3,      0.62,    0.6199,    0.6199,    17.031,    17.032,         0,      4.1406
simulation,         4,      0.68,   0.67985,   0.67987,    25.806,    25.808,         0,      4.2254
simulation,         5,      0.71,   0.70982,   0.70984,    51.593,    51.609,         0,      4.3047
simulation,         6,      0.72,   0.70937,    0.7094,    497.21,    143.39,         1,         0.0
simulation,         7,      0.15,   0.14998,   0.14998,    10.662,    10.662,         0,      4.1494
simulation,         8,       0.3,   0.29988,   0.29988,    11.148,    11.148,         0,      4.1034
simulation,         9,      0.45,   0.44996,   0.44995,    12.157,    12.157,         0,      4.0741
simulation,        10,       0.6,   0.59993,   0.59993,    15.831,    15.832,         0,      4.1226
