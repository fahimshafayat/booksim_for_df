alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
perm_seed = 57;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 87;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49981,    0.4998,    47.079,    40.232,         0,      4.5754
simulation,         2,      0.74,   0.57664,   0.61472,     758.8,    512.85,         1,         0.0
simulation,         3,      0.62,   0.57125,   0.58192,    560.51,    310.23,         1,         0.0
simulation,         4,      0.56,   0.55417,   0.55419,     502.5,    115.65,         1,         0.0
simulation,         5,      0.53,    0.5294,   0.52943,    142.44,    57.414,         0,      4.6556
simulation,         6,      0.54,   0.53888,    0.5389,     238.4,    68.975,         0,      4.6907
simulation,         7,      0.55,   0.54699,   0.54699,    302.94,     91.75,         1,         0.0
simulation,         8,       0.1,  0.099914,  0.099913,    11.162,    11.162,         0,      4.4483
simulation,         9,       0.2,   0.19995,   0.19995,    11.623,    11.623,         0,      4.4821
simulation,        10,       0.3,   0.29989,   0.29989,    12.183,    12.183,         0,      4.4535
simulation,        11,       0.4,   0.39993,   0.39993,    17.393,    17.401,         0,      4.4602
simulation,        12,       0.5,   0.49981,    0.4998,    47.079,    40.232,         0,      4.5754
