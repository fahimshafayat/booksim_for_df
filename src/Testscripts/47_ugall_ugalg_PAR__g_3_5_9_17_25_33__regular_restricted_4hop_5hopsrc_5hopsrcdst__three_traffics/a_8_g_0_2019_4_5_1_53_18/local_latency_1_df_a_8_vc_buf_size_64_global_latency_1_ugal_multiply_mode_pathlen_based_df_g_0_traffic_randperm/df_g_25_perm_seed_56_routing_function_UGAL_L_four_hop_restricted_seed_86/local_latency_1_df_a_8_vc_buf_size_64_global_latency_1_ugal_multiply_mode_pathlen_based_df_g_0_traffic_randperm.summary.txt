alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
perm_seed = 56;
routing_function = UGAL_L_four_hop_restricted;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49885,   0.49884,    233.37,     36.71,         0,      4.1151
simulation,         2,      0.74,    0.6121,   0.64213,    553.36,    377.27,         1,         0.0
simulation,         3,      0.62,   0.58961,   0.59011,    726.52,    213.33,         1,         0.0
simulation,         4,      0.56,   0.55415,   0.55418,     445.6,    84.551,         1,         0.0
simulation,         5,      0.53,   0.52711,   0.52712,    255.12,    55.344,         1,         0.0
simulation,         6,      0.51,   0.50835,   0.50834,    335.02,    43.641,         0,      4.1211
simulation,         7,      0.52,   0.51774,   0.51775,     445.8,    49.666,         0,      4.1266
simulation,         8,       0.1,  0.099922,  0.099925,    10.302,    10.302,         0,      4.0446
simulation,         9,       0.2,   0.19987,   0.19988,    10.699,    10.699,         0,      4.0812
simulation,        10,       0.3,   0.29978,   0.29979,    11.507,    11.508,         0,       4.083
simulation,        11,       0.4,   0.39973,   0.39974,    15.339,    15.348,         0,      4.0865
simulation,        12,       0.5,   0.49885,   0.49884,    233.37,     36.71,         0,      4.1151
