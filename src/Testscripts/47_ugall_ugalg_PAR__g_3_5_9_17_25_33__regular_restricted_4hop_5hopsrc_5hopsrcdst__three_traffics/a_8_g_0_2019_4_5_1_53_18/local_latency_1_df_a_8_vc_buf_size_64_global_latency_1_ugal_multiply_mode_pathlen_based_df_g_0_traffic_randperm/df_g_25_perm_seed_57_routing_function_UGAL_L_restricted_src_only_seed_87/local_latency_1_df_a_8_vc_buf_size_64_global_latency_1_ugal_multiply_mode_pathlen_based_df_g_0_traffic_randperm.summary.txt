alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
perm_seed = 57;
routing_function = UGAL_L_restricted_src_only;
seed = 87;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49999,   0.49999,    33.637,    33.724,         0,      4.4119
simulation,         2,      0.74,    0.5468,   0.58814,    844.59,    547.91,         1,         0.0
simulation,         3,      0.62,   0.55709,   0.57012,    592.51,    332.61,         1,         0.0
simulation,         4,      0.56,   0.55113,   0.55125,    515.28,    117.48,         1,         0.0
simulation,         5,      0.53,   0.52933,   0.52936,    130.78,    49.843,         0,      4.4676
simulation,         6,      0.54,   0.53823,   0.53828,    341.62,    64.574,         0,       4.497
simulation,         7,      0.55,    0.5459,   0.54585,    369.51,    92.044,         1,         0.0
simulation,         8,       0.1,  0.099988,  0.099988,    10.948,    10.948,         0,      4.3453
simulation,         9,       0.2,   0.20004,   0.20004,    11.384,    11.384,         0,      4.3744
simulation,        10,       0.3,   0.29991,   0.29991,    11.903,    11.904,         0,      4.3449
simulation,        11,       0.4,   0.39999,   0.39998,    16.179,    16.188,         0,      4.3407
simulation,        12,       0.5,   0.49999,   0.49999,    33.637,    33.724,         0,      4.4119
