alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
perm_seed = 57;
routing_function = UGAL_L;
seed = 87;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50017,   0.50017,    11.594,    11.594,         0,      3.8898
simulation,         2,      0.74,    0.7402,    0.7402,    24.359,    24.373,         0,      3.9182
simulation,         3,      0.86,   0.79934,   0.81187,    541.51,    250.27,         1,         0.0
simulation,         4,       0.8,   0.78756,    0.7877,    485.31,    119.32,         1,         0.0
simulation,         5,      0.77,   0.76774,   0.76776,    283.55,    48.972,         0,      3.9657
simulation,         6,      0.78,   0.77604,   0.77611,    460.96,    59.803,         0,      3.9848
simulation,         7,      0.79,   0.78346,   0.78349,    472.35,    75.052,         1,         0.0
simulation,         8,      0.15,   0.15008,   0.15008,    10.508,    10.508,         0,      4.0834
simulation,         9,       0.3,   0.29995,   0.29995,    10.776,    10.776,         0,      3.9876
simulation,        10,      0.45,   0.45006,   0.45005,      11.3,    11.301,         0,       3.908
simulation,        11,       0.6,   0.60011,   0.60011,    12.789,     12.79,         0,      3.8653
simulation,        12,      0.75,    0.7502,   0.75023,    27.208,     27.23,         0,      3.9324
