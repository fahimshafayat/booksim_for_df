alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
perm_seed = 55;
routing_function = UGAL_L_restricted_src_only;
seed = 85;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50001,   0.50001,    32.919,    32.967,         0,      4.4814
simulation,         2,      0.74,   0.56712,   0.60721,    794.17,    517.83,         1,         0.0
simulation,         3,      0.62,   0.57621,   0.58686,    492.77,    281.89,         1,         0.0
simulation,         4,      0.56,   0.55754,   0.55755,    451.59,    83.055,         0,      4.6331
simulation,         5,      0.59,   0.57266,    0.5729,    529.89,    207.73,         1,         0.0
simulation,         6,      0.57,   0.56388,   0.56396,    517.62,    123.13,         1,         0.0
simulation,         7,       0.1,   0.10004,   0.10004,    11.063,    11.063,         0,      4.4038
simulation,         8,       0.2,   0.20003,   0.20003,    11.502,    11.502,         0,      4.4352
simulation,         9,       0.3,   0.30011,   0.30011,    12.182,    12.183,         0,      4.4043
simulation,        10,       0.4,   0.40008,   0.40008,    16.213,    16.218,         0,      4.3953
simulation,        11,       0.5,   0.50001,   0.50001,    32.919,    32.967,         0,      4.4814
