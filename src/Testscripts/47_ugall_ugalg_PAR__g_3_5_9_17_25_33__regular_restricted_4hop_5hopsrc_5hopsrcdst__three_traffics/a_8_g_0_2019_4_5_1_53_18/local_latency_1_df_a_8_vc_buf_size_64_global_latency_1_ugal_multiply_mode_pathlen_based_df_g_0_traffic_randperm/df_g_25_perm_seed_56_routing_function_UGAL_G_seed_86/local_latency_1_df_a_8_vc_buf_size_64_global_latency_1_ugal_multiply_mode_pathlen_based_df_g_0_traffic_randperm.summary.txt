alloc_iters = 1;
sample_period = 10000;
num_vcs = 8;
credit_delay = 2;
output_speedup = 1;
sim_count = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sw_alloc_delay = 1;
input_speedup = 1;
wait_for_tail_credit = 0;
warmup_periods = 3;
vc_alloc_delay = 1;
routing_delay = 0;
injection_rate_uses_flits = 1;
priority = none;
internal_speedup = 4.0;
st_final_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
perm_seed = 56;
routing_function = UGAL_G;
seed = 86;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49976,   0.49976,    14.428,    14.429,         0,      4.2386
simulation,         2,      0.74,   0.64327,   0.67035,     450.5,     325.9,         1,         0.0
simulation,         3,      0.62,   0.61977,   0.61979,    28.828,    28.832,         0,      4.4705
simulation,         4,      0.68,   0.64611,   0.64634,    830.92,     250.3,         1,         0.0
simulation,         5,      0.65,   0.64908,   0.64907,    163.87,    72.487,         0,      4.6198
simulation,         6,      0.66,    0.6488,   0.64884,    500.25,    145.06,         1,         0.0
simulation,         7,      0.15,   0.14988,   0.14989,    10.986,    10.986,         0,      4.2959
simulation,         8,       0.3,   0.29978,   0.29979,    11.458,    11.458,         0,      4.2136
simulation,         9,      0.45,   0.44978,   0.44978,    13.039,    13.039,         0,      4.2041
simulation,        10,       0.6,    0.5998,   0.59981,    23.186,    23.189,         0,      4.4086
