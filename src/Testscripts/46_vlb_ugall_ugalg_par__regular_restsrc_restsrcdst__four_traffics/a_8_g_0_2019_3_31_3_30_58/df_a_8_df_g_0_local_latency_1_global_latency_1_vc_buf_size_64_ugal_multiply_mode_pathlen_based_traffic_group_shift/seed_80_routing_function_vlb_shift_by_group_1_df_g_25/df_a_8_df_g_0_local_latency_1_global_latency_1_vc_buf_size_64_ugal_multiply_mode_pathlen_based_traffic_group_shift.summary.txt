wait_for_tail_credit = 0;
injection_rate_uses_flits = 1;
sim_count = 1;
vc_alloc_delay = 1;
input_speedup = 1;
sw_allocator = separable_input_first;
internal_speedup = 4.0;
st_final_delay = 1;
num_vcs = 8;
warmup_periods = 3;
packet_size = 1;
sample_period = 10000;
alloc_iters = 1;
output_speedup = 1;
routing_delay = 0;
vc_allocator = separable_input_first;
priority = none;
sw_alloc_delay = 1;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = vlb;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35055,   0.40577,    1409.5,    1137.1,         1,         0.0
simulation,         2,      0.25,    0.2502,    0.2502,    18.401,    18.402,         0,      6.5005
simulation,         3,      0.37,   0.35349,   0.36528,    970.05,    916.61,         1,         0.0
simulation,         4,      0.31,   0.31022,   0.31021,    22.426,    22.427,         0,      6.5001
simulation,         5,      0.34,   0.34024,   0.34024,     31.27,    31.276,         0,      6.4998
simulation,         6,      0.35,   0.35021,   0.35019,    46.197,    46.201,         0,      6.5001
simulation,         7,      0.36,   0.35505,   0.35933,    545.71,    530.37,         1,         0.0
simulation,         8,       0.1,    0.1001,    0.1001,    15.706,    15.706,         0,      6.5002
simulation,         9,       0.2,   0.20016,   0.20016,    17.067,    17.067,         0,      6.4999
simulation,        10,       0.3,   0.30021,    0.3002,    21.303,    21.304,         0,         6.5
