wait_for_tail_credit = 0;
injection_rate_uses_flits = 1;
sim_count = 1;
vc_alloc_delay = 1;
input_speedup = 1;
sw_allocator = separable_input_first;
internal_speedup = 4.0;
st_final_delay = 1;
num_vcs = 8;
warmup_periods = 3;
packet_size = 1;
sample_period = 10000;
alloc_iters = 1;
output_speedup = 1;
routing_delay = 0;
vc_allocator = separable_input_first;
priority = none;
sw_alloc_delay = 1;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = vlb_restricted_src_and_dst;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.36602,   0.40327,     838.9,     653.6,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24998,    14.437,    14.437,         0,       5.226
simulation,         3,      0.37,   0.37018,   0.37017,    24.392,    24.436,         0,      5.2266
simulation,         4,      0.43,   0.35949,   0.38439,    354.48,    333.76,         1,         0.0
simulation,         5,       0.4,   0.39146,   0.39044,    594.68,    368.73,         1,         0.0
simulation,         6,      0.38,   0.37807,   0.37829,    366.61,    120.51,         0,      5.2264
simulation,         7,      0.39,   0.38311,   0.38504,    479.69,    244.34,         1,         0.0
simulation,         8,       0.1,   0.10003,   0.10003,    12.923,    12.923,         0,      5.2267
simulation,         9,       0.2,   0.19998,   0.19998,    13.739,    13.739,         0,      5.2263
simulation,        10,       0.3,   0.30012,   0.30011,    15.621,     15.62,         0,      5.2264
