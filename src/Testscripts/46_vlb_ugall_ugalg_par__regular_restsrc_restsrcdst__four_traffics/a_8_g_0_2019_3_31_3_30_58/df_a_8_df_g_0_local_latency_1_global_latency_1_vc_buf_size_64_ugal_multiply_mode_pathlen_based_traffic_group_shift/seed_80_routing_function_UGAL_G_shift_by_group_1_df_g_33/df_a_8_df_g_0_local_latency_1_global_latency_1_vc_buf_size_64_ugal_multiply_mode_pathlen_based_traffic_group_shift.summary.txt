wait_for_tail_credit = 0;
injection_rate_uses_flits = 1;
sim_count = 1;
vc_alloc_delay = 1;
input_speedup = 1;
sw_allocator = separable_input_first;
internal_speedup = 4.0;
st_final_delay = 1;
num_vcs = 8;
warmup_periods = 3;
packet_size = 1;
sample_period = 10000;
alloc_iters = 1;
output_speedup = 1;
routing_delay = 0;
vc_allocator = separable_input_first;
priority = none;
sw_alloc_delay = 1;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = UGAL_G;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49996,   0.49998,     53.94,     53.94,         0,      6.2823
simulation,         2,      0.74,   0.39326,   0.44085,    1828.3,    940.07,         1,         0.0
simulation,         3,      0.62,   0.41201,   0.45792,    1252.0,    779.95,         1,         0.0
simulation,         4,      0.56,   0.43625,   0.47674,     747.4,    565.66,         1,         0.0
simulation,         5,      0.53,   0.45669,   0.48978,    414.43,    364.13,         1,         0.0
simulation,         6,      0.51,   0.48439,   0.48527,    723.59,    225.28,         1,         0.0
simulation,         7,       0.1,  0.099976,  0.099975,    14.703,    14.703,         0,      5.7441
simulation,         8,       0.2,   0.20002,   0.20003,    16.407,    16.407,         0,      5.9856
simulation,         9,       0.3,   0.29997,   0.29997,    18.807,    18.808,         0,      6.1405
simulation,        10,       0.4,   0.40001,   0.40001,    23.281,    23.281,         0,       6.222
simulation,        11,       0.5,   0.49996,   0.49998,     53.94,     53.94,         0,      6.2823
