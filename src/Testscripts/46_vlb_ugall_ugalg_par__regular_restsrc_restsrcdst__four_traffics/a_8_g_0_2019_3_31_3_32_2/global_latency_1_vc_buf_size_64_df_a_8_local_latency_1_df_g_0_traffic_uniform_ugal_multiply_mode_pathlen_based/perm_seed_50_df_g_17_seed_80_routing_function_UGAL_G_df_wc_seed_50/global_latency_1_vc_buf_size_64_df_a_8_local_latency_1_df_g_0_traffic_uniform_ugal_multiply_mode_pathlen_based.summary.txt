internal_speedup = 4.0;
wait_for_tail_credit = 0;
num_vcs = 8;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
injection_rate_uses_flits = 1;
output_speedup = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
vc_alloc_delay = 1;
credit_delay = 2;
warmup_periods = 3;
sample_period = 10000;
packet_size = 1;
priority = none;
sw_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_G;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50025,   0.50025,    11.162,    11.162,         0,      3.7414
simulation,         2,      0.74,   0.74012,   0.74012,    13.704,    13.704,         0,      3.6938
simulation,         3,      0.86,   0.85997,   0.85998,    17.694,    17.695,         0,      3.6845
simulation,         4,      0.92,    0.9199,    0.9199,     23.06,    23.061,         0,      3.6838
simulation,         5,      0.95,   0.94995,   0.94994,    29.317,    29.319,         0,       3.684
simulation,         6,      0.97,   0.96993,   0.96996,    38.535,    38.547,         0,      3.6841
simulation,         7,      0.98,   0.97992,   0.97995,    49.368,    49.388,         0,      3.6837
simulation,         8,       0.1,   0.10017,   0.10017,    10.277,    10.277,         0,      4.0266
simulation,         9,       0.2,    0.2002,    0.2002,    10.286,    10.286,         0,      3.9066
simulation,        10,       0.3,   0.30023,   0.30023,    10.431,    10.431,         0,       3.829
simulation,        11,       0.4,   0.40026,   0.40026,    10.716,    10.716,         0,      3.7776
simulation,        12,       0.5,   0.50025,   0.50025,    11.162,    11.162,         0,      3.7414
simulation,        13,       0.6,   0.60016,   0.60015,    11.858,    11.858,         0,      3.7155
simulation,        14,       0.7,   0.70013,   0.70014,    13.019,     13.02,         0,      3.6988
simulation,        15,       0.8,   0.79999,   0.79999,    15.171,    15.171,         0,      3.6878
simulation,        16,       0.9,    0.8999,   0.89992,    20.677,    20.678,         0,      3.6838
