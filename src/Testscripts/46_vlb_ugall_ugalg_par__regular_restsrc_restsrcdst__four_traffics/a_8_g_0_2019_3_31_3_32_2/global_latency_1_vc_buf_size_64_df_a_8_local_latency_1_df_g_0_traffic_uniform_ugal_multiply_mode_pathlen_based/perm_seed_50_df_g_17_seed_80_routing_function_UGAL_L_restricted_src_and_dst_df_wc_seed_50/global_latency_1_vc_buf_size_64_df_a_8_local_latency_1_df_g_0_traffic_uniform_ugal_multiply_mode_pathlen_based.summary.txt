internal_speedup = 4.0;
wait_for_tail_credit = 0;
num_vcs = 8;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
injection_rate_uses_flits = 1;
output_speedup = 1;
st_final_delay = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
vc_alloc_delay = 1;
credit_delay = 2;
warmup_periods = 3;
sample_period = 10000;
packet_size = 1;
priority = none;
sw_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50025,   0.50025,    12.017,    12.017,         0,      3.9809
simulation,         2,      0.74,   0.74012,   0.74012,    15.076,    15.076,         0,      3.8941
simulation,         3,      0.86,   0.85997,   0.85998,     20.25,     20.25,         0,      3.8567
simulation,         4,      0.92,   0.91989,    0.9199,    28.941,    28.944,         0,      3.8414
simulation,         5,      0.95,   0.94996,   0.94999,    60.763,    60.794,         0,      3.8582
simulation,         6,      0.97,   0.96995,   0.96996,    83.166,    83.189,         0,      3.8093
simulation,         7,      0.98,   0.97974,   0.97995,    100.47,    100.51,         0,      3.7786
simulation,         8,       0.1,   0.10017,   0.10017,    10.484,    10.485,         0,      4.1234
simulation,         9,       0.2,    0.2002,    0.2002,    10.848,    10.848,         0,      4.1463
simulation,        10,       0.3,   0.30023,   0.30023,    11.131,    11.131,         0,      4.0952
simulation,        11,       0.4,   0.40026,   0.40026,    11.487,    11.487,         0,      4.0339
simulation,        12,       0.5,   0.50025,   0.50025,    12.017,    12.017,         0,      3.9809
simulation,        13,       0.6,   0.60015,   0.60015,    12.846,    12.847,         0,      3.9401
simulation,        14,       0.7,   0.70013,   0.70014,    14.241,    14.241,         0,      3.9073
simulation,        15,       0.8,       0.8,   0.79999,    16.924,    16.925,         0,      3.8756
simulation,        16,       0.9,   0.89993,   0.89992,    24.785,    24.787,         0,      3.8452
