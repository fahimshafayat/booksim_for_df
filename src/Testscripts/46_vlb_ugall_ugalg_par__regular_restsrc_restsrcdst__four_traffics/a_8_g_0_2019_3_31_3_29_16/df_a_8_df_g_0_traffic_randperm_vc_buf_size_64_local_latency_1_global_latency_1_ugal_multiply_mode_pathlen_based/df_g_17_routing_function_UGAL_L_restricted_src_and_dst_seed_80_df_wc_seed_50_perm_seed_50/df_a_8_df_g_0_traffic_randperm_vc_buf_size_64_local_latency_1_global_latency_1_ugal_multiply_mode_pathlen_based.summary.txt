wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50025,   0.50025,    17.246,    17.254,         0,       4.205
simulation,         2,      0.74,   0.66696,   0.68004,    733.11,    338.67,         1,         0.0
simulation,         3,      0.62,   0.62007,    0.6201,    38.853,    38.895,         0,      4.3057
simulation,         4,      0.68,    0.6585,   0.65881,    606.71,    204.69,         1,         0.0
simulation,         5,      0.65,   0.64576,   0.64578,     375.7,    100.93,         1,         0.0
simulation,         6,      0.63,    0.6299,   0.62992,     52.58,    47.918,         0,      4.3282
simulation,         7,      0.64,   0.63865,    0.6387,    205.73,    72.362,         0,      4.3552
simulation,         8,       0.1,   0.10017,   0.10017,    10.726,    10.726,         0,      4.2444
simulation,         9,       0.2,    0.2002,    0.2002,    11.129,    11.129,         0,      4.2773
simulation,        10,       0.3,   0.30023,   0.30023,      11.5,    11.501,         0,      4.2419
simulation,        11,       0.4,   0.40026,   0.40026,    12.468,    12.469,         0,       4.208
simulation,        12,       0.5,   0.50025,   0.50025,    17.246,    17.254,         0,       4.205
simulation,        13,       0.6,   0.60017,   0.60015,    31.347,    31.377,         0,      4.2712
