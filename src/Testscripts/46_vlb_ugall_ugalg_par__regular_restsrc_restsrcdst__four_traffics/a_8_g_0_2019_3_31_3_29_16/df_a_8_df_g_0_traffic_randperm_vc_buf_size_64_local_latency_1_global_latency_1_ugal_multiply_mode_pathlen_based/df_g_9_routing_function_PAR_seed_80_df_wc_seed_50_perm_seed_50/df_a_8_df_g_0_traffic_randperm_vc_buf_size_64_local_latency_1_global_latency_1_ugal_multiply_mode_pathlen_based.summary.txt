wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 50;
perm_seed = 50;
routing_function = PAR;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50015,     12.66,    12.661,         0,      4.1608
simulation,         2,      0.74,   0.74007,   0.74012,    24.733,    24.738,         0,      4.2464
simulation,         3,      0.86,   0.76124,   0.80029,    476.18,    358.21,         1,         0.0
simulation,         4,       0.8,    0.7633,   0.76413,    848.43,     330.0,         1,         0.0
simulation,         5,      0.77,   0.76814,   0.76833,    213.28,    108.64,         0,       4.394
simulation,         6,      0.78,   0.76783,   0.76816,    512.65,    197.55,         1,         0.0
simulation,         7,       0.1,   0.10003,   0.10003,    11.681,    11.682,         0,      4.6851
simulation,         8,       0.2,   0.19998,   0.19998,    11.696,    11.697,         0,      4.5161
simulation,         9,       0.3,   0.30011,   0.30011,     11.77,     11.77,         0,      4.3533
simulation,        10,       0.4,   0.40003,   0.40002,    12.052,    12.053,         0,      4.2391
simulation,        11,       0.5,   0.50014,   0.50015,     12.66,    12.661,         0,      4.1608
simulation,        12,       0.6,   0.59993,   0.59993,    14.018,    14.019,         0,      4.1224
simulation,        13,       0.7,   0.69991,   0.69993,    18.276,    18.278,         0,      4.1651
