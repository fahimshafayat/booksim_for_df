wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 50;
perm_seed = 50;
routing_function = vlb_restricted_src_and_dst;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35137,   0.39406,    1005.0,    768.07,         1,         0.0
simulation,         2,      0.25,    0.2502,    0.2502,    15.247,    15.247,         0,      5.6393
simulation,         3,      0.37,    0.3571,   0.35467,    481.49,    231.87,         1,         0.0
simulation,         4,      0.31,   0.31021,   0.31021,    16.628,    16.629,         0,      5.6396
simulation,         5,      0.34,   0.34024,   0.34024,    18.061,    18.061,         0,      5.6396
simulation,         6,      0.35,    0.3502,   0.35019,    19.007,    19.008,         0,      5.6392
simulation,         7,      0.36,   0.35579,   0.35656,    321.73,    126.27,         1,         0.0
simulation,         8,       0.1,    0.1001,    0.1001,    13.767,    13.767,         0,      5.6391
simulation,         9,       0.2,   0.20016,   0.20016,    14.585,    14.585,         0,      5.6396
simulation,        10,       0.3,   0.30021,    0.3002,    16.317,    16.317,         0,      5.6392
