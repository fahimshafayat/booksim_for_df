wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 50;
perm_seed = 50;
routing_function = PAR;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50009,   0.50008,    28.522,    28.548,         0,      5.0238
simulation,         2,      0.74,   0.54605,   0.58983,    907.86,     621.9,         1,         0.0
simulation,         3,      0.62,   0.53953,   0.57024,    468.23,    383.96,         1,         0.0
simulation,         4,      0.56,   0.53683,   0.53775,    666.64,    272.64,         1,         0.0
simulation,         5,      0.53,   0.52945,   0.52942,    182.65,    69.277,         0,       5.261
simulation,         6,      0.54,   0.53297,   0.53304,    517.49,    145.42,         1,         0.0
simulation,         7,       0.1,    0.1001,    0.1001,    12.511,    12.511,         0,      5.0577
simulation,         8,       0.2,   0.20016,   0.20016,    12.696,    12.696,         0,      4.8993
simulation,         9,       0.3,   0.30021,    0.3002,    13.263,    13.264,         0,      4.7921
simulation,        10,       0.4,   0.40012,   0.40011,    16.957,    16.967,         0,       4.784
simulation,        11,       0.5,   0.50009,   0.50008,    28.522,    28.548,         0,      5.0238
