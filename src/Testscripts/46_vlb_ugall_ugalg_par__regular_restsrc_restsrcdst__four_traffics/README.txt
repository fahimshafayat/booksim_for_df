31 MAR 2019. Trying to scrap together some results for SC.

We'll consider: 

four traffics: df_wc, group_shift, randperm, uniform

Four base routings: vlb, ugal_l, ugal_g, par
    Each base routings have three variations: regular, restricted_src, restricted_src_and_dst
So, total 12 routings.

Four networks: a 8 g 9 17 25 33

Each traffic gets a server.
Each server runs 4 networks sequentially, each routing goes parallel.
So 4 rounds of 12 parallel process. 

seeds: 80-87, trafficseeds: 50-57
For shift, shift by [1,2,25%,50%]. Needs to use a spearate seed in each case.

First do it with only one pair of seeds. Then we'll run it for another 7 rounds (hopefully). 
So for now: seed 80, traffic seed 50, shift_by 1
