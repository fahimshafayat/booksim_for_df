wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 50;
perm_seed = 50;
routing_function = UGAL_G_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.40118,   0.43608,    710.42,    558.78,         1,         0.0
simulation,         2,      0.25,    0.2502,    0.2502,    15.185,    15.185,         0,      5.2469
simulation,         3,      0.37,   0.37022,   0.37021,     19.47,    19.471,         0,      5.3633
simulation,         4,      0.43,   0.40522,   0.40493,    966.65,    345.82,         1,         0.0
simulation,         5,       0.4,   0.40013,   0.40011,    26.804,    26.807,         0,      5.3878
simulation,         6,      0.41,   0.40475,   0.40469,    518.29,    117.06,         1,         0.0
simulation,         7,       0.1,    0.1001,    0.1001,    13.052,    13.052,         0,      5.0495
simulation,         8,       0.2,   0.20016,   0.20016,    14.301,    14.302,         0,      5.1772
simulation,         9,       0.3,   0.30021,    0.3002,     16.35,     16.35,         0,      5.3044
simulation,        10,       0.4,   0.40013,   0.40011,    26.804,    26.807,         0,      5.3878
