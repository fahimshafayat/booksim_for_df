injection_rate_uses_flits = 1;
sw_allocator = separable_input_first;
input_speedup = 1;
sample_period = 1000;
output_speedup = 1;
num_vcs = 7;
sw_alloc_delay = 1;
sim_count = 1;
internal_speedup = 4.0;
packet_size = 1;
vc_allocator = separable_input_first;
credit_delay = 2;
wait_for_tail_credit = 0;
alloc_iters = 1;
routing_delay = 0;
vc_buf_size = 64;
warmup_periods = 3;
st_final_delay = 1;
priority = none;
vc_alloc_delay = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L;
shift_by_group = 12;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010043,  0.010042,    11.002,    11.003,         0,  223798,  46571,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 1 , |3-> 297 , |4-> 4966 , |5-> 44703 , |6-> 173831 ,,  |1-> 0 , |2-> 0 , |3-> 62 , |4-> 1119 , |5-> 9628 , |6-> 35762 ,
simulation,         2,      0.99,   0.17453,   0.41052,    610.49,     606.6,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,   0.20831,   0.43975,    331.78,    331.76,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.25,   0.15556,   0.24963,    165.74,     713.5,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.13,   0.13006,   0.13012,    55.533,    56.591,         0,  1217827,  4093848,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 2 , |3-> 486 , |4-> 15037 , |5-> 210197 , |6-> 992105 ,,  |1-> 0 , |2-> 36 , |3-> 6402 , |4-> 102674 , |5-> 860665 , |6-> 3124071 ,
simulation,         6,      0.19,   0.16151,   0.18987,    417.71,    435.22,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.16,   0.15615,   0.16004,    186.72,    334.41,         0,  2634587,  10938559,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 564 , |4-> 25680 , |5-> 436404 , |6-> 2171939 ,,  |1-> 0 , |2-> 65 , |3-> 16594 , |4-> 275156 , |5-> 2300945 , |6-> 8345799 ,
simulation,         8,      0.17,   0.15995,   0.17001,    236.14,    241.47,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.15,   0.14897,   0.15008,     99.63,    141.11,         0,  2395149,  9555472,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 1 , |3-> 593 , |4-> 24842 , |5-> 400297 , |6-> 1969416 ,,  |1-> 0 , |2-> 75 , |3-> 14488 , |4-> 239665 , |5-> 2010621 , |6-> 7290623 ,
simulation,        10,       0.1,   0.10013,   0.10014,    48.758,    49.368,         0,  965007,  2287028,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 4 , |3-> 657 , |4-> 14793 , |5-> 175171 , |6-> 774382 ,,  |1-> 0 , |2-> 23 , |3-> 3502 , |4-> 57434 , |5-> 480836 , |6-> 1745233 ,
simulation,        11,      0.05,  0.050129,  0.050139,    33.038,    33.199,         0,  797825,  572747,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 4 , |3-> 895 , |4-> 16372 , |5-> 157635 , |6-> 622919 ,,  |1-> 0 , |2-> 7 , |3-> 862 , |4-> 14032 , |5-> 119591 , |6-> 438255 ,
