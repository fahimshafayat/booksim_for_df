vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 1000;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L_multi_tiered;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010003,  0.010006,    10.179,    10.179,         0,  141256,  20594,  |4-> 141256 , |5-> 0 , |6-> 0 ,,  |4-> 20594 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 360 , |3-> 11423 , |4-> 129473 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 49 , |3-> 1676 , |4-> 18869 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.23188,   0.47626,    569.66,    563.48,         1,  0,  0,  ,  ,  ,  
simulation,         3,       0.5,   0.21574,   0.28945,    959.09,    957.83,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.25,   0.22209,   0.24367,    430.23,     440.7,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.13,   0.12984,   0.12984,    47.298,    47.926,         0,  845319,  1311139,  |4-> 845283 , |5-> 36 , |6-> 0 ,,  |4-> 1175860 , |5-> 1562 , |6-> 133717 ,,  |1-> 0 , |2-> 2056 , |3-> 64464 , |4-> 778780 , |5-> 19 , |6-> 0 ,,  |1-> 0 , |2-> 3086 , |3-> 104948 , |4-> 1122671 , |5-> 62800 , |6-> 17634 ,
simulation,         6,      0.19,   0.18949,   0.18988,    60.444,    68.529,         0,  1780544,  4845796,  |4-> 1530060 , |5-> 211893 , |6-> 38591 ,,  |4-> 3673892 , |5-> 674506 , |6-> 497398 ,,  |1-> 0 , |2-> 3397 , |3-> 109198 , |4-> 1507872 , |5-> 152222 , |6-> 7855 ,,  |1-> 0 , |2-> 11104 , |3-> 369106 , |4-> 3865587 , |5-> 537872 , |6-> 62127 ,
simulation,         7,      0.22,   0.21094,    0.2194,    291.65,    378.95,         0,  2653101,  8235734,  |4-> 1211001 , |5-> 853107 , |6-> 588993 ,,  |4-> 3331758 , |5-> 2806941 , |6-> 2097035 ,,  |1-> 0 , |2-> 2677 , |3-> 93987 , |4-> 1537711 , |5-> 903539 , |6-> 115187 ,,  |1-> 0 , |2-> 16267 , |3-> 506055 , |4-> 5315397 , |5-> 2175324 , |6-> 222691 ,
simulation,         8,      0.23,   0.21543,   0.22811,    251.19,    257.41,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.2,     0.198,    0.1999,    100.05,    146.16,         0,  2616815,  7556351,  |4-> 1952370 , |5-> 488345 , |6-> 176100 ,,  |4-> 4960081 , |5-> 1570818 , |6-> 1025452 ,,  |1-> 0 , |2-> 4151 , |3-> 141728 , |4-> 2025643 , |5-> 407541 , |6-> 37752 ,,  |1-> 0 , |2-> 16961 , |3-> 544721 , |4-> 5694648 , |5-> 1178546 , |6-> 121475 ,
simulation,        10,      0.15,    0.1499,   0.14989,    48.395,    49.006,         0,  988920,  1922356,  |4-> 986954 , |5-> 1966 , |6-> 0 ,,  |4-> 1731894 , |5-> 9770 , |6-> 180692 ,,  |1-> 0 , |2-> 2428 , |3-> 72648 , |4-> 912704 , |5-> 1140 , |6-> 0 ,,  |1-> 0 , |2-> 4623 , |3-> 155236 , |4-> 1649955 , |5-> 89083 , |6-> 23459 ,
simulation,        11,       0.1,  0.099898,  0.099889,    45.452,    45.821,         0,  836980,  812518,  |4-> 836980 , |5-> 0 , |6-> 0 ,,  |4-> 709738 , |5-> 2879 , |6-> 99901 ,,  |1-> 0 , |2-> 2097 , |3-> 66491 , |4-> 768392 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 1820 , |3-> 62659 , |4-> 686981 , |5-> 47967 , |6-> 13091 ,
simulation,        12,      0.05,  0.049978,  0.049976,    11.196,    11.197,         0,  541977,  267810,  |4-> 541977 , |5-> 0 , |6-> 0 ,,  |4-> 267431 , |5-> 379 , |6-> 0 ,,  |1-> 0 , |2-> 1424 , |3-> 43695 , |4-> 496858 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 664 , |3-> 22334 , |4-> 244605 , |5-> 207 , |6-> 0 ,
