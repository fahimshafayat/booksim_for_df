routing_delay = 0;
num_vcs = 7;
alloc_iters = 1;
st_final_delay = 1;
wait_for_tail_credit = 0;
input_speedup = 1;
credit_delay = 2;
sample_period = 1000;
warmup_periods = 3;
internal_speedup = 4.0;
sw_alloc_delay = 1;
vc_buf_size = 64;
packet_size = 1;
sw_allocator = separable_input_first;
sim_count = 1;
vc_allocator = separable_input_first;
vc_alloc_delay = 1;
priority = none;
output_speedup = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 33;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
routing_function = UGAL_L;
seed = 18;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010029,  0.010023,    9.6857,    9.6866,         0,  60066,  3265,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 1656 , |3-> 370 , |4-> 4073 , |5-> 19895 , |6-> 34072 ,,  |1-> 0 , |2-> 104 , |3-> 17 , |4-> 209 , |5-> 1138 , |6-> 1797 ,
simulation,         2,      0.99,   0.98183,      0.99,    134.59,    135.54,         0,  7364282,  368778,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 200392 , |3-> 38715 , |4-> 468729 , |5-> 2377606 , |6-> 4278840 ,,  |1-> 0 , |2-> 6455 , |3-> 7571 , |4-> 63221 , |5-> 178685 , |6-> 112846 ,
