wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
shift_offset = 1;
shift_percentage = 75;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_G;
seed = 80;
traffic = pe_based_shift_uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50015,   0.50015,    20.863,    20.864,         0,      5.0387
simulation,         2,      0.74,   0.59155,   0.62596,    796.39,    481.61,         1,         0.0
simulation,         3,      0.62,   0.61984,    0.6199,     43.66,    43.662,         0,      5.1706
simulation,         4,      0.68,    0.5982,   0.62464,    462.73,    333.32,         1,         0.0
simulation,         5,      0.65,   0.60486,   0.61402,    535.05,    266.67,         1,         0.0
simulation,         6,      0.63,   0.61183,   0.61131,    623.07,    216.61,         1,         0.0
simulation,         7,      0.15,   0.15006,   0.15006,     11.95,     11.95,         0,      4.6106
simulation,         8,       0.3,   0.30012,   0.30011,    13.739,    13.739,         0,      4.6826
simulation,         9,      0.45,   0.45009,   0.45009,    18.431,    18.431,         0,      4.9665
simulation,        10,       0.6,    0.5999,   0.59993,    33.184,    33.185,         0,      5.1527
