wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
shift_offset = 1;
shift_percentage = 75;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_L_restricted_src_only;
seed = 80;
traffic = router_based_shift_uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49835,   0.49835,    259.33,    70.189,         0,      4.4567
simulation,         2,      0.74,   0.58175,   0.61465,    763.87,    443.41,         1,         0.0
simulation,         3,      0.62,   0.56043,   0.57045,    713.46,    305.74,         1,         0.0
simulation,         4,      0.56,   0.53815,    0.5384,    733.37,    251.89,         1,         0.0
simulation,         5,      0.53,   0.52296,   0.52309,    536.91,    139.41,         1,         0.0
simulation,         6,      0.51,    0.5077,   0.50775,    375.26,    82.266,         0,      4.4615
simulation,         7,      0.52,   0.51604,   0.51617,    429.09,    103.81,         1,         0.0
simulation,         8,       0.1,   0.10003,   0.10003,    10.707,    10.707,         0,      4.1766
simulation,         9,       0.2,   0.19998,   0.19998,    11.574,    11.574,         0,       4.225
simulation,        10,       0.3,   0.30012,   0.30011,    26.153,    26.175,         0,      4.2905
simulation,        11,       0.4,   0.40002,   0.40002,    35.638,    35.677,         0,      4.3952
simulation,        12,       0.5,   0.49835,   0.49835,    259.33,    70.189,         0,      4.4567
