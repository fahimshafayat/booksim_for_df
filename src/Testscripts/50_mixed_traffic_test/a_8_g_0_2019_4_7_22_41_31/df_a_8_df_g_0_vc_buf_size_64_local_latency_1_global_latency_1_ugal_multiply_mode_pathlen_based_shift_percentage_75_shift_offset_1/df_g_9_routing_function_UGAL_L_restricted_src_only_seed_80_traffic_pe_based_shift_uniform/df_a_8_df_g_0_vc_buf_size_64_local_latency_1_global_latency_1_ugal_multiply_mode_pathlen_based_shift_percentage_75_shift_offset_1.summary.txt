wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
shift_offset = 1;
shift_percentage = 75;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_L_restricted_src_only;
seed = 80;
traffic = pe_based_shift_uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49896,   0.49897,    201.84,    72.969,         0,      4.4601
simulation,         2,      0.74,   0.59232,   0.62119,    654.93,    397.07,         1,         0.0
simulation,         3,      0.62,   0.56105,   0.57081,    698.28,    303.05,         1,         0.0
simulation,         4,      0.56,   0.53777,   0.53808,    796.78,    225.51,         1,         0.0
simulation,         5,      0.53,    0.5208,   0.52114,    497.52,    176.85,         1,         0.0
simulation,         6,      0.51,   0.50771,   0.50778,    366.62,    96.912,         0,      4.4653
simulation,         7,      0.52,   0.51574,   0.51581,     488.5,    120.77,         1,         0.0
simulation,         8,       0.1,   0.10003,   0.10003,    10.699,    10.699,         0,      4.1747
simulation,         9,       0.2,   0.19998,   0.19998,    11.435,    11.435,         0,      4.2218
simulation,        10,       0.3,    0.3001,   0.30011,    29.464,    29.488,         0,      4.2898
simulation,        11,       0.4,   0.40003,   0.40002,    37.466,    37.506,         0,      4.3915
simulation,        12,       0.5,   0.49896,   0.49897,    201.84,    72.969,         0,      4.4601
