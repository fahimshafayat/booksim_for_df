wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
shift_offset = 1;
shift_percentage = 75;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_L;
seed = 80;
traffic = pe_based_shift_uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.43909,     0.466,    489.96,    405.89,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24998,    29.084,    29.101,         0,      4.6338
simulation,         3,      0.37,   0.36647,   0.36665,    524.27,    263.31,         1,         0.0
simulation,         4,      0.31,   0.31019,   0.31018,    46.016,    46.064,         0,      4.8417
simulation,         5,      0.34,   0.34012,   0.34016,    51.929,     52.01,         0,      4.9315
simulation,         6,      0.35,   0.34963,   0.35007,    165.39,    160.11,         0,      4.9581
simulation,         7,      0.36,    0.3582,   0.35846,    435.72,    251.95,         0,      4.9806
simulation,         8,       0.1,   0.10003,   0.10003,    11.513,    11.513,         0,      4.5438
simulation,         9,       0.2,   0.19998,   0.19998,    12.709,    12.709,         0,       4.549
simulation,        10,       0.3,   0.30011,   0.30011,    44.776,    44.814,         0,      4.8083
