wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
shift_offset = 1;
shift_percentage = 25;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_L;
seed = 80;
traffic = pe_based_shift_uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50023,   0.50025,    25.916,    25.971,         0,      4.4237
simulation,         2,      0.74,   0.62168,   0.65215,    514.13,    387.39,         1,         0.0
simulation,         3,      0.62,   0.56342,   0.57533,    479.47,    319.87,         1,         0.0
simulation,         4,      0.56,   0.55918,   0.55922,    152.01,    51.901,         0,      4.4477
simulation,         5,      0.59,   0.52101,    0.5269,    812.07,     464.6,         1,         0.0
simulation,         6,      0.57,   0.56848,   0.56866,    247.46,    91.649,         0,      4.4534
simulation,         7,      0.58,   0.53373,   0.53843,    518.97,    326.98,         1,         0.0
simulation,         8,       0.1,   0.10017,   0.10017,    11.111,    11.111,         0,      4.3986
simulation,         9,       0.2,    0.2002,    0.2002,    11.561,    11.561,         0,      4.3667
simulation,        10,       0.3,   0.30023,   0.30023,    16.643,    16.651,         0,      4.3494
simulation,        11,       0.4,   0.40025,   0.40026,    21.885,    21.902,         0,      4.3817
simulation,        12,       0.5,   0.50023,   0.50025,    25.916,    25.971,         0,      4.4237
