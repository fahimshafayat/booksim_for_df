wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
shift_offset = 1;
shift_percentage = 25;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = PAR;
seed = 80;
traffic = router_based_shift_uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50024,   0.50025,    16.971,    16.973,         0,      4.5947
simulation,         2,      0.74,   0.61744,   0.65231,    566.97,    439.51,         1,         0.0
simulation,         3,      0.62,   0.57619,   0.59004,    473.32,    330.69,         1,         0.0
simulation,         4,      0.56,   0.56018,   0.56021,    26.422,    26.445,         0,      4.5976
simulation,         5,      0.59,   0.54681,   0.55365,    607.03,    386.33,         1,         0.0
simulation,         6,      0.57,   0.56999,    0.5701,    62.765,    62.898,         0,      4.6011
simulation,         7,      0.58,   0.55275,   0.55554,    656.38,     346.9,         1,         0.0
simulation,         8,       0.1,   0.10017,   0.10017,    12.363,    12.364,         0,      4.9787
simulation,         9,       0.2,    0.2002,    0.2002,    12.533,    12.533,         0,      4.8051
simulation,        10,       0.3,   0.30023,   0.30023,    13.029,    13.029,         0,      4.6759
simulation,        11,       0.4,   0.40026,   0.40026,    14.219,     14.22,         0,      4.6155
simulation,        12,       0.5,   0.50024,   0.50025,    16.971,    16.973,         0,      4.5947
