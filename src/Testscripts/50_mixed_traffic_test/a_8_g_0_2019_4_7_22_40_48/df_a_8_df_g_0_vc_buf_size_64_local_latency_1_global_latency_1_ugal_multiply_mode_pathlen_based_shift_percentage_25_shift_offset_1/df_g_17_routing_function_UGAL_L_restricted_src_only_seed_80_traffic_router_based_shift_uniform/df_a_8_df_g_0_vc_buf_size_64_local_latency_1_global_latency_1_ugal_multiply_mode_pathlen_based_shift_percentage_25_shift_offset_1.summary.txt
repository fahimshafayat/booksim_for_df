wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
shift_offset = 1;
shift_percentage = 25;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_L_restricted_src_only;
seed = 80;
traffic = router_based_shift_uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50024,   0.50025,     19.85,    19.862,         0,      4.1834
simulation,         2,      0.74,   0.61709,   0.64832,    443.67,    318.61,         1,         0.0
simulation,         3,      0.62,   0.55381,   0.56969,    508.89,    332.22,         1,         0.0
simulation,         4,      0.56,   0.56018,   0.56021,    29.437,    29.473,         0,      4.1882
simulation,         5,      0.59,   0.53593,   0.53707,    784.58,    405.36,         1,         0.0
simulation,         6,      0.57,   0.56995,    0.5701,    66.364,    66.539,         0,      4.1916
simulation,         7,      0.58,   0.53872,    0.5395,    569.86,    298.31,         1,         0.0
simulation,         8,       0.1,   0.10017,   0.10017,    10.629,    10.629,         0,      4.1723
simulation,         9,       0.2,    0.2002,    0.2002,    11.123,    11.123,         0,       4.204
simulation,        10,       0.3,   0.30023,   0.30023,    14.001,    14.005,         0,      4.1872
simulation,        11,       0.4,   0.40026,   0.40026,    18.312,    18.325,         0,      4.1845
simulation,        12,       0.5,   0.50024,   0.50025,     19.85,    19.862,         0,      4.1834
