wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
shift_offset = 1;
shift_percentage = 25;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = PAR;
seed = 80;
traffic = pe_based_shift_uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50025,   0.50025,    16.195,    16.196,         0,      4.5587
simulation,         2,      0.74,   0.60038,    0.6477,    628.27,    478.97,         1,         0.0
simulation,         3,      0.62,   0.57349,   0.58983,    500.85,    373.18,         1,         0.0
simulation,         4,      0.56,   0.56022,   0.56021,    23.966,    23.976,         0,      4.5572
simulation,         5,      0.59,   0.54712,   0.55527,    633.28,    456.25,         1,         0.0
simulation,         6,      0.57,   0.56987,   0.57004,    62.938,    63.105,         0,       4.579
simulation,         7,      0.58,   0.55087,   0.55703,    570.62,    393.25,         1,         0.0
simulation,         8,       0.1,   0.10017,   0.10017,    12.305,    12.305,         0,      4.9542
simulation,         9,       0.2,    0.2002,    0.2002,    12.446,    12.446,         0,      4.7774
simulation,        10,       0.3,   0.30023,   0.30023,    12.853,    12.854,         0,      4.6396
simulation,        11,       0.4,   0.40026,   0.40026,    13.884,    13.884,         0,      4.5739
simulation,        12,       0.5,   0.50025,   0.50025,    16.195,    16.196,         0,      4.5587
