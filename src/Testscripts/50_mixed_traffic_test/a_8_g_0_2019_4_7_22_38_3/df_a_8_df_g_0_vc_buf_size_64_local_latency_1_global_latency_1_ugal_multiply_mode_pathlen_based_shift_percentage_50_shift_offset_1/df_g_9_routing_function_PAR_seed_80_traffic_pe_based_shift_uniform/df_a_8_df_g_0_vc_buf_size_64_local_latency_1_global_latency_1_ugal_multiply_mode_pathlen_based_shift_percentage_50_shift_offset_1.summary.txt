wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
shift_offset = 1;
shift_percentage = 50;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = PAR;
seed = 80;
traffic = pe_based_shift_uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50015,    18.761,    18.762,         0,      4.8125
simulation,         2,      0.74,   0.59952,   0.64609,    649.52,    485.13,         1,         0.0
simulation,         3,      0.62,    0.6169,   0.61706,    417.95,    127.58,         0,      4.9923
simulation,         4,      0.68,    0.6259,   0.64191,     663.8,    382.99,         1,         0.0
simulation,         5,      0.65,   0.62594,    0.6269,     710.2,    253.48,         1,         0.0
simulation,         6,      0.63,   0.62104,   0.62184,    508.63,    167.56,         1,         0.0
simulation,         7,      0.15,   0.15006,   0.15006,    12.421,    12.422,         0,      4.8857
simulation,         8,       0.3,   0.30011,   0.30011,     13.13,    13.131,         0,      4.6968
simulation,         9,      0.45,   0.45009,   0.45009,     16.43,     16.43,         0,      4.7528
simulation,        10,       0.6,   0.59991,   0.59993,    38.239,    38.256,         0,      4.9317
