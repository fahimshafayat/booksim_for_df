wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
shift_offset = 1;
shift_percentage = 50;
topology = dragonflyfull;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_G_restricted_src_only;
seed = 80;
traffic = pe_based_shift_uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50025,   0.50025,    14.883,    14.884,         0,      4.2847
simulation,         2,      0.74,   0.59482,   0.64168,    553.67,    446.01,         1,         0.0
simulation,         3,      0.62,   0.62009,    0.6201,    21.248,    21.249,         0,      4.3123
simulation,         4,      0.68,   0.60222,   0.61368,    629.82,    487.04,         1,         0.0
simulation,         5,      0.65,   0.65007,   0.65009,    29.939,    29.944,         0,      4.3215
simulation,         6,      0.66,   0.66015,   0.66016,    44.928,    44.964,         0,      4.3245
simulation,         7,      0.67,    0.6336,   0.63877,    581.45,     342.5,         1,         0.0
simulation,         8,      0.15,   0.15021,   0.15021,    11.094,    11.094,         0,      4.2431
simulation,         9,       0.3,   0.30022,   0.30023,    12.052,    12.052,         0,      4.2389
simulation,        10,      0.45,   0.45022,   0.45022,    13.889,    13.889,         0,      4.2719
simulation,        11,       0.6,   0.60015,   0.60015,    19.121,    19.122,         0,      4.3076
