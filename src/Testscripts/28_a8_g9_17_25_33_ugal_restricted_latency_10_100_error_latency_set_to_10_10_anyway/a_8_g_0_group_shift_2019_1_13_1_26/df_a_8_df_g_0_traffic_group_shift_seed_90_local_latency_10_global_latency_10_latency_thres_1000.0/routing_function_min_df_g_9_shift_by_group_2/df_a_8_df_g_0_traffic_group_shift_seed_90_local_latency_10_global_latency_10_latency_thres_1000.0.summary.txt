vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 9;
routing_function = min;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.12463,   0.17427,    3761.3,    2932.2,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.12459,   0.17393,    2507.6,    2391.4,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12499,   0.12993,    1016.7,    1016.7,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.069964,  0.069959,    34.931,    34.932,         0,  0,  0,  ,  ,  ,  
simulation,         5,       0.1,  0.099935,  0.099933,    36.306,    36.306,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.11,   0.10991,   0.10991,    37.925,    37.926,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.12,   0.11985,   0.11985,    46.026,     46.02,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.05,  0.049935,   0.04993,     34.61,     34.61,         0,  0,  0,  ,  ,  ,  
