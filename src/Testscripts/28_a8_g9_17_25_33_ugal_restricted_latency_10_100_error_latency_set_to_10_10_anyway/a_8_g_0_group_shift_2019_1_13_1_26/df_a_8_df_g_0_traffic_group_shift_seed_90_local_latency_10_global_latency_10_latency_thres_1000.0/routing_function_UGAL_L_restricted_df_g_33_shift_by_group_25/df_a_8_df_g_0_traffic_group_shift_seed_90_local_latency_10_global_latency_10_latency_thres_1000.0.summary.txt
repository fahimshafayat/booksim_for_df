vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 33;
routing_function = UGAL_L_restricted;
shift_by_group = 25;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.3433,   0.38101,    999.87,    697.49,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25001,   0.25001,    88.429,    84.985,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.35652,   0.35673,    953.72,    259.48,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30711,   0.30713,    442.15,    123.85,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27888,   0.27886,    449.36,    105.05,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.29,   0.28808,   0.28807,    716.68,    114.97,         0,  0,  0,  ,  ,  ,  
simulation,         7,       0.3,   0.29761,    0.2976,    897.99,     115.5,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20009,   0.20009,    73.604,    73.674,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.15,   0.15012,   0.15011,    71.088,     71.13,         0,  0,  0,  ,  ,  ,  
