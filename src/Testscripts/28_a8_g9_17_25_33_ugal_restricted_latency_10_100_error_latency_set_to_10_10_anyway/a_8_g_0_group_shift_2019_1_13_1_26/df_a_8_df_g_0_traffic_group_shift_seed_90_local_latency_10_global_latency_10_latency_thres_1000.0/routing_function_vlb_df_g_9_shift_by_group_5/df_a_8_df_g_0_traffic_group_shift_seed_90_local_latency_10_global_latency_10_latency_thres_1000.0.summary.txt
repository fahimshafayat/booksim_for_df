vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 9;
routing_function = vlb;
shift_by_group = 5;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.41945,   0.44526,    1478.9,    985.78,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24997,   0.24997,    67.506,    67.506,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36994,   0.36992,    75.199,    75.198,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42992,   0.42991,    155.06,    154.99,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.42568,   0.42854,    1762.3,    1141.7,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.42904,   0.43406,    1109.7,    865.71,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.4,   0.39987,   0.39984,    83.483,    83.475,         0,  0,  0,  ,  ,  ,  
simulation,         8,      0.35,    0.3499,   0.34989,    72.638,    72.638,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.29992,   0.29991,    69.249,    69.249,         0,  0,  0,  ,  ,  ,  
