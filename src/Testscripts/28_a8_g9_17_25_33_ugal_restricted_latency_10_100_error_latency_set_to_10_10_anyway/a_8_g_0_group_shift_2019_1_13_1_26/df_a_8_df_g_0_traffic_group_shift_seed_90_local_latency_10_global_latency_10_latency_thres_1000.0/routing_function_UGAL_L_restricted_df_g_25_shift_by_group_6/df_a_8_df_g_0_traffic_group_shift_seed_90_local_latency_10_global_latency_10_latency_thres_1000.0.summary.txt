vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 25;
routing_function = UGAL_L_restricted;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.33859,   0.37435,    1043.1,    719.49,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25007,   0.25008,    80.129,     80.24,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.34526,   0.34569,    1130.4,    407.82,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31007,   0.31007,    84.404,    84.582,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,    0.3382,   0.33834,    408.27,    134.93,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.35,   0.34145,   0.34165,    1021.0,    242.83,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.30005,   0.30006,    83.573,    83.742,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20009,   0.20009,    76.649,    76.719,         0,  0,  0,  ,  ,  ,  
