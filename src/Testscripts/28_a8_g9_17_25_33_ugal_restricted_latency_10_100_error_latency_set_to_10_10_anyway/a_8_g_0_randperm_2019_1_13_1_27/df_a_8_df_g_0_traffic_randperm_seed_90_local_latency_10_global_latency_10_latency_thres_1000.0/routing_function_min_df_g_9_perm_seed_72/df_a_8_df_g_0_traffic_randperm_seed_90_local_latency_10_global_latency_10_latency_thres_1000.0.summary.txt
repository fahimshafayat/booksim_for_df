vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 9;
perm_seed = 72;
routing_function = min;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49629,   0.49627,    864.78,    90.202,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.61391,   0.63021,    1257.7,    455.31,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.57779,   0.57786,    1303.5,    295.34,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,    0.5398,   0.53983,    1029.3,    185.91,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.51636,   0.51644,    975.99,    157.81,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.50202,   0.50208,    755.82,    144.33,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44846,   0.44848,    371.22,     56.81,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39917,   0.39917,    191.28,    41.604,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.34968,   0.34967,    66.165,    42.431,         0,  0,  0,  ,  ,  ,  
