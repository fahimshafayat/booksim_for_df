vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 17;
perm_seed = 72;
routing_function = UGAL_L;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49996,   0.49997,    46.407,    46.421,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.63316,   0.64892,    1051.7,    437.19,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.61547,   0.61546,    716.07,    116.33,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.68,   0.63238,   0.63276,    1262.2,    322.79,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.65,   0.62901,   0.62912,    976.75,    220.91,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.63,   0.62139,   0.62134,    750.96,    149.06,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.6,   0.59903,   0.59906,    202.73,    81.049,         0,  0,  0,  ,  ,  ,  
simulation,         8,      0.55,   0.54991,   0.54993,    54.897,    54.924,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,   0.44994,   0.44994,     41.91,    41.918,         0,  0,  0,  ,  ,  ,  
