vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 25;
perm_seed = 74;
routing_function = min;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.41756,   0.42918,    1069.8,    477.42,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24925,   0.24928,    373.47,    69.924,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.34833,   0.34845,    1056.0,    412.02,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30424,   0.30425,    811.91,    139.15,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27686,   0.27686,    568.53,    130.99,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,   0.25842,   0.25846,    332.19,    130.53,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.27,       0.0,       0.0,       0.0,       0.0,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.2,       0.0,       0.0,       0.0,       0.0,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.15,       0.0,       0.0,       0.0,       0.0,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.1,       0.0,       0.0,       0.0,       0.0,         0,  0,  0,  ,  ,  ,  
