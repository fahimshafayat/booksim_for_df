vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 17;
perm_seed = 74;
routing_function = min;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.45633,   0.45682,    1423.6,    358.08,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25005,    33.304,    33.306,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36606,   0.36602,    936.87,    119.51,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.41279,   0.41273,    894.82,    265.57,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.39203,    0.3922,    894.83,    175.29,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.38,    0.3756,   0.37562,    641.47,    118.42,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.35,    0.3483,   0.34829,    393.67,    108.32,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.30001,   0.30001,    33.686,    33.688,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20008,   0.20007,    33.105,    33.106,         0,  0,  0,  ,  ,  ,  
