vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 9;
perm_seed = 74;
routing_function = vlb;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.48549,    0.4857,    1060.8,    519.88,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24998,   0.24997,    62.378,    62.385,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36994,   0.36992,    65.831,    65.837,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42994,   0.42991,    70.921,    70.925,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45987,   0.45986,    78.642,    78.636,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,   0.47613,   0.47621,    677.12,    168.72,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.49,   0.48059,   0.48114,    992.45,    377.99,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44994,   0.44994,    74.857,    74.864,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39986,   0.39984,     67.72,    67.724,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,    0.3499,   0.34989,    64.916,    64.922,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.29992,   0.29991,    63.388,    63.394,         0,  0,  0,  ,  ,  ,  
