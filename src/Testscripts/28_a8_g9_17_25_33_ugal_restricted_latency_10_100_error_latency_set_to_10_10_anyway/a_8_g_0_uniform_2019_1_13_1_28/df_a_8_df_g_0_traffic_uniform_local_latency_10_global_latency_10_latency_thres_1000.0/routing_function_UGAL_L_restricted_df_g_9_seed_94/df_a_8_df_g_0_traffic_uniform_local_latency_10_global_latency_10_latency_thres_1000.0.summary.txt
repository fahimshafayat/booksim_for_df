vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
df_g = 9;
routing_function = UGAL_L_restricted;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50013,   0.50013,    34.025,    34.027,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.74021,   0.74021,    36.101,    36.103,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.86016,   0.86017,    39.541,    39.544,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.92006,   0.92007,    44.125,    44.127,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.95006,   0.95006,    49.395,    49.402,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,      0.97,   0.97002,    57.633,    57.653,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,      0.98,   0.98002,    66.974,     66.99,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.90014,   0.90014,    42.099,      42.1,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,   0.85015,   0.85015,    39.096,    39.098,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.80019,   0.80019,    37.366,    37.368,         0,  0,  0,  ,  ,  ,  
