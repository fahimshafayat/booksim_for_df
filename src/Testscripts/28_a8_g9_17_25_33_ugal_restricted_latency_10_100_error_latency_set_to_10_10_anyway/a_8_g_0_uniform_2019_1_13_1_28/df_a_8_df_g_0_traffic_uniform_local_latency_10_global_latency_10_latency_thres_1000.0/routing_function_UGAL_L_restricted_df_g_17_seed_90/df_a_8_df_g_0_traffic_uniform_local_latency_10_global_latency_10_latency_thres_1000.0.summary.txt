vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
df_g = 17;
routing_function = UGAL_L_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49997,   0.49997,    35.322,    35.324,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.73994,   0.73994,    37.664,    37.665,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.85986,   0.85987,    41.795,    41.797,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.91992,   0.91991,    47.574,    47.577,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.94993,   0.94994,    54.338,    54.344,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.96995,   0.96994,     64.58,    64.592,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.98007,   0.98001,    75.314,    75.341,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.89989,   0.89989,    45.008,    45.011,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,   0.84986,   0.84988,     41.22,    41.221,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.79983,   0.79983,    39.162,    39.163,         0,  0,  0,  ,  ,  ,  
