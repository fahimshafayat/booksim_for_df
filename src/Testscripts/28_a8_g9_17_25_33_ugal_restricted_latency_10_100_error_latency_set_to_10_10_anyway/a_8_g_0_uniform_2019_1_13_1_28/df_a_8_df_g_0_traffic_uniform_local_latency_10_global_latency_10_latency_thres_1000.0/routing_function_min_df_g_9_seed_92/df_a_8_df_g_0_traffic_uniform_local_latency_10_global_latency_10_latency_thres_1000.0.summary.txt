vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
df_g = 9;
routing_function = min;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50014,   0.50012,    33.402,    33.404,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.73998,   0.73997,    35.795,    35.798,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.86008,   0.86008,    39.421,    39.424,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.92013,    0.9201,    44.153,    44.157,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.95001,   0.95002,    49.853,    49.861,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.96998,   0.96996,    58.321,    58.331,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.97997,   0.97994,    67.221,    67.237,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.90006,   0.90007,    42.104,    42.107,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,      0.85,      0.85,    38.922,    38.925,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.80006,   0.80005,    37.134,    37.137,         0,  0,  0,  ,  ,  ,  
