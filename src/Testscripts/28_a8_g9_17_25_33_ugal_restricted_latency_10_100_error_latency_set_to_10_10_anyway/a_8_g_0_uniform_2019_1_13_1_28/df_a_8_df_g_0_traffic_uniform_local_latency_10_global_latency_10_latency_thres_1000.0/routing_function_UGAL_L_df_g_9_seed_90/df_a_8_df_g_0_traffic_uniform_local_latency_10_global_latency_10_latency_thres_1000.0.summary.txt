vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
df_g = 9;
routing_function = UGAL_L;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4998,   0.49978,    34.406,    34.408,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,    0.7403,   0.74028,     36.33,    36.332,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,    0.8601,    0.8601,    39.775,    39.779,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.92007,   0.92006,    44.527,    44.532,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.95002,   0.95001,    50.064,    50.069,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.96996,   0.96995,    58.382,    58.388,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.97996,   0.97997,    67.813,    67.834,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.90006,   0.90005,    42.461,    42.465,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,   0.85015,   0.85015,    39.329,    39.332,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.80025,   0.80024,    37.606,    37.609,         0,  0,  0,  ,  ,  ,  
