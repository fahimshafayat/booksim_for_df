vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
df_g = 25;
routing_function = min;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50005,   0.50006,    35.285,    35.286,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.74005,   0.74004,    44.124,     44.13,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.76567,   0.78753,    1091.3,    490.73,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.76572,   0.76759,    1055.7,    549.74,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.76466,   0.76681,    460.15,    328.63,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.78,   0.76433,   0.76593,    1069.9,    507.03,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.75,    0.7501,   0.75008,    47.286,    47.302,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.7,   0.70004,   0.70005,    39.582,    39.584,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.65,   0.65011,   0.65012,    37.554,    37.555,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.6,   0.60007,   0.60008,    36.481,    36.483,         0,  0,  0,  ,  ,  ,  
