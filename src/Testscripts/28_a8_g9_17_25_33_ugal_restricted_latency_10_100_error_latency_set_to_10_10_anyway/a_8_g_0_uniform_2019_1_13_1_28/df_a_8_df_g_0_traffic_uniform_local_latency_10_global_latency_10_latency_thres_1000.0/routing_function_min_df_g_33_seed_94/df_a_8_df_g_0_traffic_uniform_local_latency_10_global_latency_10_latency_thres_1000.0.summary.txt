vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
df_g = 33;
routing_function = min;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49999,       0.5,    35.248,    35.249,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.73999,   0.73999,    38.165,    38.166,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.85992,   0.85992,    43.046,    43.048,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.91997,   0.91997,    50.219,    50.223,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.95002,   0.95003,    59.174,    59.183,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.96996,   0.96999,    72.848,     72.87,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,      0.98,   0.97998,    87.678,    87.691,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.89997,   0.89997,    46.991,    46.993,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,   0.84994,   0.84994,     42.37,    42.372,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.79996,   0.79997,    39.916,    39.917,         0,  0,  0,  ,  ,  ,  
