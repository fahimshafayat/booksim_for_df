vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
df_g = 17;
routing_function = vlb;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49984,   0.49985,    129.11,    129.13,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.50405,   0.56482,    1680.8,    1038.8,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.50362,   0.56156,    1018.2,    874.66,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.50443,   0.53199,    1023.2,    815.34,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,    0.5076,   0.51505,    1113.6,    929.22,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.50888,   0.50983,    265.99,    267.98,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.52,    0.5065,    0.5119,    1120.5,    842.28,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.45,   0.44981,    0.4498,    78.263,    78.262,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.4,    0.3997,    0.3997,      70.5,    70.503,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.35,   0.34974,   0.34974,    67.345,    67.349,         0,  0,  0,  ,  ,  ,  
