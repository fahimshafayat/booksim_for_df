vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
latency_thres = 1000.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
df_g = 17;
routing_function = UGAL_L;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49986,   0.49985,     35.61,    35.613,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.73987,   0.73986,     37.83,    37.832,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.85995,   0.85996,    42.028,    42.031,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.91997,   0.91996,    47.987,     47.99,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,      0.95,   0.94998,    55.102,    55.108,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.97001,   0.96999,    65.998,    66.007,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.98002,   0.98001,    77.281,    77.307,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.89999,   0.90001,    45.344,    45.347,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,   0.84997,   0.84996,    41.436,    41.439,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.79992,   0.79991,    39.333,    39.336,         0,  0,  0,  ,  ,  ,  
