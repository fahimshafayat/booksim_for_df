wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_G;
shift_by_group = 2;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.36225,    0.3723,    1122.2,    253.21,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25005,    16.769,    16.769,         0,  2040108,  6125858,  ,  ,  ,  
simulation,         3,      0.37,   0.36133,   0.36136,    546.62,    52.772,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31001,   0.31001,    18.793,    18.793,         0,  2041531,  8082330,  ,  ,  ,  
simulation,         5,      0.34,   0.34005,   0.34005,    21.209,     21.21,         0,  2043157,  9068247,  ,  ,  ,  
simulation,         6,      0.35,   0.35002,   0.35001,     23.34,    23.343,         0,  2044426,  9399469,  ,  ,  ,  
simulation,         7,      0.36,   0.35712,   0.35711,    422.71,    38.517,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.30002,   0.30001,    18.376,    18.376,         0,  2041362,  7755337,  ,  ,  ,  
simulation,        11,       0.2,   0.20008,   0.20007,    15.437,    15.438,         0,  1990690,  4541617,  ,  ,  ,  
