wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_G;
shift_by_group = 1;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.37038,   0.38025,    1044.4,    240.69,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25005,    16.737,    16.737,         0,  2040067,  6125515,  ,  ,  ,  
simulation,         3,      0.37,   0.36671,   0.36672,    401.83,     44.67,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31001,   0.31001,    18.757,    18.757,         0,  2041593,  8082268,  ,  ,  ,  
simulation,         5,      0.34,   0.34005,   0.34005,    20.833,    20.834,         0,  2043026,  9067843,  ,  ,  ,  
simulation,         6,      0.35,   0.35002,   0.35001,    22.395,    22.396,         0,  2043838,  9396997,  ,  ,  ,  
simulation,         7,      0.36,   0.36003,   0.36001,     25.01,    25.017,         0,  2047933,  9742220,  ,  ,  ,  
simulation,         9,       0.3,   0.30001,   0.30001,    18.319,    18.319,         0,  2041467,  7755580,  ,  ,  ,  
simulation,        11,       0.2,   0.20008,   0.20007,    15.413,    15.413,         0,  1990465,  4541842,  ,  ,  ,  
