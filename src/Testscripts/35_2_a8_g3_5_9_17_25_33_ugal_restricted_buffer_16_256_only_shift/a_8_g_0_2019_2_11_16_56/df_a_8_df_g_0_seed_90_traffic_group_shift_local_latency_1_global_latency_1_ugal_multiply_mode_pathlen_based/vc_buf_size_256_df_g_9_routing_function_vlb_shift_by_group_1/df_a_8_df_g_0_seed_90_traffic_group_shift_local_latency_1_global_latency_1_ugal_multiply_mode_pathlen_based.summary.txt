wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = vlb;
shift_by_group = 1;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.41668,   0.49989,    895.37,    895.37,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24998,   0.24997,    17.983,    17.983,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36993,   0.36992,    25.675,    25.674,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,      0.43,   0.42991,     101.0,    101.02,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,    0.4281,      0.46,    740.55,    740.55,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,    0.4356,   0.43976,    523.68,    526.22,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.4,   0.39986,   0.39984,    33.967,    33.966,         0,  0,  0,  ,  ,  ,  
simulation,         8,      0.35,   0.34991,   0.34989,     23.11,     23.11,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.29992,   0.29991,    19.699,    19.699,         0,  0,  0,  ,  ,  ,  
