wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = min;
shift_by_group = 1;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.48416,   0.48472,    784.16,    223.15,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25022,   0.25022,     10.29,     10.29,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.37042,   0.37043,    11.435,    11.435,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.43023,   0.43023,    13.231,    13.231,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.46022,    0.4602,    16.581,    16.581,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,   0.48019,   0.48015,    41.403,    41.039,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.49,   0.48389,   0.48461,    504.32,    208.34,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.45028,   0.45026,      14.9,      14.9,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.40035,   0.40035,     12.08,     12.08,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35027,   0.35028,    11.138,    11.138,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30019,   0.30019,    10.628,    10.628,         0,  0,  0,  ,  ,  ,  
