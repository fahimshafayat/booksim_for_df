wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_L_restricted;
shift_by_group = 1;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.19566,   0.19838,    2916.0,    136.64,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.19398,   0.19646,    950.91,    118.18,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12995,   0.12995,    16.213,    16.215,         0,  1481357,  2766142,  ,  ,  ,  
simulation,         4,      0.19,   0.17977,   0.17983,    1031.8,     65.69,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.16,   0.15818,   0.15818,    517.51,    41.849,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.14,   0.13999,      0.14,    33.642,    18.591,         0,  2623064,  5373043,  ,  ,  ,  
simulation,         7,      0.15,   0.14977,   0.14977,    156.67,    25.943,         0,  3127664,  6944976,  ,  ,  ,  
simulation,         9,       0.1,  0.099943,  0.099944,    13.664,    13.664,         0,  1394984,  1869899,  ,  ,  ,  
simulation,        10,      0.05,   0.04996,  0.049959,     11.41,     11.41,         0,  933514,  697789,  ,  ,  ,  
