wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = UGAL_L_restricted;
shift_by_group = 2;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.17164,   0.17416,    2381.4,    142.81,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.15274,   0.15519,    1614.9,    152.66,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12563,   0.12562,    586.32,    42.162,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.070094,  0.070094,    14.752,    14.753,         0,  1370732,  3069816,  ,  ,  ,  
simulation,         5,       0.1,   0.10012,   0.10012,    17.604,    17.615,         0,  1449381,  4964453,  ,  ,  ,  
simulation,         6,      0.11,   0.10912,   0.10912,    485.61,      38.5,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.05,   0.05009,   0.05009,    13.168,    13.168,         0,  1284954,  1886360,  ,  ,  ,  
