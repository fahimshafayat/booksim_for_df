wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_G;
shift_by_group = 2;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    20.817,    20.817,         0,  2401618,  2401000,  ,  ,  ,  
simulation,         2,      0.74,   0.46723,   0.69023,    1265.5,    1265.5,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.61988,    0.6199,    44.885,    44.888,         0,  2404304,  3554065,  ,  ,  ,  
simulation,         4,      0.68,   0.53405,   0.67995,    596.14,    596.14,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.65,   0.52248,   0.63284,    878.72,    878.71,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.63,   0.53449,   0.62823,    804.04,    794.97,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.6,   0.59992,   0.59991,    33.826,    33.828,         0,  2403412,  3361727,  ,  ,  ,  
simulation,         8,      0.55,   0.54997,   0.54996,    25.147,    25.147,         0,  2402152,  2881324,  ,  ,  ,  
simulation,        10,      0.45,   0.45012,   0.45013,     17.74,     17.74,         0,  2396020,  1926263,  ,  ,  ,  
