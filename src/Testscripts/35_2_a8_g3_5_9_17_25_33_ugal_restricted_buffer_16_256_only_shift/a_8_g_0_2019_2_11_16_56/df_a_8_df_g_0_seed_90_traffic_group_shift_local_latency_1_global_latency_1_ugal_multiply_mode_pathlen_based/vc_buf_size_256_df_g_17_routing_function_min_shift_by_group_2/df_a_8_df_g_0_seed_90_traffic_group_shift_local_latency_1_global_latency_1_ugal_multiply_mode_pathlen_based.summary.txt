wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = min;
shift_by_group = 2;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.062448,   0.25416,    4367.5,    4367.5,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.062445,   0.24956,    3730.0,    3730.0,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.062441,   0.13026,    2564.0,    2564.0,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.062419,  0.070238,    549.88,    549.88,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.039977,  0.039975,    10.386,    10.386,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.05,   0.04996,  0.049959,     11.47,     11.47,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.06,  0.060009,  0.060006,     21.44,    21.438,         0,  0,  0,  ,  ,  ,  
