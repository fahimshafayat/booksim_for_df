wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_L;
shift_by_group = 2;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.13837,   0.14122,    3555.2,     200.8,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.13868,   0.14152,    2104.2,    197.04,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12695,   0.12701,    519.54,    80.322,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,   0.06996,   0.06996,    13.621,    13.621,         0,  1249461,  1035792,  ,  ,  ,  
simulation,         5,       0.1,  0.099943,  0.099944,    18.681,    18.684,         0,  1461130,  1805389,  ,  ,  ,  
simulation,         6,      0.11,   0.10994,   0.10994,      24.2,    23.195,         0,  1520458,  2150881,  ,  ,  ,  
simulation,         7,      0.12,   0.11966,   0.11966,    258.92,    47.408,         0,  2983129,  4718145,  ,  ,  ,  
simulation,         9,      0.05,  0.049959,  0.049959,    12.458,    12.458,         0,  978720,  652890,  ,  ,  ,  
