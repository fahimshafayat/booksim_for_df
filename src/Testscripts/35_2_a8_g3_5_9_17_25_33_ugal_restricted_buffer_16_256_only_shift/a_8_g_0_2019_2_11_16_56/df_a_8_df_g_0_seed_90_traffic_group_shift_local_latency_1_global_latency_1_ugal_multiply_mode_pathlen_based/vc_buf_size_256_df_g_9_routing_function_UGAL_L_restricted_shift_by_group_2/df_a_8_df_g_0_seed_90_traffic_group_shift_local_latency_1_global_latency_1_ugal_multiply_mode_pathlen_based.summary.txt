wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = UGAL_L_restricted;
shift_by_group = 2;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49212,   0.49398,    482.55,    423.23,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24999,   0.24997,    128.68,    129.16,         0,  2177303,  2162723,  ,  ,  ,  
simulation,         3,      0.37,   0.36993,   0.36992,    106.73,    107.29,         0,  2181841,  4246543,  ,  ,  ,  
simulation,         4,      0.43,   0.42993,   0.42991,    100.23,    100.79,         0,  2184559,  5293086,  ,  ,  ,  
simulation,         5,      0.46,   0.45989,   0.45986,    100.44,    101.03,         0,  2190806,  5828748,  ,  ,  ,  
simulation,         6,      0.48,   0.47935,   0.47985,    162.41,     165.1,         0,  3156952,  8895357,  ,  ,  ,  
simulation,         7,      0.49,   0.48585,    0.4862,    430.36,    269.46,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44996,   0.44994,     99.75,    100.33,         0,  2186339,  5644441,  ,  ,  ,  
simulation,         9,       0.4,   0.39986,   0.39984,    102.83,    103.39,         0,  2183360,  4769311,  ,  ,  ,  
simulation,        10,      0.35,    0.3499,   0.34989,    110.07,    110.61,         0,  2181251,  3897743,  ,  ,  ,  
simulation,        11,       0.3,   0.29992,   0.29991,    120.81,    121.34,         0,  2179977,  3028459,  ,  ,  ,  
