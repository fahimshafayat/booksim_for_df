wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = min;
shift_by_group = 2;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.24917,    0.2614,    2503.3,    476.18,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24856,   0.24905,    511.47,    329.85,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12993,   0.12993,    10.136,    10.136,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.19,      0.19,      0.19,    11.194,    11.194,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.22,   0.22004,   0.22004,    13.285,    13.285,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.23,   0.23007,   0.23007,    15.318,    15.317,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.24,   0.24011,   0.24011,    21.715,    21.733,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.20008,   0.20008,    11.624,    11.624,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.15,   0.14995,   0.14995,    10.354,    10.354,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.1,  0.099924,  0.099925,    9.8987,    9.8988,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.05,   0.04979,  0.049784,    9.6524,    9.6525,         0,  0,  0,  ,  ,  ,  
