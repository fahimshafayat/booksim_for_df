wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = UGAL_G_restricted;
shift_by_group = 1;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50018,   0.50017,    11.662,    11.662,         0,  2029209,  852053,  ,  ,  ,  
simulation,         2,      0.74,   0.73996,   0.73994,    19.794,    19.794,         0,  2878273,  1386610,  ,  ,  ,  
simulation,         3,      0.86,   0.74562,   0.86012,    661.36,    661.36,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.74695,   0.79983,     655.3,     655.3,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.74986,   0.77042,     685.7,     685.7,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.75,   0.74984,   0.75001,    48.911,    48.914,         0,  3365927,  1680693,  ,  ,  ,  
simulation,         7,      0.76,   0.74994,   0.76026,    576.93,    575.05,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.7,   0.69995,   0.69995,     15.27,     15.27,         0,  2796526,  1237115,  ,  ,  ,  
simulation,        10,      0.65,   0.65002,   0.65002,    13.604,    13.604,         0,  2629523,  1115867,  ,  ,  ,  
simulation,        11,       0.6,   0.60022,   0.60022,    12.708,    12.708,         0,  2437493,  1020127,  ,  ,  ,  
