wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = min;
shift_by_group = 2;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.093087,   0.09612,    4062.1,    317.11,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.09296,  0.095969,    3132.3,     314.2,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.092833,  0.095818,    1413.2,    301.72,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.069962,  0.069959,    11.215,    11.215,         0,  0,  0,  ,  ,  ,  
simulation,         5,       0.1,  0.092929,  0.094435,    700.87,     268.5,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.08,  0.079971,  0.079967,    13.043,    13.043,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.09,  0.089945,  0.089955,    26.677,    26.715,         0,  0,  0,  ,  ,  ,  
simulation,         8,      0.05,  0.049933,   0.04993,    10.079,    10.079,         0,  0,  0,  ,  ,  ,  
