wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = UGAL_G;
shift_by_group = 2;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.17798,   0.18089,    3167.8,    159.92,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,     0.178,    0.1809,    1336.6,    156.37,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12988,   0.12987,    12.979,    12.979,         0,  1195861,  1050885,  ,  ,  ,  
simulation,         4,      0.19,   0.17923,   0.17916,    1201.3,    100.49,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.16,   0.15992,   0.15991,    13.906,    13.906,         0,  1470821,  1295100,  ,  ,  ,  
simulation,         6,      0.17,   0.16994,   0.16994,    14.803,    14.803,         0,  1566734,  1373400,  ,  ,  ,  
simulation,         7,      0.18,   0.17932,   0.17933,    307.25,    31.686,         0,  3190240,  2710489,  ,  ,  ,  
simulation,         8,      0.15,   0.14992,   0.14992,    13.463,    13.463,         0,  1377729,  1215149,  ,  ,  ,  
simulation,         9,       0.1,  0.099935,  0.099933,    12.557,    12.557,         0,  930432,  797790,  ,  ,  ,  
simulation,        10,      0.05,  0.049932,   0.04993,     12.03,     12.03,         0,  488182,  375533,  ,  ,  ,  
