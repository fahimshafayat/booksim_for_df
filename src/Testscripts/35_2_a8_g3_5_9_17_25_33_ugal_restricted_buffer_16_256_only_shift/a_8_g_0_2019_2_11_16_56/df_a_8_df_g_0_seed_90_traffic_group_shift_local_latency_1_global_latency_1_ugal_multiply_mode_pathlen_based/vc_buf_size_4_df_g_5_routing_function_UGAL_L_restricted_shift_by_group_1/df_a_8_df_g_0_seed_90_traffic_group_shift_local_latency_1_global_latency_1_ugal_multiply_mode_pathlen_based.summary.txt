wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_L_restricted;
shift_by_group = 1;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.35827,   0.36128,    1412.2,     82.12,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25018,   0.25018,    11.795,    11.795,         0,  1491873,  910458,  ,  ,  ,  
simulation,         3,      0.37,   0.35807,   0.35809,    813.84,    83.541,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31001,   0.31002,    14.434,    14.435,         0,  1792097,  1186079,  ,  ,  ,  
simulation,         5,      0.34,   0.34006,   0.34009,    20.888,    20.886,         0,  1887823,  1381807,  ,  ,  ,  
simulation,         6,      0.35,   0.35013,   0.35016,     28.11,    27.992,         0,  1907393,  1464330,  ,  ,  ,  
simulation,         7,      0.36,   0.35747,   0.35751,    472.53,    80.703,         0,  3292018,  2632079,  ,  ,  ,  
simulation,         9,       0.3,   0.29995,   0.29995,    13.651,    13.652,         0,  1748052,  1133514,  ,  ,  ,  
simulation,        11,       0.2,   0.20008,   0.20008,    11.073,    11.073,         0,  1215602,  706226,  ,  ,  ,  
