wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = UGAL_L;
shift_by_group = 2;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.31506,   0.32682,    1768.2,    347.88,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24998,   0.24997,    30.972,    30.978,         0,  2165044,  2163503,  ,  ,  ,  
simulation,         3,      0.37,   0.31378,   0.32337,    681.59,    259.64,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.29898,   0.29899,    871.59,    217.37,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27991,   0.27992,    51.996,    48.081,         0,  2244092,  2776614,  ,  ,  ,  
simulation,         6,      0.29,   0.28914,   0.28916,    260.57,    103.73,         0,  3996815,  5241888,  ,  ,  ,  
simulation,         7,       0.3,   0.29528,    0.2956,    541.58,    176.49,         1,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.19992,   0.19992,    16.425,    16.425,         0,  2106757,  1351082,  ,  ,  ,  
simulation,        11,      0.15,   0.14992,   0.14992,    12.693,    12.693,         0,  1643074,  949595,  ,  ,  ,  
