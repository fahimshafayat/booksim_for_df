wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_G_restricted;
shift_by_group = 1;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.42383,   0.49595,    557.45,    557.45,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25008,    14.706,    14.706,         0,  1972755,  10036191,  ,  ,  ,  
simulation,         3,      0.37,   0.37011,   0.37011,    18.561,    18.562,         0,  1982084,  15792913,  ,  ,  ,  
simulation,         4,      0.43,   0.41243,   0.42071,    673.73,    668.79,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.40008,   0.40009,     24.01,    24.011,         0,  1982531,  17233250,  ,  ,  ,  
simulation,         6,      0.41,   0.40988,   0.41004,    71.617,     71.64,         0,  2981800,  26612636,  ,  ,  ,  
simulation,         7,      0.42,   0.41074,   0.41402,    394.02,    477.87,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.35,   0.35011,    0.3501,    17.372,    17.373,         0,  1981881,  14830375,  ,  ,  ,  
simulation,        10,       0.3,   0.30005,   0.30006,    15.742,    15.742,         0,  1981139,  12426430,  ,  ,  ,  
