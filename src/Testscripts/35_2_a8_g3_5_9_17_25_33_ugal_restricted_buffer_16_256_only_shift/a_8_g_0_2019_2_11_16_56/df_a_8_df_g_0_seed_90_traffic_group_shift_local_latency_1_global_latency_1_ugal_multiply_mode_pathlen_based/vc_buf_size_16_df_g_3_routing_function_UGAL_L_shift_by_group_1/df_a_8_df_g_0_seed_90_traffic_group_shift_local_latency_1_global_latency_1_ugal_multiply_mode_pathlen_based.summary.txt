wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = UGAL_L;
shift_by_group = 1;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50017,   0.50017,    13.416,    13.416,         0,  2332340,  548885,  ,  ,  ,  
simulation,         2,      0.74,   0.60318,    0.6147,    934.36,    180.41,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.60215,    0.6021,    687.41,    190.16,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.56024,   0.56024,    15.266,    15.266,         0,  2616684,  610798,  ,  ,  ,  
simulation,         5,      0.59,   0.59024,   0.59024,    17.468,    17.467,         0,  2743144,  657445,  ,  ,  ,  
simulation,         6,       0.6,   0.60025,   0.60022,    18.963,    18.964,         0,  2780750,  676987,  ,  ,  ,  
simulation,         7,      0.61,   0.60349,   0.60403,    531.18,    155.35,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.55,   0.55031,    0.5503,    14.816,    14.817,         0,  2571809,  598175,  ,  ,  ,  
simulation,        11,      0.45,   0.45027,   0.45026,    12.676,    12.676,         0,  2085233,  507814,  ,  ,  ,  
