wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = vlb;
shift_by_group = 1;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.37304,   0.37628,    1266.5,    85.026,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25005,    17.847,    17.847,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36999,   0.36999,    41.543,    39.966,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.37313,   0.37636,     662.9,    83.916,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.37334,   0.37496,    675.28,    83.464,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.38,   0.37303,   0.37306,    624.42,    83.477,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.35,   0.35001,   0.35001,    23.764,    23.761,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.30001,   0.30001,    19.478,    19.478,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20008,   0.20007,    16.853,    16.853,         0,  0,  0,  ,  ,  ,  
