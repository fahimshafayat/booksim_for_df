wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_G;
shift_by_group = 2;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50005,   0.50006,    21.571,    21.573,         0,  2402265,  2401381,  ,  ,  ,  
simulation,         2,      0.74,   0.35756,    0.3701,    2595.0,    342.78,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.35911,   0.37173,    2116.8,    336.67,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.36059,   0.37327,    1789.7,    330.67,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.36267,   0.37549,    1543.8,    322.47,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.37382,   0.38637,    1184.4,    286.83,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.45013,   0.45013,    17.738,    17.739,         0,  2395993,  1926290,  ,  ,  ,  
simulation,         9,       0.4,   0.40021,   0.40022,    15.374,    15.374,         0,  2336922,  1505224,  ,  ,  ,  
simulation,        10,      0.35,   0.35016,   0.35016,    13.837,    13.838,         0,  2161183,  1200358,  ,  ,  ,  
