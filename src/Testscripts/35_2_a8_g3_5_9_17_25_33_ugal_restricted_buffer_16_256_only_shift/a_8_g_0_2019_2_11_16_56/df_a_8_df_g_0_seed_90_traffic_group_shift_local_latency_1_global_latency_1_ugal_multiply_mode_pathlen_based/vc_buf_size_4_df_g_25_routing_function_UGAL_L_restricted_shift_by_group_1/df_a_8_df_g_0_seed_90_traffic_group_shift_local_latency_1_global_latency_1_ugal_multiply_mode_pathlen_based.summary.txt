wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_L_restricted;
shift_by_group = 1;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.1619,   0.16454,    2598.2,    157.97,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.15909,   0.16154,    1477.6,    147.13,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12704,   0.12704,    545.42,    34.974,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.070055,  0.070054,    13.928,    13.928,         0,  1259356,  2103457,  ,  ,  ,  
simulation,         5,       0.1,   0.10005,   0.10005,    27.346,    19.242,         0,  1715283,  4145347,  ,  ,  ,  
simulation,         6,      0.11,   0.10932,   0.10932,    382.24,    31.529,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.05,  0.050053,  0.050053,    12.672,    12.672,         0,  1099516,  1301929,  ,  ,  ,  
