wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_L;
shift_by_group = 1;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.34722,   0.47649,    1127.2,    1127.2,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25008,    69.821,    70.274,         0,  2007565,  10097946,  ,  ,  ,  
simulation,         3,      0.37,   0.34115,   0.36997,    544.28,    544.28,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.29835,   0.30993,    460.05,    460.05,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27896,   0.28003,     203.4,    224.82,         0,  3634672,  20797670,  ,  ,  ,  
simulation,         6,      0.29,   0.28525,   0.29004,    540.04,    542.91,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.20009,   0.20009,    75.741,    76.184,         0,  2002029,  7660162,  ,  ,  ,  
simulation,         9,      0.15,   0.15006,   0.15007,    89.053,     89.52,         0,  1999727,  5240242,  ,  ,  ,  
simulation,        10,       0.1,   0.10007,   0.10007,    69.338,    69.704,         0,  1899261,  2924546,  ,  ,  ,  
