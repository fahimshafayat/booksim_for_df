wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = UGAL_L;
shift_by_group = 1;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.13315,   0.13586,    2600.9,    196.04,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.11549,     0.118,    2195.3,    208.81,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.10373,    0.1062,    901.56,    212.03,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.070093,  0.070094,    19.078,    19.081,         0,  1416950,  3025834,  ,  ,  ,  
simulation,         5,       0.1,   0.09743,  0.097434,    554.39,    89.473,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.08,  0.080057,  0.080057,    26.466,    22.509,         0,  1768872,  4441354,  ,  ,  ,  
simulation,         7,      0.09,  0.089539,  0.089543,     399.6,    53.947,         0,  3103220,  8836968,  ,  ,  ,  
simulation,         8,      0.05,  0.050089,   0.05009,    15.423,    15.424,         0,  1345270,  1826044,  ,  ,  ,  
