wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_G;
shift_by_group = 2;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49996,   0.49997,    38.114,    38.115,         0,  2044253,  14296523,  ,  ,  ,  
simulation,         2,      0.74,   0.45482,   0.62365,    1497.8,    1479.0,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,    0.5016,   0.60704,    791.38,    791.18,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.51485,   0.55177,    652.86,    651.69,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.52983,   0.52993,    85.279,    85.311,         0,  2048865,  15292217,  ,  ,  ,  
simulation,         6,      0.54,   0.52317,   0.53736,    589.86,    589.72,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44994,   0.44994,    26.762,    26.762,         0,  2042581,  12656630,  ,  ,  ,  
simulation,         9,       0.4,   0.39995,   0.39994,    22.545,    22.545,         0,  2042028,  11021074,  ,  ,  ,  
simulation,        10,      0.35,   0.35002,   0.35001,    20.053,    20.053,         0,  2041670,  9388520,  ,  ,  ,  
