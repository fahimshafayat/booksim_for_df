vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = vlb;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.37133,   0.42596,    1305.3,    1072.8,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25018,   0.25018,    18.543,    18.543,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.37031,   0.37022,    101.65,    101.66,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.37141,   0.42302,    701.46,    696.22,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.37137,   0.39404,     713.9,    682.85,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.38,   0.37227,    0.3798,    598.63,    588.12,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.35,   0.35015,   0.35016,    32.364,    32.366,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.29995,   0.29995,    21.231,    21.231,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20007,   0.20008,    17.177,    17.177,         0,  0,  0,  ,  ,  ,  
