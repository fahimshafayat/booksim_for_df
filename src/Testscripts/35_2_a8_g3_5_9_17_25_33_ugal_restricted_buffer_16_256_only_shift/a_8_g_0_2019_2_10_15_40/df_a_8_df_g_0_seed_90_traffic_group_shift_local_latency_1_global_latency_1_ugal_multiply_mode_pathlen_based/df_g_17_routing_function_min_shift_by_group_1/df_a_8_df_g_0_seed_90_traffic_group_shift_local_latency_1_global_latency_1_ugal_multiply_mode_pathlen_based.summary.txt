vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = min;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.062448,   0.11006,    4374.2,    4275.5,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.062445,   0.11009,    3742.1,    3732.7,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.062441,   0.11016,    2562.1,    2562.1,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.062416,  0.070238,    541.09,    541.09,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.039976,  0.039975,    10.392,    10.392,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.05,   0.04996,  0.049959,     11.47,     11.47,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.06,  0.059974,   0.05998,    21.017,    21.023,         0,  0,  0,  ,  ,  ,  
