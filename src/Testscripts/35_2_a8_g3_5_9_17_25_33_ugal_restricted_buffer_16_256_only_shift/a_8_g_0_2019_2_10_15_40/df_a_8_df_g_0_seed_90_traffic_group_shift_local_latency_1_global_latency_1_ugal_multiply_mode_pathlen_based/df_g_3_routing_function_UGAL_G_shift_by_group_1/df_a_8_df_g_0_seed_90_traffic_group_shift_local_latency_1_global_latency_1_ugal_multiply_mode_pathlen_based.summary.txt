vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = UGAL_G;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50018,   0.50017,    13.132,    13.133,         0,  2342230,  539032,  ,  ,  ,  
simulation,         2,      0.74,   0.73997,   0.73994,    36.849,    36.849,         0,  2883744,  1382916,  ,  ,  ,  
simulation,         3,      0.86,   0.67455,   0.72154,    1009.4,    566.74,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.68674,   0.73354,    617.28,    461.27,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.68913,   0.71205,     863.8,    470.29,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.75,   0.74983,   0.75001,    72.659,    72.662,         0,  3369481,  1680545,  ,  ,  ,  
simulation,         7,      0.76,   0.70089,   0.72272,    583.79,     389.6,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.7,   0.69998,   0.69995,    24.926,    24.926,         0,  2881596,  1152946,  ,  ,  ,  
simulation,        10,      0.65,   0.65004,   0.65002,    18.781,    18.781,         0,  2849753,  895936,  ,  ,  ,  
simulation,        11,       0.6,   0.60023,   0.60022,    15.741,    15.741,         0,  2735399,  722338,  ,  ,  ,  
