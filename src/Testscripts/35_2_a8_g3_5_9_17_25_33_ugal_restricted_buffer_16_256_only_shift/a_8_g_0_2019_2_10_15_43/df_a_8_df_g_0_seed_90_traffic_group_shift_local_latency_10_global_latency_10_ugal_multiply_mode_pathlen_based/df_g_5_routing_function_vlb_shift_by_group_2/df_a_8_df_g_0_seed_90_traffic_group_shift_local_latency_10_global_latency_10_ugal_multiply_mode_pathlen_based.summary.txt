vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = vlb;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.36973,   0.42464,    1340.2,    1097.3,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25018,   0.25018,    68.048,     68.05,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.37013,   0.37022,    151.02,    150.94,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.36927,   0.42144,     743.0,    736.59,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,    0.3707,   0.39414,    761.33,    735.06,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.38,   0.37097,    0.3797,    650.44,    645.84,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.35,   0.35012,   0.35016,    81.812,    81.806,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.29995,   0.29995,    70.778,    70.777,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20009,   0.20008,    66.678,    66.679,         0,  0,  0,  ,  ,  ,  
