vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_L_restricted;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.45658,   0.46824,     664.3,    384.71,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,    73.934,    73.978,         0,  2052075,  6144607,  ,  ,  ,  
simulation,         3,      0.37,   0.36998,   0.36997,    82.554,    82.694,         0,  2059235,  10093758,  ,  ,  ,  
simulation,         4,      0.43,   0.42916,    0.4292,    171.73,    99.357,         0,  4233072,  24784433,  ,  ,  ,  
simulation,         5,      0.46,   0.44779,   0.44789,     561.3,    196.01,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.43731,   0.43735,    318.06,    115.03,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.4,   0.39994,   0.39994,    85.271,    85.442,         0,  2061698,  11085533,  ,  ,  ,  
simulation,         8,      0.35,   0.35001,   0.35001,    80.971,     81.09,         0,  2058399,  9435527,  ,  ,  ,  
simulation,         9,       0.3,   0.30001,   0.30001,    77.459,    77.536,         0,  2054839,  7786097,  ,  ,  ,  
