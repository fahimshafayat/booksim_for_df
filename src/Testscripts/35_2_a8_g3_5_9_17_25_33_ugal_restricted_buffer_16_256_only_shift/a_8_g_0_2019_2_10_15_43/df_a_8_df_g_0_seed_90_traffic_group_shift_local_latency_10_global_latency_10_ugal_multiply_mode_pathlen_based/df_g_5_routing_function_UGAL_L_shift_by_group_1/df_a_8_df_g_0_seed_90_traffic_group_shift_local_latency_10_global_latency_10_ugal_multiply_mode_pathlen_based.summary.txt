vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_L;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.29559,   0.35005,    2065.0,    1420.0,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25019,   0.25018,    40.616,     40.62,         0,  1986021,  418152,  ,  ,  ,  
simulation,         3,      0.37,   0.29464,   0.34899,    998.66,    980.86,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30996,   0.31002,    58.776,    58.781,         0,  2398461,  582402,  ,  ,  ,  
simulation,         5,      0.34,   0.29507,   0.33924,    610.67,    610.63,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.32,   0.30148,   0.31136,    1305.1,    1237.9,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.3,   0.29991,   0.29995,    49.535,    49.539,         0,  2362813,  520393,  ,  ,  ,  
simulation,         9,       0.2,   0.20007,   0.20008,    39.845,    39.849,         0,  1571920,  351635,  ,  ,  ,  
simulation,        10,      0.15,   0.14996,   0.14995,    39.889,    39.894,         0,  1158778,  283801,  ,  ,  ,  
