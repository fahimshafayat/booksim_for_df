vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_L;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.30777,   0.34794,    1526.0,    990.99,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24919,   0.24919,    328.18,    163.01,         0,  3853382,  19381716,  ,  ,  ,  
simulation,         3,      0.37,   0.30727,   0.33574,    670.14,    582.15,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.28814,   0.29835,    589.48,    473.03,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27249,   0.27475,    605.93,    455.17,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,   0.25814,   0.25826,    451.86,    206.81,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.20009,   0.20009,    90.705,    90.788,         0,  1994850,  7655325,  ,  ,  ,  
simulation,         9,      0.15,   0.15007,   0.15007,    83.234,    83.268,         0,  1990947,  5238057,  ,  ,  ,  
simulation,        10,       0.1,   0.10006,   0.10007,    67.186,    67.198,         0,  1899539,  2911654,  ,  ,  ,  
