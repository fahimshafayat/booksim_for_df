vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = UGAL_L;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.35243,   0.40144,    1319.3,     963.3,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24997,   0.24997,    52.121,    52.157,         0,  2167098,  2162498,  ,  ,  ,  
simulation,         3,      0.37,   0.35473,   0.35545,    1052.8,    714.32,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30991,    0.3099,    62.279,    62.369,         0,  2172602,  3204368,  ,  ,  ,  
simulation,         5,      0.34,   0.33968,   0.33986,    162.01,    157.06,         0,  3747569,  6412472,  ,  ,  ,  
simulation,         6,      0.35,   0.34507,    0.3477,    558.93,    525.91,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.29992,   0.29991,    59.842,    59.915,         0,  2171055,  3029426,  ,  ,  ,  
simulation,        10,       0.2,   0.19992,   0.19992,    18.821,    18.821,         0,  2115536,  1343238,  ,  ,  ,  
