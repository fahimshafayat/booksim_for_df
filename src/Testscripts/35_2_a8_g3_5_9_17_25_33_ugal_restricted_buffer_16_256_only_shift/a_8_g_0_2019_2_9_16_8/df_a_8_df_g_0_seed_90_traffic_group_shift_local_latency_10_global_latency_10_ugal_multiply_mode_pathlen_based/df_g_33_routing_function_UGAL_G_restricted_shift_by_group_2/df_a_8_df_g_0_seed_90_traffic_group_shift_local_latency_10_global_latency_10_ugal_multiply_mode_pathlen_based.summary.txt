vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = UGAL_G_restricted;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.42765,   0.45112,    504.59,    383.06,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25007,   0.25006,    52.301,    52.303,         0,  1982727,  13879232,  ,  ,  ,  
simulation,         3,      0.37,   0.37007,   0.37006,    57.584,    57.586,         0,  1984694,  21495863,  ,  ,  ,  
simulation,         4,      0.43,   0.42682,   0.42682,    405.24,    106.14,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.40006,   0.40005,    60.258,    60.262,         0,  1986890,  23420031,  ,  ,  ,  
simulation,         6,      0.41,   0.41006,   0.41006,     61.86,    61.866,         0,  1990151,  24091626,  ,  ,  ,  
simulation,         7,      0.42,   0.42001,   0.42002,    69.191,    68.664,         0,  2042358,  25361745,  ,  ,  ,  
simulation,         9,      0.35,   0.35006,   0.35006,    56.448,    56.451,         0,  1984380,  20224998,  ,  ,  ,  
simulation,        10,       0.3,   0.30005,   0.30005,    54.271,    54.273,         0,  1983427,  17049068,  ,  ,  ,  
