wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_L;
shift_by_group = 2;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.058054,  0.061487,    4424.2,    563.93,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.058122,  0.061571,    3834.7,    559.18,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.05806,  0.061521,    2756.9,    549.99,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.058755,  0.062183,    740.74,    437.84,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.039856,  0.039848,    43.272,    43.285,         0,  282273,  102340,  ,  ,  ,  
simulation,         6,      0.05,  0.049786,  0.049784,    45.119,    45.125,         0,  359202,  121505,  ,  ,  ,  
simulation,         7,      0.06,  0.059856,  0.059871,    57.025,    57.135,         0,  517661,  158459,  ,  ,  ,  
