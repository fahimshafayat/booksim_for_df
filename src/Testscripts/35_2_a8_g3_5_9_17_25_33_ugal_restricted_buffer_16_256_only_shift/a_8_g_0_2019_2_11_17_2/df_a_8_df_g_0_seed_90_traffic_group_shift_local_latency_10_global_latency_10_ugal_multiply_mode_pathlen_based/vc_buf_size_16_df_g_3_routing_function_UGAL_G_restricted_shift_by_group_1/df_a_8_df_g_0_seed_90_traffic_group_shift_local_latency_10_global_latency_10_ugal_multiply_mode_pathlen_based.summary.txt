wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = UGAL_G_restricted;
shift_by_group = 1;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50019,   0.50017,     42.72,    42.722,         0,  2080283,  805454,  ,  ,  ,  
simulation,         2,      0.74,   0.51732,    0.5294,    1515.2,    218.59,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.51743,   0.52935,    842.66,    212.48,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,    0.5177,   0.52349,    762.26,    207.69,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.51815,   0.51812,    581.47,    200.47,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.51021,    0.5102,    51.094,    51.099,         0,  2114957,  837298,  ,  ,  ,  
simulation,         7,      0.52,   0.51729,   0.51735,    437.96,    148.13,         0,  3688565,  1516991,  ,  ,  ,  
simulation,         9,      0.45,   0.45028,   0.45026,    36.513,    36.515,         0,  1878258,  716217,  ,  ,  ,  
simulation,        10,       0.4,   0.40036,   0.40035,    35.481,    35.482,         0,  1665312,  640752,  ,  ,  ,  
simulation,        11,      0.35,   0.35028,   0.35028,    35.144,    35.146,         0,  1452821,  564850,  ,  ,  ,  
