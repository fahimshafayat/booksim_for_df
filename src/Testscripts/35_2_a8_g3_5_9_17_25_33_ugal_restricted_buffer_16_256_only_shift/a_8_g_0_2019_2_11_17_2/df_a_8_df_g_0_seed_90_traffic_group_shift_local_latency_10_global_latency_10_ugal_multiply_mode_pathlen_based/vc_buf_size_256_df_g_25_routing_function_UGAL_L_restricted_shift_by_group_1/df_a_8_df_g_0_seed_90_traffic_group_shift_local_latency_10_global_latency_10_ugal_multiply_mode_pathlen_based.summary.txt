wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_L_restricted;
shift_by_group = 1;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.34651,   0.46104,    777.37,    777.21,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25008,    104.05,    104.48,         0,  2007486,  10097238,  ,  ,  ,  
simulation,         3,      0.37,   0.33251,    0.3468,    901.76,    878.53,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,    0.3079,   0.30836,    310.84,    226.22,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.28008,   0.28008,    102.92,    103.36,         0,  2011389,  11565740,  ,  ,  ,  
simulation,         6,      0.29,   0.29006,   0.29006,    102.75,    103.19,         0,  2010726,  12044198,  ,  ,  ,  
simulation,         7,       0.3,   0.30007,   0.30006,    105.49,    105.98,         0,  2011355,  12528961,  ,  ,  ,  
simulation,        10,       0.2,   0.20009,   0.20009,    108.89,    109.28,         0,  2003634,  7667295,  ,  ,  ,  
simulation,        11,      0.15,   0.15006,   0.15007,     114.9,    115.28,         0,  1999836,  5246372,  ,  ,  ,  
