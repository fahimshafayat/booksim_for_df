wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_L_restricted;
shift_by_group = 2;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.37447,   0.46817,    719.95,    719.95,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25008,    109.05,    109.48,         0,  2191963,  9924473,  ,  ,  ,  
simulation,         3,      0.37,    0.3573,   0.36225,    450.17,    436.93,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31007,   0.31007,    106.61,     107.1,         0,  2195943,  12840049,  ,  ,  ,  
simulation,         5,      0.34,   0.34013,   0.34014,    108.48,     109.0,         0,  2205682,  14351703,  ,  ,  ,  
simulation,         6,      0.35,   0.34845,   0.34874,    374.96,    188.55,         0,  5925665,  39804557,  ,  ,  ,  
simulation,         7,      0.36,   0.35256,   0.35401,    475.79,    393.95,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.30005,   0.30006,    106.72,     107.2,         0,  2195009,  12351588,  ,  ,  ,  
simulation,        11,       0.2,   0.20009,   0.20009,    114.14,    114.55,         0,  2186242,  7486792,  ,  ,  ,  
