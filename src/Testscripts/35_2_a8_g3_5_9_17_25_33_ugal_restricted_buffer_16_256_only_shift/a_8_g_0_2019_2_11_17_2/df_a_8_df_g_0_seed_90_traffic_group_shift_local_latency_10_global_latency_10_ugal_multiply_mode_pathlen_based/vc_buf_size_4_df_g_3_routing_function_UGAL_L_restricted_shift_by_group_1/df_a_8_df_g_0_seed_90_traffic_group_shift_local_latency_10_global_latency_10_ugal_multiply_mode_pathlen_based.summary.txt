wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = UGAL_L_restricted;
shift_by_group = 1;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.14014,   0.14359,    3593.0,    239.79,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.13959,   0.14305,    2200.3,    237.35,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.13012,   0.13007,    57.701,    57.767,         0,  514343,  237887,  ,  ,  ,  
simulation,         4,      0.19,   0.14025,   0.14368,    1288.6,    231.26,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.16,   0.13933,   0.14273,    636.89,    216.74,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.14,   0.13871,   0.13874,    522.66,    168.39,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.1,   0.10015,   0.10014,    39.116,    39.124,         0,  403403,  173795,  ,  ,  ,  
simulation,         8,      0.05,   0.05009,  0.050078,    36.355,    36.357,         0,  200637,  88253,  ,  ,  ,  
