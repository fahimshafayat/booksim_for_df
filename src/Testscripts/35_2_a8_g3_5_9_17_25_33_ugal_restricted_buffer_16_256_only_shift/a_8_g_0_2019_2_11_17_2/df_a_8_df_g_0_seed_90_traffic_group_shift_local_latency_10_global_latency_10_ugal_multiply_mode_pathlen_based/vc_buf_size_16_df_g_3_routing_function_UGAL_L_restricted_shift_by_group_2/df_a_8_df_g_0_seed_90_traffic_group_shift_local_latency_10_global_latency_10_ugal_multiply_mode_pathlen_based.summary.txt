wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = UGAL_L_restricted;
shift_by_group = 2;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50021,   0.50017,    47.392,    47.404,         0,  2075492,  810389,  ,  ,  ,  
simulation,         2,      0.74,   0.54461,   0.55656,    1329.2,    211.05,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.54419,   0.55641,    624.21,     202.6,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.54602,   0.54678,     725.9,    207.67,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.53015,   0.53011,    67.933,    67.633,         0,  2141828,  935451,  ,  ,  ,  
simulation,         6,      0.54,    0.5387,    0.5388,    228.85,    114.46,         0,  3716128,  1686939,  ,  ,  ,  
simulation,         7,      0.55,   0.54505,   0.54526,    513.79,    170.39,         1,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,   0.45029,   0.45026,    38.674,    38.675,         0,  1916736,  678295,  ,  ,  ,  
simulation,        11,       0.4,   0.40035,   0.40035,    36.472,    36.474,         0,  1708492,  597467,  ,  ,  ,  
