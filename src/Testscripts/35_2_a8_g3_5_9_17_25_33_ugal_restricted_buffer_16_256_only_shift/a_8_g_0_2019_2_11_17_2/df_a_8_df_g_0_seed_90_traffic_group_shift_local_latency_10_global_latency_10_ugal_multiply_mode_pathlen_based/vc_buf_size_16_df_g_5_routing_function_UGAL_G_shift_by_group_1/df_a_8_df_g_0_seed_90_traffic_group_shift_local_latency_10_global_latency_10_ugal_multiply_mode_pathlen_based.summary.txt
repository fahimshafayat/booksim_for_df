wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_G;
shift_by_group = 1;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.23847,   0.25169,    2635.1,    522.24,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25018,   0.25018,    43.871,    43.878,         0,  1696088,  708767,  ,  ,  ,  
simulation,         3,      0.37,   0.23894,   0.25216,    1782.0,    507.73,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.23917,   0.25249,    1136.8,    480.96,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.24081,   0.25396,    672.67,    417.78,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,   0.26015,   0.26015,    44.934,    44.942,         0,  1774464,  726814,  ,  ,  ,  
simulation,         7,      0.27,   0.24413,   0.25062,    826.12,     403.8,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.2,   0.20008,   0.20008,    42.543,     42.55,         0,  1341767,  581721,  ,  ,  ,  
simulation,        10,      0.15,   0.14997,   0.14995,    42.289,    42.295,         0,  1001421,  441132,  ,  ,  ,  
simulation,        11,       0.1,  0.099926,  0.099925,    42.297,    42.306,         0,  664676,  297123,  ,  ,  ,  
