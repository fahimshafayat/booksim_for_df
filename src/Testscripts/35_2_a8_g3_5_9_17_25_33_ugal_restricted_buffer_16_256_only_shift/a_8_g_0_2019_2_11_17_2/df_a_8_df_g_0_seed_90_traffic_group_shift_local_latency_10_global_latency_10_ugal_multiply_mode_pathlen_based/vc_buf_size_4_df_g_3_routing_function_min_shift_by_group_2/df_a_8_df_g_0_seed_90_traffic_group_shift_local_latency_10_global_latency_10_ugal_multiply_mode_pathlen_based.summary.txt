wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = min;
shift_by_group = 2;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.088106,  0.091418,    4105.5,    365.19,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.087968,  0.091261,    3219.7,    365.14,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.088076,   0.09139,    1577.5,    351.42,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,   0.07021,  0.070203,    40.754,    40.755,         0,  0,  0,  ,  ,  ,  
simulation,         5,       0.1,  0.087702,  0.091037,    580.79,    306.89,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.08,  0.080186,   0.08018,    53.051,    53.055,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.09,   0.08815,  0.088449,    714.62,     334.6,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.05,  0.050085,  0.050078,    35.677,    35.676,         0,  0,  0,  ,  ,  ,  
