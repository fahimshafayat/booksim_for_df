wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = min;
shift_by_group = 2;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4989,   0.50006,    272.39,    273.65,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.49821,   0.71477,    1632.4,    1632.3,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.49814,   0.62008,     983.9,     983.9,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,     0.498,   0.55988,     565.3,     565.3,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.49888,    0.5297,    596.52,    596.52,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.49946,   0.51028,     549.6,     549.6,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.45028,   0.45026,    39.362,    39.364,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.40035,   0.40035,    36.853,    36.854,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35028,   0.35028,    35.897,    35.898,         0,  0,  0,  ,  ,  ,  
