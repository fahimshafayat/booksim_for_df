wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_G_restricted;
shift_by_group = 1;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.05483,   0.05788,    4467.9,    520.34,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.056929,  0.059985,    3688.1,    498.15,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.057086,  0.060142,    2456.7,    483.12,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.055447,  0.057717,    768.87,    330.42,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.039973,  0.039975,    49.688,    49.699,         0,  399731,  910506,  ,  ,  ,  
simulation,         6,      0.05,  0.048518,  0.048495,    549.48,    151.56,         1,  0,  0,  ,  ,  ,  
