wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_G_restricted;
shift_by_group = 1;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.049963,  0.052999,    4484.2,    560.84,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.052556,  0.055572,    3576.8,    530.84,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.052379,  0.055285,    2444.1,    500.82,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.047395,  0.050005,    1224.3,    450.38,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.038721,  0.038748,    579.72,    167.74,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.02,  0.020054,  0.020056,    48.132,    48.137,         0,  273212,  688984,  ,  ,  ,  
simulation,         7,      0.03,  0.030039,  0.030042,    52.001,    52.005,         0,  370386,  1075144,  ,  ,  ,  
