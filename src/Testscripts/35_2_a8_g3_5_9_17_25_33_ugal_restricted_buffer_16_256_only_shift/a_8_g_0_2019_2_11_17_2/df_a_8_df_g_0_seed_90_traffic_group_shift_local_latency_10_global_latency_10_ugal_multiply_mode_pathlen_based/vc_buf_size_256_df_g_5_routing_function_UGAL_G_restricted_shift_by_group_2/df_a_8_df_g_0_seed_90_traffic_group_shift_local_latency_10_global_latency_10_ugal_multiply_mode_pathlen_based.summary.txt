wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_G_restricted;
shift_by_group = 2;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    45.824,    45.829,         0,  2385981,  2419481,  ,  ,  ,  
simulation,         2,      0.74,   0.60647,   0.74011,    826.17,    826.17,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.61989,    0.6199,    67.227,    67.234,         0,  2405556,  3555620,  ,  ,  ,  
simulation,         4,      0.68,   0.58418,   0.67849,    1022.5,    1022.4,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.65,   0.55195,   0.65007,    1399.9,    1399.9,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.63,   0.62509,   0.62977,    530.49,    529.24,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.6,   0.59989,   0.59991,    58.214,    58.222,         0,  2404500,  3362939,  ,  ,  ,  
simulation,         8,      0.55,   0.54994,   0.54996,    51.038,    51.042,         0,  2404234,  2883760,  ,  ,  ,  
simulation,        10,      0.45,   0.45011,   0.45013,    42.966,     42.97,         0,  2242263,  2082206,  ,  ,  ,  
