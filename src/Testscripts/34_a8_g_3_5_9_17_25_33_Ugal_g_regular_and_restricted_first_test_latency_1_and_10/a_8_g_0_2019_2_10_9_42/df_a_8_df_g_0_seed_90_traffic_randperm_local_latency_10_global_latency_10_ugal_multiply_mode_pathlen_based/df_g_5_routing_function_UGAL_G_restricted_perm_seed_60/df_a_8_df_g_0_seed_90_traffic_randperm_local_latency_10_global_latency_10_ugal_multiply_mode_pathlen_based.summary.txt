vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
perm_seed = 60;
routing_function = UGAL_G_restricted;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    31.696,      31.7,         0,  3857263,  766752,  ,  ,  ,  
simulation,         2,      0.74,   0.74002,   0.74002,    33.302,    33.306,         0,  5803550,  1041228,  ,  ,  ,  
simulation,         3,      0.86,   0.85992,   0.85991,    35.991,    35.994,         0,  6651576,  1302587,  ,  ,  ,  
simulation,         4,      0.92,   0.91996,   0.91995,    38.388,    38.391,         0,  7026946,  1483401,  ,  ,  ,  
simulation,         5,      0.95,   0.94989,   0.94988,    40.154,    40.158,         0,  7209910,  1578080,  ,  ,  ,  
simulation,         6,      0.97,   0.96983,   0.96983,     41.68,    41.684,         0,  7331859,  1642963,  ,  ,  ,  
simulation,         7,      0.98,   0.97985,   0.97984,     42.72,    42.725,         0,  7390322,  1676986,  ,  ,  ,  
simulation,         9,       0.9,   0.89987,   0.89987,    37.432,    37.436,         0,  6905380,  1419718,  ,  ,  ,  
simulation,        10,      0.85,   0.84998,   0.84998,    35.643,    35.646,         0,  6589726,  1272521,  ,  ,  ,  
simulation,        11,       0.8,   0.80003,   0.80003,    34.325,    34.329,         0,  6252910,  1147382,  ,  ,  ,  
