vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
perm_seed = 62;
routing_function = vlb;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49956,   0.49967,    111.72,    95.411,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.52573,   0.57636,    1399.8,    850.74,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.52228,   0.56972,    760.24,    634.07,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.52309,   0.54065,    607.54,    447.21,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.51828,   0.52026,    516.48,    344.33,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,    0.5075,   0.50774,    421.47,    171.62,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.52,   0.51451,   0.51531,    511.74,    250.22,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.45,   0.45014,   0.45013,    64.971,    64.982,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.4,   0.40023,   0.40022,    61.438,    61.449,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.35,   0.35017,   0.35016,    59.655,    59.665,         0,  0,  0,  ,  ,  ,  
