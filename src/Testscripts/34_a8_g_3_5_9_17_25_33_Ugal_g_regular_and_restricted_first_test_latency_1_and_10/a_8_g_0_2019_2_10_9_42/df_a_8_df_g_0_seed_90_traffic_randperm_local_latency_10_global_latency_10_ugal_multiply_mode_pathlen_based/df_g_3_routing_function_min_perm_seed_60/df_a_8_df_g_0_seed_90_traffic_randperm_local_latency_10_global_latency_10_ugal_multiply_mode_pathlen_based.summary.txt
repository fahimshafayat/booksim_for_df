vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 60;
routing_function = min;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49871,   0.49871,    227.57,    49.221,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.70803,    0.7083,     831.4,    160.27,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.61359,   0.61376,    495.01,    98.667,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55727,   0.55727,    468.07,    48.062,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.59,   0.58586,   0.58586,    367.29,    64.769,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.57,     0.567,   0.56701,    289.72,    50.537,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.55,   0.54757,   0.54756,    408.73,    47.999,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.45,    0.4502,   0.45026,    42.309,    42.553,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.4,   0.40036,   0.40035,     29.08,    29.084,         0,  0,  0,  ,  ,  ,  
