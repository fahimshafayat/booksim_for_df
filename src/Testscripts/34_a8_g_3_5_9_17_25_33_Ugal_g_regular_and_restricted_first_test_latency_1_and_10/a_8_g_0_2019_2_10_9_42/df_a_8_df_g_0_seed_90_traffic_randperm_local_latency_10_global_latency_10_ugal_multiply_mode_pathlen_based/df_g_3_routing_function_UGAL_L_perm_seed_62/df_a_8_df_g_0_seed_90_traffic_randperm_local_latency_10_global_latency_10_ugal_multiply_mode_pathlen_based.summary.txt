vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 62;
routing_function = UGAL_L;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50018,   0.50017,    30.708,    30.712,         0,  2437087,  416375,  ,  ,  ,  
simulation,         2,      0.74,   0.73996,   0.73994,    32.852,    32.857,         0,  3603163,  620742,  ,  ,  ,  
simulation,         3,      0.86,      0.86,   0.86001,    38.846,     38.85,         0,  3939171,  970770,  ,  ,  ,  
simulation,         4,      0.92,   0.92009,   0.92005,    44.249,    44.259,         0,  4037086,  1218566,  ,  ,  ,  
simulation,         5,      0.95,   0.95006,   0.95001,    48.575,    48.583,         0,  4078942,  1349005,  ,  ,  ,  
simulation,         6,      0.97,   0.96999,   0.97002,    57.839,    57.845,         0,  4082589,  1465397,  ,  ,  ,  
simulation,         7,      0.98,     0.975,   0.97504,     400.4,    94.346,         0,  7701170,  2901055,  ,  ,  ,  
simulation,         9,       0.9,    0.9001,   0.90009,    42.123,    42.127,         0,  4009602,  1130666,  ,  ,  ,  
simulation,        10,      0.85,   0.85008,   0.85006,    38.111,    38.115,         0,  3923138,  929880,  ,  ,  ,  
simulation,        11,       0.8,   0.80011,    0.8001,    35.082,    35.086,         0,  3810501,  756532,  ,  ,  ,  
