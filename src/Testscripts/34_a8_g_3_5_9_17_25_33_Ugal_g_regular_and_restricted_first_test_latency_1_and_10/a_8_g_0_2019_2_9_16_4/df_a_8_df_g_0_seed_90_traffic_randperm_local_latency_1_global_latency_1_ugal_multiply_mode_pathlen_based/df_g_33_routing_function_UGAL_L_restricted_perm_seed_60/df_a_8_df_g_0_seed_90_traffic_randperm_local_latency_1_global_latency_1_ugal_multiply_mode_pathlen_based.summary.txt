vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 60;
routing_function = UGAL_L_restricted;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49959,    0.4996,    101.35,    34.329,         0,  42820702,  31884298,  ,  ,  ,  
simulation,         2,      0.74,   0.55694,    0.5957,    816.98,    508.81,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.56886,   0.57976,    521.31,     280.0,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,    0.5561,   0.55608,    330.17,    87.966,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.52908,   0.52907,    208.78,    50.388,         0,  55894803,  46680237,  ,  ,  ,  
simulation,         6,      0.54,   0.53847,   0.53847,    332.22,    57.909,         0,  59735301,  52278519,  ,  ,  ,  
simulation,         7,      0.55,   0.54755,   0.54755,    228.13,    66.109,         1,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,   0.44999,      0.45,    21.737,     21.78,         0,  17524037,  11595552,  ,  ,  ,  
simulation,        11,       0.4,   0.40005,   0.40005,    16.165,    16.173,         0,  15553459,  9761264,  ,  ,  ,  
