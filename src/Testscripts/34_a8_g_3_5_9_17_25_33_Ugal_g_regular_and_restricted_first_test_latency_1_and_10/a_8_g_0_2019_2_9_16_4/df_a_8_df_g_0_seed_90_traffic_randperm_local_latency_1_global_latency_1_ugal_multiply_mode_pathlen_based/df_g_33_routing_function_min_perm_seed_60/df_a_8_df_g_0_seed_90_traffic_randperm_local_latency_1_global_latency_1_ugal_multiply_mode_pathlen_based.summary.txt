vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 60;
routing_function = min;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.40252,   0.42588,    626.99,    445.75,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24912,   0.24918,    334.38,    65.644,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.33847,   0.34879,     644.0,    439.81,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30106,   0.30106,    513.84,    183.92,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27515,   0.27519,    408.95,    194.83,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,   0.25782,   0.25795,    454.54,    188.92,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.20007,   0.20007,    10.787,    10.792,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.15,   0.15011,   0.15011,     9.591,    9.5911,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.1,   0.10012,   0.10012,    9.4558,    9.4558,         0,  0,  0,  ,  ,  ,  
