internal_speedup = 4.0;
credit_delay = 2;
sw_allocator = separable_input_first;
injection_rate_uses_flits = 1;
wait_for_tail_credit = 0;
packet_size = 1;
num_vcs = 7;
routing_delay = 0;
vc_alloc_delay = 1;
priority = none;
alloc_iters = 1;
input_speedup = 1;
vc_allocator = separable_input_first;
warmup_periods = 3;
sw_alloc_delay = 1;
sample_period = 10000;
output_speedup = 1;
sim_count = 1;
vc_buf_size = 64;
st_final_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = vlb;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49997,   0.49998,    198.39,    198.45,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.49567,   0.55338,    1738.0,    1054.1,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,    0.4952,   0.55282,    1088.5,    901.82,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.49479,   0.54854,    657.87,    644.39,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.49736,   0.52319,    673.32,    638.43,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.50081,   0.50835,    558.74,    550.34,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.45001,      0.45,    82.424,    82.424,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.40005,   0.40005,    72.647,    72.649,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35007,   0.35006,    68.951,    68.953,         0,  0,  0,  ,  ,  ,  
