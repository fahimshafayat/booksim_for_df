vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = UGAL_L;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.35215,   0.40113,    1340.9,    972.82,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24997,   0.24997,    52.071,    52.107,         0,  2167240,  2162645,  ,  ,  ,  
simulation,         3,      0.37,   0.35184,   0.36438,    474.11,    428.66,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30992,    0.3099,    62.229,    62.323,         0,  2173164,  3205153,  ,  ,  ,  
simulation,         5,      0.34,   0.33881,   0.33902,    323.08,    211.84,         0,  3974625,  6775983,  ,  ,  ,  
simulation,         6,      0.35,   0.34471,   0.34697,    581.17,     507.1,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.29992,   0.29991,    59.852,    59.933,         0,  2170493,  3028591,  ,  ,  ,  
simulation,        10,       0.2,   0.19993,   0.19992,    18.682,    18.681,         0,  2115671,  1342994,  ,  ,  ,  
