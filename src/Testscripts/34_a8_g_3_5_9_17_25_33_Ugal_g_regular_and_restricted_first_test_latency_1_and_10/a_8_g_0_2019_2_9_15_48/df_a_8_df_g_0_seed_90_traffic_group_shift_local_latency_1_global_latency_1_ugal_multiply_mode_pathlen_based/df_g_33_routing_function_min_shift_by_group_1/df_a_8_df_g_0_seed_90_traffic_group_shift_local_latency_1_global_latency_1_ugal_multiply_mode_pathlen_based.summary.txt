vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = min;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.031226,  0.077794,    4286.3,    3805.3,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.031224,  0.077791,    3581.3,    3493.0,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.031223,  0.072788,    2292.6,    2292.6,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.031222,  0.069824,    2272.2,    2272.2,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.031217,  0.040022,    1035.7,    1035.7,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.02,  0.020076,  0.020077,    10.378,    10.378,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.03,  0.030115,  0.030113,    22.126,    22.133,         0,  0,  0,  ,  ,  ,  
