vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 62;
routing_function = UGAL_L_restricted;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50017,   0.50017,     9.786,    9.7862,         0,  1924885,  926148,  ,  ,  ,  
simulation,         2,      0.74,   0.73996,   0.73994,    11.028,    11.028,         0,  2902173,  1317548,  ,  ,  ,  
simulation,         3,      0.86,   0.86001,   0.86001,    12.454,    12.454,         0,  3341440,  1562786,  ,  ,  ,  
simulation,         4,      0.92,   0.92005,   0.92005,    13.803,    13.804,         0,  3507929,  1739169,  ,  ,  ,  
simulation,         5,      0.95,   0.95003,   0.95001,    14.831,    14.831,         0,  3575445,  1842760,  ,  ,  ,  
simulation,         6,      0.97,   0.97001,   0.97002,    15.765,    15.765,         0,  3618472,  1913918,  ,  ,  ,  
simulation,         7,      0.98,   0.98006,   0.98005,    16.428,    16.428,         0,  3638192,  1951016,  ,  ,  ,  
simulation,         9,       0.9,   0.90008,   0.90009,    13.282,    13.282,         0,  3455926,  1677084,  ,  ,  ,  
simulation,        10,      0.85,   0.85007,   0.85006,    12.276,    12.277,         0,  3310414,  1537038,  ,  ,  ,  
simulation,        11,       0.8,   0.80011,    0.8001,    11.601,    11.602,         0,  3134472,  1427653,  ,  ,  ,  
