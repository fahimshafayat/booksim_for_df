vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 17;
perm_seed = 62;
routing_function = UGAL_G_restricted;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49998,   0.49997,    11.987,    11.987,         0,  12305379,  3930139,  ,  ,  ,  
simulation,         2,      0.74,   0.73993,   0.73994,    21.423,    21.424,         0,  17211296,  6829665,  ,  ,  ,  
simulation,         3,      0.86,   0.77471,   0.78775,    716.58,    289.73,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.78289,   0.78318,    488.64,    165.94,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.76992,   0.76993,    29.858,     29.86,         0,  17418334,  7605974,  ,  ,  ,  
simulation,         6,      0.78,   0.77988,   0.77988,    36.487,    36.492,         0,  17456766,  7900342,  ,  ,  ,  
simulation,         7,      0.79,   0.78989,   0.78989,    51.947,    51.962,         0,  17472492,  8222989,  ,  ,  ,  
simulation,         8,      0.75,   0.74995,   0.74996,     23.34,     23.34,         0,  17296096,  7072617,  ,  ,  ,  
simulation,         9,       0.7,   0.69995,   0.69994,    17.175,    17.175,         0,  16717012,  6018759,  ,  ,  ,  
simulation,        10,      0.65,   0.65001,   0.65001,    14.699,      14.7,         0,  15843296,  5266581,  ,  ,  ,  
simulation,        11,       0.6,   0.59994,   0.59993,    13.355,    13.355,         0,  14768797,  4714590,  ,  ,  ,  
