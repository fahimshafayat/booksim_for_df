vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
perm_seed = 62;
routing_function = min;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    9.9451,    9.9452,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.70929,   0.70948,    778.53,    137.09,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.61627,   0.61648,    466.78,    91.481,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.68,   0.66448,   0.66492,    477.87,     128.7,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.65,   0.64082,   0.64082,    538.07,    97.123,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.63,   0.62452,   0.62452,    516.51,     98.55,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.6,   0.59809,   0.59808,    230.82,    44.247,         0,  0,  0,  ,  ,  ,  
simulation,         8,      0.55,   0.54995,   0.54996,    10.702,    10.703,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,   0.45013,   0.45013,    9.6605,    9.6607,         0,  0,  0,  ,  ,  ,  
