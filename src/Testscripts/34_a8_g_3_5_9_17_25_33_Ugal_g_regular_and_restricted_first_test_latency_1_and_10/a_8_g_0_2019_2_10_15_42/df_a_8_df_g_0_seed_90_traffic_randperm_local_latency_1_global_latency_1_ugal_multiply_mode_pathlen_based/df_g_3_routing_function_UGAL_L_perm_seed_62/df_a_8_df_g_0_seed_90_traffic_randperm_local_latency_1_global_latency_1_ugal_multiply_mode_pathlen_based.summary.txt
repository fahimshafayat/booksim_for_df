vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 62;
routing_function = UGAL_L;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50017,   0.50017,     10.31,     10.31,         0,  2199913,  651219,  ,  ,  ,  
simulation,         2,      0.74,   0.73994,   0.73994,    12.068,    12.068,         0,  3297374,  922830,  ,  ,  ,  
simulation,         3,      0.86,   0.86002,   0.86001,    15.106,    15.106,         0,  3666983,  1238247,  ,  ,  ,  
simulation,         4,      0.92,   0.92004,   0.92005,    19.139,     19.14,         0,  3772307,  1476382,  ,  ,  ,  
simulation,         5,      0.95,   0.94811,   0.94816,    157.17,    54.372,         0,  6657516,  2859178,  ,  ,  ,  
simulation,         6,      0.97,   0.94833,   0.94754,    540.15,    136.66,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.96,   0.95084,   0.95106,    476.06,    101.26,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,    0.9001,   0.90009,    17.291,    17.293,         0,  3746160,  1387470,  ,  ,  ,  
simulation,        10,      0.85,   0.85007,   0.85006,    14.727,    14.727,         0,  3645463,  1202558,  ,  ,  ,  
simulation,        11,       0.8,   0.80012,    0.8001,    13.208,    13.209,         0,  3510644,  1052239,  ,  ,  ,  
