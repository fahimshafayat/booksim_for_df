vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_L_restricted;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.47011,   0.47086,    994.88,    282.02,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25005,    32.849,    32.886,         0,  2047129,  6132917,  ,  ,  ,  
simulation,         3,      0.37,   0.36998,   0.36997,    32.859,    32.909,         0,  2049417,  10061262,  ,  ,  ,  
simulation,         4,      0.43,   0.42991,   0.42991,    35.903,     35.97,         0,  2053730,  12043240,  ,  ,  ,  
simulation,         5,      0.46,   0.45871,   0.45874,    203.25,    54.542,         0,  4467572,  28290173,  ,  ,  ,  
simulation,         6,      0.48,    0.4665,   0.46664,    535.68,    158.17,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.46352,   0.46362,    525.52,    103.66,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44994,   0.44994,    39.248,    39.336,         0,  2059414,  12730557,  ,  ,  ,  
simulation,         9,       0.4,   0.39995,   0.39994,    33.809,    33.865,         0,  2050104,  11043485,  ,  ,  ,  
simulation,        10,      0.35,   0.35002,   0.35001,     32.53,    32.576,         0,  2048770,  9404752,  ,  ,  ,  
simulation,        11,       0.3,   0.30002,   0.30001,    32.331,    32.373,         0,  2048685,  7769886,  ,  ,  ,  
