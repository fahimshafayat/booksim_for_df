vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = vlb;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.34957,   0.39922,    1416.6,    1065.5,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25008,    18.402,    18.402,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.35568,   0.36518,    955.39,    864.07,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31007,   0.31007,    22.405,    22.404,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,   0.34013,   0.34014,    31.093,    31.096,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.35,   0.35014,    0.3501,    45.415,    45.424,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.36,   0.35603,    0.3593,    554.43,    528.34,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.30005,   0.30006,     21.27,     21.27,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.2,   0.20009,   0.20009,    17.065,    17.065,         0,  0,  0,  ,  ,  ,  
