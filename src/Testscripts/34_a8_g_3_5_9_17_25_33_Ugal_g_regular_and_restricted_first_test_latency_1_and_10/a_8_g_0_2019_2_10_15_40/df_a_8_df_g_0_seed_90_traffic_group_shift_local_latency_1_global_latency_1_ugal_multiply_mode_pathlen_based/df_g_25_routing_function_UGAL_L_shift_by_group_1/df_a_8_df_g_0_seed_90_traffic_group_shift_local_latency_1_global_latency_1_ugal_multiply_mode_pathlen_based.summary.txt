vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_L;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.3377,   0.37561,    1348.0,    852.58,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25008,    38.248,    38.327,         0,  1998005,  10087013,  ,  ,  ,  
simulation,         3,      0.37,   0.33194,   0.34388,    842.51,    525.33,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,    0.2981,   0.29862,    823.13,    540.82,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27812,   0.27931,    416.46,    368.16,         0,  3562359,  20423560,  ,  ,  ,  
simulation,         6,      0.29,    0.2848,    0.2863,    567.97,    475.13,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.20009,   0.20009,    36.129,     36.18,         0,  1989399,  7642508,  ,  ,  ,  
simulation,         9,      0.15,   0.15007,   0.15007,    36.274,    36.312,         0,  1987330,  5230724,  ,  ,  ,  
simulation,        10,       0.1,   0.10007,   0.10007,     28.57,    28.593,         0,  1890191,  2918627,  ,  ,  ,  
