vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = vlb;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.45409,   0.47879,    851.04,    723.06,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25005,    17.761,    17.761,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36997,   0.36997,    23.538,    23.539,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42994,   0.42991,    38.162,    38.164,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45993,   0.45999,    118.75,    118.88,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,   0.45619,   0.46536,    1053.7,    879.83,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,    0.4598,   0.46865,    557.14,    520.54,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44988,   0.44994,    62.842,    62.862,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39994,   0.39994,    27.812,    27.812,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35002,   0.35001,    21.799,      21.8,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30002,   0.30001,    19.227,    19.227,         0,  0,  0,  ,  ,  ,  
