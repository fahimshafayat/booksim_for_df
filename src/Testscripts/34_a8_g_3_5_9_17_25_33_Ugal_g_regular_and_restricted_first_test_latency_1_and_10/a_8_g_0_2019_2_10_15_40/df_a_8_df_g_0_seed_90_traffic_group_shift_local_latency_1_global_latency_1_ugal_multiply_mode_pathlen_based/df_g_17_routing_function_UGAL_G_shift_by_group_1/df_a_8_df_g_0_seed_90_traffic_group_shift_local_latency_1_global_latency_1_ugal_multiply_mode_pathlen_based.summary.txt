vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_G;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49995,   0.49997,     37.95,     37.95,         0,  2044059,  14295896,  ,  ,  ,  
simulation,         2,      0.74,   0.41504,   0.45639,    1785.2,    854.88,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,    0.4669,   0.50317,    963.84,    602.22,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.49596,   0.50672,    862.26,    383.51,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.51311,   0.51317,    658.66,    226.16,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.50992,   0.50995,    43.433,    43.436,         0,  2044667,  14624010,  ,  ,  ,  
simulation,         7,      0.52,   0.51987,    0.5199,    53.109,    53.114,         0,  2045966,  14955042,  ,  ,  ,  
simulation,         9,      0.45,   0.44995,   0.44994,    26.698,    26.698,         0,  2042703,  12657485,  ,  ,  ,  
simulation,        10,       0.4,   0.39995,   0.39994,    22.486,    22.487,         0,  2042096,  11021675,  ,  ,  ,  
simulation,        11,      0.35,   0.35002,   0.35001,    20.041,    20.041,         0,  2041636,  9388378,  ,  ,  ,  
