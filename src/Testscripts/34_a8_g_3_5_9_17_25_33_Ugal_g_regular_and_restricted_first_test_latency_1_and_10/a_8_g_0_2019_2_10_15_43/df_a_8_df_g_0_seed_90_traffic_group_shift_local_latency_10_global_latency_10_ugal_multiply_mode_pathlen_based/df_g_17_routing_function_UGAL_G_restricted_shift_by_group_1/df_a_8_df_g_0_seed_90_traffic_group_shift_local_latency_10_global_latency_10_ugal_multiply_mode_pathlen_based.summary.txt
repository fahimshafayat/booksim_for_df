vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_G_restricted;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49997,   0.49997,    66.422,    66.425,         0,  2049077,  14325668,  ,  ,  ,  
simulation,         2,      0.74,   0.45496,    0.4967,    1637.1,    759.21,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.47937,   0.51409,    855.05,    548.16,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.50684,   0.51681,    711.05,    346.93,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.51653,   0.51639,    521.79,    194.15,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.50992,   0.50995,     72.56,    72.582,         0,  2053693,  14680782,  ,  ,  ,  
simulation,         7,      0.52,   0.51785,   0.51782,    364.73,    115.24,         0,  4170783,  30345239,  ,  ,  ,  
simulation,         9,      0.45,   0.44994,   0.44994,    57.928,     57.93,         0,  2044689,  12665740,  ,  ,  ,  
simulation,        10,       0.4,   0.39994,   0.39994,    54.589,     54.59,         0,  2043738,  11028516,  ,  ,  ,  
simulation,        11,      0.35,   0.35002,   0.35001,    52.058,    52.059,         0,  2043147,  9394674,  ,  ,  ,  
