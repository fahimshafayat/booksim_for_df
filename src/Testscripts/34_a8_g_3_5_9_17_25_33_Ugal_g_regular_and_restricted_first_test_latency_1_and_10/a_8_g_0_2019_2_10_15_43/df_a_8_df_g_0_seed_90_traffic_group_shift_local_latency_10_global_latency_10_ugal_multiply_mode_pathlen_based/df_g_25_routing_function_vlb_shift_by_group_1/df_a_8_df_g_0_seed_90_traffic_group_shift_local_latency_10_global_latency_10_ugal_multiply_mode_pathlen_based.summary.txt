vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = vlb;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.34852,   0.39945,    1488.0,    1100.5,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25008,    67.926,    67.927,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.35308,   0.36165,    1083.0,    966.18,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31006,   0.31007,    71.984,    71.982,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,   0.34013,   0.34014,    80.876,    80.877,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.35,   0.35011,    0.3501,    94.631,     94.62,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.36,    0.3545,   0.35936,    550.03,    537.26,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.30005,   0.30006,    70.837,    70.836,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.2,   0.20009,   0.20009,    66.573,    66.575,         0,  0,  0,  ,  ,  ,  
