vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = min;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.24918,   0.30101,    2513.7,    1676.2,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24945,   0.25005,     282.7,    285.64,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.24913,   0.30118,    1631.1,    1422.9,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,    0.2491,   0.30035,     980.0,    977.49,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.24905,   0.27994,    553.79,    553.78,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,   0.24983,   0.25982,    1011.1,    1010.4,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.20008,   0.20008,    36.383,    36.384,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.15,   0.14996,   0.14995,    35.111,    35.113,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.1,  0.099927,  0.099925,    34.644,    34.646,         0,  0,  0,  ,  ,  ,  
