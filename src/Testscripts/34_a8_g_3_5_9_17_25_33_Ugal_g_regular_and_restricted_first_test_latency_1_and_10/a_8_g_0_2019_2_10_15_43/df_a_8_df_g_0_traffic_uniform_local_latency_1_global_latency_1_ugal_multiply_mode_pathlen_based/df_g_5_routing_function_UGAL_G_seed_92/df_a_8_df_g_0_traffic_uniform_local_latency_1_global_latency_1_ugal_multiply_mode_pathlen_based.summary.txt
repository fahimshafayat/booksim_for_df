vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_G;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50011,    0.5001,    10.363,    10.363,         0,  4361035,  319693,  ,  ,  ,  
simulation,         2,      0.74,   0.74039,    0.7404,    12.218,    12.219,         0,  6623764,  305187,  ,  ,  ,  
simulation,         3,      0.86,   0.86048,   0.86047,    14.996,    14.996,         0,  7756252,  299449,  ,  ,  ,  
simulation,         4,      0.92,   0.92046,   0.92046,    18.619,    18.621,         0,  8312935,  305772,  ,  ,  ,  
simulation,         5,      0.95,   0.95036,   0.95034,    22.966,    22.971,         0,  8593008,  310324,  ,  ,  ,  
simulation,         6,      0.97,   0.97013,   0.97021,    30.414,    30.432,         0,  8781484,  317093,  ,  ,  ,  
simulation,         7,      0.98,   0.98016,   0.98011,     38.44,    38.474,         0,  8874057,  320416,  ,  ,  ,  
simulation,         9,       0.9,   0.90044,   0.90043,    16.948,    16.949,         0,  8129169,  303078,  ,  ,  ,  
simulation,        10,      0.85,    0.8504,    0.8504,    14.591,    14.592,         0,  7661384,  300549,  ,  ,  ,  
simulation,        11,       0.8,   0.80032,   0.80032,    13.259,    13.259,         0,  7189733,  301017,  ,  ,  ,  
