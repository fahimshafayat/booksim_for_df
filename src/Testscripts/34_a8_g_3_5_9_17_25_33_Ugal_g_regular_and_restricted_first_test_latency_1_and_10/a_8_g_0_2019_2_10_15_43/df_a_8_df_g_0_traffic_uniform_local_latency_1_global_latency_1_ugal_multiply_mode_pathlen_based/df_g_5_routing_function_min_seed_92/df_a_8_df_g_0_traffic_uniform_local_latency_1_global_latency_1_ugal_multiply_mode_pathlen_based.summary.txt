vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = min;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.5001,    0.5001,    10.025,    10.025,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,    0.7404,    0.7404,    12.021,    12.021,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.86047,   0.86047,     14.93,     14.93,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.92045,   0.92046,    18.759,     18.76,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.95031,   0.95034,    23.411,    23.414,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.97027,   0.97021,    30.593,    30.602,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.98015,   0.98011,     40.15,    40.157,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.90043,   0.90043,    17.048,    17.049,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,    0.8504,    0.8504,    14.522,    14.523,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,    0.8003,   0.80032,    13.115,    13.116,         0,  0,  0,  ,  ,  ,  
