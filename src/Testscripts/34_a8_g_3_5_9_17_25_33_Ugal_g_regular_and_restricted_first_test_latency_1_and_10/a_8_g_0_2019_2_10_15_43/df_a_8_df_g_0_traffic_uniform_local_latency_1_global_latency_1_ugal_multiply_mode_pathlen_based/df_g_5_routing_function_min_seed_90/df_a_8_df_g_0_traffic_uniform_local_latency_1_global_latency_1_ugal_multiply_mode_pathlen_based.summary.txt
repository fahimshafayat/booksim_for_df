vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = min;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    10.027,    10.027,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.74003,   0.74002,    12.025,    12.025,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.85991,   0.85991,    14.898,    14.899,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.91996,   0.91995,    18.706,    18.708,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.94993,   0.94988,    23.351,    23.354,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.96979,   0.96983,     30.27,    30.278,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.97993,   0.97984,    40.268,    40.281,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.89987,   0.89987,     17.02,     17.02,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,   0.84999,   0.84998,    14.514,    14.515,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.80004,   0.80003,    13.095,    13.096,         0,  0,  0,  ,  ,  ,  
