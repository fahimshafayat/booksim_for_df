vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = min;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49998,   0.49998,    11.011,    11.011,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.74005,   0.74005,    13.934,    13.934,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.85998,   0.85998,    18.808,    18.809,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.91998,   0.91999,    26.017,    26.021,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.94998,   0.94998,    34.873,     34.88,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.97001,   0.97001,    48.381,    48.397,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,      0.98,   0.98001,    63.017,    63.048,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.90001,   0.90001,    22.751,    22.753,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,      0.85,   0.85001,    18.121,    18.122,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.80001,   0.80001,     15.67,    15.671,         0,  0,  0,  ,  ,  ,  
