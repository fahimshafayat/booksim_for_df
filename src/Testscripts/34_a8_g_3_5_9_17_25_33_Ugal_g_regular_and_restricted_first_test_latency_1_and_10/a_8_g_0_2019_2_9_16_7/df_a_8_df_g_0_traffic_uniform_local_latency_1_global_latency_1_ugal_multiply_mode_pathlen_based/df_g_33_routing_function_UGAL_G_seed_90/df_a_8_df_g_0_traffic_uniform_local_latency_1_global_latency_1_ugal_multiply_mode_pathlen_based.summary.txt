vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = UGAL_G;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49998,   0.49998,    11.333,    11.334,         0,  29905702,  1664053,  ,  ,  ,  
simulation,         2,      0.74,   0.74002,   0.74003,    14.071,    14.072,         0,  45230669,  1506157,  ,  ,  ,  
simulation,         3,      0.86,   0.85999,   0.85998,    18.548,    18.548,         0,  52758210,  1563458,  ,  ,  ,  
simulation,         4,      0.92,   0.92005,   0.92004,    24.811,    24.812,         0,  56477355,  1666888,  ,  ,  ,  
simulation,         5,      0.95,   0.95005,   0.95005,    32.282,    32.288,         0,  58338462,  1727978,  ,  ,  ,  
simulation,         6,      0.97,   0.97001,   0.97003,    43.542,    43.555,         0,  59646860,  1748163,  ,  ,  ,  
simulation,         7,      0.98,   0.98003,   0.98001,    55.733,    55.756,         0,  60380221,  1742557,  ,  ,  ,  
simulation,         9,       0.9,   0.90001,   0.90001,    22.019,     22.02,         0,  55238599,  1624302,  ,  ,  ,  
simulation,        10,      0.85,   0.84996,   0.84996,    17.936,    17.937,         0,  52142641,  1550425,  ,  ,  ,  
simulation,        11,       0.8,   0.79998,   0.79998,    15.712,    15.712,         0,  49017834,  1511632,  ,  ,  ,  
