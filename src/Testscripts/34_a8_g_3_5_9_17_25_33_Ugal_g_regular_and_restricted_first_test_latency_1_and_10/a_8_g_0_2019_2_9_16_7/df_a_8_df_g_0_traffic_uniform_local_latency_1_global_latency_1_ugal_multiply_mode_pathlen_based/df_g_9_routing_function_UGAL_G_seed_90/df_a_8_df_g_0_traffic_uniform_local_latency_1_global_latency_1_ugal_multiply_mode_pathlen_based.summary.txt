vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = UGAL_G;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49979,   0.49978,    10.859,    10.859,         0,  8009356,  513022,  ,  ,  ,  
simulation,         2,      0.74,   0.74029,   0.74028,    13.102,    13.102,         0,  12141739,  476390,  ,  ,  ,  
simulation,         3,      0.86,    0.8601,    0.8601,    16.471,    16.472,         0,  14185003,  479928,  ,  ,  ,  
simulation,         4,      0.92,   0.92006,   0.92006,    20.876,    20.877,         0,  15191759,  501566,  ,  ,  ,  
simulation,         5,      0.95,   0.95003,   0.95001,    26.042,    26.044,         0,  15688386,  518527,  ,  ,  ,  
simulation,         6,      0.97,   0.96993,   0.96995,     33.77,    33.778,         0,  16027531,  534620,  ,  ,  ,  
simulation,         7,      0.98,      0.98,   0.97997,    42.953,    42.973,         0,  16196681,  542438,  ,  ,  ,  
simulation,         9,       0.9,   0.90007,   0.90005,     18.94,     18.94,         0,  14863068,  491026,  ,  ,  ,  
simulation,        10,      0.85,   0.85015,   0.85015,    16.026,    16.026,         0,  14018266,  477410,  ,  ,  ,  
simulation,        11,       0.8,   0.80023,   0.80024,    14.367,    14.367,         0,  13170941,  473275,  ,  ,  ,  
