vc_alloc_delay = 1;
st_final_delay = 1;
credit_delay = 2;
vc_buf_size = 64;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
vc_allocator = separable_input_first;
packet_size = 1;
routing_delay = 0;
sim_count = 1;
wait_for_tail_credit = 0;
sample_period = 10000;
alloc_iters = 1;
output_speedup = 1;
sw_alloc_delay = 1;
sw_allocator = separable_input_first;
num_vcs = 7;
warmup_periods = 3;
priority = none;
input_speedup = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = vlb;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.38438,   0.43622,    1149.9,    949.44,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24986,   0.24986,     65.87,    65.872,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36987,   0.36988,    81.984,    81.988,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.38449,   0.42614,    533.16,    530.82,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.38016,     0.387,    1092.1,    915.04,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.38,   0.37982,   0.37987,    101.15,     101.2,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.39,   0.38167,   0.38604,    673.66,     638.8,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.35,   0.34986,   0.34986,    73.473,    73.477,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.29983,   0.29983,    67.852,    67.855,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.2,   0.19987,   0.19988,    64.781,    64.784,         0,  0,  0,  ,  ,  ,  
