vc_alloc_delay = 1;
st_final_delay = 1;
credit_delay = 2;
vc_buf_size = 64;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
vc_allocator = separable_input_first;
packet_size = 1;
routing_delay = 0;
sim_count = 1;
wait_for_tail_credit = 0;
sample_period = 10000;
alloc_iters = 1;
output_speedup = 1;
sw_alloc_delay = 1;
sw_allocator = separable_input_first;
num_vcs = 7;
warmup_periods = 3;
priority = none;
input_speedup = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_G;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49997,   0.49997,    34.703,    34.705,         0,  16055158,  162778,  ,  ,  ,  
simulation,         2,      0.74,   0.73993,   0.73994,    37.333,    37.334,         0,  23853954,  149034,  ,  ,  ,  
simulation,         3,      0.86,   0.85986,   0.85987,    41.566,    41.568,         0,  27736090,  161075,  ,  ,  ,  
simulation,         4,      0.92,    0.9199,   0.91991,    47.334,    47.336,         0,  29666745,  185772,  ,  ,  ,  
simulation,         5,      0.95,   0.94994,   0.94994,    53.987,    53.992,         0,  30631380,  215162,  ,  ,  ,  
simulation,         6,      0.97,   0.96996,   0.96994,    63.798,    63.806,         0,  31289993,  246812,  ,  ,  ,  
simulation,         7,      0.98,   0.98001,   0.98001,    74.467,    74.498,         0,  31635737,  271048,  ,  ,  ,  
simulation,         9,       0.9,   0.89988,   0.89989,    44.755,    44.758,         0,  29026972,  173577,  ,  ,  ,  
simulation,        10,      0.85,   0.84987,   0.84988,    40.998,      41.0,         0,  27412632,  158762,  ,  ,  ,  
simulation,        11,       0.8,   0.79983,   0.79983,    38.888,     38.89,         0,  25794929,  152497,  ,  ,  ,  
