vc_alloc_delay = 1;
st_final_delay = 1;
credit_delay = 2;
vc_buf_size = 64;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
vc_allocator = separable_input_first;
packet_size = 1;
routing_delay = 0;
sim_count = 1;
wait_for_tail_credit = 0;
sample_period = 10000;
alloc_iters = 1;
output_speedup = 1;
sw_alloc_delay = 1;
sw_allocator = separable_input_first;
num_vcs = 7;
warmup_periods = 3;
priority = none;
input_speedup = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = UGAL_L_restricted;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50017,   0.50017,    29.062,    29.066,         0,  2263420,  499170,  ,  ,  ,  
simulation,         2,      0.74,   0.73996,   0.73994,    30.347,    30.351,         0,  3439231,  649856,  ,  ,  ,  
simulation,         3,      0.86,   0.86001,   0.86001,    32.379,    32.382,         0,  4035257,  716889,  ,  ,  ,  
simulation,         4,      0.92,   0.92006,   0.92005,    35.396,      35.4,         0,  4336906,  748607,  ,  ,  ,  
simulation,         5,      0.95,   0.94998,   0.95001,    39.315,    39.322,         0,  4490045,  764009,  ,  ,  ,  
simulation,         6,      0.97,   0.97004,   0.97002,    46.356,     46.37,         0,  4592345,  773424,  ,  ,  ,  
simulation,         7,      0.98,   0.98001,   0.98005,    55.579,    55.605,         0,  4642279,  779764,  ,  ,  ,  
simulation,         9,       0.9,   0.90011,   0.90009,     34.06,    34.065,         0,  4236228,  737999,  ,  ,  ,  
simulation,        10,      0.85,   0.85006,   0.85006,    32.099,    32.103,         0,  3986027,  710398,  ,  ,  ,  
simulation,        11,       0.8,    0.8001,    0.8001,    31.103,    31.107,         0,  3736679,  684087,  ,  ,  ,  
