vc_alloc_delay = 1;
st_final_delay = 1;
credit_delay = 2;
vc_buf_size = 64;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
vc_allocator = separable_input_first;
packet_size = 1;
routing_delay = 0;
sim_count = 1;
wait_for_tail_credit = 0;
sample_period = 10000;
alloc_iters = 1;
output_speedup = 1;
sw_alloc_delay = 1;
sw_allocator = separable_input_first;
num_vcs = 7;
warmup_periods = 3;
priority = none;
input_speedup = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = UGAL_L;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50018,   0.50017,    29.638,    29.643,         0,  2596396,  166459,  ,  ,  ,  
simulation,         2,      0.74,   0.73996,   0.73994,    30.822,    30.828,         0,  3921728,  167584,  ,  ,  ,  
simulation,         3,      0.86,   0.86001,   0.86001,    33.029,    33.034,         0,  4583947,  169684,  ,  ,  ,  
simulation,         4,      0.92,   0.92007,   0.92005,    36.143,    36.148,         0,  4913119,  173470,  ,  ,  ,  
simulation,         5,      0.95,   0.94999,   0.95001,    40.389,    40.399,         0,  5076961,  175155,  ,  ,  ,  
simulation,         6,      0.97,   0.96999,   0.97002,    46.844,    46.858,         0,  5190106,  178399,  ,  ,  ,  
simulation,         7,      0.98,   0.98004,   0.98005,     55.13,    55.151,         0,  5245106,  179888,  ,  ,  ,  
simulation,         9,       0.9,   0.90008,   0.90009,    34.799,    34.803,         0,  4803518,  171756,  ,  ,  ,  
simulation,        10,      0.85,   0.85008,   0.85006,    32.763,    32.767,         0,  4529218,  169557,  ,  ,  ,  
simulation,        11,       0.8,   0.80011,    0.8001,    31.653,    31.658,         0,  4253901,  168611,  ,  ,  ,  
