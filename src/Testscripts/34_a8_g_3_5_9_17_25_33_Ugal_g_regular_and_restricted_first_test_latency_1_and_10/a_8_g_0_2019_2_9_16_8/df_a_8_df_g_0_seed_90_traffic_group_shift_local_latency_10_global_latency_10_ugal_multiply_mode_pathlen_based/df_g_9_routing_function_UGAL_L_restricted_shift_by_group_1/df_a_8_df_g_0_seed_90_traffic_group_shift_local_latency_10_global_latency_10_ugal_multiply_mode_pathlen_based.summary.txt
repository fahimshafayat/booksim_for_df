vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = UGAL_L_restricted;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.47883,   0.47888,    893.92,    370.81,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24997,   0.24997,    67.152,     67.16,         0,  2166090,  2163859,  ,  ,  ,  
simulation,         3,      0.37,   0.36994,   0.36992,    89.103,    89.209,         0,  2175155,  4249561,  ,  ,  ,  
simulation,         4,      0.43,   0.42994,   0.42991,    100.72,    100.92,         0,  2181290,  5299094,  ,  ,  ,  
simulation,         5,      0.46,    0.4576,    0.4577,    412.64,    183.56,         0,  4139003,  10989532,  ,  ,  ,  
simulation,         6,      0.48,   0.47151,   0.47189,     527.9,    233.68,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,    0.4647,   0.46489,    490.85,    218.48,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44965,   0.44971,    146.31,    139.67,         0,  2308744,  5967119,  ,  ,  ,  
simulation,         9,       0.4,   0.39987,   0.39984,    94.334,    94.475,         0,  2176381,  4769297,  ,  ,  ,  
simulation,        10,      0.35,    0.3499,   0.34989,    85.756,    85.835,         0,  2172868,  3898775,  ,  ,  ,  
simulation,        11,       0.3,   0.29991,   0.29991,    77.121,    77.158,         0,  2169758,  3029930,  ,  ,  ,  
