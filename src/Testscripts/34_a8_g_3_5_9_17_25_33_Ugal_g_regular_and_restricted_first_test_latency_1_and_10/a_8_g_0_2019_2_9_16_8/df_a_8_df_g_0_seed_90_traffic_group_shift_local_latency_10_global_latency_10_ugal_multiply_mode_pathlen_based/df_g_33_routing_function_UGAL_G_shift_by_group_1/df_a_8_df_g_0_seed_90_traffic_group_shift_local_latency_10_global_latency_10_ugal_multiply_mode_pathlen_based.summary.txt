vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = UGAL_G;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.40644,   0.44201,    591.84,    473.81,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25007,   0.25006,    65.391,    65.393,         0,  1983781,  13883044,  ,  ,  ,  
simulation,         3,      0.37,   0.36825,   0.36825,    427.85,    122.84,         0,  3877831,  41688866,  ,  ,  ,  
simulation,         4,      0.43,   0.42289,   0.42288,    516.49,    124.43,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.39555,   0.39555,     484.6,    121.99,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.38,   0.37731,   0.37732,    450.63,    121.89,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.35,   0.34996,   0.35006,    84.715,    84.846,         0,  2040485,  20754359,  ,  ,  ,  
simulation,         8,       0.3,   0.30006,   0.30005,    67.917,     67.92,         0,  1984649,  17054273,  ,  ,  ,  
simulation,        10,       0.2,   0.20009,   0.20009,    62.525,     62.53,         0,  1982884,  10709426,  ,  ,  ,  
