vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 9;
perm_seed = 60;
routing_function = UGAL_G;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4998,   0.49978,    34.605,    34.608,         0,  7954893,  603820,  ,  ,  ,  
simulation,         2,      0.74,    0.7403,   0.74028,     42.84,    42.844,         0,  11428657,  1245788,  ,  ,  ,  
simulation,         3,      0.86,   0.84298,   0.84313,    534.89,    134.39,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.80026,   0.80024,    53.064,    53.073,         0,  11980988,  1727718,  ,  ,  ,  
simulation,         5,      0.83,   0.82724,   0.82724,    321.22,    76.177,         0,  26342675,  4481345,  ,  ,  ,  
simulation,         6,      0.84,   0.83417,    0.8342,    400.35,    93.875,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.75,   0.75029,   0.75028,    43.984,    43.988,         0,  11535226,  1311214,  ,  ,  ,  
simulation,         9,       0.7,   0.70019,   0.70017,    39.515,    39.517,         0,  10954378,  1032552,  ,  ,  ,  
simulation,        10,      0.65,   0.65006,   0.65004,    37.135,    37.138,         0,  10273033,  854674,  ,  ,  ,  
