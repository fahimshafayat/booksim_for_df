vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 27;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L;
routing_threshold = 0;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010038,  0.010039,    10.786,    10.786,         0
simulation,         2,      0.99,   0.17678,   0.41808,    620.88,    619.84,         1
simulation,         3,       0.5,   0.20027,   0.43994,    363.59,    363.58,         1
simulation,         4,      0.25,   0.15543,   0.24967,    476.68,    476.68,         1
simulation,         5,      0.13,   0.12999,   0.13002,    60.834,    61.891,         0
simulation,         6,      0.19,   0.16006,   0.18996,    474.65,    491.29,         1
simulation,         7,      0.16,   0.15712,   0.16004,    166.42,    244.67,         0
simulation,         8,      0.17,   0.15921,   0.17001,    376.24,    678.05,         0
simulation,         9,      0.18,    0.1603,      0.18,    434.98,    440.96,         1
simulation,        10,      0.15,   0.14975,   0.15001,    82.386,    84.821,         0
simulation,        11,       0.1,       0.1,   0.10001,    53.185,    53.859,         0
simulation,        12,      0.05,  0.050124,  0.050113,     14.64,     14.66,         0
