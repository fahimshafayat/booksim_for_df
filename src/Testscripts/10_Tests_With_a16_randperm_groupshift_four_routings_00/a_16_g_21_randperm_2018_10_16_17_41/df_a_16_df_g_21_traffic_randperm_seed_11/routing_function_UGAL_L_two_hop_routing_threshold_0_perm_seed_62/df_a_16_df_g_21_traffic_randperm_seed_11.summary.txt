vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 62;
routing_function = UGAL_L_two_hop;
routing_threshold = 0;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010003,  0.010006,    9.9369,     9.939,         0
simulation,         2,      0.99,   0.68907,   0.71423,    612.92,    531.25,         1
simulation,         3,       0.5,   0.49974,   0.49978,    14.702,    14.726,         0
simulation,         4,      0.74,   0.69303,   0.69891,    311.22,    219.94,         1
simulation,         5,      0.62,    0.6191,   0.61958,    34.932,    33.486,         0
simulation,         6,      0.68,   0.66973,   0.67162,     179.7,    94.879,         0
simulation,         7,      0.71,   0.68464,   0.68858,     380.9,     164.8,         0
simulation,         8,      0.72,   0.68798,   0.69284,     226.3,    170.34,         1
simulation,         9,       0.7,   0.68105,   0.68393,    300.69,    140.21,         0
simulation,        10,      0.65,   0.64607,   0.64687,    83.444,    53.661,         0
simulation,        11,       0.6,   0.59945,   0.59969,     25.03,    24.995,         0
simulation,        12,      0.55,   0.54966,   0.54975,     17.14,    17.209,         0
