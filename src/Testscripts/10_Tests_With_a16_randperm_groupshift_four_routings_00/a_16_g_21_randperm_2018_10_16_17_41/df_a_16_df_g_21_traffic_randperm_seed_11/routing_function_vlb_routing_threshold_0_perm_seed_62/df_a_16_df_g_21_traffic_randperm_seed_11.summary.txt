vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 62;
routing_function = vlb;
routing_threshold = 0;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,      0.01,  0.010006,    15.231,    15.233,         0
simulation,         2,      0.99,   0.47253,   0.73971,    617.33,    615.12,         1
simulation,         3,       0.5,   0.47321,   0.49625,    379.72,    438.93,         0
simulation,         4,      0.74,   0.49004,   0.46894,    863.28,    850.49,         1
simulation,         5,      0.62,    0.4749,   0.52583,    491.97,     769.5,         1
simulation,         6,      0.56,   0.47785,   0.54303,     520.8,    556.33,         1
simulation,         7,      0.53,   0.47401,   0.51454,    541.55,    523.49,         1
simulation,         8,      0.51,   0.47378,   0.50167,    435.16,    415.07,         1
simulation,        10,      0.45,   0.44972,   0.44972,     38.51,    38.534,         0
simulation,        11,       0.4,   0.39978,   0.39975,    25.482,    25.487,         0
simulation,        12,      0.35,    0.3497,   0.34966,     21.31,    21.313,         0
