packet_size = 1;
st_final_delay = 1;
priority = none;
credit_delay = 2;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
output_speedup = 1;
vc_alloc_delay = 1;
vc_buf_size = 64;
alloc_iters = 1;
injection_rate_uses_flits = 1;
warmup_periods = 3;
sw_alloc_delay = 1;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
routing_delay = 0;
internal_speedup = 4.0;
input_speedup = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L;
routing_threshold = 0;
shift_by_group = 12;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010043,  0.010042,    11.002,    11.003,         0
simulation,         2,      0.99,   0.17453,   0.41052,    610.49,     606.6,         1
simulation,         3,       0.5,   0.20831,   0.43975,    331.78,    331.76,         1
simulation,         4,      0.25,   0.15556,   0.24963,    165.74,     713.5,         1
simulation,         5,      0.13,   0.13006,   0.13012,    55.533,    56.591,         0
simulation,         6,      0.19,   0.16151,   0.18987,    417.71,    435.22,         1
simulation,         7,      0.16,   0.15615,   0.16004,    186.72,    334.41,         0
simulation,         8,      0.17,   0.15995,   0.17001,    236.14,    241.47,         1
simulation,         9,      0.15,   0.14897,   0.15008,     99.63,    141.11,         0
simulation,        10,       0.1,   0.10013,   0.10014,    48.758,    49.368,         0
simulation,        11,      0.05,  0.050129,  0.050139,    33.038,    33.199,         0
