packet_size = 1;
st_final_delay = 1;
priority = none;
credit_delay = 2;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
output_speedup = 1;
vc_alloc_delay = 1;
vc_buf_size = 64;
alloc_iters = 1;
injection_rate_uses_flits = 1;
warmup_periods = 3;
sw_alloc_delay = 1;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
routing_delay = 0;
internal_speedup = 4.0;
input_speedup = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L_two_hop;
routing_threshold = 0;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010043,  0.010042,    10.922,    10.924,         0
simulation,         2,      0.99,   0.16247,   0.39673,    661.84,    652.72,         1
simulation,         3,       0.5,   0.21919,   0.44743,     338.7,    338.64,         1
simulation,         4,      0.25,   0.18188,   0.24694,    103.62,    460.45,         1
simulation,         5,      0.13,   0.12887,   0.13008,    103.91,    158.08,         0
simulation,         6,      0.19,   0.16783,   0.18551,    404.63,     427.8,         1
simulation,         7,      0.16,   0.15265,   0.15967,    185.87,    193.73,         1
simulation,         8,      0.14,   0.13753,   0.14008,    163.48,    235.48,         0
simulation,         9,      0.15,   0.14562,      0.15,    246.89,    357.48,         0
simulation,        11,       0.1,   0.10013,   0.10014,    48.582,    49.109,         0
simulation,        12,      0.05,  0.050093,  0.050139,    30.704,    30.846,         0
