packet_size = 1;
st_final_delay = 1;
priority = none;
credit_delay = 2;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
output_speedup = 1;
vc_alloc_delay = 1;
vc_buf_size = 64;
alloc_iters = 1;
injection_rate_uses_flits = 1;
warmup_periods = 3;
sw_alloc_delay = 1;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
routing_delay = 0;
internal_speedup = 4.0;
input_speedup = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L;
routing_threshold = 0;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010044,  0.010042,    10.978,    10.979,         0
simulation,         2,      0.99,   0.17378,   0.40978,     612.9,    609.08,         1
simulation,         3,       0.5,   0.20928,   0.44135,    333.47,    333.45,         1
simulation,         4,      0.25,   0.15645,   0.24979,     167.7,    707.71,         1
simulation,         5,      0.13,   0.13009,   0.13012,    54.766,    55.609,         0
simulation,         6,      0.19,   0.16223,   0.18978,    476.07,    487.54,         1
simulation,         7,      0.16,   0.15695,   0.16004,    163.51,    290.17,         0
simulation,         8,      0.17,   0.16088,   0.17002,    322.63,    683.59,         0
simulation,         9,      0.18,   0.16172,   0.17993,     374.2,    380.57,         1
simulation,        10,      0.15,   0.14925,   0.15011,     87.71,    110.62,         0
simulation,        11,       0.1,   0.10012,   0.10014,    49.009,    49.609,         0
simulation,        12,      0.05,  0.050091,  0.050139,    31.999,    32.203,         0
