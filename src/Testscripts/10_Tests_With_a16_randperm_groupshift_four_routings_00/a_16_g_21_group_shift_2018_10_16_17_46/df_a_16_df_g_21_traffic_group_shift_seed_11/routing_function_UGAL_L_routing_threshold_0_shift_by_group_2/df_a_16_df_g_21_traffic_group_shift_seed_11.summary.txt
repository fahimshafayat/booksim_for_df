vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L;
routing_threshold = 0;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010003,  0.010006,     10.61,    10.612,         0
simulation,         2,      0.99,   0.17608,   0.42419,    637.49,     637.0,         1
simulation,         3,       0.5,   0.19034,   0.43776,    402.31,    402.31,         1
simulation,         4,      0.25,   0.14839,   0.24999,    583.85,    583.85,         1
simulation,         5,      0.13,   0.12993,   0.12993,    67.284,    68.196,         0
simulation,         6,      0.19,   0.15582,   0.18988,    480.95,    508.95,         1
simulation,         7,      0.16,    0.1564,   0.15994,    198.87,    304.34,         0
simulation,         8,      0.17,   0.15899,   0.16992,    306.01,    310.73,         1
simulation,         9,      0.15,   0.14952,   0.14991,    98.937,    101.71,         0
simulation,        10,       0.1,  0.099908,  0.099889,    54.182,    54.647,         0
simulation,        11,      0.05,  0.049976,  0.049976,    12.377,    12.379,         0
