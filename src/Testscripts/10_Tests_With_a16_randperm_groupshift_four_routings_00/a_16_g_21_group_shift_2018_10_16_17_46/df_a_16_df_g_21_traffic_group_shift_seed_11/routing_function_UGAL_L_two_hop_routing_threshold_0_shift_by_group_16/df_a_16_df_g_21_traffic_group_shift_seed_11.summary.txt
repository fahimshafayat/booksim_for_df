vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = UGAL_L_two_hop;
routing_threshold = 0;
shift_by_group = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010003,  0.010006,    10.626,    10.627,         0
simulation,         2,      0.99,   0.17551,   0.42197,    653.34,    652.42,         1
simulation,         3,       0.5,   0.19864,   0.44364,    388.89,    388.89,         1
simulation,         4,      0.25,   0.15749,   0.24999,    504.07,    504.07,         1
simulation,         5,      0.13,   0.12994,   0.12993,    64.709,    65.609,         0
simulation,         6,      0.19,   0.15951,   0.18986,    426.49,    457.43,         1
simulation,         7,      0.16,   0.15563,   0.15994,    218.94,    356.84,         0
simulation,         8,      0.17,    0.1589,   0.16991,    285.94,    290.46,         1
simulation,         9,      0.15,   0.14896,   0.14991,    102.66,    124.91,         0
simulation,        10,       0.1,   0.09991,  0.099889,    53.725,    54.198,         0
simulation,        11,      0.05,  0.049979,  0.049976,    12.407,    12.408,         0
