vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 27;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 66;
routing_function = vlb;
routing_threshold = 0;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010039,  0.010039,    15.257,    15.258,         0
simulation,         2,      0.99,   0.45043,   0.68728,    572.75,     567.4,         1
simulation,         3,       0.5,   0.42159,   0.47473,    527.57,    546.62,         1
simulation,         4,      0.25,   0.25004,   0.25003,     17.91,    17.911,         0
simulation,         5,      0.37,   0.36993,   0.36992,    22.685,    22.687,         0
simulation,         6,      0.43,   0.42133,   0.42974,    116.01,    179.62,         0
simulation,         7,      0.46,   0.41817,   0.44794,    405.55,    384.91,         1
simulation,         8,      0.44,   0.41897,   0.43783,    255.04,    336.71,         0
simulation,         9,      0.45,   0.41713,   0.44278,    319.81,    310.22,         1
simulation,        11,       0.4,   0.39998,   0.39999,    26.211,    26.221,         0
simulation,        12,      0.35,    0.3499,   0.34991,    21.373,    21.373,         0
simulation,        13,       0.3,   0.29997,   0.29999,    19.224,    19.224,         0
