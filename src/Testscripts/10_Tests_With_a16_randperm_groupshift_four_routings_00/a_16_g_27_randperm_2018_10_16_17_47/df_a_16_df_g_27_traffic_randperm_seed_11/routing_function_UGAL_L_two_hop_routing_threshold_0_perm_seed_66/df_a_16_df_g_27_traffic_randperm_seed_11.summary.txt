vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 27;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 66;
routing_function = UGAL_L_two_hop;
routing_threshold = 0;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010038,  0.010039,    9.9682,    9.9685,         0
simulation,         2,      0.99,    0.6577,   0.67878,    668.16,    581.03,         1
simulation,         3,       0.5,   0.50016,   0.50016,    14.729,    14.752,         0
simulation,         4,      0.74,   0.66867,   0.67696,    467.39,    320.74,         1
simulation,         5,      0.62,    0.6198,   0.62006,    31.972,    32.292,         0
simulation,         6,      0.68,     0.663,   0.66762,    257.62,    157.12,         0
simulation,         7,      0.71,   0.66811,   0.67529,    303.47,    228.62,         1
simulation,         8,      0.69,   0.66498,   0.67059,    201.87,    165.07,         1
simulation,         9,      0.65,   0.64714,   0.64863,    73.807,    65.017,         0
simulation,        10,       0.6,   0.59998,   0.60007,    26.073,    26.259,         0
simulation,        11,      0.55,   0.55008,   0.55012,    18.298,    18.373,         0
