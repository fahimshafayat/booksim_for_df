vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
perm_seed = 70;
routing_function = UGAL_L;
routing_threshold = 0;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010045,  0.010042,    10.045,    10.045,         0
simulation,         2,      0.99,    0.6329,   0.64935,    710.16,    625.12,         1
simulation,         3,       0.5,   0.50024,   0.50027,    17.844,    17.916,         0
simulation,         4,      0.74,   0.63892,   0.65317,    401.05,    387.32,         1
simulation,         5,      0.62,   0.61789,   0.61934,    66.579,    62.818,         0
simulation,         6,      0.68,   0.63687,   0.64444,    316.21,    245.94,         1
simulation,         7,      0.65,   0.63243,   0.63729,    277.36,    164.76,         0
simulation,         8,      0.66,   0.63465,   0.64048,    384.17,    207.17,         0
simulation,         9,      0.67,   0.63587,   0.64282,    262.95,    212.95,         1
simulation,        11,       0.6,   0.60006,    0.6003,    37.494,    37.856,         0
simulation,        12,      0.55,   0.55022,   0.55032,    23.258,    23.419,         0
