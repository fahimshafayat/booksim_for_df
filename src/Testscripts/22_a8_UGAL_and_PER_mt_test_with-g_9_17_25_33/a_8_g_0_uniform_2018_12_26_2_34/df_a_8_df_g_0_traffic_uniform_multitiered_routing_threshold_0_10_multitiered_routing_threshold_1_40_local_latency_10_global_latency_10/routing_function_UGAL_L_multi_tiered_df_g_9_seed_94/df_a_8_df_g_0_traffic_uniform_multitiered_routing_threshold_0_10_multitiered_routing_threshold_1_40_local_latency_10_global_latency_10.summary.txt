vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 9;
routing_function = UGAL_L_multi_tiered;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50014,   0.50013,    34.042,    34.044,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.74021,   0.74021,     36.12,    36.123,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.86017,   0.86017,    39.558,    39.561,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.92006,   0.92007,    44.129,    44.132,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.95009,   0.95006,    49.437,    49.442,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.97006,   0.97002,     58.01,    58.015,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.98004,   0.98002,    67.102,    67.136,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.90013,   0.90014,    42.116,    42.118,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,   0.85014,   0.85015,     39.07,    39.073,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.80018,   0.80019,    37.383,    37.385,         0,  0,  0,  ,  ,  ,  
