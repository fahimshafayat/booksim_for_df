vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 17;
routing_function = vlb;
seed = 96;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49998,   0.50003,    129.95,    129.97,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,    0.5033,   0.56125,    1677.9,    1018.4,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,    0.5039,    0.5619,    1019.3,    877.09,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.50332,    0.5539,    583.62,    578.75,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.50536,   0.52799,    520.12,    510.11,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.50745,   0.50954,    488.21,    459.45,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.52,   0.50782,   0.51703,    644.53,    618.71,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.45,   0.45003,   0.45003,    78.319,     78.32,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.4,   0.40006,   0.40006,    70.562,    70.566,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.35,   0.35002,   0.35002,    67.354,    67.358,         0,  0,  0,  ,  ,  ,  
