vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 17;
routing_function = PAR_multi_tiered;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49989,   0.49989,    36.228,     36.23,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.74004,   0.74004,    38.186,    38.188,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.85999,   0.85999,    42.314,    42.318,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.91991,   0.91991,    48.264,    48.267,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.94998,   0.94998,    55.469,    55.474,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.96999,   0.97001,    66.367,    66.374,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.98004,      0.98,     78.03,    78.055,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.89992,   0.89992,     45.59,    45.593,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,   0.85004,   0.85004,    41.716,    41.719,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.80003,   0.80004,    39.654,    39.657,         0,  0,  0,  ,  ,  ,  
