vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 17;
routing_function = UGAL_L;
seed = 96;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50002,   0.50003,    35.611,    35.613,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.73987,   0.73987,    37.827,    37.828,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.85999,   0.85999,    42.027,    42.029,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.92002,   0.92002,    47.996,    47.999,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.95001,   0.95001,    55.183,    55.188,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.96999,   0.97002,    65.886,    65.901,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.97999,   0.97999,    77.619,    77.649,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.90004,   0.90003,     45.34,    45.343,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,   0.84995,   0.84995,    41.451,    41.453,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.79986,   0.79986,     39.33,    39.333,         0,  0,  0,  ,  ,  ,  
