vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 9;
routing_function = PAR;
seed = 96;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50017,   0.50016,    35.324,    35.328,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.73982,   0.73981,    36.838,    36.841,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,      0.86,   0.86001,    40.282,    40.285,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.91991,   0.91994,    45.049,    45.052,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.94997,   0.94998,    50.808,    50.815,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.96998,   0.97005,    59.443,    59.458,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.98008,   0.98005,    68.559,    68.581,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.89992,   0.89994,    42.955,     42.96,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,      0.85,   0.84999,    39.786,    39.789,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.79994,   0.79993,    38.093,    38.097,         0,  0,  0,  ,  ,  ,  
