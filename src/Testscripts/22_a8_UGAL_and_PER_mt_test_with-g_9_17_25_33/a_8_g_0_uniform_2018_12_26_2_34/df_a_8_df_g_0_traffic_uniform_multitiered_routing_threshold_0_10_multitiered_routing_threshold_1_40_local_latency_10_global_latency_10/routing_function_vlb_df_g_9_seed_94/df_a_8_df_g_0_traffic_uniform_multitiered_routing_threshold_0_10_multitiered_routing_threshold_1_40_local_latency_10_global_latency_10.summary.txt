vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 9;
routing_function = vlb;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.5002,   0.50013,    94.025,    94.027,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,    0.5189,   0.57931,    1576.2,    1000.6,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.51865,   0.57879,    897.46,    808.97,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.52197,   0.54935,    745.54,    698.76,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.52514,   0.53062,    532.06,    527.56,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.51019,   0.51015,    110.78,    110.76,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.52,   0.52015,   0.52011,    158.65,    158.64,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.45,   0.45019,   0.45016,    72.401,    72.403,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.4,   0.40029,   0.40028,    67.054,     67.06,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.35,   0.35023,   0.35021,    64.505,    64.511,         0,  0,  0,  ,  ,  ,  
