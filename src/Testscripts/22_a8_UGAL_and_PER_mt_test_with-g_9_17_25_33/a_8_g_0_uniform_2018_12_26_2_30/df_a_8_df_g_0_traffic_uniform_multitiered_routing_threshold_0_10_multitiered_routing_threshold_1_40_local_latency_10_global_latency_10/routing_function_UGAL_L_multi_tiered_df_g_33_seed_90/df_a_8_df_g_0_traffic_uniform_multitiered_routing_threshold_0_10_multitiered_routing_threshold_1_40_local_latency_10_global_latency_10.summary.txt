vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 33;
routing_function = UGAL_L_multi_tiered;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49998,   0.49998,    36.059,    36.061,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.74003,   0.74003,    38.573,    38.575,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.85999,   0.85998,    43.285,    43.287,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.92004,   0.92004,    50.391,    50.394,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.95005,   0.95005,    59.492,    59.495,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.97005,   0.97003,    74.665,     74.68,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.97999,   0.98001,    92.875,    92.906,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,       0.9,   0.90001,    47.145,    47.148,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,   0.84997,   0.84996,    42.611,    42.612,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.79997,   0.79998,    40.249,     40.25,         0,  0,  0,  ,  ,  ,  
