vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 33;
routing_function = PAR;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49998,   0.49998,     37.31,    37.314,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.74004,   0.74005,    39.317,     39.32,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.85999,   0.85998,    44.111,    44.114,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,      0.92,   0.91999,    51.698,    51.704,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.94998,   0.94998,    61.873,     61.88,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.97001,   0.97001,    79.466,    79.483,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.97997,   0.98001,    101.47,     101.5,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.90002,   0.90001,    48.205,    48.209,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,   0.85001,   0.85001,    43.412,    43.414,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,       0.8,   0.80001,    40.965,    40.968,         0,  0,  0,  ,  ,  ,  
