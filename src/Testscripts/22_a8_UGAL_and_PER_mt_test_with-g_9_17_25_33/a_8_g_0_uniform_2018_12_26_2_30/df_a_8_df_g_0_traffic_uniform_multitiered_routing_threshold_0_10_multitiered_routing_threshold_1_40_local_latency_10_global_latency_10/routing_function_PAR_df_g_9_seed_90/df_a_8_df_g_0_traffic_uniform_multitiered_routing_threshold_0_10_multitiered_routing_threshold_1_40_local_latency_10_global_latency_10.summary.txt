vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 9;
routing_function = PAR;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49979,   0.49978,    35.314,    35.318,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.74029,   0.74028,    36.843,    36.845,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.86011,    0.8601,    40.257,    40.261,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.92008,   0.92006,    45.094,    45.097,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.95001,   0.95001,    50.726,    50.734,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.96993,   0.96995,    59.331,    59.343,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.97997,   0.97997,    69.368,    69.393,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.90007,   0.90005,     42.94,    42.943,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,   0.85014,   0.85015,    39.815,    39.818,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.80024,   0.80024,    38.064,    38.068,         0,  0,  0,  ,  ,  ,  
