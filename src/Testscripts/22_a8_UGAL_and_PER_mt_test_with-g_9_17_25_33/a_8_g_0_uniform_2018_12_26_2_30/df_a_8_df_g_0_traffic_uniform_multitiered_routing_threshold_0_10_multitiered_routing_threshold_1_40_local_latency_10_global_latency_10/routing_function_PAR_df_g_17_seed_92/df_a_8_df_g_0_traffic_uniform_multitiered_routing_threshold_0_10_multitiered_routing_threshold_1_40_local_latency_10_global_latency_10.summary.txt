vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 17;
routing_function = PAR;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49988,   0.49985,     36.59,    36.594,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.73987,   0.73986,    38.401,    38.403,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.85996,   0.85996,    42.567,    42.571,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.91995,   0.91996,    48.791,    48.793,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.94997,   0.94998,    56.316,    56.326,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.96998,   0.96999,    67.476,    67.496,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.98001,   0.98001,    80.015,    80.031,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,       0.9,   0.90001,     46.01,    46.014,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,   0.84996,   0.84996,    42.015,    42.018,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.79991,   0.79991,    39.883,    39.885,         0,  0,  0,  ,  ,  ,  
