vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 33;
routing_function = min;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49998,   0.49998,    35.248,    35.249,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.74003,   0.74003,    38.162,    38.163,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.85999,   0.85998,    43.048,    43.049,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.92005,   0.92004,    50.261,    50.264,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.95007,   0.95005,    59.147,    59.156,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.97001,   0.97003,    72.895,     72.91,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.98006,   0.98001,    87.493,    87.519,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,       0.9,   0.90001,     46.98,    46.982,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,   0.84996,   0.84996,    42.355,    42.356,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.79997,   0.79998,    39.913,    39.914,         0,  0,  0,  ,  ,  ,  
