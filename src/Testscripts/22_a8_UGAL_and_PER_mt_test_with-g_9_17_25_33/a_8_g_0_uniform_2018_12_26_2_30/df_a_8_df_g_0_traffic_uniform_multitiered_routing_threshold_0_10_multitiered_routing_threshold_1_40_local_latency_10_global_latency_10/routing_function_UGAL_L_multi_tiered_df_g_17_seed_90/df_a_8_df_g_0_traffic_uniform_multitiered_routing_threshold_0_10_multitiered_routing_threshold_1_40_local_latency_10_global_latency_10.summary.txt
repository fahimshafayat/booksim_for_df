vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
topology = dragonflyfull;
traffic = uniform;
df_g = 17;
routing_function = UGAL_L_multi_tiered;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49997,   0.49997,    35.345,    35.347,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.73994,   0.73994,    37.672,    37.674,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.85987,   0.85987,    41.807,    41.809,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.91991,   0.91991,     47.64,    47.642,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.94994,   0.94994,    54.469,    54.474,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.96993,   0.96994,    64.838,    64.852,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.98008,   0.98001,    76.549,     76.57,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.89988,   0.89989,    45.048,    45.051,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,   0.84988,   0.84988,    41.253,    41.255,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.79983,   0.79983,    39.164,    39.166,         0,  0,  0,  ,  ,  ,  
