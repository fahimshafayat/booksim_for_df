vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 33;
perm_seed = 76;
routing_function = UGAL_L_multi_tiered;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4996,    0.4996,    127.83,    87.118,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.53466,   0.57503,    1003.7,    619.47,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.53252,   0.56163,    545.19,    413.63,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.53222,   0.53253,     956.3,    337.73,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.52465,    0.5247,    518.35,    160.22,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.50884,   0.50882,    276.81,    99.685,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.52,   0.51758,   0.51758,    303.94,    118.27,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.45,   0.45001,      0.45,    58.544,    58.574,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.4,   0.40004,   0.40005,    49.327,    49.341,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.35,   0.35006,   0.35006,    44.075,    44.083,         0,  0,  0,  ,  ,  ,  
