vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 25;
perm_seed = 76;
routing_function = UGAL_L;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4962,   0.49623,    428.85,    133.46,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25008,    42.197,    42.205,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.37011,   0.37011,    47.953,    47.966,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42973,   0.42973,     109.5,    61.679,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45952,   0.45952,    189.19,     73.07,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,   0.47883,   0.47887,    354.03,    93.767,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.49,   0.48777,   0.48777,    282.67,    109.74,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44958,   0.44958,    164.47,    69.045,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.40009,   0.40009,    51.641,    51.663,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,    0.3501,    0.3501,    45.779,    45.791,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30005,   0.30006,    42.985,    42.993,         0,  0,  0,  ,  ,  ,  
