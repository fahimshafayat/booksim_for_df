vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 25;
perm_seed = 74;
routing_function = UGAL_L_multi_tiered;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4999,    0.4999,    81.975,    69.462,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.56572,   0.60109,    789.95,    503.64,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.55561,   0.56646,    707.39,    341.67,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,    0.5441,   0.54411,    562.49,    200.68,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.52701,   0.52702,     359.0,    114.18,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.50966,   0.50968,    117.03,    76.914,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.52,   0.51903,   0.51903,    228.01,    88.897,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.45,   0.45007,   0.45008,    52.618,    52.651,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.4,   0.40009,   0.40009,    47.326,    47.344,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.35,    0.3501,    0.3501,    42.251,    42.256,         0,  0,  0,  ,  ,  ,  
