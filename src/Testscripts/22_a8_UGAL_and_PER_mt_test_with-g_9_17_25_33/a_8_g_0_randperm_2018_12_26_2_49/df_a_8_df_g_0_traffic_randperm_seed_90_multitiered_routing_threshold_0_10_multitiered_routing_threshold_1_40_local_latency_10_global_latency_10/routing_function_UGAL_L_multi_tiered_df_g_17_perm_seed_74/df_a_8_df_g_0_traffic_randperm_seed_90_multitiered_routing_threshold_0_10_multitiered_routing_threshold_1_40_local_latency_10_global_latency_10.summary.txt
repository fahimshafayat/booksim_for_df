vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 17;
perm_seed = 74;
routing_function = UGAL_L_multi_tiered;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49998,   0.49997,    44.272,    44.287,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.64899,     0.677,     490.4,     337.5,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.61618,    0.6162,     335.7,    98.296,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55996,   0.55997,    52.563,    52.595,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.59,   0.58984,   0.58985,    66.063,    64.136,         0,  0,  0,  ,  ,  ,  
simulation,         6,       0.6,   0.59842,    0.5984,    213.46,    74.329,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.61,   0.60739,   0.60743,    434.89,     87.95,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.55,   0.54993,   0.54993,    50.559,    50.583,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.45,   0.44995,   0.44994,    40.525,    40.532,         0,  0,  0,  ,  ,  ,  
