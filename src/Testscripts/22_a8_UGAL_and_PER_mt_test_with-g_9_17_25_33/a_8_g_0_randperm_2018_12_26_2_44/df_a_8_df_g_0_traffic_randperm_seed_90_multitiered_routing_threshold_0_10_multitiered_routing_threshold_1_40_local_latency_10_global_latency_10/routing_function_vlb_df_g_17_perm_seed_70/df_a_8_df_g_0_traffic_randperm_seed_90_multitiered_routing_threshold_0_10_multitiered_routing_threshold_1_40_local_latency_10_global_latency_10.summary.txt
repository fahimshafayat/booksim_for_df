vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 17;
perm_seed = 70;
routing_function = vlb;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.47885,   0.48164,    948.23,    598.42,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25005,    64.633,    64.637,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36998,   0.36997,    68.654,    68.657,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42993,   0.42991,     75.07,    75.073,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45997,   0.45999,    84.811,    84.813,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,   0.47402,   0.47489,    497.59,    294.79,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.46904,   0.46943,    177.27,    145.86,         0,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44995,   0.44994,    80.204,    80.205,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39994,   0.39994,    70.974,    70.977,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35002,   0.35001,     67.59,    67.593,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30002,   0.30001,    65.767,    65.771,         0,  0,  0,  ,  ,  ,  
