vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 25;
perm_seed = 72;
routing_function = vlb;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.37911,   0.42603,    1122.5,    874.45,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25008,     65.31,    65.314,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36786,   0.36848,    426.54,    277.13,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,    0.3777,    0.4148,    560.07,    536.75,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.37378,    0.3888,    598.79,    506.47,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.38,   0.37141,   0.37624,    493.53,     426.3,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.35,   0.35009,    0.3501,    72.989,    72.989,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.30005,   0.30006,    67.244,    67.246,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20009,   0.20009,    64.247,     64.25,         0,  0,  0,  ,  ,  ,  
