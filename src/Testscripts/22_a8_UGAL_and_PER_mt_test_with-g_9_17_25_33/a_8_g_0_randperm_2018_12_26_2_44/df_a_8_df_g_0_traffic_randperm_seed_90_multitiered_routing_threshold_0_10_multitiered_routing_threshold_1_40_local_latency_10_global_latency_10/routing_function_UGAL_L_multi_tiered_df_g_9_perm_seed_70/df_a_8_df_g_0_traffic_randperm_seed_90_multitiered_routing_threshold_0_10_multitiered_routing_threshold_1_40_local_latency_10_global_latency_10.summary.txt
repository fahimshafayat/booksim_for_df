vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 9;
perm_seed = 70;
routing_function = UGAL_L_multi_tiered;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4998,   0.49978,    35.535,    35.537,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.73855,   0.73856,    197.86,    61.406,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.81519,   0.81582,    918.23,    203.69,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.79174,   0.79175,    492.36,    94.648,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.76659,   0.76659,    408.45,    73.422,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.78,   0.77568,   0.77568,    344.07,    79.627,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.75,   0.74807,   0.74808,    251.31,    63.119,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.7,   0.70019,   0.70017,    46.325,    46.343,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.65,   0.65006,   0.65004,    41.082,     41.09,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.6,   0.59998,   0.59996,    38.225,     38.23,         0,  0,  0,  ,  ,  ,  
