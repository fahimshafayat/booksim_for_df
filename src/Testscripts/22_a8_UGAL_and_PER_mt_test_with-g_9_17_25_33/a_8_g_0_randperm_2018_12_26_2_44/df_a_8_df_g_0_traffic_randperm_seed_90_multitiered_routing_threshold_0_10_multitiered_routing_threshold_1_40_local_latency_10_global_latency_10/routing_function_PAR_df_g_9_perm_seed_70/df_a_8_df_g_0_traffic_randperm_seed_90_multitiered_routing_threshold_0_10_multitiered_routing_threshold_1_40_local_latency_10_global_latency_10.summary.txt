vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 9;
perm_seed = 70;
routing_function = PAR;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4998,   0.49978,    38.902,    38.908,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.74031,   0.74028,    56.813,     56.82,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.74446,   0.78243,    552.87,    377.53,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.75303,   0.75372,    1098.9,    353.21,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.76836,   0.76835,    230.42,    110.33,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.78,   0.76419,   0.76464,    515.64,    202.55,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.75,   0.75034,   0.75028,    61.812,    61.823,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.7,   0.70017,   0.70017,    47.293,    47.303,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.65,   0.65006,   0.65004,    42.684,     42.69,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.6,   0.59997,   0.59996,    40.577,    40.584,         0,  0,  0,  ,  ,  ,  
