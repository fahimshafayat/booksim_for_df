vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 9;
perm_seed = 70;
routing_function = vlb;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.48782,   0.49106,    680.52,    402.44,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24998,   0.24997,    62.017,    62.023,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36993,   0.36992,     65.39,    65.395,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42992,   0.42991,     70.21,    70.214,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45988,   0.45986,    76.597,    76.601,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,   0.47898,   0.47921,    181.55,    129.31,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.49,   0.48656,   0.48697,     467.4,    218.95,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44995,   0.44994,    73.686,    73.692,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39986,   0.39984,    67.213,    67.217,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.34991,   0.34989,     64.52,    64.526,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.29992,   0.29991,    63.015,    63.021,         0,  0,  0,  ,  ,  ,  
