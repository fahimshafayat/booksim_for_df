vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 17;
perm_seed = 70;
routing_function = PAR;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49997,   0.49997,    45.049,    45.059,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.62844,   0.66535,    613.61,    446.82,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.61994,   0.61995,    68.781,    68.805,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.68,   0.63344,   0.64597,    559.55,    337.34,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.65,   0.63801,   0.63819,    529.58,    250.25,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.63,   0.62992,   0.62996,    80.196,    80.234,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.64,   0.63953,   0.63959,    144.14,    133.99,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.6,    0.5999,   0.59993,    58.354,    58.373,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.55,   0.54993,   0.54993,    48.207,    48.218,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.45,   0.44994,   0.44994,     43.73,     43.74,         0,  0,  0,  ,  ,  ,  
