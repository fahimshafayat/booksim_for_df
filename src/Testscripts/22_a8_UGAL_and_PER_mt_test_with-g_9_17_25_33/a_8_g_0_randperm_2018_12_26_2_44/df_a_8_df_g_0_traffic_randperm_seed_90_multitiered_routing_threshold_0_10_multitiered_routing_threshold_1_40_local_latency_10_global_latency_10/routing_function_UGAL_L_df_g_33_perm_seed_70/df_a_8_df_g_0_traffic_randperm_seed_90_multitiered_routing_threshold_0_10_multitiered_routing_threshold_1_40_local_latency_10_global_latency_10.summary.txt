vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 33;
perm_seed = 70;
routing_function = UGAL_L;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49957,   0.49957,    143.41,    97.235,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.54071,   0.58126,    996.31,    613.08,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.53731,   0.56556,    527.68,    399.85,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.53477,   0.53498,    898.47,    319.52,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,     0.524,   0.52397,    525.86,     183.9,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.50904,   0.50901,    251.61,    111.01,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.52,   0.51748,   0.51748,    334.07,    138.49,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.45,   0.45001,      0.45,    63.698,    63.744,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.4,   0.40005,   0.40005,    52.156,     52.18,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.35,   0.35007,   0.35006,     46.15,     46.16,         0,  0,  0,  ,  ,  ,  
