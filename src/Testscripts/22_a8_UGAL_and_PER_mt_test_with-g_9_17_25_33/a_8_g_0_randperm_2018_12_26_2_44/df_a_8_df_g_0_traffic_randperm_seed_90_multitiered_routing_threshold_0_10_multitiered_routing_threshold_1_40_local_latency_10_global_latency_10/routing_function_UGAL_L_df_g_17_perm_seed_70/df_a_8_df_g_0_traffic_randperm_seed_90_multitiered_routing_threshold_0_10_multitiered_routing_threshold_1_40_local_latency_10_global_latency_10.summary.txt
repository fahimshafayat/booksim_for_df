vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 17;
perm_seed = 70;
routing_function = UGAL_L;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49997,   0.49997,    48.007,    48.022,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.61977,   0.65297,    626.78,     420.9,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,    0.6098,   0.60988,    477.81,    162.42,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55903,   0.55905,    270.35,    62.363,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.59,    0.5868,   0.58681,    248.66,    85.954,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.57,   0.56793,   0.56794,    426.66,    70.218,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.58,    0.5774,   0.57742,    225.88,    75.426,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.55,   0.54919,   0.54918,    215.47,    58.574,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,   0.44994,   0.44994,    43.656,    43.666,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.4,   0.39994,   0.39994,    41.128,    41.135,         0,  0,  0,  ,  ,  ,  
