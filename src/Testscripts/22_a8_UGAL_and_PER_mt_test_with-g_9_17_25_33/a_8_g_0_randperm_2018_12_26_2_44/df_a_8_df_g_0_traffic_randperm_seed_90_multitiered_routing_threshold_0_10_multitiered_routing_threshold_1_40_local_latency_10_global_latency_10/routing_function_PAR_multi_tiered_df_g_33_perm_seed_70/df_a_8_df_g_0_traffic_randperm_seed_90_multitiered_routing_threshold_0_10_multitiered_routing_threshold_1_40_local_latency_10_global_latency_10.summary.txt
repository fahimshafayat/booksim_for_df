vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 33;
perm_seed = 70;
routing_function = PAR_multi_tiered;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49998,   0.49998,    56.507,    56.527,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.54566,   0.59094,    1026.2,     670.9,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.54697,   0.57878,    496.47,    417.42,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55318,    0.5531,    509.31,    235.39,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.53001,   0.53001,    68.861,    68.901,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.54,   0.54005,   0.54002,    77.836,    77.869,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.55,   0.54986,   0.54989,    110.58,    105.83,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,      0.45,      0.45,    49.509,     49.52,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.4,   0.40005,   0.40005,    46.935,    46.944,         0,  0,  0,  ,  ,  ,  
