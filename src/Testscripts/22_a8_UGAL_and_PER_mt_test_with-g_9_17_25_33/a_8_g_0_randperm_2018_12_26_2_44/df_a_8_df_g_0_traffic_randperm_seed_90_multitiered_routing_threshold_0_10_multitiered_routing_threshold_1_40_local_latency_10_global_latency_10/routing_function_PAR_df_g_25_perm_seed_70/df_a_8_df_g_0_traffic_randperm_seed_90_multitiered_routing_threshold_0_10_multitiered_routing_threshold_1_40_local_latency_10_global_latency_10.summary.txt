vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 25;
perm_seed = 70;
routing_function = PAR;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50005,   0.50006,    62.449,    62.472,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.54988,   0.59221,     971.1,    615.97,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.54907,     0.577,    464.75,     374.7,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.54911,   0.54885,     568.4,    229.91,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.52978,   0.52978,    106.55,    82.449,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.54,   0.53874,   0.53873,    260.52,    108.32,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.55,    0.5456,   0.54564,    497.13,    168.98,         1,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,   0.45008,   0.45008,      51.7,     51.71,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.4,   0.40009,   0.40009,    48.132,    48.143,         0,  0,  0,  ,  ,  ,  
