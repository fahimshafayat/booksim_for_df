vc_alloc_delay = 1;
input_speedup = 1;
sw_alloc_delay = 1;
warmup_periods = 3;
sample_period = 10000;
internal_speedup = 4.0;
alloc_iters = 1;
vc_buf_size = 64;
routing_delay = 0;
vc_allocator = separable_input_first;
sim_count = 1;
num_vcs = 7;
output_speedup = 1;
credit_delay = 2;
packet_size = 1;
sw_allocator = separable_input_first;
priority = none;
st_final_delay = 1;
injection_rate_uses_flits = 1;
wait_for_tail_credit = 0;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 17;
routing_function = min;
shift_by_group = 9;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.062339,   0.10988,    4384.2,    4284.2,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.062315,    0.1099,    3754.7,    3745.2,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.062296,   0.10991,    2579.6,    2579.6,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.062268,  0.070238,    561.24,    561.24,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.039978,  0.039975,    35.141,    35.143,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.05,  0.049959,  0.049959,    36.213,    36.214,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.06,   0.05997,   0.05998,     45.65,    45.654,         0,  0,  0,  ,  ,  ,  
