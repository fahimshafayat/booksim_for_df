vc_alloc_delay = 1;
input_speedup = 1;
sw_alloc_delay = 1;
warmup_periods = 3;
sample_period = 10000;
internal_speedup = 4.0;
alloc_iters = 1;
vc_buf_size = 64;
routing_delay = 0;
vc_allocator = separable_input_first;
sim_count = 1;
num_vcs = 7;
output_speedup = 1;
credit_delay = 2;
packet_size = 1;
sw_allocator = separable_input_first;
priority = none;
st_final_delay = 1;
injection_rate_uses_flits = 1;
wait_for_tail_credit = 0;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 9;
routing_function = min;
shift_by_group = 5;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.12462,   0.17409,    3761.0,    2926.4,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.12458,   0.17398,    2508.2,    2393.1,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12494,   0.12993,    1016.2,    1016.2,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.069965,  0.069959,    34.917,    34.918,         0,  0,  0,  ,  ,  ,  
simulation,         5,       0.1,  0.099939,  0.099933,    36.269,     36.27,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.11,   0.10991,   0.10991,    37.897,      37.9,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.12,   0.11986,   0.11985,    45.412,    45.419,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.05,  0.049936,   0.04993,    34.609,     34.61,         0,  0,  0,  ,  ,  ,  
