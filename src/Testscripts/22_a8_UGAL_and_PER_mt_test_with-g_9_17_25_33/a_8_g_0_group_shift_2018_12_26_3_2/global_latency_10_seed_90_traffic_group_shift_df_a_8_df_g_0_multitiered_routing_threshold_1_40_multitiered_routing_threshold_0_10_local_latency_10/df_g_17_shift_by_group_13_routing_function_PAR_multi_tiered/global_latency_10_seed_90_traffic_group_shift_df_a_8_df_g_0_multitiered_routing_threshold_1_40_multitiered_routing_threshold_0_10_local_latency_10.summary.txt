vc_alloc_delay = 1;
input_speedup = 1;
sw_alloc_delay = 1;
warmup_periods = 3;
sample_period = 10000;
internal_speedup = 4.0;
alloc_iters = 1;
vc_buf_size = 64;
routing_delay = 0;
vc_allocator = separable_input_first;
sim_count = 1;
num_vcs = 7;
output_speedup = 1;
credit_delay = 2;
packet_size = 1;
sw_allocator = separable_input_first;
priority = none;
st_final_delay = 1;
injection_rate_uses_flits = 1;
wait_for_tail_credit = 0;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 17;
routing_function = PAR_multi_tiered;
shift_by_group = 13;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.47121,   0.47222,    1091.4,    392.41,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,    57.891,    57.896,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36997,   0.36997,    74.901,    74.905,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42991,   0.42991,    91.276,    91.296,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45914,   0.45916,    237.75,    139.97,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,   0.46887,   0.46871,    565.81,    233.45,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.46679,   0.46688,    480.47,    174.07,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44986,   0.44988,    115.44,    108.34,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39994,   0.39994,    82.161,    82.167,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,      0.35,   0.35001,    71.173,    71.178,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,       0.3,   0.30001,    63.114,    63.118,         0,  0,  0,  ,  ,  ,  
