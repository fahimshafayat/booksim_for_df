vc_alloc_delay = 1;
input_speedup = 1;
sw_alloc_delay = 1;
warmup_periods = 3;
sample_period = 10000;
internal_speedup = 4.0;
alloc_iters = 1;
vc_buf_size = 64;
routing_delay = 0;
vc_allocator = separable_input_first;
sim_count = 1;
num_vcs = 7;
output_speedup = 1;
credit_delay = 2;
packet_size = 1;
sw_allocator = separable_input_first;
priority = none;
st_final_delay = 1;
injection_rate_uses_flits = 1;
wait_for_tail_credit = 0;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 17;
routing_function = vlb;
shift_by_group = 9;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.45494,   0.49696,    493.63,    491.43,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,    67.285,    67.286,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36998,   0.36997,    73.088,    73.087,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,    0.4299,   0.42991,    87.879,    87.872,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45989,   0.45999,    163.59,    163.88,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,   0.45547,   0.47547,    508.99,    492.04,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.45861,   0.46703,    597.06,    581.89,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44987,   0.44994,    112.22,    112.25,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39995,   0.39994,    77.394,     77.39,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35001,   0.35001,    71.364,    71.363,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30001,   0.30001,    68.761,    68.762,         0,  0,  0,  ,  ,  ,  
