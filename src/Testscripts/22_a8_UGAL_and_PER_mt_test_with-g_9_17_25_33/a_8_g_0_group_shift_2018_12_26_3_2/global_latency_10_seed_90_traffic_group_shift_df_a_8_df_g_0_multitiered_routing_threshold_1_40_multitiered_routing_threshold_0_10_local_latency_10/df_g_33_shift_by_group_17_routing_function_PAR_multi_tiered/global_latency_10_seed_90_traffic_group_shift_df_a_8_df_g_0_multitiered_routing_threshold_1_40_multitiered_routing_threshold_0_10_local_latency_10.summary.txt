vc_alloc_delay = 1;
input_speedup = 1;
sw_alloc_delay = 1;
warmup_periods = 3;
sample_period = 10000;
internal_speedup = 4.0;
alloc_iters = 1;
vc_buf_size = 64;
routing_delay = 0;
vc_allocator = separable_input_first;
sim_count = 1;
num_vcs = 7;
output_speedup = 1;
credit_delay = 2;
packet_size = 1;
sw_allocator = separable_input_first;
priority = none;
st_final_delay = 1;
injection_rate_uses_flits = 1;
wait_for_tail_credit = 0;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 33;
routing_function = PAR_multi_tiered;
shift_by_group = 17;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.41373,   0.44632,    661.09,    519.27,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25007,   0.25006,    66.401,    66.406,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.37007,   0.37006,    83.319,    83.335,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42793,   0.42799,    366.25,    144.86,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.42512,   0.42537,    1148.9,    386.77,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.43062,   0.43072,    488.95,    200.97,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.4,   0.40005,   0.40005,    96.015,    96.043,         0,  0,  0,  ,  ,  ,  
simulation,         8,      0.35,   0.35007,   0.35006,    78.753,    78.764,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.30005,   0.30005,    72.048,    72.055,         0,  0,  0,  ,  ,  ,  
