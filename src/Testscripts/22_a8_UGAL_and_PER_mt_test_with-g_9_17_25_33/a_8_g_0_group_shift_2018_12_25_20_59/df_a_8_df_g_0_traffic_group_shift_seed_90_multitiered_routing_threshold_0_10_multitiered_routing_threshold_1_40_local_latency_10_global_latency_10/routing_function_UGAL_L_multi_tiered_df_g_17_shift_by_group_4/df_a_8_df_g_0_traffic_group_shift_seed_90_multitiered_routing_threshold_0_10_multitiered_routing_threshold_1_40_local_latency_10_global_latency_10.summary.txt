vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 17;
routing_function = UGAL_L_multi_tiered;
shift_by_group = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.39045,   0.42594,    928.63,    669.06,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,    90.548,    90.672,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.34164,    0.3444,    1068.3,    732.79,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,      0.31,   0.31001,    100.54,    100.78,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,    0.3385,    0.3389,    313.24,    239.74,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.35,   0.34205,   0.34374,    564.67,    449.91,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.30001,   0.30001,    97.419,     97.62,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20007,   0.20007,    84.096,    84.157,         0,  0,  0,  ,  ,  ,  
