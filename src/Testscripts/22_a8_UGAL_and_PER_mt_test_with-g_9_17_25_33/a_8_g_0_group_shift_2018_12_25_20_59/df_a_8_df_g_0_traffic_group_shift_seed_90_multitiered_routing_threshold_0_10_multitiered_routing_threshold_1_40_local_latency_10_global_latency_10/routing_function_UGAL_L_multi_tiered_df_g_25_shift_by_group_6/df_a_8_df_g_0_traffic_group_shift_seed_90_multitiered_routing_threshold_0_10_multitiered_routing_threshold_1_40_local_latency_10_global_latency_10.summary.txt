vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 25;
routing_function = UGAL_L_multi_tiered;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.33074,   0.36971,    1423.0,    912.72,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25008,    87.885,    88.028,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.32651,   0.35328,    514.66,     475.0,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.29998,   0.30041,    731.17,     472.1,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27844,   0.27914,    441.42,    351.84,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.29,   0.28589,   0.28677,    545.47,    433.16,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.20009,   0.20009,    81.516,    81.597,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.15,   0.15006,   0.15007,    75.468,    75.504,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.1,   0.10006,   0.10007,    59.863,     59.88,         0,  0,  0,  ,  ,  ,  
