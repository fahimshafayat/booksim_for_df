vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 9;
routing_function = PAR_multi_tiered;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49979,   0.49978,    87.983,    87.989,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.52033,   0.56816,    1300.5,    741.11,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.53013,   0.56625,    628.73,    506.64,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.54315,   0.54354,    733.71,    402.18,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.52981,   0.52978,    119.46,    119.46,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.54,   0.53871,   0.53894,    239.91,    163.02,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.55,   0.54266,     0.543,    500.98,    272.63,         1,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,   0.44995,   0.44994,    69.278,    69.278,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.4,   0.39986,   0.39984,    61.455,    61.457,         0,  0,  0,  ,  ,  ,  
