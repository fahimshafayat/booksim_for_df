vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 25;
routing_function = vlb;
shift_by_group = 6;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.3525,   0.40381,    1465.2,    1102.3,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25008,    67.941,    67.942,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.34692,    0.3574,    1122.4,    978.36,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31007,   0.31007,    71.998,    71.998,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,   0.34014,   0.34014,    80.883,    80.881,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.35,   0.35013,    0.3501,    95.204,    95.201,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.36,   0.35506,   0.35958,    554.43,    546.76,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.30006,   0.30006,     70.87,    70.869,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.2,   0.20009,   0.20009,    66.584,    66.585,         0,  0,  0,  ,  ,  ,  
