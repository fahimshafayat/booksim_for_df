vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 9;
routing_function = min;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.12462,   0.17395,    3760.7,    2924.7,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.12458,   0.17384,    2507.5,    2389.3,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,     0.125,   0.12993,    1012.9,    1012.9,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.069965,  0.069959,    34.916,    34.917,         0,  0,  0,  ,  ,  ,  
simulation,         5,       0.1,  0.099938,  0.099933,    36.286,    36.288,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.11,   0.10991,   0.10991,    37.914,    37.916,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.12,   0.11986,   0.11985,    45.944,    45.949,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.05,  0.049933,   0.04993,    34.616,    34.618,         0,  0,  0,  ,  ,  ,  
