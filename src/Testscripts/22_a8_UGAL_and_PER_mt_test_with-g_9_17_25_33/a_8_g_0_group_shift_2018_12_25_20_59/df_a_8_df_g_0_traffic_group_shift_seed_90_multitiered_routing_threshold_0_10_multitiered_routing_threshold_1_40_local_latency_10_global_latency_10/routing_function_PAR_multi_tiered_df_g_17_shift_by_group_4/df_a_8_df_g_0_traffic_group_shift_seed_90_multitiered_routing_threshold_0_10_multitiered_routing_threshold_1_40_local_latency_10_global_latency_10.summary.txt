vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 17;
routing_function = PAR_multi_tiered;
shift_by_group = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.46646,   0.47746,    548.91,    360.39,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25005,     57.76,    57.764,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36997,   0.36997,    74.896,    74.899,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42992,   0.42991,    93.276,     93.29,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45579,   0.45582,    509.62,    179.86,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.43987,   0.43992,    100.93,    101.04,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.45,   0.44817,    0.4482,    391.27,    152.89,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39994,   0.39994,    83.302,    83.311,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35001,   0.35001,    71.332,    71.335,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30001,   0.30001,    63.235,    63.238,         0,  0,  0,  ,  ,  ,  
