vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 2.0;
warmup_periods = 3;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
sample_period = 10000;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
routing_function = vlb;
routing_threshold = 0;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099963, 0.0099967,    22.308,    22.308,         0
simulation,         2,      0.99,   0.38311,   0.42939,    3037.0,    1104.2,         1
simulation,         3,       0.5,   0.37671,   0.42248,    1184.2,    857.09,         1
simulation,         4,      0.25,   0.24993,   0.24993,    25.445,    25.445,         0
simulation,         5,      0.37,   0.36996,   0.36995,    35.543,    35.546,         0
simulation,         6,      0.43,   0.38371,    0.4227,    495.62,    482.14,         1
simulation,         7,       0.4,   0.38298,    0.3916,    970.95,    838.84,         1
simulation,         8,      0.38,   0.37998,   0.37995,    45.578,    45.617,         0
simulation,         9,      0.39,   0.38296,   0.38811,    521.78,    505.25,         1
simulation,        10,      0.35,   0.34997,   0.34996,    31.077,    31.077,         0
simulation,        11,       0.3,   0.29994,   0.29994,    27.244,    27.244,         0
simulation,        13,       0.2,   0.19994,   0.19994,    24.327,    24.327,         0
