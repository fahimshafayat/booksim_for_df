vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 2.0;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
warmup_periods = 10;
routing_function = vlb;
routing_threshold = 0;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010033,   0.01003,    22.302,    22.304,         0
simulation,         2,      0.99,   0.39451,   0.63802,    591.41,    586.47,         1
simulation,         3,       0.5,   0.36224,    0.4676,     627.4,    625.82,         1
simulation,         4,      0.25,      0.25,   0.24998,    25.443,    25.442,         0
simulation,         5,      0.37,      0.37,   0.37001,    35.531,    35.567,         0
simulation,         6,      0.43,   0.38216,   0.42178,     663.9,    658.58,         1
simulation,         7,       0.4,   0.38371,   0.39968,    306.32,    423.81,         0
simulation,         8,      0.41,   0.38064,   0.40753,    521.82,    519.14,         1
simulation,        10,      0.35,   0.34996,   0.34994,    31.075,    31.072,         0
simulation,        11,       0.3,   0.30003,   0.30001,    27.247,    27.244,         0
