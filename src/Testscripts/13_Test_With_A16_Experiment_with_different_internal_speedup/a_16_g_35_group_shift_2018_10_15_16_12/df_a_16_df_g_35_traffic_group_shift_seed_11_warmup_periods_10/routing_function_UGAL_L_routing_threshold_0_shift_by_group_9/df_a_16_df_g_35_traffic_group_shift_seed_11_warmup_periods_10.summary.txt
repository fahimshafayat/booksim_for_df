vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 2.0;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 35;
seed = 11;
topology = dragonflyfull;
traffic = group_shift;
warmup_periods = 10;
routing_function = UGAL_L;
routing_threshold = 0;
shift_by_group = 9;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.010035,   0.01003,    15.553,    15.558,         0
simulation,         2,      0.99,    0.1637,   0.40028,    640.41,    636.82,         1
simulation,         3,       0.5,   0.20374,   0.43704,    351.26,    351.24,         1
simulation,         4,      0.25,   0.15992,   0.24963,     437.1,     437.1,         1
simulation,         5,      0.13,   0.12984,      0.13,    73.918,    81.533,         0
simulation,         6,      0.19,   0.15254,   0.18999,    497.25,    497.24,         1
simulation,         7,      0.16,   0.14823,   0.15996,    371.63,    371.63,         1
simulation,         8,      0.14,   0.13881,   0.14001,    104.98,     136.2,         0
simulation,         9,      0.15,   0.14588,   0.15002,    182.95,    287.03,         0
simulation,        11,       0.1,   0.10009,   0.10008,    57.483,    58.906,         0
simulation,        12,      0.05,   0.05005,   0.05003,     26.01,    26.172,         0
