wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_L_restricted;
shift_by_group = 1;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.31268,   0.32222,    1359.1,    271.59,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25008,    21.139,    21.148,         0,  1984911,  10039173,  ,  ,  ,  
simulation,         3,      0.37,    0.3186,   0.32446,    464.33,    150.04,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30752,    0.3075,     434.3,    41.885,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.28004,   0.28004,    23.863,    23.154,         0,  2083387,  12046575,  ,  ,  ,  
simulation,         6,      0.29,   0.28977,   0.28977,    79.954,    26.228,         0,  3879434,  23356260,  ,  ,  ,  
simulation,         7,       0.3,   0.29914,   0.29916,    241.45,    34.652,         0,  4374739,  27334049,  ,  ,  ,  
simulation,        10,       0.2,   0.20009,   0.20009,    19.628,    19.633,         0,  1983884,  7632397,  ,  ,  ,  
simulation,        11,      0.15,   0.15007,   0.15007,    17.744,    17.747,         0,  1981209,  5226382,  ,  ,  ,  
