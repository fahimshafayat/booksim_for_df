wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = UGAL_G;
shift_by_group = 1;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.18165,   0.18456,    3134.6,    157.46,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.18186,    0.1848,    1255.8,    153.05,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12987,   0.12987,    12.975,    12.976,         0,  1197063,  1049717,  ,  ,  ,  
simulation,         4,      0.19,   0.17961,   0.17949,    1128.3,    82.964,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.16,   0.15992,   0.15991,    13.884,    13.884,         0,  1471008,  1294953,  ,  ,  ,  
simulation,         6,      0.17,   0.16995,   0.16994,    14.818,    14.818,         0,  1569427,  1370182,  ,  ,  ,  
simulation,         7,      0.18,   0.17914,   0.17914,    404.79,    32.745,         0,  3180257,  2660409,  ,  ,  ,  
simulation,         8,      0.15,   0.14992,   0.14992,    13.455,    13.455,         0,  1378898,  1214173,  ,  ,  ,  
simulation,         9,       0.1,  0.099935,  0.099933,    12.554,    12.554,         0,  930793,  797429,  ,  ,  ,  
simulation,        10,      0.05,  0.049932,   0.04993,    12.028,    12.028,         0,  488932,  374795,  ,  ,  ,  
