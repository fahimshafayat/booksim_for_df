wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_L_restricted;
shift_by_group = 2;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50005,   0.50006,    146.78,    147.35,         0,  2423102,  2401780,  ,  ,  ,  
simulation,         2,      0.74,   0.59298,   0.74013,    864.21,    864.21,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.61027,   0.61968,    558.45,    557.21,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55998,   0.55998,    145.47,    146.11,         0,  2425470,  2981747,  ,  ,  ,  
simulation,         5,      0.59,   0.58993,   0.58992,    151.79,    152.51,         0,  2428206,  3272255,  ,  ,  ,  
simulation,         6,       0.6,   0.59993,   0.59991,    158.68,    159.45,         0,  2428796,  3368503,  ,  ,  ,  
simulation,         7,      0.61,   0.60991,   0.60986,    174.85,     175.8,         0,  2434747,  3470608,  ,  ,  ,  
simulation,         9,      0.55,   0.54996,   0.54996,    145.03,    145.64,         0,  2425657,  2885925,  ,  ,  ,  
simulation,        11,      0.45,   0.45012,   0.45013,    152.15,    152.65,         0,  2421480,  1919185,  ,  ,  ,  
