wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_L_restricted;
shift_by_group = 1;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.35004,   0.46213,    731.17,    731.12,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25007,   0.25008,    60.547,    60.901,         0,  2000837,  10071496,  ,  ,  ,  
simulation,         3,      0.37,   0.33935,   0.35295,    870.17,    835.43,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30852,   0.30897,    326.47,    171.42,         0,  4890874,  31644913,  ,  ,  ,  
simulation,         5,      0.34,   0.33308,   0.33437,    402.31,    362.77,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.32,   0.31838,   0.31897,    386.25,    223.37,         0,  5503959,  36838718,  ,  ,  ,  
simulation,         7,      0.33,   0.32668,   0.32715,    396.39,    286.79,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.30006,   0.30006,    58.208,    58.575,         0,  2007194,  12518507,  ,  ,  ,  
simulation,        10,       0.2,   0.20009,   0.20009,    69.017,    69.411,         0,  1999494,  7653517,  ,  ,  ,  
simulation,        11,      0.15,   0.15007,   0.15007,    83.571,    84.002,         0,  1998442,  5238644,  ,  ,  ,  
