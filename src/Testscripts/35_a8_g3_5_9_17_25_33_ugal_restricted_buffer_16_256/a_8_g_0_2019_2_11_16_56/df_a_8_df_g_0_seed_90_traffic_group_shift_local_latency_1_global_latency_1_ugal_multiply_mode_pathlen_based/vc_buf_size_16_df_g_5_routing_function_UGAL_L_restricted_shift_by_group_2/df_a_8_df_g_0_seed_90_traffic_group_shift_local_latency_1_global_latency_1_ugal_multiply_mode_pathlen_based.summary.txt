wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_L_restricted;
shift_by_group = 2;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50004,   0.50006,    36.374,    36.397,         0,  2406443,  2402708,  ,  ,  ,  
simulation,         2,      0.74,   0.53753,   0.54949,    1369.2,    212.28,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.53768,    0.5495,    657.87,    200.99,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.53748,   0.53747,    929.45,     221.2,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.52997,   0.52999,    45.741,    45.795,         0,  2411421,  2694579,  ,  ,  ,  
simulation,         6,      0.54,   0.53983,   0.53992,    56.702,    55.776,         0,  2429297,  2810296,  ,  ,  ,  
simulation,         7,      0.55,   0.53786,   0.53854,    573.45,    196.02,         1,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,   0.45012,   0.45013,    24.052,    24.057,         0,  2403834,  1921786,  ,  ,  ,  
simulation,        11,       0.4,   0.40019,   0.40022,     14.84,    14.841,         0,  2334395,  1508494,  ,  ,  ,  
