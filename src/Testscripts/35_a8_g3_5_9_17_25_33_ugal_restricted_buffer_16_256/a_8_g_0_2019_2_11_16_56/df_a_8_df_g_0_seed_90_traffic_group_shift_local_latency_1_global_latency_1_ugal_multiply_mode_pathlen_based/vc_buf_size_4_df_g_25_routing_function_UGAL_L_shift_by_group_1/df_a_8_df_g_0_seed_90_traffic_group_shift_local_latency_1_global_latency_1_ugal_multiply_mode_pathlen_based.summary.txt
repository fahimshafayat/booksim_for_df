wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_L;
shift_by_group = 1;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.12512,   0.12791,    2818.6,    215.63,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.11785,   0.12047,    2175.8,     213.8,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,    0.1088,   0.11074,    661.77,    158.44,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.070055,  0.070054,    17.498,      17.5,         0,  1319405,  2048209,  ,  ,  ,  
simulation,         5,       0.1,  0.097355,  0.097378,     569.6,    92.596,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.08,   0.08005,  0.080049,    19.995,    19.995,         0,  1390066,  2472683,  ,  ,  ,  
simulation,         7,      0.09,  0.089518,  0.089517,    479.74,    43.105,         0,  2984541,  5979448,  ,  ,  ,  
simulation,         8,      0.05,  0.050054,  0.050053,    14.521,    14.522,         0,  1150132,  1251413,  ,  ,  ,  
