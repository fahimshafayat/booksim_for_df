wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = vlb;
shift_by_group = 1;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.41092,   0.42293,     857.4,    272.15,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24998,   0.24997,    17.983,    17.983,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36993,   0.36992,    25.661,    25.661,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.41144,   0.41131,    898.03,     277.5,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.39987,   0.39984,    34.022,     34.02,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.41,   0.40996,   0.40993,    41.617,    41.617,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.42,   0.41535,   0.41576,    516.63,    236.43,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.35,   0.34991,   0.34989,    23.066,    23.066,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.3,   0.29992,   0.29991,    19.699,    19.699,         0,  0,  0,  ,  ,  ,  
