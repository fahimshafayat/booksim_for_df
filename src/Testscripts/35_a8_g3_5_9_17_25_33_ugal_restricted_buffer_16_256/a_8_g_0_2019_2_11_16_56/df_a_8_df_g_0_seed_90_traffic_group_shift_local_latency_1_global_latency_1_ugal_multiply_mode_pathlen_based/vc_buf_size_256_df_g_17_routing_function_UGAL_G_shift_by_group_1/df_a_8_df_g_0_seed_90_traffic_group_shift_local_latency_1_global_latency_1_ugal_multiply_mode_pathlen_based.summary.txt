wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_G;
shift_by_group = 1;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49995,   0.49997,     37.95,     37.95,         0,  2044059,  14295896,  ,  ,  ,  
simulation,         2,      0.74,   0.43689,   0.60891,    1490.7,    1475.5,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.50654,   0.61025,     789.7,    789.48,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.51946,   0.55379,     640.3,    639.62,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.52981,   0.52993,    84.776,    84.819,         0,  2049403,  15296252,  ,  ,  ,  
simulation,         6,      0.54,   0.52714,   0.54015,    578.36,    578.36,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44995,   0.44994,    26.698,    26.698,         0,  2042703,  12657485,  ,  ,  ,  
simulation,         9,       0.4,   0.39995,   0.39994,    22.486,    22.487,         0,  2042096,  11021675,  ,  ,  ,  
simulation,        10,      0.35,   0.35002,   0.35001,    20.041,    20.041,         0,  2041636,  9388378,  ,  ,  ,  
