wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_L;
shift_by_group = 2;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.35021,   0.47802,    1104.1,    1104.1,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25007,   0.25008,    73.595,     74.07,         0,  2188878,  9910526,  ,  ,  ,  
simulation,         3,      0.37,   0.34478,   0.36998,    489.24,    489.24,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.29978,   0.31007,     569.7,    727.57,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27904,   0.28003,    203.83,    224.09,         0,  3981132,  20580506,  ,  ,  ,  
simulation,         6,      0.29,   0.28578,   0.29004,    479.44,    482.07,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.20009,   0.20009,     80.43,    80.896,         0,  2183641,  7477158,  ,  ,  ,  
simulation,         9,      0.15,   0.15007,   0.15007,    94.949,    95.439,         0,  2181652,  5058912,  ,  ,  ,  
simulation,        10,       0.1,   0.10007,   0.10007,    59.545,    59.869,         0,  2029378,  2794429,  ,  ,  ,  
