wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = min;
shift_by_group = 1;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4988,   0.50006,    234.03,    235.81,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.49944,   0.71974,    1612.1,    1612.0,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.49933,   0.62008,    962.62,    962.62,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.49926,   0.55988,    541.41,    541.41,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.49937,    0.5297,    573.85,    573.85,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.49939,   0.51028,    535.81,    535.81,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.45028,   0.45026,    14.592,    14.593,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.40035,   0.40035,    12.068,    12.068,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35028,   0.35028,    11.125,    11.125,         0,  0,  0,  ,  ,  ,  
