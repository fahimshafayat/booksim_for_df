wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = UGAL_G;
shift_by_group = 1;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.1583,   0.16107,    3045.2,    170.45,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,    0.1468,   0.14937,    1776.3,    168.51,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12456,   0.12454,    768.67,    61.882,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.070093,  0.070094,    14.297,    14.297,         0,  1242205,  3196544,  ,  ,  ,  
simulation,         5,       0.1,   0.10012,   0.10012,    16.115,    16.116,         0,  1592131,  4751623,  ,  ,  ,  
simulation,         6,      0.11,   0.10982,   0.10982,    177.95,    21.246,         0,  3703094,  11785934,  ,  ,  ,  
simulation,         7,      0.12,   0.11766,   0.11766,    550.04,    47.933,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.05,  0.050089,   0.05009,    13.861,    13.861,         0,  972545,  2198175,  ,  ,  ,  
