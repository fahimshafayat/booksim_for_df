wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_L;
shift_by_group = 2;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.31161,   0.32477,    1898.6,     393.8,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25018,   0.25018,    12.297,    12.297,         0,  1780088,  622127,  ,  ,  ,  
simulation,         3,      0.37,   0.30938,   0.32249,    814.74,     362.1,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31001,   0.31002,    14.449,    14.449,         0,  2214444,  762656,  ,  ,  ,  
simulation,         5,      0.34,   0.31141,    0.3179,    804.61,    345.88,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.32,    0.3201,    0.3201,     15.53,     15.53,         0,  2281189,  792597,  ,  ,  ,  
simulation,         7,      0.33,   0.32293,   0.32362,    549.83,    155.81,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.29995,   0.29995,    13.751,    13.752,         0,  2144667,  736409,  ,  ,  ,  
simulation,        10,       0.2,   0.20007,   0.20008,    11.782,    11.782,         0,  1408347,  513512,  ,  ,  ,  
simulation,        11,      0.15,   0.14995,   0.14995,    11.542,    11.542,         0,  1041378,  399930,  ,  ,  ,  
