wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = vlb;
shift_by_group = 1;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.3649,   0.49976,    1449.2,    1449.2,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25018,   0.25018,    18.547,    18.547,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.37026,   0.37022,    99.506,    99.537,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.36439,    0.4298,    829.59,    829.59,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.36989,    0.3997,    810.47,    810.47,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.38,   0.37324,   0.38042,    548.72,    548.72,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.35,   0.35017,   0.35016,    32.346,    32.343,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.29995,   0.29995,    21.246,    21.246,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20008,   0.20008,    17.188,    17.188,         0,  0,  0,  ,  ,  ,  
