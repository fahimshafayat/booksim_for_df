wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = UGAL_G;
shift_by_group = 2;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50017,   0.50017,    13.132,    13.132,         0,  2342246,  538840,  ,  ,  ,  
simulation,         2,      0.74,   0.73999,   0.73994,    36.826,    36.826,         0,  2883547,  1382834,  ,  ,  ,  
simulation,         3,      0.86,   0.72334,   0.85998,    730.92,    730.91,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.72849,   0.79777,    788.66,    788.53,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.72002,   0.76982,    1129.0,    1128.9,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.75,   0.74985,   0.75001,    71.038,    71.036,         0,  3368025,  1680117,  ,  ,  ,  
simulation,         7,      0.76,   0.71761,    0.7601,    881.51,    873.77,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.7,   0.69997,   0.69995,    24.968,    24.968,         0,  2881699,  1152966,  ,  ,  ,  
simulation,        10,      0.65,   0.65004,   0.65002,    18.776,    18.775,         0,  2849899,  895856,  ,  ,  ,  
simulation,        11,       0.6,   0.60023,   0.60022,    15.735,    15.736,         0,  2735844,  721947,  ,  ,  ,  
