wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = UGAL_G_restricted;
shift_by_group = 2;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50017,   0.50017,    11.654,    11.654,         0,  2029214,  851872,  ,  ,  ,  
simulation,         2,      0.74,   0.73996,   0.73994,    19.804,    19.804,         0,  2878584,  1386380,  ,  ,  ,  
simulation,         3,      0.86,   0.74455,   0.86013,    661.72,    661.72,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.74795,   0.79987,    657.31,    657.31,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.74974,   0.77042,    686.81,    686.81,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.75,   0.74986,   0.75001,    48.681,    48.682,         0,  3365328,  1680485,  ,  ,  ,  
simulation,         7,      0.76,      0.75,   0.76026,    576.62,    574.72,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.7,   0.69995,   0.69995,     15.29,     15.29,         0,  2796434,  1237207,  ,  ,  ,  
simulation,        10,      0.65,   0.65002,   0.65002,    13.595,    13.595,         0,  2629625,  1115765,  ,  ,  ,  
simulation,        11,       0.6,   0.60022,   0.60022,    12.704,    12.705,         0,  2437543,  1019917,  ,  ,  ,  
