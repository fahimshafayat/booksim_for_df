wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = vlb;
shift_by_group = 2;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.45081,   0.45725,    954.93,    258.55,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25006,    17.624,    17.624,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.37007,   0.37006,     22.64,    22.641,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.43002,   0.43001,    32.863,    32.864,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45254,   0.45348,    544.69,    225.65,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.44001,   0.44001,    37.133,    37.135,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.45,   0.45003,      0.45,    44.612,    44.611,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.40005,   0.40005,    25.983,    25.984,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35006,   0.35006,    21.203,    21.203,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30005,   0.30005,    18.946,    18.946,         0,  0,  0,  ,  ,  ,  
