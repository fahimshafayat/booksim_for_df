wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = vlb;
shift_by_group = 1;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.24318,   0.48368,    2673.5,    2673.5,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24936,   0.25015,    397.91,    399.47,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.24322,   0.36924,    1815.6,    1815.6,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.24292,   0.30966,    1168.7,    1168.7,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.24281,   0.27986,    714.13,    714.13,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,   0.24644,    0.2598,    541.85,    541.85,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.20011,   0.20011,     19.91,    19.909,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.15,   0.15003,   0.15004,     17.09,     17.09,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.1,   0.10014,   0.10014,    16.005,    16.005,         0,  0,  0,  ,  ,  ,  
