wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_G;
shift_by_group = 1;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    21.635,    21.636,         0,  2402301,  2401509,  ,  ,  ,  
simulation,         2,      0.74,    0.3613,   0.37372,    2583.1,    336.19,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.35993,   0.37257,    2111.9,    335.76,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.36138,   0.37409,    1783.1,    329.44,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.36443,   0.37712,    1544.8,    320.09,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.37215,   0.38484,    1213.4,    293.44,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.45012,   0.45013,    17.759,    17.759,         0,  2396043,  1926240,  ,  ,  ,  
simulation,         9,       0.4,   0.40022,   0.40022,    15.374,    15.375,         0,  2337317,  1504829,  ,  ,  ,  
simulation,        10,      0.35,   0.35016,   0.35016,    13.836,    13.837,         0,  2160142,  1201295,  ,  ,  ,  
