wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = min;
shift_by_group = 1;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.24977,   0.47406,    2473.2,    2473.2,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24965,    0.2501,    219.89,    220.86,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.24975,   0.36958,    1593.5,    1593.5,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.24972,   0.30977,    956.93,    956.93,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.24967,   0.27994,    539.57,    539.57,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,      0.25,   0.26007,    989.08,    989.08,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.20008,   0.20008,    11.615,    11.616,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.15,   0.14995,   0.14995,    10.353,    10.353,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.1,  0.099926,  0.099925,    9.8972,    9.8973,         0,  0,  0,  ,  ,  ,  
