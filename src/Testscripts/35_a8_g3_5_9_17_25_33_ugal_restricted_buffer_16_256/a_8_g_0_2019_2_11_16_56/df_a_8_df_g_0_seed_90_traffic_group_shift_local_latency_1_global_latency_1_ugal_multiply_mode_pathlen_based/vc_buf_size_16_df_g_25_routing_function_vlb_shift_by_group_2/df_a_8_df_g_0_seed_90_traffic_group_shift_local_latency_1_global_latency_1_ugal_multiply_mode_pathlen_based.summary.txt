wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = vlb;
shift_by_group = 2;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.33488,   0.34666,    1572.6,    336.36,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25008,    18.416,    18.417,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.33792,   0.34377,     692.5,    277.88,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31007,   0.31007,    22.445,    22.445,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,   0.34012,   0.34014,    32.971,    32.962,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.35,   0.34454,    0.3458,     497.7,    241.92,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.30005,   0.30006,    21.321,    21.322,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20009,   0.20009,    17.077,    17.077,         0,  0,  0,  ,  ,  ,  
