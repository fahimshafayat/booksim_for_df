wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
perm_seed = 60;
routing_function = UGAL_L_restricted;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50007,   0.50006,    32.345,    32.349,         0,  3678534,  945547,  ,  ,  ,  
simulation,         2,      0.74,   0.74003,   0.74002,     33.98,    33.983,         0,  5559781,  1285813,  ,  ,  ,  
simulation,         3,      0.86,   0.85992,   0.85991,    37.003,    37.007,         0,  6378545,  1576950,  ,  ,  ,  
simulation,         4,      0.92,   0.91994,   0.91995,    40.583,    40.589,         0,  6720285,  1795040,  ,  ,  ,  
simulation,         5,      0.95,   0.94994,   0.94988,     45.45,    45.471,         0,  6900009,  1920954,  ,  ,  ,  
simulation,         6,      0.97,   0.96982,   0.96983,    54.197,    54.265,         0,  7015597,  2011929,  ,  ,  ,  
simulation,         7,      0.98,   0.97982,   0.97984,    61.929,    62.066,         0,  7074021,  2062084,  ,  ,  ,  
simulation,         9,       0.9,   0.89987,   0.89987,    39.051,    39.053,         0,  6609654,  1717661,  ,  ,  ,  
simulation,        10,      0.85,   0.84997,   0.84998,    36.596,    36.599,         0,  6317127,  1546181,  ,  ,  ,  
simulation,        11,       0.8,   0.80004,   0.80003,    35.086,     35.09,         0,  5993995,  1406297,  ,  ,  ,  
