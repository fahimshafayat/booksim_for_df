wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 62;
routing_function = UGAL_G;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.1209,    0.1241,    3439.2,    259.66,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.11921,   0.12239,    2043.1,    256.96,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.10222,   0.10428,    743.57,     189.3,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.069821,  0.069821,    343.95,    57.576,         0,  10665796,  3454352,  ,  ,  ,  
simulation,         5,       0.1,  0.091133,  0.091838,    631.65,    144.91,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.08,  0.078647,  0.078643,    482.09,    80.956,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.05,   0.05009,   0.05009,    40.884,    40.891,         0,  2338562,  820191,  ,  ,  ,  
