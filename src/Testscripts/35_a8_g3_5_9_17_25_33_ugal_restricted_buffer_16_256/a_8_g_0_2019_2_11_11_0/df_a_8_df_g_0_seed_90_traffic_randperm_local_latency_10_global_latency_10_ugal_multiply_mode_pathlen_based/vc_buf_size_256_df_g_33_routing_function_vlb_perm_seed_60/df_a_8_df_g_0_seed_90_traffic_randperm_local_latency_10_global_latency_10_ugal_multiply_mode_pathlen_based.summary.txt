wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 60;
routing_function = vlb;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49203,   0.50004,    519.28,     532.4,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25006,    65.448,    65.451,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.37007,   0.37006,    69.688,    69.691,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.43002,   0.43001,    76.637,    76.638,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,      0.46,   0.45999,    86.922,    86.922,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,   0.47996,   0.48001,    112.74,    112.81,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.49,   0.48687,    0.4894,    360.75,    391.26,         0,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.45001,      0.45,    82.217,    82.217,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.40006,   0.40005,    72.189,    72.191,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35007,   0.35006,    68.535,    68.538,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30005,   0.30005,    66.622,    66.625,         0,  0,  0,  ,  ,  ,  
