wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 25;
perm_seed = 62;
routing_function = UGAL_G;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    45.445,    45.451,         0,  18920826,  4845238,  ,  ,  ,  
simulation,         2,      0.74,   0.64856,   0.72832,     557.3,     557.3,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.62005,   0.62008,    85.156,    85.182,         0,  20549587,  8986070,  ,  ,  ,  
simulation,         4,      0.68,   0.65131,   0.65575,    871.83,    780.38,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.65,   0.65009,   0.65012,    155.56,    155.68,         0,  19886150,  11168970,  ,  ,  ,  
simulation,         6,      0.66,    0.6527,   0.65493,    497.08,    439.34,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.6,   0.60006,   0.60008,    70.231,    70.246,         0,  20594698,  7962179,  ,  ,  ,  
simulation,         9,      0.55,   0.55004,   0.55004,    53.195,    53.203,         0,  20048705,  6103448,  ,  ,  ,  
