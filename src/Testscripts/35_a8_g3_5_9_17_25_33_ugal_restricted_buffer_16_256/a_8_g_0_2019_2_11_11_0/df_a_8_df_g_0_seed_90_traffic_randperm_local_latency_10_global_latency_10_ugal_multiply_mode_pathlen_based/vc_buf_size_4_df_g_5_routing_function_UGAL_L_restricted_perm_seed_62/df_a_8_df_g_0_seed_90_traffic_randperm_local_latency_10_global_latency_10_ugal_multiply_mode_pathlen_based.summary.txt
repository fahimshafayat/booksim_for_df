wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
perm_seed = 62;
routing_function = UGAL_L_restricted;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.18507,   0.18835,    2746.2,    171.96,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.18079,   0.18386,    1014.5,    161.87,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12994,   0.12993,     34.28,    34.285,         0,  884625,  334395,  ,  ,  ,  
simulation,         4,      0.19,   0.18041,   0.18033,    986.72,    87.827,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.16,    0.1599,    0.1599,    53.813,    41.086,         0,  1342656,  467150,  ,  ,  ,  
simulation,         6,      0.17,   0.16903,   0.16904,    463.05,    53.975,         0,  2568782,  874695,  ,  ,  ,  
simulation,         7,      0.18,   0.17611,    0.1761,    506.13,    67.913,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.15,   0.14996,   0.14995,    36.499,    36.501,         0,  1038121,  369765,  ,  ,  ,  
simulation,         9,       0.1,  0.099927,  0.099925,    33.428,    33.431,         0,  662967,  274613,  ,  ,  ,  
simulation,        10,      0.05,  0.049785,  0.049784,    33.186,    33.188,         0,  325425,  142959,  ,  ,  ,  
