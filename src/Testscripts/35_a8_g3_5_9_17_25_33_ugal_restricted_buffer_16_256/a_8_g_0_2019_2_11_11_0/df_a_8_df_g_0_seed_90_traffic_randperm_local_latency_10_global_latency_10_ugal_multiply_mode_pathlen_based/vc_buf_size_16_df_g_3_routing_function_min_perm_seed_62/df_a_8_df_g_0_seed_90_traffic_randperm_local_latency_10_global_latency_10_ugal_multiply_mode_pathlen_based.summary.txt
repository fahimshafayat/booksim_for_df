wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 62;
routing_function = min;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49222,   0.49222,    529.57,    58.224,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25021,   0.25022,    27.648,    27.652,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36846,   0.36846,    434.89,    34.257,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42689,   0.42689,    341.36,    34.096,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.39771,   0.39771,    311.52,    34.009,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.38,   0.37824,   0.37824,    283.27,    34.142,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.35,   0.34885,   0.34885,    331.85,    34.498,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.29984,   0.29985,    52.494,    35.389,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,    0.2001,   0.20011,    27.518,    27.521,         0,  0,  0,  ,  ,  ,  
