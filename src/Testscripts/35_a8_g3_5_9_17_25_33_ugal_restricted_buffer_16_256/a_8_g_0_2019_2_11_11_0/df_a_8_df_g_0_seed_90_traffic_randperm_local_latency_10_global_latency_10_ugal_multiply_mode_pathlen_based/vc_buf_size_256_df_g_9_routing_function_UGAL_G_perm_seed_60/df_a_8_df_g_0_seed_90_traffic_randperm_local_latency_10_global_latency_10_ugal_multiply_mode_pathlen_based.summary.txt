wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 9;
perm_seed = 60;
routing_function = UGAL_G;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4998,   0.49978,    34.605,    34.608,         0,  7954893,  603820,  ,  ,  ,  
simulation,         2,      0.74,    0.7403,   0.74028,    42.847,    42.852,         0,  11429824,  1245040,  ,  ,  ,  
simulation,         3,      0.86,   0.86012,    0.8601,    82.189,    82.225,         0,  12262886,  2493382,  ,  ,  ,  
simulation,         4,      0.92,   0.89042,    0.8939,    759.57,     626.9,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.89,   0.88987,   0.89004,    185.36,     185.5,         0,  12208170,  3132572,  ,  ,  ,  
simulation,         6,       0.9,    0.8887,   0.89178,    507.98,     460.6,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.85,   0.85016,   0.85015,     73.74,    73.766,         0,  12240850,  2337286,  ,  ,  ,  
simulation,         9,       0.8,   0.80024,   0.80024,     53.02,     53.03,         0,  11982030,  1726895,  ,  ,  ,  
simulation,        10,      0.75,    0.7503,   0.75028,    43.991,    43.995,         0,  11538562,  1308937,  ,  ,  ,  
