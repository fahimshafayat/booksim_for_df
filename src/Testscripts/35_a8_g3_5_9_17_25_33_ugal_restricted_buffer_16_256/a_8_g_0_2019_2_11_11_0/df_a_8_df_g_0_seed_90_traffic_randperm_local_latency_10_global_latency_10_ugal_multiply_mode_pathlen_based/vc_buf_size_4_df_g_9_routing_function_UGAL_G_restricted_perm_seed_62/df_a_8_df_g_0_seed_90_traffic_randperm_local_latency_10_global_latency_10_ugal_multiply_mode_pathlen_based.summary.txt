wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 9;
perm_seed = 62;
routing_function = UGAL_G_restricted;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.15907,   0.16235,    2983.8,    201.51,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.15484,   0.15784,    1481.8,    187.53,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12931,   0.12931,    201.49,    47.594,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.069964,  0.069959,    35.087,     35.09,         0,  910491,  295924,  ,  ,  ,  
simulation,         5,       0.1,  0.099936,  0.099933,    35.648,    35.652,         0,  1335983,  387662,  ,  ,  ,  
simulation,         6,      0.11,   0.10991,   0.10991,    36.313,    36.318,         0,  1482852,  413370,  ,  ,  ,  
simulation,         7,      0.12,   0.11984,   0.11984,    49.053,     39.49,         0,  1993755,  534805,  ,  ,  ,  
simulation,         9,      0.05,  0.049933,   0.04993,    35.114,    35.119,         0,  638031,  223243,  ,  ,  ,  
