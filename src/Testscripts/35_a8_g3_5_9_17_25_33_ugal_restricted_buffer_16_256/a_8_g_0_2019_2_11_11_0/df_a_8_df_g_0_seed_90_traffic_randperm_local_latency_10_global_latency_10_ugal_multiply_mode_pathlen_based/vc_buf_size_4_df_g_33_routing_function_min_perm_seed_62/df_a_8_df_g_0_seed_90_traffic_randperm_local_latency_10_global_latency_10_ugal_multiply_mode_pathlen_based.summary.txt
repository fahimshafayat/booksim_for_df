wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 62;
routing_function = min;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.10487,   0.10801,    3656.0,    289.81,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.10366,   0.10679,    2444.4,     291.1,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.090491,  0.092678,    1042.3,    228.44,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.064755,  0.065277,    528.98,     148.8,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.039917,  0.039919,    327.54,    55.401,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.05,  0.049305,  0.049307,    526.52,    78.841,         1,  0,  0,  ,  ,  ,  
