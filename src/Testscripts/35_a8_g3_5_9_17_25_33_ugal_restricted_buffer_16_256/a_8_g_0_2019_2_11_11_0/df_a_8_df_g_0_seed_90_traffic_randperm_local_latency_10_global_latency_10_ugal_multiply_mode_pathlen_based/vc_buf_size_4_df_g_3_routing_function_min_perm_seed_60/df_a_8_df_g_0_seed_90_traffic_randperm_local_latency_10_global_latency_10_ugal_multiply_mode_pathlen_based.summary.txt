wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 60;
routing_function = min;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.17691,   0.17995,    2564.6,     169.1,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,    0.1601,    0.1631,    1372.2,    177.88,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12797,   0.12808,     568.8,    68.621,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.070205,  0.070203,    29.188,    29.191,         0,  0,  0,  ,  ,  ,  
simulation,         5,       0.1,  0.099687,  0.099689,    330.42,    36.762,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.11,   0.10944,   0.10945,    487.26,    39.428,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.12,   0.11896,   0.11896,    458.68,    54.864,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.05,  0.050084,  0.050078,    28.574,    28.579,         0,  0,  0,  ,  ,  ,  
