wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
perm_seed = 62;
routing_function = UGAL_G_restricted;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50007,   0.50006,     32.05,    32.053,         0,  3942905,  741453,  ,  ,  ,  
simulation,         2,      0.74,   0.74002,   0.74002,    33.667,    33.671,         0,  5944189,  989654,  ,  ,  ,  
simulation,         3,      0.86,   0.85992,   0.85991,     36.56,    36.563,         0,  6823519,  1235144,  ,  ,  ,  
simulation,         4,      0.92,   0.91996,   0.91995,    39.211,    39.215,         0,  7205394,  1416042,  ,  ,  ,  
simulation,         5,      0.95,   0.94989,   0.94988,    41.177,    41.181,         0,  7388982,  1514186,  ,  ,  ,  
simulation,         6,      0.97,   0.96984,   0.96983,    42.983,    42.987,         0,  7511550,  1579945,  ,  ,  ,  
simulation,         7,      0.98,   0.97985,   0.97984,    44.131,    44.135,         0,  7572724,  1612308,  ,  ,  ,  
simulation,         9,       0.9,   0.89986,   0.89987,    38.144,    38.147,         0,  7079732,  1353580,  ,  ,  ,  
simulation,        10,      0.85,   0.84999,   0.84998,    36.199,    36.201,         0,  6759697,  1204979,  ,  ,  ,  
simulation,        11,       0.8,   0.80004,   0.80003,     34.75,    34.753,         0,  6410097,  1086914,  ,  ,  ,  
