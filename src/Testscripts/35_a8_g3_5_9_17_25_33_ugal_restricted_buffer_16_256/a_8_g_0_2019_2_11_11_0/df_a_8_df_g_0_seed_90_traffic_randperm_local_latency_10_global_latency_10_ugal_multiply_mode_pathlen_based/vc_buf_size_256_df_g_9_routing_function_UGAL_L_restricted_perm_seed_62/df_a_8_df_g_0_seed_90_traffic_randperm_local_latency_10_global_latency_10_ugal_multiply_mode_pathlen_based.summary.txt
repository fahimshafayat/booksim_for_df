wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 9;
perm_seed = 62;
routing_function = UGAL_L_restricted;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4998,   0.49978,    35.869,    35.873,         0,  6791551,  1826045,  ,  ,  ,  
simulation,         2,      0.74,   0.74036,   0.74028,    69.139,    69.348,         0,  9943456,  2935590,  ,  ,  ,  
simulation,         3,      0.86,   0.83603,   0.83841,    556.13,    407.45,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.79741,   0.79744,    345.35,    138.25,         0,  22457020,  8031758,  ,  ,  ,  
simulation,         5,      0.83,   0.82127,   0.82184,    497.62,    248.41,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.81,   0.80553,   0.80552,    336.52,    161.66,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.75,   0.75027,   0.75028,     72.24,    72.497,         0,  10030017,  3038381,  ,  ,  ,  
simulation,         9,       0.7,   0.70015,   0.70017,    53.398,    53.521,         0,  9554124,  2601852,  ,  ,  ,  
simulation,        10,      0.65,   0.65007,   0.65004,    41.533,    41.565,         0,  8928013,  2322984,  ,  ,  ,  
