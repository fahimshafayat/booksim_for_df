wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 60;
routing_function = UGAL_G_restricted;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.13364,   0.13689,    3284.1,    237.74,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.13101,   0.13415,    1843.8,    231.71,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.11065,   0.11248,    536.24,    152.77,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.070095,  0.070094,     40.78,    40.787,         0,  3069807,  1358311,  ,  ,  ,  
simulation,         5,       0.1,  0.096187,  0.096202,    701.39,    100.21,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.08,   0.07992,   0.07992,    185.89,    48.046,         0,  8887055,  3824394,  ,  ,  ,  
simulation,         7,      0.09,  0.088973,  0.088971,    493.36,    71.986,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.05,  0.050091,   0.05009,     39.74,    39.746,         0,  2152908,  1008047,  ,  ,  ,  
