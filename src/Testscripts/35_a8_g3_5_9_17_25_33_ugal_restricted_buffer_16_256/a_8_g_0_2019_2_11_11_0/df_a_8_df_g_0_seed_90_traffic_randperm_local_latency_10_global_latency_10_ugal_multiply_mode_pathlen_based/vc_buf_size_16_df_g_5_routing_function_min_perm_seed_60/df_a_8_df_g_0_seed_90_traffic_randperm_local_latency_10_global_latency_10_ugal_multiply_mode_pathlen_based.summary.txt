wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
perm_seed = 60;
routing_function = min;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49234,   0.49235,    522.35,    60.871,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25018,   0.25018,    29.989,    29.993,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36894,   0.36894,    286.63,    34.138,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42812,   0.42813,    219.66,    34.329,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.39854,   0.39855,     385.9,    34.092,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.41,   0.40844,   0.40844,    429.15,    34.122,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.42,   0.41832,   0.41832,    461.77,    34.195,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.35,   0.34917,   0.34916,    215.66,    34.234,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.3,   0.29979,   0.29979,    44.521,    34.699,         0,  0,  0,  ,  ,  ,  
