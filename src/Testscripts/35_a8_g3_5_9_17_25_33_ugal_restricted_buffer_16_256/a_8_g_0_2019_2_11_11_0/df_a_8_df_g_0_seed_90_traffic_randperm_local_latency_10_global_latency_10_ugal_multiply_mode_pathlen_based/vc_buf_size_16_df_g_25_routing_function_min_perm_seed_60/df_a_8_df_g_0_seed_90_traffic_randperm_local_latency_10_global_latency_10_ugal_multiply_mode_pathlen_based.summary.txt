wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 25;
perm_seed = 60;
routing_function = min;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.36497,   0.37399,    941.92,    229.92,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24147,   0.24144,    666.56,    120.81,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.13009,   0.13009,    33.762,    33.763,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.19,   0.18905,   0.18905,    285.25,    50.766,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.16,   0.16007,   0.16007,    34.334,    34.338,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.17,   0.16986,   0.16987,    103.45,    47.965,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.18,   0.17946,   0.17946,    304.04,    48.867,         0,  0,  0,  ,  ,  ,  
simulation,         8,      0.15,   0.15006,   0.15007,    33.947,    33.949,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.1,   0.10007,   0.10007,     33.66,    33.661,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.05,  0.050053,  0.050053,    33.569,     33.57,         0,  0,  0,  ,  ,  ,  
