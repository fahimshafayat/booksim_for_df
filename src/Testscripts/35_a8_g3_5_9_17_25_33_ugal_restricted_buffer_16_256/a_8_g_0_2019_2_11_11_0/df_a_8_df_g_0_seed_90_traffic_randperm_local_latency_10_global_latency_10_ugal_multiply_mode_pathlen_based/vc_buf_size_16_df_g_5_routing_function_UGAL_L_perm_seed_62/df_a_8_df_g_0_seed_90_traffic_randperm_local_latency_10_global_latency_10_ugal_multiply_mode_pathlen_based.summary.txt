wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
perm_seed = 62;
routing_function = UGAL_L;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    33.157,    33.162,         0,  4214804,  471100,  ,  ,  ,  
simulation,         2,      0.74,   0.65761,   0.66108,    791.15,    99.785,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,    0.6126,   0.61257,    508.47,    53.778,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55998,   0.55998,    34.148,    34.151,         0,  4744683,  503704,  ,  ,  ,  
simulation,         5,      0.59,   0.58915,   0.58914,     133.0,    41.312,         0,  9465046,  992757,  ,  ,  ,  
simulation,         6,       0.6,   0.59767,   0.59768,    313.94,    48.332,         0,  10036677,  1051429,  ,  ,  ,  
simulation,         7,      0.61,   0.60576,   0.60576,    450.91,    51.336,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.55,   0.54996,   0.54996,    33.773,    33.777,         0,  4656944,  497666,  ,  ,  ,  
simulation,        11,      0.45,   0.45012,   0.45013,    32.941,    32.945,         0,  3772916,  444285,  ,  ,  ,  
