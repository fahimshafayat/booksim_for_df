wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 60;
routing_function = UGAL_L_restricted;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.14242,   0.14576,    3229.1,    228.87,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.14017,   0.14337,    1683.0,    219.71,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.11633,   0.11716,    738.75,    135.69,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.070095,  0.070094,    42.755,    42.762,         0,  2717472,  1711318,  ,  ,  ,  
simulation,         5,       0.1,  0.098218,  0.098243,    499.84,    75.876,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.08,  0.080021,  0.080019,     78.25,    47.309,         0,  4332444,  2693630,  ,  ,  ,  
simulation,         7,      0.09,   0.08961,  0.089611,    459.35,    60.025,         0,  9078155,  5618348,  ,  ,  ,  
simulation,         8,      0.05,  0.050089,   0.05009,    41.737,    41.743,         0,  1913901,  1247484,  ,  ,  ,  
