wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 9;
perm_seed = 60;
routing_function = vlb;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.47062,   0.47622,    559.38,    215.08,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24998,   0.24997,    62.176,    62.183,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36994,   0.36992,    65.554,    65.561,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42993,   0.42991,    70.576,     70.58,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45985,   0.45986,    82.996,    82.486,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,    0.4712,    0.4719,     612.7,    166.08,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.46803,   0.46804,    429.19,    115.47,         0,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44996,   0.44994,    74.621,    74.629,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39986,   0.39984,    67.399,    67.407,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,    0.3499,   0.34989,    64.679,    64.684,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.29992,   0.29991,    63.156,    63.162,         0,  0,  0,  ,  ,  ,  
