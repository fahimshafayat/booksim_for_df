wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 17;
perm_seed = 62;
routing_function = min;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.11712,   0.12032,    3424.3,     265.5,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.11385,   0.11692,    2149.1,    261.97,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.10037,   0.10263,    823.24,    204.76,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.069277,  0.069277,    519.84,    68.512,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.039979,  0.039975,    33.605,    33.607,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.05,   0.04996,  0.049959,    34.384,    34.388,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.06,  0.059949,  0.059951,    160.06,    42.133,         0,  0,  0,  ,  ,  ,  
