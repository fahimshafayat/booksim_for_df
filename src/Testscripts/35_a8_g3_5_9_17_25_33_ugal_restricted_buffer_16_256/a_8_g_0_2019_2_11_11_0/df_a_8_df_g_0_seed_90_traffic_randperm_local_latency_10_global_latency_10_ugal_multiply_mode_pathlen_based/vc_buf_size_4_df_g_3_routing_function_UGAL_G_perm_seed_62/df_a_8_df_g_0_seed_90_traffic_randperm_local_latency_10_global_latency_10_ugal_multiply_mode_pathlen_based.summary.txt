wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 62;
routing_function = UGAL_G;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.16503,   0.16825,    2903.0,    189.93,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.16333,   0.16641,    1116.1,    179.77,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,    0.1296,   0.12961,    272.05,    37.546,         0,  1272832,  186580,  ,  ,  ,  
simulation,         4,      0.19,   0.16375,    0.1647,    890.03,    108.33,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.16,   0.15466,   0.15471,    756.96,    82.356,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.14,   0.13918,   0.13918,    328.68,    45.796,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.1,   0.10014,   0.10014,    30.489,    30.493,         0,  486349,  84846,  ,  ,  ,  
simulation,         8,      0.05,  0.050084,  0.050078,    30.607,    30.617,         0,  233762,  52168,  ,  ,  ,  
