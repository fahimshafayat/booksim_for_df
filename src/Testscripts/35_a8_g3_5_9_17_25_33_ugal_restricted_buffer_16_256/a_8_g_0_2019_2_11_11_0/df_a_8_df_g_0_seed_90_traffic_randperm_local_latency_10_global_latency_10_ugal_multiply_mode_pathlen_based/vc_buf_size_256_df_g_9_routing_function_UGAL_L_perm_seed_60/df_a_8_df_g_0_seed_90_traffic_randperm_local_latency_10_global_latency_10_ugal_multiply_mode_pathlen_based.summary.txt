wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 9;
perm_seed = 60;
routing_function = UGAL_L;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4998,   0.49978,    36.579,    36.584,         0,  7454619,  1104968,  ,  ,  ,  
simulation,         2,      0.74,   0.74034,   0.74028,     97.07,    97.419,         0,  10393645,  2395052,  ,  ,  ,  
simulation,         3,      0.86,   0.78027,   0.82803,    770.67,    720.91,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,    0.7773,    0.7829,    646.93,    538.25,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.76623,   0.76661,    471.83,    262.66,         0,  22522627,  6963465,  ,  ,  ,  
simulation,         6,      0.78,   0.77079,   0.77129,     495.4,    370.72,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.75,   0.75024,   0.75028,    112.31,    112.79,         0,  10501312,  2658055,  ,  ,  ,  
simulation,         8,       0.7,   0.70021,   0.70017,    66.034,    66.224,         0,  10207608,  1855205,  ,  ,  ,  
simulation,         9,      0.65,   0.65007,   0.65004,    52.773,    52.887,         0,  9648464,  1533074,  ,  ,  ,  
simulation,        10,       0.6,   0.59998,   0.59996,    44.305,    44.359,         0,  8972251,  1342881,  ,  ,  ,  
