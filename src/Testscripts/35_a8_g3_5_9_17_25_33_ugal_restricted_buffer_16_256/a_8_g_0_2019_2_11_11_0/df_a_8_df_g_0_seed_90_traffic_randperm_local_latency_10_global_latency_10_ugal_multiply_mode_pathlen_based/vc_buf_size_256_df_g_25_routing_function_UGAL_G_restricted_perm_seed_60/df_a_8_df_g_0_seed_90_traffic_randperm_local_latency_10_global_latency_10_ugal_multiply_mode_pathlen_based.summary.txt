wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 25;
perm_seed = 60;
routing_function = UGAL_G_restricted;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    41.195,    41.199,         0,  17688334,  6189956,  ,  ,  ,  
simulation,         2,      0.74,   0.69995,   0.72919,    491.54,    488.34,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.62007,   0.62008,    53.251,    53.257,         0,  20519330,  9106434,  ,  ,  ,  
simulation,         4,      0.68,   0.68007,   0.68008,    83.291,     83.31,         0,  20619421,  11926994,  ,  ,  ,  
simulation,         5,      0.71,   0.70326,   0.70515,    495.74,    424.28,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.69,   0.69006,   0.69006,    99.567,      99.6,         0,  20427451,  12621680,  ,  ,  ,  
simulation,         7,       0.7,   0.70004,   0.70005,    135.72,    135.78,         0,  20121123,  13458059,  ,  ,  ,  
simulation,         9,      0.65,    0.6501,   0.65012,    62.385,    62.394,         0,  20758751,  10321922,  ,  ,  ,  
simulation,        10,       0.6,   0.60007,   0.60008,     49.54,    49.545,         0,  20225397,  8442815,  ,  ,  ,  
simulation,        11,      0.55,   0.55004,   0.55004,    43.945,     43.95,         0,  19140954,  7131675,  ,  ,  ,  
