wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 62;
routing_function = UGAL_L_restricted;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.14098,    0.1443,    3216.3,    230.51,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,    0.1385,    0.1417,    1677.7,    221.36,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.11494,   0.11581,    832.44,    143.89,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.070095,  0.070094,    43.129,    43.137,         0,  2696136,  1731751,  ,  ,  ,  
simulation,         5,       0.1,  0.097915,  0.097939,    557.77,    81.415,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.08,  0.080021,  0.080021,    90.864,    47.662,         0,  5677100,  3599543,  ,  ,  ,  
simulation,         7,      0.09,   0.08938,  0.089382,    398.75,    61.718,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.05,  0.050089,   0.05009,    41.901,    41.906,         0,  1898979,  1259085,  ,  ,  ,  
