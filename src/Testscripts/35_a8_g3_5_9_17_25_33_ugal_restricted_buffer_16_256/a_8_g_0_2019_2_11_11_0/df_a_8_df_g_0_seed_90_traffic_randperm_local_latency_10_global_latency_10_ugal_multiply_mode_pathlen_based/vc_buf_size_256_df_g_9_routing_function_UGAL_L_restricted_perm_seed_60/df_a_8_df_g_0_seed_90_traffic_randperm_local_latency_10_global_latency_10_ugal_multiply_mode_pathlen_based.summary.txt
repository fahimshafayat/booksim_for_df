wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 9;
perm_seed = 60;
routing_function = UGAL_L_restricted;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4998,   0.49978,    35.867,     35.87,         0,  6783890,  1773395,  ,  ,  ,  
simulation,         2,      0.74,   0.74031,   0.74028,    74.078,    74.354,         0,  9925480,  2855067,  ,  ,  ,  
simulation,         3,      0.86,   0.82755,   0.82817,    745.86,    555.92,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.79432,   0.79431,    437.43,    217.34,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.76919,   0.76952,    161.35,    145.55,         0,  14661842,  4655444,  ,  ,  ,  
simulation,         6,      0.78,   0.77772,   0.77785,    300.26,    166.75,         0,  21311713,  7036385,  ,  ,  ,  
simulation,         7,      0.79,   0.78669,   0.78687,    423.35,    196.32,         0,  22611753,  7831007,  ,  ,  ,  
simulation,         8,      0.75,   0.75027,   0.75028,    80.889,    81.164,         0,  9993736,  2960033,  ,  ,  ,  
simulation,         9,       0.7,    0.7002,   0.70017,    56.227,    56.371,         0,  9516619,  2533838,  ,  ,  ,  
simulation,        10,      0.65,   0.65005,   0.65004,     46.89,    46.973,         0,  8922906,  2279537,  ,  ,  ,  
simulation,        11,       0.6,   0.59995,   0.59996,    39.999,    40.031,         0,  8227516,  2082545,  ,  ,  ,  
