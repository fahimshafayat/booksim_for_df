wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 62;
routing_function = UGAL_L;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.17305,   0.17631,    2823.4,    184.72,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.17377,   0.17678,     966.8,    165.54,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12987,   0.12987,    135.82,    37.815,         0,  1056923,  278281,  ,  ,  ,  
simulation,         4,      0.19,   0.17359,   0.17443,    650.83,    92.922,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.16,   0.15861,   0.15861,    456.08,    54.024,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.14,   0.13948,   0.13949,    306.28,    38.678,         0,  1269924,  321609,  ,  ,  ,  
simulation,         7,      0.15,   0.14921,   0.14919,    308.62,     41.16,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.1,   0.10014,   0.10014,    32.488,    32.495,         0,  440285,  130960,  ,  ,  ,  
simulation,        10,      0.05,  0.050085,  0.050078,    32.997,    33.008,         0,  209103,  76827,  ,  ,  ,  
