wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 60;
routing_function = UGAL_G;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.41201,   0.41955,    634.29,    171.55,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25006,    39.048,    39.054,         0,  12493846,  3311340,  ,  ,  ,  
simulation,         3,      0.37,   0.35589,   0.35583,    704.46,    94.317,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30823,   0.30823,    320.75,    54.969,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27969,   0.27969,    129.63,     41.88,         0,  33693206,  8776283,  ,  ,  ,  
simulation,         6,      0.29,   0.28952,   0.28953,    190.21,    43.618,         0,  39992853,  10360689,  ,  ,  ,  
simulation,         7,       0.3,   0.29906,   0.29906,    317.54,    50.025,         0,  45340403,  11688796,  ,  ,  ,  
simulation,        10,       0.2,   0.20009,   0.20009,    38.893,    38.899,         0,  9914905,  2728156,  ,  ,  ,  
simulation,        11,      0.15,   0.15012,   0.15011,    39.036,    39.043,         0,  7355850,  2127280,  ,  ,  ,  
