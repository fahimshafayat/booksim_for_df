wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 17;
perm_seed = 60;
routing_function = UGAL_G;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.13378,   0.13704,    3245.9,     237.7,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.12921,    0.1323,    1855.0,    230.47,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.11263,   0.11449,    487.88,    148.63,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.069959,   0.06996,     38.42,    38.428,         0,  1877913,  392615,  ,  ,  ,  
simulation,         5,       0.1,  0.097239,  0.097244,    537.71,    86.011,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.08,  0.079936,  0.079937,    39.486,    39.492,         0,  2170735,  425544,  ,  ,  ,  
simulation,         7,      0.09,  0.089617,   0.08962,    461.87,    58.799,         0,  8424597,  1556385,  ,  ,  ,  
simulation,         8,      0.05,  0.049961,  0.049959,    38.177,    38.182,         0,  1308826,  312167,  ,  ,  ,  
