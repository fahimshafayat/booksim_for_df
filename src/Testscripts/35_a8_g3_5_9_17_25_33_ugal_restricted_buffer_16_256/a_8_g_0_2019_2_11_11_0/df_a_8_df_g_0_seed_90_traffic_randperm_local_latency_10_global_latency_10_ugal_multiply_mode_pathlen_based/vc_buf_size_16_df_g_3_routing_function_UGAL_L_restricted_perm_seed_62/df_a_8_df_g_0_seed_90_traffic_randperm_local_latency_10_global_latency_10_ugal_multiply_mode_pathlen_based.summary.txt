wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 62;
routing_function = UGAL_L_restricted;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50017,   0.50017,     29.91,    29.913,         0,  2119758,  732820,  ,  ,  ,  
simulation,         2,      0.74,   0.72553,   0.72541,    575.61,    60.508,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.61725,   0.61724,    412.36,    33.952,         0,  5628729,  1838066,  ,  ,  ,  
simulation,         4,      0.68,   0.67486,   0.67487,    340.35,    34.955,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.65,   0.64605,   0.64604,    306.74,    34.175,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.63,   0.62674,   0.62673,    465.95,      34.0,         0,  5906430,  1919691,  ,  ,  ,  
simulation,         7,      0.64,   0.63645,   0.63645,    288.99,    34.066,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.6,   0.59803,   0.59803,    286.82,    33.879,         0,  5082797,  1676235,  ,  ,  ,  
simulation,         9,      0.55,   0.55027,    0.5503,    31.869,    31.842,         0,  2352181,  797879,  ,  ,  ,  
simulation,        11,      0.45,   0.45027,   0.45026,    29.769,    29.773,         0,  1895238,  672091,  ,  ,  ,  
