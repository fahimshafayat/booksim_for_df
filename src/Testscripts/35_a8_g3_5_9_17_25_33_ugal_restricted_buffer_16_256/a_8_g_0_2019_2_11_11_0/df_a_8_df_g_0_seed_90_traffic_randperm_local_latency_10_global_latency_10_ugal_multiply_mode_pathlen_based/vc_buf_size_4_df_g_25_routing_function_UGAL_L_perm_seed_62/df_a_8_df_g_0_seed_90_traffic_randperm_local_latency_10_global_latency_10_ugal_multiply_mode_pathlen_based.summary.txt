wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 25;
perm_seed = 62;
routing_function = UGAL_L;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.13218,   0.13549,    3221.5,    243.84,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.12759,   0.13076,    1854.6,    239.48,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.10881,   0.11081,    576.18,    166.31,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.070036,  0.070036,    60.316,    47.281,         0,  2591990,  1157824,  ,  ,  ,  
simulation,         5,       0.1,  0.094913,  0.094932,    912.74,    120.11,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.08,  0.079561,  0.079562,     369.8,    66.037,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.05,  0.050051,  0.050053,    43.139,    43.144,         0,  1621903,  754269,  ,  ,  ,  
