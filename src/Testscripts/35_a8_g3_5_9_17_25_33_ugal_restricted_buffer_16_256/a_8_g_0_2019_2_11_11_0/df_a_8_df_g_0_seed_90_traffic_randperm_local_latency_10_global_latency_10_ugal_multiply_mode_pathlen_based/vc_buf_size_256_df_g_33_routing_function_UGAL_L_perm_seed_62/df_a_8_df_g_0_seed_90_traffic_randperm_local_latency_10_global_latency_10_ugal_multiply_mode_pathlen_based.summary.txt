wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 62;
routing_function = UGAL_L;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49971,   0.49985,    189.69,    186.68,         0,  22169830,  14578088,  ,  ,  ,  
simulation,         2,      0.74,   0.55337,   0.68759,    1016.9,    1016.8,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,    0.5495,    0.6147,    509.77,    509.77,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.54373,   0.54651,    675.02,    621.82,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,    0.5268,   0.52703,    456.85,    333.36,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.50925,   0.50931,    289.28,    213.47,         0,  46169003,  32970522,  ,  ,  ,  
simulation,         7,      0.52,   0.51847,   0.51868,    414.71,    266.17,         0,  60787568,  47097644,  ,  ,  ,  
simulation,         9,      0.45,   0.44999,      0.45,    97.527,    98.053,         0,  19337702,  9774907,  ,  ,  ,  
simulation,        10,       0.4,   0.40004,   0.40005,    67.157,    67.412,         0,  17671477,  8049314,  ,  ,  ,  
simulation,        11,      0.35,   0.35006,   0.35006,    51.038,     51.12,         0,  15547601,  6775299,  ,  ,  ,  
