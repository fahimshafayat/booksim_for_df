wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 62;
routing_function = UGAL_G_restricted;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4166,   0.41841,    566.67,    42.508,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25006,     11.36,     11.36,         0,  10684754,  5089296,  ,  ,  ,  
simulation,         3,      0.37,   0.35815,   0.35817,    578.85,     24.31,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30893,   0.30893,    329.26,    15.101,         0,  41222243,  18835608,  ,  ,  ,  
simulation,         5,      0.34,   0.33532,   0.33532,    529.91,    19.195,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.32,   0.31805,   0.31805,    311.87,    16.268,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.3,   0.29941,    0.2994,    182.78,    13.464,         0,  36138484,  16611108,  ,  ,  ,  
simulation,         9,       0.2,   0.20009,   0.20009,    11.087,    11.087,         0,  8463927,  4155525,  ,  ,  ,  
simulation,        10,      0.15,   0.15011,   0.15011,    10.904,    10.905,         0,  6303136,  3161780,  ,  ,  ,  
