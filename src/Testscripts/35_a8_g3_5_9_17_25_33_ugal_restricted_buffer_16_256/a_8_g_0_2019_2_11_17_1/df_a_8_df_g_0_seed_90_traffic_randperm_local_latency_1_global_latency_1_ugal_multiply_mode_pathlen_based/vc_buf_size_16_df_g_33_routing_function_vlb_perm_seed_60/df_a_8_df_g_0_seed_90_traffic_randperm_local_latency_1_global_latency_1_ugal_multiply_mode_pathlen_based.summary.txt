wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 60;
routing_function = vlb;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.47258,   0.47832,    527.08,    202.23,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25006,    17.182,    17.182,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.37007,   0.37006,    21.412,    21.413,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.43002,   0.43001,    28.321,    28.323,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45999,   0.45999,    38.641,    38.646,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,   0.47299,   0.47323,     561.9,    182.86,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,    0.4699,      0.47,     54.19,    53.372,         0,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.45001,      0.45,    33.944,    33.946,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.40005,   0.40005,    23.911,    23.912,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35006,   0.35006,    20.273,    20.274,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30005,   0.30005,     18.36,     18.36,         0,  0,  0,  ,  ,  ,  
