wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 60;
routing_function = UGAL_L;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49788,   0.49787,    404.89,    32.285,         0,  54601187,  40124551,  ,  ,  ,  
simulation,         2,      0.74,   0.53887,   0.54884,     931.9,    175.18,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.53412,   0.54113,    485.08,    122.96,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.52798,   0.52804,    1024.4,    86.317,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.51844,   0.51843,    561.72,    56.545,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,    0.5057,    0.5057,    436.28,    38.665,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,      0.45,      0.45,    18.147,    18.149,         0,  18004324,  10521491,  ,  ,  ,  
simulation,         9,       0.4,   0.40005,   0.40005,    14.879,     14.88,         0,  16581252,  8691508,  ,  ,  ,  
simulation,        10,      0.35,   0.35006,   0.35006,    13.171,    13.171,         0,  14692502,  7412415,  ,  ,  ,  
