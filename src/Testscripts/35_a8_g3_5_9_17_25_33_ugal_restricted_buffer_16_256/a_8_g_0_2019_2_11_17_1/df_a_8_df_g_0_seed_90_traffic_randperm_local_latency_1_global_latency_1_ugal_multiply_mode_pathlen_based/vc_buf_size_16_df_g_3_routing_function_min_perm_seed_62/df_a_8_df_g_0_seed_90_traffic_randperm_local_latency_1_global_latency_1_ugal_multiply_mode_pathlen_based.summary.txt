wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 62;
routing_function = min;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49747,   0.49748,    476.65,    14.162,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.68263,   0.68504,     583.0,    67.254,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.60953,   0.60955,    505.68,    41.131,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55586,   0.55587,     324.2,    15.524,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.52658,   0.52659,    300.46,     14.28,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.50727,   0.50727,    278.24,     14.17,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44872,   0.44872,    240.72,    14.371,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.40019,   0.40021,     11.31,    11.281,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35027,   0.35028,    8.7774,    8.7775,         0,  0,  0,  ,  ,  ,  
