wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 9;
perm_seed = 60;
routing_function = UGAL_L;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49979,   0.49978,    12.064,    12.064,         0,  6774598,  1777554,  ,  ,  ,  
simulation,         2,      0.74,   0.73003,   0.73002,    531.16,      38.3,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.62001,      0.62,    14.149,     14.15,         0,  8441985,  2167563,  ,  ,  ,  
simulation,         4,      0.68,   0.68017,   0.68016,      17.0,    17.002,         0,  9128367,  2513551,  ,  ,  ,  
simulation,         5,      0.71,    0.7085,   0.70849,    185.65,    24.466,         0,  18363909,  5460199,  ,  ,  ,  
simulation,         6,      0.72,   0.71614,   0.71614,    427.33,    29.675,         0,  19423612,  5902346,  ,  ,  ,  
simulation,         7,      0.73,   0.72324,   0.72325,    525.76,    31.717,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.7,   0.69974,   0.69974,    40.187,     21.75,         0,  16113567,  4682054,  ,  ,  ,  
simulation,         9,      0.65,   0.65005,   0.65004,    15.239,     15.24,         0,  8806073,  2319035,  ,  ,  ,  
simulation,        10,       0.6,   0.59998,   0.59996,    13.592,    13.593,         0,  8184299,  2080429,  ,  ,  ,  
simulation,        11,      0.55,   0.54982,   0.54981,    12.642,    12.642,         0,  7495695,  1911962,  ,  ,  ,  
