wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 62;
routing_function = min;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.40764,   0.48325,     590.5,     590.5,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24857,    0.2487,    239.76,    172.63,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.13014,   0.13014,     9.548,     9.548,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.19,   0.19007,   0.19007,    9.9282,    9.9284,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.22,   0.21942,   0.21966,    239.94,    154.92,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.23,   0.22914,   0.22934,    363.36,    171.48,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.24,   0.23888,   0.23902,    204.52,    156.99,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.20007,   0.20007,    13.157,    13.159,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.15,   0.15011,   0.15011,    9.6148,    9.6149,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.1,   0.10012,   0.10012,    9.4728,    9.4728,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.05,  0.050091,   0.05009,    9.3843,    9.3843,         0,  0,  0,  ,  ,  ,  
