wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 17;
perm_seed = 62;
routing_function = vlb;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.39876,   0.40198,    997.27,    78.309,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25005,    16.984,    16.985,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36997,   0.36997,    22.087,    22.087,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.39978,   0.40131,    685.13,    73.434,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.39865,   0.39865,    293.37,    53.277,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.41,   0.40031,   0.40038,    626.64,    69.111,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.35,   0.35002,   0.35001,    20.337,    20.338,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.30002,   0.30001,    18.193,    18.194,         0,  0,  0,  ,  ,  ,  
