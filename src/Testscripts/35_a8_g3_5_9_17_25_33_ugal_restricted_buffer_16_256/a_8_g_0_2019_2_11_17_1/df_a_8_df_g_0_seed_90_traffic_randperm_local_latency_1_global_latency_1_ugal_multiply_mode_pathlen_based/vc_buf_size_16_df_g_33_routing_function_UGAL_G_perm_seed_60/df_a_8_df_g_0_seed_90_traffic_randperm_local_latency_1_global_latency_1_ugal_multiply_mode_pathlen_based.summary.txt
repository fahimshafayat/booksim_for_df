wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 60;
routing_function = UGAL_G;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49998,   0.49998,    14.709,     14.71,         0,  23023063,  8549250,  ,  ,  ,  
simulation,         2,      0.74,   0.58888,   0.59746,    712.38,    138.13,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.58899,   0.58898,    895.21,    66.277,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55924,   0.55924,    118.52,    21.825,         0,  53753751,  24059992,  ,  ,  ,  
simulation,         5,      0.59,   0.58104,   0.58106,    531.72,    40.447,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.57,   0.56804,   0.56804,    327.99,    24.833,         0,  79684312,  37060296,  ,  ,  ,  
simulation,         7,      0.58,   0.57574,   0.57574,    379.03,    32.397,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.55,   0.54975,   0.54975,    43.981,    19.456,         0,  42164013,  18172659,  ,  ,  ,  
simulation,        10,      0.45,      0.45,      0.45,    13.146,    13.146,         0,  21187881,  7225448,  ,  ,  ,  
simulation,        11,       0.4,   0.40005,   0.40005,    12.299,      12.3,         0,  18987494,  6270735,  ,  ,  ,  
