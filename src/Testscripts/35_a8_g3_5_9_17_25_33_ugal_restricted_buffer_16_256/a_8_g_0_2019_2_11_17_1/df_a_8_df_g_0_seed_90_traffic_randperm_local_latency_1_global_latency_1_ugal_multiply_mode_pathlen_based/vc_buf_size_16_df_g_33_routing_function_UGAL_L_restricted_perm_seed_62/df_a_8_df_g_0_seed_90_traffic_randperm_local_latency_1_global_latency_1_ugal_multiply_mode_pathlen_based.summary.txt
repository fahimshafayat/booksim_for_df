wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 62;
routing_function = UGAL_L_restricted;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49998,   0.49998,    19.261,    19.261,         0,  18032289,  13579047,  ,  ,  ,  
simulation,         2,      0.74,   0.55254,   0.56252,    883.14,    166.72,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,    0.5639,    0.5668,    617.11,    99.472,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55236,   0.55237,     512.0,    42.929,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.52906,   0.52906,    157.22,    26.285,         0,  42618161,  36079051,  ,  ,  ,  
simulation,         6,      0.54,   0.53767,   0.53767,    392.45,    31.635,         0,  49762994,  43985020,  ,  ,  ,  
simulation,         7,      0.55,   0.54538,   0.54538,    424.75,    36.479,         1,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,      0.45,      0.45,    15.415,    15.417,         0,  16995110,  11414781,  ,  ,  ,  
simulation,        11,       0.4,   0.40005,   0.40005,    13.485,    13.485,         0,  15386089,  9860006,  ,  ,  ,  
