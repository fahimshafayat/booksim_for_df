wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 62;
routing_function = UGAL_G;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.39199,   0.39395,    734.77,    49.558,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24999,   0.24999,    26.137,    11.992,         0,  19783059,  6748133,  ,  ,  ,  
simulation,         3,      0.37,   0.34483,   0.34484,    1166.5,    34.697,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30462,   0.30462,    512.63,    20.696,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27851,   0.27851,    488.48,    16.285,         0,  43359795,  14366034,  ,  ,  ,  
simulation,         6,      0.29,   0.28747,   0.28747,    442.13,    17.799,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.20009,   0.20009,    11.266,    11.267,         0,  9271623,  3348857,  ,  ,  ,  
simulation,         9,      0.15,   0.15011,   0.15011,    11.097,    11.097,         0,  6853172,  2612066,  ,  ,  ,  
simulation,        10,       0.1,   0.10012,   0.10012,     10.99,    10.991,         0,  4512181,  1797689,  ,  ,  ,  
