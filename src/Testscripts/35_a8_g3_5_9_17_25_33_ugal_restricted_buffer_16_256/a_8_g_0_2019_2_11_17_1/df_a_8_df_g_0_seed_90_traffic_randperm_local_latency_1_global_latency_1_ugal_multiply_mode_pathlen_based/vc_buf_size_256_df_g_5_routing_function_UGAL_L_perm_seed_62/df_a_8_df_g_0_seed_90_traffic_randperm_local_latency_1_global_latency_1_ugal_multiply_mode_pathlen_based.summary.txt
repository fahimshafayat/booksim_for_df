wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
perm_seed = 62;
routing_function = UGAL_L;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50005,   0.50006,    11.012,    11.012,         0,  3776649,  905555,  ,  ,  ,  
simulation,         2,      0.74,   0.74002,   0.74002,    13.164,    13.164,         0,  5737150,  1193828,  ,  ,  ,  
simulation,         3,      0.86,   0.85993,   0.85991,    19.648,    19.669,         0,  6546028,  1543031,  ,  ,  ,  
simulation,         4,      0.92,   0.91906,   0.91909,    121.07,    88.993,         0,  11226259,  3027256,  ,  ,  ,  
simulation,         5,      0.95,   0.94299,   0.94448,    402.18,    190.03,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.93,   0.92836,   0.92837,    183.08,    97.354,         0,  12803641,  3527925,  ,  ,  ,  
simulation,         7,      0.94,   0.93675,   0.93683,    308.01,    115.99,         0,  13996829,  3917657,  ,  ,  ,  
simulation,         8,       0.9,   0.89979,   0.89991,    43.022,    43.094,         0,  9031631,  2313824,  ,  ,  ,  
simulation,         9,      0.85,   0.84996,   0.84998,    18.636,    18.647,         0,  6493435,  1501833,  ,  ,  ,  
simulation,        10,       0.8,   0.80002,   0.80003,    14.651,    14.651,         0,  6158750,  1335611,  ,  ,  ,  
simulation,        11,      0.75,   0.75002,   0.75001,    13.354,    13.354,         0,  5809874,  1215664,  ,  ,  ,  
