wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 17;
perm_seed = 60;
routing_function = UGAL_G;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.43075,   0.43162,    969.41,    39.636,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25005,    10.845,    10.845,         0,  6606850,  1498249,  ,  ,  ,  
simulation,         3,      0.37,   0.36452,   0.36452,    522.58,    20.061,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31001,   0.31001,    11.269,    11.269,         0,  8343714,  1705423,  ,  ,  ,  
simulation,         5,      0.34,   0.33967,   0.33967,    131.41,    13.576,         0,  27090881,  5264595,  ,  ,  ,  
simulation,         6,      0.35,   0.34851,   0.34851,    419.52,     15.76,         0,  33711545,  6470098,  ,  ,  ,  
simulation,         7,      0.36,   0.35683,   0.35683,    453.42,    18.233,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.30002,   0.30001,    11.151,    11.151,         0,  8047938,  1677562,  ,  ,  ,  
simulation,        11,       0.2,   0.20008,   0.20007,    10.702,    10.702,         0,  5199439,  1284539,  ,  ,  ,  
