wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
perm_seed = 60;
routing_function = UGAL_L_restricted;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49984,   0.49984,    33.108,    11.208,         0,  5643617,  2276461,  ,  ,  ,  
simulation,         2,      0.74,   0.66737,    0.6683,    631.65,    26.959,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.61616,   0.61616,    280.55,    13.846,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,    0.5584,   0.55839,    257.56,    11.758,         0,  8041530,  3080486,  ,  ,  ,  
simulation,         5,      0.59,    0.5877,    0.5877,    393.77,    12.416,         0,  9584427,  3588425,  ,  ,  ,  
simulation,         6,       0.6,   0.59725,   0.59725,    477.74,     12.69,         0,  10209130,  3786300,  ,  ,  ,  
simulation,         7,      0.61,   0.60669,   0.60669,    245.54,    13.092,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.55,    0.5486,    0.5486,    222.29,    11.627,         0,  7642274,  2954211,  ,  ,  ,  
simulation,        11,      0.45,   0.45013,   0.45013,    10.434,    10.434,         0,  2929355,  1230055,  ,  ,  ,  
