wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 60;
routing_function = UGAL_G;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50017,   0.50017,    9.8753,    9.8756,         0,  2366834,  333872,  ,  ,  ,  
simulation,         2,      0.74,   0.73995,   0.73994,     11.27,     11.27,         0,  3550763,  446645,  ,  ,  ,  
simulation,         3,      0.86,   0.86002,   0.86001,    13.187,    13.187,         0,  4067950,  577838,  ,  ,  ,  
simulation,         4,      0.92,   0.92007,   0.92005,    15.144,    15.144,         0,  4289322,  681263,  ,  ,  ,  
simulation,         5,      0.95,   0.95004,   0.95001,    16.629,     16.63,         0,  4393984,  739314,  ,  ,  ,  
simulation,         6,      0.97,   0.97001,   0.97002,    18.175,    18.176,         0,  4461577,  780112,  ,  ,  ,  
simulation,         7,      0.98,   0.98006,   0.98005,    19.237,    19.238,         0,  4493340,  802806,  ,  ,  ,  
simulation,         9,       0.9,   0.90009,   0.90009,    14.359,     14.36,         0,  4219167,  643790,  ,  ,  ,  
simulation,        10,      0.85,   0.85007,   0.85006,     12.96,     12.96,         0,  4027763,  564540,  ,  ,  ,  
simulation,        11,       0.8,   0.80011,    0.8001,    12.026,    12.026,         0,  3821033,  501056,  ,  ,  ,  
