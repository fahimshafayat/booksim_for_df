wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 62;
routing_function = UGAL_L_restricted;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49994,   0.49998,    81.652,    82.334,         0,  18469238,  13805743,  ,  ,  ,  
simulation,         2,      0.74,   0.58107,   0.70066,    769.42,    769.19,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.58022,   0.60822,    486.63,    482.43,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55696,   0.55711,    394.01,    269.41,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.52967,   0.52983,    151.14,    143.87,         0,  28429505,  24051782,  ,  ,  ,  
simulation,         6,      0.54,   0.53937,    0.5395,    204.39,    173.54,         0,  31438258,  28048032,  ,  ,  ,  
simulation,         7,      0.55,   0.54861,   0.54883,    332.47,    220.94,         0,  45161468,  42655941,  ,  ,  ,  
simulation,        10,      0.45,      0.45,      0.45,    40.667,    40.982,         0,  17343514,  11616180,  ,  ,  ,  
simulation,        11,       0.4,   0.40005,   0.40005,    22.138,    22.228,         0,  15566956,  9962667,  ,  ,  ,  
