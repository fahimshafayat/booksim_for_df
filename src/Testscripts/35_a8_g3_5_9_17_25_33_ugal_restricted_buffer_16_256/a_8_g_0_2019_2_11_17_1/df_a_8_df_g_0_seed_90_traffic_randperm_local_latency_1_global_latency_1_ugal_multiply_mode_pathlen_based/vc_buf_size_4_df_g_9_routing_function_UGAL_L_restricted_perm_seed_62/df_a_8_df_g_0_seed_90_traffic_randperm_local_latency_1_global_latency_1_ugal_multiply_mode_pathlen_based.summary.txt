wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 9;
perm_seed = 62;
routing_function = UGAL_L_restricted;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49977,   0.49976,    19.721,    12.752,         0,  8446400,  3401112,  ,  ,  ,  
simulation,         2,      0.74,   0.58318,   0.58543,    805.49,    37.736,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.56966,   0.57048,    598.07,    28.151,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.54671,   0.54674,    635.36,    20.049,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,    0.5264,    0.5264,    343.57,     16.27,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.50962,   0.50961,    51.795,    13.412,         0,  12140548,  4873227,  ,  ,  ,  
simulation,         7,      0.52,   0.51816,   0.51817,    270.73,    14.804,         0,  16309127,  6532334,  ,  ,  ,  
simulation,         9,      0.45,   0.44995,   0.44994,    11.611,    11.611,         0,  5480352,  2270295,  ,  ,  ,  
simulation,        10,       0.4,   0.39985,   0.39984,    11.175,    11.175,         0,  4815343,  2073593,  ,  ,  ,  
simulation,        11,      0.35,    0.3499,   0.34989,    10.894,    10.894,         0,  4160364,  1867742,  ,  ,  ,  
