wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
perm_seed = 60;
routing_function = UGAL_L_restricted;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    10.472,    10.472,         0,  3273498,  1347837,  ,  ,  ,  
simulation,         2,      0.74,   0.74002,   0.74002,      12.1,      12.1,         0,  5022784,  1818113,  ,  ,  ,  
simulation,         3,      0.86,   0.85993,   0.85991,    14.212,    14.212,         0,  5848122,  2104196,  ,  ,  ,  
simulation,         4,      0.92,   0.91996,   0.91995,    16.495,    16.496,         0,  6220608,  2287017,  ,  ,  ,  
simulation,         5,      0.95,   0.94989,   0.94988,    18.483,    18.484,         0,  6396339,  2389012,  ,  ,  ,  
simulation,         6,      0.97,   0.96983,   0.96983,    20.843,    20.842,         0,  6497341,  2473589,  ,  ,  ,  
simulation,         7,      0.98,   0.97984,   0.97984,    22.653,    22.653,         0,  6548293,  2516151,  ,  ,  ,  
simulation,         9,       0.9,   0.89989,   0.89987,    15.591,    15.591,         0,  6098347,  2222146,  ,  ,  ,  
simulation,        10,      0.85,   0.84999,   0.84998,     13.95,    13.951,         0,  5782205,  2075871,  ,  ,  ,  
simulation,        11,       0.8,   0.80004,   0.80003,    12.931,    12.931,         0,  5444773,  1950840,  ,  ,  ,  
