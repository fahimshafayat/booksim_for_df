wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 17;
perm_seed = 62;
routing_function = vlb;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.48886,    0.4994,     523.1,    555.91,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25005,    16.935,    16.935,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36998,   0.36997,    20.905,    20.905,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42992,   0.42991,    27.213,    27.213,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45997,   0.45999,    36.607,    36.614,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,   0.47975,   0.47997,    78.774,    79.258,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.49,   0.48221,   0.48899,    490.74,    481.21,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44993,   0.44994,    32.267,    32.268,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39995,   0.39994,    23.182,    23.182,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35002,   0.35001,    19.838,    19.838,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30002,   0.30001,    18.053,    18.054,         0,  0,  0,  ,  ,  ,  
