wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 62;
routing_function = UGAL_G;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50016,   0.50017,    9.6665,    9.6666,         0,  2433065,  418108,  ,  ,  ,  
simulation,         2,      0.74,   0.73995,   0.73994,    11.039,    11.039,         0,  3630822,  588961,  ,  ,  ,  
simulation,         3,      0.86,   0.86001,   0.86001,    13.122,    13.122,         0,  4108647,  795579,  ,  ,  ,  
simulation,         4,      0.92,   0.92005,   0.92005,    15.214,    15.215,         0,  4295830,  951717,  ,  ,  ,  
simulation,         5,      0.95,      0.95,   0.95001,    16.811,    16.812,         0,  4381854,  1036995,  ,  ,  ,  
simulation,         6,      0.97,   0.97001,   0.97002,    18.435,    18.436,         0,  4440015,  1093117,  ,  ,  ,  
simulation,         7,      0.98,   0.98007,   0.98005,    19.699,    19.701,         0,  4465948,  1124287,  ,  ,  ,  
simulation,         9,       0.9,   0.90011,   0.90009,    14.404,    14.404,         0,  4234568,  898442,  ,  ,  ,  
simulation,        10,      0.85,   0.85008,   0.85006,    12.871,    12.871,         0,  4074500,  773113,  ,  ,  ,  
simulation,        11,       0.8,   0.80011,    0.8001,    11.852,    11.853,         0,  3890955,  671244,  ,  ,  ,  
