wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 60;
routing_function = UGAL_L_restricted;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50017,   0.50017,    9.9535,    9.9538,         0,  1878299,  822321,  ,  ,  ,  
simulation,         2,      0.74,   0.73995,   0.73994,    11.268,    11.268,         0,  2864485,  1132786,  ,  ,  ,  
simulation,         3,      0.86,   0.86001,   0.86001,    12.751,    12.751,         0,  3345912,  1300182,  ,  ,  ,  
simulation,         4,      0.92,   0.92006,   0.92005,    14.234,    14.235,         0,  3569713,  1400956,  ,  ,  ,  
simulation,         5,      0.95,   0.95003,   0.95001,    15.395,    15.396,         0,  3677995,  1455216,  ,  ,  ,  
simulation,         6,      0.97,      0.97,   0.97002,    16.511,    16.513,         0,  3746178,  1496299,  ,  ,  ,  
simulation,         7,      0.98,   0.98008,   0.98005,    17.387,    17.389,         0,  3780232,  1515650,  ,  ,  ,  
simulation,         9,       0.9,    0.9001,   0.90009,    13.625,    13.626,         0,  3496840,  1365452,  ,  ,  ,  
simulation,        10,      0.85,   0.85007,   0.85006,    12.575,    12.575,         0,  3307724,  1284658,  ,  ,  ,  
simulation,        11,       0.8,   0.80012,    0.8001,    11.869,     11.87,         0,  3109437,  1212152,  ,  ,  ,  
