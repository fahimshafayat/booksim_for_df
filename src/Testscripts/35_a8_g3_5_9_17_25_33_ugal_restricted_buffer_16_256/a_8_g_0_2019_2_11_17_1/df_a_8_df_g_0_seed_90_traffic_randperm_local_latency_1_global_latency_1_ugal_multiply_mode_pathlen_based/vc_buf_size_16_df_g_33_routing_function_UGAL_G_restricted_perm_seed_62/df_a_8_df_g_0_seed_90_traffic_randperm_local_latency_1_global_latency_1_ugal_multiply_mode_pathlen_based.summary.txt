wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 33;
perm_seed = 62;
routing_function = UGAL_G_restricted;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49998,   0.49998,    13.632,    13.632,         0,  21357060,  10183872,  ,  ,  ,  
simulation,         2,      0.74,   0.63184,    0.6389,    514.43,    105.45,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.61728,   0.61728,    415.27,    28.277,         0,  66953584,  41956690,  ,  ,  ,  
simulation,         4,      0.68,   0.63036,   0.63288,    521.29,    77.067,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.65,   0.62777,    0.6277,    626.62,    56.608,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.63,   0.62241,   0.62241,    535.16,     37.91,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.6,   0.59984,   0.59983,    39.456,    20.233,         0,  33846995,  19782695,  ,  ,  ,  
simulation,         8,      0.55,   0.55001,   0.55001,    15.328,    15.329,         0,  22939230,  11759499,  ,  ,  ,  
simulation,        10,      0.45,      0.45,      0.45,    12.687,    12.688,         0,  19405397,  8981232,  ,  ,  ,  
