wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 5;
perm_seed = 62;
routing_function = UGAL_G_restricted;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    10.386,    10.386,         0,  3561333,  1120255,  ,  ,  ,  
simulation,         2,      0.74,   0.74003,   0.74002,    11.884,    11.884,         0,  5468595,  1461556,  ,  ,  ,  
simulation,         3,      0.86,   0.85991,   0.85991,    13.632,    13.632,         0,  6392122,  1661721,  ,  ,  ,  
simulation,         4,      0.92,   0.91994,   0.91995,     15.27,     15.27,         0,  6816964,  1799417,  ,  ,  ,  
simulation,         5,      0.95,   0.94989,   0.94988,    16.502,    16.503,         0,  7020049,  1876729,  ,  ,  ,  
simulation,         6,      0.97,   0.96984,   0.96983,    17.657,    17.658,         0,  7155868,  1928196,  ,  ,  ,  
simulation,         7,      0.98,   0.97984,   0.97984,     18.41,    18.411,         0,  7226616,  1951829,  ,  ,  ,  
simulation,         9,       0.9,   0.89986,   0.89987,     14.63,     14.63,         0,  6676447,  1752347,  ,  ,  ,  
simulation,        10,      0.85,   0.84998,   0.84998,    13.441,    13.441,         0,  6315697,  1644626,  ,  ,  ,  
simulation,        11,       0.8,   0.80003,   0.80003,    12.604,    12.604,         0,  5942409,  1550722,  ,  ,  ,  
