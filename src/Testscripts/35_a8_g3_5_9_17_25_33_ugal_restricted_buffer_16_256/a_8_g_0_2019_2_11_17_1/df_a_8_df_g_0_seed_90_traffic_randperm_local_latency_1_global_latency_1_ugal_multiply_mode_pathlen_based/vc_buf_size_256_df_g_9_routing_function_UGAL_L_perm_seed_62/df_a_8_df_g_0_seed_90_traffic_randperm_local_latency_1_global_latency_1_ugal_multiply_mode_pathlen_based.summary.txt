wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 9;
perm_seed = 62;
routing_function = UGAL_L;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49979,   0.49978,    12.011,    12.011,         0,  6801927,  1809202,  ,  ,  ,  
simulation,         2,      0.74,   0.73742,   0.73766,    374.95,     140.3,         0,  24091626,  8641674,  ,  ,  ,  
simulation,         3,      0.86,   0.78007,   0.82779,    717.96,     663.6,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.77569,   0.77872,    592.81,    429.76,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,      0.76,   0.75987,    485.92,    252.69,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.75,   0.74591,   0.74592,    327.42,    148.24,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.7,   0.70018,   0.70017,    42.642,    42.847,         0,  9335235,  2846368,  ,  ,  ,  
simulation,         9,      0.65,   0.65005,   0.65004,    22.806,    22.872,         0,  8877067,  2371131,  ,  ,  ,  
simulation,        10,       0.6,   0.59997,   0.59996,    14.633,    14.643,         0,  8259520,  2120740,  ,  ,  ,  
