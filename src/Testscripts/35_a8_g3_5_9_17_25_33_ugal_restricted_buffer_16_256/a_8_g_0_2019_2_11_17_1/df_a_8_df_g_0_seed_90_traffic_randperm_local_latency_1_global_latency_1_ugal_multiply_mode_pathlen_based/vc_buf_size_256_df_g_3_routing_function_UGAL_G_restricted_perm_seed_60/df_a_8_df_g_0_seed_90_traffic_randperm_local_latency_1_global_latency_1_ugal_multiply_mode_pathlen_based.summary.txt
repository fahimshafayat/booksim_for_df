wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 60;
routing_function = UGAL_G_restricted;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50017,   0.50017,    9.8183,    9.8185,         0,  1950179,  750300,  ,  ,  ,  
simulation,         2,      0.74,   0.73994,   0.73994,    11.048,    11.048,         0,  2973340,  1023998,  ,  ,  ,  
simulation,         3,      0.86,   0.86001,   0.86001,    12.299,    12.299,         0,  3485075,  1160713,  ,  ,  ,  
simulation,         4,      0.92,   0.92004,   0.92005,    13.406,    13.406,         0,  3722653,  1247514,  ,  ,  ,  
simulation,         5,      0.95,   0.95001,   0.95001,    14.206,    14.206,         0,  3836977,  1295799,  ,  ,  ,  
simulation,         6,      0.97,   0.97002,   0.97002,    14.936,    14.936,         0,  3913814,  1327962,  ,  ,  ,  
simulation,         7,      0.98,   0.98005,   0.98005,    15.429,     15.43,         0,  3950091,  1345259,  ,  ,  ,  
simulation,         9,       0.9,   0.90009,   0.90009,    12.986,    12.986,         0,  3644474,  1217658,  ,  ,  ,  
simulation,        10,      0.85,   0.85007,   0.85006,    12.163,    12.164,         0,  3442320,  1149756,  ,  ,  ,  
simulation,        11,       0.8,   0.80012,    0.8001,    11.574,    11.574,         0,  3231587,  1090074,  ,  ,  ,  
