wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
df_g = 3;
perm_seed = 60;
routing_function = UGAL_L;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50017,   0.50017,    10.537,    10.537,         0,  2137071,  563729,  ,  ,  ,  
simulation,         2,      0.74,   0.73995,   0.73994,    12.279,    12.279,         0,  3236051,  761553,  ,  ,  ,  
simulation,         3,      0.86,   0.86001,   0.86001,    14.977,    14.978,         0,  3691449,  955449,  ,  ,  ,  
simulation,         4,      0.92,   0.92008,   0.92005,    18.622,    18.623,         0,  3863592,  1107666,  ,  ,  ,  
simulation,         5,      0.95,   0.95007,   0.95001,    29.878,    29.902,         0,  3934072,  1218639,  ,  ,  ,  
simulation,         6,      0.97,   0.96259,    0.9631,    449.85,    237.92,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.96,   0.95862,   0.95924,     119.5,    96.653,         0,  6901697,  2216062,  ,  ,  ,  
simulation,         9,       0.9,   0.90011,   0.90009,    16.956,    16.958,         0,  3813698,  1050724,  ,  ,  ,  
simulation,        10,      0.85,   0.85008,   0.85006,    14.624,    14.624,         0,  3659138,  933641,  ,  ,  ,  
simulation,        11,       0.8,    0.8001,    0.8001,    13.277,    13.278,         0,  3478768,  843473,  ,  ,  ,  
