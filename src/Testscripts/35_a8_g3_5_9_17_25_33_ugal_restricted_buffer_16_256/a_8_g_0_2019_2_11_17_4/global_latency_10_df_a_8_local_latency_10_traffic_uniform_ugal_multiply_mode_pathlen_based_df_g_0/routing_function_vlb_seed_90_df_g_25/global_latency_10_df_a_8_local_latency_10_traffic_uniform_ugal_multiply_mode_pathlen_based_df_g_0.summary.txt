priority = none;
sample_period = 10000;
injection_rate_uses_flits = 1;
output_speedup = 1;
sim_count = 1;
wait_for_tail_credit = 0;
sw_allocator = separable_input_first;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
vc_alloc_delay = 1;
packet_size = 1;
sw_alloc_delay = 1;
vc_buf_size = 64;
warmup_periods = 3;
num_vcs = 7;
credit_delay = 2;
st_final_delay = 1;
vc_allocator = separable_input_first;
internal_speedup = 4.0;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = vlb;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.38337,   0.43394,    1153.6,    953.59,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25008,     65.87,    65.874,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,    0.3701,   0.37011,     82.37,     82.37,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.38408,   0.42608,    534.61,    531.28,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.38192,   0.38978,    1055.4,    911.59,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.38,   0.38027,    0.3801,    106.55,    106.67,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.39,   0.38195,   0.38678,    686.16,    620.74,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.35,    0.3501,    0.3501,    73.568,    73.568,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.30005,   0.30006,    67.883,    67.886,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.2,   0.20009,   0.20009,    64.763,    64.767,         0,  0,  0,  ,  ,  ,  
