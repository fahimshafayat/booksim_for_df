priority = none;
sample_period = 10000;
injection_rate_uses_flits = 1;
output_speedup = 1;
sim_count = 1;
wait_for_tail_credit = 0;
sw_allocator = separable_input_first;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
vc_alloc_delay = 1;
packet_size = 1;
sw_alloc_delay = 1;
vc_buf_size = 64;
warmup_periods = 3;
num_vcs = 7;
credit_delay = 2;
st_final_delay = 1;
vc_allocator = separable_input_first;
internal_speedup = 4.0;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_G;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    31.558,    31.561,         0,  4623871,  60704,  ,  ,  ,  
simulation,         2,      0.74,   0.74002,   0.74002,     33.45,    33.454,         0,  6880069,  52770,  ,  ,  ,  
simulation,         3,      0.86,   0.85992,   0.85991,    36.253,    36.257,         0,  8005299,  52394,  ,  ,  ,  
simulation,         4,      0.92,   0.91998,   0.91995,    40.033,    40.037,         0,  8566305,  55923,  ,  ,  ,  
simulation,         5,      0.95,   0.94988,   0.94988,    44.481,    44.489,         0,  8848777,  58595,  ,  ,  ,  
simulation,         6,      0.97,   0.96992,   0.96983,    51.778,    51.796,         0,  9036687,  61013,  ,  ,  ,  
simulation,         7,      0.98,   0.97993,   0.97984,    60.191,    60.227,         0,  9137899,  62685,  ,  ,  ,  
simulation,         9,       0.9,    0.8999,   0.89987,    38.321,    38.326,         0,  8381934,  53805,  ,  ,  ,  
simulation,        10,      0.85,   0.84998,   0.84998,    35.909,    35.912,         0,  7912323,  52870,  ,  ,  ,  
simulation,        11,       0.8,   0.80005,   0.80003,    34.522,    34.525,         0,  7445254,  52197,  ,  ,  ,  
