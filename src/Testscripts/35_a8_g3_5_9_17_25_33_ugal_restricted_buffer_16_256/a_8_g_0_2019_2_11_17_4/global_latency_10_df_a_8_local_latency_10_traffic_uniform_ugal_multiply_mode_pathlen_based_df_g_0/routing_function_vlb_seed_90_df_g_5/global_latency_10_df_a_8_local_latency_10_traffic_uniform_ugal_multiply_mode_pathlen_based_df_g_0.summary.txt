priority = none;
sample_period = 10000;
injection_rate_uses_flits = 1;
output_speedup = 1;
sim_count = 1;
wait_for_tail_credit = 0;
sw_allocator = separable_input_first;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
vc_alloc_delay = 1;
packet_size = 1;
sw_alloc_delay = 1;
vc_buf_size = 64;
warmup_periods = 3;
num_vcs = 7;
credit_delay = 2;
st_final_delay = 1;
vc_allocator = separable_input_first;
internal_speedup = 4.0;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = vlb;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    73.119,     73.13,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,    0.5458,    0.6088,    1392.8,    968.84,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.54516,   0.60667,    679.46,    666.56,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55067,   0.55921,    527.99,    516.27,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.53002,   0.52999,    91.592,    91.608,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.54,   0.53995,   0.53992,    113.19,    113.25,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.55,    0.5498,   0.54996,    214.87,    215.03,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,   0.45014,   0.45013,     64.88,    64.892,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.4,   0.40021,   0.40022,    61.699,     61.71,         0,  0,  0,  ,  ,  ,  
