priority = none;
sample_period = 10000;
injection_rate_uses_flits = 1;
output_speedup = 1;
sim_count = 1;
wait_for_tail_credit = 0;
sw_allocator = separable_input_first;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
vc_alloc_delay = 1;
packet_size = 1;
sw_alloc_delay = 1;
vc_buf_size = 64;
warmup_periods = 3;
num_vcs = 7;
credit_delay = 2;
st_final_delay = 1;
vc_allocator = separable_input_first;
internal_speedup = 4.0;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_G;
seed = 90;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50005,   0.50006,     35.56,    35.562,         0,  23443152,  463430,  ,  ,  ,  
simulation,         2,      0.74,   0.74005,   0.74004,    41.216,    41.218,         0,  34683184,  708246,  ,  ,  ,  
simulation,         3,      0.86,   0.80237,   0.82334,    634.87,    400.23,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,       0.8,   0.79999,    52.023,    52.029,         0,  37077276,  1198515,  ,  ,  ,  
simulation,         5,      0.83,   0.80571,    0.8074,    651.94,    407.85,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.81,   0.81004,   0.81002,    57.108,    57.118,         0,  37352263,  1417922,  ,  ,  ,  
simulation,         7,      0.82,   0.80721,   0.81009,    507.25,    315.47,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.75,   0.75008,   0.75008,    42.037,    42.038,         0,  35129300,  748527,  ,  ,  ,  
simulation,        10,       0.7,   0.70004,   0.70005,    39.048,    39.049,         0,  32867095,  608630,  ,  ,  ,  
simulation,        11,      0.65,   0.65012,   0.65012,    37.558,     37.56,         0,  30541138,  543007,  ,  ,  ,  
