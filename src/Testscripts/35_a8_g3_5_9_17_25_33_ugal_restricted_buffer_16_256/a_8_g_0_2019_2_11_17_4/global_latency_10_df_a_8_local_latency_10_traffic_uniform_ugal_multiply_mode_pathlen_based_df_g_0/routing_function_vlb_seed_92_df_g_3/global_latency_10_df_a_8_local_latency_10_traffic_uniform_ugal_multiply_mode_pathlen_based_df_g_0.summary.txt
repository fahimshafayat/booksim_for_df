priority = none;
sample_period = 10000;
injection_rate_uses_flits = 1;
output_speedup = 1;
sim_count = 1;
wait_for_tail_credit = 0;
sw_allocator = separable_input_first;
routing_delay = 0;
input_speedup = 1;
alloc_iters = 1;
vc_alloc_delay = 1;
packet_size = 1;
sw_alloc_delay = 1;
vc_buf_size = 64;
warmup_periods = 3;
num_vcs = 7;
credit_delay = 2;
st_final_delay = 1;
vc_allocator = separable_input_first;
internal_speedup = 4.0;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = vlb;
seed = 92;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49957,   0.49958,    59.252,    59.266,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.59159,   0.65163,    1070.2,    831.75,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.59664,   0.60619,    1043.9,    869.18,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55926,   0.55931,    72.372,    72.401,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.59,   0.58912,   0.58901,    129.16,    129.24,         0,  0,  0,  ,  ,  ,  
simulation,         6,       0.6,   0.59585,   0.59907,    508.07,    486.06,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.55,   0.54928,   0.54937,     68.06,    68.072,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,    0.4496,   0.44956,     56.14,    56.155,         0,  0,  0,  ,  ,  ,  
