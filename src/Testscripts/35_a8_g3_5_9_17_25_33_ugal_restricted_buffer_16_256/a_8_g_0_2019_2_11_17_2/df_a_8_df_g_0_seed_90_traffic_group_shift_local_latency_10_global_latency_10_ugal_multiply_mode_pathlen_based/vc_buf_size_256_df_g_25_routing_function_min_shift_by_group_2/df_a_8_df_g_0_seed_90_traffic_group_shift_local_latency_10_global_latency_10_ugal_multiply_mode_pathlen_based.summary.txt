wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = min;
shift_by_group = 2;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.044884,   0.23353,    4344.6,    4344.6,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.04487,   0.22743,    3688.4,    3688.4,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.044856,   0.13013,    2525.9,    2525.9,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.044836,  0.070087,    1256.9,    1256.9,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.034942,  0.040037,    574.35,    574.35,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.02,  0.020055,  0.020056,    34.839,     34.84,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.03,  0.030037,  0.030042,    40.949,    40.951,         0,  0,  0,  ,  ,  ,  
