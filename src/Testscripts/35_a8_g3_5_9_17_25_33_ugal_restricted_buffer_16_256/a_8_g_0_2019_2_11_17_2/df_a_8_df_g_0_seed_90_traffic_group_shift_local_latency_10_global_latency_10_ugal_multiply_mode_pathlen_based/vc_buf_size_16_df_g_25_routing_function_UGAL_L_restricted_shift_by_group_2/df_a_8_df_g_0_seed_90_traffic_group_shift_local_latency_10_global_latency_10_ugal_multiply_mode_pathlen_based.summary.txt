wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_L_restricted;
shift_by_group = 2;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.20712,   0.21821,    2410.0,    497.25,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.19986,   0.20828,    731.68,    343.41,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12958,   0.12958,    348.67,    83.177,         0,  3175921,  9759569,  ,  ,  ,  
simulation,         4,      0.19,   0.17881,   0.17884,    980.89,    198.08,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.16,   0.15675,   0.15674,     462.9,    113.62,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.14,   0.13888,   0.13888,    459.88,    91.015,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.1,   0.10007,   0.10007,     55.17,    55.176,         0,  1468029,  3343244,  ,  ,  ,  
simulation,         8,      0.05,  0.050052,  0.050053,    46.378,    46.381,         0,  967977,  1434819,  ,  ,  ,  
