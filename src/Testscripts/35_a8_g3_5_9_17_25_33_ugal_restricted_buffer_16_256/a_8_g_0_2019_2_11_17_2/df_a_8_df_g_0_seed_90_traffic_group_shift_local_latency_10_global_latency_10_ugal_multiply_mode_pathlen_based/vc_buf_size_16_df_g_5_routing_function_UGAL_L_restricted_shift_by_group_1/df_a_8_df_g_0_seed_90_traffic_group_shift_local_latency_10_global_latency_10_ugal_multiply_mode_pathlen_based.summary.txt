wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_L_restricted;
shift_by_group = 1;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.37872,     0.392,    1213.2,    325.42,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25018,   0.25018,    38.524,    38.526,         0,  1596789,  806941,  ,  ,  ,  
simulation,         3,      0.37,    0.3702,   0.37022,     85.99,    86.074,         0,  1997785,  1570796,  ,  ,  ,  
simulation,         4,      0.43,   0.37911,   0.39256,    593.12,    299.68,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.37906,   0.38575,    517.46,    289.73,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.38,   0.38013,   0.38022,    115.17,    115.31,         0,  2023612,  1666016,  ,  ,  ,  
simulation,         7,      0.39,   0.38016,   0.38092,     614.0,    335.12,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.35,   0.35012,   0.35016,    73.792,    73.844,         0,  1966725,  1404551,  ,  ,  ,  
simulation,         9,       0.3,   0.29992,   0.29995,      47.7,    47.706,         0,  1874290,  1010390,  ,  ,  ,  
simulation,        11,       0.2,   0.20008,   0.20008,    37.563,    37.565,         0,  1272607,  650276,  ,  ,  ,  
