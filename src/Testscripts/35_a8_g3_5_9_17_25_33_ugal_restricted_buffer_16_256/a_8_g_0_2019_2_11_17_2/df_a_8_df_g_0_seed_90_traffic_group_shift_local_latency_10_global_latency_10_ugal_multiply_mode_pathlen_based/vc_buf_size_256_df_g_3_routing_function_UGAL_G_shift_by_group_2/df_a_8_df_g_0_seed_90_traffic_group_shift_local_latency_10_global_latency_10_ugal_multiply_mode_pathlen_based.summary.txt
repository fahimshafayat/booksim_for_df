wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = UGAL_G;
shift_by_group = 2;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50018,   0.50017,    40.714,    40.716,         0,  2450716,  432874,  ,  ,  ,  
simulation,         2,      0.74,   0.73997,   0.73994,    98.352,    98.347,         0,  2889485,  1382509,  ,  ,  ,  
simulation,         3,      0.86,   0.70726,    0.8594,    812.81,     812.8,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.70621,   0.79186,     973.6,    972.84,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.70528,    0.7674,    1294.7,    1293.6,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.75,   0.74986,      0.75,    136.21,    136.19,         0,  2893852,  1439578,  ,  ,  ,  
simulation,         7,      0.76,   0.71227,    0.7605,    658.99,    658.99,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.7,   0.70001,   0.69995,    82.178,    82.177,         0,  2887398,  1151837,  ,  ,  ,  
simulation,        10,      0.65,   0.65004,   0.65002,    61.332,    61.334,         0,  2885133,  864620,  ,  ,  ,  
simulation,        11,       0.6,   0.60022,   0.60022,    47.714,    47.716,         0,  2837639,  623371,  ,  ,  ,  
