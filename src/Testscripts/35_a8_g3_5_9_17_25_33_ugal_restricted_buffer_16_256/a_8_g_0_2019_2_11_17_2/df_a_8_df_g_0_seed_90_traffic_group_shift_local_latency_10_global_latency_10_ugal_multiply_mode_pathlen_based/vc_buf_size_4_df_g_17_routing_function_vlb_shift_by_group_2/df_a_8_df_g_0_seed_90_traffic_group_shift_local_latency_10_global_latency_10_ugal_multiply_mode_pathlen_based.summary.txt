wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = vlb;
shift_by_group = 2;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.13618,   0.13997,    3644.9,    271.34,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.13643,   0.14019,    2275.2,    268.33,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12992,   0.12995,     96.45,    96.384,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.19,   0.13597,   0.13972,    1428.1,    264.72,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.16,   0.13612,   0.13992,    775.19,    254.73,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.14,   0.13642,   0.13653,    758.52,    261.79,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.1,  0.099938,  0.099944,    68.594,    68.597,         0,  0,  0,  ,  ,  ,  
simulation,         8,      0.05,   0.04996,  0.049959,    65.031,    65.033,         0,  0,  0,  ,  ,  ,  
