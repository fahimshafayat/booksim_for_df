wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_G_restricted;
shift_by_group = 1;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.22396,   0.23535,    2330.2,    472.74,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.21185,   0.21816,    524.99,    246.77,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.13009,   0.13009,    49.511,    49.513,         0,  1389511,  4859466,  ,  ,  ,  
simulation,         4,      0.19,   0.18479,   0.18482,    498.31,    124.59,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.16,   0.16003,   0.16003,    59.856,    55.799,         0,  1736717,  6471585,  ,  ,  ,  
simulation,         6,      0.17,   0.16931,   0.16931,    401.61,    72.417,         0,  3776956,  14457156,  ,  ,  ,  
simulation,         7,      0.18,   0.17763,   0.17764,    530.67,    93.957,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.15,   0.15006,   0.15007,    50.992,    50.996,         0,  1556694,  5659119,  ,  ,  ,  
simulation,         9,       0.1,   0.10007,   0.10007,    48.868,     48.87,         0,  1128691,  3677267,  ,  ,  ,  
simulation,        10,      0.05,  0.050049,  0.050053,     47.48,    47.483,         0,  664790,  1738006,  ,  ,  ,  
