wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = min;
shift_by_group = 1;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.35296,   0.36564,    1473.6,    333.39,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25022,   0.25022,    35.245,    35.246,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.35372,   0.35383,    1186.6,    350.41,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31024,   0.31023,    37.302,    37.303,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,    0.3402,   0.34017,    45.301,    45.313,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.35,   0.35002,   0.35027,    91.654,    90.828,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.36,   0.35284,   0.35445,     545.0,    302.09,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.30018,   0.30019,    36.626,    36.626,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.2,   0.20011,   0.20011,    34.823,    34.824,         0,  0,  0,  ,  ,  ,  
