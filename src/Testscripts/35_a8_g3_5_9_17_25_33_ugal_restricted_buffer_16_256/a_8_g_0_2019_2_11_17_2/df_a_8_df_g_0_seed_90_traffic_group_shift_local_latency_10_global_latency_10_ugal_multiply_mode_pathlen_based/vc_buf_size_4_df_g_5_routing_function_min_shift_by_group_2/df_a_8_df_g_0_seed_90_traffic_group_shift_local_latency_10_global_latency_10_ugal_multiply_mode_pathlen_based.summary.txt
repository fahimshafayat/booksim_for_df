wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = min;
shift_by_group = 2;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.047403,  0.050736,    4503.3,     669.4,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.047348,  0.050661,    4021.8,    662.05,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.047547,  0.050902,    3153.7,    656.08,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.047418,   0.05078,    1581.3,    614.61,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.039845,  0.039848,    44.914,    44.918,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.05,  0.047196,  0.048754,     587.5,    443.45,         1,  0,  0,  ,  ,  ,  
