wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = vlb;
shift_by_group = 1;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.11023,   0.11375,    3889.9,    311.27,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.11008,   0.11357,    2776.9,     308.8,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.10986,   0.11328,    750.13,    279.45,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.070055,  0.070054,    66.314,    66.317,         0,  0,  0,  ,  ,  ,  
simulation,         5,       0.1,   0.10007,   0.10007,    77.915,    77.931,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.11,   0.10883,   0.10881,     519.4,    228.72,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.05,   0.05005,  0.050053,    65.182,    65.184,         0,  0,  0,  ,  ,  ,  
