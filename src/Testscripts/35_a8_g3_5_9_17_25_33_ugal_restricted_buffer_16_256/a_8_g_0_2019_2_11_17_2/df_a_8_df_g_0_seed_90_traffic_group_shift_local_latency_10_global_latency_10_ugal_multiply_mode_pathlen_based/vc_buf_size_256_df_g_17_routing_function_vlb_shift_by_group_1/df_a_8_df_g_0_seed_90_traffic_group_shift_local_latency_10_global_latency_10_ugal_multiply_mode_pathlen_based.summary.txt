wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = vlb;
shift_by_group = 1;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4473,       0.5,    573.41,    573.41,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,    67.281,    67.282,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36998,   0.36997,    73.082,     73.08,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,    0.4299,   0.42991,    87.911,    87.906,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45977,   0.45999,    166.59,    166.72,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,   0.45854,   0.47994,    515.74,    515.74,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,    0.4664,   0.46974,    524.81,    527.49,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44992,   0.44994,    112.79,    112.81,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39994,   0.39994,    77.421,    77.418,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35002,   0.35001,    71.369,    71.369,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30001,   0.30001,    68.749,     68.75,         0,  0,  0,  ,  ,  ,  
