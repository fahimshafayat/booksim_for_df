wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = min;
shift_by_group = 2;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.35333,   0.36578,    1467.6,    332.07,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25023,   0.25022,    35.246,    35.248,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.35338,   0.35386,    1129.9,    352.99,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31023,   0.31023,    37.368,    37.367,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,   0.34021,   0.34017,    45.787,    45.799,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.35,   0.35042,   0.35029,    105.28,    102.88,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.36,   0.35338,   0.35419,    523.51,    318.65,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.3,   0.30019,   0.30019,    36.645,    36.646,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.2,   0.20011,   0.20011,    34.838,    34.839,         0,  0,  0,  ,  ,  ,  
