wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_G;
shift_by_group = 1;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.38277,   0.49298,    861.68,    861.68,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25008,    65.279,    65.281,         0,  1983727,  10036774,  ,  ,  ,  
simulation,         3,      0.37,   0.37012,   0.37011,    75.912,    75.914,         0,  1986645,  15812078,  ,  ,  ,  
simulation,         4,      0.43,   0.40188,   0.42896,    557.87,    557.75,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.40008,   0.40009,    89.254,    89.257,         0,  1988669,  17264345,  ,  ,  ,  
simulation,         6,      0.41,   0.40057,    0.4079,    486.38,    485.04,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.35,    0.3501,    0.3501,    73.132,    73.133,         0,  1986186,  14848847,  ,  ,  ,  
simulation,         9,       0.3,   0.30005,   0.30006,    68.778,    68.779,         0,  1984748,  12439290,  ,  ,  ,  
