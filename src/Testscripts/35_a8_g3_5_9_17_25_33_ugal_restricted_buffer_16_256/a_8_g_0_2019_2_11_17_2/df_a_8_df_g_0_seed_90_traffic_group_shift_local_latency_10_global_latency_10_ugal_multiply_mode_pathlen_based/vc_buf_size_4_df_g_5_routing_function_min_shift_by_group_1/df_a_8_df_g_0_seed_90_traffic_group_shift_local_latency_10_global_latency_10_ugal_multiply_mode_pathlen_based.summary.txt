wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = min;
shift_by_group = 1;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.047326,   0.05063,    4502.4,    669.28,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.047415,  0.050732,    4025.1,     665.8,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.047633,  0.051006,    3152.4,    652.22,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.047535,  0.050883,    1564.4,    613.87,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.039846,  0.039848,    45.049,     45.05,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.05,  0.047343,  0.048889,    557.78,    435.85,         1,  0,  0,  ,  ,  ,  
