wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = UGAL_L_restricted;
shift_by_group = 2;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.13983,   0.14328,    3612.0,    238.96,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.13997,   0.14341,    2201.0,    237.01,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.13011,   0.13007,    58.134,    58.178,         0,  514798,  238156,  ,  ,  ,  
simulation,         4,      0.19,   0.13953,   0.14295,    1327.3,    233.27,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.16,   0.13942,   0.14289,    623.97,    216.54,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.14,   0.13895,   0.13892,     528.9,     164.4,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.1,   0.10014,   0.10014,    39.181,    39.186,         0,  403427,  173780,  ,  ,  ,  
simulation,         8,      0.05,  0.050084,  0.050078,    36.378,    36.381,         0,  200753,  88122,  ,  ,  ,  
