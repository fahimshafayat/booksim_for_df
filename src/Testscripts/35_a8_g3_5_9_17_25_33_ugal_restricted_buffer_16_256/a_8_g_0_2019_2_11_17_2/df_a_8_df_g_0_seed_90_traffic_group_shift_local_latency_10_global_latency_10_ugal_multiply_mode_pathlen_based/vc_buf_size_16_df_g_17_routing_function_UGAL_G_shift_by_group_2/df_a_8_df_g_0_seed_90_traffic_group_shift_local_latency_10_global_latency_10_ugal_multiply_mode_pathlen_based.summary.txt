wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_G;
shift_by_group = 2;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.17086,   0.18293,    3089.2,    655.91,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.17216,    0.1831,    1271.7,    543.35,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12995,   0.12995,    54.526,    54.534,         0,  1510233,  2738812,  ,  ,  ,  
simulation,         4,      0.19,   0.16584,   0.17189,    458.73,    279.74,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.16,   0.15549,   0.15565,    545.43,    177.64,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.14,   0.13997,   0.13997,    55.625,     55.63,         0,  1619733,  2957141,  ,  ,  ,  
simulation,         7,      0.15,   0.14984,   0.14984,    145.98,    76.966,         0,  3174788,  5869460,  ,  ,  ,  
simulation,         9,       0.1,  0.099939,  0.099944,    53.493,    53.501,         0,  1187670,  2079696,  ,  ,  ,  
simulation,        10,      0.05,  0.049961,  0.049959,    52.002,    52.011,         0,  638652,  994251,  ,  ,  ,  
