wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_G;
shift_by_group = 1;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.17575,    0.1878,    3047.8,     638.8,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,    0.1778,    0.1884,    1160.0,    506.16,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12995,   0.12995,    54.388,    54.397,         0,  1506805,  2742240,  ,  ,  ,  
simulation,         4,      0.19,   0.17055,   0.17324,    734.44,    278.33,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.16,   0.15718,   0.15716,    525.84,    157.61,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.14,   0.13997,   0.13997,    55.339,    55.346,         0,  1615622,  2960502,  ,  ,  ,  
simulation,         7,      0.15,   0.15002,   0.15003,    59.265,    59.277,         0,  1720812,  3187886,  ,  ,  ,  
simulation,         9,       0.1,   0.09994,  0.099944,    53.401,    53.408,         0,  1186666,  2080553,  ,  ,  ,  
simulation,        10,      0.05,  0.049959,  0.049959,    51.914,    51.923,         0,  638204,  994699,  ,  ,  ,  
