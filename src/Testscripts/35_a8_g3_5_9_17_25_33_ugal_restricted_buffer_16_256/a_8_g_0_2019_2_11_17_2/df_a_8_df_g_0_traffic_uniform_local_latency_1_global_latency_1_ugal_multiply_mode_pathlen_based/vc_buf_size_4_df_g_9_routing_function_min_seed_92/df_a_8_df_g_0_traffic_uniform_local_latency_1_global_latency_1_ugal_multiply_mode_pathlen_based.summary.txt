wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = min;
seed = 92;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50013,   0.50012,    11.367,    11.367,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.62102,   0.62334,    804.11,    38.013,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.62007,   0.62006,    73.783,    33.156,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.68,   0.62168,   0.62288,    856.57,    37.978,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.65,   0.62261,   0.62255,    1070.2,    38.333,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.63,    0.6224,   0.62239,    519.07,    38.235,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.6,   0.60003,   0.60001,    19.567,    18.936,         0,  0,  0,  ,  ,  ,  
simulation,         8,      0.55,   0.55014,   0.55013,    12.796,    12.787,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,   0.45007,   0.45006,    10.697,    10.697,         0,  0,  0,  ,  ,  ,  
