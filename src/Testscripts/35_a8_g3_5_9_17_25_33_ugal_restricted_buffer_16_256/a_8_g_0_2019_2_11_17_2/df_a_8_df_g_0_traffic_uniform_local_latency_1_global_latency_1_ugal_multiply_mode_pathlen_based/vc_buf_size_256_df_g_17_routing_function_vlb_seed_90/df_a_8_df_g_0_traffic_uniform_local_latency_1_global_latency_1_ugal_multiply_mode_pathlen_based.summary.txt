wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = vlb;
seed = 90;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49986,   0.49997,    81.842,    81.906,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.45692,   0.74003,    2229.8,    2229.8,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.46347,      0.62,    1263.0,    1263.0,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.49119,   0.56006,    636.61,    636.61,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.50519,   0.52986,    511.68,    511.68,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.50974,   0.50995,    186.22,    186.56,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.52,    0.5116,   0.52008,    560.13,    560.13,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.45,   0.44995,   0.44994,    30.765,    30.767,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.4,   0.39995,   0.39994,    23.017,    23.018,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.35,   0.35002,   0.35001,    19.827,    19.827,         0,  0,  0,  ,  ,  ,  
