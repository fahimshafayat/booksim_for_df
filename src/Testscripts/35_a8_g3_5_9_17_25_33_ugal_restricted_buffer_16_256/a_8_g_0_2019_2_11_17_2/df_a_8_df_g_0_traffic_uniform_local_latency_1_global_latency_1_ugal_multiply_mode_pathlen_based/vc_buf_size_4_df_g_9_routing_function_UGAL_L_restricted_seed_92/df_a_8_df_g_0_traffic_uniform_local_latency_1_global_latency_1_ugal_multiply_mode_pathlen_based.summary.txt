wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 9;
routing_function = UGAL_L_restricted;
seed = 92;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50013,   0.50012,     11.74,     11.74,         0,  6519775,  2004030,  ,  ,  ,  
simulation,         2,      0.74,   0.68163,   0.68283,    794.62,    35.056,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.62003,   0.62002,    14.143,    14.136,         0,  8305401,  2263665,  ,  ,  ,  
simulation,         4,      0.68,   0.67995,   0.67997,    46.898,    29.902,         0,  12237421,  3255351,  ,  ,  ,  
simulation,         5,      0.71,   0.68205,   0.68205,    969.08,    35.261,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.69,   0.68251,   0.68251,    511.11,    35.225,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.65,   0.64993,   0.64992,    16.516,    16.455,         0,  8739016,  2347253,  ,  ,  ,  
simulation,         8,       0.6,       0.6,       0.6,    13.393,    13.392,         0,  8012154,  2214552,  ,  ,  ,  
simulation,         9,      0.55,   0.55014,   0.55013,    12.342,    12.342,         0,  7270623,  2105933,  ,  ,  ,  
