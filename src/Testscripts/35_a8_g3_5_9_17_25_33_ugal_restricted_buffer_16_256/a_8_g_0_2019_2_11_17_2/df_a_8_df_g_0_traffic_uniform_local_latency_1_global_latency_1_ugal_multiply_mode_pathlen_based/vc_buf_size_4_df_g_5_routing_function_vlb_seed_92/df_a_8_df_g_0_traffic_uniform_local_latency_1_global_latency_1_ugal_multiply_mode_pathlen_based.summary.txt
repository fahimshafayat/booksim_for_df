wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = vlb;
seed = 92;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.45823,   0.45983,    837.15,    66.994,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25006,    15.367,    15.367,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.37017,   0.37018,    17.981,    17.981,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.43003,   0.43002,     22.29,    22.283,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45859,   0.45862,     281.3,    64.419,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,   0.45819,   0.45816,    1101.2,    68.505,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.45849,   0.45841,     602.6,    67.967,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44999,   0.45003,     31.48,    30.877,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.40009,    0.4001,    19.364,    19.365,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35029,    0.3503,    17.336,    17.337,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30021,   0.30022,    16.153,    16.154,         0,  0,  0,  ,  ,  ,  
