wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = vlb;
seed = 90;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.45798,   0.45953,    854.37,     67.26,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25018,   0.25018,    15.365,    15.366,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.37021,   0.37022,    17.982,    17.983,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.43012,   0.43016,    22.155,    22.151,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.45918,   0.45919,    261.81,    63.889,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.48,   0.45935,   0.45937,    1130.9,    68.243,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.45843,   0.45851,    611.04,     68.18,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.45018,   0.45014,    31.604,    30.959,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,    0.4002,   0.40022,    19.385,    19.386,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35016,   0.35016,    17.317,    17.317,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.29995,   0.29995,    16.142,    16.142,         0,  0,  0,  ,  ,  ,  
