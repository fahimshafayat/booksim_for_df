wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = UGAL_L;
seed = 90;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50016,   0.50017,    10.307,    10.308,         0,  2274433,  486352,  ,  ,  ,  
simulation,         2,      0.74,   0.73995,   0.73994,    11.941,    11.941,         0,  3529875,  557184,  ,  ,  ,  
simulation,         3,      0.86,   0.86003,   0.86001,    14.332,    14.332,         0,  4170556,  579958,  ,  ,  ,  
simulation,         4,      0.92,   0.92007,   0.92005,    17.487,    17.489,         0,  4492653,  589590,  ,  ,  ,  
simulation,         5,      0.95,   0.95002,   0.95001,    21.587,    21.598,         0,  4656062,  595937,  ,  ,  ,  
simulation,         6,      0.97,    0.9707,   0.97029,    54.704,    40.511,         0,  7789350,  1142450,  ,  ,  ,  
simulation,         7,      0.98,   0.97187,   0.97201,    458.15,    61.844,         0,  7829815,  1256731,  ,  ,  ,  
simulation,         9,       0.9,   0.90009,   0.90009,    16.081,    16.082,         0,  4385125,  586344,  ,  ,  ,  
simulation,        10,      0.85,   0.85006,   0.85006,    13.991,    13.991,         0,  4119225,  576470,  ,  ,  ,  
simulation,        11,       0.8,    0.8001,    0.8001,    12.826,    12.826,         0,  3850562,  568634,  ,  ,  ,  
