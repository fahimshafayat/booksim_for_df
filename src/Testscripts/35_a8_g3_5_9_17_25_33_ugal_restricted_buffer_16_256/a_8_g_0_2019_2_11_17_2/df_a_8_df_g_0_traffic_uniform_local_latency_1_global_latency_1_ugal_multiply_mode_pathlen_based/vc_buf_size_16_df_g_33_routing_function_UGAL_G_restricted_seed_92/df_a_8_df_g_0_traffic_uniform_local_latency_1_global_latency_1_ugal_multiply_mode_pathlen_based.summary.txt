wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = UGAL_G_restricted;
seed = 92;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49998,   0.49998,    11.502,    11.502,         0,  28400415,  3168757,  ,  ,  ,  
simulation,         2,      0.74,   0.74005,   0.74005,    14.222,    14.223,         0,  43578168,  3153555,  ,  ,  ,  
simulation,         3,      0.86,   0.85998,   0.85998,    18.528,    18.529,         0,  51214319,  3113123,  ,  ,  ,  
simulation,         4,      0.92,   0.91998,   0.91999,    24.606,    24.609,         0,  55081054,  3077044,  ,  ,  ,  
simulation,         5,      0.95,   0.94998,   0.94998,    33.199,    33.196,         0,  57011685,  3037051,  ,  ,  ,  
simulation,         6,      0.97,   0.93994,   0.93992,    751.21,    74.998,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.96,   0.94049,   0.94044,     631.6,    74.503,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.90002,   0.90001,    21.868,    21.869,         0,  53771593,  3098365,  ,  ,  ,  
simulation,        10,      0.85,   0.85001,   0.85001,    17.934,    17.934,         0,  50581348,  3112152,  ,  ,  ,  
simulation,        11,       0.8,   0.80001,   0.80001,    15.808,    15.808,         0,  47397885,  3128831,  ,  ,  ,  
