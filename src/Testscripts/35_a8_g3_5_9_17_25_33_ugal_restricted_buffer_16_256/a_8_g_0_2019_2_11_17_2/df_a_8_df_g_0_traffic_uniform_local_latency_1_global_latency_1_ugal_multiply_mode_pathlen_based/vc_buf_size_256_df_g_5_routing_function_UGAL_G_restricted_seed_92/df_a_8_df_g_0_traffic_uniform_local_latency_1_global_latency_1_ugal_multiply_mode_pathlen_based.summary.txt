wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_G_restricted;
seed = 92;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50009,    0.5001,    10.523,    10.523,         0,  3722244,  958142,  ,  ,  ,  
simulation,         2,      0.74,    0.7404,    0.7404,    12.467,    12.467,         0,  5757698,  1171733,  ,  ,  ,  
simulation,         3,      0.86,   0.86045,   0.86047,    15.078,    15.079,         0,  6834007,  1221684,  ,  ,  ,  
simulation,         4,      0.92,   0.92046,   0.92046,    18.603,    18.604,         0,  7386854,  1233417,  ,  ,  ,  
simulation,         5,      0.95,   0.95036,   0.95034,    22.681,    22.684,         0,  7672048,  1231512,  ,  ,  ,  
simulation,         6,      0.97,   0.97023,   0.97021,    29.602,    29.611,         0,  7866506,  1232064,  ,  ,  ,  
simulation,         7,      0.98,   0.98015,   0.98011,    38.326,    38.337,         0,  7972418,  1230593,  ,  ,  ,  
simulation,         9,       0.9,   0.90044,   0.90043,    17.004,    17.006,         0,  7202628,  1231080,  ,  ,  ,  
simulation,        10,      0.85,    0.8504,    0.8504,    14.739,     14.74,         0,  6743438,  1219290,  ,  ,  ,  
simulation,        11,       0.8,   0.80032,   0.80032,    13.447,    13.447,         0,  6291371,  1199070,  ,  ,  ,  
