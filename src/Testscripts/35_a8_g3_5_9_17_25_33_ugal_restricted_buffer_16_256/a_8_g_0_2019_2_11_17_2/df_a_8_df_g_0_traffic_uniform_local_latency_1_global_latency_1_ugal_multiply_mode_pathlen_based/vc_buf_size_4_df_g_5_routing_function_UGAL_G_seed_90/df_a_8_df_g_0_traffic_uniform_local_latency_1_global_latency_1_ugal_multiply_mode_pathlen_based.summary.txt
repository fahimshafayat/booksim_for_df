wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 5;
routing_function = UGAL_G;
seed = 90;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    10.661,    10.661,         0,  4397404,  285072,  ,  ,  ,  
simulation,         2,      0.74,   0.68337,   0.68452,    768.23,    32.816,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,    0.6199,    0.6199,    12.995,    12.977,         0,  5583552,  224196,  ,  ,  ,  
simulation,         4,      0.68,   0.67969,   0.67972,    53.874,    28.383,         0,  7283232,  167277,  ,  ,  ,  
simulation,         5,      0.71,   0.68411,   0.68418,    956.51,    32.925,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.69,   0.68284,   0.68284,    528.41,    32.965,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.65,   0.64986,   0.64988,    15.636,    15.479,         0,  5898608,  192829,  ,  ,  ,  
simulation,         8,       0.6,   0.59992,   0.59991,    12.205,      12.2,         0,  5378820,  239555,  ,  ,  ,  
simulation,         9,      0.55,   0.54996,   0.54996,    11.185,    11.185,         0,  4883242,  267699,  ,  ,  ,  
