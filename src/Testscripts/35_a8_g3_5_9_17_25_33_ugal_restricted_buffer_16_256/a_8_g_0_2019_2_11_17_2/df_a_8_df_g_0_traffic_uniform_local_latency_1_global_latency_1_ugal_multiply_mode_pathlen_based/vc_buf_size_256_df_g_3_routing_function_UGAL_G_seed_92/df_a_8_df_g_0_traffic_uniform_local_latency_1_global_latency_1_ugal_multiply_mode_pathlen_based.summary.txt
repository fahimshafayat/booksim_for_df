wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = UGAL_G;
seed = 92;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4996,   0.49958,    9.6771,    9.6773,         0,  2532771,  226137,  ,  ,  ,  
simulation,         2,      0.74,   0.73959,   0.73959,    11.177,    11.177,         0,  3861838,  223262,  ,  ,  ,  
simulation,         3,      0.86,   0.85989,   0.85988,    13.393,    13.393,         0,  4531862,  217394,  ,  ,  ,  
simulation,         4,      0.92,   0.92009,    0.9201,    16.527,    16.527,         0,  4864969,  216677,  ,  ,  ,  
simulation,         5,      0.95,   0.95003,   0.95004,    20.632,    20.633,         0,  5031789,  217385,  ,  ,  ,  
simulation,         6,      0.97,   0.96998,   0.97006,    26.837,    26.848,         0,  6005041,  252715,  ,  ,  ,  
simulation,         7,      0.98,   0.98019,   0.98005,    36.335,    36.354,         0,  5204014,  217569,  ,  ,  ,  
simulation,         9,       0.9,   0.90007,   0.90006,    15.115,    15.116,         0,  4754829,  216053,  ,  ,  ,  
simulation,        10,      0.85,   0.84985,   0.84985,    13.085,    13.086,         0,  4475251,  217745,  ,  ,  ,  
simulation,        11,       0.8,   0.79946,   0.79946,    11.974,    11.975,         0,  4195423,  221044,  ,  ,  ,  
