wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 25;
routing_function = UGAL_L_restricted;
seed = 92;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49994,   0.49994,     12.44,     12.44,         0,  18562315,  5324899,  ,  ,  ,  
simulation,         2,      0.74,   0.74001,   0.74001,    21.402,    21.404,         0,  28296020,  7083734,  ,  ,  ,  
simulation,         3,      0.86,   0.75779,   0.76688,    557.51,     112.3,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.75821,   0.75835,    1182.4,    97.607,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.76019,   0.76023,    538.79,    61.676,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.75,   0.75004,   0.75001,    24.559,    24.559,         0,  28279596,  7608376,  ,  ,  ,  
simulation,         7,      0.76,   0.75956,   0.75957,    67.848,    34.754,         0,  48298363,  14854921,  ,  ,  ,  
simulation,         9,       0.7,       0.7,       0.7,    16.872,    16.873,         0,  27052399,  6401793,  ,  ,  ,  
simulation,        10,      0.65,   0.64999,      0.65,    14.797,    14.797,         0,  24977043,  6084608,  ,  ,  ,  
simulation,        11,       0.6,   0.59999,   0.59998,    13.687,    13.687,         0,  22828238,  5837275,  ,  ,  ,  
