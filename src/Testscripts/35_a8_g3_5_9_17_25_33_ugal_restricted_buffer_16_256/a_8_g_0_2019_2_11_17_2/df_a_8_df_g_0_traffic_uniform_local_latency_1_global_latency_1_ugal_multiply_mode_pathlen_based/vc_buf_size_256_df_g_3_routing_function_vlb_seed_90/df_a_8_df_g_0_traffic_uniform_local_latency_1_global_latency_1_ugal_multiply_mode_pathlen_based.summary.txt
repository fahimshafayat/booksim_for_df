wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 3;
routing_function = vlb;
seed = 90;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50018,   0.50017,    21.077,    21.079,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.56565,   0.74038,    1233.0,    1233.0,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.59084,   0.61966,    497.93,    497.93,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.56025,   0.56024,    34.575,    34.578,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.59,   0.59036,   0.59024,    102.35,    102.33,         0,  0,  0,  ,  ,  ,  
simulation,         6,       0.6,   0.59783,   0.59999,    418.73,     421.2,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.61,   0.59573,   0.61038,    671.19,    671.19,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.55,   0.55035,    0.5503,    30.094,    30.101,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.45,   0.45027,   0.45026,    17.896,    17.897,         0,  0,  0,  ,  ,  ,  
