wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = min;
seed = 92;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49986,   0.49985,    12.077,    12.075,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.59338,   0.59585,    989.92,    41.044,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.59442,   0.59439,    1078.0,    41.289,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55981,   0.55982,    16.384,    16.279,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.59,      0.59,   0.58998,    57.469,      33.4,         0,  0,  0,  ,  ,  ,  
simulation,         6,       0.6,   0.59329,    0.5933,    522.02,    41.199,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.55,   0.54985,   0.54984,    14.954,    14.912,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.45,    0.4498,    0.4498,    11.134,    11.134,         0,  0,  0,  ,  ,  ,  
