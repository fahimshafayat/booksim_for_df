wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_L;
seed = 90;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49998,   0.49997,    12.092,    12.092,         0,  13630982,  2575790,  ,  ,  ,  
simulation,         2,      0.74,   0.73993,   0.73994,    15.136,    15.136,         0,  21144507,  2846392,  ,  ,  ,  
simulation,         3,      0.86,   0.85987,   0.85987,    20.429,    20.429,         0,  24993188,  2896398,  ,  ,  ,  
simulation,         4,      0.92,   0.91993,   0.91991,    28.695,    28.697,         0,  26959927,  2887648,  ,  ,  ,  
simulation,         5,      0.95,   0.94995,   0.94994,    40.042,    40.048,         0,  28052310,  2791450,  ,  ,  ,  
simulation,         6,      0.97,    0.9699,   0.96994,    57.133,    57.152,         0,  28980324,  2556911,  ,  ,  ,  
simulation,         7,      0.98,   0.97999,   0.98001,    74.806,     74.83,         0,  29537653,  2365095,  ,  ,  ,  
simulation,         9,       0.9,   0.89989,   0.89989,    24.842,    24.844,         0,  26292690,  2900072,  ,  ,  ,  
simulation,        10,      0.85,   0.84987,   0.84988,    19.673,    19.674,         0,  24670331,  2893247,  ,  ,  ,  
simulation,        11,       0.8,   0.79982,   0.79983,    17.018,    17.019,         0,  23059260,  2878778,  ,  ,  ,  
