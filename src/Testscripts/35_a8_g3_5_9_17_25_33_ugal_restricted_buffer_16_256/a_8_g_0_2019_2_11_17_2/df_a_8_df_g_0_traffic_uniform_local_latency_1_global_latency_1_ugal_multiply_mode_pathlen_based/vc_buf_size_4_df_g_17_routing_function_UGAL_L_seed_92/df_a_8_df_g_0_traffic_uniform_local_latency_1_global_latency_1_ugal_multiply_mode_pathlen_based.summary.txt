wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 17;
routing_function = UGAL_L;
seed = 92;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49985,   0.49985,    12.885,    12.886,         0,  13791006,  2414956,  ,  ,  ,  
simulation,         2,      0.74,   0.60852,   0.61108,    890.62,    41.423,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,    0.6094,   0.60938,    637.61,     41.78,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55983,   0.55981,    15.091,    15.081,         0,  15813276,  2343686,  ,  ,  ,  
simulation,         5,      0.59,   0.58983,   0.58983,    20.315,    20.081,         0,  16986933,  2168209,  ,  ,  ,  
simulation,         6,       0.6,   0.59987,   0.59985,    27.617,    25.789,         0,  17443520,  2046307,  ,  ,  ,  
simulation,         7,      0.61,   0.60844,   0.60845,    234.25,    40.859,         0,  30179098,  3174176,  ,  ,  ,  
simulation,         9,      0.55,   0.54985,   0.54984,    14.434,     14.43,         0,  15458175,  2373803,  ,  ,  ,  
simulation,        11,      0.45,    0.4498,    0.4498,    12.191,    12.191,         0,  12194301,  2388548,  ,  ,  ,  
