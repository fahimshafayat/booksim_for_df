wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
df_g = 33;
routing_function = UGAL_L;
seed = 92;
vc_buf_size = 4;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49998,   0.49998,    13.311,    13.311,         0,  26953110,  4623090,  ,  ,  ,  
simulation,         2,      0.74,   0.59303,   0.59561,    993.99,    43.209,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.59405,   0.59406,    1071.2,    43.489,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.56002,   0.56003,    16.733,    16.707,         0,  31024903,  4379863,  ,  ,  ,  
simulation,         5,      0.59,   0.59004,   0.59004,     42.96,    32.986,         0,  39265238,  4395297,  ,  ,  ,  
simulation,         6,       0.6,   0.59366,   0.59366,    500.82,    43.476,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.55,      0.55,      0.55,     15.62,    15.608,         0,  30287656,  4468616,  ,  ,  ,  
simulation,        10,      0.45,      0.45,      0.45,    12.467,    12.467,         0,  23820040,  4593756,  ,  ,  ,  
