wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 8;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 1;
local_latency = 1;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
routing_function = PAR;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.3847,   0.42705,     990.9,    740.88,         1,         0.0
simulation,         2,      0.25,    0.2501,    0.2501,    20.072,    20.072,         0,      6.4326
simulation,         3,      0.37,   0.37011,    0.3701,     36.84,    36.853,         0,      6.7201
simulation,         4,      0.43,   0.38735,   0.40108,    793.82,    516.57,         1,         0.0
simulation,         5,       0.4,   0.39333,   0.39345,    503.12,    140.41,         1,         0.0
simulation,         6,      0.38,   0.37866,   0.37871,    414.27,    65.701,         0,      6.7138
simulation,         7,      0.39,   0.38691,    0.3869,     404.1,    90.192,         1,         0.0
simulation,         8,       0.1,   0.10006,   0.10006,    14.672,    14.672,         0,      5.9464
simulation,         9,       0.2,   0.20005,   0.20005,    17.369,    17.369,         0,      6.1863
simulation,        10,       0.3,   0.30012,   0.30013,    23.732,    23.733,         0,       6.602
