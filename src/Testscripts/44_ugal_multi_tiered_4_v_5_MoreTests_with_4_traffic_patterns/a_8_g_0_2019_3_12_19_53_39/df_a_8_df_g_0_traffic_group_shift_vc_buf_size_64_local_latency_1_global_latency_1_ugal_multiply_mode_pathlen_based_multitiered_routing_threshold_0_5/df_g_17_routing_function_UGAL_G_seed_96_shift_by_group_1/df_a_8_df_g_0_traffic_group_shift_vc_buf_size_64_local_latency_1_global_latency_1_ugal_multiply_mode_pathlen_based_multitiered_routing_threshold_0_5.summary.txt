wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_G;
seed = 96;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50003,   0.50003,    37.992,    37.992,         0,      6.0692
simulation,         2,      0.74,   0.41691,    0.4593,    1831.6,    856.67,         1,         0.0
simulation,         3,      0.62,   0.47148,   0.50601,    910.37,    565.52,         1,         0.0
simulation,         4,      0.56,   0.49124,    0.5029,    899.07,    412.01,         1,         0.0
simulation,         5,      0.53,   0.51248,   0.51256,    669.39,    224.55,         1,         0.0
simulation,         6,      0.51,   0.51003,   0.51004,    43.526,    43.529,         0,      6.0785
simulation,         7,      0.52,   0.52004,   0.52006,    53.276,    53.278,         0,      6.0871
simulation,         8,       0.1,   0.09997,   0.09997,    13.614,    13.615,         0,      5.3591
simulation,         9,       0.2,   0.20001,       0.2,    15.413,    15.414,         0,      5.5553
simulation,        10,       0.3,   0.30004,   0.30003,    18.249,    18.249,         0,      5.8192
simulation,        11,       0.4,   0.40007,   0.40006,    22.485,    22.486,         0,      5.9689
simulation,        12,       0.5,   0.50003,   0.50003,    37.992,    37.992,         0,      6.0692
