wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = UGAL_G;
seed = 96;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.37203,   0.40835,     973.3,    687.56,         1,         0.0
simulation,         2,      0.25,      0.25,      0.25,    17.694,    17.695,         0,      5.9684
simulation,         3,      0.37,   0.37006,   0.37006,     23.95,    23.951,         0,      6.1269
simulation,         4,      0.43,   0.39781,   0.40689,    535.01,    367.75,         1,         0.0
simulation,         5,       0.4,   0.40008,   0.40008,    32.953,    32.953,         0,      6.1567
simulation,         6,      0.41,   0.40159,   0.40169,     495.9,    210.26,         1,         0.0
simulation,         7,       0.1,  0.099939,  0.099935,    14.376,    14.376,         0,      5.6108
simulation,         8,       0.2,   0.19991,    0.1999,    16.324,    16.324,         0,      5.8541
simulation,         9,       0.3,   0.30005,   0.30004,    19.393,    19.393,         0,      6.0479
simulation,        10,       0.4,   0.40008,   0.40008,    32.953,    32.953,         0,      6.1567
