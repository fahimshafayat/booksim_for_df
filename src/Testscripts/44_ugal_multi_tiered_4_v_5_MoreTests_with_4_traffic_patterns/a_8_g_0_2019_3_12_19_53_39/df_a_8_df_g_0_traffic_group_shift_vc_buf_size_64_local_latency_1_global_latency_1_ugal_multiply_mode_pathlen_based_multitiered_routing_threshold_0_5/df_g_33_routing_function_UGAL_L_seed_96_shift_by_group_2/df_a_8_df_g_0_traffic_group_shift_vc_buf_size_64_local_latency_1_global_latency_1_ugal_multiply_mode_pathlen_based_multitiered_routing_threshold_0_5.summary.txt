wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = UGAL_L;
seed = 96;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.39124,   0.43042,    952.21,    699.75,         1,         0.0
simulation,         2,      0.25,   0.25001,      0.25,    32.765,    32.823,         0,      6.1393
simulation,         3,      0.37,   0.36807,   0.36956,    357.26,    338.97,         0,      6.2576
simulation,         4,      0.43,   0.38058,   0.40018,    990.95,    702.32,         1,         0.0
simulation,         5,       0.4,   0.37247,   0.37568,    1435.4,    1002.1,         1,         0.0
simulation,         6,      0.38,   0.37063,   0.37686,    666.37,    636.05,         1,         0.0
simulation,         7,       0.1,  0.099918,  0.099917,    35.402,    35.433,         0,       5.622
simulation,         8,       0.2,   0.19995,   0.19994,    32.419,    32.466,         0,      6.0489
simulation,         9,       0.3,       0.3,   0.29999,    34.492,     34.57,         0,      6.2006
