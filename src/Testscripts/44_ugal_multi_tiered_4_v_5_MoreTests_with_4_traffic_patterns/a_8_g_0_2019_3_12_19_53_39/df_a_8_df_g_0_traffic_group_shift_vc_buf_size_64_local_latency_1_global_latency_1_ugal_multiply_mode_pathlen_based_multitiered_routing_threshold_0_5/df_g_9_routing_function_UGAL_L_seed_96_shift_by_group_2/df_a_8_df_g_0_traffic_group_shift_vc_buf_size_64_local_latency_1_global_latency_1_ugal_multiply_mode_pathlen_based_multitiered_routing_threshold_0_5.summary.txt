wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_L;
seed = 96;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35163,   0.40038,    1339.0,    971.52,         1,         0.0
simulation,         2,      0.25,   0.24991,   0.24992,    52.083,    52.122,         0,      5.1182
simulation,         3,      0.37,   0.35446,   0.35503,    1047.3,    711.31,         1,         0.0
simulation,         4,      0.31,   0.31016,   0.31016,    62.114,     62.22,         0,      5.3741
simulation,         5,      0.34,   0.33946,      0.34,    214.49,    208.28,         0,      5.4661
simulation,         6,      0.35,   0.34508,   0.34846,    552.94,    523.79,         1,         0.0
simulation,         7,       0.1,  0.099968,   0.09997,    11.993,    11.993,         0,      4.7171
simulation,         8,       0.2,    0.1999,   0.19992,    18.618,    18.622,         0,      4.8206
simulation,         9,       0.3,   0.30007,   0.30008,    59.852,    59.939,         0,      5.3377
