wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = UGAL_G_restricted_src_only;
seed = 96;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.41195,   0.43888,    603.04,    452.85,         1,         0.0
simulation,         2,      0.25,      0.25,      0.25,    14.819,    14.819,         0,      5.1373
simulation,         3,      0.37,   0.37006,   0.37006,    18.516,    18.517,         0,      5.2443
simulation,         4,      0.43,   0.41127,   0.41117,    701.69,    254.57,         1,         0.0
simulation,         5,       0.4,   0.40009,   0.40008,    22.677,    22.678,         0,      5.2658
simulation,         6,      0.41,   0.40947,   0.40948,    112.29,    60.778,         0,      5.2728
simulation,         7,      0.42,   0.41138,   0.41147,     471.8,    146.14,         1,         0.0
simulation,         8,       0.1,  0.099938,  0.099935,    12.855,    12.855,         0,      4.9624
simulation,         9,       0.2,    0.1999,    0.1999,    14.011,    14.011,         0,      5.0768
simulation,        10,       0.3,   0.30004,   0.30004,     15.86,     15.86,         0,      5.1899
simulation,        11,       0.4,   0.40009,   0.40008,    22.677,    22.678,         0,      5.2658
