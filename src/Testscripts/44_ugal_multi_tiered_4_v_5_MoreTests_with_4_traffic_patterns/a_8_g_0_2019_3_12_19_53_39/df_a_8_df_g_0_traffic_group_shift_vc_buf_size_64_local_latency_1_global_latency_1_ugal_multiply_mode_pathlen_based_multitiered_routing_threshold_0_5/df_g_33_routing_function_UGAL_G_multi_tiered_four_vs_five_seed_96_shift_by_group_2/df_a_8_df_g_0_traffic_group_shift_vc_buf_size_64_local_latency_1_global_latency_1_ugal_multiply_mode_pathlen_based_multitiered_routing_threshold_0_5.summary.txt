wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = UGAL_G_multi_tiered_four_vs_five;
seed = 96;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.43469,   0.45686,    452.36,    347.26,         1,         0.0
simulation,         2,      0.25,   0.25001,      0.25,    13.578,    13.579,         0,      4.4403
simulation,         3,      0.37,   0.36994,   0.36994,     47.23,    47.256,         0,      4.7416
simulation,         4,      0.43,   0.42572,   0.42571,    506.25,    118.65,         1,         0.0
simulation,         5,       0.4,   0.39991,   0.39993,    51.287,     51.43,         0,      4.9454
simulation,         6,      0.41,    0.4098,   0.40982,    68.467,    57.447,         0,      5.0113
simulation,         7,      0.42,   0.41881,   0.41884,     263.5,    89.848,         0,      5.0731
simulation,         8,       0.1,  0.099916,  0.099917,    11.588,    11.588,         0,       4.303
simulation,         9,       0.2,   0.19994,   0.19994,    12.691,    12.692,         0,      4.3948
simulation,        10,       0.3,       0.3,   0.29999,    15.483,    15.483,         0,      4.4892
simulation,        11,       0.4,   0.39991,   0.39993,    51.287,     51.43,         0,      4.9454
