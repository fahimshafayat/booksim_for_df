wait_for_tail_credit = 0;
sw_allocator = separable_input_first;
internal_speedup = 4.0;
sample_period = 10000;
input_speedup = 1;
routing_delay = 0;
sw_alloc_delay = 1;
injection_rate_uses_flits = 1;
num_vcs = 7;
sim_count = 1;
warmup_periods = 3;
alloc_iters = 1;
vc_allocator = separable_input_first;
priority = none;
st_final_delay = 1;
credit_delay = 2;
output_speedup = 1;
vc_alloc_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_G;
seed = 94;
shift_by_group = 5;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50013,    28.842,    28.841,         0,      5.6765
simulation,         2,      0.74,   0.47338,   0.51712,    1582.8,    769.31,         1,         0.0
simulation,         3,      0.62,   0.50693,   0.53899,     720.2,    484.34,         1,         0.0
simulation,         4,      0.56,   0.56007,   0.56008,    61.853,    61.852,         0,      5.7589
simulation,         5,      0.59,   0.51851,   0.54435,    458.86,    353.97,         1,         0.0
simulation,         6,      0.57,   0.52954,   0.53911,    541.17,    295.23,         1,         0.0
simulation,         7,       0.1,   0.10022,   0.10022,    12.451,    12.451,         0,      4.9493
simulation,         8,       0.2,   0.20004,   0.20005,    13.672,    13.673,         0,      5.0336
simulation,         9,       0.3,   0.30016,   0.30016,     16.21,     16.21,         0,       5.233
simulation,        10,       0.4,   0.40028,   0.40028,    20.466,    20.466,         0,      5.5034
simulation,        11,       0.5,   0.50014,   0.50013,    28.842,    28.841,         0,      5.6765
