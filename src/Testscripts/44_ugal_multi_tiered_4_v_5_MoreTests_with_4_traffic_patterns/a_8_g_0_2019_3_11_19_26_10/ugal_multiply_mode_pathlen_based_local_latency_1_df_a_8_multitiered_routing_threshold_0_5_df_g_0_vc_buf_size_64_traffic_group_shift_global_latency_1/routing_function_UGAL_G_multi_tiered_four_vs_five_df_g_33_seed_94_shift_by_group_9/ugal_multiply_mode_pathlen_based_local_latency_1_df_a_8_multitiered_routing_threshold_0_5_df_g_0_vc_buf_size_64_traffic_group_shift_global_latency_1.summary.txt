wait_for_tail_credit = 0;
sw_allocator = separable_input_first;
internal_speedup = 4.0;
sample_period = 10000;
input_speedup = 1;
routing_delay = 0;
sw_alloc_delay = 1;
injection_rate_uses_flits = 1;
num_vcs = 7;
sim_count = 1;
warmup_periods = 3;
alloc_iters = 1;
vc_allocator = separable_input_first;
priority = none;
st_final_delay = 1;
credit_delay = 2;
output_speedup = 1;
vc_alloc_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = UGAL_G_multi_tiered_four_vs_five;
seed = 94;
shift_by_group = 9;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4658,   0.47349,     496.0,    266.64,         1,         0.0
simulation,         2,      0.25,   0.25004,   0.25004,    13.866,    13.866,         0,      4.5889
simulation,         3,      0.37,   0.36998,   0.36999,    44.512,    44.324,         0,      4.8567
simulation,         4,      0.43,   0.42986,   0.42984,     66.43,    52.315,         0,      5.1552
simulation,         5,      0.46,   0.45757,   0.45761,    445.78,    94.964,         0,      5.3068
simulation,         6,      0.48,   0.46566,   0.46576,    510.24,    184.27,         1,         0.0
simulation,         7,      0.47,   0.46489,   0.46494,    499.59,    119.32,         1,         0.0
simulation,         8,       0.1,   0.10001,   0.10001,    11.841,    11.841,         0,      4.4352
simulation,         9,       0.2,   0.20004,   0.20004,     12.96,     12.96,         0,      4.5407
simulation,        10,       0.3,   0.30001,   0.30001,    21.918,    21.928,         0,      4.6441
simulation,        11,       0.4,   0.39982,   0.39983,    64.458,    49.096,         0,      5.0045
