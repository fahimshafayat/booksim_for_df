wait_for_tail_credit = 0;
sw_allocator = separable_input_first;
internal_speedup = 4.0;
sample_period = 10000;
input_speedup = 1;
routing_delay = 0;
sw_alloc_delay = 1;
injection_rate_uses_flits = 1;
num_vcs = 7;
sim_count = 1;
warmup_periods = 3;
alloc_iters = 1;
vc_allocator = separable_input_first;
priority = none;
st_final_delay = 1;
credit_delay = 2;
output_speedup = 1;
vc_alloc_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_L;
seed = 94;
shift_by_group = 5;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.3517,    0.4007,    1344.0,    970.48,         1,         0.0
simulation,         2,      0.25,   0.25013,   0.25013,     52.09,     52.13,         0,      5.1215
simulation,         3,      0.37,    0.3541,   0.35531,    1003.9,    706.66,         1,         0.0
simulation,         4,      0.31,   0.31016,   0.31015,    62.275,    62.361,         0,      5.3757
simulation,         5,      0.34,   0.33982,   0.34017,    157.39,    157.64,         0,        5.47
simulation,         6,      0.35,    0.3449,   0.34856,    490.94,    483.18,         1,         0.0
simulation,         7,       0.1,   0.10022,   0.10022,    12.009,    12.009,         0,      4.7257
simulation,         8,       0.2,   0.20005,   0.20005,    18.517,    18.517,         0,      4.8278
simulation,         9,       0.3,   0.30016,   0.30016,    59.812,     59.89,         0,      5.3406
