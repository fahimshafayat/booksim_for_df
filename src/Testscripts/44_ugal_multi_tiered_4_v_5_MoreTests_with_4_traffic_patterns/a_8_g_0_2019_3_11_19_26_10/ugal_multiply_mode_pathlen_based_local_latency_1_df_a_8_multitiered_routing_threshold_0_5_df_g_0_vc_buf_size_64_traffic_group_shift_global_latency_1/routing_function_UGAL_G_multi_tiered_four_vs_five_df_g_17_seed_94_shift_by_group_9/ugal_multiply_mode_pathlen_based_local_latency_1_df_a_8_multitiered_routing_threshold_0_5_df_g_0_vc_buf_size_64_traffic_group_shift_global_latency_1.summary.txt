wait_for_tail_credit = 0;
sw_allocator = separable_input_first;
internal_speedup = 4.0;
sample_period = 10000;
input_speedup = 1;
routing_delay = 0;
sw_alloc_delay = 1;
injection_rate_uses_flits = 1;
num_vcs = 7;
sim_count = 1;
warmup_periods = 3;
alloc_iters = 1;
vc_allocator = separable_input_first;
priority = none;
st_final_delay = 1;
credit_delay = 2;
output_speedup = 1;
vc_alloc_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_G_multi_tiered_four_vs_five;
seed = 94;
shift_by_group = 9;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4999,   0.49989,    55.142,    55.167,         0,      4.8928
simulation,         2,      0.74,   0.45915,   0.50127,    1636.4,    752.63,         1,         0.0
simulation,         3,      0.62,   0.49972,   0.53202,    738.62,     489.1,         1,         0.0
simulation,         4,      0.56,   0.51693,   0.52567,    568.89,    292.11,         1,         0.0
simulation,         5,      0.53,   0.52281,   0.52287,    483.07,     170.2,         1,         0.0
simulation,         6,      0.51,   0.50988,   0.50992,    59.778,      59.8,         0,      4.9384
simulation,         7,      0.52,   0.51994,   0.51994,    68.591,    68.611,         0,      4.9997
simulation,         8,       0.1,  0.099815,  0.099819,    11.311,    11.311,         0,      4.3186
simulation,         9,       0.2,   0.19986,   0.19987,    12.376,    12.376,         0,      4.4009
simulation,        10,       0.3,   0.29984,   0.29985,    13.931,    13.932,         0,      4.4833
simulation,        11,       0.4,   0.39986,   0.39987,    18.781,    18.783,         0,      4.5828
simulation,        12,       0.5,    0.4999,   0.49989,    55.142,    55.167,         0,      4.8928
