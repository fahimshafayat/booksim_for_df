wait_for_tail_credit = 0;
sw_allocator = separable_input_first;
internal_speedup = 4.0;
sample_period = 10000;
input_speedup = 1;
routing_delay = 0;
sw_alloc_delay = 1;
injection_rate_uses_flits = 1;
num_vcs = 7;
sim_count = 1;
warmup_periods = 3;
alloc_iters = 1;
vc_allocator = separable_input_first;
priority = none;
st_final_delay = 1;
credit_delay = 2;
output_speedup = 1;
vc_alloc_delay = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_G_restricted_src_only;
seed = 94;
shift_by_group = 3;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50013,    19.241,     19.24,         0,      4.6635
simulation,         2,      0.74,   0.53703,   0.57536,    1158.4,    597.15,         1,         0.0
simulation,         3,      0.62,   0.55313,   0.56612,    859.68,    398.59,         1,         0.0
simulation,         4,      0.56,   0.56008,   0.56008,     37.16,    37.161,         0,      4.7127
simulation,         5,      0.59,   0.55702,   0.55712,    1106.5,    302.93,         1,         0.0
simulation,         6,      0.57,   0.55931,   0.55875,    538.26,    222.98,         1,         0.0
simulation,         7,       0.1,   0.10022,   0.10022,    11.268,    11.268,         0,      4.4201
simulation,         8,       0.2,   0.20005,   0.20005,    12.085,    12.085,         0,      4.4618
simulation,         9,       0.3,   0.30015,   0.30016,    13.177,    13.177,         0,      4.4984
simulation,        10,       0.4,   0.40028,   0.40028,    15.184,    15.184,         0,      4.5801
simulation,        11,       0.5,   0.50014,   0.50013,    19.241,     19.24,         0,      4.6635
