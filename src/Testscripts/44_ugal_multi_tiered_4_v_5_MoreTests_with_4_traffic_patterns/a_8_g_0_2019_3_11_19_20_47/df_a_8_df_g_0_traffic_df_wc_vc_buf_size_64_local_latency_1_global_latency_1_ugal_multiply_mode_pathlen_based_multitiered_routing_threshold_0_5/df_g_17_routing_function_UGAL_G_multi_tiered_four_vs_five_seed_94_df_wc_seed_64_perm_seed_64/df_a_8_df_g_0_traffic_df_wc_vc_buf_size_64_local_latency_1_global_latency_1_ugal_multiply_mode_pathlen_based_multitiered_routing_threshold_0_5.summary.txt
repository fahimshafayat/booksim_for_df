wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 64;
perm_seed = 64;
routing_function = UGAL_G_multi_tiered_four_vs_five;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49987,   0.49989,    50.868,    50.886,         0,      5.0047
simulation,         2,      0.74,   0.46819,   0.50813,    1531.9,    710.84,         1,         0.0
simulation,         3,      0.62,   0.49983,   0.53083,    734.79,    475.79,         1,         0.0
simulation,         4,      0.56,   0.51531,   0.52472,    603.73,    310.65,         1,         0.0
simulation,         5,      0.53,    0.5229,   0.52301,    499.21,     159.0,         1,         0.0
simulation,         6,      0.51,   0.50993,   0.50992,    55.882,    55.897,         0,      5.0474
simulation,         7,      0.52,   0.51993,   0.51994,    63.591,    63.603,         0,      5.1121
simulation,         8,       0.1,  0.099814,  0.099819,    11.551,    11.551,         0,      4.4332
simulation,         9,       0.2,   0.19987,   0.19987,    12.668,    12.668,         0,      4.5307
simulation,        10,       0.3,   0.29985,   0.29985,     14.27,     14.27,         0,      4.6303
simulation,        11,       0.4,   0.39986,   0.39987,    18.381,    18.382,         0,      4.7455
simulation,        12,       0.5,   0.49987,   0.49989,    50.868,    50.886,         0,      5.0047
