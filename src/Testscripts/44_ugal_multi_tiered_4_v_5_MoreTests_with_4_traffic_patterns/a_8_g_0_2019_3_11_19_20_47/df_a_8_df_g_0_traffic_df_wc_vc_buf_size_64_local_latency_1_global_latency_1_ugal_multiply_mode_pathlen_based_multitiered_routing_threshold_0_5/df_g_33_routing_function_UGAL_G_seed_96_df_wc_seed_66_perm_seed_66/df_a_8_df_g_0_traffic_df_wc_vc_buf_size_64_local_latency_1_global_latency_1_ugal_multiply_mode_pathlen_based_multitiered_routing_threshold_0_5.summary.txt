wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 66;
perm_seed = 66;
routing_function = UGAL_G;
seed = 96;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49997,   0.49995,    54.992,    54.991,         0,      6.2895
simulation,         2,      0.74,   0.39184,   0.43316,    1937.3,    865.73,         1,         0.0
simulation,         3,      0.62,   0.40324,    0.4436,    1272.5,    725.76,         1,         0.0
simulation,         4,      0.56,   0.43096,   0.46787,     794.1,    562.57,         1,         0.0
simulation,         5,      0.53,   0.45522,   0.48645,    474.05,    388.92,         1,         0.0
simulation,         6,      0.51,   0.47372,   0.47336,    961.44,    275.51,         1,         0.0
simulation,         7,       0.1,  0.099916,  0.099917,    14.778,    14.778,         0,      5.7682
simulation,         8,       0.2,   0.19995,   0.19994,     16.47,     16.47,         0,      6.0072
simulation,         9,       0.3,       0.3,   0.29999,    18.865,    18.866,         0,      6.1556
simulation,        10,       0.4,   0.39994,   0.39993,    23.396,    23.396,         0,      6.2336
simulation,        11,       0.5,   0.49997,   0.49995,    54.992,    54.991,         0,      6.2895
