wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 64;
perm_seed = 64;
routing_function = UGAL_G_restricted_src_only;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46904,   0.46884,    748.41,    192.79,         1,         0.0
simulation,         2,      0.25,   0.25004,   0.25004,    15.417,    15.417,         0,      5.4354
simulation,         3,      0.37,      0.37,      0.37,    18.276,    18.276,         0,      5.5204
simulation,         4,      0.43,   0.42999,   0.42999,     22.02,    22.021,         0,      5.5496
simulation,         5,      0.46,   0.45997,   0.45997,    27.275,    27.278,         0,      5.5648
simulation,         6,      0.48,   0.47912,    0.4791,    247.34,    43.705,         0,       5.576
simulation,         7,      0.49,    0.4765,   0.47639,    429.15,    108.89,         1,         0.0
simulation,         8,       0.1,   0.10001,   0.10001,    13.533,    13.533,         0,      5.2304
simulation,         9,       0.2,   0.20004,   0.20004,    14.672,    14.672,         0,      5.3747
simulation,        10,       0.3,   0.30001,   0.30001,    16.346,    16.346,         0,      5.4782
simulation,        11,       0.4,   0.39998,   0.39997,     19.69,    19.691,         0,      5.5352
