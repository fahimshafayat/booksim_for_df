wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 64;
perm_seed = 64;
routing_function = UGAL_L_restricted_src_only;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46722,   0.47546,    469.57,    264.01,         1,         0.0
simulation,         2,      0.25,   0.24984,   0.24984,    34.172,    34.211,         0,      5.0618
simulation,         3,      0.37,   0.36989,   0.36989,    34.911,    34.969,         0,      5.1984
simulation,         4,      0.43,   0.42993,   0.42992,    39.229,    39.313,         0,      5.2411
simulation,         5,      0.46,   0.45429,   0.45438,    499.52,    102.79,         1,         0.0
simulation,         6,      0.44,   0.43894,   0.43896,     169.6,    56.336,         0,       5.247
simulation,         7,      0.45,   0.44706,   0.44705,    475.65,    80.285,         0,      5.2516
simulation,         8,       0.1,  0.099815,  0.099819,    12.284,    12.284,         0,       4.668
simulation,         9,       0.2,   0.19986,   0.19987,    35.004,    35.037,         0,      4.9601
simulation,        10,       0.3,   0.29985,   0.29985,    33.866,    33.912,         0,      5.1313
simulation,        11,       0.4,   0.39987,   0.39987,     36.22,    36.285,         0,      5.2209
