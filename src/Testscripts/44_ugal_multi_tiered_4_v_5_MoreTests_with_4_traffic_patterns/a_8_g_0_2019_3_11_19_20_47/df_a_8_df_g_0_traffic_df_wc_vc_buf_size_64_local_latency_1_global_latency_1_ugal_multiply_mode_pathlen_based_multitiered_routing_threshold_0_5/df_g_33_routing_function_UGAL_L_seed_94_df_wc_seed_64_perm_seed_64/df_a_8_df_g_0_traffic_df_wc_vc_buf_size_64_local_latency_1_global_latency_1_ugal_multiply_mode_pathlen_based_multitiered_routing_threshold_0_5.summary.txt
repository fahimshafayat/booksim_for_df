wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 64;
perm_seed = 64;
routing_function = UGAL_L;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.38689,   0.42616,     981.9,    711.92,         1,         0.0
simulation,         2,      0.25,   0.25004,   0.25004,    32.549,    32.604,         0,        6.15
simulation,         3,      0.37,   0.36722,   0.36895,    461.14,    403.66,         0,      6.2653
simulation,         4,      0.43,   0.37917,   0.39878,    1026.2,    709.83,         1,         0.0
simulation,         5,       0.4,   0.37603,   0.39396,    492.03,    456.39,         1,         0.0
simulation,         6,      0.38,   0.36936,   0.37777,    523.17,    503.39,         1,         0.0
simulation,         7,       0.1,   0.10001,   0.10001,    35.282,    35.309,         0,      5.6404
simulation,         8,       0.2,   0.20004,   0.20004,     32.22,    32.264,         0,      6.0625
simulation,         9,       0.3,   0.30001,   0.30001,    34.228,    34.301,         0,      6.2099
