wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 66;
perm_seed = 66;
routing_function = UGAL_G_multi_tiered_four_vs_five;
seed = 96;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50008,   0.50007,    11.697,    11.697,         0,       3.831
simulation,         2,      0.74,   0.74002,   0.74001,    15.099,    15.099,         0,      3.7904
simulation,         3,      0.86,   0.85997,   0.85998,    24.834,    24.837,         0,       3.808
simulation,         4,      0.92,   0.85031,   0.85081,    1465.9,    383.92,         1,         0.0
simulation,         5,      0.89,   0.88995,   0.88995,     35.99,    35.994,         0,      3.8345
simulation,         6,       0.9,   0.89996,   0.89995,    44.781,    44.791,         0,      3.8469
simulation,         7,      0.91,   0.85705,   0.85748,    898.41,    356.58,         1,         0.0
simulation,         8,       0.1,  0.099938,  0.099935,    10.075,    10.075,         0,      3.9242
simulation,         9,       0.2,   0.19991,    0.1999,    10.312,    10.312,         0,      3.8979
simulation,        10,       0.3,   0.30004,   0.30004,    10.638,    10.638,         0,      3.8733
simulation,        11,       0.4,   0.40009,   0.40008,    11.082,    11.082,         0,      3.8514
simulation,        12,       0.5,   0.50008,   0.50007,    11.697,    11.697,         0,       3.831
simulation,        13,       0.6,   0.60009,   0.60009,     12.59,     12.59,         0,       3.811
simulation,        14,       0.7,       0.7,   0.69999,    14.102,    14.102,         0,      3.7942
simulation,        15,       0.8,   0.79999,   0.79998,    17.633,    17.634,         0,      3.7895
simulation,        16,       0.9,   0.89996,   0.89995,    44.781,    44.791,         0,      3.8469
