wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 66;
perm_seed = 66;
routing_function = UGAL_G_multi_tiered_four_vs_five;
seed = 96;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50003,   0.50003,    11.411,    11.411,         0,      3.7758
simulation,         2,      0.74,   0.73986,   0.73987,    13.973,    13.973,         0,      3.7283
simulation,         3,      0.86,   0.86001,   0.85999,    17.723,    17.724,         0,      3.7083
simulation,         4,      0.92,   0.92002,   0.92002,    22.742,    22.744,         0,         3.7
simulation,         5,      0.95,   0.95004,   0.95001,     28.64,    28.642,         0,      3.6953
simulation,         6,      0.97,   0.97006,   0.97002,    37.721,    37.731,         0,      3.6919
simulation,         7,      0.98,      0.98,   0.97999,    47.534,    47.558,         0,      3.6895
simulation,         8,       0.1,   0.09997,   0.09997,    9.9519,     9.952,         0,      3.8682
simulation,         9,       0.2,       0.2,       0.2,    10.171,    10.171,         0,       3.842
simulation,        10,       0.3,   0.30003,   0.30003,    10.464,    10.464,         0,      3.8166
simulation,        11,       0.4,   0.40007,   0.40006,    10.865,    10.865,         0,      3.7954
simulation,        12,       0.5,   0.50003,   0.50003,    11.411,    11.411,         0,      3.7758
simulation,        13,       0.6,   0.60001,   0.60002,    12.165,    12.165,         0,      3.7554
simulation,        14,       0.7,   0.69993,   0.69993,    13.313,    13.313,         0,      3.7353
simulation,        15,       0.8,   0.79986,   0.79986,    15.363,    15.363,         0,      3.7178
simulation,        16,       0.9,   0.90004,   0.90003,     20.53,    20.531,         0,      3.7029
