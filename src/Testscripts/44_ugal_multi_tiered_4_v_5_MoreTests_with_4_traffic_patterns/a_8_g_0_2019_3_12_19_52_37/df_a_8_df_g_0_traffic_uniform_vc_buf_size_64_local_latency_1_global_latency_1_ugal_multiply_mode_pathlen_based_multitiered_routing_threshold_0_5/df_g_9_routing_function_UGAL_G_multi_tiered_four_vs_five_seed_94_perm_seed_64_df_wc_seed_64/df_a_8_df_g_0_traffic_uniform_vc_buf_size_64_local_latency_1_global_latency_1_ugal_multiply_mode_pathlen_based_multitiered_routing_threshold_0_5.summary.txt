wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 64;
perm_seed = 64;
routing_function = UGAL_G_multi_tiered_four_vs_five;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50013,   0.50013,    11.052,    11.052,         0,      3.6677
simulation,         2,      0.74,   0.74021,   0.74021,    13.384,    13.384,         0,      3.6314
simulation,         3,      0.86,   0.86016,   0.86017,    16.556,    16.557,         0,      3.6144
simulation,         4,      0.92,   0.92006,   0.92007,    20.706,    20.708,         0,      3.6064
simulation,         5,      0.95,   0.95008,   0.95006,    25.702,    25.705,         0,      3.6025
simulation,         6,      0.97,   0.97002,   0.97002,    33.452,    33.461,         0,      3.5989
simulation,         7,      0.98,   0.98003,   0.98002,    42.023,     42.05,         0,      3.5978
simulation,         8,       0.1,   0.10022,   0.10022,    9.7002,    9.7003,         0,      3.7495
simulation,         9,       0.2,   0.20005,   0.20005,    9.9064,    9.9066,         0,      3.7269
simulation,        10,       0.3,   0.30016,   0.30016,    10.184,    10.184,         0,      3.7048
simulation,        11,       0.4,   0.40027,   0.40028,    10.551,    10.551,         0,       3.685
simulation,        12,       0.5,   0.50013,   0.50013,    11.052,    11.052,         0,      3.6677
simulation,        13,       0.6,   0.60015,   0.60015,    11.753,    11.754,         0,      3.6518
simulation,        14,       0.7,   0.70005,   0.70005,    12.795,    12.795,         0,      3.6367
simulation,        15,       0.8,   0.80019,   0.80019,    14.598,    14.598,         0,      3.6227
simulation,        16,       0.9,   0.90013,   0.90014,    18.897,    18.898,         0,       3.609
