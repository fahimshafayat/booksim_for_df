wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
df_wc_seed = 66;
perm_seed = 66;
routing_function = UGAL_G;
seed = 96;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49995,   0.49995,    11.332,    11.332,         0,      3.7923
simulation,         2,      0.74,   0.73996,   0.73997,    14.074,    14.074,         0,      3.7456
simulation,         3,      0.86,   0.85993,   0.85992,    18.548,    18.548,         0,      3.7376
simulation,         4,      0.92,   0.91995,   0.91995,    24.826,    24.828,         0,      3.7371
simulation,         5,      0.95,   0.94999,      0.95,    32.281,    32.285,         0,       3.737
simulation,         6,      0.97,   0.97001,   0.97002,    43.455,    43.466,         0,      3.7365
simulation,         7,      0.98,      0.98,      0.98,    55.917,    55.938,         0,      3.7353
simulation,         8,       0.1,  0.099915,  0.099917,    10.405,    10.405,         0,      4.0871
simulation,         9,       0.2,   0.19994,   0.19994,    10.409,    10.409,         0,      3.9611
simulation,        10,       0.3,   0.29999,   0.29999,    10.561,    10.561,         0,       3.881
simulation,        11,       0.4,   0.39993,   0.39993,    10.859,    10.859,         0,       3.829
simulation,        12,       0.5,   0.49995,   0.49995,    11.332,    11.332,         0,      3.7923
simulation,        13,       0.6,   0.59997,   0.59996,    12.079,    12.079,         0,      3.7672
simulation,        14,       0.7,   0.69991,    0.6999,    13.317,    13.318,         0,      3.7502
simulation,        15,       0.8,   0.79995,   0.79995,    15.692,    15.692,         0,      3.7399
simulation,        16,       0.9,   0.89995,   0.89994,    22.006,    22.008,         0,      3.7371
