wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 64;
perm_seed = 64;
routing_function = UGAL_L_restricted_src_only;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49989,   0.49989,    11.935,    11.935,         0,      3.9583
simulation,         2,      0.74,   0.74004,   0.74004,    14.908,    14.908,         0,      3.8743
simulation,         3,      0.86,   0.85999,   0.85999,    19.759,    19.759,         0,      3.8349
simulation,         4,      0.92,   0.91992,   0.91991,    27.351,    27.353,         0,      3.8147
simulation,         5,      0.95,   0.94994,   0.94998,    40.758,    40.765,         0,      3.8118
simulation,         6,      0.97,   0.96995,   0.97001,    83.948,    83.985,         0,      3.7929
simulation,         7,      0.98,   0.98017,      0.98,    100.88,    100.92,         0,      3.7679
simulation,         8,       0.1,  0.099816,  0.099819,    10.407,    10.407,         0,      4.0873
simulation,         9,       0.2,   0.19986,   0.19987,    10.759,    10.759,         0,       4.108
simulation,        10,       0.3,   0.29984,   0.29985,    11.043,    11.043,         0,      4.0614
simulation,        11,       0.4,   0.39986,   0.39987,    11.406,    11.406,         0,      4.0065
simulation,        12,       0.5,   0.49989,   0.49989,    11.935,    11.935,         0,      3.9583
simulation,        13,       0.6,   0.60003,   0.60003,     12.75,     12.75,         0,      3.9186
simulation,        14,       0.7,   0.70003,   0.70003,    14.096,    14.096,         0,      3.8862
simulation,        15,       0.8,   0.80004,   0.80004,    16.653,    16.653,         0,      3.8552
simulation,        16,       0.9,   0.89993,   0.89992,     23.81,    23.813,         0,      3.8215
