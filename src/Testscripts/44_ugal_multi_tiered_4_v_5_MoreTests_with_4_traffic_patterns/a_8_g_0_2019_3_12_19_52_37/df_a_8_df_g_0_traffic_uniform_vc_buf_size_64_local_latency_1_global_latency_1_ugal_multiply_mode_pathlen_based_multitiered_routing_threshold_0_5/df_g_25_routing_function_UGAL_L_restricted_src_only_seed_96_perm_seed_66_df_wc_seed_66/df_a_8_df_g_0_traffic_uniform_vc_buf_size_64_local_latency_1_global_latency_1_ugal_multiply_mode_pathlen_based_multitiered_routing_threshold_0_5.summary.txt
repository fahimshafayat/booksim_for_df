wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
df_wc_seed = 66;
perm_seed = 66;
routing_function = UGAL_L_restricted_src_only;
seed = 96;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50008,   0.50007,    12.445,    12.445,         0,      4.0418
simulation,         2,      0.74,   0.74001,   0.74001,    23.746,    23.756,         0,      4.0055
simulation,         3,      0.86,   0.78241,   0.79956,    792.61,     355.6,         1,         0.0
simulation,         4,       0.8,   0.78338,   0.78499,    572.76,    235.07,         1,         0.0
simulation,         5,      0.77,   0.76999,   0.76999,    60.017,    60.066,         0,       4.089
simulation,         6,      0.78,   0.77953,    0.7796,    97.593,    84.119,         0,      4.1067
simulation,         7,      0.79,    0.7835,    0.7836,    505.77,    156.73,         1,         0.0
simulation,         8,       0.1,  0.099937,  0.099935,    10.614,    10.614,         0,       4.181
simulation,         9,       0.2,   0.19991,    0.1999,    10.995,    10.995,         0,      4.2011
simulation,        10,       0.3,   0.30004,   0.30004,    11.318,    11.318,         0,      4.1498
simulation,        11,       0.4,   0.40009,   0.40008,    11.748,    11.749,         0,        4.09
simulation,        12,       0.5,   0.50008,   0.50007,    12.445,    12.445,         0,      4.0418
simulation,        13,       0.6,    0.6001,   0.60009,    13.685,    13.686,         0,      4.0055
simulation,        14,       0.7,       0.7,   0.69999,    16.938,    16.939,         0,      3.9886
