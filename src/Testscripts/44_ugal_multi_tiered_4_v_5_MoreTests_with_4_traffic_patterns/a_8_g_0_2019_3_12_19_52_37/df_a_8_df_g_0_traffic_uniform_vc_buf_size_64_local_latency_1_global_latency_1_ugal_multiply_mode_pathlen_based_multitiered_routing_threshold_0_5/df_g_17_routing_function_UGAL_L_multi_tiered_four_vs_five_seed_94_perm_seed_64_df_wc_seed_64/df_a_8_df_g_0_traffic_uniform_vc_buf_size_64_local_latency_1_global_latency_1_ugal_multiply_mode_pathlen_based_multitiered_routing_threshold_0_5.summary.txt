wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 64;
perm_seed = 64;
routing_function = UGAL_L_multi_tiered_four_vs_five;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.4999,   0.49989,    11.745,    11.745,         0,      3.8895
simulation,         2,      0.74,   0.74005,   0.74004,    14.906,    14.907,         0,      3.8645
simulation,         3,      0.86,   0.85998,   0.85999,    19.811,    19.812,         0,      3.8339
simulation,         4,      0.92,    0.9199,   0.91991,    27.437,     27.44,         0,      3.8149
simulation,         5,      0.95,   0.94998,   0.94998,    41.317,    41.318,         0,      3.8131
simulation,         6,      0.97,   0.96989,   0.97001,    86.116,    86.106,         0,      3.7946
simulation,         7,      0.98,   0.97978,      0.98,    105.87,    105.97,         0,       3.764
simulation,         8,       0.1,  0.099815,  0.099819,    9.9959,     9.996,         0,      3.8928
simulation,         9,       0.2,   0.19987,   0.19987,    10.322,    10.322,         0,      3.9155
simulation,        10,       0.3,   0.29984,   0.29985,    10.669,    10.669,         0,      3.9088
simulation,        11,       0.4,   0.39987,   0.39987,    11.123,    11.123,         0,      3.8989
simulation,        12,       0.5,    0.4999,   0.49989,    11.745,    11.745,         0,      3.8895
simulation,        13,       0.6,   0.60002,   0.60003,    12.654,    12.655,         0,      3.8822
simulation,        14,       0.7,   0.70003,   0.70003,    14.072,    14.072,         0,      3.8707
simulation,        15,       0.8,   0.80004,   0.80004,    16.685,    16.686,         0,      3.8512
simulation,        16,       0.9,   0.89993,   0.89992,    23.834,    23.835,         0,       3.821
