wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 64;
perm_seed = 64;
routing_function = UGAL_G_multi_tiered_four_vs_five;
seed = 94;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50013,   0.50013,    11.275,    11.275,         0,      3.7748
simulation,         2,      0.74,    0.7402,   0.74021,    14.176,    14.176,         0,      3.7599
simulation,         3,      0.86,   0.86016,   0.86017,    22.823,    22.824,         0,      3.7911
simulation,         4,      0.92,   0.88843,   0.88891,    629.89,     171.0,         1,         0.0
simulation,         5,      0.89,   0.89015,   0.89014,    36.742,    36.744,         0,      3.8128
simulation,         6,       0.9,   0.89272,   0.89299,    458.59,    109.64,         0,      3.8286
simulation,         7,      0.91,   0.89102,   0.89086,    533.16,    144.09,         1,         0.0
simulation,         8,       0.1,   0.10022,   0.10022,    9.8485,    9.8486,         0,      3.8308
simulation,         9,       0.2,   0.20005,   0.20005,    10.067,    10.067,         0,       3.816
simulation,        10,       0.3,   0.30016,   0.30016,    10.356,    10.356,         0,      3.8007
simulation,        11,       0.4,   0.40028,   0.40028,    10.747,    10.747,         0,       3.787
simulation,        12,       0.5,   0.50013,   0.50013,    11.275,    11.275,         0,      3.7748
simulation,        13,       0.6,   0.60015,   0.60015,     12.04,    12.041,         0,      3.7646
simulation,        14,       0.7,   0.70005,   0.70005,    13.324,    13.324,         0,       3.759
simulation,        15,       0.8,   0.80019,   0.80019,    16.399,      16.4,         0,      3.7687
simulation,        16,       0.9,   0.89272,   0.89299,    458.59,    109.64,         0,      3.8286
