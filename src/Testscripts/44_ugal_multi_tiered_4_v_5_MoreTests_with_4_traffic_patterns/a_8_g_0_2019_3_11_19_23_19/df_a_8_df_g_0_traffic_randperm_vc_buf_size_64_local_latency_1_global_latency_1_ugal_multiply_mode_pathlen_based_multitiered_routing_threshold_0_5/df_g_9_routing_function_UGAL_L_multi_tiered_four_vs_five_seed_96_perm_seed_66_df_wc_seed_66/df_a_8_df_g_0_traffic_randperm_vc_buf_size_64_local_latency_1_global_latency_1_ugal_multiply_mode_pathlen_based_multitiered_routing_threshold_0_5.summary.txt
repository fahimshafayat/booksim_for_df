wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 66;
perm_seed = 66;
routing_function = UGAL_L_multi_tiered_four_vs_five;
seed = 96;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50016,   0.50016,    11.423,    11.423,         0,      3.8281
simulation,         2,      0.74,   0.73983,   0.73981,    24.298,    24.323,         0,      3.8738
simulation,         3,      0.86,   0.81294,   0.81249,    1121.4,    270.63,         1,         0.0
simulation,         4,       0.8,   0.79704,   0.79702,    433.47,    63.526,         0,      3.9352
simulation,         5,      0.83,   0.80779,   0.80879,    513.59,    186.18,         1,         0.0
simulation,         6,      0.81,   0.80313,   0.80319,    415.12,    104.15,         1,         0.0
simulation,         7,       0.1,  0.099968,   0.09997,    9.8002,    9.8003,         0,      3.8115
simulation,         8,       0.2,   0.19992,   0.19992,    10.104,    10.104,         0,      3.8393
simulation,         9,       0.3,   0.30008,   0.30008,    10.416,    10.416,         0,      3.8361
simulation,        10,       0.4,   0.40022,   0.40022,    10.826,    10.826,         0,      3.8304
simulation,        11,       0.5,   0.50016,   0.50016,    11.423,    11.423,         0,      3.8281
simulation,        12,       0.6,   0.59994,   0.59994,    12.618,    12.618,         0,       3.832
simulation,        13,       0.7,   0.69979,    0.6998,    18.614,    18.623,         0,      3.8517
simulation,        14,       0.8,   0.79704,   0.79702,    433.47,    63.526,         0,      3.9352
