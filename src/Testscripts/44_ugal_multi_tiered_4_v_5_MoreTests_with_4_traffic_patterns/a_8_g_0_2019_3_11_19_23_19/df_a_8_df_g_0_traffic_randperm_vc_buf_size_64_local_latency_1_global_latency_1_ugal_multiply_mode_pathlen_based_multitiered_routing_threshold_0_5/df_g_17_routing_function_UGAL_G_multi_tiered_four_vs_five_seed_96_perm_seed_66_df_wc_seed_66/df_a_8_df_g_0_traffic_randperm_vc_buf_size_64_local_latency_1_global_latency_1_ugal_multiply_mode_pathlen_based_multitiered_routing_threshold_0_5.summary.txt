wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
df_wc_seed = 66;
perm_seed = 66;
routing_function = UGAL_G_multi_tiered_four_vs_five;
seed = 96;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50002,   0.50003,    11.841,    11.841,         0,      3.8966
simulation,         2,      0.74,   0.73987,   0.73987,    19.549,    19.549,         0,      3.9657
simulation,         3,      0.86,   0.78112,   0.79333,    647.34,    270.79,         1,         0.0
simulation,         4,       0.8,   0.79661,   0.79663,    462.25,    84.665,         0,      4.0845
simulation,         5,      0.83,   0.78569,   0.78625,    928.12,    226.82,         1,         0.0
simulation,         6,      0.81,   0.78863,   0.78876,    550.61,    162.14,         1,         0.0
simulation,         7,       0.1,  0.099969,   0.09997,    10.029,    10.029,         0,      3.9152
simulation,         8,       0.2,       0.2,       0.2,     10.29,     10.29,         0,      3.9101
simulation,         9,       0.3,   0.30003,   0.30003,    10.646,    10.646,         0,      3.9041
simulation,        10,       0.4,   0.40007,   0.40006,    11.136,    11.136,         0,      3.8996
simulation,        11,       0.5,   0.50002,   0.50003,    11.841,    11.841,         0,      3.8966
simulation,        12,       0.6,   0.60001,   0.60002,    13.071,    13.072,         0,      3.9017
simulation,        13,       0.7,   0.69992,   0.69993,     16.22,    16.221,         0,      3.9343
simulation,        14,       0.8,   0.79661,   0.79663,    462.25,    84.665,         0,      4.0845
