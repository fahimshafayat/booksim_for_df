wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = UGAL_L;
seed = 94;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.34342,   0.38126,    1316.6,    844.93,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24998,    39.168,    39.246,         0,      5.9915
simulation,         3,      0.37,   0.33673,   0.34758,    738.22,    467.26,         1,         0.0
simulation,         4,      0.31,   0.29948,   0.29979,    674.49,    453.35,         1,         0.0
simulation,         5,      0.28,   0.27831,   0.27934,    383.53,    334.38,         0,      6.0437
simulation,         6,      0.29,   0.28558,   0.28689,    503.28,     419.7,         1,         0.0
simulation,         7,      0.05,  0.050003,  0.050002,    12.737,    12.737,         0,      4.9956
simulation,         8,       0.1,  0.099995,  0.099995,    25.974,    25.994,         0,      5.3435
simulation,         9,      0.15,   0.14998,   0.14998,    37.391,    37.429,         0,      5.6647
simulation,        10,       0.2,   0.20003,   0.20002,    37.255,    37.306,         0,       5.867
simulation,        11,      0.25,   0.24998,   0.24998,    39.168,    39.246,         0,      5.9915
