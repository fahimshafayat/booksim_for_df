wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = UGAL_L;
seed = 94;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.33919,   0.37706,    1343.3,    848.07,         1,         0.0
simulation,         2,      0.25,   0.24997,   0.24998,    38.095,    38.166,         0,      6.0345
simulation,         3,      0.37,   0.33171,   0.34362,    842.26,    524.25,         1,         0.0
simulation,         4,      0.31,   0.29773,   0.29827,    832.24,    542.56,         1,         0.0
simulation,         5,      0.28,   0.27803,   0.27927,     439.2,    385.02,         0,      6.0817
simulation,         6,      0.29,   0.28478,   0.28672,    479.98,    458.37,         1,         0.0
simulation,         7,      0.05,  0.050004,  0.050002,    12.864,    12.864,         0,       5.029
simulation,         8,       0.1,  0.099998,  0.099995,    28.568,    28.592,         0,      5.4151
simulation,         9,      0.15,   0.14998,   0.14998,    36.291,    36.329,         0,      5.7349
simulation,        10,       0.2,   0.20002,   0.20002,    36.106,    36.156,         0,      5.9203
simulation,        11,      0.25,   0.24997,   0.24998,    38.095,    38.166,         0,      6.0345
