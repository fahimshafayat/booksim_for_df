wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_G_multi_tiered_four_vs_five;
seed = 94;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49989,   0.49989,    52.538,    52.558,         0,      4.7714
simulation,         2,      0.74,   0.46076,   0.50208,    1633.3,    747.18,         1,         0.0
simulation,         3,      0.62,   0.49603,   0.53003,    785.12,    517.03,         1,         0.0
simulation,         4,      0.56,   0.51661,   0.52634,    575.51,    318.13,         1,         0.0
simulation,         5,      0.53,   0.52574,   0.52577,    428.86,     140.8,         1,         0.0
simulation,         6,      0.51,   0.50991,   0.50992,    56.637,     56.66,         0,      4.8201
simulation,         7,      0.52,   0.51994,   0.51994,    64.211,    64.237,         0,      4.8868
simulation,         8,       0.1,  0.099815,  0.099819,    11.094,    11.094,         0,      4.2159
simulation,         9,       0.2,   0.19986,   0.19987,    12.124,    12.124,         0,       4.292
simulation,        10,       0.3,   0.29984,   0.29985,    13.658,    13.658,         0,      4.3672
simulation,        11,       0.4,   0.39987,   0.39987,    18.119,     18.12,         0,      4.4584
simulation,        12,       0.5,   0.49989,   0.49989,    52.538,    52.558,         0,      4.7714
