wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 17;
routing_function = UGAL_G;
seed = 94;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49989,   0.49989,    38.153,    38.156,         0,       6.077
simulation,         2,      0.74,   0.42088,   0.46253,    1720.7,    824.61,         1,         0.0
simulation,         3,      0.62,   0.45145,   0.49037,    1045.3,    649.52,         1,         0.0
simulation,         4,      0.56,   0.49006,   0.51356,    438.46,    342.98,         1,         0.0
simulation,         5,      0.53,   0.50818,   0.50804,    822.64,    240.65,         1,         0.0
simulation,         6,      0.51,    0.5099,   0.50992,    43.675,    43.678,         0,      6.0851
simulation,         7,      0.52,   0.51994,   0.51994,    53.596,    53.597,         0,      6.0938
simulation,         8,       0.1,  0.099814,  0.099819,    13.627,    13.627,         0,      5.3633
simulation,         9,       0.2,   0.19986,   0.19987,     15.43,     15.43,         0,      5.5626
simulation,        10,       0.3,   0.29984,   0.29985,    18.265,    18.265,         0,      5.8281
simulation,        11,       0.4,   0.39987,   0.39987,    22.535,    22.536,         0,      5.9777
simulation,        12,       0.5,   0.49989,   0.49989,    38.153,    38.156,         0,       6.077
