wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = UGAL_G_restricted_src_only;
seed = 94;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.41412,   0.44012,    600.19,    454.24,         1,         0.0
simulation,         2,      0.25,   0.24998,   0.24998,    14.817,    14.817,         0,      5.1369
simulation,         3,      0.37,   0.36992,   0.36992,    18.496,    18.496,         0,      5.2443
simulation,         4,      0.43,   0.41089,   0.41008,    723.49,    254.43,         1,         0.0
simulation,         5,       0.4,    0.3999,   0.39991,    22.602,    22.605,         0,      5.2658
simulation,         6,      0.41,   0.40942,   0.40944,    119.77,    63.389,         0,      5.2708
simulation,         7,      0.42,   0.41072,    0.4102,    456.05,    142.28,         1,         0.0
simulation,         8,       0.1,  0.099997,  0.099995,    12.857,    12.857,         0,      4.9625
simulation,         9,       0.2,   0.20003,   0.20002,    14.014,    14.014,         0,       5.077
simulation,        10,       0.3,   0.29995,   0.29995,    15.859,    15.859,         0,        5.19
simulation,        11,       0.4,    0.3999,   0.39991,    22.602,    22.605,         0,      5.2658
