wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = UGAL_L;
seed = 94;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.38955,    0.4288,    963.83,    703.96,         1,         0.0
simulation,         2,      0.25,   0.25004,   0.25004,    32.761,    32.819,         0,      6.1392
simulation,         3,      0.37,   0.36816,   0.36963,    346.69,    330.85,         0,      6.2577
simulation,         4,      0.43,   0.38068,   0.40026,    999.38,    702.12,         1,         0.0
simulation,         5,       0.4,   0.37195,   0.37546,    1464.5,    999.18,         1,         0.0
simulation,         6,      0.38,   0.36983,   0.37669,    665.35,    631.53,         1,         0.0
simulation,         7,       0.1,   0.10001,   0.10001,    35.407,    35.437,         0,      5.6226
simulation,         8,       0.2,   0.20004,   0.20004,    32.413,     32.46,         0,      6.0492
simulation,         9,       0.3,   0.30002,   0.30001,    34.502,    34.579,         0,      6.1999
