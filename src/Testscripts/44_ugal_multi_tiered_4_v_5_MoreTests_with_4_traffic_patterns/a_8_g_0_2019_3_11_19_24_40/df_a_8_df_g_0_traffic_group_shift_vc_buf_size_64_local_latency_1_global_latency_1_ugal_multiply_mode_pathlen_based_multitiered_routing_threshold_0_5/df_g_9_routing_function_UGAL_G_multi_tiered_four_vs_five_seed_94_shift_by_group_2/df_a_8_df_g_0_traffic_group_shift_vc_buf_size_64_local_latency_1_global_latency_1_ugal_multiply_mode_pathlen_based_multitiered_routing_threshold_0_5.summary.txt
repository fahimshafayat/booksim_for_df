wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_G_multi_tiered_four_vs_five;
seed = 94;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50013,    20.902,    20.902,         0,      4.4212
simulation,         2,      0.74,   0.53364,   0.57615,    1182.3,    617.42,         1,         0.0
simulation,         3,      0.62,   0.55351,   0.57808,     451.6,    341.86,         1,         0.0
simulation,         4,      0.56,   0.56007,   0.56008,    76.588,      76.6,         0,      4.5791
simulation,         5,      0.59,   0.55581,   0.56564,    502.96,    293.02,         1,         0.0
simulation,         6,      0.57,   0.55839,   0.55879,    475.26,    240.44,         1,         0.0
simulation,         7,       0.1,   0.10022,   0.10022,    10.801,    10.801,         0,      4.2011
simulation,         8,       0.2,   0.20005,   0.20005,    11.586,    11.587,         0,      4.2397
simulation,         9,       0.3,   0.30016,   0.30016,    12.642,    12.643,         0,      4.2726
simulation,        10,       0.4,   0.40028,   0.40028,    14.631,    14.631,         0,      4.3357
simulation,        11,       0.5,   0.50014,   0.50013,    20.902,    20.902,         0,      4.4212
