wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_L;
seed = 96;
shift_by_group = 3;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.35098,   0.39956,    1336.8,    974.31,         1,         0.0
simulation,         2,      0.25,   0.24991,   0.24992,    52.084,    52.125,         0,      5.1215
simulation,         3,      0.37,   0.35523,   0.35611,    1001.7,    704.65,         1,         0.0
simulation,         4,      0.31,   0.31017,   0.31016,    62.233,    62.325,         0,      5.3775
simulation,         5,      0.34,   0.33966,   0.34008,    156.43,    156.63,         0,        5.47
simulation,         6,      0.35,   0.34598,   0.34855,    515.16,    519.65,         1,         0.0
simulation,         7,       0.1,  0.099967,   0.09997,    12.029,    12.029,         0,      4.7368
simulation,         8,       0.2,   0.19992,   0.19992,    18.406,    18.409,         0,      4.8284
simulation,         9,       0.3,   0.30007,   0.30008,    59.657,    59.739,         0,       5.341
