wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_G;
seed = 96;
shift_by_group = 5;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50017,   0.50016,    28.848,    28.849,         0,      5.6765
simulation,         2,      0.74,   0.47063,    0.5146,    1584.8,     774.5,         1,         0.0
simulation,         3,      0.62,   0.50613,   0.53883,    720.21,    487.04,         1,         0.0
simulation,         4,      0.56,   0.56005,   0.56003,    61.693,    61.691,         0,      5.7589
simulation,         5,      0.59,   0.51944,   0.54445,     459.8,    347.75,         1,         0.0
simulation,         6,      0.57,   0.53003,   0.53947,    563.72,    301.35,         1,         0.0
simulation,         7,       0.1,  0.099966,   0.09997,    12.453,    12.453,         0,      4.9506
simulation,         8,       0.2,   0.19992,   0.19992,    13.669,    13.669,         0,      5.0338
simulation,         9,       0.3,   0.30008,   0.30008,    16.213,    16.213,         0,      5.2335
simulation,        10,       0.4,   0.40022,   0.40022,    20.439,    20.438,         0,      5.5033
simulation,        11,       0.5,   0.50017,   0.50016,    28.848,    28.849,         0,      5.6765
