wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 9;
routing_function = UGAL_G_multi_tiered_four_vs_five;
seed = 96;
shift_by_group = 5;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50014,   0.50016,    24.826,    24.826,         0,      4.4351
simulation,         2,      0.74,   0.53571,   0.57515,    1166.0,    604.79,         1,         0.0
simulation,         3,      0.62,   0.55272,   0.57826,    451.17,    339.97,         1,         0.0
simulation,         4,      0.56,   0.56006,   0.56003,     78.28,     78.29,         0,      4.6287
simulation,         5,      0.59,   0.55479,   0.56356,    497.01,     279.9,         1,         0.0
simulation,         6,      0.57,   0.55805,     0.558,    496.75,    226.96,         1,         0.0
simulation,         7,       0.1,  0.099965,   0.09997,    10.782,    10.782,         0,      4.1869
simulation,         8,       0.2,   0.19992,   0.19992,    11.606,    11.606,         0,      4.2276
simulation,         9,       0.3,   0.30008,   0.30008,    12.729,     12.73,         0,      4.2656
simulation,        10,       0.4,   0.40022,   0.40022,     14.92,     14.92,         0,      4.3357
simulation,        11,       0.5,   0.50014,   0.50016,    24.826,    24.826,         0,      4.4351
