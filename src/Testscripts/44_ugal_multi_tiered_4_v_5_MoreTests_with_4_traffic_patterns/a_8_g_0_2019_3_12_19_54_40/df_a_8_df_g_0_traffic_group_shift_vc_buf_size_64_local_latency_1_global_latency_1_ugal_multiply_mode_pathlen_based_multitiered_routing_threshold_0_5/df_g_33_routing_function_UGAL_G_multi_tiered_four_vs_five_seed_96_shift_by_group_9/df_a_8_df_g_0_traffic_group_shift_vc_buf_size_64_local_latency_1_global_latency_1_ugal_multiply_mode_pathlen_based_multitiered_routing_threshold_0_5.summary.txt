wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = UGAL_G_multi_tiered_four_vs_five;
seed = 96;
shift_by_group = 9;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46644,   0.47408,     488.0,     262.8,         1,         0.0
simulation,         2,      0.25,   0.25001,      0.25,    13.863,    13.863,         0,       4.589
simulation,         3,      0.37,   0.36995,   0.36993,    45.838,    44.882,         0,       4.858
simulation,         4,      0.43,   0.42985,   0.42985,    59.981,    52.148,         0,      5.1549
simulation,         5,      0.46,   0.45744,   0.45747,    475.11,     94.42,         0,      5.3063
simulation,         6,      0.48,   0.46602,   0.46613,    530.38,    183.75,         1,         0.0
simulation,         7,      0.47,   0.46419,    0.4642,    530.05,    122.27,         1,         0.0
simulation,         8,       0.1,  0.099915,  0.099917,    11.841,    11.841,         0,      4.4351
simulation,         9,       0.2,   0.19994,   0.19994,    12.957,    12.957,         0,      4.5407
simulation,        10,       0.3,   0.29999,   0.29999,    21.846,    21.858,         0,      4.6443
simulation,        11,       0.4,   0.39973,   0.39974,    70.517,     49.13,         0,      5.0059
