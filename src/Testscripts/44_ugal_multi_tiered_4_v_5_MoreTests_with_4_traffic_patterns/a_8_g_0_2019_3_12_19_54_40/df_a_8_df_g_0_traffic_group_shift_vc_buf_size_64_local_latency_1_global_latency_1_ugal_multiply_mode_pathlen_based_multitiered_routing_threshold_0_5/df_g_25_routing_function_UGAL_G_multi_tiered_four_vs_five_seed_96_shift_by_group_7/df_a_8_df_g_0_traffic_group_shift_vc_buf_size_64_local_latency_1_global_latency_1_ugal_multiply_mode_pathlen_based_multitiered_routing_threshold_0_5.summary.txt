wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 25;
routing_function = UGAL_G_multi_tiered_four_vs_five;
seed = 96;
shift_by_group = 7;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.38924,   0.42149,    793.79,    575.74,         1,         0.0
simulation,         2,      0.25,      0.25,      0.25,     13.93,     13.93,         0,      4.6216
simulation,         3,      0.37,   0.37006,   0.37006,    38.247,     38.26,         0,      4.8512
simulation,         4,      0.43,   0.39716,   0.40547,    544.16,    327.06,         1,         0.0
simulation,         5,       0.4,   0.39804,   0.39794,    376.29,    95.079,         0,      5.2205
simulation,         6,      0.41,    0.3972,    0.3971,     608.9,    231.78,         1,         0.0
simulation,         7,       0.1,  0.099937,  0.099935,    11.822,    11.822,         0,      4.4553
simulation,         8,       0.2,   0.19991,    0.1999,    13.008,    13.008,         0,      4.5673
simulation,         9,       0.3,   0.30004,   0.30004,    15.577,    15.577,         0,      4.6785
simulation,        10,       0.4,   0.39804,   0.39794,    376.29,    95.079,         0,      5.2205
