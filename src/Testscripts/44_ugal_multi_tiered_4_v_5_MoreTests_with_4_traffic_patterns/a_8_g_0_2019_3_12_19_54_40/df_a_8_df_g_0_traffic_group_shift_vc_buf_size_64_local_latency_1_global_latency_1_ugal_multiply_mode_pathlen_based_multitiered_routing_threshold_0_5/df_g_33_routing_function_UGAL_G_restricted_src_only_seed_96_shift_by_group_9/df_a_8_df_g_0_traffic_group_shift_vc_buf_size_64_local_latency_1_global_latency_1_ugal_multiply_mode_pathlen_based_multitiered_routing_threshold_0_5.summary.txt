wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
local_latency = 1;
multitiered_routing_threshold_0 = 5;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 64;
df_g = 33;
routing_function = UGAL_G_restricted_src_only;
seed = 96;
shift_by_group = 9;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.46772,   0.46774,    1090.8,    302.42,         1,         0.0
simulation,         2,      0.25,   0.25001,      0.25,    14.938,    14.938,         0,      5.2706
simulation,         3,      0.37,   0.36995,   0.36994,    17.635,    17.636,         0,        5.35
simulation,         4,      0.43,   0.42997,   0.42997,    21.192,    21.193,         0,      5.3769
simulation,         5,      0.46,   0.45995,   0.45995,    26.797,    26.798,         0,      5.3905
simulation,         6,      0.48,   0.47422,   0.47419,    521.81,    129.04,         1,         0.0
simulation,         7,      0.47,   0.46997,   0.46996,    31.854,    31.858,         0,      5.3962
simulation,         8,       0.1,  0.099917,  0.099917,    13.223,    13.223,         0,       5.095
simulation,         9,       0.2,   0.19994,   0.19994,    14.255,    14.256,         0,      5.2152
simulation,        10,       0.3,   0.29999,   0.29999,    15.818,    15.818,         0,      5.3111
simulation,        11,       0.4,   0.39993,   0.39993,    18.935,    18.936,         0,      5.3636
