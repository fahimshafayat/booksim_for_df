vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 25;
perm_seed = 76;
routing_function = UGAL_L_two_hop;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49819,    0.4982,    388.12,    93.176,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.55802,   0.59515,     822.9,    516.15,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.56148,   0.57203,    647.44,    323.49,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.54437,   0.54443,    509.51,    191.42,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.52336,    0.5234,    505.54,    134.35,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.50698,   0.50697,    346.99,    103.19,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.45006,   0.45008,    57.772,    57.812,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.40009,   0.40009,    48.141,    48.159,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,    0.3501,    0.3501,    43.191,      43.2,         0,  0,  0,  ,  ,  ,  
