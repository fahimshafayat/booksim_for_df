vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 33;
perm_seed = 72;
routing_function = UGAL_L_restricted;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49894,   0.49895,    293.06,    77.273,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.56081,   0.59899,    846.85,    530.97,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.55878,   0.57122,    688.15,    361.24,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55096,   0.55091,    457.79,    167.83,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.52715,   0.52715,    298.56,    101.38,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.50862,   0.50862,    370.45,    84.349,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.52,   0.51802,   0.51802,    487.85,    93.262,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.45,   0.44996,   0.44995,    59.045,    55.076,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.4,   0.40005,   0.40005,    47.004,    47.018,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.35,   0.35007,   0.35006,    43.116,    43.123,         0,  0,  0,  ,  ,  ,  
