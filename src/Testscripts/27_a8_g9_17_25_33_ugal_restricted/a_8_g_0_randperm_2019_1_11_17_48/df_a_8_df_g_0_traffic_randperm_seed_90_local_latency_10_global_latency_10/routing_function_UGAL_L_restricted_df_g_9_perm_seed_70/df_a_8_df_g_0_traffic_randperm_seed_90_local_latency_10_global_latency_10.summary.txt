vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 9;
perm_seed = 70;
routing_function = UGAL_L_restricted;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4998,   0.49978,    35.401,    35.404,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,    0.7403,   0.74028,    50.555,    50.575,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.82811,   0.82815,    747.47,    189.14,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.79491,    0.7949,    378.71,    87.162,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.76915,   0.76914,     127.1,    62.297,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.78,   0.77858,   0.77859,    190.04,     68.37,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.79,   0.78754,   0.78753,    290.95,    79.799,         0,  0,  0,  ,  ,  ,  
simulation,         8,      0.75,   0.75029,   0.75028,    52.253,    52.279,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.7,   0.70019,   0.70017,    44.999,    45.014,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.65,   0.65005,   0.65004,    40.503,    40.512,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.6,   0.59997,   0.59996,    37.875,    37.879,         0,  0,  0,  ,  ,  ,  
