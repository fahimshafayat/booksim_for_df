vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = randperm;
df_g = 9;
perm_seed = 76;
routing_function = UGAL_L_restricted;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4998,   0.49978,    36.233,    36.236,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.74025,   0.74028,    65.547,    65.592,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.79505,    0.8055,    601.43,    240.76,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.77969,   0.77988,    559.56,    172.34,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.76495,   0.76494,    434.44,    99.718,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.75,   0.74899,   0.74903,     151.9,    80.583,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.76,   0.75737,   0.75744,    282.31,    89.981,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.7,   0.70022,   0.70017,    51.065,     51.09,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.65,   0.65007,   0.65004,    43.529,    43.539,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.6,   0.59999,   0.59996,    39.818,    39.824,         0,  0,  0,  ,  ,  ,  
