vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 25;
routing_function = UGAL_L_restricted;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.31556,   0.35495,    1262.5,    856.98,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25007,   0.25008,    80.131,    80.246,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.32491,   0.33494,    707.08,    438.46,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30908,   0.30909,    323.41,    108.38,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,   0.32058,   0.32187,    677.12,     290.3,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.32,   0.31603,     0.316,    491.83,    179.02,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.3,   0.30001,   0.30006,    87.724,    87.848,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.2,   0.20009,   0.20009,    76.577,    76.642,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.15,   0.15006,   0.15007,    71.779,    71.815,         0,  0,  0,  ,  ,  ,  
