vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 25;
routing_function = UGAL_L_restricted;
shift_by_group = 19;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.34823,   0.38428,    965.89,    678.86,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25007,   0.25008,    79.975,    80.086,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.34681,   0.34736,    1024.3,    359.78,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31007,   0.31007,    84.355,    84.542,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,   0.33705,   0.33717,    424.09,    140.36,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.32,   0.32007,   0.32007,    85.366,    85.561,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.33,   0.33007,   0.33011,    90.069,    90.369,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.30005,   0.30006,    83.478,    83.643,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20009,   0.20009,    76.471,     76.54,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.15,   0.15006,   0.15007,    71.723,    71.754,         0,  0,  0,  ,  ,  ,  
