vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 33;
routing_function = UGAL_L_two_hop;
shift_by_group = 8;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.35886,   0.39332,    885.11,     632.8,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25006,    75.122,    75.225,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.35432,   0.35515,    901.96,    336.16,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31007,   0.31006,    77.564,    77.707,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.34,   0.33731,   0.33765,    498.27,    208.45,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.32,   0.31995,   0.32002,    99.562,    95.884,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.33,   0.32871,   0.32873,    350.29,    132.42,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.30005,   0.30005,    77.001,    77.142,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20009,   0.20009,    73.312,    73.383,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.15,   0.15012,   0.15011,    70.865,    70.903,         0,  0,  0,  ,  ,  ,  
