vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 17;
routing_function = UGAL_L_two_hop;
shift_by_group = 13;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.32945,   0.37039,    1317.6,    904.45,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25007,   0.25005,    101.74,    101.95,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,    0.3247,    0.3477,    480.38,    434.93,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.29978,   0.30035,    604.47,    346.67,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27691,   0.27693,    478.67,    225.84,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,   0.25945,   0.25972,     237.8,    200.54,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.27,   0.26808,   0.26815,    403.97,    224.66,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.2,   0.20007,   0.20007,    91.183,    91.255,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.15,   0.15002,   0.15003,    80.198,    80.221,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.1,  0.099943,  0.099944,    48.089,    48.094,         0,  0,  0,  ,  ,  ,  
