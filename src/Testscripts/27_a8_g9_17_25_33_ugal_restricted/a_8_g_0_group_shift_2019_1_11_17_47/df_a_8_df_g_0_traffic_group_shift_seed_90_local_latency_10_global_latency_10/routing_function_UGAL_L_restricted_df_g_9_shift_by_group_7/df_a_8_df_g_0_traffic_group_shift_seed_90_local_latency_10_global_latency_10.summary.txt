vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
local_latency = 10;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
df_g = 9;
routing_function = UGAL_L_restricted;
shift_by_group = 7;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.45901,   0.47105,    665.72,    413.45,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24997,   0.24997,    84.253,    84.292,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.36994,   0.36992,    111.56,    111.78,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,   0.42995,   0.42991,    126.97,    127.45,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.44407,   0.44427,    635.41,    299.84,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.43995,   0.43992,    130.94,    131.44,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.45,   0.44471,   0.44484,    531.63,    188.91,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.39986,   0.39984,    118.47,    118.79,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.34991,   0.34989,     107.4,    107.58,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.29991,   0.29991,    96.989,    97.085,         0,  0,  0,  ,  ,  ,  
