vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 1000;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
topology = dragonflyfull;
traffic = uniform;
routing_function = min;
seed = 60;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01, 0.0099938, 0.0099942,    9.5862,    9.5866,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
simulation,         2,      0.99,   0.96089,   0.97447,    210.46,    176.04,         0,  0,  0,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 ,
