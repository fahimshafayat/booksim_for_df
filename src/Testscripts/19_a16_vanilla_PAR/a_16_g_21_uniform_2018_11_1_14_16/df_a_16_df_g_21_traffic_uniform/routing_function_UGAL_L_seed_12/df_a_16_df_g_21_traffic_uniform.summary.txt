vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 1000;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 21;
topology = dragonflyfull;
traffic = uniform;
routing_function = UGAL_L;
seed = 12;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.01,  0.009997,  0.010003,    9.8863,    9.8878,         0,  152785,  8905,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 6977 , |3-> 122 , |4-> 3009 , |5-> 30156 , |6-> 112521 ,,  |1-> 0 , |2-> 424 , |3-> 6 , |4-> 180 , |5-> 1785 , |6-> 6510 ,
simulation,         2,      0.99,   0.93859,   0.95959,    345.45,    275.05,         0,  28794092,  2110552,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |4-> 0 , |5-> 0 , |6-> 0 ,,  |1-> 0 , |2-> 1321918 , |3-> 16593 , |4-> 448244 , |5-> 5193904 , |6-> 21813433 ,,  |1-> 0 , |2-> 65348 , |3-> 9376 , |4-> 154205 , |5-> 900319 , |6-> 981304 ,
