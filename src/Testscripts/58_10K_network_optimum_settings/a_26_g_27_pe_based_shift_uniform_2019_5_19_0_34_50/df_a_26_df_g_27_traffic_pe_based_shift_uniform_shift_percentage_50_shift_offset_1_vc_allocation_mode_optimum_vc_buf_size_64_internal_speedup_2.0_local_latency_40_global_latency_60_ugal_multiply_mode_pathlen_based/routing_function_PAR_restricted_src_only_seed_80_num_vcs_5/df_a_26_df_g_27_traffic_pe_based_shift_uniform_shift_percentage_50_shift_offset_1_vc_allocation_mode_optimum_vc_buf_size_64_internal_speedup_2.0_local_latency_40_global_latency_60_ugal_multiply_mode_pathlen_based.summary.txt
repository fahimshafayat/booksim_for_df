wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 26;
df_arrangement = absolute_improved;
df_g = 27;
global_latency = 60;
internal_speedup = 2.0;
local_latency = 40;
shift_offset = 1;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 64;
num_vcs = 5;
routing_function = PAR_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.27329,   0.30774,    1671.4,    936.29,         1,         0.0
simulation,         2,      0.25,   0.23857,   0.23922,    715.77,    424.44,         1,         0.0
simulation,         3,      0.13,   0.13001,   0.13001,    193.09,    193.19,         0,      4.7673
simulation,         4,      0.19,   0.19002,   0.19002,     208.2,    208.41,         0,      4.7506
simulation,         5,      0.22,   0.22001,      0.22,    228.72,    227.19,         0,      4.7427
simulation,         6,      0.23,   0.22901,   0.22907,    395.89,    269.81,         1,         0.0
