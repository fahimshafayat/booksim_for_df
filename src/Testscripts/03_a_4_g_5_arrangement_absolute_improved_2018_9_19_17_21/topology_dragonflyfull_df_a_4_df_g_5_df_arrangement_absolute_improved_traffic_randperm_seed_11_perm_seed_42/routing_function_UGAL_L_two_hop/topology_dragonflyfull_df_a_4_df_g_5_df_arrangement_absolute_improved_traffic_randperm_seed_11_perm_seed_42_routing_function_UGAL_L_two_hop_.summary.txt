vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
internal_speedup = 2.0;
warmup_periods = 3;
sim_count = 1;
sample_period = 1000;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 4;
df_arrangement = absolute_improved;
df_g = 5;
perm_seed = 42;
seed = 11;
topology = dragonflyfull;
traffic = randperm;
routing_function = UGAL_L_two_hop;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?
simulation,         1,      0.01, 0.0098063,  0.009775,    12.041,         0
simulation,         2,      0.99,   0.74375,   0.77587,    475.34,         1
simulation,         3,       0.5,   0.50069,   0.50092,    14.757,         0
simulation,         4,      0.74,   0.73257,   0.73871,     121.7,         0
simulation,         5,      0.86,   0.73956,   0.75878,    428.64,         1
simulation,         6,       0.8,   0.74095,   0.74651,    300.41,         1
simulation,         7,      0.77,   0.73947,   0.75206,    259.92,         0
simulation,         8,      0.78,   0.73891,   0.74846,    257.19,         1
