BEGIN Configuration File: dragonflyfull_0
// $Id$

// Copyright (c) 2007-2015, Trustees of The Leland Stanford Junior University
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.
// Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Dragonfly
//

//dragonfly specific parameters
topology = dragonflyfull;
df_a = 16;
df_g = 129;
df_arrangement = absolute_improved;
//end dragonfly specific paramters

vc_buf_size = 64;


wait_for_tail_credit = 0;

//
// Router architecture
//
vc_allocator = separable_input_first; 
sw_allocator = separable_input_first;
alloc_iters  = 1;

credit_delay   = 2;
routing_delay  = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;

input_speedup     = 1;
output_speedup    = 1;
internal_speedup  = 2.5;


warmup_periods = 3;
sim_count          = 1;

sample_period  = 1000;  



routing_function = UGAL_L; //min/vlb//UGAL_L
num_vcs     = 7;

priority = none;
traffic       = randperm;
perm_seed = 42;

injection_rate = 0.55;
packet_size = 1;
injection_rate_uses_flits=1;


//watch_file = watch;
//watch_out = watch.out;


END Configuration File: dragonflyfull_0
topology: dragonflyfull
inside _RegisterRoutingFunctions() ...
done with _RegisterRoutingFunctions() ...
Routing function registered ...
inside DragonFlyFull() constructor ...
_a: 16  _g: 129  _h: 8  _p: 8  _arrangement: absolute_improved
inside _AllocateArrays() ... 
done with _AllocateArrays() ... 
_BuilfGraphForGlobal() starts ...
_Computesize starts ...
_nodes: 16512
_size: 2064
_channels: 47472
_ComputeSize ends ...
inside _BuildNet() ...
Routers created ...
PEs connected ...
done with _BuildNet() ...
Done with DragonFlyFull() constructor ...
topology object created ...
Class 0:
Packet latency average = 66.8196
	minimum = 4
	maximum = 674
Network latency average = 66.8196
	minimum = 4
	maximum = 674
Slowest packet = 2906554
Flit latency average = 66.8196
	minimum = 4
	maximum = 674
Slowest flit = 2906554
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.549798
	minimum = 0.489 (at node 6710)
	maximum = 0.614 (at node 13376)
Accepted packet rate average = 0.488137
	minimum = 0.267 (at node 8644)
	maximum = 0.57 (at node 9899)
Injected flit rate average = 0.549798
	minimum = 0.489 (at node 6710)
	maximum = 0.614 (at node 13376)
Accepted flit rate average= 0.488137
	minimum = 0.267 (at node 8644)
	maximum = 0.57 (at node 9899)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 1018140 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 101.65
	minimum = 4
	maximum = 1457
Network latency average = 101.65
	minimum = 4
	maximum = 1457
Slowest packet = 4925854
Flit latency average = 101.65
	minimum = 4
	maximum = 1457
Slowest flit = 4925854
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.549779
	minimum = 0.429 (at node 14253)
	maximum = 0.5945 (at node 3517)
Accepted packet rate average = 0.497375
	minimum = 0.1915 (at node 8644)
	maximum = 0.5755 (at node 6671)
Injected flit rate average = 0.549779
	minimum = 0.429 (at node 14253)
	maximum = 0.5945 (at node 3517)
Accepted flit rate average= 0.497375
	minimum = 0.1915 (at node 8644)
	maximum = 0.5755 (at node 6671)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 1730728 (0 measured)
latency change    = 0.34265
throughput change = 0.0185736
Class 0:
Packet latency average = 191.687
	minimum = 4
	maximum = 2341
Network latency average = 191.685
	minimum = 4
	maximum = 2341
Slowest packet = 5890386
Flit latency average = 191.685
	minimum = 4
	maximum = 2341
Slowest flit = 5890386
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.540662
	minimum = 0.071 (at node 7849)
	maximum = 0.613 (at node 6831)
Accepted packet rate average = 0.50462
	minimum = 0.073 (at node 7525)
	maximum = 0.624 (at node 5066)
Injected flit rate average = 0.540662
	minimum = 0.071 (at node 7849)
	maximum = 0.613 (at node 6831)
Accepted flit rate average= 0.50462
	minimum = 0.073 (at node 7525)
	maximum = 0.624 (at node 5066)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 2326630 (0 measured)
latency change    = 0.469709
throughput change = 0.014357
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 138.624
	minimum = 4
	maximum = 1665
Network latency average = 138.594
	minimum = 4
	maximum = 999
Slowest packet = 27091001
Flit latency average = 247.876
	minimum = 4
	maximum = 3208
Slowest flit = 7043991
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.524559
	minimum = 0.071 (at node 1948)
	maximum = 0.61 (at node 11656)
Accepted packet rate average = 0.504001
	minimum = 0.071 (at node 4635)
	maximum = 0.701 (at node 5799)
Injected flit rate average = 0.524559
	minimum = 0.071 (at node 1948)
	maximum = 0.61 (at node 11656)
Accepted flit rate average= 0.504001
	minimum = 0.071 (at node 4635)
	maximum = 0.701 (at node 5799)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 2666763 (2283316 measured)
latency change    = 0.382782
throughput change = 0.00122722
Class 0:
Packet latency average = 196.736
	minimum = 4
	maximum = 2994
Network latency average = 194.851
	minimum = 4
	maximum = 1994
Slowest packet = 27091001
Flit latency average = 273.924
	minimum = 4
	maximum = 4017
Slowest flit = 8904628
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.520161
	minimum = 0.0785 (at node 1948)
	maximum = 0.59 (at node 1440)
Accepted packet rate average = 0.50392
	minimum = 0.076 (at node 8640)
	maximum = 0.6165 (at node 8054)
Injected flit rate average = 0.520161
	minimum = 0.0785 (at node 1948)
	maximum = 0.59 (at node 1440)
Accepted flit rate average= 0.50392
	minimum = 0.076 (at node 8640)
	maximum = 0.6165 (at node 8054)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 2864050 (2807436 measured)
latency change    = 0.29538
throughput change = 0.000161765
Class 0:
Packet latency average = 251.346
	minimum = 4
	maximum = 4139
Network latency average = 240.924
	minimum = 4
	maximum = 2993
Slowest packet = 27091001
Flit latency average = 294.311
	minimum = 4
	maximum = 4899
Slowest flit = 9595130
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.517052
	minimum = 0.0746667 (at node 1948)
	maximum = 0.583333 (at node 9313)
Accepted packet rate average = 0.50378
	minimum = 0.078 (at node 8640)
	maximum = 0.62 (at node 8054)
Injected flit rate average = 0.517052
	minimum = 0.0746667 (at node 1948)
	maximum = 0.583333 (at node 9313)
Accepted flit rate average= 0.50378
	minimum = 0.078 (at node 8640)
	maximum = 0.62 (at node 8054)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 2985356 (2977668 measured)
latency change    = 0.217268
throughput change = 0.000277697
Class 0:
Packet latency average = 295.22
	minimum = 4
	maximum = 5146
Network latency average = 270.689
	minimum = 4
	maximum = 3979
Slowest packet = 27091001
Flit latency average = 309.324
	minimum = 4
	maximum = 5627
Slowest flit = 10740186
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.514926
	minimum = 0.08025 (at node 1948)
	maximum = 0.577 (at node 3343)
Accepted packet rate average = 0.5037
	minimum = 0.08175 (at node 8640)
	maximum = 0.61375 (at node 8054)
Injected flit rate average = 0.514926
	minimum = 0.08025 (at node 1948)
	maximum = 0.577 (at node 3343)
Accepted flit rate average= 0.5037
	minimum = 0.08175 (at node 8640)
	maximum = 0.61375 (at node 8054)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 3069555 (3067991 measured)
latency change    = 0.148615
throughput change = 0.00015928
Class 0:
Packet latency average = 332.647
	minimum = 4
	maximum = 6120
Network latency average = 290.63
	minimum = 4
	maximum = 4947
Slowest packet = 27091001
Flit latency average = 320.564
	minimum = 4
	maximum = 6463
Slowest flit = 13367190
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.513258
	minimum = 0.0858 (at node 1948)
	maximum = 0.5778 (at node 2233)
Accepted packet rate average = 0.503624
	minimum = 0.0852 (at node 8640)
	maximum = 0.6036 (at node 8054)
Injected flit rate average = 0.513258
	minimum = 0.0858 (at node 1948)
	maximum = 0.5778 (at node 2233)
Accepted flit rate average= 0.503624
	minimum = 0.0852 (at node 8640)
	maximum = 0.6036 (at node 8054)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 3123590 (3123233 measured)
latency change    = 0.112514
throughput change = 0.000150322
Class 0:
Packet latency average = 367.025
	minimum = 4
	maximum = 7042
Network latency average = 305.154
	minimum = 4
	maximum = 5884
Slowest packet = 27091001
Flit latency average = 329.454
	minimum = 4
	maximum = 7374
Slowest flit = 12638301
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.51203
	minimum = 0.0798333 (at node 1948)
	maximum = 0.578667 (at node 15382)
Accepted packet rate average = 0.503565
	minimum = 0.0823333 (at node 8640)
	maximum = 0.600667 (at node 14139)
Injected flit rate average = 0.51203
	minimum = 0.0798333 (at node 1948)
	maximum = 0.578667 (at node 15382)
Accepted flit rate average= 0.503565
	minimum = 0.0823333 (at node 8640)
	maximum = 0.600667 (at node 14139)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 3166998 (3166910 measured)
latency change    = 0.0936665
throughput change = 0.000117925
Class 0:
Packet latency average = 399.5
	minimum = 4
	maximum = 8024
Network latency average = 316.193
	minimum = 4
	maximum = 6830
Slowest packet = 27091001
Flit latency average = 336.59
	minimum = 4
	maximum = 7905
Slowest flit = 18537083
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.511062
	minimum = 0.0811429 (at node 1946)
	maximum = 0.574286 (at node 15382)
Accepted packet rate average = 0.503515
	minimum = 0.0812857 (at node 8620)
	maximum = 0.588857 (at node 14139)
Injected flit rate average = 0.511062
	minimum = 0.0811429 (at node 1946)
	maximum = 0.574286 (at node 15382)
Accepted flit rate average= 0.503515
	minimum = 0.0812857 (at node 8620)
	maximum = 0.588857 (at node 14139)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 3200608 (3200584 measured)
latency change    = 0.0812892
throughput change = 9.83649e-05
Draining all recorded packets ...
Average latency for class 0 exceeded 500 cycles. Aborting simulation.
Simulation unstable, ending ...
Total run time 4004.29
