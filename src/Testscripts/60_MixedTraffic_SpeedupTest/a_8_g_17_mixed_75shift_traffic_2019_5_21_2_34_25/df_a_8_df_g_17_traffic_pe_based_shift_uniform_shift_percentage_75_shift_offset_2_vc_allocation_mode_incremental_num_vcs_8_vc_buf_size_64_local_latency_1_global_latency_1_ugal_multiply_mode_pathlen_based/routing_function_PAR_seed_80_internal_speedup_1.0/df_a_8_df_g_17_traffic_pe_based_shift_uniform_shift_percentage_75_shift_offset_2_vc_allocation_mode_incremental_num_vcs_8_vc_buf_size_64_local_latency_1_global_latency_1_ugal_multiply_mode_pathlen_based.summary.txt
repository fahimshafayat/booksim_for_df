wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 1;
local_latency = 1;
num_vcs = 8;
shift_offset = 2;
shift_percentage = 75;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
vc_buf_size = 64;
internal_speedup = 1.0;
routing_function = PAR;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.31658,   0.35783,    1361.9,    984.25,         1,         0.0
simulation,         2,      0.25,   0.24975,   0.24975,    148.88,    71.105,         0,      5.6976
simulation,         3,      0.37,   0.32028,   0.34897,    557.01,    530.18,         1,         0.0
simulation,         4,      0.31,   0.30779,   0.30785,    373.72,    111.25,         1,         0.0
simulation,         5,      0.28,    0.2789,    0.2789,    409.54,    88.424,         0,      5.7195
simulation,         6,      0.29,   0.28859,    0.2886,    287.88,    89.808,         1,         0.0
simulation,         7,      0.05,  0.050088,  0.050086,    24.444,    24.446,         0,       5.463
simulation,         8,       0.1,   0.10017,   0.10017,    25.407,    25.408,         0,       5.411
simulation,         9,      0.15,   0.15021,   0.15021,    29.072,    29.075,         0,      5.4589
simulation,        10,       0.2,   0.20019,    0.2002,    33.804,    33.807,         0,      5.6103
simulation,        11,      0.25,   0.24975,   0.24975,    148.88,    71.105,         0,      5.6976
