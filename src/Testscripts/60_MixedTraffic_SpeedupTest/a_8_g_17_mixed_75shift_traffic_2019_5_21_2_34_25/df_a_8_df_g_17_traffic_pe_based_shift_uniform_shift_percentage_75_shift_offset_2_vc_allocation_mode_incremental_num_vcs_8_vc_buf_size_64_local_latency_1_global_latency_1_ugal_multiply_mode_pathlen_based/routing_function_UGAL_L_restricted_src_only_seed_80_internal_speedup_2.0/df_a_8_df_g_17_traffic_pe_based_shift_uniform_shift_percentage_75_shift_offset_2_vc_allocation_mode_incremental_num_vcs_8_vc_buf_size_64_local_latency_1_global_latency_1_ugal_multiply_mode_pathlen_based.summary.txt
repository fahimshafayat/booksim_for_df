wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 1;
local_latency = 1;
num_vcs = 8;
shift_offset = 2;
shift_percentage = 75;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
vc_buf_size = 64;
internal_speedup = 2.0;
routing_function = UGAL_L_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.48453,   0.48511,    521.72,    195.71,         1,         0.0
simulation,         2,      0.25,   0.25024,   0.25024,     35.69,    35.722,         0,      4.6581
simulation,         3,      0.37,   0.37028,   0.37028,    36.497,    36.545,         0,      4.7631
simulation,         4,      0.43,   0.43021,   0.43021,    38.883,    38.946,         0,      4.7958
simulation,         5,      0.46,   0.45874,   0.45884,    176.48,     64.14,         0,        4.81
simulation,         6,      0.48,   0.47334,   0.47336,    528.54,    114.76,         1,         0.0
simulation,         7,      0.47,   0.46648,   0.46654,    360.07,    89.409,         1,         0.0
simulation,         8,       0.1,   0.10017,   0.10017,    15.901,    15.902,         0,      4.4424
simulation,         9,       0.2,    0.2002,    0.2002,    31.439,    31.457,         0,      4.5783
simulation,        10,       0.3,   0.30023,   0.30023,    35.622,    35.661,         0,      4.7121
simulation,        11,       0.4,   0.40025,   0.40026,    37.361,    37.412,         0,        4.78
