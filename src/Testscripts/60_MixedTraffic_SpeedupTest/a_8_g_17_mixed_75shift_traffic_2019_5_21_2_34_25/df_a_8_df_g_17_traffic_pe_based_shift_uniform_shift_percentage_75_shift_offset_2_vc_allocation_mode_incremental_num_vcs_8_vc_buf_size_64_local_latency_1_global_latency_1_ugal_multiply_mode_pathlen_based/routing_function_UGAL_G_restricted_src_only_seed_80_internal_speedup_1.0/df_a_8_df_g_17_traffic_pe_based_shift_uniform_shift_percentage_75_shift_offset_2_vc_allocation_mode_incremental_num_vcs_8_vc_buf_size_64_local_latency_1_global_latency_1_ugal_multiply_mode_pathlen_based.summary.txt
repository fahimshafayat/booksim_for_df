wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 1;
local_latency = 1;
num_vcs = 8;
shift_offset = 2;
shift_percentage = 75;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
vc_buf_size = 64;
internal_speedup = 1.0;
routing_function = UGAL_G_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.37509,   0.41279,    895.03,    696.41,         1,         0.0
simulation,         2,      0.25,   0.25012,   0.25013,    50.848,     43.13,         0,      4.6588
simulation,         3,      0.37,   0.36805,   0.36805,    320.85,    75.142,         1,         0.0
simulation,         4,      0.31,   0.30877,   0.30877,    402.55,    68.266,         0,      4.7036
simulation,         5,      0.34,   0.33829,    0.3383,    300.78,    68.134,         1,         0.0
simulation,         6,      0.32,   0.31858,   0.31857,    454.12,    68.103,         0,      4.7097
simulation,         7,      0.33,   0.32844,   0.32845,    287.17,    67.552,         1,         0.0
simulation,         8,       0.1,   0.10017,   0.10017,    21.613,    21.613,         0,      4.4953
simulation,         9,       0.2,    0.2002,    0.2002,    26.222,    26.222,         0,      4.6043
simulation,        10,       0.3,   0.29893,   0.29893,    329.92,    68.653,         0,      4.6958
