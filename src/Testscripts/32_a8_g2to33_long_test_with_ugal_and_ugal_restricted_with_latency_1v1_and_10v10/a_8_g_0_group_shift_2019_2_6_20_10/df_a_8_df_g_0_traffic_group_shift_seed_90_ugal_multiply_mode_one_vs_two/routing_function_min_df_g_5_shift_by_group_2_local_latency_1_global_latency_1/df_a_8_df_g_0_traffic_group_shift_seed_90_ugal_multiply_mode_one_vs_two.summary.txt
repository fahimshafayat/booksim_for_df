vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;
df_g = 5;
global_latency = 1;
local_latency = 1;
routing_function = min;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.24976,   0.30257,    2497.0,    1680.1,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24954,   0.25005,    231.26,    233.56,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.24975,   0.30193,    1612.1,    1416.7,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.24972,   0.30124,    959.64,    957.47,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.24966,   0.27994,    531.38,    531.38,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,      0.25,   0.25993,    989.08,    988.25,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.20008,   0.20008,    11.615,    11.616,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.15,   0.14995,   0.14995,    10.356,    10.356,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.1,  0.099924,  0.099925,    9.8987,    9.8988,         0,  0,  0,  ,  ,  ,  
