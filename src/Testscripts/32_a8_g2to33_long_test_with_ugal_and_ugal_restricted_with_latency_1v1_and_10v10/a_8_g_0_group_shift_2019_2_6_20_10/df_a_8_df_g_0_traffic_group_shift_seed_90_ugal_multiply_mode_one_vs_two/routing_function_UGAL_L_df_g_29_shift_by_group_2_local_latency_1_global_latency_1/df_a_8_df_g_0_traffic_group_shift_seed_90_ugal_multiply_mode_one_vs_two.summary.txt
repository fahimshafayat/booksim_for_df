vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;
df_g = 29;
global_latency = 1;
local_latency = 1;
routing_function = UGAL_L;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.37837,   0.41518,    1047.5,    717.53,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,    0.2501,    0.2501,    37.404,    37.469,         0,  2413460,  11559087,  ,  ,  ,  
simulation,         3,      0.37,   0.35072,   0.35196,    1111.1,    588.81,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31011,    0.3101,    43.553,    43.726,         0,  2437710,  15041582,  ,  ,  ,  
simulation,         5,      0.34,   0.33437,   0.33574,    514.63,     455.6,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.32,   0.31988,   0.32011,    77.532,    78.776,         0,  2920840,  18677723,  ,  ,  ,  
simulation,         7,      0.33,    0.3273,   0.32852,    473.91,    360.67,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.30008,   0.30008,    40.977,    41.083,         0,  2420072,  14378268,  ,  ,  ,  
simulation,        10,       0.2,   0.20012,   0.20012,     36.75,    36.798,         0,  2410579,  8759098,  ,  ,  ,  
simulation,        11,      0.15,   0.15013,   0.15013,    37.258,    37.295,         0,  2408263,  5963816,  ,  ,  ,  
