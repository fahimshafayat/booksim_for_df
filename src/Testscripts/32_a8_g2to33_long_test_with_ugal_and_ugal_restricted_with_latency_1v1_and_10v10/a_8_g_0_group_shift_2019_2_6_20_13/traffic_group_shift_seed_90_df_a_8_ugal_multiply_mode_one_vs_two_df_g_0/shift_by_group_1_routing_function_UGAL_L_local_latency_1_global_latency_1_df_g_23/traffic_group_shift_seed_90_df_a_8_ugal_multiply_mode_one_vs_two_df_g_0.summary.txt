num_vcs = 7;
sim_count = 1;
packet_size = 1;
vc_alloc_delay = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
routing_delay = 0;
output_speedup = 1;
alloc_iters = 1;
sw_allocator = separable_input_first;
sample_period = 10000;
warmup_periods = 3;
credit_delay = 2;
input_speedup = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
vc_buf_size = 64;
priority = none;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;
df_g = 23;
global_latency = 1;
local_latency = 1;
routing_function = UGAL_L;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.32569,   0.36445,    1488.0,    929.57,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25002,   0.25006,    48.157,    48.568,         0,  2403421,  10973060,  ,  ,  ,  
simulation,         3,      0.37,   0.32546,   0.35335,    511.74,    466.65,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30386,   0.30616,    484.64,    372.59,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27893,   0.27893,     321.8,    104.82,         0,  3832167,  19974963,  ,  ,  ,  
simulation,         6,      0.29,   0.28854,   0.28854,    416.17,     106.6,         0,  3953878,  21451847,  ,  ,  ,  
simulation,         7,       0.3,   0.29781,   0.29809,    451.53,    179.51,         1,  0,  0,  ,  ,  ,  
simulation,        10,       0.2,   0.20002,   0.20003,    37.877,    37.929,         0,  1988777,  6869224,  ,  ,  ,  
simulation,        11,      0.15,      0.15,   0.15001,     38.06,      38.1,         0,  1986857,  4651329,  ,  ,  ,  
