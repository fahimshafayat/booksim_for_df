num_vcs = 7;
sim_count = 1;
packet_size = 1;
vc_alloc_delay = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
routing_delay = 0;
output_speedup = 1;
alloc_iters = 1;
sw_allocator = separable_input_first;
sample_period = 10000;
warmup_periods = 3;
credit_delay = 2;
input_speedup = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
vc_buf_size = 64;
priority = none;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;
df_g = 23;
global_latency = 1;
local_latency = 1;
routing_function = min;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.048873,  0.096032,    4350.1,    4146.0,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.04887,   0.09607,    3697.3,    3666.1,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.048868,  0.093879,    2491.7,    2491.7,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.048854,  0.070033,    1026.0,    1026.0,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.036153,  0.039998,    787.88,    787.88,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.02,  0.020018,  0.020017,    10.017,    10.018,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.03,  0.030027,  0.030028,    15.009,    15.008,         0,  0,  0,  ,  ,  ,  
