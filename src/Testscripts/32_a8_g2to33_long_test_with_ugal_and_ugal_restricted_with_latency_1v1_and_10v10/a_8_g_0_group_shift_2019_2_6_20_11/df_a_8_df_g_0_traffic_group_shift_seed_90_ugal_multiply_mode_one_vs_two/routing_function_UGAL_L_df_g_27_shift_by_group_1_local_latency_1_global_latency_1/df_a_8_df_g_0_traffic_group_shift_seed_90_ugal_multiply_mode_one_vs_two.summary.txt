vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;
df_g = 27;
global_latency = 1;
local_latency = 1;
routing_function = UGAL_L;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.35556,   0.39389,    1283.8,    828.43,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25009,   0.25009,    36.537,    36.606,         0,  1992915,  11026063,  ,  ,  ,  
simulation,         3,      0.37,   0.34399,   0.35637,    593.19,    442.11,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30645,   0.30762,    507.66,    386.07,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.28011,    0.2801,     39.27,    39.361,         0,  2001309,  12633453,  ,  ,  ,  
simulation,         6,      0.29,   0.29006,   0.29008,    44.562,    44.616,         0,  2038160,  13388972,  ,  ,  ,  
simulation,         7,       0.3,   0.29933,   0.29987,    183.59,     176.4,         0,  3497025,  23844027,  ,  ,  ,  
simulation,        10,       0.2,   0.20011,   0.20011,    35.564,    35.615,         0,  1988912,  8410014,  ,  ,  ,  
simulation,        11,      0.15,   0.15008,   0.15008,    35.992,    36.034,         0,  1987387,  5807892,  ,  ,  ,  
