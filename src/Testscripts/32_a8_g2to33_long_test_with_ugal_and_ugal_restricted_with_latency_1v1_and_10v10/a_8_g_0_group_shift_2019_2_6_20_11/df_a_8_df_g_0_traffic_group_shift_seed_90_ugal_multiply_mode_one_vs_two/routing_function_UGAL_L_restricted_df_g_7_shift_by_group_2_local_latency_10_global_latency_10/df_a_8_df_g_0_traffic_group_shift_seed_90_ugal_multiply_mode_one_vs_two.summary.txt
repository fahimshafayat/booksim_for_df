vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;
df_g = 7;
global_latency = 10;
local_latency = 10;
routing_function = UGAL_L_restricted;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.47533,   0.47543,    919.01,    359.13,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24999,      0.25,    50.996,    51.004,         0,  2268541,  1098558,  ,  ,  ,  
simulation,         3,      0.37,   0.36983,   0.36984,    111.46,     111.6,         0,  2416121,  2578454,  ,  ,  ,  
simulation,         4,      0.43,   0.42992,   0.42994,    132.12,    132.46,         0,  2424096,  3392653,  ,  ,  ,  
simulation,         5,      0.46,   0.45987,   0.45991,    145.54,    146.02,         0,  2430620,  3803333,  ,  ,  ,  
simulation,         6,      0.48,   0.47108,   0.47082,    464.44,    248.97,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.46733,   0.46737,    439.47,    182.65,         0,  4833566,  7795675,  ,  ,  ,  
simulation,         8,      0.45,   0.44993,   0.44994,     140.6,    141.04,         0,  2427394,  3665061,  ,  ,  ,  
simulation,         9,       0.4,   0.39992,   0.39993,    121.08,    121.27,         0,  2420339,  2985847,  ,  ,  ,  
simulation,        10,      0.35,   0.34976,   0.34978,    104.91,    105.01,         0,  2414279,  2307104,  ,  ,  ,  
simulation,        11,       0.3,   0.29979,   0.29981,    85.835,    85.868,         0,  2409750,  1632875,  ,  ,  ,  
