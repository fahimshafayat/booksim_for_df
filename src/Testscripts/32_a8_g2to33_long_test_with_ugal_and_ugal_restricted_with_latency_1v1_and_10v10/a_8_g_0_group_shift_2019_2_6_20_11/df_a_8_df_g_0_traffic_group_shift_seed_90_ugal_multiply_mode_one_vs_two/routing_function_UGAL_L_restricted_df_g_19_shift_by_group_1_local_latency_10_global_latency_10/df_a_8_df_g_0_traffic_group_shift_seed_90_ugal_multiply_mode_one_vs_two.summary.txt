vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;
df_g = 19;
global_latency = 10;
local_latency = 10;
routing_function = UGAL_L_restricted;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.37326,    0.4081,    846.58,    611.31,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25011,   0.25012,    83.008,    83.114,         0,  1994121,  7170059,  ,  ,  ,  
simulation,         3,      0.37,   0.35952,   0.35981,    482.03,    233.92,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.31005,   0.31005,    88.146,    88.325,         0,  1999483,  9380141,  ,  ,  ,  
simulation,         5,      0.34,   0.34008,   0.34009,    92.163,    92.389,         0,  2004325,  10499819,  ,  ,  ,  
simulation,         6,      0.35,   0.34722,   0.34723,    454.33,    150.93,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.30005,   0.30005,    87.232,    87.399,         0,  1998908,  9012772,  ,  ,  ,  
simulation,        10,       0.2,   0.20011,   0.20011,    78.276,    78.334,         0,  1990391,  5332452,  ,  ,  ,  
