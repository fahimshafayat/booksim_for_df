vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;
df_g = 3;
global_latency = 10;
local_latency = 10;
routing_function = vlb;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.24676,   0.30185,    2563.1,    1771.4,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24838,   0.25017,    512.18,    512.53,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.13007,   0.13006,    66.081,    66.082,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.19,   0.19011,    0.1901,    68.495,    68.496,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.22,   0.22018,   0.22017,    72.896,    72.891,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.23,   0.23022,   0.23019,    77.113,    77.106,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.24,   0.24031,   0.24025,    90.209,    90.178,         0,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.20011,   0.20011,    69.388,    69.386,         0,  0,  0,  ,  ,  ,  
simulation,         9,      0.15,   0.15003,   0.15004,    66.589,     66.59,         0,  0,  0,  ,  ,  ,  
simulation,        10,       0.1,   0.10015,   0.10014,    65.525,    65.527,         0,  0,  0,  ,  ,  ,  
simulation,        11,      0.05,  0.050083,  0.050078,     64.91,    64.909,         0,  0,  0,  ,  ,  ,  
