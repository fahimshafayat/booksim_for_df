vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;
df_g = 31;
global_latency = 10;
local_latency = 10;
routing_function = vlb;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.44201,   0.49048,    610.99,    600.99,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,    67.257,    67.259,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.37006,   0.37006,    73.339,    73.339,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,      0.43,   0.42999,    93.893,    93.889,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,    0.4428,   0.45278,    858.03,    759.19,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.44001,   0.43999,     112.7,    112.72,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.45,   0.44602,   0.44898,    515.81,    476.29,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.40003,   0.40004,    78.312,    78.312,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35005,   0.35006,     71.47,     71.47,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30004,   0.30004,    68.746,    68.747,         0,  0,  0,  ,  ,  ,  
