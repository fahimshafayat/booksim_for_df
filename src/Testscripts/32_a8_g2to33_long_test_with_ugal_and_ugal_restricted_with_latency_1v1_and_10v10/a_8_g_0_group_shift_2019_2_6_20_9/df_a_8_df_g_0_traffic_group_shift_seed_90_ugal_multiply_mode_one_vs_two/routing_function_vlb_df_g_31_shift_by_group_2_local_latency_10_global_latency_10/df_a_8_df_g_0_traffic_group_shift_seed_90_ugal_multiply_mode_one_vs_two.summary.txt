vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;
df_g = 31;
global_latency = 10;
local_latency = 10;
routing_function = vlb;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.44123,   0.48978,    616.09,    605.46,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25005,     67.28,    67.281,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.37,   0.37006,   0.37006,    73.486,    73.486,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.43,      0.43,   0.42999,    94.679,    94.686,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.46,   0.44323,    0.4526,    838.06,     763.6,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.44001,   0.43999,    115.78,    115.79,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.45,   0.44544,   0.44904,     519.2,    487.73,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.40003,   0.40004,    78.588,     78.59,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.35,   0.35005,   0.35006,    71.561,    71.561,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.3,   0.30005,   0.30004,    68.794,    68.795,         0,  0,  0,  ,  ,  ,  
