vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;
df_g = 15;
global_latency = 1;
local_latency = 1;
routing_function = UGAL_L_restricted;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.44538,   0.45526,     758.7,    356.35,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24997,   0.24997,     38.71,    38.762,         0,  2047576,  5169456,  ,  ,  ,  
simulation,         3,      0.37,   0.36986,   0.36986,    40.447,    40.533,         0,  2051673,  8639997,  ,  ,  ,  
simulation,         4,      0.43,   0.42143,   0.42145,    473.06,    150.55,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.39981,   0.39981,    43.052,    43.155,         0,  2053914,  9513336,  ,  ,  ,  
simulation,         6,      0.41,   0.40965,   0.40966,    77.679,    55.553,         0,  3017070,  14401679,  ,  ,  ,  
simulation,         7,      0.42,   0.41507,   0.41508,    492.09,    109.57,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.35,   0.34989,    0.3499,    39.665,    39.743,         0,  2050253,  8057680,  ,  ,  ,  
simulation,        10,       0.3,    0.2999,    0.2999,    38.788,    38.853,         0,  2048906,  6611786,  ,  ,  ,  
