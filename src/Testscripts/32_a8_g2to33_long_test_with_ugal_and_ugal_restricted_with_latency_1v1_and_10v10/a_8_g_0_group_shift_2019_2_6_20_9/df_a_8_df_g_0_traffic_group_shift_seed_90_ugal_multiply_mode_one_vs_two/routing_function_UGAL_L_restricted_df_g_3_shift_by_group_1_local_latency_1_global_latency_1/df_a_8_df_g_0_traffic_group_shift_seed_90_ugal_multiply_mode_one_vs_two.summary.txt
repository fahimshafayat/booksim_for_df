vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;
df_g = 3;
global_latency = 1;
local_latency = 1;
routing_function = UGAL_L_restricted;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50018,   0.50017,    12.185,    12.185,         0,  2248635,  632403,  ,  ,  ,  
simulation,         2,      0.74,   0.73997,   0.73994,    72.705,    72.751,         0,  2890531,  1383277,  ,  ,  ,  
simulation,         3,      0.86,   0.72185,   0.77075,    751.04,    511.22,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,    0.7222,   0.74684,    868.05,    524.54,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.71693,   0.71985,    1069.9,    636.65,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.75,   0.74959,   0.75001,    155.53,    155.76,         0,  3393791,  1682724,  ,  ,  ,  
simulation,         7,      0.76,   0.72779,   0.73291,    811.69,    547.22,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.7,   0.69998,   0.69995,    53.549,    53.571,         0,  2883347,  1156296,  ,  ,  ,  
simulation,        10,      0.65,   0.65004,   0.65002,    25.704,     25.71,         0,  2860114,  888097,  ,  ,  ,  
simulation,        11,       0.6,   0.60022,   0.60022,    14.953,    14.954,         0,  2689659,  768429,  ,  ,  ,  
