vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;
df_g = 21;
global_latency = 1;
local_latency = 1;
routing_function = min;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.055014,   0.10243,    4362.8,    4213.1,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.05501,    0.1024,    3719.7,    3700.1,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.055009,   0.10118,    2528.1,    2528.0,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.054993,  0.070076,    770.77,    770.77,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.037879,  0.039998,    424.91,    424.91,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.02,  0.020029,  0.020028,    9.8958,    9.8958,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.03,  0.030016,  0.030012,    13.072,    13.077,         0,  0,  0,  ,  ,  ,  
