vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;
df_g = 21;
global_latency = 10;
local_latency = 10;
routing_function = UGAL_L;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.30441,   0.34598,    1618.8,    1056.2,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24806,   0.24807,    491.29,    250.96,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.13001,   0.13002,    80.773,    80.794,         0,  1987977,  3268407,  ,  ,  ,  
simulation,         4,      0.19,    0.1901,   0.19009,    92.472,    92.543,         0,  1994802,  5707519,  ,  ,  ,  
simulation,         5,      0.22,    0.2201,    0.2201,    98.617,    98.753,         0,  2002444,  6944066,  ,  ,  ,  
simulation,         6,      0.23,   0.22983,   0.23004,    155.15,    154.87,         0,  2485581,  9116936,  ,  ,  ,  
simulation,         7,      0.24,   0.23893,   0.23909,    398.11,    252.99,         0,  3668747,  14155181,  ,  ,  ,  
simulation,         8,       0.2,   0.20011,   0.20011,    94.079,     94.16,         0,  1997741,  6121255,  ,  ,  ,  
simulation,         9,      0.15,   0.15003,   0.15003,    85.396,    85.428,         0,  1990059,  4079747,  ,  ,  ,  
simulation,        10,       0.1,  0.099974,  0.099976,    61.925,    61.938,         0,  1870348,  2170395,  ,  ,  ,  
simulation,        11,      0.05,  0.050014,  0.050013,     49.95,    49.956,         0,  1046547,  971364,  ,  ,  ,  
