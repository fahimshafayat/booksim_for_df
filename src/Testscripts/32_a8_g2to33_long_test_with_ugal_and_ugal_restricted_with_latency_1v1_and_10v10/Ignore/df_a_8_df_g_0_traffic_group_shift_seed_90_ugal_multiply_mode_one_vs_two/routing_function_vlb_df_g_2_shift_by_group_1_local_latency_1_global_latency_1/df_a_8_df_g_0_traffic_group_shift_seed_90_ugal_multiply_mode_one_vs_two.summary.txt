vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 7;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 3;
sample_period = 10000;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
seed = 90;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;
df_g = 2;
global_latency = 1;
local_latency = 1;
routing_function = vlb;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,       0.0,       0.0,       0.0,       0.0,         0,  0,  0,  ,  ,  ,  
simulation,         2,      0.74,       0.0,       0.0,       0.0,       0.0,         0,  0,  0,  ,  ,  ,  
simulation,         3,      0.86,       0.0,       0.0,       0.0,       0.0,         0,  0,  0,  ,  ,  ,  
simulation,         4,      0.92,       0.0,       0.0,       0.0,       0.0,         0,  0,  0,  ,  ,  ,  
simulation,         5,      0.95,       0.0,       0.0,       0.0,       0.0,         0,  0,  0,  ,  ,  ,  
simulation,         6,      0.97,       0.0,       0.0,       0.0,       0.0,         0,  0,  0,  ,  ,  ,  
simulation,         7,      0.98,       0.0,       0.0,       0.0,       0.0,         0,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,       0.0,       0.0,       0.0,       0.0,         0,  0,  0,  ,  ,  ,  
simulation,        10,      0.85,       0.0,       0.0,       0.0,       0.0,         0,  0,  0,  ,  ,  ,  
simulation,        11,       0.8,       0.0,       0.0,       0.0,       0.0,         0,  0,  0,  ,  ,  ,  
