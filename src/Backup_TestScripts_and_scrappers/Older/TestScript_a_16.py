import os
import multiprocessing as MP
import math
import sys
import datetime

#for multiprocessing, each process gets its own copy of globals in its own memory pool,
#so using global is safe. It might be considered bad design though.

topLevelOutputFolderName = "" #set downunder
secondLevelFolderName = ""    #changed downunder
thirdLevelFolderName = ""    #changed downunder

parameters_default = {
                    "vc_buf_size"               :   64,
                    "wait_for_tail_credit"      :   0,
                    "vc_allocator"              :   "separable_input_first", 
                    "sw_allocator"              :   "separable_input_first",
                    "alloc_iters"               :   1,
                    "credit_delay"              :   2,
                    "routing_delay"             :   0,
                    "vc_alloc_delay"            :   1,
                    "sw_alloc_delay"            :   1,
                    "st_final_delay"            :   1,
                    "input_speedup"             :   1,
                    "output_speedup"            :   1,
                    "internal_speedup"          :   2.0,
                    "warmup_periods"            :   3,
                    "sim_count"                 :   1,
                    "sample_period"             :   1000,  
                    "num_vcs"                   :   7,
                    "priority"                  :   "none",
                    "packet_size"               :   1,
                    "injection_rate_uses_flits" :   1,
                    }


#may be change to params dict
def setSecondLevelFolderName(params):
    global secondLevelFolderName
    
    #secondLevelFolderName = "Q_{}_seed_{}_traffic_{}_tSeed_{}_permSeed_{}".format(params["slimflyQ"], params["seed"],params["traffic"], params["traffic_seed"], params["perm_seed"])
    for k,v in params.items():
        secondLevelFolderName += k + "_" + str(v) + "_" 
    
    secondLevelFolderName = secondLevelFolderName[:len(secondLevelFolderName)-1] #to chop off the trailing underscore
    
    pass
    

#change to params dict
def setThirdLevelFolderName(params):
    global thirdLevelFolderName
    
    for k,v in params.items():
        thirdLevelFolderName += k + "_" + str(v) + "_" 
    
    thirdLevelFolderName = thirdLevelFolderName[:len(thirdLevelFolderName)-1] #to chop off the trailing underscore
    
    pass


def launchSimulation(second_level_params, third_level_params, injection_rate, count):
    
    
    outputFolderName = topLevelOutputFolderName + "/" + secondLevelFolderName+ "/" + thirdLevelFolderName 
    os.makedirs(outputFolderName, exist_ok= True)

    inputFiles = []
    outputFiles = []    
    
    #make the input config file
    #inputFileName = outputFolderName + "/" + params["topology"] + "_" + params["traffic"] + "_" + "Q_" + str(params["slimflyQ"]) + "_" + "{:.0f}".format( (float(params["injection_rate"])*100))
    
    inputFileName = outputFolderName + "/"
    for k,v in second_level_params.items():
        inputFileName += k + "_" + str(v) + "_"
        
    for k,v in third_level_params.items():
        inputFileName += k + "_" + str(v) + "_"
    
    inputFileName += "irate" + "_" + "{:.0f}".format(injection_rate*100)
    
    inputFiles.append(inputFileName)
    fp = open(inputFileName,"w")

    #then give the changing values        
    parameters = "" #initializing
    
    for k,v in parameters_default.items():
        parameters += "{} = {};\n".format(k, str(v))
    
    for k,v in second_level_params.items():
        parameters += "{} = {};\n".format(k, str(v))
    
    for k,v in third_level_params.items():
        parameters += "{} = {};\n".format(k, str(v))
    
    parameters += "injection_rate = {:.2f};\n".format(injection_rate)
    
    fp.write(parameters)
    
    fp.close()


    #run the simulation. Save the result to a specific output file.
    outputFileName = inputFileName  + ".out"    #inputFilename already contains the outputFolderName2 as part of its name
    outputFiles.append(outputFileName)
    
    if os.path.exists(outputFileName) == False:
        command = exe_name + " " + inputFileName + " > " + outputFileName
        print(command)
        
        #******************************************************************
        os.system(command)
        #******************************************************************
        
    else:
        print("skipping simulation for " + inputFileName + " as it was already done at last round ...")
    
    #simulation done. Now open the output files, scrap important info, present in a final output file.
    #summaryResultFileName = topLevelOutputFolderName + "/" + secondLevelFolderName + "/" + thirdLevelFolderName + "/" +  "{}_{}_Q_{}.summary.txt".format(params["topology"], params["traffic"], params["slimflyQ"])
    
    summaryResultFileName = outputFolderName + "/"
    for k,v in second_level_params.items():
        summaryResultFileName += k + "_" + str(v) + "_"
    for k,v in third_level_params.items():
        summaryResultFileName += k + "_" + str(v) + "_"
    summaryResultFileName += ".summary.txt"
    
    print("*********** summaryResultFileName: ", summaryResultFileName)
    
    fp2 = open(summaryResultFileName,"a")
    
    if count == 1:
        parameters = ""
        for k,v in parameters_default.items():
            parameters += "{} = {};\n".format(k, str(v))
    
        for k,v in sorted(second_level_params.items()):
            parameters += "{} = {};\n".format(k, str(v))
        for k,v in sorted(third_level_params.items()):
            parameters += "{} = {};\n".format(k, str(v))
        fp2.write(parameters)
        fp2.write("\n")
        fp2.write("defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?\n")

    for outputFile in outputFiles:
        fp = open(outputFile,"r")
        text = fp.readlines()
    
        line1 = ""
        line2 = ""
        line3 = ""
        iRate = 0.0
        aRate = 0.0
        latency = 0.0
        deadlock = False
        flag1 = False
        flag2 = False
        flag3 = False
        for line in text[::-1]:
            if "Accepted flit rate average" in line and flag1 is False:
                line1 = line
                flag1 = True
            if "Injected flit rate average" in line and flag2 is False:
                line2 = line
                flag2 = True
            if "Flit latency average" in line and flag3 is False:
                line3 = line
                flag3 = True  
        if line1 != "":
            iRate = float(line1[line1.find("=")+2 : line1.find("(")])
        if line2 != "":
            aRate = float(line2[line2.find("=")+2 : line2.find("(")])
        if line3 != "":
            latency = float(line3[line3.find("=")+2 : line3.find("(")])        
        
        for line in text[::-1]:
            if "Simulation unstable, ending " in line:
                deadlock = True
                break
            if "WARNING: Possible network deadlock" in line:
                deadlock = True
                break
            if "At least one router queue full" in line:
                deadlock = True
                break
            
        result = "simulation,{:>10},{:>10.5},{:>10.5},{:>10.5},{:>10.5},{:>10}\n".format(count,injection_rate,iRate,aRate,latency,deadlock)
         
        fp2.write(result)

    fp2.close()

    #cleanup. Remove the input files.
    for file in inputFiles:
        os.remove(file)
    
    return deadlock
    
    pass    




def probeDeeperAndDeeperBinarySearch(second_level_params, third_level_params, lowLim = 1, highLim = 99):
    #no global. bad bad global.
        
    #setSecondLevelFolderName(a = params["dfm_a"], h = params["dfm_h"], g = params["dfm_g"], p = params["dfm_p"], traffic = params["traffic"], routing = params["routing_function"])
    setSecondLevelFolderName(second_level_params)
    
    #setThirdLevelFolderName(connectionPattern = params["dfm_interG_topology"])
    setThirdLevelFolderName(third_level_params)
    
    low = lowLim
    high = highLim
    
    #print("simulating for i-rate: {0:.2f} ".format(low * 0.01))
    count = 1
    #params["injection_rate"] = "{:.2f}".format(low * 0.01)
    satLow = launchSimulation(second_level_params, third_level_params, low*0.01, count) 
    
    count = 2
    #print("simulating for i-rate: {0:.2f} ".format(high * 0.01))
    #params["injection_rate"] = "{:.2f}".format(high * 0.01)
    satHigh = launchSimulation(second_level_params, third_level_params, high*0.01, count)
    
    if satLow == True or satHigh == False:
        sys.exit("Incorrect low and high i_rate settings. Exiting.")    
    
    while( (high - low) > 1):    
        mid = math.floor((low + high)/2)
        count += 1
        #params["injection_rate"] = "{:.2f}".format(mid * 0.01)
        #i_rate = mid * 0.01
        #print("simulating for i-rate: {0:.2f} ".format(mid * 0.01))
        satMid = launchSimulation(second_level_params, third_level_params, mid*0.01, count)
        if satMid is True:
            high = mid
        else:
            low = mid
                
    pass

if __name__ == "__main__":
    print("Hello World!")
    
    '''
    #    List of params:
    #        //customizables
    #        //dragonfly specific parameters
    #        topology = dragonflyfull;
    #        df_a = 4;
    #        df_g = 7;
    #        df_arrangement = absolute_improved;
    #        //end dragonfly specific paramters

    #        routing_function = UGAL_L_two_hop; //min/vlb//UGAL_L//UGAL_L_two_hop
    #        traffic       = randperm;
    #        perm_seed = 42;
    #        injection_rate = 0.15;
    #        seed = 11
    '''
    
    #put the defaults
    parameters_specific =            {
                        "topology"          :   "dragonflyfull",     
                        "df_a"              :   16,
                         "df_arrangement"    :   "absolute_improved",
                        "traffic"           :   "randperm",
                        "seed"              :   11,
                        }
                        #"df_g"              :   21,
                        #"perm_seed"         :   42,
                        

    exe_name = "../booksim"    #global
    
    now = datetime.datetime.now()
    
    topLevelOutputFolderName = "a_{}_arrangement_{}_{}_{}_{}_{}".format(parameters_specific["df_a"], parameters_specific["df_arrangement"], now.year, now.month, now.day, now.hour, now.minute)
    os.makedirs(topLevelOutputFolderName, exist_ok= True)

    #parameters we want to loop over with
    #seeds = [2]
    #perm_seeds = [2]
    #traffics = ["randperm"]
    
    df_gs = [21,27,35]
    perm_seeds = [42, 44]
    routing_functions = ["UGAL_L", "UGAL_L_two_hop"]

    processes = []
    
    for df_g in df_gs:
        for perm_seed in perm_seeds:
            for routing_function in routing_functions:
                second_level_params = {}
                third_level_params = {}
                
                #        for k,v in parameters_default.items():
                #            params[k] = v
                    
                for k,v in parameters_specific.items():
                    second_level_params[k] = v
                
                third_level_params["routing_function"] = routing_function
                third_level_params["df_g"] = df_g
                third_level_params["perm_seed"] = perm_seed
                
                p = MP.Process( target = probeDeeperAndDeeperBinarySearch, args = (second_level_params, third_level_params))
                processes.append(p)
                p.start()
                
                temp_str = "process started with "
                for k,v in second_level_params.items():
                    temp_str += " " + k + " : " + str(v) + " , "
                for k,v in third_level_params.items():
                    temp_str += " " + k + " : " + str(v) + " , "
                temp_str += "process_name: {} , pid: {}".format(p.name, p.pid)
                print(temp_str)  
                    
    #wait for all processes to finish
    #this means all the processes with a same traffic seed will finish before the next t_seed kicks in
    for p in processes:
        p.join()
    print("------ All process ended.")
    
    print("The world never says hello back.")
    
