import os
import multiprocessing as MP
import math
import sys

topLevelOutputFolderName = "default" #set downunder
secondLevelFolderName = "default"    #changed downunder
thirdLevelFolderName = "default"    #changed downunder

defaultParameters = {"topology"       :  "slimfly",
                    "num_vcs"         :   5,
                    "internal_speedup":   2.5,
                    "credit_delay"    :   2,
                    "sim_type"        :   "latency",
                    "vc_buf_size"     :   64,
                    }


#may be change to params dict
def setSecondLevelFolderName(params):
    global secondLevelFolderName
    
    secondLevelFolderName = "Q_{}_seed_{}_traffic_{}_tSeed_{}_permSeed_{}".format(params["slimflyQ"], params["seed"],params["traffic"], params["traffic_seed"], params["perm_seed"])
    
    pass
    

#change to params dict
def setThirdLevelFolderName(params):
    global thirdLevelFolderName
    
    thirdLevelFolderName = "{}_probMultipliers_{:.0f}_{:.0f}_{:.0f}_{:.0f}_{:.0f}".format(params["routing_function"], params["probMultiplierA"]*100, params["probMultiplierB"]*100, params["probMultiplierC"]*100,  params["probMultiplierD"]*100, params["probMultiplierBase"]*100 )
    
    pass


def launchSimulation(params, count):
    
    
    outputFolderName = topLevelOutputFolderName + "/" + secondLevelFolderName+ "/" + thirdLevelFolderName 
    os.makedirs(outputFolderName, exist_ok= True)

    inputFiles = []
    outputFiles = []    
    
    #make the input config file
    inputFileName = outputFolderName + "/" + params["topology"] + "_" + params["traffic"] + "_" + "Q_" + str(params["slimflyQ"]) + "_" + "{:.0f}".format( (float(params["injection_rate"])*100))
    
    inputFiles.append(inputFileName)
    fp = open(inputFileName,"w")

    #then give the changing values        
    parameters = "" #initializing
    
#    for k,v in defaultParameters.items():
#        parameters += "{} = {};\n".format(k, str(v))
#    
    for k,v in sorted(params.items()):
        parameters += "{} = {};\n".format(k, str(v))
    
    fp.write(parameters)
    
    fp.close()


    #run the simulation. Save the result to a specific output file.
    outputFileName = inputFileName  + ".out"    #inputFilename already contains the outputFolderName2 as part of its name
    outputFiles.append(outputFileName)
    
    if os.path.exists(outputFileName) == False:
        command = exe_name + " " + inputFileName + " > " + outputFileName
        print(command)
        
        #******************************************************************
        os.system(command)
        #******************************************************************
        
    else:
        print("skipping simulation for " + inputFileName + " as it was already done at last round ...")
    
    #simulation done. Now open the output files, scrap important info, present in a final output file.
    summaryResultFileName = topLevelOutputFolderName + "/" + secondLevelFolderName + "/" + thirdLevelFolderName + "/" +  "{}_{}_Q_{}.summary.txt".format(params["topology"], params["traffic"], params["slimflyQ"])

    fp2 = open(summaryResultFileName,"a")
    
    if count == 1:
        parameters = ""
#        for k,v in defaultParameters.items():
#            parameters += "{} = {};\n".format(k, str(v))
#    
        for k,v in sorted(params.items()):
            if k != "injection_rate":    
                parameters += "{} = {};\n".format(k, str(v))
        fp2.write(parameters)
        fp2.write("\n")
        fp2.write("defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?\n")

    for outputFile in outputFiles:
        fp = open(outputFile,"r")
        text = fp.readlines()
    
        line1 = ""
        line2 = ""
        line3 = ""
        iRate = 0.0
        aRate = 0.0
        latency = 0.0
        deadlock = False
        flag1 = False
        flag2 = False
        flag3 = False
        for line in text[::-1]:
            if "Accepted flit rate average" in line and flag1 is False:
                line1 = line
                flag1 = True
            if "Injected flit rate average" in line and flag2 is False:
                line2 = line
                flag2 = True
            if "Flit latency average" in line and flag3 is False:
                line3 = line
                flag3 = True  
        if line1 != "":
            iRate = float(line1[line1.find("=")+2 : line1.find("(")])
        if line2 != "":
            aRate = float(line2[line2.find("=")+2 : line2.find("(")])
        if line3 != "":
            latency = float(line3[line3.find("=")+2 : line3.find("(")])        
        
        for line in text[::-1]:
            if "Simulation unstable, ending " in line:
                deadlock = True
                break
            if "WARNING: Possible network deadlock" in line:
                deadlock = True
                break
            if "At least one router queue full" in line:
                deadlock = True
                break
            
        result = "simulation,{:>10},{:>10.5},{:>10.5},{:>10.5},{:>10.5},{:>10}\n".format(count,params["injection_rate"],iRate,aRate,latency,deadlock)
         
        fp2.write(result)

    fp2.close()

    #cleanup. Remove the input files.
    for file in inputFiles:
        os.remove(file)
    
    return deadlock
    
    pass    




def probeDeeperAndDeeperBinarySearch(params, lowLim = 1, highLim = 99):
    #no global. bad bad global.
        
    #setSecondLevelFolderName(a = params["dfm_a"], h = params["dfm_h"], g = params["dfm_g"], p = params["dfm_p"], traffic = params["traffic"], routing = params["routing_function"])
    setSecondLevelFolderName(params)
    
    #setThirdLevelFolderName(connectionPattern = params["dfm_interG_topology"])
    setThirdLevelFolderName(params)
    
    low = lowLim
    high = highLim
    
    #print("simulating for i-rate: {0:.2f} ".format(low * 0.01))
    count = 1
    params["injection_rate"] = "{:.2f}".format(low * 0.01)
    satLow = launchSimulation(params, count) 
    
    count = 2
    #print("simulating for i-rate: {0:.2f} ".format(high * 0.01))
    params["injection_rate"] = "{:.2f}".format(high * 0.01)
    satHigh = launchSimulation(params, count)
    
    if satLow == True or satHigh == False:
        sys.exit("Incorrect low and high i_rate settings. Exiting.")    
    
    while( (high - low) > 1):    
        mid = math.floor((low + high)/2)
        count += 1
        params["injection_rate"] = "{:.2f}".format(mid * 0.01)
        #print("simulating for i-rate: {0:.2f} ".format(mid * 0.01))
        satMid = launchSimulation(params, count)
        if satMid is True:
            high = mid
        else:
            low = mid
                
    pass

if __name__ == "__main__":
    print("Hello World!")
    
    '''
    List of params:
        slimflyQ
        
        routing_function 
                [MIN, VLB, UGAL_L, Weighted_VLB, "Weighted_UGAL_L", "Cplex_Weighted_VLB", "Cplex_Weighted_UGAL_L"]
        
        traffic:
                [uniform, randperm, slimfly_worstcase, slimfly_router_perm]
        seed
        traffic_seed    [for slimfly_worstcase traffic only]
        perm_seed [      for randperm and slimfly_router_perm]
        
        injection_rate
        
        cplexSettingsFile   [only for Cplex related routings]
        worstCaseTrafficFile  [for slimfly_worstcase]
        
        probMultiplierA
        probMultiplierB
        probMultiplierC
        probMultiplierD
        probMultiplierBase [same as probMultiplierE]
    '''
    
    #put the defaults
    params =            {
                        "slimflyQ"          :   5,     
                        "seed"              :   0,
                        "routing_function"  :   "",
                        "traffic"           :   "slimfly_router_perm",
                        "seed"              :   2,
                        "traffic_seed"      :   0,
                        "perm_seed"         :   0,
                        "probMultiplierA"   :   1,
                        "probMultiplierB"   :   1,
                        "probMultiplierC"   :   1,
                        "probMultiplierD"   :   1,
                        "probMultiplierBase"   :   1,
                        }

    exe_name = "../../booksim_sw_perm_traffic_1"    #global
    
    topLevelOutputFolderName = "Q_5"
    os.makedirs(topLevelOutputFolderName, exist_ok= True)

    #parameters we want to loop over with
    #seeds = [1228, 4603, 8101, 3937]
    #perm_seeds = [1228, 4603, 8101, 3937]  #should be same as traffic_seeds
    seeds = [2]
    perm_seeds = [2]
    routing_functions = ["VLB", "UGAL_L", "Weighted_VLB", "Weighted_UGAL_L"]

    probMultiplierDict = {}
    probMultiplierDict["VLB"] = [ [1,1,1,1,1] ]
    probMultiplierDict["UGAL_L"] = [ [1,1,1,1,1] ]
    probMultiplierDict["Weighted_VLB"] = [ [1.5,1.5,2,1,1] ]
    probMultiplierDict["Weighted_UGAL_L"] = [ [1.5,1.5,2,1,1] ]
    
    processes = []
    
#    for dfm_interG_topology in dfm_interG_topologys:
#        params["dfm_interG_topology"] = dfm_interG_topology
#        p = MP.Process( target = probeDeeperAndDeeperBinarySearch, args = (params,))
#        processes.append(p)
#        p.start()
#        print("process started with dfm_interG_topology: {}  name: {} pid: {}".format(params["dfm_interG_topology"], p.name,p.pid))

    for seed,perm_seed in zip(seeds,perm_seeds):
        for routing_function in routing_functions:
            for probMultipliers in probMultiplierDict[routing_function]:
                params_loc = {}
                
                for k,v in defaultParameters.items():
                    params_loc[k] = v
                    
                for k,v in params.items():
                    params_loc[k] = v
                
                params_loc["seed"] = seed
                params_loc["perm_seed"] = perm_seed
                params_loc["routing_function"] = routing_function
                a,b,c,d,e = probMultipliers
                params_loc["probMultiplierA"] = float(a)
                params_loc["probMultiplierB"] = float(b)
                params_loc["probMultiplierC"] = float(c)
                params_loc["probMultiplierD"] = float(d)
                params_loc["probMultiplierBase"] = float(e)

                p = MP.Process( target = probeDeeperAndDeeperBinarySearch, args = (params_loc,))
                processes.append(p)
                p.start()
                print("process started with q: {} traffic: {} seed: {} perm_seed: {} routing: {} probMultipliers: {} name: {} pid: {} ".format(params_loc["slimflyQ"], params_loc["traffic"], params_loc["seed"], params_loc["perm_seed"], params_loc["routing_function"], "_".join(str(x) for x in probMultipliers), p.name, p.pid))
                
    #wait for all processes to finish
    #this means all the processes with a same traffic seed will finish before the next t_seed kicks in
    for p in processes:
        p.join()
    print("------ All process ended.")
    
    print("The world never says hello back.")
    
