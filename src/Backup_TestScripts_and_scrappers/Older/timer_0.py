'''
A very simple python script that runs a booksim instance with a specific config file
and measures the time it took to run.

Will get to a full-fledged testscript later.
'''

import os
import time 

def time_test(config_filename, output_filename):
    #config_filename = "../examples/dragonflyfull" 
    #output_filename = "test1.txt"

    command = "../booksim {} > {}".format(config_filename, output_filename)

    start_time = time.time()
    os.system(command)
    end_time = time.time()

    print("total time: ", end_time - start_time)
    
    
if __name__ == "__main__":
    config_filename = "dragonflyfull_0" 
    output_filename = "test0.txt"
    time_test(config_filename, output_filename)
