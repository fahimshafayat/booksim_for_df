wait_for_tail_credit = 0;
vc_allocator = islip;
sw_allocator = islip;
alloc_iters = 1;
credit_delay = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 8192;
latency_thres = 512.0;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = one_vs_two;
vc_allocation_mode = optimum;
vc_buf_size = 64;
df_g = 9;
df_wc_seed = 50;
num_vcs = 2;
routing_function = min;
seed = 1290;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,      0.01,  0.010037,  0.010035,    38.069,    38.071,         0,      3.7517
simulation,         2,      0.04,  0.040086,  0.040085,    38.264,    38.265,         0,      3.7504
simulation,         3,      0.07,  0.070025,  0.070021,    38.678,     38.68,         0,      3.7491
simulation,         4,       0.1,  0.099942,  0.099938,    40.063,    40.064,         0,      3.7498
simulation,         5,      0.13,   0.12408,    0.1281,    1016.6,    957.56,         1,         0.0
