wait_for_tail_credit = 0;
vc_allocator = islip;
sw_allocator = islip;
alloc_iters = 1;
credit_delay = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 8192;
latency_thres = 512.0;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
internal_speedup = 2.0;
local_latency = 1;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = one_vs_two;
vc_allocation_mode = optimum;
vc_buf_size = 16;
df_g = 17;
num_vcs = 4;
routing_function = UGAL_L;
seed = 1290;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,      0.01,  0.010032,  0.010032,    13.337,    13.337,         0,      3.7732
simulation,         2,      0.04,  0.040048,   0.04005,    14.339,     14.34,         0,      4.0849
simulation,         3,      0.07,  0.070089,   0.07009,    14.851,    14.852,         0,      4.2268
simulation,         4,       0.1,   0.10013,   0.10013,    15.062,    15.062,         0,      4.2672
simulation,         5,      0.13,    0.1302,    0.1302,    15.152,    15.153,         0,      4.2652
simulation,         6,      0.16,   0.16023,   0.16023,    15.186,    15.187,         0,      4.2438
simulation,         7,      0.19,   0.19012,   0.19012,    15.204,    15.204,         0,      4.2143
simulation,         8,      0.22,   0.22016,   0.22016,    15.229,     15.23,         0,      4.1856
simulation,         9,      0.25,   0.25016,   0.25016,     15.28,    15.281,         0,      4.1614
simulation,        10,      0.28,   0.28008,   0.28007,     15.34,    15.341,         0,      4.1369
simulation,        11,      0.31,   0.31016,   0.31016,    15.419,     15.42,         0,      4.1138
simulation,        12,      0.34,   0.34005,   0.34005,    15.514,    15.514,         0,      4.0917
simulation,        13,      0.37,   0.37001,   0.37001,    15.632,    15.632,         0,      4.0711
simulation,        14,       0.4,   0.39998,   0.39998,    15.775,    15.776,         0,      4.0517
simulation,        15,      0.43,   0.42996,   0.42996,     15.95,    15.951,         0,      4.0343
simulation,        16,      0.46,   0.45995,   0.45995,    16.152,    16.153,         0,      4.0164
simulation,        17,      0.49,   0.49003,   0.49002,    16.393,    16.394,         0,      3.9989
simulation,        18,      0.52,   0.52002,   0.52002,     16.69,    16.691,         0,       3.982
simulation,        19,      0.55,   0.54995,   0.54994,    17.048,    17.049,         0,      3.9673
simulation,        20,      0.58,   0.57993,   0.57994,    17.497,    17.498,         0,       3.953
simulation,        21,      0.61,   0.60996,   0.60996,    18.038,    18.039,         0,      3.9375
simulation,        22,      0.64,   0.63999,   0.63999,    18.775,    18.776,         0,      3.9254
simulation,        23,      0.67,   0.67004,   0.67003,    19.739,     19.74,         0,      3.9127
simulation,        24,       0.7,   0.70003,   0.70003,    21.127,    21.128,         0,      3.9015
simulation,        25,      0.73,   0.73001,   0.73001,    23.313,    23.314,         0,      3.8929
simulation,        26,      0.76,   0.76003,   0.76003,    27.969,    27.971,         0,      3.8801
simulation,        27,      0.79,   0.75335,   0.75333,     940.4,    99.911,         1,         0.0
