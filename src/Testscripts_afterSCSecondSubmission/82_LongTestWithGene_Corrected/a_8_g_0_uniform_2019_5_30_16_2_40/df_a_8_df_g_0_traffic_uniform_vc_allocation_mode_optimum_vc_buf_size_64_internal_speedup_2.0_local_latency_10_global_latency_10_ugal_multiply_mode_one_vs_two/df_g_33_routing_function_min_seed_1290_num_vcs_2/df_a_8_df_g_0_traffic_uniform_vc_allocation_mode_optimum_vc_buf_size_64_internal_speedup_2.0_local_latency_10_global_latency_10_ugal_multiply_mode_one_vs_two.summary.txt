wait_for_tail_credit = 0;
vc_allocator = islip;
sw_allocator = islip;
alloc_iters = 1;
credit_delay = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 8192;
latency_thres = 512.0;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = one_vs_two;
vc_allocation_mode = optimum;
vc_buf_size = 64;
df_g = 33;
num_vcs = 2;
routing_function = min;
seed = 1290;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,      0.01, 0.0099998, 0.0099985,    37.346,    37.347,         0,      3.6941
simulation,         2,      0.04,  0.039965,  0.039963,    37.377,    37.379,         0,      3.6925
simulation,         3,      0.07,  0.069935,  0.069931,    37.441,    37.443,         0,      3.6931
simulation,         4,       0.1,  0.099948,  0.099943,    37.501,    37.502,         0,      3.6929
simulation,         5,      0.13,    0.1299,   0.12989,    37.567,    37.569,         0,      3.6928
simulation,         6,      0.16,   0.15987,   0.15986,    37.651,    37.653,         0,      3.6935
simulation,         7,      0.19,   0.18986,   0.18986,    37.731,    37.733,         0,      3.6933
simulation,         8,      0.22,   0.21984,   0.21983,    37.823,    37.825,         0,      3.6934
simulation,         9,      0.25,   0.24989,   0.24989,    37.925,    37.927,         0,      3.6935
simulation,        10,      0.28,   0.27991,   0.27991,    38.036,    38.038,         0,      3.6934
simulation,        11,      0.31,   0.30994,   0.30994,     38.16,    38.162,         0,      3.6931
simulation,        12,      0.34,   0.33989,   0.33989,    38.303,    38.305,         0,      3.6931
simulation,        13,      0.37,   0.36985,   0.36984,    38.468,     38.47,         0,      3.6933
simulation,        14,       0.4,   0.39989,   0.39989,     38.65,    38.652,         0,       3.693
simulation,        15,      0.43,   0.42987,   0.42986,    38.868,     38.87,         0,      3.6932
simulation,        16,      0.46,   0.45989,   0.45989,    39.111,    39.113,         0,      3.6928
simulation,        17,      0.49,    0.4899,    0.4899,    39.414,    39.416,         0,      3.6932
simulation,        18,      0.52,   0.51985,   0.51985,    39.769,    39.771,         0,      3.6931
simulation,        19,      0.55,   0.54984,   0.54983,    40.208,     40.21,         0,      3.6932
simulation,        20,      0.58,   0.57981,   0.57981,    40.757,    40.759,         0,       3.693
simulation,        21,      0.61,   0.60986,   0.60986,    41.478,     41.48,         0,      3.6932
simulation,        22,      0.64,   0.63988,   0.63987,    42.443,    42.446,         0,      3.6931
simulation,        23,      0.67,   0.66987,   0.66987,    43.817,     43.82,         0,      3.6931
simulation,        24,       0.7,   0.69993,   0.69993,    45.989,    45.992,         0,      3.6932
simulation,        25,      0.73,   0.72999,   0.72999,     49.91,    49.913,         0,      3.6934
simulation,        26,      0.76,      0.76,   0.76001,    59.959,    59.966,         0,      3.6931
simulation,        27,      0.79,   0.77394,   0.77426,    622.88,    246.96,         1,         0.0
