wait_for_tail_credit = 0;
vc_allocator = islip;
sw_allocator = islip;
alloc_iters = 1;
credit_delay = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 8192;
latency_thres = 512.0;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = one_vs_two;
vc_allocation_mode = optimum;
vc_buf_size = 64;
df_g = 9;
num_vcs = 4;
routing_function = vlb;
seed = 1290;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,      0.01,  0.010036,  0.010035,    66.058,    66.075,         0,      6.0842
simulation,         2,      0.04,  0.040085,  0.040085,    66.248,    66.255,         0,      6.0868
simulation,         3,      0.07,  0.070021,  0.070021,    66.389,    66.397,         0,      6.0839
simulation,         4,       0.1,  0.099944,  0.099938,    66.591,    66.601,         0,      6.0837
simulation,         5,      0.13,   0.12997,   0.12996,     66.82,     66.83,         0,      6.0834
simulation,         6,      0.16,   0.16006,   0.16005,    67.108,    67.118,         0,      6.0846
simulation,         7,      0.19,   0.19002,   0.19001,    67.415,    67.424,         0,      6.0832
simulation,         8,      0.22,   0.22008,   0.22008,    67.826,    67.835,         0,      6.0856
simulation,         9,      0.25,   0.25018,   0.25016,    68.261,    68.271,         0,      6.0835
simulation,        10,      0.28,   0.28019,   0.28016,    68.829,    68.838,         0,      6.0834
simulation,        11,      0.31,   0.31024,   0.31022,    69.537,    69.547,         0,      6.0826
simulation,        12,      0.34,   0.34019,   0.34018,    70.452,    70.462,         0,      6.0825
simulation,        13,      0.37,   0.37018,   0.37015,    71.704,    71.712,         0,      6.0819
simulation,        14,       0.4,    0.4002,   0.40019,    73.517,    73.524,         0,      6.0834
simulation,        15,      0.43,   0.43014,   0.43012,    76.271,    76.276,         0,      6.0833
simulation,        16,      0.46,   0.46006,   0.46003,    81.103,    81.105,         0,      6.0814
simulation,        17,      0.49,   0.49008,   0.49002,    92.464,    92.465,         0,      6.0835
simulation,        18,      0.52,   0.52011,   0.52001,    167.94,    167.99,         0,      6.0834
simulation,        19,      0.55,   0.51695,   0.53816,    544.52,    465.99,         1,         0.0
