wait_for_tail_credit = 0;
vc_allocator = islip;
sw_allocator = islip;
alloc_iters = 1;
credit_delay = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 8192;
latency_thres = 512.0;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 10;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = one_vs_two;
vc_allocation_mode = optimum;
vc_buf_size = 64;
df_g = 9;
num_vcs = 5;
routing_function = PAR;
seed = 1290;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,      0.01,  0.010038,  0.010035,    45.898,    45.927,         0,       4.406
simulation,         2,      0.04,  0.040086,  0.040085,    47.936,    47.952,         0,      4.5687
simulation,         3,      0.07,  0.070026,  0.070021,    45.916,    45.934,         0,       4.394
simulation,         4,       0.1,  0.099938,  0.099938,    44.461,    44.475,         0,      4.2665
simulation,         5,      0.13,   0.12997,   0.12996,    43.387,    43.399,         0,      4.1708
simulation,         6,      0.16,   0.16006,   0.16005,    42.456,     42.47,         0,      4.0866
simulation,         7,      0.19,   0.19002,   0.19001,    41.767,     41.78,         0,      4.0225
simulation,         8,      0.22,   0.22009,   0.22008,    41.171,    41.183,         0,      3.9652
simulation,         9,      0.25,   0.25017,   0.25016,    40.656,    40.665,         0,      3.9148
simulation,        10,      0.28,   0.28018,   0.28016,    40.285,    40.295,         0,      3.8754
simulation,        11,      0.31,   0.31024,   0.31022,    39.953,    39.962,         0,       3.839
simulation,        12,      0.34,   0.34018,   0.34018,    39.717,    39.726,         0,      3.8093
simulation,        13,      0.37,   0.37016,   0.37015,    39.502,    39.511,         0,      3.7807
simulation,        14,       0.4,   0.40019,   0.40019,    39.361,    39.368,         0,      3.7569
simulation,        15,      0.43,   0.43013,   0.43012,    39.254,     39.26,         0,      3.7349
simulation,        16,      0.46,   0.46003,   0.46003,    39.207,    39.214,         0,      3.7161
simulation,        17,      0.49,   0.49004,   0.49002,    39.183,     39.19,         0,      3.6977
simulation,        18,      0.52,   0.52001,   0.52001,    39.235,    39.241,         0,      3.6829
simulation,        19,      0.55,   0.54997,   0.54997,    39.328,    39.333,         0,      3.6696
simulation,        20,      0.58,   0.57993,   0.57991,    39.467,    39.473,         0,      3.6565
simulation,        21,      0.61,   0.60983,   0.60982,    39.669,    39.674,         0,      3.6448
simulation,        22,      0.64,   0.63977,   0.63975,    39.956,    39.962,         0,      3.6347
simulation,        23,      0.67,   0.66984,   0.66984,    40.332,    40.337,         0,      3.6261
simulation,        24,       0.7,   0.69998,   0.69996,    40.802,    40.806,         0,      3.6172
simulation,        25,      0.73,   0.72999,   0.72994,     41.46,    41.464,         0,      3.6113
simulation,        26,      0.76,   0.75996,   0.75993,    42.323,    42.328,         0,      3.6056
simulation,        27,      0.79,   0.79009,   0.79007,      43.5,    43.505,         0,      3.6016
simulation,        28,      0.82,   0.82013,   0.82009,    45.103,    45.108,         0,      3.5983
simulation,        29,      0.85,   0.85003,      0.85,    47.484,    47.486,         0,      3.5978
simulation,        30,      0.88,   0.88011,   0.88006,    51.308,    51.309,         0,      3.6003
simulation,        31,      0.91,      0.91,   0.90999,     58.51,    58.516,         0,      3.6121
simulation,        32,      0.94,   0.93995,    0.9399,    80.633,    80.652,         0,      3.6581
simulation,        33,      0.97,   0.90282,   0.90287,    1310.7,     378.8,         1,         0.0
