wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 4;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
internal_speedup = 4.0;
local_latency = 1;
num_vcs = 8;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = incremental;
vc_buf_size = 64;
routing_function = UGAL_G;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,      0.05,  0.050009,  0.050008,     11.28,     11.28,         0,      4.5461
simulation,         2,      0.15,   0.14992,   0.14991,    11.994,    11.994,         0,      4.5967
simulation,         3,      0.25,   0.25019,   0.25017,    13.152,    13.152,         0,      4.6413
simulation,         4,      0.35,   0.34994,   0.34993,    15.888,    15.889,         0,      4.8312
simulation,         5,      0.45,   0.44982,   0.44982,     20.31,    20.311,         0,      5.0312
