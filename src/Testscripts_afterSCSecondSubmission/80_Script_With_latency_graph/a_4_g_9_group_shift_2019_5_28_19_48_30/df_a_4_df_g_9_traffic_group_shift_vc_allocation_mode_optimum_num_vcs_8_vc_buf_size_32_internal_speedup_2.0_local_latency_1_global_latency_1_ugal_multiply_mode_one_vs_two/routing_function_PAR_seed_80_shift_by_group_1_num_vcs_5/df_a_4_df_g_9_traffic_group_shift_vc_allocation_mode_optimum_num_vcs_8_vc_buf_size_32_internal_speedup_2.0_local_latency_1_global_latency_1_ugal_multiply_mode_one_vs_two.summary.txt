wait_for_tail_credit = 0;
vc_allocator = islip;
sw_allocator = islip;
alloc_iters = 1;
credit_delay = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 1024;
latency_thres = 512.0;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 4;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
internal_speedup = 2.0;
local_latency = 1;
num_vcs = 8;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = one_vs_two;
vc_allocation_mode = optimum;
vc_buf_size = 32;
num_vcs = 5;
routing_function = PAR;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,      0.05,  0.050071,  0.050008,    17.401,     17.41,         0,      5.0598
simulation,         2,       0.1,   0.10017,    0.1001,    17.916,     17.92,         0,      5.1346
simulation,         3,      0.15,   0.15012,   0.15008,    18.394,    18.404,         0,      5.1569
