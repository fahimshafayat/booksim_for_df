wait_for_tail_credit = 0;
vc_allocator = islip;
sw_allocator = islip;
alloc_iters = 1;
credit_delay = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 8192;
latency_thres = 512.0;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 4;
df_arrangement = absolute_improved;
df_g = 0;
global_latency = 1;
internal_speedup = 2.0;
local_latency = 1;
topology = dragonflyfull;
traffic = df_wc;
ugal_multiply_mode = one_vs_two;
vc_allocation_mode = optimum;
vc_buf_size = 16;
df_g = 9;
df_wc_seed = 50;
num_vcs = 5;
routing_function = PAR;
seed = 1290;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,      0.01, 0.0098798,  0.009882,    15.283,    15.284,         0,      4.4141
simulation,         2,      0.04,  0.039819,  0.039823,    17.238,    17.239,         0,      5.0216
simulation,         3,      0.07,   0.06975,  0.069748,    17.683,    17.685,         0,      5.1208
simulation,         4,       0.1,   0.10009,   0.10008,    17.943,    17.945,         0,      5.1424
simulation,         5,      0.13,   0.13021,    0.1302,    18.216,    18.216,         0,      5.1529
simulation,         6,      0.16,   0.16039,   0.16038,    18.553,    18.554,         0,      5.1632
simulation,         7,      0.19,   0.19062,   0.19063,    18.984,    18.985,         0,      5.1736
