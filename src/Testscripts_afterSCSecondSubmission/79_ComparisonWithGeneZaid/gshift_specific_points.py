import os
import multiprocessing as MP
import math
import sys
import datetime
import pickle
import time
import seaborn as sns
import matplotlib.pyplot as plt


#for multiprocessing, each process gets its own copy of globals in its own memory pool,
#so using global is safe. It might be considered bad design though.

topLevelOutputFolderName = "" #set downunder
secondLevelFolderName = ""    #changed downunder
thirdLevelFolderName = ""    #changed downunder

parameters_default = {
                    "wait_for_tail_credit"      :   0,
                    "vc_allocator"              :   "separable_input_first", 
                    "sw_allocator"              :   "separable_input_first",
                    "alloc_iters"               :   1,
                    "credit_delay"              :   2,
                    "routing_delay"             :   0,
                    "vc_alloc_delay"            :   1,
                    "sw_alloc_delay"            :   1,
                    "st_final_delay"            :   1,
                    "input_speedup"             :   1,
                    "output_speedup"            :   1,
                    "warmup_periods"            :   3,
                    "sim_count"                 :   1,
                    "sample_period"             :   10000,  
                    "priority"                  :   "none",
                    "packet_size"               :   1,
                    "injection_rate_uses_flits" :   1,
                    }


#may be change to params dict
def setSecondLevelFolderName(params):
    global secondLevelFolderName
    
    #secondLevelFolderName = "Q_{}_seed_{}_traffic_{}_tSeed_{}_permSeed_{}".format(params["slimflyQ"], params["seed"],params["traffic"], params["traffic_seed"], params["perm_seed"])
    for k,v in params.items():
        if (k == "topology") or (k == "df_arrangement"):
            continue 
        secondLevelFolderName += k + "_" + str(v) + "_" 
    
    secondLevelFolderName = secondLevelFolderName[:len(secondLevelFolderName)-1] #to chop off the trailing underscore
    
    pass
    

#change to params dict
def setThirdLevelFolderName(params):
    global thirdLevelFolderName
    
    for k,v in params.items():
        thirdLevelFolderName += k + "_" + str(v) + "_" 
    
    thirdLevelFolderName = thirdLevelFolderName[:len(thirdLevelFolderName)-1] #to chop off the trailing underscore
    
    pass

def write_inputfile(parameters_default, second_level_params, third_level_params, injection_rate, inputFileName):
    fp = open(inputFileName,"w")

    #then give the changing values        
    parameters = "" #initializing
    
    for k,v in parameters_default.items():
        parameters += "{} = {};\n".format(k, str(v))
    
    for k,v in second_level_params.items():
        parameters += "{} = {};\n".format(k, str(v))
    
    for k,v in third_level_params.items():
        parameters += "{} = {};\n".format(k, str(v))
    
    parameters += "injection_rate = {:.2f};\n".format(injection_rate)
    
    fp.write(parameters)
    
    fp.close()

    pass

def write_header_info_in_summary_file(parameters_default, second_level_params, third_level_params):
    parameters = ""
    for k,v in parameters_default.items():
        parameters += "{} = {};\n".format(k, str(v))

    for k,v in sorted(second_level_params.items()):
        parameters += "{} = {};\n".format(k, str(v))
    for k,v in sorted(third_level_params.items()):
        parameters += "{} = {};\n".format(k, str(v))
    parameters += "\n"
    parameters += "defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length\n"
    
    stat_headers = ""
    stat_headers += "total_flits, total_min_path_flits, total_vlb_path_flits,"
    stat_headers += "i_rate_mid, min_path_dist_in_group_for_mid, vlb_path_dist_in_group_for_mid, min_path_dist_out_group_for_mid, vlb_path_dist_out_group, _for_mid,"
    stat_headers += "i_rate_sat, min_path_dist_in_group_for_sat, vlb_path_dist_in_group_for_sat, min_path_dist_out_group_for_sat, vlb_path_dist_out_group_for_sat\n"

    return parameters, stat_headers

    pass



def extract_simulation_output_from_file(filename, count, injection_rate):
    
    results = {}
    
    basic_results = collect_basic_result_data(filename, count, injection_rate)
    
    for k,v in basic_results.items():
        results[k] = v
    
    stats_for_text, stats_for_pickle = collect_custom_stats_data(filename, count, injection_rate)
    
    for k,v in stats_for_text.items():
        results[k] = v
    
    deadlock = results["deadlock"]
    
    pathlen_stat = collect_min_vs_vlb_comparison_stats(filename, count, injection_rate)
    
    #result = "simulation,{:>10},{:>10.5},{:>10.5},{:>10.5},{:>10.5},{:>10.5},{:>10},  {},  {},  {},  {},  {},  {}\n".format(count,injection_rate,iRate,aRate,p_latency,f_latency,deadlock,flits_through_min,flits_through_non_min, r_tier_dist_when_min_taken, r_tier_dist_when_non_min_taken, non_min_pathlen_dist_when_min_taken, non_min_pathlen_dist_when_non_min_taken)
    
    results_basic = "simulation,{:>10},{:>10.5},{:>10.5},{:>10.5},{:>10.5},{:>10.5},{:>10},  {:>10.5}\n".format( count, injection_rate,results["iRate"],results["aRate"],results["p_latency"] ,results["f_latency"],results["deadlock"],results["hops_average"])
    
    results_stats =  "simulation,{}, {}, {}, {}, {}, {}, {},  {}, {}, {}, {}, {}, {}, {}, {} \n".format(count, injection_rate,results["iRate"],results["aRate"],results["p_latency"],results["f_latency"],results["deadlock"],results["hops_average"], results["total_flits"], results["flits_through_min"], results["flits_through_non_min"], results["min_paths_dist_in_group_traffic"], results["min_paths_dist_out_group_traffic"], results["vlb_paths_dist_in_group_traffic"], results["vlb_paths_dist_out_group_traffic"])

    
    return deadlock, results_basic, results_stats, pathlen_stat

    pass



def collect_basic_result_data(outputFile, count, injection_rate):
    results = {}
    
    fp = open(outputFile,"r")
    
    text = fp.readlines()

    saved_lines = {}
    
    iRate = 0.0
    flag_iRate = False
    
    aRate = 0.0
    flag_aRate = False
    
    p_latency = 0.0
    flag_p_latency = False
    
    f_latency = 0.0
    flag_f_latency = False
    
    hops_average = 0.0
    flag_hops_average = False
    
    deadlock = False
    
     #extract deafutl booksim results values
    for line in text[::-1]:
        if "Accepted flit rate average" in line and flag_iRate is False:
            saved_lines["iRate"] = line
            flag_iRate = True
        
        if "Injected flit rate average" in line and flag_aRate is False:
            saved_lines["aRate"] = line
            flag_aRate = True
            
        if "Flit latency average" in line and flag_f_latency is False:
            saved_lines["f_latency"] = line
            flag_f_latency = True
        
        if "Packet latency average" in line and flag_p_latency is False:
            saved_lines["p_latency"] = line
            flag_p_latency = True
        
        if "Hops average" in line and flag_hops_average is False:
            saved_lines["hops_average"] = line
            flag_hops_average = True
            
        if flag_aRate and flag_iRate and flag_f_latency and flag_p_latency and flag_hops_average:
            break
            
    if "iRate" in saved_lines:
        line = saved_lines["iRate"]
        iRate = float(line[line.find("=")+2 : line.find("(")])
        
    if "aRate" in saved_lines:
        line = saved_lines["aRate"]
        aRate = float(line[line.find("=")+2 : line.find("(")])
        
    if "f_latency" in saved_lines:
        line = saved_lines["f_latency"]
        f_latency = float(line[line.find("=")+2 : line.find("(")])
        
    if "p_latency" in saved_lines:
        line = saved_lines["p_latency"]
        p_latency = float(line[line.find("=")+2 : line.find("(")])
        
    if "hops_average" in saved_lines:
        line = saved_lines["hops_average"]
        hops_average = float(line[line.find("=")+2 : line.find("(")])
        
    
    #figure out if deadlock happened or not
    for line in text[::-1]:
        if "Simulation unstable, ending " in line:
            deadlock = True
            break
        if "WARNING: Possible network deadlock" in line:
            deadlock = True
            break
        if "At least one router queue full" in line:
            deadlock = True
            break
        
    #the values may or may not be in the results file. If not, default values will be loaded.
    results["iRate"] = iRate
    results["aRate"] = aRate
    results["f_latency"] = f_latency
    results["p_latency"] = p_latency
    results["hops_average"] = hops_average
    results["deadlock"] = deadlock
    
    fp.close()
    
    return results
    
    pass

def collect_custom_stats_data(outputFile, count, injection_rate):
    results_for_text = {}
    results_for_pickle = {}
    
    fp = open(outputFile,"r")
    
    text = fp.readlines()
    
    field_names = ["total_flits", "flits_through_min", "flits_through_non_min",  "min_paths_dist_in_group_traffic", "min_paths_dist_out_group_traffic", "vlb_paths_dist_in_group_traffic", "vlb_paths_dist_out_group_traffic" ]
    
    int_fields = set(["total_flits", "flits_through_min", "flits_through_non_min"])
    
    fields_to_broken_up = set(["min_paths_dist_in_group_traffic", "min_paths_dist_out_group_traffic", "vlb_paths_dist_in_group_traffic", "vlb_paths_dist_out_group_traffic"])
    
    default_values = [0 if field in int_fields else "" for field in field_names]
    
    search_text = ["Total Flits:",
                   "Total flits through min paths:",
                   "Total flits through non-min paths:",
                   "Min paths in group traffic:",
                   "Min paths out group traffic:",
                   "Vlb paths in group traffic:",
                   "Vlb paths out group traffic:"]

    #first build the results_for_text    
    for idx, field_name in enumerate(field_names):
        results_for_text[field_name] = default_values[idx]
    
    for line in text:
        for idx in range(len(field_names)):
            if line.startswith(search_text[idx]):
                val = line[line.find(":") + 1 : ]
                if field_names[idx] in int_fields:
                    results_for_text[field_names[idx]] = int(val)
                else:
                    results_for_text[field_names[idx]] = val.strip()
    
    #now build results_for_pickle
    for k,v in results_for_text.items():
        if k not in fields_to_broken_up:
            results_for_pickle[k] = v
        else:
            list_of_value_pairs = extract_value_pairs(v)
            results_for_pickle[k] = list_of_value_pairs
    
    
    fp.close()
    
    return results_for_text, results_for_pickle
        
    pass

def collect_min_vs_vlb_comparison_stats(outputFile, count, injection_rate):
    '''
    This will only be included in the pickle. So just return the dict.
    
    String format:
        pathlen pairs: ( 1 , 2 )  : 24136 , 17504 , 6632
        pathlen pairs: ( 2 , 2 )  : 70 , 30 , 40
        pathlen pairs: ( 2 , 3 )  : 878 , 547 , 331
        pathlen pairs: ( 2 , 4 )  : 5554 , 4139 , 1415
        pathlen pairs: ( 2 , 5 )  : 11112 , 9047 , 2065
        pathlen pairs: ( 2 , 6 )  : 8040 , 7061 , 979
        pathlen pairs: ( 3 , 2 )  : 228 , 42 , 186
        pathlen pairs: ( 3 , 3 )  : 3458 , 1550 , 1908
        pathlen pairs: ( 3 , 4 )  : 15246 , 8863 , 6383
        pathlen pairs: ( 3 , 5 )  : 26175 , 18884 , 7291
        pathlen pairs: ( 3 , 6 )  : 16542 , 13962 , 2580
    
    output:
        List of:
            (1,2), (24136,17504,6632)
            (2,2), (70,30,40)
            And so on.
            
    The pairs are already sorted in the booksim out file, so just 
    read sequantially and add to the list.
    '''
    
    fp = open(outputFile,"r")
    
    text = fp.readlines()
    
    pathlenpair_vs_freq = []
    
    for line in text:
        if line.startswith("pathlen pairs:"):
            temp = line.split(":")
            
            len_pair = temp[1]
            len_pair_a = int( len_pair[len_pair.find("(") + 1: len_pair.find(",")] )
            len_pair_b = int( len_pair[len_pair.find(",") + 1: len_pair.find(")")] )
            len_pairs = (len_pair_a, len_pair_b)
            
            freqs = temp[2]
            freqs = freqs.split(",")
            freq_triplet = tuple([int(x) for x in freqs])
            
            pathlenpair_vs_freq.append((len_pairs, freq_triplet))
#            
#    for element in pathlenpair_vs_freq:
#        print(element)
            
    return pathlenpair_vs_freq

    pass

def extract_value_pairs(text):
    '''
    Sample input: " | 0:0| 1:0| 2:563| 3:1384| 4:0| 5:0| 6:0| 7:0| 8:0| 9:0"
    Sample output: [ (0, 0) , (1,0) , (2, 1), (3, 4) , (4 , 33) , (5, 60) , (6, 26) , (7, 0) , (8, 0) , (9, 0) ]
    '''
    results = []
    
    text = text.strip()
    
    if text == "":
        return results
    
    splitted = text.split("|")
    
    #drop the first entry. It was created because there is an empty | at the very beginning.
    splitted = splitted[1:]
    
    for element in splitted:
        temp = element.split(":")
        val = int(temp[0])
        freq = int(temp[1])
        results.append((val, freq))
    
    return results
    
    
def create_pickle_for_settings_and_results(parameters_default, second_level_params, third_level_params, outputFile, count, injection_rate):
    
    
    settings_and_results = {}
    
    for k,v in parameters_default.items():
        settings_and_results[k] = v
    for k,v in second_level_params.items():
        settings_and_results[k] = v
    for k,v in third_level_params.items():
        settings_and_results[k] = v
    
    settings_and_results["directed_injection_rate"] = injection_rate
        
    basic_results = collect_basic_result_data(outputFile, count, injection_rate)
    
    for k,v in basic_results.items():
        settings_and_results[k] = v
    
    stats_for_text, stats_for_pickle = collect_custom_stats_data(outputFile, count, injection_rate)
    
    pathlen_stat = collect_min_vs_vlb_comparison_stats(outputFile, count, injection_rate)
    
        
    for k,v in stats_for_pickle.items():
        settings_and_results[k] = v
    
    settings_and_results["pathlen_stat"] = pathlen_stat    
#    print("pickle values:")
#    for k,v in settings_and_results.items():
#        print(k,v)
    
    
    return settings_and_results
    

def launchSimulation(second_level_params, third_level_params, injection_rate, count):
        
    outputFolderName = topLevelOutputFolderName + "/" + secondLevelFolderName+ "/" + thirdLevelFolderName 
    os.makedirs(outputFolderName, exist_ok= True)

    inputFiles = []
    outputFiles = []    
        
    inputFileName = outputFolderName + "/"
    inputFileName += "irate" + "_" + "{:.0f}".format(injection_rate*100)
    
    inputFiles.append(inputFileName)
    
    write_inputfile(parameters_default, second_level_params, third_level_params, injection_rate, inputFileName)

    #run the simulation. Save the result to a specific output file.
    outputFileName = inputFileName  + ".out"    #inputFilename already contains the outputFolderName2 as part of its name
    outputFiles.append(outputFileName)
    
    if os.path.exists(outputFileName) == False:
        command = exe_name + " " + inputFileName + " > " + outputFileName
        #print(command)
        
        #******************************************************************
        os.system(command)
        #******************************************************************
        
    else:
        print("skipping simulation for " + inputFileName + " as it was already done at last round ...")
    
    #simulation done. Now open the output files, scrap important info, present in a final output file.
    
    summaryResultFileName = outputFolderName + "/" + secondLevelFolderName
    statFileName = outputFolderName + "/" + secondLevelFolderName
    
    summaryResultFileName += ".summary.txt"
    statFileName += ".summary.extended.stats"
    
    #print("*********** summaryResultFileName: ", summaryResultFileName)
    
    fp2 = open(summaryResultFileName,"a")
    fp3 = open(statFileName,"a")
    
    if count == 1:
        header_text, stat_header_text = write_header_info_in_summary_file(parameters_default, second_level_params, third_level_params)
        fp2.write(header_text)
        
        fp3.write(header_text)
        fp3.write(stat_header_text)
    
    for outputFile in outputFiles:
        deadlock, results_basic, results_stats, pathlen_stats = extract_simulation_output_from_file(outputFile, count, injection_rate) 
        fp2.write(results_basic)
        fp3.write(results_stats)
        
        results_pickle = create_pickle_for_settings_and_results(parameters_default, second_level_params, third_level_params, outputFile, count, injection_rate)
        
    fp2.close()
    fp3.close()
    
    #cleanup. Remove the input files.
    for file in inputFiles:
        os.remove(file)
    
    return deadlock, results_pickle
    
    pass    

def getSaturationPoint(result_pickle_dict):
    '''
    The value returned will be an int. 
    For example, 82 for 0.82 injection rate.
    '''
    
    isSaturated = {}
    
    for key,dic in result_pickle_dict.items():
        isSaturated[key] = dic["deadlock"]
    
    isSaturated = sorted(isSaturated.items())
    
    old_k = 0
    for k,v in isSaturated:
        if v == True:
            break
        else:
            old_k = k
        
    print("saturation: ", old_k)
    
    return old_k
    
    pass

def writePickleFile(result_pickle_dict, saturationPoint):
    result_pickle_list = [v for (k,v) in sorted(result_pickle_dict.items())]
    
    final_pickle = {}
    
    final_pickle["saturation_point"] = saturationPoint
    final_pickle["results"] = result_pickle_list
    
    pickle_filename = topLevelOutputFolderName + "/" + secondLevelFolderName+ "/" + thirdLevelFolderName + "/" + secondLevelFolderName+ ".pk"
    
    pickle.dump(final_pickle, open(pickle_filename,"wb"))    
#    
#    for element in result_pickle_list:
#        print(element)

def getInterval(saturationPoint):
    if saturationPoint > 90:
        interval = 20
    elif saturationPoint > 60:
        interval = 15
    elif saturationPoint > 30:
        interval = 10
    elif saturationPoint > 10:
        interval = 5
    else:
        interval = 2
    
    points = [interval*ii for ii in range(1, saturationPoint//interval + 1)]
    
    return points
    

def probeDeeperAndDeeperBinarySearch(second_level_params, third_level_params, *, lowLim = 1, highLim = 99, latencyPoints = False):
    #no global. bad bad global.
        
    #setSecondLevelFolderName(a = params["dfm_a"], h = params["dfm_h"], g = params["dfm_g"], p = params["dfm_p"], traffic = params["traffic"], routing = params["routing_function"])
    setSecondLevelFolderName(second_level_params)
    
    #setThirdLevelFolderName(connectionPattern = params["dfm_interG_topology"])
    setThirdLevelFolderName(third_level_params)
    
    result_pickle_dict = {}
    
    low = lowLim
    high = highLim

    count = 0
    while( (high - low) > 1):    
        mid = math.floor((low + high)/2)
        count += 1
        #params["injection_rate"] = "{:.2f}".format(mid * 0.01)
        #i_rate = mid * 0.01
        #print("simulating for i-rate: {0:.2f} ".format(mid * 0.01))
        satMid, result_pickle = launchSimulation(second_level_params, third_level_params, mid*0.01, count)
        result_pickle_dict[mid] = result_pickle
        
        if satMid is True:
            high = mid
        else:
            low = mid
                
    #    #mid was the last one we ran a test on. 
    #    #Because we are now more concerned abour latency, so let's run a few more simulations
    #    #in the non-saturated zone. We'll reduce the i-rate by 0.05 four times.
    #    irate = (mid//5) * 5
    #    count2 = 0
    #    while(irate > 0 and count2 < 4):
    #        count += 1
    #        isSaturated, result_pickle =launchSimulation(second_level_params, third_level_params, irate*0.01, count)
    #        result_pickle_dict[irate] = result_pickle
    #        
    #        irate -= 5
    #        count2 += 1

    saturationPoint = getSaturationPoint(result_pickle_dict)
    
    if latencyPoints:
        new_points = getInterval(saturationPoint)
        print("new points:", new_points)    
        for point in new_points:
            count += 1
            satPoint, result_pickle = launchSimulation(second_level_params, third_level_params, point*0.01, count)
            result_pickle_dict[point] = result_pickle
            
    writePickleFile(result_pickle_dict, saturationPoint)
    
    pass

def runSimulationForSpecificPoints(second_level_params, third_level_params, points):
    #no global. bad bad global.
        
    #setSecondLevelFolderName(a = params["dfm_a"], h = params["dfm_h"], g = params["dfm_g"], p = params["dfm_p"], traffic = params["traffic"], routing = params["routing_function"])
    setSecondLevelFolderName(second_level_params)
    
    #setThirdLevelFolderName(connectionPattern = params["dfm_interG_topology"])
    setThirdLevelFolderName(third_level_params)
    
    result_pickle_dict = {}
    
    count = 0
    print("points to probe:", points)    
    for point in points:
        count += 1
        satPoint, result_pickle = launchSimulation(second_level_params, third_level_params, point, count)
        result_pickle_dict[point] = result_pickle
        
    writePickleFile(result_pickle_dict, 0)

    pass


def probeWithLinearSearch(second_level_params, third_level_params, lowLim = 5, highLim = 95, interval = 5):
    setSecondLevelFolderName(second_level_params)
    
    #setThirdLevelFolderName(connectionPattern = params["dfm_interG_topology"])
    setThirdLevelFolderName(third_level_params)
    
    result_pickle_dict = {}
    count = 0
    
    injections = []
    latencies = []
    
    for point in range(lowLim, highLim+1, interval):
            count += 1
            deadlock, result_pickle = launchSimulation(second_level_params, third_level_params, point*0.01, count)
            result_pickle_dict[point] = result_pickle
            
#            print()
#            #print(count, point, deadlock)
##            for k,v in result_pickle.items():
##                print(k,v)
#            
#            print("injection rate: {:.2f}".format( result_pickle["iRate"]))
#            print("packet latency: {:.2f}".format( result_pickle["p_latency"]))
#            print("deadlock?", result_pickle["deadlock"])
#            print("average hop length: {:.2f}".format( result_pickle["hops_average"]))
            
            if deadlock == True:
                break
            else:
                injections.append(point)
                latencies.append(result_pickle["p_latency"])
        
    #writePickleFile(result_pickle_dict, 0)
    #make graph
    #draw_latency_graph(injections, latencies)

    pass

def draw_latency_graph(injections, latencies):

    #graphFile = outputFolderName + "/" + secondLevelFolderName + "latency.png"
    print("printing graph")

    plt.figure(figsize = (6,4))
    ax = plt.gca()

    plt.plot(injections, latencies, marker = "o", linestyle = "-", markersize = 10)
    
    plt.xlim(0, 100)
    plt.ylim(0, 100)
    
    plt.show()

#    if filename:
#        plt.savefig(graphFile, format = "png", bbox_inches='tight')
#    

if __name__ == "__main__":
    print("Hello World!")
    
    '''
    traffic       = df_wc; //randperm / group_shift / uniform / df_wc
df_wc_seed  = 13;
    '''
    
    #put the defaults
    parameters_specific =            {
                        "topology"          :   "dragonflyfull",     
                        "df_a"              :   4,
                        "df_g"              :   9,
                        "df_arrangement"    :   "absolute_improved",
                        "traffic"           :   "group_shift",
                        "vc_allocation_mode" :  "incremental",
                        "num_vcs"                   :   8,
                        "vc_buf_size"       :   64,
                        "internal_speedup"  :   4.0,
                        "local_latency"     :   1,
                        "global_latency"    :   1,
                        "ugal_multiply_mode" :  "pathlen_based", 
                        #"multitiered_routing_threshold_0"  : 5,
                        }

    exe_name = "../../booksim"    #global
    
    now = datetime.datetime.now()
    
    start_time = time.time()
    
    topLevelOutputFolderName = "a_{}_g_{}_{}_{}_{}_{}_{}_{}_{}".format(parameters_specific["df_a"], parameters_specific["df_g"], parameters_specific["traffic"], now.year, now.month, now.day, now.hour, now.minute, now.second)
    os.makedirs(topLevelOutputFolderName, exist_ok= True)

    #parameters we want to loop over with
    seeds = [80] #(80 - 83 done)
    traffic_seeds = [1] #(1 - 4 done)
    #traffic_seeds = [1,2,25,50]
    
    #basically, we want to cycle though shift_by groups
    #by 1, 2, 25% and 50%. The amount will depend on the 
    #seed selected.
    
    #traffic_seeds = [50]
    #df_wc_seeds = [60]
    #perm_seeds = [2]
    #traffics = ["randperm"]
    
     
    #df_gs = [9,17,25,33]
    #routing_functions = [ "UGAL_L", "UGAL_L_restricted_src_only",  "UGAL_G", "UGAL_G_restricted_src_only", "PAR", "PAR_restricted_src_only"]
    routing_functions = [ "UGAL_L", "UGAL_L_restricted_src_only"]
    
    #latencies = [1, 10, 40, 1015] #1015 is actually 10 and 15. Handle it as a separate case. 
    buffers = [8, 16, 32, 64]
    
    
#    simulate_specific_points = True
#    simulation_points = {"UGAL_L" : [0.03, 0.04, 0.05, 0.06, 0.07], 
#                         "UGAL_L_restricted_src_only": [0.05, 0.08, 0.10, 0.12, 0.13],  
#                         "UGAL_G": [0.10, 0.20, 0.30, 0.40, 0.45], 
#                         "UGAL_G_restricted_src_only": [0.10, 0.20, 0.30, 0.40, 0.49], 
#                         "PAR": [0.10, 0.15, 0.20, 0.25, 0.30], 
#                         "PAR_restricted_src_only": [0.10, 0.20, 0.30, 0.40, 0.45]}
    
    
    
    processes = []
    
    for (seed, traffic_seed) in zip(seeds, traffic_seeds):
        for routing_function in routing_functions:
            second_level_params = {}
            third_level_params = {}
            
            for k,v in parameters_specific.items():
                second_level_params[k] = v
            
            #third_level_params["df_g"] = df_g
            third_level_params["routing_function"] = routing_function
            third_level_params["seed"] = seed
            third_level_params["shift_by_group"] = traffic_seed
            
#            if simulate_specific_points is True:
#                p = MP.Process( target = runSimulationForSpecificPoints, args = (second_level_params, third_level_params, simulation_points[routing_function]))    
#            else:
#                p = MP.Process( target = probeDeeperAndDeeperBinarySearch, args = (second_level_params, third_level_params))
            
            lowLim = 5
            highLim = 95
            interval = 5
            
            p = MP.Process( target = probeWithLinearSearch, args = (second_level_params, third_level_params, lowLim, highLim, interval)) 
            
            processes.append(p)
            p.start()
            
            temp_str = "process started with "
            for k,v in second_level_params.items():
                temp_str += " " + k + " : " + str(v) + " , "
            for k,v in third_level_params.items():
                temp_str += " " + k + " : " + str(v) + " , "
            temp_str += "process_name: {} , pid: {}".format(p.name, p.pid)
            print(temp_str)  
                        
        #wait for all processes to finish
        #this means all the processes with a same traffic seed will finish before the next t_seed kicks in
        for p in processes:
            p.join()
        
            
    end_time = time.time()
    
    print("------ All process ended.")
    
    elapsed = end_time - start_time
    
    print("total time: {} seconds / {:.2f} hours".format(elapsed, elapsed/3600) )
    
    print("The world never says hello back.")
    
