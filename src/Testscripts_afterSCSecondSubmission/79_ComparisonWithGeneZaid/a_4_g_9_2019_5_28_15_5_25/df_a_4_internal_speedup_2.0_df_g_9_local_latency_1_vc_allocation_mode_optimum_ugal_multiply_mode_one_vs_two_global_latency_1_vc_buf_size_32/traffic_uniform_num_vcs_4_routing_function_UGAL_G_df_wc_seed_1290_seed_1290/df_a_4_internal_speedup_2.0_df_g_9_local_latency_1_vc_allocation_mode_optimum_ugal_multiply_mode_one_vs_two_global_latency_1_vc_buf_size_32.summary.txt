sim_count = 1;
input_speedup = 1;
routing_delay = 0;
output_speedup = 1;
warmup_periods = 3;
packet_size = 1;
vc_alloc_delay = 1;
alloc_iters = 1;
vc_allocator = islip;
priority = none;
st_final_delay = 1;
credit_delay = 1;
sw_allocator = islip;
latency_thres = 512.0;
wait_for_tail_credit = 0;
sw_alloc_delay = 1;
injection_rate_uses_flits = 1;
sample_period = 1024;
df_a = 4;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
internal_speedup = 2.0;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = one_vs_two;
vc_allocation_mode = optimum;
vc_buf_size = 32;
df_wc_seed = 1290;
num_vcs = 4;
routing_function = UGAL_G;
seed = 1290;
traffic = uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50083,   0.50072,    13.694,    13.697,         0,      3.4033
simulation,         2,      0.74,   0.74083,   0.74102,    16.696,    16.709,         0,      3.3549
simulation,         3,      0.86,   0.86115,    0.8612,    25.033,    25.033,         0,      3.3493
simulation,         4,      0.92,   0.88188,   0.88773,    264.58,    147.07,         0,      3.3623
simulation,         5,      0.95,   0.88182,    0.8826,    475.84,    159.29,         1,         0.0
simulation,         6,      0.93,   0.88234,   0.88594,    355.19,    157.71,         0,      3.3588
simulation,         7,      0.94,   0.87867,   0.87897,    457.56,    159.41,         0,      3.3682
simulation,         8,       0.2,   0.20047,   0.20042,    12.988,    12.994,         0,      3.5345
simulation,         9,       0.4,   0.40064,   0.40059,    13.309,    13.311,         0,      3.4348
simulation,        10,       0.6,   0.60128,   0.60125,     14.44,    14.442,         0,      3.3792
simulation,        11,       0.8,   0.80079,   0.80125,    19.094,    19.092,         0,      3.3503
