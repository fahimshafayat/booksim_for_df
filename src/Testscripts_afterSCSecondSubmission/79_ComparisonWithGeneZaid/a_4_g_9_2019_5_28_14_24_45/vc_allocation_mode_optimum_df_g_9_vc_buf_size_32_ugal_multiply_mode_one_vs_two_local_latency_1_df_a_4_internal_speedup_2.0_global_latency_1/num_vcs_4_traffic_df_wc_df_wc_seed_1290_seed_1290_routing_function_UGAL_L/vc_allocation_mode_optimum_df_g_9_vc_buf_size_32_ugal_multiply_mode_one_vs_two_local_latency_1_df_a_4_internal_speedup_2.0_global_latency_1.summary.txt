vc_alloc_delay = 1;
input_speedup = 1;
credit_delay = 1;
sim_count = 1;
wait_for_tail_credit = 0;
latency_thres = 512.0;
warmup_periods = 3;
injection_rate_uses_flits = 1;
sw_allocator = islip;
alloc_iters = 1;
routing_delay = 0;
st_final_delay = 1;
packet_size = 1;
vc_allocator = islip;
output_speedup = 1;
priority = none;
sample_period = 1024;
sw_alloc_delay = 1;
df_a = 4;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 1;
internal_speedup = 2.0;
local_latency = 1;
topology = dragonflyfull;
ugal_multiply_mode = one_vs_two;
vc_allocation_mode = optimum;
vc_buf_size = 32;
df_wc_seed = 1290;
num_vcs = 4;
routing_function = UGAL_L;
seed = 1290;
traffic = df_wc;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,      0.05,  0.049714,  0.049687,    15.822,    15.825,         0,      4.5481
simulation,         2,       0.1,    0.1001,   0.10006,    16.438,    16.445,         0,      4.6666
simulation,         3,      0.15,   0.15066,   0.15065,    17.029,    17.036,         0,      4.7195
simulation,         4,       0.2,   0.20045,   0.20042,    18.002,    18.012,         0,      4.7844
simulation,         5,      0.25,   0.25136,   0.25136,    20.637,    20.645,         0,      4.9157
simulation,         6,       0.3,   0.30102,   0.30084,    32.656,    32.696,         0,      5.1413
simulation,         7,      0.35,   0.35031,   0.35026,    34.738,    34.802,         0,      5.2747
simulation,         8,       0.4,   0.40063,   0.40059,    37.309,    37.404,         0,      5.3769
simulation,         9,      0.45,   0.45015,   0.45021,     42.04,     42.17,         0,      5.4436
simulation,        10,       0.5,   0.46423,   0.46509,    309.31,    128.27,         1,         0.0
