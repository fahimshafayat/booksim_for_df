//We need to consolidate the g_ and ed_ version of variables so that
//we can use one function to print them at the end.
//it's a really poor design to keep it this way.

#ifndef _GLOBALSTATS_HPP_
#define _GLOBALSTATS_HPP_

#include "pair_hash.hpp"
#include <unordered_map>
#include <string>

extern std::string g_routing_function; 

extern long int g_total_flit;
extern long int g_total_min_flit;
extern long int g_total_non_min_flit;

#define MAX_PATH_LEN 10 //arbitrarily large. In reality path lengths will be much smaller.

extern long int g_min_ingroup_path_dist[MAX_PATH_LEN];
extern long int g_min_outgroup_path_dist[MAX_PATH_LEN];

extern long int g_vlb_ingroup_path_dist[MAX_PATH_LEN * 2];
extern long int g_vlb_outgroup_path_dist[MAX_PATH_LEN * 2];

#define MAX_CASE 7
extern long int g_cases_when_not_taken[MAX_CASE];
extern long int g_cases_when_taken[MAX_CASE];
extern long int g_lens_when_not_taken[MAX_CASE];
extern long int g_lens_when_taken[MAX_CASE];

//extern std::unordered_map < std::pair<int, int>, int, pair_hash > g_pathlen_pair_freq_for_ugal_comparison;

struct Counters{
    int tot;
    int min;
    int vlb;

    Counters(){
        tot = 1;
        min = 0;
        vlb = 0;
    }
};

extern std::unordered_map < std::pair<int, int>, struct Counters, pair_hash > g_pathlen_pair_freq_for_ugal_comparison;

bool comparator_for_counters( std::pair< std::pair<int, int>, struct Counters > e1, std::pair< std::pair<int, int>, struct Counters > e2 );

#define ED_MAX_PATH_LEN 13  //for PAR the max path len cna be 12. [0,12] => 13 cells
extern long int ed_total_min_flit; 
extern long int ed_total_non_min_flit;
extern long int ed_ingroup_path_dist[ED_MAX_PATH_LEN];
extern long int ed_outgroup_path_dist[ED_MAX_PATH_LEN];


#include <fstream>
extern std::ofstream g_q_len_file;	//used in dragonflyfull
extern std::ofstream ed_q_len_file;	//used in edison

#endif