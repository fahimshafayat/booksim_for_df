vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = PAR;
seed = 25;
shift_offset = 1536;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.44293,   0.50022,    576.74,    576.74,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25007,   0.25007,    110.33,    110.51,         0,  2337238,  9408659,  ,  ,  ,  
simulation,         3,      0.37,   0.36998,   0.36997,    129.06,    129.57,         0,  3090302,  15829145,  ,  ,  ,  
simulation,         4,      0.43,   0.42744,   0.42985,    241.31,    258.87,         0,  5576936,  33098672,  ,  ,  ,  
simulation,         5,      0.46,   0.44529,   0.45984,    379.52,    393.11,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.43652,    0.4398,    267.81,    311.74,         0,  6458038,  38987467,  ,  ,  ,  
simulation,         7,      0.45,   0.43919,   0.44985,    369.87,    479.76,         0,  7590785,  46763767,  ,  ,  ,  
simulation,         9,       0.4,   0.39884,   0.39985,    166.49,    171.88,         0,  4796413,  26501493,  ,  ,  ,  
simulation,        10,      0.35,   0.34995,   0.34998,    119.52,    119.72,         0,  2863623,  13985022,  ,  ,  ,  
simulation,        11,       0.3,   0.30001,   0.30003,    113.27,    113.42,         0,  2605624,  11492305,  ,  ,  ,  
