vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = UGAL_L;
seed = 27;
shift_offset = 1536;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4353,   0.50032,    329.67,    329.67,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24962,   0.25001,    392.18,     413.4,         0,  5526839,  15236011,  ,  ,  ,  
simulation,         3,      0.37,   0.34892,   0.36993,    355.72,    355.72,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30526,   0.31007,    383.97,    383.97,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27819,      0.28,    315.01,    401.67,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,   0.25945,   0.26001,     402.6,    423.98,         0,  5894028,  17114659,  ,  ,  ,  
simulation,         7,      0.27,   0.26893,   0.26998,    418.17,    441.67,         0,  6125141,  18589529,  ,  ,  ,  
simulation,         9,       0.2,   0.20003,   0.20001,    361.66,    378.04,         0,  5106774,  10450707,  ,  ,  ,  
simulation,        10,      0.15,   0.15013,   0.14997,    337.06,     348.3,         0,  4602083,  6032752,  ,  ,  ,  
simulation,        11,       0.1,  0.099909,  0.099904,    81.415,    81.554,         0,  2786943,  1889311,  ,  ,  ,  
