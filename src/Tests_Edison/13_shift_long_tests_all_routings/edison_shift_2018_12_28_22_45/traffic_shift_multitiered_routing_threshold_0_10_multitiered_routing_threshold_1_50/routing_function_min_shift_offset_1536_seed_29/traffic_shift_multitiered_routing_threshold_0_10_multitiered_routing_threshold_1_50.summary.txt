vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = min;
seed = 29;
shift_offset = 1536;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.060916,   0.49795,    878.72,    878.72,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.060826,   0.24984,    748.32,    748.32,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.060719,   0.12976,    516.97,    516.97,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.062086,  0.069983,    584.49,    688.87,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.040008,  0.040003,    54.937,    54.964,         0,  1857009,  0,  ,  ,  ,  
simulation,         6,      0.05,  0.049978,  0.049968,    56.077,    56.103,         0,  2322355,  0,  ,  ,  ,  
simulation,         7,      0.06,  0.059954,  0.059969,    65.919,    65.962,         0,  2849486,  0,  ,  ,  ,  
