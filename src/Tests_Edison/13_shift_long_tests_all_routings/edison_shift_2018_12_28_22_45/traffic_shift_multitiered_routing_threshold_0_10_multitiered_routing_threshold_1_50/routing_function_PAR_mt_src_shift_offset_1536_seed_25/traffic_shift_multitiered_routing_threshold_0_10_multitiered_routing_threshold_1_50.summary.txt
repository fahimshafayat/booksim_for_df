vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = PAR_mt_src;
seed = 25;
shift_offset = 1536;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.44251,   0.50022,    588.24,    588.24,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.25007,    97.984,    98.071,         0,  947208,  10760063,  ,  ,  ,  
simulation,         3,      0.37,   0.37019,   0.36997,    134.68,    136.33,         0,  1624723,  23722820,  ,  ,  ,  
simulation,         4,      0.43,   0.42811,   0.42985,    230.82,    251.06,         0,  4384140,  36699642,  ,  ,  ,  
simulation,         5,      0.46,   0.44746,   0.45984,    401.91,    577.01,         0,  7134446,  48861305,  ,  ,  ,  
simulation,         6,      0.48,   0.44448,    0.4799,    538.92,    563.63,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.44565,   0.46971,    485.58,    492.74,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44296,   0.44985,    296.57,    349.56,         0,  5257957,  37476437,  ,  ,  ,  
simulation,         9,       0.4,   0.39948,    0.3999,    146.96,    149.97,         0,  2657481,  27601297,  ,  ,  ,  
simulation,        10,      0.35,   0.35008,   0.34998,    125.61,    126.38,         0,  1391482,  23520471,  ,  ,  ,  
simulation,        11,       0.3,   0.29999,   0.30003,    102.55,    102.66,         0,  850499,  13375204,  ,  ,  ,  
