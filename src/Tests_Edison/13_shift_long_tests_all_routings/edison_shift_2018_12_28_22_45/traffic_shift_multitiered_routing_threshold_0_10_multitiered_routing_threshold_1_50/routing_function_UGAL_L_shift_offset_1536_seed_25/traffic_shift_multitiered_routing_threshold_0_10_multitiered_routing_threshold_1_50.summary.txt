vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = UGAL_L;
seed = 25;
shift_offset = 1536;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.43538,   0.49989,    328.16,    328.16,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24965,   0.24996,    391.26,    412.26,         0,  5672145,  15668220,  ,  ,  ,  
simulation,         3,      0.37,   0.34914,   0.37023,    352.22,    352.22,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30294,   0.30989,    288.96,    414.76,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,    0.2784,   0.27995,    437.85,    470.68,         0,  6933902,  21909643,  ,  ,  ,  
simulation,         6,      0.29,   0.28719,   0.28996,    313.48,    409.04,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.19996,   0.19993,    361.33,    378.05,         0,  5054822,  10322823,  ,  ,  ,  
simulation,         9,      0.15,   0.15019,   0.15003,    337.79,    349.85,         0,  4559638,  5963537,  ,  ,  ,  
simulation,        10,       0.1,  0.099989,   0.09997,    81.379,    81.523,         0,  2791681,  1893257,  ,  ,  ,  
