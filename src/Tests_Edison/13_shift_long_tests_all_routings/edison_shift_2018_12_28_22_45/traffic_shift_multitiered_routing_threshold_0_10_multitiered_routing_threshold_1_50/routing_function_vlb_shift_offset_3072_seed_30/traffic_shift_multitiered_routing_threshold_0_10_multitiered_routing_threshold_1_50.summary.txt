vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = vlb;
seed = 30;
shift_offset = 3072;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4075,   0.50001,    661.81,    661.81,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24985,   0.24986,    106.75,    106.77,         0,  0,  11695395,  ,  ,  ,  
simulation,         3,      0.37,    0.3699,   0.36985,    116.23,    116.22,         0,  0,  17419859,  ,  ,  ,  
simulation,         4,      0.43,   0.40469,   0.42995,    539.43,    540.72,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,    0.3992,   0.39983,    161.83,    162.51,         0,  0,  20725194,  ,  ,  ,  
simulation,         6,      0.41,    0.4035,   0.40986,    261.25,    273.75,         0,  0,  23464037,  ,  ,  ,  
simulation,         7,      0.42,   0.40449,   0.41999,    453.08,    561.43,         0,  0,  39510361,  ,  ,  ,  
simulation,         9,      0.35,   0.34979,    0.3498,    112.12,    112.11,         0,  0,  16437285,  ,  ,  ,  
simulation,        10,       0.3,   0.29989,   0.29989,    108.34,    108.35,         0,  0,  14052644,  ,  ,  ,  
