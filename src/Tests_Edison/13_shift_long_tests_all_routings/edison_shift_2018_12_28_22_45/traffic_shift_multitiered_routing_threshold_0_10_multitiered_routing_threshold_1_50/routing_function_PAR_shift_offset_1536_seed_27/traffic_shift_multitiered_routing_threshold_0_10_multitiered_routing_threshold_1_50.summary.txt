vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = PAR;
seed = 27;
shift_offset = 1536;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.44195,   0.50004,    577.87,    577.87,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24997,   0.24995,    110.38,    110.56,         0,  2336853,  9400327,  ,  ,  ,  
simulation,         3,      0.37,   0.37003,   0.36994,     129.2,    129.77,         0,  3063693,  15698804,  ,  ,  ,  
simulation,         4,      0.43,   0.42737,   0.42996,    240.32,    258.85,         0,  5525189,  32783671,  ,  ,  ,  
simulation,         5,      0.46,   0.44423,   0.45995,    388.33,    402.12,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.43606,   0.43999,    268.47,    297.94,         0,  5974244,  36137989,  ,  ,  ,  
simulation,         7,      0.45,     0.441,   0.44996,    359.72,    447.77,         0,  6846762,  42040488,  ,  ,  ,  
simulation,         9,       0.4,   0.39903,       0.4,    165.02,    170.16,         0,  4666858,  25785976,  ,  ,  ,  
simulation,        10,      0.35,   0.34984,    0.3499,     119.3,    119.49,         0,  2953891,  14426822,  ,  ,  ,  
simulation,        11,       0.3,   0.29996,   0.29995,    113.25,    113.41,         0,  2606281,  11496359,  ,  ,  ,  
