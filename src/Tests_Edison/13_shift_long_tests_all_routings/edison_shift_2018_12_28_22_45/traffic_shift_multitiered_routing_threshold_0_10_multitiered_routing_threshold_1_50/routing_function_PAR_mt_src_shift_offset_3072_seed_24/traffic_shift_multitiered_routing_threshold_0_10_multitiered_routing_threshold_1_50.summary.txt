vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = PAR_mt_src;
seed = 24;
shift_offset = 3072;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.44305,   0.50018,     586.4,     586.4,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25004,   0.25005,    97.879,    97.959,         0,  914612,  10798223,  ,  ,  ,  
simulation,         3,      0.37,   0.36988,   0.37001,    132.69,    133.47,         0,  1297471,  18699091,  ,  ,  ,  
simulation,         4,      0.43,   0.42885,   0.43004,    214.57,    224.64,         0,  4416194,  38569498,  ,  ,  ,  
simulation,         5,      0.46,   0.44928,      0.46,    384.28,    493.64,         0,  6693023,  46469604,  ,  ,  ,  
simulation,         6,      0.48,   0.44704,   0.47989,    540.54,     558.0,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.44844,   0.47001,    479.25,    483.86,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44332,   0.45002,    292.11,    342.26,         0,  6571126,  47061886,  ,  ,  ,  
simulation,         9,       0.4,   0.39925,   0.40003,     148.3,    151.43,         0,  2031441,  21869043,  ,  ,  ,  
simulation,        10,      0.35,   0.35008,   0.35006,    119.86,    120.28,         0,  990336,  16392755,  ,  ,  ,  
simulation,        11,       0.3,   0.30005,   0.30008,    103.16,    103.28,         0,  820166,  13447219,  ,  ,  ,  
