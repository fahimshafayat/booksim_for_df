vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = min;
seed = 25;
shift_offset = 1536;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.060941,   0.49815,    879.14,    879.14,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.060846,      0.25,    748.44,    748.44,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.060717,   0.13001,    516.83,    516.83,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.062045,  0.069968,     589.3,     692.9,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.040054,  0.040037,    54.955,    54.982,         0,  1858733,  0,  ,  ,  ,  
simulation,         6,      0.05,  0.049989,  0.049975,     56.09,    56.114,         0,  2323592,  0,  ,  ,  ,  
simulation,         7,      0.06,   0.05998,  0.059949,    65.887,    65.916,         0,  2826048,  0,  ,  ,  ,  
