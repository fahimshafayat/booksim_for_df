vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = PAR;
seed = 29;
shift_offset = 3072;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.44366,   0.50039,    565.22,    565.22,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24991,   0.24996,     110.4,    110.58,         0,  2330594,  9402636,  ,  ,  ,  
simulation,         3,      0.37,   0.37008,   0.37003,    127.31,     127.9,         0,  3077394,  15797517,  ,  ,  ,  
simulation,         4,      0.43,   0.42873,   0.43001,    223.22,    235.22,         0,  5557183,  33019904,  ,  ,  ,  
simulation,         5,      0.46,   0.44688,   0.46007,    439.49,    528.84,         0,  6275364,  39387644,  ,  ,  ,  
simulation,         6,      0.48,   0.44651,   0.48008,     505.2,    566.61,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.44717,   0.47003,    486.64,    507.39,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44257,   0.45004,    346.35,    362.59,         0,  5658366,  35147903,  ,  ,  ,  
simulation,         9,       0.4,   0.39909,   0.40005,    156.81,    159.99,         0,  3659081,  20264450,  ,  ,  ,  
simulation,        10,      0.35,   0.34987,   0.34998,     119.6,    119.83,         0,  2867101,  14027113,  ,  ,  ,  
simulation,        11,       0.3,   0.29996,   0.30001,    113.27,    113.43,         0,  2599169,  11496459,  ,  ,  ,  
