vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = min;
seed = 27;
shift_offset = 1536;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.060907,   0.49832,    878.94,    878.94,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.060885,    0.2501,    749.94,    749.94,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.060739,   0.13009,    518.16,    518.16,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.062093,  0.069983,    591.06,    695.98,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.039993,  0.039992,    54.923,    54.949,         0,  1858450,  0,  ,  ,  ,  
simulation,         6,      0.05,   0.04997,  0.049974,    56.105,    56.126,         0,  2324603,  0,  ,  ,  ,  
simulation,         7,      0.06,  0.059971,  0.059948,    66.264,    66.321,         0,  2826407,  0,  ,  ,  ,  
