vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = UGAL_L;
seed = 30;
shift_offset = 2304;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.43501,   0.50009,    330.72,    330.72,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24953,   0.24987,    391.57,    412.44,         0,  5569250,  15351211,  ,  ,  ,  
simulation,         3,      0.37,   0.34861,   0.37018,    354.63,    354.63,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30463,   0.31004,    382.31,    382.31,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27825,   0.27992,    314.41,    399.84,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,   0.25929,   0.25987,    402.98,    424.49,         0,  5804939,  16818818,  ,  ,  ,  
simulation,         7,      0.27,   0.26897,   0.26989,    417.37,    442.41,         0,  6381593,  19395176,  ,  ,  ,  
simulation,         9,       0.2,   0.19998,   0.19989,    360.97,    377.98,         0,  5032655,  10278608,  ,  ,  ,  
simulation,        10,      0.15,   0.15007,   0.14991,    336.81,    347.66,         0,  4685490,  6148309,  ,  ,  ,  
simulation,        11,       0.1,  0.099912,  0.099888,    81.348,    81.496,         0,  2791638,  1894857,  ,  ,  ,  
