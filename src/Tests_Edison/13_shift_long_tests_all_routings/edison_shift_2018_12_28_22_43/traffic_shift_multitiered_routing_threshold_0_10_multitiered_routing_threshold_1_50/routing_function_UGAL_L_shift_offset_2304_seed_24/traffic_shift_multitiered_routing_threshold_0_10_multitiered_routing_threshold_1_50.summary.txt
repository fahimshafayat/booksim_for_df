vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = UGAL_L;
seed = 24;
shift_offset = 2304;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.43516,    0.4998,    328.42,    328.42,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24958,   0.25003,    392.83,    411.44,         0,  5850943,  16211225,  ,  ,  ,  
simulation,         3,      0.37,   0.34954,   0.37019,    354.43,    354.43,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30473,   0.30991,    382.07,    382.07,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27853,   0.28007,    436.97,    469.17,         0,  6763190,  21325451,  ,  ,  ,  
simulation,         6,      0.29,   0.28739,   0.29004,    311.72,    404.73,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.2,   0.20007,   0.20003,    361.19,    376.83,         0,  5130807,  10492148,  ,  ,  ,  
simulation,         9,      0.15,   0.15021,   0.15005,    336.73,     348.0,         0,  4608742,  6036335,  ,  ,  ,  
simulation,        10,       0.1,   0.10008,   0.10007,    81.484,    81.623,         0,  2788935,  1893666,  ,  ,  ,  
