vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = min;
seed = 30;
shift_offset = 384;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.060866,   0.49801,    879.24,    879.24,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.060753,   0.24995,    747.93,    747.93,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.060651,   0.12996,     516.8,     516.8,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.062183,  0.069926,    586.18,    690.22,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.039962,  0.039962,    54.955,    54.986,         0,  1858821,  0,  ,  ,  ,  
simulation,         6,      0.05,  0.049956,  0.049956,    56.074,    56.103,         0,  2324515,  0,  ,  ,  ,  
simulation,         7,      0.06,  0.059962,  0.059952,     66.36,    66.432,         0,  2855515,  0,  ,  ,  ,  
