vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = PAR;
seed = 27;
shift_offset = 384;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4411,   0.50005,    590.25,    590.25,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24996,   0.24995,    110.32,     110.5,         0,  2336800,  9400380,  ,  ,  ,  
simulation,         3,      0.37,   0.36995,   0.36994,    127.72,    128.22,         0,  3181875,  16288542,  ,  ,  ,  
simulation,         4,      0.43,   0.42789,   0.42996,    240.31,    254.66,         0,  5416860,  32158572,  ,  ,  ,  
simulation,         5,      0.46,   0.44313,   0.45992,    402.18,    416.19,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.43571,   0.43999,    273.38,    316.26,         0,  6534104,  39584215,  ,  ,  ,  
simulation,         7,      0.45,   0.44078,   0.44996,    363.97,    460.42,         0,  6963081,  42839515,  ,  ,  ,  
simulation,         9,       0.4,   0.39926,   0.39992,    153.21,    156.86,         0,  3921062,  21684384,  ,  ,  ,  
simulation,        10,      0.35,   0.34983,    0.3499,    120.07,    120.32,         0,  2937753,  14329887,  ,  ,  ,  
simulation,        11,       0.3,   0.29995,   0.29995,    113.22,    113.39,         0,  2609648,  11501529,  ,  ,  ,  
