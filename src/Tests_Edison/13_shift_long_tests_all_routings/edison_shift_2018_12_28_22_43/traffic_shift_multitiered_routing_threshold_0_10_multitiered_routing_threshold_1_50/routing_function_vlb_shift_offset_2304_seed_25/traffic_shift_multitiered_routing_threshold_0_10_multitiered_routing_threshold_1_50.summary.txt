vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = vlb;
seed = 25;
shift_offset = 2304;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.40762,   0.50008,    661.64,    661.64,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25003,   0.25007,    106.76,    106.78,         0,  0,  11701455,  ,  ,  ,  
simulation,         3,      0.37,   0.36996,   0.36997,    116.31,    116.27,         0,  0,  17424240,  ,  ,  ,  
simulation,         4,      0.43,   0.40486,   0.42992,     541.9,    542.73,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.39933,   0.39994,     161.8,    162.32,         0,  0,  20566734,  ,  ,  ,  
simulation,         6,      0.41,    0.4035,   0.40996,    262.45,     286.5,         0,  0,  27105333,  ,  ,  ,  
simulation,         7,      0.42,   0.40446,   0.41988,    456.32,    560.39,         0,  0,  38867581,  ,  ,  ,  
simulation,         9,      0.35,   0.34996,   0.34998,    112.14,    112.12,         0,  0,  16445875,  ,  ,  ,  
simulation,        10,       0.3,   0.30001,   0.30003,    108.33,    108.33,         0,  0,  14053414,  ,  ,  ,  
