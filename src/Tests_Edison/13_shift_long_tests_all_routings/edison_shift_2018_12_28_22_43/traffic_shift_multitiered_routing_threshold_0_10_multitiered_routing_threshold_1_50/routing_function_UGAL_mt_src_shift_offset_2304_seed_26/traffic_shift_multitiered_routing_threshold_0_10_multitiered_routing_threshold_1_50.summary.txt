vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = UGAL_mt_src;
seed = 26;
shift_offset = 2304;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.43753,   0.50011,    380.16,    380.16,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25015,   0.25006,    341.48,    361.05,         0,  5282749,  14688349,  ,  ,  ,  
simulation,         3,      0.37,   0.36231,   0.37001,     240.4,    365.87,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30995,   0.31005,    354.36,    374.03,         0,  6246859,  22944369,  ,  ,  ,  
simulation,         5,      0.34,   0.33896,   0.34005,    380.64,    443.72,         0,  10197825,  42270126,  ,  ,  ,  
simulation,         6,      0.35,   0.34794,   0.35004,    266.27,    356.57,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.29996,   0.30002,    351.73,    372.18,         0,  5644220,  19759551,  ,  ,  ,  
simulation,        10,       0.2,   0.20025,   0.20015,    351.05,    368.97,         0,  4960200,  10118128,  ,  ,  ,  
