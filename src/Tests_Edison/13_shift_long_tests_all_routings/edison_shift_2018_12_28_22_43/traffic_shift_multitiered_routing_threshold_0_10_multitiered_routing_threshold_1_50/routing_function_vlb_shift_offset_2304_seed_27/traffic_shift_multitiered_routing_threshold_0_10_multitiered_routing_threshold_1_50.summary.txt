vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = vlb;
seed = 27;
shift_offset = 2304;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.40753,   0.50011,     663.4,     663.4,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24994,   0.24995,    106.75,    106.77,         0,  0,  11702712,  ,  ,  ,  
simulation,         3,      0.37,   0.36992,   0.36994,    116.23,    116.21,         0,  0,  17455738,  ,  ,  ,  
simulation,         4,      0.43,   0.40499,   0.42991,    541.63,    542.44,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.39929,   0.39992,    162.03,    162.41,         0,  0,  20121956,  ,  ,  ,  
simulation,         6,      0.41,   0.40356,   0.40994,    262.85,     277.9,         0,  0,  24398311,  ,  ,  ,  
simulation,         7,      0.42,   0.40458,   0.41997,    453.78,    553.36,         0,  0,  38427552,  ,  ,  ,  
simulation,         9,      0.35,   0.34991,    0.3499,    112.14,    112.13,         0,  0,  16431177,  ,  ,  ,  
simulation,        10,       0.3,   0.29995,   0.29995,    108.33,    108.34,         0,  0,  14056007,  ,  ,  ,  
