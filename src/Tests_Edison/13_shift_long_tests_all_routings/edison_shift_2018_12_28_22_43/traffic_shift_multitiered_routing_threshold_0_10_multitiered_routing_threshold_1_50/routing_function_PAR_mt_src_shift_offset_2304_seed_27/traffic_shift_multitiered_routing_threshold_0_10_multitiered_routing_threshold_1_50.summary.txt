vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = PAR_mt_src;
seed = 27;
shift_offset = 2304;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.44156,   0.50005,    590.65,    590.65,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24994,   0.24995,    97.739,    97.827,         0,  966423,  10757839,  ,  ,  ,  
simulation,         3,      0.37,   0.36967,   0.36994,    135.25,    137.06,         0,  1741968,  24759729,  ,  ,  ,  
simulation,         4,      0.43,   0.42821,   0.42996,    231.22,    252.41,         0,  5132517,  42849860,  ,  ,  ,  
simulation,         5,      0.46,   0.44762,   0.45995,    396.83,    617.06,         0,  7898893,  54490557,  ,  ,  ,  
simulation,         6,      0.48,   0.44515,   0.47997,    537.96,    559.52,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.44627,   0.46992,    482.07,    489.62,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44369,   0.44996,    289.75,    339.39,         0,  5397708,  38988998,  ,  ,  ,  
simulation,         9,       0.4,   0.39922,   0.39992,    149.11,    153.18,         0,  2419044,  24644027,  ,  ,  ,  
simulation,        10,      0.35,   0.35003,    0.3499,    124.07,     124.5,         0,  1355952,  22400634,  ,  ,  ,  
simulation,        11,       0.3,   0.29994,   0.29995,    102.45,    102.58,         0,  858162,  13435561,  ,  ,  ,  
