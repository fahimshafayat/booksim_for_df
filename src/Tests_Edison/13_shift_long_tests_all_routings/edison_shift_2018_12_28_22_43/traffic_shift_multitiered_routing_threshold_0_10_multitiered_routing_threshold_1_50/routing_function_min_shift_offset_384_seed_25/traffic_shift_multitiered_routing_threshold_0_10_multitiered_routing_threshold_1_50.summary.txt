vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = min;
seed = 25;
shift_offset = 384;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.060836,   0.49812,    878.86,    878.86,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.06075,      0.25,    748.85,    748.85,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.06072,   0.13001,    516.84,    516.84,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.062106,  0.069968,    590.15,    692.85,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.040052,  0.040037,    54.946,    54.975,         0,  1858971,  0,  ,  ,  ,  
simulation,         6,      0.05,  0.049989,  0.049975,    56.073,    56.097,         0,  2324159,  0,  ,  ,  ,  
simulation,         7,      0.06,  0.059976,  0.059949,    66.229,     66.26,         0,  2820891,  0,  ,  ,  ,  
