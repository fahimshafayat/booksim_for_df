vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = vlb;
seed = 29;
shift_offset = 2304;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4075,   0.50011,    661.46,    661.46,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24992,   0.24996,    106.75,    106.76,         0,  0,  11700059,  ,  ,  ,  
simulation,         3,      0.37,      0.37,   0.37003,    116.37,    116.33,         0,  0,  17410907,  ,  ,  ,  
simulation,         4,      0.43,   0.40497,      0.43,    540.26,    540.85,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.39926,   0.40005,    161.85,    162.53,         0,  0,  20537801,  ,  ,  ,  
simulation,         6,      0.41,    0.4035,   0.41004,    262.14,    275.82,         0,  0,  23864161,  ,  ,  ,  
simulation,         7,      0.42,   0.40447,      0.42,    454.13,    571.94,         0,  0,  40698580,  ,  ,  ,  
simulation,         9,      0.35,   0.34992,   0.34998,    112.12,    112.11,         0,  0,  16425205,  ,  ,  ,  
simulation,        10,       0.3,   0.29997,   0.30001,    108.34,    108.34,         0,  0,  14054218,  ,  ,  ,  
