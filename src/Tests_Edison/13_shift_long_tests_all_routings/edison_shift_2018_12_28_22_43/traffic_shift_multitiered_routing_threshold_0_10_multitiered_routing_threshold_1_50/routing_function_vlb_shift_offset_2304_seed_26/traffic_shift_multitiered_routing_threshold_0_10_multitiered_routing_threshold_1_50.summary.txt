vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = vlb;
seed = 26;
shift_offset = 2304;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.40753,   0.49989,    661.96,    661.96,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25015,   0.25013,    106.75,    106.77,         0,  0,  11704407,  ,  ,  ,  
simulation,         3,      0.37,   0.37008,   0.37005,    116.28,    116.25,         0,  0,  17420327,  ,  ,  ,  
simulation,         4,      0.43,   0.40463,   0.43004,    541.13,    542.12,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.39941,   0.40001,    162.25,    162.78,         0,  0,  20337617,  ,  ,  ,  
simulation,         6,      0.41,    0.4035,   0.40999,     262.5,    281.68,         0,  0,  25571520,  ,  ,  ,  
simulation,         7,      0.42,    0.4045,      0.42,    454.83,    538.04,         0,  0,  36493820,  ,  ,  ,  
simulation,         9,      0.35,   0.35011,   0.35009,    112.17,    112.16,         0,  0,  16436292,  ,  ,  ,  
simulation,        10,       0.3,   0.30011,   0.30008,    108.34,    108.35,         0,  0,  14058388,  ,  ,  ,  
