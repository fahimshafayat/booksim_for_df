vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 5;
multitiered_routing_threshold_1 = 20;
seed = 12;
topology = edison;
traffic = shift;
routing_function = min;
shift_offset = 1920;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,  0.060928,   0.46391,     879.8,     879.8,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,  0.060834,   0.25009,    748.94,    748.94,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.06073,   0.13001,    517.19,    517.19,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.062069,    0.0699,    590.75,    695.17,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.04,  0.039919,  0.039921,    54.955,     54.98,         0,  1857461,  0,  ,  ,  ,  
simulation,         6,      0.05,  0.049918,  0.049928,    56.091,    56.115,         0,  2323105,  0,  ,  ,  ,  
simulation,         7,      0.06,  0.059944,  0.059925,    65.803,    65.804,         0,  2842908,  0,  ,  ,  ,  
