credit_delay = 2;
priority = none;
sw_allocator = separable_input_first;
sim_count = 1;
packet_size = 1;
warmup_periods = 5;
num_vcs = 11;
sw_alloc_delay = 1;
routing_delay = 0;
vc_allocator = separable_input_first;
sample_period = 1000;
wait_for_tail_credit = 0;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
alloc_iters = 1;
vc_alloc_delay = 1;
internal_speedup = 4.0;
vc_buf_size = 64;
injection_rate_uses_flits = 1;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
seed = 12;
topology = edison;
traffic = shift;
routing_function = UGAL_L_mt;
shift_offset = 1920;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4371,   0.49969,    374.75,    374.75,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24997,   0.24991,    330.77,    348.71,         0,  5339434,  14883165,  ,  ,  ,  
simulation,         3,      0.37,   0.36293,   0.36995,    244.62,    363.83,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30978,   0.30991,    348.73,    370.64,         0,  5755046,  20967156,  ,  ,  ,  
simulation,         5,      0.34,   0.33853,   0.33993,    383.67,    437.66,         0,  8976368,  36852382,  ,  ,  ,  
simulation,         6,      0.35,    0.3477,   0.34993,    260.78,    353.26,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.3,   0.29975,   0.29993,    345.32,    363.51,         0,  6005247,  21160580,  ,  ,  ,  
simulation,        10,       0.2,   0.20008,   0.19995,    339.76,    357.88,         0,  4886534,  9970327,  ,  ,  ,  
