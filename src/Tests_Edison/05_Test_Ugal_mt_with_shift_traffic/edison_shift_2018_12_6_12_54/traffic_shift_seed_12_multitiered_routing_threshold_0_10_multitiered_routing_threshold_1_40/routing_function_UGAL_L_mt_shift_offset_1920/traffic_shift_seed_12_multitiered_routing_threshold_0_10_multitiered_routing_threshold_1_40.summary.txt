vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 12;
topology = edison;
traffic = shift;
routing_function = UGAL_L_mt;
shift_offset = 1920;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.43658,   0.49969,    335.69,    335.69,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24988,   0.24991,    342.91,    361.62,         0,  5389623,  14988975,  ,  ,  ,  
simulation,         3,      0.37,   0.35844,   0.37003,    345.18,    345.18,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30849,   0.30991,    406.99,    440.08,         0,  7010438,  25224966,  ,  ,  ,  
simulation,         5,      0.34,   0.33283,   0.33993,    254.71,    387.71,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.32,   0.31715,   0.31993,    278.19,    377.82,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.3,   0.29902,   0.29993,    389.14,    413.38,         0,  6468326,  22544298,  ,  ,  ,  
simulation,         9,       0.2,   0.20004,   0.19995,    340.91,     357.9,         0,  4986752,  10198273,  ,  ,  ,  
simulation,        10,      0.15,   0.15001,   0.14996,    327.63,    339.53,         0,  4591748,  6019800,  ,  ,  ,  
