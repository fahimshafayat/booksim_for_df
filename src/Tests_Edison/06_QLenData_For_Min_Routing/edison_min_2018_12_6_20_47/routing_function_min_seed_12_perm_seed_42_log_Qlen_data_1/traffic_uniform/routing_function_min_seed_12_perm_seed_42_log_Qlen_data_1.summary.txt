vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
log_Qlen_data = 1;
perm_seed = 42;
routing_function = min;
seed = 12;
topology = edison;
traffic = uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,      0.25,   0.24996,   0.24996,    52.473,    52.506,         0,  11604958,  0,  ,  ,  ,  
simulation,         2,       0.5,   0.50007,   0.50006,    53.439,    53.471,         0,  23225827,  0,  ,  ,  ,  
simulation,         3,       0.9,   0.90006,   0.90003,    68.846,     68.92,         0,  42635946,  0,  ,  ,  ,  
simulation,         4,      0.97,    0.9267,   0.94665,    416.71,    374.59,         0,  77752329,  0,  ,  ,  ,  
