vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
shift_offset = 384;
shift_percentage = 50;
topology = edison;
traffic = pe_based_shift_uniform;
routing_function = UGAL_mt_src;
seed = 70;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49333,   0.49953,    178.62,    228.56,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24998,   0.24994,    184.68,    191.28,         0,  13243164,  5014643,  ,  ,  ,  
simulation,         3,      0.37,      0.37,   0.37001,    192.04,    200.37,         0,  18336559,  10250499,  ,  ,  ,  
simulation,         4,      0.43,   0.42991,   0.42999,    200.49,    206.74,         0,  31308852,  19814456,  ,  ,  ,  
simulation,         5,      0.46,   0.45915,   0.46002,    215.32,    242.16,         0,  48635515,  32373653,  ,  ,  ,  
simulation,         6,      0.48,   0.47735,   0.47994,    242.84,    301.07,         0,  45592359,  30858874,  ,  ,  ,  
simulation,         7,      0.49,   0.48538,    0.4898,    266.56,    341.24,         0,  48986454,  33251679,  ,  ,  ,  
simulation,         8,      0.45,    0.4496,   0.45003,     208.4,    219.38,         0,  32535016,  21251384,  ,  ,  ,  
simulation,         9,       0.4,   0.40007,       0.4,    195.03,    203.79,         0,  20833413,  12394209,  ,  ,  ,  
simulation,        10,      0.35,   0.34999,   0.34998,    190.32,    197.81,         0,  18097112,  9681586,  ,  ,  ,  
simulation,        11,       0.3,   0.30002,   0.29993,    184.44,    191.22,         0,  15497177,  7192409,  ,  ,  ,  
