vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
shift_offset = 384;
shift_percentage = 75;
topology = edison;
traffic = router_based_shift_uniform;
routing_function = UGAL_L;
seed = 72;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.42628,   0.49963,    351.29,    351.25,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24999,   0.24998,    278.34,     290.9,         0,  9706849,  10265364,  ,  ,  ,  
simulation,         3,      0.37,   0.36077,   0.37008,    227.53,    332.13,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30946,   0.31004,     311.0,    326.88,         0,  13363601,  16868996,  ,  ,  ,  
simulation,         5,      0.34,    0.3376,   0.34007,    243.32,    311.21,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.32,   0.31907,   0.32008,    322.44,    344.25,         0,  15920369,  20601312,  ,  ,  ,  
simulation,         7,      0.33,   0.32848,   0.33005,    338.01,    397.78,         0,  19765981,  26127524,  ,  ,  ,  
simulation,         8,       0.3,   0.29966,   0.30004,    300.39,    314.75,         0,  12227050,  15055462,  ,  ,  ,  
simulation,        10,       0.2,   0.20002,   0.19996,    257.62,    267.58,         0,  7858930,  6628975,  ,  ,  ,  
simulation,        11,      0.15,   0.14988,   0.15001,    183.02,    187.36,         0,  6360457,  3699280,  ,  ,  ,  
