routing_delay = 0;
num_vcs = 11;
packet_size = 1;
wait_for_tail_credit = 0;
output_speedup = 1;
vc_alloc_delay = 1;
input_speedup = 1;
warmup_periods = 5;
internal_speedup = 4.0;
injection_rate_uses_flits = 1;
credit_delay = 2;
priority = none;
vc_allocator = separable_input_first;
alloc_iters = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_buf_size = 64;
sw_allocator = separable_input_first;
sample_period = 1000;
sim_count = 1;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
shift_offset = 384;
shift_percentage = 75;
topology = edison;
traffic = pe_based_shift_uniform;
routing_function = UGAL_mt_src;
seed = 70;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.47424,   0.49972,    305.98,    305.98,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25001,   0.24994,    259.07,    271.01,         0,  9538732,  9113518,  ,  ,  ,  
simulation,         3,      0.37,   0.36989,   0.37001,    270.71,    285.33,         0,  13836140,  18612315,  ,  ,  ,  
simulation,         4,      0.43,   0.42315,   0.42997,    210.25,    290.97,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,    0.3987,       0.4,    298.01,    336.19,         0,  19715332,  28172675,  ,  ,  ,  
simulation,         6,      0.41,    0.4077,   0.40998,    215.79,    277.34,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.35,   0.34991,   0.34998,     265.5,    279.19,         0,  13084951,  16864572,  ,  ,  ,  
simulation,         9,       0.3,   0.29996,   0.29993,    259.03,    272.22,         0,  10761187,  12196832,  ,  ,  ,  
