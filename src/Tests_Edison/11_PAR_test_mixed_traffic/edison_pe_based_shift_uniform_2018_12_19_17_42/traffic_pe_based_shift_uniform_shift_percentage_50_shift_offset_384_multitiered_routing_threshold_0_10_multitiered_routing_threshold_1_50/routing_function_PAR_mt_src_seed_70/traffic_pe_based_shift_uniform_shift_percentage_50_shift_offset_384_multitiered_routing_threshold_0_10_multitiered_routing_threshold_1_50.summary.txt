vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
shift_offset = 384;
shift_percentage = 50;
topology = edison;
traffic = pe_based_shift_uniform;
routing_function = PAR_mt_src;
seed = 70;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.46965,    0.4988,    445.12,     463.6,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24988,   0.24991,    89.242,    89.439,         0,  2954797,  8751118,  ,  ,  ,  
simulation,         3,      0.37,   0.36908,      0.37,     114.8,    117.47,         0,  6784790,  26498976,  ,  ,  ,  
simulation,         4,      0.43,   0.42405,   0.42999,    179.69,    203.98,         0,  7994493,  36391163,  ,  ,  ,  
simulation,         5,      0.46,   0.45314,   0.46002,    233.56,    259.78,         0,  8338589,  44294725,  ,  ,  ,  
simulation,         6,      0.48,    0.4658,   0.47997,     270.4,    281.11,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,    0.4609,   0.47004,    271.55,    379.52,         0,  10211792,  56982272,  ,  ,  ,  
simulation,         8,      0.45,   0.44403,   0.45003,    210.31,    241.29,         0,  7952835,  39613394,  ,  ,  ,  
simulation,         9,       0.4,    0.3971,       0.4,    146.01,    150.16,         0,  8828853,  36799748,  ,  ,  ,  
simulation,        10,      0.35,   0.34986,   0.34994,    102.23,    103.77,         0,  5553822,  20918590,  ,  ,  ,  
simulation,        11,       0.3,   0.29992,    0.2999,    91.138,    91.334,         0,  3243886,  10875416,  ,  ,  ,  
