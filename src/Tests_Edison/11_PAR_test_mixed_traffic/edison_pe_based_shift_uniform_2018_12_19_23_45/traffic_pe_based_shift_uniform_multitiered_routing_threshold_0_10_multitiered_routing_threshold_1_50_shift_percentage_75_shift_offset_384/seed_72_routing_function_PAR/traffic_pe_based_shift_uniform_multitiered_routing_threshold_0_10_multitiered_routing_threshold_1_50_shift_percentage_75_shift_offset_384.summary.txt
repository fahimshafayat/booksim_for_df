sample_period = 1000;
injection_rate_uses_flits = 1;
routing_delay = 0;
vc_buf_size = 64;
output_speedup = 1;
st_final_delay = 1;
sw_allocator = separable_input_first;
sim_count = 1;
vc_alloc_delay = 1;
warmup_periods = 5;
num_vcs = 13;
sw_alloc_delay = 1;
alloc_iters = 1;
packet_size = 1;
priority = none;
credit_delay = 2;
vc_allocator = separable_input_first;
input_speedup = 1;
internal_speedup = 4.0;
wait_for_tail_credit = 0;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
shift_offset = 384;
shift_percentage = 75;
topology = edison;
traffic = pe_based_shift_uniform;
routing_function = PAR;
seed = 72;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.45419,   0.50022,    466.06,    589.54,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25005,   0.24998,    106.23,    106.51,         0,  2983882,  8765092,  ,  ,  ,  
simulation,         3,      0.37,    0.3701,   0.37009,    115.73,    115.97,         0,  4087118,  14752514,  ,  ,  ,  
simulation,         4,      0.43,   0.42908,    0.4301,    181.92,    189.17,         0,  10826180,  47683638,  ,  ,  ,  
simulation,         5,      0.46,   0.45336,   0.46011,    294.59,    381.81,         0,  8928216,  45070897,  ,  ,  ,  
simulation,         6,      0.48,   0.45297,   0.48001,    467.58,    488.62,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.45359,   0.47011,    350.95,    363.71,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44751,   0.45014,     236.4,    254.91,         0,  8706430,  42597499,  ,  ,  ,  
simulation,         9,       0.4,    0.4002,   0.40011,    127.36,    127.91,         0,  4424778,  17441313,  ,  ,  ,  
simulation,        10,      0.35,   0.35005,   0.35003,    112.24,    112.49,         0,  3701243,  12785200,  ,  ,  ,  
simulation,        11,       0.3,    0.3001,   0.30004,    108.83,     109.1,         0,  3385396,  10719367,  ,  ,  ,  
