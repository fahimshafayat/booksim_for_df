sample_period = 1000;
injection_rate_uses_flits = 1;
routing_delay = 0;
vc_buf_size = 64;
output_speedup = 1;
st_final_delay = 1;
sw_allocator = separable_input_first;
sim_count = 1;
vc_alloc_delay = 1;
warmup_periods = 5;
num_vcs = 13;
sw_alloc_delay = 1;
alloc_iters = 1;
packet_size = 1;
priority = none;
credit_delay = 2;
vc_allocator = separable_input_first;
input_speedup = 1;
internal_speedup = 4.0;
wait_for_tail_credit = 0;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
shift_offset = 384;
shift_percentage = 75;
topology = edison;
traffic = pe_based_shift_uniform;
routing_function = PAR;
seed = 70;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.45261,   0.49999,    463.51,    599.94,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24987,   0.24991,    106.24,    106.51,         0,  2983274,  8766473,  ,  ,  ,  
simulation,         3,      0.37,   0.36994,   0.36996,    115.62,    115.89,         0,  4159020,  15002206,  ,  ,  ,  
simulation,         4,      0.43,    0.4289,   0.42999,     180.6,    186.45,         0,  8368712,  36892294,  ,  ,  ,  
simulation,         5,      0.46,   0.45365,   0.46002,    287.22,    356.39,         0,  8086289,  41361027,  ,  ,  ,  
simulation,         6,      0.48,   0.45447,   0.48002,    453.75,    475.39,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.45284,   0.47004,    356.48,    370.46,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44773,   0.45003,    236.91,    265.84,         0,  11138802,  54187198,  ,  ,  ,  
simulation,         9,       0.4,   0.39982,   0.39995,    126.29,    126.96,         0,  4507849,  17758296,  ,  ,  ,  
simulation,        10,      0.35,   0.34995,   0.34994,     112.2,    112.45,         0,  3704163,  12778106,  ,  ,  ,  
simulation,        11,       0.3,    0.2999,    0.2999,    108.82,    109.09,         0,  3390392,  10734732,  ,  ,  ,  
