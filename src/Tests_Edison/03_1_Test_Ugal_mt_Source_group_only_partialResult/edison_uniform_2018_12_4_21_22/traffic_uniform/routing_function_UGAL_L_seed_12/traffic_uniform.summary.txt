vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
topology = edison;
traffic = uniform;
routing_function = UGAL_L;
seed = 12;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50007,   0.50006,    56.282,    56.347,         0,  22234855,  1145956,  ,  ,  ,  
simulation,         2,      0.74,   0.74015,   0.74014,    57.941,     57.99,         0,  33752995,  908228,  ,  ,  ,  
simulation,         3,      0.86,   0.86006,   0.86006,    66.765,    66.879,         0,  40998961,  827444,  ,  ,  ,  
simulation,         4,      0.92,   0.85187,    0.9076,    473.75,    468.57,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.89,   0.88921,   0.89007,    83.581,    84.229,         0,  45845602,  953925,  ,  ,  ,  
simulation,         6,       0.9,   0.89748,   0.90003,     98.67,    102.77,         0,  53473383,  1331779,  ,  ,  ,  
simulation,         7,      0.91,   0.84186,   0.90872,    352.18,    469.89,         0,  74167435,  5754933,  ,  ,  ,  
simulation,         9,      0.85,   0.85007,   0.85008,    64.482,    64.559,         0,  40182013,  835106,  ,  ,  ,  
simulation,        10,       0.8,   0.80012,   0.80011,    59.821,    59.872,         0,  36781991,  860010,  ,  ,  ,  
simulation,        11,      0.75,   0.75015,   0.75014,    58.179,    58.229,         0,  34225569,  899687,  ,  ,  ,  
