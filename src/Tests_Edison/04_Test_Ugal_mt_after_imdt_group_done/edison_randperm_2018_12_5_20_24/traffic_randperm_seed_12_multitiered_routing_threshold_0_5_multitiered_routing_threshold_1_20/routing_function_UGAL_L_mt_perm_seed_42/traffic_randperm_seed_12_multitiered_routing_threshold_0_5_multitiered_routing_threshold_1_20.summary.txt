vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 5;
multitiered_routing_threshold_1 = 20;
seed = 12;
topology = edison;
traffic = randperm;
perm_seed = 42;
routing_function = UGAL_L_mt;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    58.055,     58.12,         0,  20136648,  3170697,  ,  ,  ,  
simulation,         2,      0.74,   0.72447,   0.73886,    166.18,    171.89,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,    0.6201,   0.62011,    61.186,    61.282,         0,  27714089,  4124756,  ,  ,  ,  
simulation,         4,      0.68,   0.67992,    0.6801,    73.485,    73.979,         0,  38015436,  5743806,  ,  ,  ,  
simulation,         5,      0.71,   0.70904,   0.71007,    95.766,    98.204,         0,  62881260,  10445942,  ,  ,  ,  
simulation,         6,      0.72,   0.71812,   0.72007,    110.87,    116.51,         0,  64914129,  11585255,  ,  ,  ,  
simulation,         7,      0.73,   0.72467,    0.7299,    138.34,    178.58,         0,  69441411,  15576092,  ,  ,  ,  
simulation,         8,       0.7,   0.69953,   0.70009,    84.677,    85.961,         0,  45250922,  7132699,  ,  ,  ,  
simulation,         9,      0.65,   0.64998,    0.6501,    65.655,    65.935,         0,  31247233,  4632704,  ,  ,  ,  
simulation,        10,       0.6,   0.60011,    0.6001,    59.793,    59.863,         0,  25333556,  3804973,  ,  ,  ,  
simulation,        11,      0.55,   0.55005,   0.55002,    58.624,     58.69,         0,  22246514,  3423818,  ,  ,  ,  
