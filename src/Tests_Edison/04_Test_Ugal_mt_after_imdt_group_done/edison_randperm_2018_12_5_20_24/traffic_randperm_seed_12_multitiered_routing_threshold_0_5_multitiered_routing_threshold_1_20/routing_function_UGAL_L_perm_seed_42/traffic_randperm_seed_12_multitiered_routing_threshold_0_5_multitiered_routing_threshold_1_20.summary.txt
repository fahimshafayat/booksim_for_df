vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 5;
multitiered_routing_threshold_1 = 20;
seed = 12;
topology = edison;
traffic = randperm;
perm_seed = 42;
routing_function = UGAL_L;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    60.648,    60.765,         0,  20171062,  3199248,  ,  ,  ,  
simulation,         2,      0.74,   0.72606,   0.73925,    157.56,     162.4,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.62011,   0.62011,    62.796,     62.92,         0,  26953384,  3944184,  ,  ,  ,  
simulation,         4,      0.68,   0.67995,    0.6801,     73.83,    74.334,         0,  35285860,  5202574,  ,  ,  ,  
simulation,         5,      0.71,   0.70961,   0.71011,    92.061,    93.197,         0,  56511584,  9043918,  ,  ,  ,  
simulation,         6,      0.72,   0.71856,   0.72008,    104.77,     109.5,         0,  66584655,  11478368,  ,  ,  ,  
simulation,         7,      0.73,   0.72596,   0.73008,    128.64,    154.83,         0,  66737208,  13778067,  ,  ,  ,  
simulation,         8,       0.7,    0.6998,   0.70009,    83.332,    84.018,         0,  45774471,  7016029,  ,  ,  ,  
simulation,         9,      0.65,      0.65,    0.6501,    66.409,    66.623,         0,  29755662,  4321884,  ,  ,  ,  
simulation,        10,       0.6,   0.60012,    0.6001,    61.749,    61.861,         0,  24524885,  3629147,  ,  ,  ,  
simulation,        11,      0.55,   0.55006,   0.55002,    60.958,    61.073,         0,  22306156,  3408360,  ,  ,  ,  
