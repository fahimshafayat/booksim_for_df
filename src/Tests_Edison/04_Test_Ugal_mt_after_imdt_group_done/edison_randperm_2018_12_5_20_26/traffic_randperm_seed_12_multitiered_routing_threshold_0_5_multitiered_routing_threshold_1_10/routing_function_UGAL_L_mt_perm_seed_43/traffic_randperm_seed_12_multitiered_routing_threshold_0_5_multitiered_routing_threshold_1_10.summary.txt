vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 5;
multitiered_routing_threshold_1 = 10;
seed = 12;
topology = edison;
traffic = randperm;
perm_seed = 43;
routing_function = UGAL_L_mt;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50005,   0.50006,    58.937,    59.007,         0,  20240907,  3131676,  ,  ,  ,  
simulation,         2,      0.74,   0.71935,   0.73877,    181.32,    187.41,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.62009,   0.62011,     62.04,    62.146,         0,  27512318,  3981169,  ,  ,  ,  
simulation,         4,      0.68,   0.67994,    0.6801,    73.379,    73.771,         0,  35825881,  5154671,  ,  ,  ,  
simulation,         5,      0.71,   0.70879,   0.71008,    98.157,    102.07,         0,  59191414,  9532847,  ,  ,  ,  
simulation,         6,      0.72,   0.71742,   0.72004,    116.15,    127.33,         0,  64556346,  11514929,  ,  ,  ,  
simulation,         7,      0.73,    0.7235,   0.72957,    147.96,    341.81,         0,  98303759,  29704707,  ,  ,  ,  
simulation,         8,       0.7,   0.69963,   0.70009,    85.705,    86.681,         0,  41523284,  6216357,  ,  ,  ,  
simulation,         9,      0.65,   0.65012,    0.6501,    65.662,    65.841,         0,  31057426,  4446082,  ,  ,  ,  
simulation,        10,       0.6,   0.60011,    0.6001,    60.997,    61.078,         0,  26241458,  3848879,  ,  ,  ,  
simulation,        11,      0.55,   0.55007,   0.55002,    59.652,    59.728,         0,  22347218,  3373017,  ,  ,  ,  
