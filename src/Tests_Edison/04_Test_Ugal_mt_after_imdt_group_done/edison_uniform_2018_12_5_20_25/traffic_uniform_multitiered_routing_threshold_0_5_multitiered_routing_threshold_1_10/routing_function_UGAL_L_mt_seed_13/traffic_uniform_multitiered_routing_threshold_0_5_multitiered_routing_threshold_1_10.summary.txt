vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 5;
multitiered_routing_threshold_1 = 10;
topology = edison;
traffic = uniform;
routing_function = UGAL_L_mt;
seed = 13;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49988,   0.49986,    55.448,    55.493,         0,  22213173,  1145425,  ,  ,  ,  
simulation,         2,      0.74,   0.73991,   0.73992,    57.809,    57.858,         0,  33680004,  975802,  ,  ,  ,  
simulation,         3,      0.86,   0.85983,   0.85991,    67.028,     67.16,         0,  40797691,  859151,  ,  ,  ,  
simulation,         4,      0.92,   0.85209,   0.90798,    469.46,    463.57,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.89,   0.88889,   0.88977,    84.387,    85.075,         0,  46383059,  1006169,  ,  ,  ,  
simulation,         6,       0.9,   0.89747,   0.89977,    99.356,    101.82,         0,  49214797,  1220167,  ,  ,  ,  
simulation,         7,      0.91,   0.84132,   0.90798,    371.86,    487.77,         0,  76267010,  6039670,  ,  ,  ,  
simulation,         9,      0.85,   0.84994,   0.84989,    64.684,     64.76,         0,  40238720,  875022,  ,  ,  ,  
simulation,        10,       0.8,    0.7999,   0.79988,    59.782,    59.831,         0,  36665110,  911805,  ,  ,  ,  
simulation,        11,      0.75,   0.74992,   0.74991,    58.051,    58.097,         0,  34171882,  964915,  ,  ,  ,  
