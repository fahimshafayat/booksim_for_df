vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 12;
topology = edison;
traffic = randperm;
perm_seed = 42;
routing_function = UGAL_L_mt;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    55.306,    55.344,         0,  21769988,  1517164,  ,  ,  ,  
simulation,         2,      0.74,   0.73296,   0.73991,    140.73,    349.73,         0,  84009884,  25536105,  ,  ,  ,  
simulation,         3,      0.86,   0.66785,   0.74064,    694.99,     690.3,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.66248,   0.74645,    423.52,    577.54,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.67536,   0.74238,    414.41,    422.44,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.75,   0.72621,   0.74876,    177.94,    183.19,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.7,   0.69994,   0.70012,    72.601,    73.203,         0,  35119424,  4257828,  ,  ,  ,  
simulation,         9,      0.65,   0.65009,    0.6501,    59.835,    59.937,         0,  29722042,  3122809,  ,  ,  ,  
simulation,        10,       0.6,    0.6001,    0.6001,    57.062,    57.104,         0,  25589001,  2413127,  ,  ,  ,  
