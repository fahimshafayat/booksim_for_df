vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 40;
seed = 12;
topology = edison;
traffic = randperm;
perm_seed = 43;
routing_function = UGAL_L;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50006,    60.791,    60.907,         0,  20281020,  3108810,  ,  ,  ,  
simulation,         2,      0.74,   0.71914,   0.73847,    181.04,    187.28,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.62013,   0.62011,    62.973,    63.092,         0,  27418213,  3857689,  ,  ,  ,  
simulation,         4,      0.68,   0.67998,    0.6801,    73.422,     73.87,         0,  34762571,  4893993,  ,  ,  ,  
simulation,         5,      0.71,   0.70912,   0.71011,    95.144,      97.5,         0,  50369693,  7793088,  ,  ,  ,  
simulation,         6,      0.72,   0.71718,   0.72004,    114.65,     127.2,         0,  67163218,  11952931,  ,  ,  ,  
simulation,         7,      0.73,   0.72267,   0.72971,     150.5,    439.75,         0,  119229820,  41017298,  ,  ,  ,  
simulation,         8,       0.7,   0.69968,   0.70009,    83.865,    84.889,         0,  40729100,  5943540,  ,  ,  ,  
simulation,         9,      0.65,   0.65007,    0.6501,    66.213,    66.413,         0,  30318030,  4223997,  ,  ,  ,  
simulation,        10,       0.6,   0.60014,    0.6001,    62.014,    62.135,         0,  25619873,  3649527,  ,  ,  ,  
simulation,        11,      0.55,   0.55003,   0.55002,    61.106,    61.216,         0,  22427477,  3305319,  ,  ,  ,  
