vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
perm_seed = 51;
routing_function = UGAL_L;
seed = 29;
traffic = uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50013,   0.50009,    56.279,    56.347,         0,  22237454,  1146872,  ,  ,  ,  
simulation,         2,      0.74,    0.7403,   0.74028,    57.955,    58.006,         0,  33747091,  910388,  ,  ,  ,  
simulation,         3,      0.86,   0.86011,    0.8601,     66.52,    66.618,         0,  41653744,  839506,  ,  ,  ,  
simulation,         4,      0.92,   0.85371,   0.91293,     466.6,    464.44,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.89,    0.8893,      0.89,    82.809,     83.49,         0,  46572152,  955894,  ,  ,  ,  
simulation,         6,       0.9,   0.89774,   0.89999,    96.616,    98.475,         0,  46065645,  1069086,  ,  ,  ,  
simulation,         7,      0.91,   0.84183,   0.90944,    357.34,    495.24,         0,  77104823,  6042996,  ,  ,  ,  
simulation,         9,      0.85,   0.85005,   0.85015,    64.467,    64.549,         0,  40001508,  828623,  ,  ,  ,  
simulation,        10,       0.8,   0.80025,   0.80024,    59.826,    59.878,         0,  36756055,  857630,  ,  ,  ,  
simulation,        11,      0.75,   0.75023,   0.75025,    58.165,    58.217,         0,  34237345,  901026,  ,  ,  ,  
