vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
perm_seed = 51;
routing_function = vlb;
seed = 29;
traffic = uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.46697,   0.50009,    532.14,    542.96,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24992,   0.24996,    102.06,    102.15,         0,  0,  11688997,  ,  ,  ,  
simulation,         3,      0.37,   0.36996,   0.37003,    105.37,    105.45,         0,  0,  17328768,  ,  ,  ,  
simulation,         4,      0.43,   0.42996,      0.43,    112.77,    112.84,         0,  0,  20248647,  ,  ,  ,  
simulation,         5,      0.46,   0.45964,   0.46007,    148.98,    149.46,         0,  0,  23464434,  ,  ,  ,  
simulation,         6,      0.48,   0.46638,   0.48005,    367.61,    441.69,         0,  0,  43187458,  ,  ,  ,  
simulation,         7,      0.49,   0.46656,   0.49006,     478.7,    477.47,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.45009,   0.45003,    124.92,     125.0,         0,  0,  21391567,  ,  ,  ,  
simulation,         9,       0.4,   0.39997,   0.40005,    107.56,    107.63,         0,  0,  18755428,  ,  ,  ,  
simulation,        10,      0.35,   0.34993,   0.34998,    104.45,    104.53,         0,  0,  16379496,  ,  ,  ,  
simulation,        11,       0.3,   0.29998,   0.30001,    102.97,    103.06,         0,  0,  14035903,  ,  ,  ,  
