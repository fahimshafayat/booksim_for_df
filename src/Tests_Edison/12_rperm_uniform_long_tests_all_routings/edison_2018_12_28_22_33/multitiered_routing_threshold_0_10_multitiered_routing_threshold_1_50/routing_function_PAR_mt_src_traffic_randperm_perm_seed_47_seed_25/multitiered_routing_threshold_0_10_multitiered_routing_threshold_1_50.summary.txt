vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
perm_seed = 47;
routing_function = PAR_mt_src;
seed = 25;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49991,   0.49998,    111.34,    112.16,         0,  6789681,  19144158,  ,  ,  ,  
simulation,         2,      0.74,   0.46839,     0.739,    870.12,    870.12,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.46761,   0.62004,    577.69,    577.69,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.47317,   0.56004,    613.75,    613.75,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.47273,   0.52993,    501.09,    501.55,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.51,   0.50928,   0.50996,    129.36,    131.82,         0,  8041542,  24967621,  ,  ,  ,  
simulation,         7,      0.52,   0.50485,   0.51985,     227.4,    463.79,         0,  7840491,  40985600,  ,  ,  ,  
simulation,         9,      0.45,   0.44996,   0.44996,    90.018,    90.326,         0,  6461841,  14939374,  ,  ,  ,  
simulation,        10,       0.4,   0.39995,   0.39994,    85.705,    85.953,         0,  6101903,  12629787,  ,  ,  ,  
simulation,        11,      0.35,   0.34998,   0.34998,    84.019,    84.264,         0,  5615376,  10755033,  ,  ,  ,  
