vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
perm_seed = 50;
routing_function = vlb;
seed = 28;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.46481,   0.50013,    528.82,    552.31,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25013,   0.25013,    102.16,    102.25,         0,  0,  11702519,  ,  ,  ,  
simulation,         3,      0.37,   0.37015,   0.37013,    105.49,    105.57,         0,  0,  17341758,  ,  ,  ,  
simulation,         4,      0.43,   0.43009,   0.43002,    113.44,    113.51,         0,  0,  20275881,  ,  ,  ,  
simulation,         5,      0.46,   0.45826,   0.46004,    167.63,     170.7,         0,  0,  25496855,  ,  ,  ,  
simulation,         6,      0.48,   0.46345,   0.48004,    368.81,    370.39,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.46221,   0.47004,    268.03,    324.76,         0,  0,  44163488,  ,  ,  ,  
simulation,         8,      0.45,   0.44988,   0.44999,    130.33,    130.62,         0,  0,  23584356,  ,  ,  ,  
simulation,         9,       0.4,   0.40008,   0.40005,    107.77,    107.84,         0,  0,  18791478,  ,  ,  ,  
simulation,        10,      0.35,   0.35013,   0.35011,    104.55,    104.63,         0,  0,  16395416,  ,  ,  ,  
simulation,        11,       0.3,   0.30013,   0.30012,    103.08,    103.17,         0,  0,  14046397,  ,  ,  ,  
