vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
perm_seed = 46;
routing_function = min;
seed = 24;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49988,   0.49989,    54.127,    54.261,         0,  31644627,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.67443,    0.6874,    271.79,    291.08,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.60823,   0.61502,    89.814,    92.962,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55873,   0.55967,    62.676,    69.519,         0,  57598520,  0,  ,  ,  ,  
simulation,         5,      0.59,    0.5849,   0.58826,    106.25,    108.69,         0,  138898031,  0,  ,  ,  ,  
simulation,         6,       0.6,   0.59263,   0.59696,    141.03,    130.44,         0,  271143692,  0,  ,  ,  ,  
simulation,         7,      0.61,   0.60124,    0.6067,    81.529,    84.082,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.55,   0.54877,   0.54977,    61.482,    66.228,         0,  48864262,  0,  ,  ,  ,  
simulation,        11,      0.45,   0.44999,   0.44997,    53.182,    53.218,         0,  21108524,  0,  ,  ,  ,  
