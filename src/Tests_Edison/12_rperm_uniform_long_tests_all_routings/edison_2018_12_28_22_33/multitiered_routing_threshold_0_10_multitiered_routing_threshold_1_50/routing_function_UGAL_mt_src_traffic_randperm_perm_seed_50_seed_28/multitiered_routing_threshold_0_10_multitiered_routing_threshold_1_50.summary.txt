vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
perm_seed = 50;
routing_function = UGAL_mt_src;
seed = 28;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50012,   0.50014,    55.939,    55.983,         0,  21828079,  1507581,  ,  ,  ,  
simulation,         2,      0.74,    0.7318,   0.73917,    136.32,    141.94,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.62023,   0.62021,    59.468,    59.538,         0,  29212457,  2862361,  ,  ,  ,  
simulation,         4,      0.68,    0.6799,   0.68012,     70.99,    71.484,         0,  35749326,  4133229,  ,  ,  ,  
simulation,         5,      0.71,   0.70955,   0.71004,    87.986,    89.428,         0,  52425992,  7093059,  ,  ,  ,  
simulation,         6,      0.72,   0.71886,   0.72005,    99.594,    103.37,         0,  58080451,  8609616,  ,  ,  ,  
simulation,         7,      0.73,   0.72691,   0.72996,    120.06,     139.1,         0,  94402555,  16512422,  ,  ,  ,  
simulation,         8,       0.7,   0.69978,   0.70009,    80.766,    81.589,         0,  48822676,  6202678,  ,  ,  ,  
simulation,         9,      0.65,   0.65019,   0.65021,    63.004,    63.189,         0,  30917041,  3262178,  ,  ,  ,  
simulation,        10,       0.6,   0.60014,   0.60015,    58.241,    58.306,         0,  27729083,  2592317,  ,  ,  ,  
simulation,        11,      0.55,   0.55014,   0.55016,    56.805,    56.854,         0,  23790651,  1945324,  ,  ,  ,  
