vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
perm_seed = 49;
routing_function = min;
seed = 27;
traffic = uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50006,   0.50004,    53.444,    53.475,         0,  23223555,  0,  ,  ,  ,  
simulation,         2,      0.74,   0.73991,   0.73991,    55.942,    55.973,         0,  34434772,  0,  ,  ,  ,  
simulation,         3,      0.86,   0.85997,   0.85999,    61.231,    61.273,         0,  40239603,  0,  ,  ,  ,  
simulation,         4,      0.92,   0.91914,   0.92003,    86.366,    86.868,         0,  46317361,  0,  ,  ,  ,  
simulation,         5,      0.95,   0.92722,   0.94844,    252.46,    280.98,         0,  76598710,  0,  ,  ,  ,  
simulation,         6,      0.97,   0.92646,   0.95256,    418.44,    407.42,         0,  81442732,  0,  ,  ,  ,  
simulation,         7,      0.98,   0.92698,   0.95178,    434.56,    405.77,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.9,   0.90005,   0.90002,    68.977,    69.047,         0,  43117215,  0,  ,  ,  ,  
simulation,        10,      0.85,   0.84998,   0.84999,    60.331,    60.371,         0,  39692970,  0,  ,  ,  ,  
simulation,        11,       0.8,   0.79996,   0.79996,    57.587,    57.618,         0,  37267795,  0,  ,  ,  ,  
