vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
perm_seed = 50;
routing_function = UGAL_mt_src;
seed = 28;
traffic = uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50011,   0.50014,    54.096,    54.129,         0,  23048863,  264261,  ,  ,  ,  
simulation,         2,      0.74,   0.74018,   0.74017,    57.078,    57.113,         0,  33948657,  627560,  ,  ,  ,  
simulation,         3,      0.86,   0.86033,   0.86028,     65.02,    65.091,         0,  40597910,  726266,  ,  ,  ,  
simulation,         4,      0.92,   0.83521,   0.90968,    441.09,     433.8,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.89,    0.8894,   0.89016,    80.073,    80.693,         0,  45714819,  839134,  ,  ,  ,  
simulation,         6,       0.9,   0.89913,   0.90016,    89.045,    89.853,         0,  47037078,  884017,  ,  ,  ,  
simulation,         7,      0.91,   0.86198,   0.91003,    227.83,    446.25,         0,  77201025,  6497616,  ,  ,  ,  
simulation,         9,      0.85,   0.85028,    0.8503,    63.125,     63.18,         0,  39453513,  711260,  ,  ,  ,  
simulation,        10,       0.8,   0.80019,   0.80016,    59.012,    59.052,         0,  36751569,  682826,  ,  ,  ,  
simulation,        11,      0.75,   0.75019,   0.75016,     57.32,    57.359,         0,  34402741,  635652,  ,  ,  ,  
