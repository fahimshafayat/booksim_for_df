vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
perm_seed = 51;
routing_function = PAR;
seed = 29;
traffic = uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4691,   0.50006,     466.5,    462.64,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.24996,    94.602,    95.089,         0,  4690158,  7029153,  ,  ,  ,  
simulation,         3,      0.37,   0.36992,   0.37003,    97.419,    97.972,         0,  7017911,  10353358,  ,  ,  ,  
simulation,         4,      0.43,   0.43006,      0.43,    101.77,    102.31,         0,  7684824,  12630878,  ,  ,  ,  
simulation,         5,      0.46,   0.46017,   0.46007,    111.78,    112.41,         0,  9193047,  16786967,  ,  ,  ,  
simulation,         6,      0.48,   0.47965,   0.48004,     132.3,    133.89,         0,  9826970,  21676748,  ,  ,  ,  
simulation,         7,      0.49,   0.48745,   0.49006,     170.8,    198.25,         0,  7917528,  27655337,  ,  ,  ,  
simulation,         8,      0.45,   0.45013,   0.45003,    108.11,    108.69,         0,  8461123,  14810375,  ,  ,  ,  
simulation,         9,       0.4,   0.39972,   0.40005,    99.258,    99.675,         0,  7408015,  11382013,  ,  ,  ,  
simulation,        10,      0.35,   0.35017,   0.34998,    96.614,    97.097,         0,  6707211,  9720691,  ,  ,  ,  
simulation,        11,       0.3,   0.29995,   0.30001,    95.035,    95.508,         0,  5777204,  8291288,  ,  ,  ,  
