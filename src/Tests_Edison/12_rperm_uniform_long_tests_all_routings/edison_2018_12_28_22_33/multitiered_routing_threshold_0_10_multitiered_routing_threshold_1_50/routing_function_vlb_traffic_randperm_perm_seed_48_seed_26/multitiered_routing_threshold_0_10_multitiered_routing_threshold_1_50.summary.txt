vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
perm_seed = 48;
routing_function = vlb;
seed = 26;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.46487,   0.49991,    526.92,    549.21,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25015,   0.25013,    102.15,    102.24,         0,  0,  11702453,  ,  ,  ,  
simulation,         3,      0.37,   0.37009,   0.37005,    105.48,    105.56,         0,  0,  17347056,  ,  ,  ,  
simulation,         4,      0.43,   0.42999,   0.42995,    113.38,    113.44,         0,  0,  20260425,  ,  ,  ,  
simulation,         5,      0.46,   0.45811,   0.45996,    166.09,    170.13,         0,  0,  26416097,  ,  ,  ,  
simulation,         6,      0.48,   0.46357,   0.47997,    409.21,    562.76,         0,  0,  52236827,  ,  ,  ,  
simulation,         7,      0.49,   0.46372,   0.48972,    484.05,    487.33,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44978,   0.44995,    129.91,    130.28,         0,  0,  22900627,  ,  ,  ,  
simulation,         9,       0.4,   0.40007,   0.40001,    107.76,    107.83,         0,  0,  18785339,  ,  ,  ,  
simulation,        10,      0.35,   0.35012,   0.35009,    104.56,    104.64,         0,  0,  16397345,  ,  ,  ,  
simulation,        11,       0.3,    0.3001,   0.30008,    103.08,    103.17,         0,  0,  14050900,  ,  ,  ,  
