vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
perm_seed = 50;
routing_function = UGAL_L;
seed = 28;
traffic = uniform;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50011,   0.50014,    56.287,    56.352,         0,  22241974,  1145693,  ,  ,  ,  
simulation,         2,      0.74,   0.74019,   0.74017,    57.941,    57.995,         0,  33787292,  912697,  ,  ,  ,  
simulation,         3,      0.86,   0.86023,   0.86028,    66.753,    66.851,         0,  41304040,  831856,  ,  ,  ,  
simulation,         4,      0.92,   0.85322,   0.91172,    467.87,    465.18,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.89,   0.88936,   0.89016,    84.184,    84.758,         0,  45922350,  970372,  ,  ,  ,  
simulation,         6,       0.9,   0.89807,   0.90016,    96.958,    99.141,         0,  47940867,  1119849,  ,  ,  ,  
simulation,         7,      0.91,   0.84116,   0.90926,    371.05,    516.57,         0,  79690893,  6375893,  ,  ,  ,  
simulation,         9,      0.85,    0.8502,    0.8503,     64.41,    64.488,         0,  39808903,  825685,  ,  ,  ,  
simulation,        10,       0.8,   0.80019,   0.80016,    59.832,    59.886,         0,  36738073,  861860,  ,  ,  ,  
simulation,        11,      0.75,   0.75018,   0.75016,     58.16,    58.213,         0,  34240779,  901491,  ,  ,  ,  
