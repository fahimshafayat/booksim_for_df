vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
seed = 22;
topology = edison;
traffic = randperm;
perm_seed = 44;
routing_function = UGAL_L;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.50013,   0.50013,    60.903,     61.02,         0,  20243851,  3140406,  ,  ,  ,  
simulation,         2,      0.74,   0.72424,   0.73826,    171.13,    178.36,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.62006,   0.62009,     64.92,     65.11,         0,  29055932,  4158044,  ,  ,  ,  
simulation,         4,      0.68,    0.6799,   0.68014,    79.966,    80.543,         0,  42033795,  6231093,  ,  ,  ,  
simulation,         5,      0.71,   0.70917,   0.71005,    101.82,     104.3,         0,  69809846,  11644084,  ,  ,  ,  
simulation,         6,      0.72,   0.71749,   0.71988,    119.98,     141.0,         0,  158971225,  31479029,  ,  ,  ,  
simulation,         7,      0.73,   0.72354,   0.72959,    153.35,    395.03,         0,  137840263,  46048510,  ,  ,  ,  
simulation,         8,       0.7,   0.69966,   0.70013,    92.057,    93.309,         0,  68313323,  10828126,  ,  ,  ,  
simulation,         9,      0.65,   0.64995,   0.65009,    70.225,    70.617,         0,  31398366,  4495213,  ,  ,  ,  
simulation,        10,       0.6,   0.60005,   0.60005,    63.061,    63.182,         0,  27182488,  3924582,  ,  ,  ,  
simulation,        11,      0.55,   0.55009,   0.55007,    61.281,    61.393,         0,  22437411,  3345836,  ,  ,  ,  
