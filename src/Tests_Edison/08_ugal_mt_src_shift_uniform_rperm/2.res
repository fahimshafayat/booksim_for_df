BEGIN Configuration File: ../../examples/edison_3
//Defaults
vc_buf_size = 64;
wait_for_tail_credit = 0;

//
// Router architecture
//
vc_allocator = separable_input_first; 
sw_allocator = separable_input_first;
alloc_iters  = 1;
credit_delay   = 2;
routing_delay  = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup     = 1;
output_speedup    = 1;
internal_speedup  = 4.0;
warmup_periods = 5;
sim_count          = 1;
sample_period  = 1000;  
num_vcs     = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits=1;


//customizables
//dragonfly specific parameters
topology = edison;
//end dragonfly specific paramters

routing_function = UGAL_mt_src;	//min/vlb/UGAL_L//UGAL_L_mt //UGAL_mt_src

multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;

seed = 12;

//traffic = uniform;

traffic       = randperm;
perm_seed = 42;

//traffic = shift;
//shift_offset = 384; // = 96 * 4; one group 

injection_rate = 0.50;

log_Qlen_data = 0;	//1 -> true. 0 -> false.

watch_file = watch;
watch_out = watch.out;


END Configuration File: ../../examples/edison_3
topology: edison
inside _RegisterRoutingFunctions() ...
done with _RegisterRoutingFunctions() ...
Routing function registered ...
inside Edison() constructor ...
inside _AllocateArrays() ... 
done with _AllocateArrays() ... 
inside _BuildGraphForLocal() ...
done with _BuildGraphForLocal() ...
/nfsdata/rahman/BooksimResources/edison_globallinks.txt file opened successfully.
_Computesize starts ...
_nodes: 5760
_size: 1440
_channels: 48240
_ComputeSize ends ...
inside _BuildNet() ...
Routers created ...
PEs connected ...
starting to connect the router channels ...
done with _BuildNet() ...
Done with Edison() constructor ...
topology object created ...
inside Run()
inside _SingleSim
_step_count: 200
_step_count: 400
_step_count: 600
_step_count: 800
_step_count: 1000
Class 0:
Packet latency average = 55.5911
	minimum = 4
	maximum = 109
Network latency average = 55.5911
	minimum = 4
	maximum = 109
Slowest packet = 2416046
Flit latency average = 55.5911
	minimum = 4
	maximum = 109
Slowest flit = 2416046
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500126
	minimum = 0.438 (at node 3976)
	maximum = 0.555 (at node 3860)
Accepted packet rate average = 0.472249
	minimum = 0.414 (at node 8)
	maximum = 0.532 (at node 508)
Injected flit rate average = 0.500126
	minimum = 0.438 (at node 3976)
	maximum = 0.555 (at node 3860)
Accepted flit rate average= 0.472249
	minimum = 0.414 (at node 8)
	maximum = 0.532 (at node 508)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 160576 (0 measured)
latency change    = 1
throughput change = 1
_step_count: 1200
_step_count: 1400
_step_count: 1600
_step_count: 1800
_step_count: 2000
Class 0:
Packet latency average = 55.7128
	minimum = 4
	maximum = 114
Network latency average = 55.7128
	minimum = 4
	maximum = 114
Slowest packet = 2612754
Flit latency average = 55.7128
	minimum = 4
	maximum = 114
Slowest flit = 2612754
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500073
	minimum = 0.4625 (at node 5666)
	maximum = 0.5395 (at node 3753)
Accepted packet rate average = 0.486096
	minimum = 0.445 (at node 1198)
	maximum = 0.5245 (at node 5245)
Injected flit rate average = 0.500073
	minimum = 0.4625 (at node 5666)
	maximum = 0.5395 (at node 3753)
Accepted flit rate average= 0.486096
	minimum = 0.445 (at node 1198)
	maximum = 0.5245 (at node 5245)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 161016 (0 measured)
latency change    = 0.00218327
throughput change = 0.0284871
_step_count: 2200
_step_count: 2400
_step_count: 2600
_step_count: 2800
_step_count: 3000
Class 0:
Packet latency average = 55.8246
	minimum = 4
	maximum = 108
Network latency average = 55.8246
	minimum = 4
	maximum = 108
Slowest packet = 6073441
Flit latency average = 55.8246
	minimum = 4
	maximum = 108
Slowest flit = 6073441
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.49969
	minimum = 0.447 (at node 4832)
	maximum = 0.555 (at node 2484)
Accepted packet rate average = 0.499649
	minimum = 0.447 (at node 212)
	maximum = 0.558 (at node 1582)
Injected flit rate average = 0.49969
	minimum = 0.447 (at node 4832)
	maximum = 0.555 (at node 2484)
Accepted flit rate average= 0.499649
	minimum = 0.447 (at node 212)
	maximum = 0.558 (at node 1582)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 161252 (0 measured)
latency change    = 0.00200351
throughput change = 0.0271241
_step_count: 3200
_step_count: 3400
_step_count: 3600
_step_count: 3800
_step_count: 4000
Class 0:
Packet latency average = 55.8215
	minimum = 4
	maximum = 111
Network latency average = 55.8215
	minimum = 4
	maximum = 111
Slowest packet = 9065601
Flit latency average = 55.8215
	minimum = 4
	maximum = 111
Slowest flit = 9065601
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.49986
	minimum = 0.4595 (at node 3626)
	maximum = 0.5405 (at node 5026)
Accepted packet rate average = 0.49984
	minimum = 0.4595 (at node 997)
	maximum = 0.546 (at node 734)
Injected flit rate average = 0.49986
	minimum = 0.4595 (at node 3626)
	maximum = 0.5405 (at node 5026)
Accepted flit rate average= 0.49984
	minimum = 0.4595 (at node 997)
	maximum = 0.546 (at node 734)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 161246 (0 measured)
latency change    = 5.52096e-05
throughput change = 0.000382241
_step_count: 4200
_step_count: 4400
_step_count: 4600
_step_count: 4800
_step_count: 5000
Class 0:
Packet latency average = 55.8201
	minimum = 4
	maximum = 112
Network latency average = 55.8201
	minimum = 4
	maximum = 112
Slowest packet = 13939733
Flit latency average = 55.8201
	minimum = 4
	maximum = 112
Slowest flit = 13939733
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500127
	minimum = 0.442 (at node 3853)
	maximum = 0.558 (at node 96)
Accepted packet rate average = 0.500228
	minimum = 0.439 (at node 971)
	maximum = 0.562 (at node 594)
Injected flit rate average = 0.500127
	minimum = 0.442 (at node 3853)
	maximum = 0.558 (at node 96)
Accepted flit rate average= 0.500228
	minimum = 0.439 (at node 971)
	maximum = 0.562 (at node 594)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 160664 (0 measured)
latency change    = 2.47199e-05
throughput change = 0.000776209
Warmed up ...Time used is 5000 cycles
_step_count: 5200
_step_count: 5400
_step_count: 5600
_step_count: 5800
_step_count: 6000
Class 0:
Packet latency average = 55.6557
	minimum = 4
	maximum = 108
Network latency average = 55.6557
	minimum = 4
	maximum = 108
Slowest packet = 16145294
Flit latency average = 55.8179
	minimum = 4
	maximum = 109
Slowest flit = 14212144
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.499836
	minimum = 0.432 (at node 4728)
	maximum = 0.549 (at node 186)
Accepted packet rate average = 0.49981
	minimum = 0.442 (at node 5456)
	maximum = 0.552 (at node 2443)
Injected flit rate average = 0.499836
	minimum = 0.432 (at node 4728)
	maximum = 0.549 (at node 186)
Accepted flit rate average= 0.49981
	minimum = 0.442 (at node 5456)
	maximum = 0.552 (at node 2443)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 160818 (160818 measured)
latency change    = 0.00295514
throughput change = 0.000836777
_step_count: 6200
_step_count: 6400
_step_count: 6600
_step_count: 6800
_step_count: 7000
Class 0:
Packet latency average = 55.7414
	minimum = 4
	maximum = 110
Network latency average = 55.7414
	minimum = 4
	maximum = 110
Slowest packet = 17717824
Flit latency average = 55.8201
	minimum = 4
	maximum = 110
Slowest flit = 17717824
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500134
	minimum = 0.461 (at node 1030)
	maximum = 0.5405 (at node 995)
Accepted packet rate average = 0.50012
	minimum = 0.4605 (at node 67)
	maximum = 0.5435 (at node 2428)
Injected flit rate average = 0.500134
	minimum = 0.461 (at node 1030)
	maximum = 0.5405 (at node 995)
Accepted flit rate average= 0.50012
	minimum = 0.4605 (at node 67)
	maximum = 0.5435 (at node 2428)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 160826 (160826 measured)
latency change    = 0.00153876
throughput change = 0.000621031
_step_count: 7200
_step_count: 7400
_step_count: 7600
_step_count: 7800
_step_count: 8000
Class 0:
Packet latency average = 55.7661
	minimum = 4
	maximum = 112
Network latency average = 55.7661
	minimum = 4
	maximum = 112
Slowest packet = 21252511
Flit latency average = 55.8181
	minimum = 4
	maximum = 112
Slowest flit = 21252511
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500063
	minimum = 0.462 (at node 1030)
	maximum = 0.532 (at node 4313)
Accepted packet rate average = 0.50006
	minimum = 0.462667 (at node 67)
	maximum = 0.530333 (at node 2824)
Injected flit rate average = 0.500063
	minimum = 0.462 (at node 1030)
	maximum = 0.532 (at node 4313)
Accepted flit rate average= 0.50006
	minimum = 0.462667 (at node 67)
	maximum = 0.530333 (at node 2824)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 160718 (160718 measured)
latency change    = 0.000441409
throughput change = 0.00012105
Draining all recorded packets ...
done with _SingleSim
Draining remaining packets ...
_step_count: 8200
Time taken is 8213 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 55.8181 (1 samples)
	minimum = 4 (1 samples)
	maximum = 112 (1 samples)
Network latency average = 55.8181 (1 samples)
	minimum = 4 (1 samples)
	maximum = 112 (1 samples)
Flit latency average = 55.8637 (1 samples)
	minimum = 4 (1 samples)
	maximum = 112 (1 samples)
Fragmentation average = 0 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0 (1 samples)
Injected packet rate average = 0.500063 (1 samples)
	minimum = 0.462 (1 samples)
	maximum = 0.532 (1 samples)
Accepted packet rate average = 0.50006 (1 samples)
	minimum = 0.462667 (1 samples)
	maximum = 0.530333 (1 samples)
Injected flit rate average = 0.500063 (1 samples)
	minimum = 0.462 (1 samples)
	maximum = 0.532 (1 samples)
Accepted flit rate average = 0.50006 (1 samples)
	minimum = 0.462667 (1 samples)
	maximum = 0.530333 (1 samples)
Injected packet size average = 1 (1 samples)
Accepted packet size average = 1 (1 samples)
Hops average = 5.56205 (1 samples)
Stats for UGAL routing for Edison: 
Total flits through min paths: 21754883
Total flits through non-min paths: 1543758
in-group traffic path-len distribution: |1-> 0 , |2-> 106342 , |3-> 74197 , |4-> 0 , |5-> 0 , |6-> 0 , |7-> 0 , |8-> 0 , |9-> 0 , |10-> 0 , |11-> 0 , 
out-group traffic path-len distribution: |1-> 2 , |2-> 22053 , |3-> 213648 , |4-> 3434 , |5-> 18213 , |6-> 69889 , |7-> 356680 , |8-> 679300 , |9-> 0 , |10-> 0 , |11-> 0 , 
done with Run()
Total run time 1240.48
