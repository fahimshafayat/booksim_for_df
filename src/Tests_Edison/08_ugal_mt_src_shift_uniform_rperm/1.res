BEGIN Configuration File: ../../examples/edison_2
//Defaults
vc_buf_size = 64;
wait_for_tail_credit = 0;

//
// Router architecture
//
vc_allocator = separable_input_first; 
sw_allocator = separable_input_first;
alloc_iters  = 1;
credit_delay   = 2;
routing_delay  = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup     = 1;
output_speedup    = 1;
internal_speedup  = 4.0;
warmup_periods = 5;
sim_count          = 1;
sample_period  = 1000;  
num_vcs     = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits=1;


//customizables
//dragonfly specific parameters
topology = edison;
//end dragonfly specific paramters

routing_function = UGAL_L_mt;	//min/vlb/UGAL_L//UGAL_L_mt //UGAL_mt_src

multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;

seed = 12;

//traffic = uniform;

traffic       = randperm;
perm_seed = 42;

//traffic = shift;
//shift_offset = 384; // = 96 * 4; one group 

injection_rate = 0.50;

log_Qlen_data = 0;	//1 -> true. 0 -> false.

watch_file = watch;
watch_out = watch.out;


END Configuration File: ../../examples/edison_2
topology: edison
inside _RegisterRoutingFunctions() ...
done with _RegisterRoutingFunctions() ...
Routing function registered ...
inside Edison() constructor ...
inside _AllocateArrays() ... 
done with _AllocateArrays() ... 
inside _BuildGraphForLocal() ...
done with _BuildGraphForLocal() ...
/nfsdata/rahman/BooksimResources/edison_globallinks.txt file opened successfully.
_Computesize starts ...
_nodes: 5760
_size: 1440
_channels: 48240
_ComputeSize ends ...
inside _BuildNet() ...
Routers created ...
PEs connected ...
starting to connect the router channels ...
done with _BuildNet() ...
Done with Edison() constructor ...
topology object created ...
inside Run()
inside _SingleSim
_step_count: 200
_step_count: 400
_step_count: 600
_step_count: 800
_step_count: 1000
Class 0:
Packet latency average = 55.1387
	minimum = 4
	maximum = 105
Network latency average = 55.1387
	minimum = 4
	maximum = 105
Slowest packet = 1725290
Flit latency average = 55.1387
	minimum = 4
	maximum = 105
Slowest flit = 1725290
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500126
	minimum = 0.438 (at node 3976)
	maximum = 0.555 (at node 3860)
Accepted packet rate average = 0.472487
	minimum = 0.416 (at node 4685)
	maximum = 0.532 (at node 508)
Injected flit rate average = 0.500126
	minimum = 0.438 (at node 3976)
	maximum = 0.555 (at node 3860)
Accepted flit rate average= 0.472487
	minimum = 0.416 (at node 4685)
	maximum = 0.532 (at node 508)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 159201 (0 measured)
latency change    = 1
throughput change = 1
_step_count: 1200
_step_count: 1400
_step_count: 1600
_step_count: 1800
_step_count: 2000
Class 0:
Packet latency average = 55.2301
	minimum = 4
	maximum = 108
Network latency average = 55.2301
	minimum = 4
	maximum = 108
Slowest packet = 2939522
Flit latency average = 55.2301
	minimum = 4
	maximum = 108
Slowest flit = 2939522
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500073
	minimum = 0.4625 (at node 5666)
	maximum = 0.5395 (at node 3753)
Accepted packet rate average = 0.486239
	minimum = 0.446 (at node 873)
	maximum = 0.5245 (at node 5245)
Injected flit rate average = 0.500073
	minimum = 0.4625 (at node 5666)
	maximum = 0.5395 (at node 3753)
Accepted flit rate average= 0.486239
	minimum = 0.446 (at node 873)
	maximum = 0.5245 (at node 5245)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 159373 (0 measured)
latency change    = 0.00165536
throughput change = 0.0282811
_step_count: 2200
_step_count: 2400
_step_count: 2600
_step_count: 2800
_step_count: 3000
Class 0:
Packet latency average = 55.3131
	minimum = 4
	maximum = 108
Network latency average = 55.3131
	minimum = 4
	maximum = 108
Slowest packet = 6697139
Flit latency average = 55.3131
	minimum = 4
	maximum = 108
Slowest flit = 6697139
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.49969
	minimum = 0.447 (at node 4832)
	maximum = 0.555 (at node 2484)
Accepted packet rate average = 0.499638
	minimum = 0.45 (at node 212)
	maximum = 0.555 (at node 280)
Injected flit rate average = 0.49969
	minimum = 0.447 (at node 4832)
	maximum = 0.555 (at node 2484)
Accepted flit rate average= 0.499638
	minimum = 0.45 (at node 212)
	maximum = 0.555 (at node 280)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 159670 (0 measured)
latency change    = 0.00149972
throughput change = 0.026818
_step_count: 3200
_step_count: 3400
_step_count: 3600
_step_count: 3800
_step_count: 4000
Class 0:
Packet latency average = 55.3145
	minimum = 4
	maximum = 108
Network latency average = 55.3145
	minimum = 4
	maximum = 108
Slowest packet = 6697139
Flit latency average = 55.3145
	minimum = 4
	maximum = 108
Slowest flit = 6697139
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.49986
	minimum = 0.4595 (at node 3626)
	maximum = 0.5405 (at node 5026)
Accepted packet rate average = 0.499844
	minimum = 0.4605 (at node 997)
	maximum = 0.5465 (at node 734)
Injected flit rate average = 0.49986
	minimum = 0.4595 (at node 3626)
	maximum = 0.5405 (at node 5026)
Accepted flit rate average= 0.499844
	minimum = 0.4605 (at node 997)
	maximum = 0.5465 (at node 734)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 159550 (0 measured)
latency change    = 2.65049e-05
throughput change = 0.000412629
_step_count: 4200
_step_count: 4400
_step_count: 4600
_step_count: 4800
_step_count: 5000
Class 0:
Packet latency average = 55.3106
	minimum = 4
	maximum = 108
Network latency average = 55.3106
	minimum = 4
	maximum = 108
Slowest packet = 11924578
Flit latency average = 55.3106
	minimum = 4
	maximum = 108
Slowest flit = 11924578
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500127
	minimum = 0.442 (at node 3853)
	maximum = 0.558 (at node 96)
Accepted packet rate average = 0.500189
	minimum = 0.441 (at node 590)
	maximum = 0.561 (at node 594)
Injected flit rate average = 0.500127
	minimum = 0.442 (at node 3853)
	maximum = 0.558 (at node 96)
Accepted flit rate average= 0.500189
	minimum = 0.441 (at node 590)
	maximum = 0.561 (at node 594)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 159191 (0 measured)
latency change    = 7.03963e-05
throughput change = 0.00068967
Warmed up ...Time used is 5000 cycles
_step_count: 5200
_step_count: 5400
_step_count: 5600
_step_count: 5800
_step_count: 6000
Class 0:
Packet latency average = 55.1682
	minimum = 4
	maximum = 112
Network latency average = 55.1682
	minimum = 4
	maximum = 112
Slowest packet = 15624689
Flit latency average = 55.3032
	minimum = 4
	maximum = 112
Slowest flit = 15624689
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.499836
	minimum = 0.432 (at node 4728)
	maximum = 0.549 (at node 186)
Accepted packet rate average = 0.499854
	minimum = 0.44 (at node 5456)
	maximum = 0.556 (at node 2443)
Injected flit rate average = 0.499836
	minimum = 0.432 (at node 4728)
	maximum = 0.549 (at node 186)
Accepted flit rate average= 0.499854
	minimum = 0.44 (at node 5456)
	maximum = 0.556 (at node 2443)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 159089 (159089 measured)
latency change    = 0.00258251
throughput change = 0.000670334
_step_count: 6200
_step_count: 6400
_step_count: 6600
_step_count: 6800
_step_count: 7000
Class 0:
Packet latency average = 55.2431
	minimum = 4
	maximum = 112
Network latency average = 55.2431
	minimum = 4
	maximum = 112
Slowest packet = 15624689
Flit latency average = 55.3085
	minimum = 4
	maximum = 112
Slowest flit = 15624689
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500134
	minimum = 0.461 (at node 1030)
	maximum = 0.5405 (at node 995)
Accepted packet rate average = 0.500107
	minimum = 0.461 (at node 67)
	maximum = 0.544 (at node 2428)
Injected flit rate average = 0.500134
	minimum = 0.461 (at node 1030)
	maximum = 0.5405 (at node 995)
Accepted flit rate average= 0.500107
	minimum = 0.461 (at node 67)
	maximum = 0.544 (at node 2428)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 159507 (159507 measured)
latency change    = 0.00135617
throughput change = 0.000505447
_step_count: 7200
_step_count: 7400
_step_count: 7600
_step_count: 7800
_step_count: 8000
Class 0:
Packet latency average = 55.2629
	minimum = 4
	maximum = 113
Network latency average = 55.2629
	minimum = 4
	maximum = 113
Slowest packet = 21898755
Flit latency average = 55.3062
	minimum = 4
	maximum = 113
Slowest flit = 21898755
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500063
	minimum = 0.462 (at node 1030)
	maximum = 0.532 (at node 4313)
Accepted packet rate average = 0.500063
	minimum = 0.463 (at node 67)
	maximum = 0.529667 (at node 2824)
Injected flit rate average = 0.500063
	minimum = 0.462 (at node 1030)
	maximum = 0.532 (at node 4313)
Accepted flit rate average= 0.500063
	minimum = 0.463 (at node 67)
	maximum = 0.529667 (at node 2824)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 159190 (159190 measured)
latency change    = 0.000358494
throughput change = 8.79519e-05
Draining all recorded packets ...
done with _SingleSim
Draining remaining packets ...
_step_count: 8200
Time taken is 8204 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 55.3062 (1 samples)
	minimum = 4 (1 samples)
	maximum = 113 (1 samples)
Network latency average = 55.3062 (1 samples)
	minimum = 4 (1 samples)
	maximum = 113 (1 samples)
Flit latency average = 55.3439 (1 samples)
	minimum = 4 (1 samples)
	maximum = 113 (1 samples)
Fragmentation average = 0 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0 (1 samples)
Injected packet rate average = 0.500063 (1 samples)
	minimum = 0.462 (1 samples)
	maximum = 0.532 (1 samples)
Accepted packet rate average = 0.500063 (1 samples)
	minimum = 0.463 (1 samples)
	maximum = 0.529667 (1 samples)
Injected flit rate average = 0.500063 (1 samples)
	minimum = 0.462 (1 samples)
	maximum = 0.532 (1 samples)
Accepted flit rate average = 0.500063 (1 samples)
	minimum = 0.463 (1 samples)
	maximum = 0.529667 (1 samples)
Injected packet size average = 1 (1 samples)
Accepted packet size average = 1 (1 samples)
Hops average = 5.51772 (1 samples)
Stats for UGAL routing for Edison: 
Total flits through min paths: 21769988
Total flits through non-min paths: 1517164
in-group traffic path-len distribution: |1-> 0 , |2-> 107109 , |3-> 74144 , |4-> 0 , |5-> 0 , |6-> 0 , |7-> 0 , |8-> 0 , |9-> 0 , |10-> 0 , |11-> 0 , 
out-group traffic path-len distribution: |1-> 2 , |2-> 22171 , |3-> 220797 , |4-> 12212 , |5-> 49044 , |6-> 310522 , |7-> 651693 , |8-> 69470 , |9-> 0 , |10-> 0 , |11-> 0 , 
done with Run()
Total run time 1231.48
