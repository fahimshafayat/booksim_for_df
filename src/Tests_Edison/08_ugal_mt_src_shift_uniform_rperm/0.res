BEGIN Configuration File: ../../examples/edison
//Defaults
vc_buf_size = 64;
wait_for_tail_credit = 0;

//
// Router architecture
//
vc_allocator = separable_input_first; 
sw_allocator = separable_input_first;
alloc_iters  = 1;
credit_delay   = 2;
routing_delay  = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup     = 1;
output_speedup    = 1;
internal_speedup  = 4.0;
warmup_periods = 5;
sim_count          = 1;
sample_period  = 1000;  
num_vcs     = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits=1;


//customizables
//dragonfly specific parameters
topology = edison;
//end dragonfly specific paramters

routing_function = UGAL_L;	//min/vlb/UGAL_L//UGAL_L_mt //UGAL_mt_src

multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;

seed = 12;

//traffic = uniform;

traffic       = randperm;
perm_seed = 42;

//traffic = shift;
//shift_offset = 384; // = 96 * 4; one group 

injection_rate = 0.50;

log_Qlen_data = 0;	//1 -> true. 0 -> false.

watch_file = watch;
watch_out = watch.out;


END Configuration File: ../../examples/edison
topology: edison
inside _RegisterRoutingFunctions() ...
done with _RegisterRoutingFunctions() ...
Routing function registered ...
inside Edison() constructor ...
inside _AllocateArrays() ... 
done with _AllocateArrays() ... 
inside _BuildGraphForLocal() ...
done with _BuildGraphForLocal() ...
/nfsdata/rahman/BooksimResources/edison_globallinks.txt file opened successfully.
_Computesize starts ...
_nodes: 5760
_size: 1440
_channels: 48240
_ComputeSize ends ...
inside _BuildNet() ...
Routers created ...
PEs connected ...
starting to connect the router channels ...
done with _BuildNet() ...
Done with Edison() constructor ...
topology object created ...
inside Run()
inside _SingleSim
_step_count: 200
_step_count: 400
_step_count: 600
_step_count: 800
_step_count: 1000
Class 0:
Packet latency average = 60.4055
	minimum = 4
	maximum = 135
Network latency average = 60.4055
	minimum = 4
	maximum = 135
Slowest packet = 540271
Flit latency average = 60.4055
	minimum = 4
	maximum = 135
Slowest flit = 540271
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500126
	minimum = 0.438 (at node 3976)
	maximum = 0.555 (at node 3860)
Accepted packet rate average = 0.469834
	minimum = 0.409 (at node 4685)
	maximum = 0.533 (at node 809)
Injected flit rate average = 0.500126
	minimum = 0.438 (at node 3976)
	maximum = 0.555 (at node 3860)
Accepted flit rate average= 0.469834
	minimum = 0.409 (at node 4685)
	maximum = 0.533 (at node 809)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 174487 (0 measured)
latency change    = 1
throughput change = 1
_step_count: 1200
_step_count: 1400
_step_count: 1600
_step_count: 1800
_step_count: 2000
Class 0:
Packet latency average = 60.5287
	minimum = 4
	maximum = 140
Network latency average = 60.5287
	minimum = 4
	maximum = 140
Slowest packet = 4299866
Flit latency average = 60.5287
	minimum = 4
	maximum = 140
Slowest flit = 4299866
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500073
	minimum = 0.4625 (at node 5666)
	maximum = 0.5395 (at node 3753)
Accepted packet rate average = 0.484872
	minimum = 0.4455 (at node 873)
	maximum = 0.526 (at node 5245)
Injected flit rate average = 0.500073
	minimum = 0.4625 (at node 5666)
	maximum = 0.5395 (at node 3753)
Accepted flit rate average= 0.484872
	minimum = 0.4455 (at node 873)
	maximum = 0.526 (at node 5245)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 175115 (0 measured)
latency change    = 0.00203619
throughput change = 0.0310158
_step_count: 2200
_step_count: 2400
_step_count: 2600
_step_count: 2800
_step_count: 3000
Class 0:
Packet latency average = 60.6678
	minimum = 4
	maximum = 137
Network latency average = 60.6678
	minimum = 4
	maximum = 137
Slowest packet = 7071022
Flit latency average = 60.6678
	minimum = 4
	maximum = 137
Slowest flit = 7071022
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.49969
	minimum = 0.447 (at node 4832)
	maximum = 0.555 (at node 2484)
Accepted packet rate average = 0.499664
	minimum = 0.447 (at node 212)
	maximum = 0.556 (at node 4749)
Injected flit rate average = 0.49969
	minimum = 0.447 (at node 4832)
	maximum = 0.555 (at node 2484)
Accepted flit rate average= 0.499664
	minimum = 0.447 (at node 212)
	maximum = 0.556 (at node 4749)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 175264 (0 measured)
latency change    = 0.00229243
throughput change = 0.0296029
_step_count: 3200
_step_count: 3400
_step_count: 3600
_step_count: 3800
_step_count: 4000
Class 0:
Packet latency average = 60.6632
	minimum = 4
	maximum = 137
Network latency average = 60.6632
	minimum = 4
	maximum = 137
Slowest packet = 7071022
Flit latency average = 60.6632
	minimum = 4
	maximum = 137
Slowest flit = 7071022
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.49986
	minimum = 0.4595 (at node 3626)
	maximum = 0.5405 (at node 5026)
Accepted packet rate average = 0.49991
	minimum = 0.459 (at node 4811)
	maximum = 0.548 (at node 734)
Injected flit rate average = 0.49986
	minimum = 0.4595 (at node 3626)
	maximum = 0.5405 (at node 5026)
Accepted flit rate average= 0.49991
	minimum = 0.459 (at node 4811)
	maximum = 0.548 (at node 734)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 174531 (0 measured)
latency change    = 7.54304e-05
throughput change = 0.000493318
_step_count: 4200
_step_count: 4400
_step_count: 4600
_step_count: 4800
_step_count: 5000
Class 0:
Packet latency average = 60.6526
	minimum = 4
	maximum = 136
Network latency average = 60.6526
	minimum = 4
	maximum = 136
Slowest packet = 12156458
Flit latency average = 60.6526
	minimum = 4
	maximum = 136
Slowest flit = 12156458
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500127
	minimum = 0.442 (at node 3853)
	maximum = 0.558 (at node 96)
Accepted packet rate average = 0.50009
	minimum = 0.439 (at node 2741)
	maximum = 0.562 (at node 594)
Injected flit rate average = 0.500127
	minimum = 0.442 (at node 3853)
	maximum = 0.558 (at node 96)
Accepted flit rate average= 0.50009
	minimum = 0.439 (at node 2741)
	maximum = 0.562 (at node 594)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 174744 (0 measured)
latency change    = 0.000175345
throughput change = 0.000359137
Warmed up ...Time used is 5000 cycles
_step_count: 5200
_step_count: 5400
_step_count: 5600
_step_count: 5800
_step_count: 6000
Class 0:
Packet latency average = 60.2209
	minimum = 4
	maximum = 137
Network latency average = 60.2209
	minimum = 4
	maximum = 137
Slowest packet = 16023037
Flit latency average = 60.6413
	minimum = 4
	maximum = 137
Slowest flit = 16023037
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.499836
	minimum = 0.432 (at node 4728)
	maximum = 0.549 (at node 186)
Accepted packet rate average = 0.499882
	minimum = 0.44 (at node 5456)
	maximum = 0.551 (at node 290)
Injected flit rate average = 0.499836
	minimum = 0.432 (at node 4728)
	maximum = 0.549 (at node 186)
Accepted flit rate average= 0.499882
	minimum = 0.44 (at node 5456)
	maximum = 0.551 (at node 290)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 174484 (174484 measured)
latency change    = 0.00716795
throughput change = 0.000416765
_step_count: 6200
_step_count: 6400
_step_count: 6600
_step_count: 6800
_step_count: 7000
Class 0:
Packet latency average = 60.4404
	minimum = 4
	maximum = 137
Network latency average = 60.4404
	minimum = 4
	maximum = 137
Slowest packet = 16023037
Flit latency average = 60.6438
	minimum = 4
	maximum = 137
Slowest flit = 16023037
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500134
	minimum = 0.461 (at node 1030)
	maximum = 0.5405 (at node 995)
Accepted packet rate average = 0.500143
	minimum = 0.4605 (at node 67)
	maximum = 0.5425 (at node 2428)
Injected flit rate average = 0.500134
	minimum = 0.461 (at node 1030)
	maximum = 0.5405 (at node 995)
Accepted flit rate average= 0.500143
	minimum = 0.4605 (at node 67)
	maximum = 0.5425 (at node 2428)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 174640 (174640 measured)
latency change    = 0.00363178
throughput change = 0.000523461
_step_count: 7200
_step_count: 7400
_step_count: 7600
_step_count: 7800
_step_count: 8000
Class 0:
Packet latency average = 60.5117
	minimum = 4
	maximum = 147
Network latency average = 60.5117
	minimum = 4
	maximum = 147
Slowest packet = 22370509
Flit latency average = 60.6459
	minimum = 4
	maximum = 147
Slowest flit = 22370509
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500063
	minimum = 0.462 (at node 1030)
	maximum = 0.532 (at node 4313)
Accepted packet rate average = 0.500061
	minimum = 0.463667 (at node 67)
	maximum = 0.531333 (at node 1140)
Injected flit rate average = 0.500063
	minimum = 0.462 (at node 1030)
	maximum = 0.532 (at node 4313)
Accepted flit rate average= 0.500061
	minimum = 0.463667 (at node 67)
	maximum = 0.531333 (at node 1140)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 174773 (174773 measured)
latency change    = 0.00117778
throughput change = 0.000164332
Draining all recorded packets ...
done with _SingleSim
Draining remaining packets ...
_step_count: 8200
Time taken is 8257 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 60.6476 (1 samples)
	minimum = 4 (1 samples)
	maximum = 147 (1 samples)
Network latency average = 60.6476 (1 samples)
	minimum = 4 (1 samples)
	maximum = 147 (1 samples)
Flit latency average = 60.7646 (1 samples)
	minimum = 4 (1 samples)
	maximum = 147 (1 samples)
Fragmentation average = 0 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0 (1 samples)
Injected packet rate average = 0.500063 (1 samples)
	minimum = 0.462 (1 samples)
	maximum = 0.532 (1 samples)
Accepted packet rate average = 0.500061 (1 samples)
	minimum = 0.463667 (1 samples)
	maximum = 0.531333 (1 samples)
Injected flit rate average = 0.500063 (1 samples)
	minimum = 0.462 (1 samples)
	maximum = 0.532 (1 samples)
Accepted flit rate average = 0.500061 (1 samples)
	minimum = 0.463667 (1 samples)
	maximum = 0.531333 (1 samples)
Injected packet size average = 1 (1 samples)
Accepted packet size average = 1 (1 samples)
Hops average = 5.97024 (1 samples)
Stats for UGAL routing for Edison: 
Total flits through min paths: 20171062
Total flits through non-min paths: 3199248
in-group traffic path-len distribution: |1-> 0 , |2-> 20355 , |3-> 22995 , |4-> 122079 , |5-> 0 , |6-> 0 , |7-> 0 , |8-> 0 , |9-> 0 , |10-> 0 , |11-> 0 , 
out-group traffic path-len distribution: |1-> 0 , |2-> 4043 , |3-> 68388 , |4-> 127747 , |5-> 1213 , |6-> 16504 , |7-> 125937 , |8-> 528968 , |9-> 1151681 , |10-> 1009338 , |11-> 0 , 
done with Run()
Total run time 1266.59
