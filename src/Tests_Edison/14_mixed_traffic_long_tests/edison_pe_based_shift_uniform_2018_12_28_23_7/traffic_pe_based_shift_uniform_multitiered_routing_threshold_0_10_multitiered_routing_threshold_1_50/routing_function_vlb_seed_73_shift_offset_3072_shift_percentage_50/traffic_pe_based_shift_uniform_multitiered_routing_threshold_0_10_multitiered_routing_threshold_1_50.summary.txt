vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = pe_based_shift_uniform;
routing_function = vlb;
seed = 73;
shift_offset = 3072;
shift_percentage = 50;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.43145,    0.5002,    669.05,    669.05,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,      0.25,   0.25005,    104.33,    104.39,         0,  0,  11697847,  ,  ,  ,  
simulation,         3,      0.37,   0.37007,   0.37011,    109.48,    109.52,         0,  0,  17380524,  ,  ,  ,  
simulation,         4,      0.43,   0.42333,   0.42995,    233.69,    292.99,         0,  0,  39919442,  ,  ,  ,  
simulation,         5,      0.46,   0.42571,   0.45998,    508.22,    530.94,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.42529,   0.43993,     369.4,    550.92,         0,  0,  49049335,  ,  ,  ,  
simulation,         7,      0.45,   0.42565,   0.44985,    431.71,    440.28,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.40012,   0.40018,    115.99,    116.01,         0,  0,  18945481,  ,  ,  ,  
simulation,        10,      0.35,   0.35001,   0.35006,    107.76,    107.81,         0,  0,  16410267,  ,  ,  ,  
simulation,        11,       0.3,       0.3,   0.30005,    105.53,    105.58,         0,  0,  14039489,  ,  ,  ,  
