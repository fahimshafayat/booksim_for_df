vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = pe_based_shift_uniform;
routing_function = min;
seed = 74;
shift_offset = 3072;
shift_percentage = 25;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.41259,    0.4738,    244.47,    244.47,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.23555,   0.24896,    221.51,    352.95,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,   0.12996,   0.12997,    53.024,    53.054,         0,  6035334,  0,  ,  ,  ,  
simulation,         4,      0.19,   0.18932,   0.19006,     94.01,    117.47,         0,  25969180,  0,  ,  ,  ,  
simulation,         5,      0.22,   0.21401,   0.22002,    158.79,    205.22,         1,  0,  0,  ,  ,  ,  
simulation,         6,       0.2,   0.19825,   0.20004,    148.51,    201.85,         0,  32432714,  0,  ,  ,  ,  
simulation,         7,      0.21,   0.20642,   0.21004,    125.74,    152.11,         1,  0,  0,  ,  ,  ,  
simulation,         9,      0.15,   0.14994,   0.14994,    53.284,    53.315,         0,  6964655,  0,  ,  ,  ,  
simulation,        10,       0.1,  0.099986,       0.1,    52.817,    52.849,         0,  4641185,  0,  ,  ,  ,  
simulation,        11,      0.05,  0.050035,  0.050028,    52.612,    52.647,         0,  2322604,  0,  ,  ,  ,  
