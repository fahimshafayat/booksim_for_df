vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = pe_based_shift_uniform;
routing_function = UGAL_mt_src;
seed = 74;
shift_offset = 384;
shift_percentage = 50;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49265,   0.49969,    176.72,    229.21,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25012,   0.25005,    184.64,     191.1,         0,  13308991,  5043673,  ,  ,  ,  
simulation,         3,      0.37,   0.37018,    0.3701,    192.59,     199.5,         0,  20591261,  11565162,  ,  ,  ,  
simulation,         4,      0.43,   0.42999,   0.43005,    200.43,     208.7,         0,  24686865,  15541444,  ,  ,  ,  
simulation,         5,      0.46,   0.45934,   0.46005,    214.12,    235.69,         0,  38796201,  25787143,  ,  ,  ,  
simulation,         6,      0.48,   0.47766,   0.48006,    238.97,    316.35,         0,  48587806,  32893488,  ,  ,  ,  
simulation,         7,      0.49,   0.48632,   0.48998,    261.72,    361.99,         0,  57472323,  39161320,  ,  ,  ,  
simulation,         8,      0.45,   0.44965,   0.45003,    207.66,    219.64,         0,  31306486,  20439339,  ,  ,  ,  
simulation,         9,       0.4,   0.40003,   0.40008,    196.04,    203.53,         0,  21128407,  12582303,  ,  ,  ,  
simulation,        10,      0.35,   0.35005,   0.35006,    190.49,    198.02,         0,  17718355,  9480549,  ,  ,  ,  
simulation,        11,       0.3,   0.30009,   0.30003,    184.18,    191.47,         0,  14987035,  6948445,  ,  ,  ,  
