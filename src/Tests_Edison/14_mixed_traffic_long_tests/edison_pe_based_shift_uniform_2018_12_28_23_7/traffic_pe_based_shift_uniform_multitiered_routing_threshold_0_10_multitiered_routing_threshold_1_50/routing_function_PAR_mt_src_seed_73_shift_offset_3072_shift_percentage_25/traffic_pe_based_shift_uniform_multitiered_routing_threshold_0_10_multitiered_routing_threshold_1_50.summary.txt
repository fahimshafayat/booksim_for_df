vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = pe_based_shift_uniform;
routing_function = PAR_mt_src;
seed = 73;
shift_offset = 3072;
shift_percentage = 25;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.46634,   0.49981,    368.75,     381.2,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25001,   0.25005,    85.079,    85.311,         0,  3770925,  7924918,  ,  ,  ,  
simulation,         3,      0.37,   0.37007,   0.37011,    88.785,    89.003,         0,  4864519,  12483822,  ,  ,  ,  
simulation,         4,      0.43,   0.43008,   0.43013,    99.675,    100.03,         0,  6365048,  18947817,  ,  ,  ,  
simulation,         5,      0.46,   0.45753,   0.46004,    131.22,    146.25,         0,  11941780,  41007638,  ,  ,  ,  
simulation,         6,      0.48,   0.46506,      0.48,    247.33,    325.36,         0,  9191108,  44562987,  ,  ,  ,  
simulation,         7,      0.49,     0.465,      0.49,    267.47,    274.48,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44914,   0.45017,    114.43,    116.26,         0,  8019560,  25568929,  ,  ,  ,  
simulation,         9,       0.4,   0.40017,   0.40018,    91.651,    91.882,         0,  5182346,  14248099,  ,  ,  ,  
simulation,        10,      0.35,   0.35003,   0.35006,    87.849,    88.079,         0,  4726947,  11666597,  ,  ,  ,  
simulation,        11,       0.3,   0.30001,   0.30005,    86.309,    86.534,         0,  4297980,  9746333,  ,  ,  ,  
