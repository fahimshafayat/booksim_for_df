vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = pe_based_shift_uniform;
routing_function = UGAL_L;
seed = 74;
shift_offset = 384;
shift_percentage = 25;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49857,   0.50002,    167.83,    197.02,         0,  78137569,  22676591,  ,  ,  ,  
simulation,         2,      0.74,   0.64591,   0.71044,    503.63,    502.63,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.58769,   0.60899,    290.47,     352.1,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55021,    0.5584,    155.01,    192.45,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.53,   0.52566,   0.52966,    203.09,    245.01,         0,  68256897,  20069814,  ,  ,  ,  
simulation,         6,      0.54,   0.53431,   0.53946,    217.35,    278.56,         0,  70831568,  20812598,  ,  ,  ,  
simulation,         7,      0.55,   0.54275,   0.54933,    234.91,    303.69,         0,  73528082,  21580112,  ,  ,  ,  
simulation,        10,      0.45,   0.44998,   0.45003,    143.85,    147.55,         0,  35780878,  9825417,  ,  ,  ,  
simulation,        11,       0.4,   0.40012,   0.40008,    132.38,    135.88,         0,  25382165,  6467679,  ,  ,  ,  
