vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = pe_based_shift_uniform;
routing_function = PAR_mt_src;
seed = 74;
shift_offset = 384;
shift_percentage = 75;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.45368,   0.49991,     527.1,    594.34,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25007,   0.25004,    93.824,    93.972,         0,  1974917,  9753047,  ,  ,  ,  
simulation,         3,      0.37,   0.36985,    0.3701,    142.09,    142.95,         0,  4510471,  29117032,  ,  ,  ,  
simulation,         4,      0.43,   0.42751,   0.43005,    202.72,    215.31,         0,  7407526,  49972117,  ,  ,  ,  
simulation,         5,      0.46,   0.45331,   0.46005,    295.56,    419.42,         0,  8899916,  55494957,  ,  ,  ,  
simulation,         6,      0.48,   0.46069,   0.47994,    379.66,    397.36,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.46037,   0.47005,    290.46,    304.75,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44598,   0.45003,     248.3,    278.66,         0,  5879734,  39006438,  ,  ,  ,  
simulation,         9,       0.4,   0.39915,   0.40008,    159.85,    168.63,         0,  5117974,  34131946,  ,  ,  ,  
simulation,        10,      0.35,   0.34951,   0.35006,     134.0,    135.29,         0,  4663454,  28946399,  ,  ,  ,  
simulation,        11,       0.3,   0.30005,    0.3001,    106.88,    107.37,         0,  2486572,  14346678,  ,  ,  ,  
