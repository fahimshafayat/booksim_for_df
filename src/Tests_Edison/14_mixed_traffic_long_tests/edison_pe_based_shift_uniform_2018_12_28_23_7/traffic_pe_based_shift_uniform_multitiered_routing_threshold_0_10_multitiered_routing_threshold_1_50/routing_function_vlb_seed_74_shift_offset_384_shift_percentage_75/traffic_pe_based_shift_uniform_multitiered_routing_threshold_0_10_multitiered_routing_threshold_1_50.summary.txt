vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = pe_based_shift_uniform;
routing_function = vlb;
seed = 74;
shift_offset = 384;
shift_percentage = 75;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.42128,   0.49997,    566.63,    566.63,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25004,    105.44,    105.48,         0,  0,  11694866,  ,  ,  ,  
simulation,         3,      0.37,   0.37015,   0.37014,    112.18,    112.19,         0,  0,  17409191,  ,  ,  ,  
simulation,         4,      0.43,   0.41577,   0.43005,    399.86,     559.5,         0,  0,  47886213,  ,  ,  ,  
simulation,         5,      0.46,   0.41715,   0.45996,    533.38,    631.98,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.41581,   0.44003,    486.05,    491.32,         1,  0,  0,  ,  ,  ,  
simulation,         7,       0.4,    0.4001,   0.40011,    127.36,    127.38,         0,  0,  19162836,  ,  ,  ,  
simulation,         8,      0.35,    0.3501,   0.35008,    109.62,    109.64,         0,  0,  16415026,  ,  ,  ,  
simulation,         9,       0.3,   0.30012,    0.3001,    106.81,    106.84,         0,  0,  14047734,  ,  ,  ,  
