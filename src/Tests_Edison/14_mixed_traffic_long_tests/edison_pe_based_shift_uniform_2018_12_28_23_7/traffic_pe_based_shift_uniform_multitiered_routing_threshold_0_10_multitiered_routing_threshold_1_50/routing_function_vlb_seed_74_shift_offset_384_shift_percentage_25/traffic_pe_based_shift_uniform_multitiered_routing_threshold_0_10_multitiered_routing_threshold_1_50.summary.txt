vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = pe_based_shift_uniform;
routing_function = vlb;
seed = 74;
shift_offset = 384;
shift_percentage = 25;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.44856,   0.49996,    587.36,    693.32,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25004,    103.28,    103.35,         0,  0,  11692691,  ,  ,  ,  
simulation,         3,      0.37,   0.37016,   0.37014,     107.4,    107.46,         0,  0,  17341907,  ,  ,  ,  
simulation,         4,      0.43,    0.4299,   0.43006,    126.22,    126.32,         0,  0,  21421209,  ,  ,  ,  
simulation,         5,      0.46,   0.44619,   0.46005,    374.65,    501.03,         0,  0,  46692823,  ,  ,  ,  
simulation,         6,      0.48,    0.4478,   0.48001,    539.35,    552.38,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.44693,   0.46995,    453.55,    457.73,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44371,   0.45003,    242.43,    304.98,         0,  0,  43846070,  ,  ,  ,  
simulation,         9,       0.4,   0.40014,   0.40011,    110.85,     110.9,         0,  0,  18803727,  ,  ,  ,  
simulation,        10,      0.35,   0.35011,   0.35008,    106.17,    106.23,         0,  0,  16393817,  ,  ,  ,  
simulation,        11,       0.3,   0.30011,    0.3001,    104.33,     104.4,         0,  0,  14037832,  ,  ,  ,  
