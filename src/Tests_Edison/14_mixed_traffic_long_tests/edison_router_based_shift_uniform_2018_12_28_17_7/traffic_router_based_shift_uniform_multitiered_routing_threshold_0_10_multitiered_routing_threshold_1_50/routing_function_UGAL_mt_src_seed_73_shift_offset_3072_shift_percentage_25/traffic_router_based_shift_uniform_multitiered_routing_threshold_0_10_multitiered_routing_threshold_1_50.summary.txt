vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = router_based_shift_uniform;
routing_function = UGAL_mt_src;
seed = 73;
shift_offset = 3072;
shift_percentage = 25;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49982,       0.5,    131.88,    135.47,         0,  44018729,  10410304,  ,  ,  ,  
simulation,         2,      0.74,   0.61863,   0.71948,    525.24,    524.86,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,   0.58835,   0.61619,     318.2,    354.93,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,    0.5584,   0.55988,    162.26,    183.83,         0,  84522663,  22376892,  ,  ,  ,  
simulation,         5,      0.59,   0.57887,   0.58899,    217.49,    241.62,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.57,   0.56723,   0.56983,    180.38,    227.21,         0,  92002844,  24844796,  ,  ,  ,  
simulation,         7,      0.58,   0.57325,   0.57943,    226.57,    312.24,         0,  77881726,  21484336,  ,  ,  ,  
simulation,         8,      0.55,   0.54893,   0.54995,    150.96,    164.25,         0,  74277354,  19289182,  ,  ,  ,  
simulation,        10,      0.45,   0.45001,   0.44999,    126.48,    128.84,         0,  37028945,  7833617,  ,  ,  ,  
simulation,        11,       0.4,   0.39999,       0.4,    122.78,    125.05,         0,  28676886,  5235049,  ,  ,  ,  
