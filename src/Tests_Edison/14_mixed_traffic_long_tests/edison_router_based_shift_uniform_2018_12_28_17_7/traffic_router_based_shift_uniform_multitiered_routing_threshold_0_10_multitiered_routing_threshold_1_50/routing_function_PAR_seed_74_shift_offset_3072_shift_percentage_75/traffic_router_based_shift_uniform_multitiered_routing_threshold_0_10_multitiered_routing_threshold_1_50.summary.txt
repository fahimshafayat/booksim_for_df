vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = router_based_shift_uniform;
routing_function = PAR;
seed = 74;
shift_offset = 3072;
shift_percentage = 75;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.44514,   0.49993,    486.77,    630.09,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25008,   0.25004,    106.38,    106.66,         0,  2960464,  8784820,  ,  ,  ,  
simulation,         3,      0.37,   0.37012,   0.37014,    118.17,    118.56,         0,  4038631,  14654113,  ,  ,  ,  
simulation,         4,      0.43,   0.42981,   0.43005,    178.55,    175.51,         0,  12222637,  54278860,  ,  ,  ,  
simulation,         5,      0.46,   0.45039,   0.46005,    309.28,    568.76,         0,  13859289,  66371871,  ,  ,  ,  
simulation,         6,      0.48,   0.44707,   0.48005,    492.44,    513.04,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.47,   0.44384,   0.47005,    392.63,    407.64,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44868,   0.45003,    217.73,     230.5,         0,  8715559,  42861951,  ,  ,  ,  
simulation,         9,       0.4,   0.40013,   0.40011,    130.66,    131.43,         0,  4574693,  18005099,  ,  ,  ,  
simulation,        10,      0.35,   0.35012,   0.35008,    112.45,    112.71,         0,  3683025,  12798415,  ,  ,  ,  
simulation,        11,       0.3,   0.30013,    0.3001,    108.98,    109.23,         0,  3365521,  10744264,  ,  ,  ,  
