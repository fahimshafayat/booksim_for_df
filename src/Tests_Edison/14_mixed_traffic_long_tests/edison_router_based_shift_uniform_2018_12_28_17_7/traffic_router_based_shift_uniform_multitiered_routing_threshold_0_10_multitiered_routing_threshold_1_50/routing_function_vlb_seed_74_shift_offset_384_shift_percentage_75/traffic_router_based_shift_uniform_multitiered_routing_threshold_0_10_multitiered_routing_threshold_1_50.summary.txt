vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = router_based_shift_uniform;
routing_function = vlb;
seed = 74;
shift_offset = 384;
shift_percentage = 75;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.42129,   0.49997,    568.46,    568.46,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25007,   0.25004,    105.49,    105.53,         0,  0,  11697729,  ,  ,  ,  
simulation,         3,      0.37,   0.37017,   0.37014,    112.22,    112.23,         0,  0,  17402726,  ,  ,  ,  
simulation,         4,      0.43,   0.41454,   0.43005,    355.86,    358.05,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.40002,   0.40011,    127.52,    127.61,         0,  0,  20529610,  ,  ,  ,  
simulation,         6,      0.41,   0.40868,   0.41007,    156.96,    161.24,         0,  0,  24487365,  ,  ,  ,  
simulation,         7,      0.42,   0.41322,   0.42006,    256.67,    340.03,         0,  0,  41460943,  ,  ,  ,  
simulation,         9,      0.35,   0.35011,   0.35008,    109.67,    109.69,         0,  0,  16425026,  ,  ,  ,  
simulation,        10,       0.3,   0.30012,    0.3001,    106.85,    106.88,         0,  0,  14047951,  ,  ,  ,  
