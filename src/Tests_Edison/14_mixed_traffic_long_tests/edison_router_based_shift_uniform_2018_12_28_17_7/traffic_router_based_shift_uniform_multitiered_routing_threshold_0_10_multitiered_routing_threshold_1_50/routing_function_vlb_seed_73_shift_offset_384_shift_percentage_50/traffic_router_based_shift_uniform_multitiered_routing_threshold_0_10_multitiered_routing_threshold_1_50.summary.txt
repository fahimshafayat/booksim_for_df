vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = router_based_shift_uniform;
routing_function = vlb;
seed = 73;
shift_offset = 384;
shift_percentage = 50;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.43462,    0.5002,    663.59,    663.59,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25001,   0.25005,    104.36,    104.41,         0,  0,  11696102,  ,  ,  ,  
simulation,         3,      0.37,   0.37006,   0.37011,    109.58,    109.61,         0,  0,  17387359,  ,  ,  ,  
simulation,         4,      0.43,   0.42384,   0.42995,    236.44,    316.16,         0,  0,  42536457,  ,  ,  ,  
simulation,         5,      0.46,   0.42904,   0.45988,    486.53,    511.05,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.44,   0.42582,   0.43992,    371.12,    548.16,         0,  0,  49011400,  ,  ,  ,  
simulation,         7,      0.45,   0.42701,   0.44989,     413.2,    422.62,         1,  0,  0,  ,  ,  ,  
simulation,         9,       0.4,   0.40014,   0.40018,    116.45,    116.49,         0,  0,  18959303,  ,  ,  ,  
simulation,        10,      0.35,      0.35,   0.35006,    107.83,    107.87,         0,  0,  16412288,  ,  ,  ,  
simulation,        11,       0.3,   0.30001,   0.30005,    105.57,    105.62,         0,  0,  14042880,  ,  ,  ,  
