vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = router_based_shift_uniform;
routing_function = UGAL_L;
seed = 74;
shift_offset = 384;
shift_percentage = 25;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.49861,   0.50005,    161.35,    187.05,         0,  60574657,  16318305,  ,  ,  ,  
simulation,         2,      0.74,   0.63464,   0.71374,    477.47,    476.62,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.62,    0.5839,   0.60866,    287.22,    345.19,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.56,   0.55088,   0.55824,    255.48,    306.38,         0,  90212008,  24564548,  ,  ,  ,  
simulation,         5,      0.59,   0.57097,   0.58535,    210.82,    255.61,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.57,   0.55889,   0.56771,    162.22,    199.75,         1,  0,  0,  ,  ,  ,  
simulation,         7,      0.55,   0.54267,   0.54896,    232.45,    279.99,         0,  81818233,  22324194,  ,  ,  ,  
simulation,         9,      0.45,    0.4499,   0.45003,    140.46,    143.86,         0,  38968955,  9956718,  ,  ,  ,  
simulation,        10,       0.4,   0.40005,   0.40008,     127.3,    130.22,         0,  26084675,  6176532,  ,  ,  ,  
