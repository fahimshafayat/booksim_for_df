vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = router_based_shift_uniform;
routing_function = min;
seed = 74;
shift_offset = 384;
shift_percentage = 75;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.17543,    0.4993,    351.38,    351.38,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.12036,   0.25001,    368.14,    368.14,         1,  0,  0,  ,  ,  ,  
simulation,         3,      0.13,  0.094665,   0.13001,    592.04,    592.04,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.07,  0.069962,  0.069961,    55.672,    55.707,         0,  3259400,  0,  ,  ,  ,  
simulation,         5,       0.1,  0.087263,  0.099782,    530.32,    530.32,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.08,  0.079705,  0.079978,    93.774,    100.19,         0,  6762824,  0,  ,  ,  ,  
simulation,         7,      0.09,  0.084801,  0.089954,    386.88,    427.32,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.05,   0.05003,  0.050028,    54.132,    54.163,         0,  2324840,  0,  ,  ,  ,  
