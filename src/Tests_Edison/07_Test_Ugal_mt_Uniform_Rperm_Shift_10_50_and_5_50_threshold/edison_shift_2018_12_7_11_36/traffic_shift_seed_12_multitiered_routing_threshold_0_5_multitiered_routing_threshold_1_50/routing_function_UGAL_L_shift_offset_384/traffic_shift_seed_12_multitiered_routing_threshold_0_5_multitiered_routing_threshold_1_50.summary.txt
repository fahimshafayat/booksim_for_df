vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 5;
multitiered_routing_threshold_1 = 50;
seed = 12;
topology = edison;
traffic = shift;
routing_function = UGAL_L;
shift_offset = 384;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.43507,   0.49969,    330.31,    330.31,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24955,   0.24991,    396.96,    417.25,         0,  5735581,  15832229,  ,  ,  ,  
simulation,         3,      0.37,   0.34908,   0.37003,    354.88,    354.88,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,   0.30473,   0.31016,    386.44,    386.44,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27804,   0.27993,    316.38,    405.64,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,   0.25939,   0.25992,     405.6,    427.69,         0,  5762323,  16671232,  ,  ,  ,  
simulation,         7,      0.27,   0.26885,   0.26991,     424.8,    447.22,         0,  6493705,  19752718,  ,  ,  ,  
simulation,         9,       0.2,   0.20008,   0.19995,    364.04,    381.01,         0,  4992846,  10186474,  ,  ,  ,  
simulation,        10,      0.15,   0.15019,   0.14996,    338.58,    350.05,         0,  4579129,  5994126,  ,  ,  ,  
simulation,        11,       0.1,  0.099961,   0.09992,    82.237,    82.389,         0,  2811687,  1877261,  ,  ,  ,  
