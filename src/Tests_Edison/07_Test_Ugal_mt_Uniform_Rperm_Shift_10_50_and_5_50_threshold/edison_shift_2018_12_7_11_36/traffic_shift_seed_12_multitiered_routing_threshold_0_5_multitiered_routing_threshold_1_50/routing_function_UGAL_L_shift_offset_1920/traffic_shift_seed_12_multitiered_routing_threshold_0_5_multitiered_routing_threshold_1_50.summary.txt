vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 5;
multitiered_routing_threshold_1 = 50;
seed = 12;
topology = edison;
traffic = shift;
routing_function = UGAL_L;
shift_offset = 1920;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4349,   0.49969,    328.27,    328.27,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24958,   0.24991,    391.95,    411.56,         0,  5841822,  16182949,  ,  ,  ,  
simulation,         3,      0.37,   0.34991,   0.37003,    354.83,    354.83,         1,  0,  0,  ,  ,  ,  
simulation,         4,      0.31,     0.305,   0.31016,    383.68,    383.68,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.28,   0.27812,   0.27993,    315.04,    398.45,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.26,   0.25942,   0.25992,    400.94,    423.99,         0,  5673916,  16403431,  ,  ,  ,  
simulation,         7,      0.27,   0.26889,   0.26991,    417.18,    440.52,         0,  6138630,  18612893,  ,  ,  ,  
simulation,         9,       0.2,   0.20006,   0.19995,    361.49,    377.46,         0,  5115478,  10457244,  ,  ,  ,  
simulation,        10,      0.15,   0.15018,   0.14996,    336.77,    347.74,         0,  4684222,  6146596,  ,  ,  ,  
simulation,        11,       0.1,  0.099963,   0.09992,    81.778,    81.915,         0,  2806458,  1897453,  ,  ,  ,  
