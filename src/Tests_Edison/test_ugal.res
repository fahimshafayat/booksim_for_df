BEGIN Configuration File: examples/edison_ugal
//Defaults
vc_buf_size = 64;
wait_for_tail_credit = 0;

//
// Router architecture
//
vc_allocator = separable_input_first; 
sw_allocator = separable_input_first;
alloc_iters  = 1;
credit_delay   = 2;
routing_delay  = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup     = 1;
output_speedup    = 1;
internal_speedup  = 4.0;
warmup_periods = 5;
sim_count          = 1;
sample_period  = 1000;  
num_vcs     = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits=1;


//customizables
//dragonfly specific parameters
topology = edison;
//end dragonfly specific paramters

routing_function = UGAL_L;	//min/vlb/UGAL_L//UGAL_L_mt

seed = 13;

traffic       = randperm;
perm_seed = 42;

injection_rate = 0.50;

log_Qlen_data = 0;	//1 -> true. 0 -> false.

watch_file = watch;
watch_out = watch.out;


END Configuration File: examples/edison_ugal
topology: edison
inside _RegisterRoutingFunctions() ...
done with _RegisterRoutingFunctions() ...
Routing function registered ...
inside Edison() constructor ...
inside _AllocateArrays() ... 
done with _AllocateArrays() ... 
inside _BuildGraphForLocal() ...
done with _BuildGraphForLocal() ...
edison_globallinks.txt file opened successfully.
_Computesize starts ...
_nodes: 5760
_size: 1440
_channels: 48240
_ComputeSize ends ...
inside _BuildNet() ...
Routers created ...
PEs connected ...
starting to connect the router channels ...
done with _BuildNet() ...
Done with Edison() constructor ...
topology object created ...
inside Run()
inside _SingleSim
_step_count: 200
_step_count: 400
_step_count: 600
_step_count: 800
_step_count: 1000
Class 0:
Packet latency average = 60.4126
	minimum = 4
	maximum = 143
Network latency average = 60.4126
	minimum = 4
	maximum = 143
Slowest packet = 708804
Flit latency average = 60.4126
	minimum = 4
	maximum = 143
Slowest flit = 708804
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.499977
	minimum = 0.446 (at node 2244)
	maximum = 0.553 (at node 5277)
Accepted packet rate average = 0.46962
	minimum = 0.409 (at node 1529)
	maximum = 0.525 (at node 2129)
Injected flit rate average = 0.499977
	minimum = 0.446 (at node 2244)
	maximum = 0.553 (at node 5277)
Accepted flit rate average= 0.46962
	minimum = 0.409 (at node 1529)
	maximum = 0.525 (at node 2129)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 174855 (0 measured)
latency change    = 1
throughput change = 1
_step_count: 1200
_step_count: 1400
_step_count: 1600
_step_count: 1800
_step_count: 2000
Class 0:
Packet latency average = 60.533
	minimum = 4
	maximum = 143
Network latency average = 60.533
	minimum = 4
	maximum = 143
Slowest packet = 708804
Flit latency average = 60.533
	minimum = 4
	maximum = 143
Slowest flit = 708804
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500076
	minimum = 0.4595 (at node 4241)
	maximum = 0.5505 (at node 706)
Accepted packet rate average = 0.484892
	minimum = 0.443 (at node 2803)
	maximum = 0.535 (at node 248)
Injected flit rate average = 0.500076
	minimum = 0.4595 (at node 4241)
	maximum = 0.5505 (at node 706)
Accepted flit rate average= 0.484892
	minimum = 0.443 (at node 2803)
	maximum = 0.535 (at node 248)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 174914 (0 measured)
latency change    = 0.00199007
throughput change = 0.0314961
_step_count: 2200
_step_count: 2400
_step_count: 2600
_step_count: 2800
_step_count: 3000
Class 0:
Packet latency average = 60.6326
	minimum = 4
	maximum = 138
Network latency average = 60.6326
	minimum = 4
	maximum = 138
Slowest packet = 7113843
Flit latency average = 60.6326
	minimum = 4
	maximum = 138
Slowest flit = 7113843
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500109
	minimum = 0.447 (at node 349)
	maximum = 0.562 (at node 5211)
Accepted packet rate average = 0.500105
	minimum = 0.446 (at node 5498)
	maximum = 0.558 (at node 916)
Injected flit rate average = 0.500109
	minimum = 0.447 (at node 349)
	maximum = 0.562 (at node 5211)
Accepted flit rate average= 0.500105
	minimum = 0.446 (at node 5498)
	maximum = 0.558 (at node 916)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 174935 (0 measured)
latency change    = 0.00164285
throughput change = 0.0304193
_step_count: 3200
_step_count: 3400
_step_count: 3600
_step_count: 3800
_step_count: 4000
Class 0:
Packet latency average = 60.6405
	minimum = 4
	maximum = 138
Network latency average = 60.6405
	minimum = 4
	maximum = 138
Slowest packet = 7113843
Flit latency average = 60.6405
	minimum = 4
	maximum = 138
Slowest flit = 7113843
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500026
	minimum = 0.4565 (at node 1095)
	maximum = 0.539 (at node 3226)
Accepted packet rate average = 0.500014
	minimum = 0.4595 (at node 3962)
	maximum = 0.5395 (at node 1562)
Injected flit rate average = 0.500026
	minimum = 0.4565 (at node 1095)
	maximum = 0.539 (at node 3226)
Accepted flit rate average= 0.500014
	minimum = 0.4595 (at node 3962)
	maximum = 0.5395 (at node 1562)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 175054 (0 measured)
latency change    = 0.000129603
throughput change = 0.000182808
_step_count: 4200
_step_count: 4400
_step_count: 4600
_step_count: 4800
_step_count: 5000
Class 0:
Packet latency average = 60.6484
	minimum = 4
	maximum = 137
Network latency average = 60.6484
	minimum = 4
	maximum = 137
Slowest packet = 12881282
Flit latency average = 60.6484
	minimum = 4
	maximum = 137
Slowest flit = 12881282
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.499879
	minimum = 0.439 (at node 2065)
	maximum = 0.555 (at node 2885)
Accepted packet rate average = 0.499904
	minimum = 0.444 (at node 44)
	maximum = 0.557 (at node 2950)
Injected flit rate average = 0.499879
	minimum = 0.439 (at node 2065)
	maximum = 0.555 (at node 2885)
Accepted flit rate average= 0.499904
	minimum = 0.444 (at node 44)
	maximum = 0.557 (at node 2950)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 174908 (0 measured)
latency change    = 0.000130955
throughput change = 0.000218618
Warmed up ...Time used is 5000 cycles
_step_count: 5200
_step_count: 5400
_step_count: 5600
_step_count: 5800
_step_count: 6000
Class 0:
Packet latency average = 60.2378
	minimum = 4
	maximum = 138
Network latency average = 60.2378
	minimum = 4
	maximum = 138
Slowest packet = 15011692
Flit latency average = 60.6577
	minimum = 4
	maximum = 138
Slowest flit = 15011692
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.499599
	minimum = 0.442 (at node 2260)
	maximum = 0.551 (at node 4989)
Accepted packet rate average = 0.499669
	minimum = 0.443 (at node 1237)
	maximum = 0.559 (at node 131)
Injected flit rate average = 0.499599
	minimum = 0.442 (at node 2260)
	maximum = 0.551 (at node 4989)
Accepted flit rate average= 0.499669
	minimum = 0.443 (at node 1237)
	maximum = 0.559 (at node 131)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 174507 (174507 measured)
latency change    = 0.00681787
throughput change = 0.000471145
_step_count: 6200
_step_count: 6400
_step_count: 6600
_step_count: 6800
_step_count: 7000
Class 0:
Packet latency average = 60.4426
	minimum = 4
	maximum = 143
Network latency average = 60.4426
	minimum = 4
	maximum = 143
Slowest packet = 17724325
Flit latency average = 60.6463
	minimum = 4
	maximum = 143
Slowest flit = 17724325
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.499838
	minimum = 0.458 (at node 5181)
	maximum = 0.5435 (at node 3276)
Accepted packet rate average = 0.499891
	minimum = 0.4585 (at node 5462)
	maximum = 0.543 (at node 2669)
Injected flit rate average = 0.499838
	minimum = 0.458 (at node 5181)
	maximum = 0.5435 (at node 3276)
Accepted flit rate average= 0.499891
	minimum = 0.4585 (at node 5462)
	maximum = 0.543 (at node 2669)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 174292 (174292 measured)
latency change    = 0.00338984
throughput change = 0.000445236
_step_count: 7200
_step_count: 7400
_step_count: 7600
_step_count: 7800
_step_count: 8000
Class 0:
Packet latency average = 60.518
	minimum = 4
	maximum = 143
Network latency average = 60.518
	minimum = 4
	maximum = 143
Slowest packet = 17724325
Flit latency average = 60.6522
	minimum = 4
	maximum = 143
Slowest flit = 17724325
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.499856
	minimum = 0.470333 (at node 970)
	maximum = 0.538 (at node 4615)
Accepted packet rate average = 0.49989
	minimum = 0.471 (at node 2591)
	maximum = 0.534333 (at node 5260)
Injected flit rate average = 0.499856
	minimum = 0.470333 (at node 970)
	maximum = 0.538 (at node 4615)
Accepted flit rate average= 0.49989
	minimum = 0.471 (at node 2591)
	maximum = 0.534333 (at node 5260)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 174334 (174334 measured)
latency change    = 0.00124491
throughput change = 3.70452e-06
Draining all recorded packets ...
done with _SingleSim
Draining remaining packets ...
_step_count: 8200
Time taken is 8260 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 60.6522 (1 samples)
	minimum = 4 (1 samples)
	maximum = 143 (1 samples)
Network latency average = 60.6522 (1 samples)
	minimum = 4 (1 samples)
	maximum = 143 (1 samples)
Flit latency average = 60.7698 (1 samples)
	minimum = 4 (1 samples)
	maximum = 143 (1 samples)
Fragmentation average = 0 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0 (1 samples)
Injected packet rate average = 0.499856 (1 samples)
	minimum = 0.470333 (1 samples)
	maximum = 0.538 (1 samples)
Accepted packet rate average = 0.49989 (1 samples)
	minimum = 0.471 (1 samples)
	maximum = 0.534333 (1 samples)
Injected flit rate average = 0.499856 (1 samples)
	minimum = 0.470333 (1 samples)
	maximum = 0.538 (1 samples)
Accepted flit rate average = 0.49989 (1 samples)
	minimum = 0.471 (1 samples)
	maximum = 0.534333 (1 samples)
Injected packet size average = 1 (1 samples)
Accepted packet size average = 1 (1 samples)
Hops average = 5.97074 (1 samples)
Stats for UGAL routing: 
Total flits through min paths: 0
Total flits through non-min paths: 0
routing tier distribution when non-min paths are not chosen: |4-> 0 , |5-> 0 , |6-> 0 , 
routing tier distribution when non-min paths are chosen: |4-> 0 , |5-> 0 , |6-> 0 , 
non-min path length distribution when non-min paths are not chosen: |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 , 
non-min path length distribution when non-min paths are chosen: |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 , 
done with Run()
Total run time 1445.25
