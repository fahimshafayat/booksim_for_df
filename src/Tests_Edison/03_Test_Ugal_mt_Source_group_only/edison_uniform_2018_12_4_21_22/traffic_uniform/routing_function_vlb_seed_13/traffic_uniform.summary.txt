vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
topology = edison;
traffic = uniform;
routing_function = vlb;
seed = 13;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.46693,   0.49986,     531.3,    542.01,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.24983,   0.24982,    102.05,    102.15,         0,  0,  11683802,  ,  ,  ,  
simulation,         3,      0.37,    0.3699,   0.36983,    105.36,    105.44,         0,  0,  17323392,  ,  ,  ,  
simulation,         4,      0.43,   0.42997,   0.42987,    112.71,    112.79,         0,  0,  20256238,  ,  ,  ,  
simulation,         5,      0.46,   0.45936,   0.45989,    148.25,    148.65,         0,  0,  22429913,  ,  ,  ,  
simulation,         6,      0.48,   0.46647,   0.47997,    366.38,    456.06,         0,  0,  46359371,  ,  ,  ,  
simulation,         7,      0.49,   0.46664,   0.48997,    478.19,    477.27,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.45,   0.44999,   0.44986,    124.81,    124.92,         0,  0,  21427006,  ,  ,  ,  
simulation,         9,       0.4,   0.39991,   0.39988,    107.55,    107.62,         0,  0,  18755864,  ,  ,  ,  
simulation,        10,      0.35,   0.34989,   0.34985,    104.44,    104.53,         0,  0,  16381500,  ,  ,  ,  
simulation,        11,       0.3,   0.29981,   0.29978,    102.98,    103.07,         0,  0,  14027802,  ,  ,  ,  
