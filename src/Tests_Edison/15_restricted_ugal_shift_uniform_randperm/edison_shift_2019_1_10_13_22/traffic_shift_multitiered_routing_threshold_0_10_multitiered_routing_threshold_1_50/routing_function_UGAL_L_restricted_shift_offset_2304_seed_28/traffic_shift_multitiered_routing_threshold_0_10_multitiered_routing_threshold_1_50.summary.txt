vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = UGAL_L_restricted;
seed = 28;
shift_offset = 2304;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.4408,   0.50005,    393.24,    393.24,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25015,   0.25007,    340.32,    360.57,         0,  5349121,  14894050,  ,  ,  ,  
simulation,         3,      0.37,   0.36911,    0.3701,    385.53,    410.33,         0,  7504468,  33895051,  ,  ,  ,  
simulation,         4,      0.43,   0.42128,   0.43017,    368.82,    368.82,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.39753,   0.40005,    231.28,     365.2,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.38,   0.37861,   0.38009,     401.4,    434.34,         0,  8544967,  39973656,  ,  ,  ,  
simulation,         7,      0.39,    0.3881,   0.39003,     233.9,    358.91,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.35,    0.3495,    0.3501,    372.41,    392.02,         0,  7361721,  31395324,  ,  ,  ,  
simulation,         9,       0.3,   0.29999,   0.30011,    350.26,    373.35,         0,  5779044,  20244663,  ,  ,  ,  
simulation,        11,       0.2,    0.2002,   0.20004,    331.67,     349.2,         0,  4914848,  10102938,  ,  ,  ,  
