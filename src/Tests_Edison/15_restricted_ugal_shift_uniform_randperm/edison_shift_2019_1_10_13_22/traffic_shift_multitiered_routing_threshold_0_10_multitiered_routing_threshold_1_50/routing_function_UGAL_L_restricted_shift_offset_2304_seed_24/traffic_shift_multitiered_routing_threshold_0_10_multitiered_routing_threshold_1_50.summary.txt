vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
traffic = shift;
routing_function = UGAL_L_restricted;
seed = 24;
shift_offset = 2304;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,   0.44119,    0.4998,    393.42,    393.42,         1,  0,  0,  ,  ,  ,  
simulation,         2,      0.25,   0.25006,   0.25003,    339.16,    360.51,         0,  5282314,  14689611,  ,  ,  ,  
simulation,         3,      0.37,   0.36911,   0.37004,    384.89,    411.33,         0,  7967282,  36224485,  ,  ,  ,  
simulation,         4,      0.43,   0.42116,   0.43022,    362.38,    362.38,         1,  0,  0,  ,  ,  ,  
simulation,         5,       0.4,   0.39742,   0.40028,    343.62,    343.62,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.38,   0.37869,   0.38005,    399.42,    429.76,         0,  8254878,  38533488,  ,  ,  ,  
simulation,         7,      0.39,   0.38818,   0.39005,    234.54,    358.72,         1,  0,  0,  ,  ,  ,  
simulation,         8,      0.35,   0.34952,   0.35006,    370.31,    391.59,         0,  6959774,  29562406,  ,  ,  ,  
simulation,         9,       0.3,   0.29998,   0.30005,    350.62,    373.56,         0,  5812207,  20376830,  ,  ,  ,  
simulation,        11,       0.2,   0.20016,   0.20003,    331.18,    349.25,         0,  4874995,  10014082,  ,  ,  ,  
