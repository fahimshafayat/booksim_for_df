vc_buf_size = 64;
wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
sim_count = 1;
num_vcs = 13;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
internal_speedup = 4.0;
warmup_periods = 5;
sample_period = 1000;
multitiered_routing_threshold_0 = 10;
multitiered_routing_threshold_1 = 50;
topology = edison;
perm_seed = 51;
routing_function = UGAL_L_restricted;
seed = 29;
traffic = randperm;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?
simulation,         1,       0.5,    0.5001,   0.50009,    58.586,    58.653,         0,  20080224,  3244725,  ,  ,  ,  
simulation,         2,      0.74,   0.73132,   0.73995,    167.84,    427.67,         0,  105039496,  35553961,  ,  ,  ,  
simulation,         3,      0.86,   0.68761,     0.792,    597.59,    593.52,         1,  0,  0,  ,  ,  ,  
simulation,         4,       0.8,   0.68714,   0.77454,    355.67,    495.52,         1,  0,  0,  ,  ,  ,  
simulation,         5,      0.77,   0.69627,   0.75581,    374.82,    389.37,         1,  0,  0,  ,  ,  ,  
simulation,         6,      0.75,    0.7268,   0.74919,    196.35,     203.6,         1,  0,  0,  ,  ,  ,  
simulation,         8,       0.7,   0.69984,   0.70029,    84.284,    84.948,         0,  49661108,  7640266,  ,  ,  ,  
simulation,         9,      0.65,   0.65026,   0.65027,    64.831,    65.013,         0,  33114693,  4900713,  ,  ,  ,  
simulation,        10,       0.6,    0.6002,    0.6002,    60.216,    60.282,         0,  26928865,  4067273,  ,  ,  ,  
