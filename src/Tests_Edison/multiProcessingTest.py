#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 15:41:25 2018

@author: rahman
"""

import multiprocessing as mp
import time
import random

def test_func(param, lock):
    lock.acquire()
    try:
        sleep_time = random.randint(2,10)
        print("process started with patameter: ", param, " , will sleep for ", sleep_time, " secs")
        time.sleep(sleep_time)
        print("process continued with patameter: ", param)
        print("process still continued with patameter: ", param)
        print("process ended with patameter: ", param)
    finally:
        lock.release()
       

if __name__ == "__main__":
    print("Hello world!")
    
    total_cpu = mp.cpu_count()
    print("cpu count: ", total_cpu)
    
    params = [ii for ii in range(20)]
    
    processes = []
    
    lock = mp.Lock()
    
    for param in range(5):
        p = mp.Process( target = test_func, args = (param,lock))
        processes.append(p)
        p.start()
        
    for p in processes:
        p.join()

    print("------ All process ended.")
    
            