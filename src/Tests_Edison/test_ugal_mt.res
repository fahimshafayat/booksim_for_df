BEGIN Configuration File: examples/edison_ugal_mt
//Defaults
vc_buf_size = 64;
wait_for_tail_credit = 0;

//
// Router architecture
//
vc_allocator = separable_input_first; 
sw_allocator = separable_input_first;
alloc_iters  = 1;
credit_delay   = 2;
routing_delay  = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup     = 1;
output_speedup    = 1;
internal_speedup  = 4.0;
warmup_periods = 5;
sim_count          = 1;
sample_period  = 1000;  
num_vcs     = 11;
priority = none;
packet_size = 1;
injection_rate_uses_flits=1;


//customizables
//dragonfly specific parameters
topology = edison;
//end dragonfly specific paramters

routing_function = UGAL_L_mt;	//min/vlb/UGAL_L//UGAL_L_mt

seed = 13;

traffic       = randperm;
perm_seed = 42;

injection_rate = 0.50;

log_Qlen_data = 0;	//1 -> true. 0 -> false.

watch_file = watch;
watch_out = watch.out;


END Configuration File: examples/edison_ugal_mt
topology: edison
inside _RegisterRoutingFunctions() ...
done with _RegisterRoutingFunctions() ...
Routing function registered ...
inside Edison() constructor ...
inside _AllocateArrays() ... 
done with _AllocateArrays() ... 
inside _BuildGraphForLocal() ...
done with _BuildGraphForLocal() ...
edison_globallinks.txt file opened successfully.
_Computesize starts ...
_nodes: 5760
_size: 1440
_channels: 48240
_ComputeSize ends ...
inside _BuildNet() ...
Routers created ...
PEs connected ...
starting to connect the router channels ...
done with _BuildNet() ...
Done with Edison() constructor ...
topology object created ...
inside Run()
inside _SingleSim
_step_count: 200
_step_count: 400
_step_count: 600
_step_count: 800
_step_count: 1000
Class 0:
Packet latency average = 59.0804
	minimum = 4
	maximum = 137
Network latency average = 59.0804
	minimum = 4
	maximum = 137
Slowest packet = 1363676
Flit latency average = 59.0804
	minimum = 4
	maximum = 137
Slowest flit = 1363676
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.499977
	minimum = 0.446 (at node 2244)
	maximum = 0.553 (at node 5277)
Accepted packet rate average = 0.470219
	minimum = 0.415 (at node 1529)
	maximum = 0.53 (at node 415)
Injected flit rate average = 0.499977
	minimum = 0.446 (at node 2244)
	maximum = 0.553 (at node 5277)
Accepted flit rate average= 0.470219
	minimum = 0.415 (at node 1529)
	maximum = 0.53 (at node 415)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 171403 (0 measured)
latency change    = 1
throughput change = 1
_step_count: 1200
_step_count: 1400
_step_count: 1600
_step_count: 1800
_step_count: 2000
Class 0:
Packet latency average = 59.2872
	minimum = 4
	maximum = 137
Network latency average = 59.2872
	minimum = 4
	maximum = 137
Slowest packet = 1363676
Flit latency average = 59.2872
	minimum = 4
	maximum = 137
Slowest flit = 1363676
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500076
	minimum = 0.4595 (at node 4241)
	maximum = 0.5505 (at node 706)
Accepted packet rate average = 0.485198
	minimum = 0.4435 (at node 2803)
	maximum = 0.536 (at node 248)
Injected flit rate average = 0.500076
	minimum = 0.4595 (at node 4241)
	maximum = 0.5505 (at node 706)
Accepted flit rate average= 0.485198
	minimum = 0.4435 (at node 2803)
	maximum = 0.536 (at node 248)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 171393 (0 measured)
latency change    = 0.00348774
throughput change = 0.030871
_step_count: 2200
_step_count: 2400
_step_count: 2600
_step_count: 2800
_step_count: 3000
Class 0:
Packet latency average = 59.4702
	minimum = 4
	maximum = 126
Network latency average = 59.4702
	minimum = 4
	maximum = 126
Slowest packet = 6735874
Flit latency average = 59.4702
	minimum = 4
	maximum = 126
Slowest flit = 6735874
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500109
	minimum = 0.447 (at node 349)
	maximum = 0.562 (at node 5211)
Accepted packet rate average = 0.500103
	minimum = 0.445 (at node 2545)
	maximum = 0.559 (at node 916)
Injected flit rate average = 0.500109
	minimum = 0.447 (at node 349)
	maximum = 0.562 (at node 5211)
Accepted flit rate average= 0.500103
	minimum = 0.445 (at node 2545)
	maximum = 0.559 (at node 916)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 171428 (0 measured)
latency change    = 0.00307609
throughput change = 0.0298034
_step_count: 3200
_step_count: 3400
_step_count: 3600
_step_count: 3800
_step_count: 4000
Class 0:
Packet latency average = 59.4664
	minimum = 4
	maximum = 127
Network latency average = 59.4664
	minimum = 4
	maximum = 127
Slowest packet = 10125128
Flit latency average = 59.4664
	minimum = 4
	maximum = 127
Slowest flit = 10125128
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.500026
	minimum = 0.4565 (at node 1095)
	maximum = 0.539 (at node 3226)
Accepted packet rate average = 0.500004
	minimum = 0.4585 (at node 3962)
	maximum = 0.5395 (at node 1562)
Injected flit rate average = 0.500026
	minimum = 0.4565 (at node 1095)
	maximum = 0.539 (at node 3226)
Accepted flit rate average= 0.500004
	minimum = 0.4585 (at node 3962)
	maximum = 0.5395 (at node 1562)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 171644 (0 measured)
latency change    = 6.30075e-05
throughput change = 0.000197221
_step_count: 4200
_step_count: 4400
_step_count: 4600
_step_count: 4800
_step_count: 5000
Class 0:
Packet latency average = 59.4761
	minimum = 4
	maximum = 130
Network latency average = 59.4761
	minimum = 4
	maximum = 130
Slowest packet = 11257023
Flit latency average = 59.4761
	minimum = 4
	maximum = 130
Slowest flit = 11257023
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.499879
	minimum = 0.439 (at node 2065)
	maximum = 0.555 (at node 2885)
Accepted packet rate average = 0.499896
	minimum = 0.438 (at node 44)
	maximum = 0.562 (at node 2310)
Injected flit rate average = 0.499879
	minimum = 0.439 (at node 2065)
	maximum = 0.555 (at node 2885)
Accepted flit rate average= 0.499896
	minimum = 0.438 (at node 44)
	maximum = 0.562 (at node 2310)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 171546 (0 measured)
latency change    = 0.000162576
throughput change = 0.000216017
Warmed up ...Time used is 5000 cycles
_step_count: 5200
_step_count: 5400
_step_count: 5600
_step_count: 5800
_step_count: 6000
Class 0:
Packet latency average = 59.1431
	minimum = 4
	maximum = 128
Network latency average = 59.1431
	minimum = 4
	maximum = 128
Slowest packet = 15481217
Flit latency average = 59.4781
	minimum = 4
	maximum = 128
Slowest flit = 15481217
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.499599
	minimum = 0.442 (at node 2260)
	maximum = 0.551 (at node 4989)
Accepted packet rate average = 0.499614
	minimum = 0.443 (at node 1440)
	maximum = 0.554 (at node 131)
Injected flit rate average = 0.499599
	minimum = 0.442 (at node 2260)
	maximum = 0.551 (at node 4989)
Accepted flit rate average= 0.499614
	minimum = 0.443 (at node 1440)
	maximum = 0.554 (at node 131)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 171461 (171461 measured)
latency change    = 0.00563017
throughput change = 0.000564324
_step_count: 6200
_step_count: 6400
_step_count: 6600
_step_count: 6800
_step_count: 7000
Class 0:
Packet latency average = 59.3115
	minimum = 4
	maximum = 128
Network latency average = 59.3115
	minimum = 4
	maximum = 128
Slowest packet = 15481217
Flit latency average = 59.4739
	minimum = 4
	maximum = 128
Slowest flit = 15481217
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.499838
	minimum = 0.458 (at node 5181)
	maximum = 0.5435 (at node 3276)
Accepted packet rate average = 0.499893
	minimum = 0.46 (at node 5462)
	maximum = 0.543 (at node 2669)
Injected flit rate average = 0.499838
	minimum = 0.458 (at node 5181)
	maximum = 0.5435 (at node 3276)
Accepted flit rate average= 0.499893
	minimum = 0.46 (at node 5462)
	maximum = 0.543 (at node 2669)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 170915 (170915 measured)
latency change    = 0.00283865
throughput change = 0.000557585
_step_count: 7200
_step_count: 7400
_step_count: 7600
_step_count: 7800
_step_count: 8000
Class 0:
Packet latency average = 59.3696
	minimum = 4
	maximum = 129
Network latency average = 59.3696
	minimum = 4
	maximum = 129
Slowest packet = 21018682
Flit latency average = 59.4768
	minimum = 4
	maximum = 129
Slowest flit = 21018682
Fragmentation average = 0
	minimum = 0
	maximum = 0
Injected packet rate average = 0.499856
	minimum = 0.470333 (at node 970)
	maximum = 0.538 (at node 4615)
Accepted packet rate average = 0.499884
	minimum = 0.470667 (at node 3202)
	maximum = 0.534 (at node 3873)
Injected flit rate average = 0.499856
	minimum = 0.470333 (at node 970)
	maximum = 0.538 (at node 4615)
Accepted flit rate average= 0.499884
	minimum = 0.470667 (at node 3202)
	maximum = 0.534 (at node 3873)
Injected packet length average = 1
Accepted packet length average = 1
Total in-flight flits = 171076 (171076 measured)
latency change    = 0.000980169
throughput change = 1.83492e-05
Draining all recorded packets ...
done with _SingleSim
Draining remaining packets ...
_step_count: 8200
Time taken is 8245 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 59.4751 (1 samples)
	minimum = 4 (1 samples)
	maximum = 129 (1 samples)
Network latency average = 59.4751 (1 samples)
	minimum = 4 (1 samples)
	maximum = 129 (1 samples)
Flit latency average = 59.5701 (1 samples)
	minimum = 4 (1 samples)
	maximum = 129 (1 samples)
Fragmentation average = 0 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0 (1 samples)
Injected packet rate average = 0.499856 (1 samples)
	minimum = 0.470333 (1 samples)
	maximum = 0.538 (1 samples)
Accepted packet rate average = 0.499884 (1 samples)
	minimum = 0.470667 (1 samples)
	maximum = 0.534 (1 samples)
Injected flit rate average = 0.499856 (1 samples)
	minimum = 0.470333 (1 samples)
	maximum = 0.538 (1 samples)
Accepted flit rate average = 0.499884 (1 samples)
	minimum = 0.470667 (1 samples)
	maximum = 0.534 (1 samples)
Injected packet size average = 1 (1 samples)
Accepted packet size average = 1 (1 samples)
Hops average = 5.86715 (1 samples)
Stats for UGAL routing: 
Total flits through min paths: 0
Total flits through non-min paths: 0
routing tier distribution when non-min paths are not chosen: |4-> 0 , |5-> 0 , |6-> 0 , 
routing tier distribution when non-min paths are chosen: |4-> 0 , |5-> 0 , |6-> 0 , 
non-min path length distribution when non-min paths are not chosen: |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 , 
non-min path length distribution when non-min paths are chosen: |1-> 0 , |2-> 0 , |3-> 0 , |4-> 0 , |5-> 0 , |6-> 0 , 
done with Run()
Total run time 1275.94
