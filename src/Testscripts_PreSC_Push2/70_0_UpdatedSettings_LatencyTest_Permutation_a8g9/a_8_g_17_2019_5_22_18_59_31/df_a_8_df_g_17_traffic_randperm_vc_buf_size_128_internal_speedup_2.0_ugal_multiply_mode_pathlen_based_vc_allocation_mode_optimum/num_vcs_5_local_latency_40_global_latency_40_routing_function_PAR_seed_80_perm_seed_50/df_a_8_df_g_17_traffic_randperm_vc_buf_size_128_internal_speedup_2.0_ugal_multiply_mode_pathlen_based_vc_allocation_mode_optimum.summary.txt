wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
internal_speedup = 2.0;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 128;
global_latency = 40;
local_latency = 40;
num_vcs = 5;
perm_seed = 50;
routing_function = PAR;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50022,   0.50025,    145.48,    145.57,         0,      4.1601
simulation,         2,      0.74,    0.6045,   0.65315,    699.51,    584.06,         1,         0.0
simulation,         3,      0.62,   0.60376,   0.60435,    574.04,    326.03,         1,         0.0
simulation,         4,      0.56,   0.55986,   0.55988,    209.99,    166.22,         0,      4.2317
simulation,         5,      0.59,    0.5856,   0.58566,    455.43,    234.53,         1,         0.0
simulation,         6,      0.57,   0.56967,   0.56973,    236.97,    174.96,         0,      4.2531
simulation,         7,      0.58,   0.57938,   0.57942,    291.38,    187.36,         0,      4.2794
simulation,         8,       0.1,   0.10017,   0.10017,    141.52,    141.62,         0,      4.2453
simulation,         9,       0.2,   0.20019,    0.2002,    138.62,    138.71,         0,      4.1693
simulation,        10,       0.3,   0.30021,   0.30023,    137.96,    138.04,         0,      4.1421
simulation,        11,       0.4,   0.40023,   0.40026,    138.63,    138.72,         0,      4.1311
simulation,        12,       0.5,   0.50022,   0.50025,    145.48,    145.57,         0,      4.1601
