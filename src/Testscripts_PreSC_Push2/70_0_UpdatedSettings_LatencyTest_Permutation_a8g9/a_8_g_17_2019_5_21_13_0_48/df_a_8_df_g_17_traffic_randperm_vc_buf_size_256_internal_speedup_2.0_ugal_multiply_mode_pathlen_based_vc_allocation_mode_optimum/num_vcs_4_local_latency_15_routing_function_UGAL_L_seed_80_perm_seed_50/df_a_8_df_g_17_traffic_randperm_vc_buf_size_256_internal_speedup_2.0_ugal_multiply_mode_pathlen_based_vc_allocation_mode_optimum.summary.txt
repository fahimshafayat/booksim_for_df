wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
internal_speedup = 2.0;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 256;
local_latency = 15;
num_vcs = 4;
perm_seed = 50;
routing_function = UGAL_L;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50022,   0.50025,    70.483,     70.66,         0,      4.2319
simulation,         2,      0.74,   0.62554,   0.69952,    642.09,    635.53,         1,         0.0
simulation,         3,      0.62,   0.61187,   0.61261,    501.66,    372.69,         1,         0.0
simulation,         4,      0.56,   0.56003,   0.56006,    114.25,    109.86,         0,      4.3193
simulation,         5,      0.59,   0.58962,   0.58965,    190.67,    153.32,         0,      4.4096
simulation,         6,       0.6,   0.59912,   0.59919,    260.37,    187.02,         0,      4.4577
simulation,         7,      0.61,   0.60722,   0.60736,    416.52,    270.14,         1,         0.0
simulation,         8,       0.1,   0.10016,   0.10017,    43.963,    43.973,         0,      4.2734
simulation,         9,       0.2,   0.20019,    0.2002,    43.528,    43.535,         0,      4.2141
simulation,        10,       0.3,   0.30022,   0.30023,    43.765,    43.772,         0,      4.1872
simulation,        11,       0.4,   0.40024,   0.40026,    47.941,    47.971,         0,       4.183
simulation,        12,       0.5,   0.50022,   0.50025,    70.483,     70.66,         0,      4.2319
simulation,        13,       0.6,   0.59912,   0.59919,    260.37,    187.02,         0,      4.4577
