wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
internal_speedup = 2.0;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 256;
local_latency = 40;
num_vcs = 4;
perm_seed = 50;
routing_function = UGAL_L;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50023,   0.50025,    130.28,    130.57,         0,      4.2047
simulation,         2,      0.74,   0.61726,   0.69292,    702.48,     690.4,         1,         0.0
simulation,         3,      0.62,   0.60793,   0.60996,    575.11,    483.89,         1,         0.0
simulation,         4,      0.56,   0.55963,   0.55972,    231.71,    191.77,         0,      4.2885
simulation,         5,      0.59,     0.588,     0.588,    477.02,    262.89,         0,      4.3789
simulation,         6,       0.6,   0.59631,   0.59659,    491.25,    331.75,         1,         0.0
simulation,         7,       0.1,   0.10017,   0.10017,    93.466,    93.508,         0,       4.206
simulation,         8,       0.2,   0.20019,    0.2002,    92.644,    92.681,         0,      4.1665
simulation,         9,       0.3,   0.30022,   0.30023,    92.818,    92.857,         0,      4.1521
simulation,        10,       0.4,   0.40024,   0.40026,    98.967,    99.035,         0,      4.1527
simulation,        11,       0.5,   0.50023,   0.50025,    130.28,    130.57,         0,      4.2047
