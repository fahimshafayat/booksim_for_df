wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
internal_speedup = 2.0;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 256;
local_latency = 40;
num_vcs = 5;
perm_seed = 50;
routing_function = PAR_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50024,   0.50025,     102.3,    102.34,         0,      4.5657
simulation,         2,      0.74,    0.6803,   0.72023,    742.21,    720.94,         1,         0.0
simulation,         3,      0.62,   0.62009,    0.6201,    134.61,    134.66,         0,      4.5527
simulation,         4,      0.68,   0.67979,   0.67997,    253.94,    245.66,         0,      4.5054
simulation,         5,      0.71,     0.684,   0.69673,    752.38,    722.74,         1,         0.0
simulation,         6,      0.69,   0.68942,   0.68967,    305.15,    285.58,         0,      4.5074
simulation,         7,       0.7,   0.69047,    0.6966,    528.74,    500.67,         1,         0.0
simulation,         8,      0.15,    0.1502,   0.15021,    97.048,    97.087,         0,      4.6056
simulation,         9,       0.3,   0.30022,   0.30023,    96.758,    96.794,         0,      4.5597
simulation,        10,      0.45,   0.45021,   0.45022,    99.324,    99.357,         0,      4.5544
simulation,        11,       0.6,    0.6001,   0.60015,    123.83,    123.86,         0,      4.5683
