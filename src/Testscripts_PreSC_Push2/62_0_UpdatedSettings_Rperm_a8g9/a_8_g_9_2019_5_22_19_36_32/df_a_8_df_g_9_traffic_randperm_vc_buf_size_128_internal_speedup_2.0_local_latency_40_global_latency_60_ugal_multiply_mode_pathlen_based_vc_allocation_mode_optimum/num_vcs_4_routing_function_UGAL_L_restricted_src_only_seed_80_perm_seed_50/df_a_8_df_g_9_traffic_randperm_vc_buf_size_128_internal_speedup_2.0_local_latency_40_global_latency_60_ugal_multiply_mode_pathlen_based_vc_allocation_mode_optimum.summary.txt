wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 60;
internal_speedup = 2.0;
local_latency = 40;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 128;
num_vcs = 4;
perm_seed = 50;
routing_function = UGAL_L_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.5001,   0.50015,    138.98,    139.05,         0,      3.6728
simulation,         2,      0.74,   0.73253,   0.73269,    487.63,    227.83,         1,         0.0
simulation,         3,      0.62,   0.61987,    0.6199,     145.9,    145.97,         0,      3.6746
simulation,         4,      0.68,   0.67945,   0.67949,    199.97,    164.52,         0,      3.6856
simulation,         5,      0.71,   0.70736,   0.70737,     349.3,    187.85,         1,         0.0
simulation,         6,      0.69,   0.68894,   0.68903,    248.37,    174.43,         0,      3.6892
simulation,         7,       0.7,   0.69853,   0.69859,    309.82,    180.87,         0,      3.6918
simulation,         8,      0.15,   0.15005,   0.15006,    139.18,    139.24,         0,      3.7072
simulation,         9,       0.3,    0.3001,   0.30011,    138.29,    138.35,         0,      3.6836
simulation,        10,      0.45,   0.45006,   0.45009,    138.59,    138.66,         0,      3.6749
simulation,        11,       0.6,   0.59988,   0.59993,    143.24,    143.31,         0,       3.673
