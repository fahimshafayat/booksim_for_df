wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
num_vcs = 5;
perm_seed = 50;
routing_function = PAR;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50013,   0.50015,    46.709,    46.719,         0,      3.8742
simulation,         2,      0.74,   0.69813,   0.70276,    489.27,    126.75,         1,         0.0
simulation,         3,      0.62,   0.61986,    0.6199,    50.109,    50.123,         0,      3.8701
simulation,         4,      0.68,   0.67088,   0.67082,     548.8,    83.838,         1,         0.0
simulation,         5,      0.65,   0.64939,   0.64941,    120.36,     57.63,         0,      3.8875
simulation,         6,      0.66,   0.65864,   0.65865,    242.21,    65.561,         0,      3.8968
simulation,         7,      0.67,   0.66575,   0.66572,    386.79,    75.768,         1,         0.0
simulation,         8,      0.15,   0.15006,   0.15006,    48.594,     48.61,         0,      4.1497
simulation,         9,       0.3,   0.30012,   0.30011,    46.538,     46.55,         0,      3.9633
simulation,        10,      0.45,   0.45009,   0.45009,     46.38,    46.391,         0,      3.8876
simulation,        11,       0.6,   0.59991,   0.59993,    48.869,    48.879,         0,      3.8671
