sw_alloc_delay = 1;
alloc_iters = 1;
packet_size = 1;
wait_for_tail_credit = 0;
injection_rate_uses_flits = 1;
priority = none;
warmup_periods = 3;
sample_period = 10000;
input_speedup = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
routing_delay = 0;
st_final_delay = 1;
vc_alloc_delay = 1;
output_speedup = 1;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
five_hop_percentage = 50;
num_vcs = 4;
routing_function = UGAL_G_four_hop_some_five_hop_restricted;
seed = 82;
shift_by_group = 3;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.29289,   0.30814,    1837.4,    470.28,         1,         0.0
simulation,         2,      0.25,   0.25011,   0.25012,    58.428,     58.43,         0,      4.4217
simulation,         3,      0.37,   0.29292,   0.30484,    865.54,    353.65,         1,         0.0
simulation,         4,      0.31,   0.28988,   0.29299,    516.49,    197.09,         1,         0.0
simulation,         5,      0.28,   0.28012,   0.28012,    62.575,    62.574,         0,      4.3975
simulation,         6,      0.29,   0.28727,   0.28727,    503.39,    123.06,         1,         0.0
simulation,         7,      0.05,  0.049977,  0.049975,    54.458,     54.46,         0,      4.4555
simulation,         8,       0.1,  0.099978,  0.099984,    54.839,    54.844,         0,      4.4567
simulation,         9,      0.15,   0.15006,   0.15005,    55.629,    55.633,         0,       4.455
simulation,        10,       0.2,    0.2001,   0.20009,    57.188,    57.191,         0,      4.4486
simulation,        11,      0.25,   0.25011,   0.25012,    58.428,     58.43,         0,      4.4217
