sw_alloc_delay = 1;
alloc_iters = 1;
packet_size = 1;
wait_for_tail_credit = 0;
injection_rate_uses_flits = 1;
priority = none;
warmup_periods = 3;
sample_period = 10000;
input_speedup = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
routing_delay = 0;
st_final_delay = 1;
vc_alloc_delay = 1;
output_speedup = 1;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
five_hop_percentage = 70;
num_vcs = 4;
routing_function = UGAL_G_four_hop_some_five_hop_restricted;
seed = 82;
shift_by_group = 3;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.28835,    0.3033,    1881.1,    473.44,         1,         0.0
simulation,         2,      0.25,   0.25011,   0.25012,    58.725,    58.732,         0,      4.4513
simulation,         3,      0.37,   0.28942,   0.30151,     899.4,     367.8,         1,         0.0
simulation,         4,      0.31,   0.28613,   0.28951,     608.1,    222.19,         1,         0.0
simulation,         5,      0.28,   0.28014,   0.28012,    68.513,    68.505,         0,       4.429
simulation,         6,      0.29,   0.28438,    0.2844,    613.93,    140.14,         1,         0.0
simulation,         7,      0.05,  0.049976,  0.049975,    54.818,    54.824,         0,      4.4868
simulation,         8,       0.1,  0.099976,  0.099984,    55.169,    55.177,         0,      4.4854
simulation,         9,      0.15,   0.15005,   0.15005,      56.0,    56.005,         0,       4.486
simulation,        10,       0.2,   0.20009,   0.20009,    57.521,    57.526,         0,      4.4783
simulation,        11,      0.25,   0.25011,   0.25012,    58.725,    58.732,         0,      4.4513
