sw_alloc_delay = 1;
alloc_iters = 1;
packet_size = 1;
wait_for_tail_credit = 0;
injection_rate_uses_flits = 1;
priority = none;
warmup_periods = 3;
sample_period = 10000;
input_speedup = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
sim_count = 1;
routing_delay = 0;
st_final_delay = 1;
vc_alloc_delay = 1;
output_speedup = 1;
credit_delay = 2;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
five_hop_percentage = 0;
num_vcs = 4;
routing_function = UGAL_G;
seed = 82;
shift_by_group = 3;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.21617,   0.23154,    2808.6,     663.6,         1,         0.0
simulation,         2,      0.25,   0.21592,   0.22768,    609.35,    404.39,         1,         0.0
simulation,         3,      0.13,   0.13003,   0.13004,    61.781,    61.791,         0,      5.0103
simulation,         4,      0.19,   0.19009,    0.1901,    63.988,      64.0,         0,      4.9903
simulation,         5,      0.22,   0.22005,   0.22005,    65.936,     65.94,         0,      4.9618
simulation,         6,      0.23,   0.21886,   0.21906,    953.87,    304.51,         1,         0.0
simulation,         7,      0.05,  0.049974,  0.049975,    60.908,    60.922,         0,       5.017
simulation,         8,       0.1,  0.099985,  0.099984,    61.288,    61.299,         0,      5.0123
simulation,         9,      0.15,   0.15005,   0.15005,     62.28,    62.289,         0,      5.0058
simulation,        10,       0.2,    0.2001,   0.20009,    64.735,    64.745,         0,      4.9877
