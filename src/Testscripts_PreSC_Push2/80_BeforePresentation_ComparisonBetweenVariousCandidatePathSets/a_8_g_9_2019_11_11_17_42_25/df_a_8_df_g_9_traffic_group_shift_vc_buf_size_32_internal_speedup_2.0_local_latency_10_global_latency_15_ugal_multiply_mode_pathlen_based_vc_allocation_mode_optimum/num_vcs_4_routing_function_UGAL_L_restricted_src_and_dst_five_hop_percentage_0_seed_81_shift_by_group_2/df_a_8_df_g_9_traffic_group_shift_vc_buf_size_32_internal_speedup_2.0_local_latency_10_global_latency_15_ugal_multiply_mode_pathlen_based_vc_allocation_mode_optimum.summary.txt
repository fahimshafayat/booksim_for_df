wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
five_hop_percentage = 0;
num_vcs = 4;
routing_function = UGAL_L_restricted_src_and_dst;
seed = 81;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.32106,   0.33444,    1410.4,    385.24,         1,         0.0
simulation,         2,      0.25,   0.24984,   0.24984,     85.76,    85.781,         0,      4.4516
simulation,         3,      0.37,   0.31332,   0.32383,    651.92,    292.09,         1,         0.0
simulation,         4,      0.31,   0.29906,    0.2992,    843.27,    235.73,         1,         0.0
simulation,         5,      0.28,   0.27885,   0.27886,    392.19,    125.11,         0,       4.494
simulation,         6,      0.29,   0.28703,   0.28709,    528.97,    164.19,         1,         0.0
simulation,         7,      0.05,  0.049854,  0.049851,    52.525,    52.531,         0,      4.3543
simulation,         8,       0.1,  0.099921,  0.099918,    52.555,    52.559,         0,      4.3371
simulation,         9,      0.15,   0.14984,   0.14984,     52.98,    52.985,         0,      4.3224
simulation,        10,       0.2,   0.19981,    0.1998,    58.948,    58.952,         0,      4.3412
simulation,        11,      0.25,   0.24984,   0.24984,     85.76,    85.781,         0,      4.4516
