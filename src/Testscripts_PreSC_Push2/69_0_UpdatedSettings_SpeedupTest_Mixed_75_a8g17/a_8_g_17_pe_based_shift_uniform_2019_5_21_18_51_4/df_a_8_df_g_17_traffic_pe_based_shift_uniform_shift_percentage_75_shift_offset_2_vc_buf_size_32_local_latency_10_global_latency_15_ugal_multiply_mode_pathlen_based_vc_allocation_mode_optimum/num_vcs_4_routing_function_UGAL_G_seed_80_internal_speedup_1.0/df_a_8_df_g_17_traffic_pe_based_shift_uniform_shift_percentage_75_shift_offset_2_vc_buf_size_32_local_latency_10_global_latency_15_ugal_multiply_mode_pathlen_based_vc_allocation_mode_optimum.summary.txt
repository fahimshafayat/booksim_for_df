wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 15;
local_latency = 10;
shift_offset = 2;
shift_percentage = 75;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
internal_speedup = 1.0;
num_vcs = 4;
routing_function = UGAL_G;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.14689,    0.1611,    2747.1,     847.7,         1,         0.0
simulation,         2,      0.25,   0.13658,   0.14911,    1549.7,    760.67,         1,         0.0
simulation,         3,      0.13,   0.13021,    0.1302,    81.015,    81.043,         0,      5.1171
simulation,         4,      0.19,    0.1426,   0.15148,    825.24,    521.67,         1,         0.0
simulation,         5,      0.16,   0.14268,   0.14543,    723.72,    344.94,         1,         0.0
simulation,         6,      0.14,   0.14024,   0.14025,    83.329,    83.323,         0,      5.1661
simulation,         7,      0.15,   0.14579,   0.14583,    671.73,    227.54,         1,         0.0
simulation,         8,      0.05,  0.050084,  0.050086,    65.946,    65.961,         0,      4.9571
simulation,         9,       0.1,   0.10017,   0.10017,    70.491,    70.502,         0,      4.9705
