wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 15;
local_latency = 10;
shift_offset = 2;
shift_percentage = 75;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
internal_speedup = 2.0;
num_vcs = 4;
routing_function = UGAL_G;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.23079,   0.24421,    1877.9,     534.1,         1,         0.0
simulation,         2,      0.25,   0.21654,   0.22383,    484.12,    284.27,         1,         0.0
simulation,         3,      0.13,    0.1302,    0.1302,    61.479,    61.491,         0,      4.9477
simulation,         4,      0.19,   0.19022,   0.19022,    63.244,    63.257,         0,      4.9342
simulation,         5,      0.22,    0.2127,   0.21277,    583.28,    162.47,         1,         0.0
simulation,         6,       0.2,   0.20019,    0.2002,    65.051,    65.056,         0,      4.9323
simulation,         7,      0.21,   0.20885,   0.20886,    360.36,    100.75,         1,         0.0
simulation,         8,      0.05,  0.050085,  0.050086,    60.033,    60.047,         0,      4.9569
simulation,         9,       0.1,   0.10017,   0.10017,    60.635,    60.646,         0,      4.9472
simulation,        10,      0.15,   0.15021,   0.15021,    62.133,    62.145,         0,      4.9466
simulation,        11,       0.2,   0.20019,    0.2002,    65.051,    65.056,         0,      4.9323
