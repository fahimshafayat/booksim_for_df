wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 15;
local_latency = 10;
shift_offset = 2;
shift_percentage = 75;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
internal_speedup = 3.0;
num_vcs = 4;
routing_function = UGAL_G;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.24207,   0.25525,    1766.9,    500.95,         1,         0.0
simulation,         2,      0.25,   0.22291,   0.22617,    755.71,     263.8,         1,         0.0
simulation,         3,      0.13,   0.13021,    0.1302,    56.395,    56.406,         0,      4.9681
simulation,         4,      0.19,   0.19021,   0.19022,     57.41,     57.42,         0,      4.9487
simulation,         5,      0.22,   0.21714,   0.21716,    518.79,    110.05,         1,         0.0
simulation,         6,       0.2,   0.20019,    0.2002,    57.998,    58.007,         0,      4.9408
simulation,         7,      0.21,   0.21019,    0.2102,    62.715,    62.721,         0,      4.9402
simulation,         8,      0.05,  0.050086,  0.050086,    55.111,    55.121,         0,      4.9599
simulation,         9,       0.1,   0.10017,   0.10017,    55.718,     55.73,         0,      4.9609
simulation,        10,      0.15,   0.15021,   0.15021,    56.781,    56.791,         0,       4.967
simulation,        11,       0.2,   0.20019,    0.2002,    57.998,    58.007,         0,      4.9408
