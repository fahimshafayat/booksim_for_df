wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 15;
local_latency = 10;
shift_offset = 2;
shift_percentage = 75;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
internal_speedup = 4.0;
num_vcs = 4;
routing_function = UGAL_L_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.36046,   0.37108,    953.92,    270.82,         1,         0.0
simulation,         2,      0.25,   0.24979,   0.24979,    178.96,    75.016,         0,      4.5919
simulation,         3,      0.37,   0.33419,    0.3376,    692.63,    190.45,         1,         0.0
simulation,         4,      0.31,   0.30196,   0.30201,     502.7,    119.17,         1,         0.0
simulation,         5,      0.28,   0.27756,   0.27757,    502.71,    94.869,         1,         0.0
simulation,         6,      0.26,   0.25946,   0.25947,    267.49,    81.324,         0,      4.6022
simulation,         7,      0.27,   0.26852,   0.26853,    352.79,    90.848,         1,         0.0
simulation,         8,      0.05,  0.050088,  0.050086,    49.057,    49.062,         0,      4.4367
simulation,         9,       0.1,   0.10017,   0.10017,    48.988,    48.993,         0,       4.402
simulation,        10,      0.15,   0.15021,   0.15021,    51.826,    51.829,         0,       4.419
simulation,        11,       0.2,   0.20019,    0.2002,    62.659,    62.667,         0,      4.5229
simulation,        12,      0.25,   0.24979,   0.24979,    178.96,    75.016,         0,      4.5919
