wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 15;
local_latency = 10;
shift_offset = 2;
shift_percentage = 75;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
internal_speedup = 2.0;
num_vcs = 4;
routing_function = UGAL_L_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.34575,   0.35666,    1037.6,    292.48,         1,         0.0
simulation,         2,      0.25,    0.2497,    0.2497,    222.76,    83.349,         0,      4.6016
simulation,         3,      0.37,   0.32462,   0.32844,     849.7,    219.13,         1,         0.0
simulation,         4,      0.31,   0.29954,   0.29962,     630.2,    134.85,         1,         0.0
simulation,         5,      0.28,   0.27605,   0.27607,     509.0,    110.69,         1,         0.0
simulation,         6,      0.26,   0.25859,    0.2586,    344.26,    95.651,         1,         0.0
simulation,         7,      0.05,  0.050085,  0.050086,    53.507,    53.513,         0,      4.4371
simulation,         8,       0.1,   0.10017,   0.10017,    53.456,    53.463,         0,      4.4036
simulation,         9,      0.15,    0.1502,   0.15021,     56.79,    56.794,         0,      4.4254
simulation,        10,       0.2,   0.20019,    0.2002,    68.652,    68.661,         0,      4.5329
simulation,        11,      0.25,    0.2497,    0.2497,    222.76,    83.349,         0,      4.6016
