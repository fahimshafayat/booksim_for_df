wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 60;
local_latency = 40;
shift_offset = 2;
shift_percentage = 75;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 128;
internal_speedup = 3.0;
num_vcs = 5;
routing_function = PAR_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.41843,    0.4513,    581.17,    547.19,         1,         0.0
simulation,         2,      0.25,   0.25024,   0.25024,     201.4,    201.53,         0,      4.9515
simulation,         3,      0.37,   0.35758,   0.35773,    691.57,    403.71,         1,         0.0
simulation,         4,      0.31,   0.31023,   0.31024,    213.91,    214.06,         0,       4.973
simulation,         5,      0.34,   0.33757,   0.33756,    492.18,    287.16,         1,         0.0
simulation,         6,      0.32,   0.32018,   0.32022,    219.67,    219.82,         0,      4.9727
simulation,         7,      0.33,   0.32984,   0.32991,    274.94,    254.44,         0,      4.9668
simulation,         8,       0.1,   0.10015,   0.10017,    196.49,    196.64,         0,       4.921
simulation,         9,       0.2,   0.20018,    0.2002,    199.34,    199.48,         0,      4.9386
simulation,        10,       0.3,   0.30022,   0.30023,    208.97,     209.1,         0,       4.969
