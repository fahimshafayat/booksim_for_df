wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 60;
local_latency = 40;
shift_offset = 2;
shift_percentage = 75;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 128;
internal_speedup = 4.0;
num_vcs = 5;
routing_function = PAR_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.41955,   0.45216,    567.14,    536.15,         1,         0.0
simulation,         2,      0.25,   0.25024,   0.25024,    201.18,    201.32,         0,       4.952
simulation,         3,      0.37,   0.35771,   0.35795,    702.89,    403.46,         1,         0.0
simulation,         4,      0.31,   0.31024,   0.31024,    213.52,    213.66,         0,      4.9741
simulation,         5,      0.34,   0.33751,   0.33752,    501.69,    285.91,         1,         0.0
simulation,         6,      0.32,   0.32019,   0.32022,    219.36,    219.53,         0,      4.9743
simulation,         7,      0.33,   0.32974,   0.32987,    278.22,    257.62,         0,       4.968
simulation,         8,       0.1,   0.10016,   0.10017,     196.5,    196.64,         0,      4.9215
simulation,         9,       0.2,    0.2002,    0.2002,    199.28,    199.43,         0,      4.9396
simulation,        10,       0.3,    0.3002,   0.30023,    208.56,    208.69,         0,      4.9709
