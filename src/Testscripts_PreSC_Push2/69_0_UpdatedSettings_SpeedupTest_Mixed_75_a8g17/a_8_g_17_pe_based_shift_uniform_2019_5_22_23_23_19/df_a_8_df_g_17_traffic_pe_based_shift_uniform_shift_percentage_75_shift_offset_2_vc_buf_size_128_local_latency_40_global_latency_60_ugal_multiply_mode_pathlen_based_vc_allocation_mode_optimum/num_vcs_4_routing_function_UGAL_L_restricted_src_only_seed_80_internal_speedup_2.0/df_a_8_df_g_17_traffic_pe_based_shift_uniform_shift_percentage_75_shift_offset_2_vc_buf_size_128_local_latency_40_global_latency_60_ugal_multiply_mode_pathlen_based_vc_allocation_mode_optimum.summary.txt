wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 60;
local_latency = 40;
shift_offset = 2;
shift_percentage = 75;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 128;
internal_speedup = 2.0;
num_vcs = 4;
routing_function = UGAL_L_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.37606,   0.41307,    854.12,    720.08,         1,         0.0
simulation,         2,      0.25,   0.25022,   0.25024,    254.25,    254.63,         0,      4.5828
simulation,         3,      0.37,   0.34164,   0.35109,    559.62,    463.15,         1,         0.0
simulation,         4,      0.31,   0.30468,    0.3048,     452.0,    347.31,         1,         0.0
simulation,         5,      0.28,   0.27963,    0.2797,    408.13,     286.8,         0,      4.6168
simulation,         6,      0.29,   0.28796,   0.28798,    495.61,    313.17,         1,         0.0
simulation,         7,      0.05,  0.050083,  0.050086,    174.17,    174.26,         0,      4.3683
simulation,         8,       0.1,   0.10017,   0.10017,    173.32,     173.4,         0,      4.3426
simulation,         9,      0.15,    0.1502,   0.15021,    183.74,    183.79,         0,      4.3662
simulation,        10,       0.2,   0.20018,    0.2002,    224.81,    224.93,         0,      4.5005
simulation,        11,      0.25,   0.25022,   0.25024,    254.25,    254.63,         0,      4.5828
