wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
shift_offset = 1;
topology = dragonflyfull;
traffic = time_pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
df_g = 17;
num_vcs = 4;
routing_function = UGAL_L_restricted_src_only;
seed = 80;
shift_percentage = 50;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.39936,   0.41054,    796.61,    250.51,         1,         0.0
simulation,         2,      0.25,   0.25024,   0.25024,    62.517,    62.531,         0,       4.237
simulation,         3,      0.37,   0.36143,   0.36146,    562.66,    127.36,         1,         0.0
simulation,         4,      0.31,   0.31023,   0.31024,    70.588,    70.627,         0,      4.2903
simulation,         5,      0.34,    0.3393,   0.33932,     256.9,      90.4,         0,      4.3093
simulation,         6,      0.35,   0.34738,   0.34742,    490.83,    104.68,         1,         0.0
simulation,         7,       0.1,   0.10017,   0.10017,    50.008,    50.016,         0,      4.1856
simulation,         8,       0.2,    0.2002,    0.2002,    53.597,    53.603,         0,      4.1689
simulation,         9,       0.3,   0.30021,   0.30023,    69.358,    69.389,         0,      4.2827
