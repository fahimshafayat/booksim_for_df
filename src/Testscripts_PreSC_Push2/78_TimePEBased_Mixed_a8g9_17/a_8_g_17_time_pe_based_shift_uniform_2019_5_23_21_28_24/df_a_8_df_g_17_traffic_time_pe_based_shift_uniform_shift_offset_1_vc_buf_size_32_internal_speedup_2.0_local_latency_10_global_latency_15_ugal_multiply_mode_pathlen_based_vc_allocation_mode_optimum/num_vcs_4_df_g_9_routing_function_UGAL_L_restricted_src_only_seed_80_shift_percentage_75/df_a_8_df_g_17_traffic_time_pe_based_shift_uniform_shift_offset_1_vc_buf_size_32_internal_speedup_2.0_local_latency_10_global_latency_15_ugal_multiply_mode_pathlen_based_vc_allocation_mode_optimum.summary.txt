wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
shift_offset = 1;
topology = dragonflyfull;
traffic = time_pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
df_g = 9;
num_vcs = 4;
routing_function = UGAL_L_restricted_src_only;
seed = 80;
shift_percentage = 75;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.38365,   0.39575,    952.01,    287.81,         1,         0.0
simulation,         2,      0.25,   0.24997,   0.24998,    58.461,    58.464,         0,      4.1659
simulation,         3,      0.37,   0.35791,   0.35789,     779.7,    180.24,         1,         0.0
simulation,         4,      0.31,   0.31017,   0.31018,    81.476,    81.504,         0,      4.2488
simulation,         5,      0.34,    0.3393,   0.33933,    272.47,    117.21,         0,      4.2797
simulation,         6,      0.35,   0.34665,   0.34669,     535.7,     153.8,         1,         0.0
simulation,         7,       0.1,   0.10003,   0.10003,    49.984,    49.988,         0,      4.1724
simulation,         8,       0.2,   0.19998,   0.19998,    50.726,    50.731,         0,      4.1421
simulation,         9,       0.3,   0.30011,   0.30011,     77.91,    77.934,         0,      4.2385
