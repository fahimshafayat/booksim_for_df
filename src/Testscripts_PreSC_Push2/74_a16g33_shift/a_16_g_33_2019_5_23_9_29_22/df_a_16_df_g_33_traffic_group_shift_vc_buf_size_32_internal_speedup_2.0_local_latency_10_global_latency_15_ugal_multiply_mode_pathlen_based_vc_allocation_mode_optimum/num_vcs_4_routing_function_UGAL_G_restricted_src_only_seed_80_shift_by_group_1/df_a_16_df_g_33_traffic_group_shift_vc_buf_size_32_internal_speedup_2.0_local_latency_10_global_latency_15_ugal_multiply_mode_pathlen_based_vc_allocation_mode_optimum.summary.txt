wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 33;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
num_vcs = 4;
routing_function = UGAL_G_restricted_src_only;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.19262,   0.20694,    2868.6,    677.61,         1,         0.0
simulation,         2,      0.25,   0.20192,   0.21234,    733.15,    407.62,         1,         0.0
simulation,         3,      0.13,   0.13002,   0.13002,    66.133,    66.138,         0,      4.9513
simulation,         4,      0.19,   0.19005,   0.19005,    74.144,    74.165,         0,      4.9618
simulation,         5,      0.22,   0.20364,   0.20684,    527.74,    262.42,         1,         0.0
simulation,         6,       0.2,   0.19894,   0.19894,    330.96,    105.88,         1,         0.0
simulation,         7,      0.05,  0.050007,  0.050005,     61.39,    61.393,         0,      4.8755
simulation,         8,       0.1,       0.1,       0.1,    64.733,    64.735,         0,      4.9288
simulation,         9,      0.15,   0.15002,   0.15003,    66.271,    66.277,         0,      4.9576
