wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 16;
df_arrangement = absolute_improved;
df_g = 33;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
num_vcs = 4;
routing_function = UGAL_L;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.12954,   0.14417,    3637.2,    1020.9,         1,         0.0
simulation,         2,      0.25,   0.13127,   0.14593,    2215.3,    944.82,         1,         0.0
simulation,         3,      0.13,   0.12587,   0.12717,     800.3,    621.99,         1,         0.0
simulation,         4,      0.07,  0.070004,  0.070004,    93.337,    93.351,         0,      5.5559
simulation,         5,       0.1,       0.1,       0.1,    122.64,    122.77,         0,      5.9065
simulation,         6,      0.11,   0.11001,   0.11001,    132.56,    132.79,         0,      5.9814
simulation,         7,      0.12,   0.12001,   0.12003,    160.12,    160.87,         0,      6.0439
simulation,         8,      0.05,  0.050006,  0.050005,     65.19,      65.2,         0,      5.1543
simulation,         9,       0.1,       0.1,       0.1,    122.64,    122.77,         0,      5.9065
