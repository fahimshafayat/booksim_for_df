output_speedup = 1;
vc_allocator = separable_input_first;
packet_size = 1;
sw_allocator = separable_input_first;
injection_rate_uses_flits = 1;
warmup_periods = 3;
sample_period = 10000;
routing_delay = 0;
st_final_delay = 1;
priority = none;
vc_alloc_delay = 1;
credit_delay = 2;
input_speedup = 1;
alloc_iters = 1;
sim_count = 1;
wait_for_tail_credit = 0;
sw_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 60;
internal_speedup = 2.0;
local_latency = 40;
shift_offset = 2;
shift_percentage = 75;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 128;
num_vcs = 4;
routing_function = UGAL_G;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.24003,   0.29064,    1774.2,    1419.3,         1,         0.0
simulation,         2,      0.25,   0.23496,    0.2428,    537.91,    507.59,         1,         0.0
simulation,         3,      0.13,   0.13021,    0.1302,    204.42,    204.57,         0,      4.9483
simulation,         4,      0.19,    0.1902,   0.19022,    211.54,    211.69,         0,      4.9531
simulation,         5,      0.22,   0.22018,   0.22021,    214.79,     214.9,         0,      4.9581
simulation,         6,      0.23,   0.23023,   0.23022,     236.2,    236.36,         0,      4.9792
simulation,         7,      0.24,   0.23511,   0.23516,    504.07,    404.33,         1,         0.0
simulation,         8,      0.05,  0.050099,  0.050086,    200.86,    201.04,         0,      4.9601
simulation,         9,       0.1,   0.10016,   0.10017,    202.24,    202.47,         0,      4.9538
simulation,        10,      0.15,   0.15018,   0.15021,    206.34,    206.53,         0,      4.9481
simulation,        11,       0.2,   0.20018,    0.2002,    212.84,    212.99,         0,      4.9539
