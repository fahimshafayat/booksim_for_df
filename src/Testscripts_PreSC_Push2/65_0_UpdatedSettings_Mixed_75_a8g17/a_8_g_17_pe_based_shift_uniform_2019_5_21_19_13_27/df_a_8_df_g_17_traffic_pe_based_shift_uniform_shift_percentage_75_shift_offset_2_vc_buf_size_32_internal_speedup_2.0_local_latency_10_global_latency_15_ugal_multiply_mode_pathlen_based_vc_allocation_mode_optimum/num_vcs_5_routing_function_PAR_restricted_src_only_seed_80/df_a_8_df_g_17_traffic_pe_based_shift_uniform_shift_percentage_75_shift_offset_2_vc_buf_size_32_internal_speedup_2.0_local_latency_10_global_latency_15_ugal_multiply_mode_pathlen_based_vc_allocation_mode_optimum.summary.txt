wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
shift_offset = 2;
shift_percentage = 75;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
num_vcs = 5;
routing_function = PAR_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.36964,    0.3816,     841.5,    294.59,         1,         0.0
simulation,         2,      0.25,   0.25025,   0.25024,    63.226,    63.236,         0,      4.9173
simulation,         3,      0.37,   0.34416,   0.34401,    1010.4,    159.54,         1,         0.0
simulation,         4,      0.31,   0.30675,   0.30676,     491.4,    99.416,         1,         0.0
simulation,         5,      0.28,   0.28021,   0.28022,    65.871,    65.881,         0,      4.9158
simulation,         6,      0.29,   0.29016,   0.29016,    87.081,    71.531,         0,      4.9123
simulation,         7,       0.3,    0.2989,   0.29891,    360.05,    84.569,         0,      4.9031
simulation,         8,      0.05,  0.050082,  0.050086,     60.94,    60.948,         0,       4.976
simulation,         9,       0.1,   0.10017,   0.10017,    60.671,    60.681,         0,      4.9368
simulation,        10,      0.15,   0.15021,   0.15021,    61.003,    61.014,         0,      4.9218
simulation,        11,       0.2,   0.20019,    0.2002,    61.863,    61.873,         0,      4.9174
simulation,        12,      0.25,   0.25025,   0.25024,    63.226,    63.236,         0,      4.9173
simulation,        13,       0.3,    0.2989,   0.29891,    360.05,    84.569,         0,      4.9031
