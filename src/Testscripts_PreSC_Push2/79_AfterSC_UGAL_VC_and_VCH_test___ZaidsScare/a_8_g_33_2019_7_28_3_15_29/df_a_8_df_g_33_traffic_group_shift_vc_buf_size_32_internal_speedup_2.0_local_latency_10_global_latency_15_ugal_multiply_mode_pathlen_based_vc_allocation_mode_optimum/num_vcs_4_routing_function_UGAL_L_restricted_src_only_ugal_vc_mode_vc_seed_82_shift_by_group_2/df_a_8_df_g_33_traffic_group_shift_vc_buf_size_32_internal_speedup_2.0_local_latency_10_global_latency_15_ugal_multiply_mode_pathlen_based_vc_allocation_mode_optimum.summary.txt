wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 33;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
num_vcs = 4;
routing_function = UGAL_L_restricted_src_only;
seed = 82;
shift_by_group = 2;
ugal_vc_mode = vc;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,  0.089952,   0.10644,    3630.1,    1413.6,         1,         0.0
simulation,         2,      0.25,  0.085655,  0.085747,    4522.4,    1670.2,         1,         0.0
simulation,         3,      0.13,   0.12995,   0.12994,    65.523,    65.527,         0,      5.0825
simulation,         4,      0.19,      0.19,      0.19,    71.134,    71.136,         0,      5.1714
simulation,         5,      0.22,   0.22004,   0.22004,    73.094,    73.097,         0,      5.1957
simulation,         6,      0.23,   0.23005,   0.23005,    73.733,    73.737,         0,      5.2022
simulation,         7,      0.24,  0.087362,  0.087524,    3752.6,    1578.0,         1,         0.0
simulation,         8,      0.05,  0.049984,  0.049981,    61.764,     61.77,         0,      4.9908
simulation,         9,       0.1,  0.099916,  0.099915,    63.139,    63.144,         0,      5.0369
simulation,        10,      0.15,   0.14997,   0.14997,    67.701,    67.705,         0,      5.1214
simulation,        11,       0.2,   0.20003,   0.20002,    71.823,    71.826,         0,      5.1808
