wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 33;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
num_vcs = 4;
routing_function = UGAL_L_restricted_src_only;
seed = 82;
ugal_vc_mode = vc_h;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.50013,   0.50012,     46.73,    46.734,         0,       3.841
simulation,         2,      0.74,   0.71617,    0.7165,    802.89,     203.2,         1,         0.0
simulation,         3,      0.62,   0.62012,   0.62012,    48.926,     48.93,         0,      3.8247
simulation,         4,      0.68,    0.6801,   0.68008,     53.13,    53.135,         0,      3.8249
simulation,         5,      0.71,   0.71011,   0.71008,    61.409,    61.418,         0,      3.8389
simulation,         6,      0.72,   0.72006,   0.72002,    74.667,    74.642,         0,      3.8567
simulation,         7,      0.73,    0.7168,   0.71709,    620.38,    182.61,         1,         0.0
simulation,         8,      0.15,   0.14996,   0.14997,    46.808,    46.813,         0,      3.9834
simulation,         9,       0.3,   0.30005,   0.30006,    46.029,    46.033,         0,      3.8932
simulation,        10,      0.45,   0.45018,   0.45018,    46.356,     46.36,         0,      3.8504
simulation,        11,       0.6,   0.60007,   0.60007,    48.316,     48.32,         0,      3.8269
