packet_size = 1;
warmup_periods = 3;
priority = none;
vc_allocator = separable_input_first;
routing_delay = 0;
wait_for_tail_credit = 0;
injection_rate_uses_flits = 1;
alloc_iters = 1;
sw_allocator = separable_input_first;
output_speedup = 1;
sim_count = 1;
vc_alloc_delay = 1;
input_speedup = 1;
sample_period = 10000;
st_final_delay = 1;
credit_delay = 2;
sw_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = randperm;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
num_vcs = 4;
perm_seed = 52;
routing_function = UGAL_L_restricted_src_only;
seed = 82;
ugal_vc_mode = vc_h;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.49989,   0.49989,    45.829,    45.835,         0,      3.7554
simulation,         2,      0.74,   0.68778,   0.69244,    569.76,    126.51,         1,         0.0
simulation,         3,      0.62,   0.61726,   0.61727,    413.38,    56.227,         0,      3.7642
simulation,         4,      0.68,   0.66706,   0.66711,    546.52,    76.799,         1,         0.0
simulation,         5,      0.65,   0.64391,    0.6439,    475.15,     66.36,         1,         0.0
simulation,         6,      0.63,   0.62667,   0.62668,    297.21,    57.656,         1,         0.0
simulation,         7,      0.15,   0.15005,   0.15005,    44.609,    44.615,         0,      3.8215
simulation,         8,       0.3,    0.3001,    0.3001,    44.423,    44.429,         0,       3.778
simulation,         9,      0.45,   0.44993,   0.44993,    45.081,    45.087,         0,      3.7588
simulation,        10,       0.6,   0.59853,   0.59853,    226.62,    54.612,         0,      3.7607
