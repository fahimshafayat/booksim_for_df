packet_size = 1;
sw_allocator = separable_input_first;
output_speedup = 1;
injection_rate_uses_flits = 1;
sw_alloc_delay = 1;
warmup_periods = 3;
input_speedup = 1;
vc_allocator = separable_input_first;
vc_alloc_delay = 1;
sim_count = 1;
alloc_iters = 1;
st_final_delay = 1;
wait_for_tail_credit = 0;
sample_period = 10000;
credit_delay = 2;
routing_delay = 0;
priority = none;
df_a = 26;
df_arrangement = absolute_improved;
df_g = 27;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
shift_offset = 1;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
num_vcs = 4;
routing_function = UGAL_G;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.18278,   0.19769,    2994.6,    750.89,         1,         0.0
simulation,         2,      0.25,   0.17661,   0.19011,    1257.4,    644.33,         1,         0.0
simulation,         3,      0.13,   0.13001,   0.13001,    60.332,    60.344,         0,      4.8458
simulation,         4,      0.19,   0.16933,   0.17386,    817.17,    422.37,         1,         0.0
simulation,         5,      0.16,   0.16003,   0.16003,    61.614,    61.627,         0,      4.8536
simulation,         6,      0.17,   0.17002,   0.17002,    61.773,    61.787,         0,      4.8543
simulation,         7,      0.18,   0.18002,   0.18002,    61.787,    61.801,         0,      4.8544
simulation,         8,      0.05,  0.050012,   0.05001,    58.395,    58.409,         0,      4.8758
simulation,         9,       0.1,   0.10001,   0.10001,    58.956,    58.968,         0,      4.8393
simulation,        10,      0.15,   0.15002,   0.15002,     61.28,    61.293,         0,      4.8519
