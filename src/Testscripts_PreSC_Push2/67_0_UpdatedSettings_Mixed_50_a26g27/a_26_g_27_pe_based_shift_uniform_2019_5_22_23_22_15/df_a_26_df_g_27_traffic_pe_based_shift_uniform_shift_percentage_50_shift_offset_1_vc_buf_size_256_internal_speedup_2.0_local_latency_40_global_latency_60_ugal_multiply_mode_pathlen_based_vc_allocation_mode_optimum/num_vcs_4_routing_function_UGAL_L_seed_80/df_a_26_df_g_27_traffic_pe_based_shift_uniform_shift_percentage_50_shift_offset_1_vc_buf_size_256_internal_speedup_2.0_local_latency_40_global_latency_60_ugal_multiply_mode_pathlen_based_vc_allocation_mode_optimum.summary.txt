wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 26;
df_arrangement = absolute_improved;
df_g = 27;
global_latency = 60;
internal_speedup = 2.0;
local_latency = 40;
shift_offset = 1;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 256;
num_vcs = 4;
routing_function = UGAL_L;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.21895,   0.34224,    2259.6,    2235.7,         1,         0.0
simulation,         2,      0.25,   0.18947,   0.24968,    893.17,    893.17,         1,         0.0
simulation,         3,      0.13,   0.12868,   0.13003,    546.69,    546.69,         1,         0.0
simulation,         4,      0.07,  0.070014,  0.070015,    166.99,    167.08,         0,      4.2987
simulation,         5,       0.1,   0.10001,   0.10001,    285.59,    286.16,         0,      4.4437
simulation,         6,      0.11,   0.10991,   0.11002,    367.33,    377.56,         0,       4.574
simulation,         7,      0.12,    0.1195,   0.11992,    493.71,    504.09,         1,         0.0
simulation,         8,      0.05,  0.050009,   0.05001,    169.38,    169.48,         0,      4.3606
simulation,         9,       0.1,   0.10001,   0.10001,    285.59,    286.16,         0,      4.4437
