st_final_delay = 1;
sample_period = 10000;
alloc_iters = 1;
credit_delay = 2;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
priority = none;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
output_speedup = 1;
warmup_periods = 3;
routing_delay = 0;
injection_rate_uses_flits = 1;
sim_count = 1;
wait_for_tail_credit = 0;
input_speedup = 1;
packet_size = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_buf_size = 32;
num_vcs = 6;
routing_function = UGAL_L_restricted_src_only;
seed = 81;
shift_by_group = 1;
vc_allocation_mode = incremental;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.36589,   0.38258,    1070.4,    405.47,         1,         0.0
simulation,         2,      0.25,   0.24985,   0.24984,    79.703,    79.726,         0,      4.4323
simulation,         3,      0.37,   0.34122,   0.34645,    661.56,     268.0,         1,         0.0
simulation,         4,      0.31,   0.30778,   0.30781,    507.78,    179.42,         1,         0.0
simulation,         5,      0.28,   0.27985,   0.27986,    91.024,    91.065,         0,      4.4667
simulation,         6,      0.29,   0.28992,   0.28992,    95.084,    95.132,         0,      4.4768
simulation,         7,       0.3,   0.29969,   0.29973,    143.57,    123.75,         0,      4.4865
simulation,         8,      0.05,  0.049853,  0.049851,     52.44,    52.446,         0,      4.3456
simulation,         9,       0.1,  0.099918,  0.099918,    52.528,    52.533,         0,      4.3336
simulation,        10,      0.15,   0.14984,   0.14984,    52.966,    52.969,         0,      4.3227
simulation,        11,       0.2,   0.19981,    0.1998,    57.639,    57.645,         0,        4.34
simulation,        12,      0.25,   0.24985,   0.24984,    79.703,    79.726,         0,      4.4323
simulation,        13,       0.3,   0.29969,   0.29973,    143.57,    123.75,         0,      4.4865
