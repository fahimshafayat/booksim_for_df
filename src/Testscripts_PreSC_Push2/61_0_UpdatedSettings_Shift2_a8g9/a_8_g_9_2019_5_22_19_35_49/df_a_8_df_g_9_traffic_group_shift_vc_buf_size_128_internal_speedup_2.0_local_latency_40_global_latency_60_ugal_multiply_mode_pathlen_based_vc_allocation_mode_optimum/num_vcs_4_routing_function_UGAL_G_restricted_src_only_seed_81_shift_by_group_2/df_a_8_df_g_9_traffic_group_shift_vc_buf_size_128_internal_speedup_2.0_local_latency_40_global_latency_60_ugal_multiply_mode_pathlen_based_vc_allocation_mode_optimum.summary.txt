wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 9;
global_latency = 60;
internal_speedup = 2.0;
local_latency = 40;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 128;
num_vcs = 4;
routing_function = UGAL_G_restricted_src_only;
seed = 81;
shift_by_group = 2;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.32305,   0.37348,    1429.6,    1125.1,         1,         0.0
simulation,         2,      0.25,   0.24994,   0.24984,    197.22,    197.32,         0,      4.4057
simulation,         3,      0.37,   0.32919,   0.35728,    552.19,    539.25,         1,         0.0
simulation,         4,      0.31,   0.30991,   0.30993,    199.69,    199.73,         0,       4.381
simulation,         5,      0.34,    0.3345,   0.33507,    539.69,    443.63,         1,         0.0
simulation,         6,      0.32,   0.31999,   0.31998,    210.31,    210.39,         0,      4.3811
simulation,         7,      0.33,   0.32994,   0.32993,    254.85,     255.3,         0,      4.3983
simulation,         8,       0.1,  0.099915,  0.099918,    180.84,     180.9,         0,      4.4136
simulation,         9,       0.2,   0.19975,    0.1998,    187.61,    187.65,         0,      4.4029
simulation,        10,       0.3,   0.29993,   0.29995,    197.84,    197.89,         0,      4.3802
