wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 26;
df_arrangement = absolute_improved;
df_g = 27;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 128;
num_vcs = 4;
routing_function = UGAL_L_restricted_src_only;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.19186,   0.25452,    2311.2,    1916.2,         1,         0.0
simulation,         2,      0.25,   0.19463,   0.23887,    659.89,    659.17,         1,         0.0
simulation,         3,      0.13,      0.13,   0.13001,    173.82,    174.79,         0,      4.7619
simulation,         4,      0.19,   0.17259,   0.18653,    640.61,    634.05,         1,         0.0
simulation,         5,      0.16,   0.15429,   0.15831,    643.21,    635.43,         1,         0.0
simulation,         6,      0.14,   0.13888,   0.13949,     495.1,    447.15,         1,         0.0
simulation,         7,      0.05,  0.050011,   0.05001,    54.475,    54.478,         0,      4.4524
simulation,         8,       0.1,   0.10001,   0.10001,    142.62,    142.94,         0,      4.6603
