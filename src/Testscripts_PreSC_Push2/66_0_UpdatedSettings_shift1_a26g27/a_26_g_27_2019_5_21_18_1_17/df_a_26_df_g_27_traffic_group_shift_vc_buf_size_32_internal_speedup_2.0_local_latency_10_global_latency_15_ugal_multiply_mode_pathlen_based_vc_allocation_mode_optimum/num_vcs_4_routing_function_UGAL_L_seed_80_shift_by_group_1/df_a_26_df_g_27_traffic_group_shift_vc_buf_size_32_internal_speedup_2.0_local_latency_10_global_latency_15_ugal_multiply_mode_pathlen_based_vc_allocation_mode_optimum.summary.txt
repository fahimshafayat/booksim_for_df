wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 26;
df_arrangement = absolute_improved;
df_g = 27;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
num_vcs = 4;
routing_function = UGAL_L;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,  0.080233,  0.095835,    4162.5,    1680.3,         1,         0.0
simulation,         2,      0.25,  0.080883,  0.096495,    3299.6,    1600.7,         1,         0.0
simulation,         3,      0.13,  0.080994,  0.096606,    1781.5,    1340.6,         1,         0.0
simulation,         4,      0.07,  0.070016,  0.070015,    155.94,    156.18,         0,       5.346
simulation,         5,       0.1,  0.080714,  0.095115,     889.0,    863.64,         1,         0.0
simulation,         6,      0.08,  0.078931,  0.079937,    508.36,    505.73,         1,         0.0
simulation,         7,      0.02,  0.019999,  0.019999,    58.798,     58.81,         0,      4.9096
simulation,         8,      0.04,   0.04001,   0.04001,    59.281,    59.292,         0,      4.8947
simulation,         9,      0.06,   0.06001,  0.060011,    101.83,    101.87,         0,      5.1007
