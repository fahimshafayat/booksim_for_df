wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 26;
df_arrangement = absolute_improved;
df_g = 27;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
topology = dragonflyfull;
traffic = group_shift;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
num_vcs = 4;
routing_function = UGAL_G_restricted_src_only;
seed = 80;
shift_by_group = 1;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.17923,   0.19427,    3087.3,    760.42,         1,         0.0
simulation,         2,      0.25,   0.18668,   0.19905,    977.02,     518.8,         1,         0.0
simulation,         3,      0.13,      0.13,   0.13001,    65.835,    65.838,         0,      4.8495
simulation,         4,      0.19,   0.19002,   0.19002,    65.838,    65.844,         0,      4.8597
simulation,         5,      0.22,   0.18688,   0.19593,    563.85,     368.9,         1,         0.0
simulation,         6,       0.2,   0.18623,   0.18935,    522.12,    275.68,         1,         0.0
simulation,         7,      0.05,  0.050011,   0.05001,    59.935,    59.938,         0,      4.7842
simulation,         8,       0.1,   0.10001,   0.10001,    63.609,    63.614,         0,      4.8273
simulation,         9,      0.15,      0.15,   0.15002,    66.404,     66.41,         0,        4.86
