wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
shift_offset = 2;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
vc_buf_size = 32;
num_vcs = 4;
routing_function = UGAL_G_restricted_src_only;
seed = 80;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.37205,   0.38231,    795.59,    253.62,         1,         0.0
simulation,         2,      0.25,   0.25024,   0.25024,    52.157,    52.163,         0,      4.1738
simulation,         3,      0.37,   0.34121,   0.34405,    515.71,    156.33,         1,         0.0
simulation,         4,      0.31,   0.30961,   0.30961,    200.62,    65.104,         0,      4.1791
simulation,         5,      0.34,   0.33014,   0.33016,     524.0,    112.38,         1,         0.0
simulation,         6,      0.32,    0.3176,    0.3176,    404.39,    76.319,         1,         0.0
simulation,         7,       0.1,   0.10017,   0.10017,    50.641,    50.648,         0,      4.2137
simulation,         8,       0.2,   0.20019,    0.2002,    51.599,    51.604,         0,      4.1875
simulation,         9,       0.3,   0.30022,   0.30023,    56.739,    56.754,         0,      4.1711
