input_speedup = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
output_speedup = 1;
sw_alloc_delay = 1;
wait_for_tail_credit = 0;
st_final_delay = 1;
priority = none;
warmup_periods = 3;
sim_count = 1;
credit_delay = 2;
injection_rate_uses_flits = 1;
routing_delay = 0;
sample_period = 10000;
packet_size = 1;
vc_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
shift_offset = 2;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
num_vcs = 4;
routing_function = UGAL_G;
seed = 80;
vc_buf_size = 32;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.32225,   0.33413,    1050.8,    330.82,         1,         0.0
simulation,         2,      0.25,   0.25023,   0.25024,    62.456,    62.466,         0,      4.4551
simulation,         3,      0.37,   0.29207,   0.30071,     644.5,    271.45,         1,         0.0
simulation,         4,      0.31,   0.27643,   0.27946,    716.87,    208.63,         1,         0.0
simulation,         5,      0.28,   0.26726,   0.26733,    817.45,    158.21,         1,         0.0
simulation,         6,      0.26,   0.25824,   0.25825,    416.93,    86.518,         1,         0.0
simulation,         7,      0.05,  0.050083,  0.050086,    53.824,    53.834,         0,      4.5164
simulation,         8,       0.1,   0.10017,   0.10017,      53.6,    53.613,         0,      4.4781
simulation,         9,      0.15,    0.1502,   0.15021,     54.08,    54.091,         0,      4.4644
simulation,        10,       0.2,   0.20019,    0.2002,    54.888,    54.897,         0,       4.451
simulation,        11,      0.25,   0.25023,   0.25024,    62.456,    62.466,         0,      4.4551
