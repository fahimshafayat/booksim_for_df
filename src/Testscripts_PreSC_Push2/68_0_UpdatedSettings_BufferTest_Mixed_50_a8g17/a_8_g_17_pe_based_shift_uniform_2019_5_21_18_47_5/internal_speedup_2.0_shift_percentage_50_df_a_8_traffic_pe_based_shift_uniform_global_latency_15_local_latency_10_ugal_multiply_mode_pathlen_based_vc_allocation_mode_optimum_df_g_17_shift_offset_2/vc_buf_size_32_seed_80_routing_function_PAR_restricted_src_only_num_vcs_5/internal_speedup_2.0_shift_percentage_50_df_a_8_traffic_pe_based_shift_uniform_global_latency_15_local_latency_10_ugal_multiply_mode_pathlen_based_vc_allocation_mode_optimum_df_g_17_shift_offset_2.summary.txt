input_speedup = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
output_speedup = 1;
sw_alloc_delay = 1;
wait_for_tail_credit = 0;
st_final_delay = 1;
priority = none;
warmup_periods = 3;
sim_count = 1;
credit_delay = 2;
injection_rate_uses_flits = 1;
routing_delay = 0;
sample_period = 10000;
packet_size = 1;
vc_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
shift_offset = 2;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
num_vcs = 5;
routing_function = PAR_restricted_src_only;
seed = 80;
vc_buf_size = 32;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.44429,   0.44809,    656.74,    163.64,         1,         0.0
simulation,         2,      0.25,   0.25024,   0.25024,    56.818,    56.829,         0,      4.5729
simulation,         3,      0.37,   0.36807,   0.36809,    313.65,     73.48,         1,         0.0
simulation,         4,      0.31,   0.31022,   0.31024,    58.115,    58.125,         0,      4.5618
simulation,         5,      0.34,   0.34029,    0.3403,    60.196,    60.208,         0,      4.5568
simulation,         6,      0.35,   0.34992,   0.34992,    99.306,    66.285,         0,      4.5543
simulation,         7,      0.36,   0.35897,   0.35897,    325.72,    70.614,         0,      4.5496
simulation,         8,       0.1,   0.10016,   0.10017,    56.553,    56.565,         0,      4.6608
simulation,         9,       0.2,   0.20019,    0.2002,    56.327,    56.338,         0,      4.5896
simulation,        10,       0.3,   0.30023,   0.30023,    57.764,    57.775,         0,      4.5638
