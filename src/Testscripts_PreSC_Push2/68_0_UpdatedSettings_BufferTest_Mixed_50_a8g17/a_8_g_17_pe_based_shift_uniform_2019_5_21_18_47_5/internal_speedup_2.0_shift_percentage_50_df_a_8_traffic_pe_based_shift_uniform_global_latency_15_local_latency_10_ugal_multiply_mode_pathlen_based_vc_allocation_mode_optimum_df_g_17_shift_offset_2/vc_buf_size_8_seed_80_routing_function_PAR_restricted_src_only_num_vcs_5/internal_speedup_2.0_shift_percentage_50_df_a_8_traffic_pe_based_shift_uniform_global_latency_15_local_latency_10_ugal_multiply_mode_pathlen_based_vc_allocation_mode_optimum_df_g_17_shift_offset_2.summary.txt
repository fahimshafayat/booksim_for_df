input_speedup = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
output_speedup = 1;
sw_alloc_delay = 1;
wait_for_tail_credit = 0;
st_final_delay = 1;
priority = none;
warmup_periods = 3;
sim_count = 1;
credit_delay = 2;
injection_rate_uses_flits = 1;
routing_delay = 0;
sample_period = 10000;
packet_size = 1;
vc_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
shift_offset = 2;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
num_vcs = 5;
routing_function = PAR_restricted_src_only;
seed = 80;
vc_buf_size = 8;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.14292,   0.14734,    2539.6,    293.28,         1,         0.0
simulation,         2,      0.25,   0.13876,   0.14282,    1245.5,    270.69,         1,         0.0
simulation,         3,      0.13,   0.11381,   0.11493,    767.31,    182.35,         1,         0.0
simulation,         4,      0.07,  0.070101,    0.0701,    57.843,    57.855,         0,      4.7078
simulation,         5,       0.1,  0.098156,  0.098153,    502.24,    100.25,         1,         0.0
simulation,         6,      0.08,  0.080104,  0.080107,    58.819,    58.831,         0,      4.6862
simulation,         7,      0.09,   0.09002,  0.090019,    92.945,    65.206,         0,       4.659
simulation,         8,      0.02,  0.020053,  0.020053,     58.53,    58.543,         0,      4.8276
simulation,         9,      0.04,  0.040083,  0.040084,    58.057,    58.068,         0,       4.782
simulation,        10,      0.06,  0.060102,  0.060106,    57.694,    57.706,         0,      4.7305
simulation,        11,      0.08,  0.080104,  0.080107,    58.819,    58.831,         0,      4.6862
