input_speedup = 1;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
output_speedup = 1;
sw_alloc_delay = 1;
wait_for_tail_credit = 0;
st_final_delay = 1;
priority = none;
warmup_periods = 3;
sim_count = 1;
credit_delay = 2;
injection_rate_uses_flits = 1;
routing_delay = 0;
sample_period = 10000;
packet_size = 1;
vc_alloc_delay = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 15;
internal_speedup = 2.0;
local_latency = 10;
shift_offset = 2;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
num_vcs = 4;
routing_function = UGAL_G_restricted_src_only;
seed = 80;
vc_buf_size = 64;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.48791,   0.48807,    501.57,    130.82,         1,         0.0
simulation,         2,      0.25,   0.25024,   0.25024,    52.375,    52.382,         0,      4.1806
simulation,         3,      0.37,   0.36993,   0.36995,    88.705,    62.158,         0,      4.2163
simulation,         4,      0.43,   0.42923,   0.42923,    233.37,    69.723,         0,      4.2562
simulation,         5,      0.46,   0.45865,    0.4587,    329.26,    75.035,         0,      4.2729
simulation,         6,      0.48,   0.47607,   0.47613,    444.47,    87.635,         1,         0.0
simulation,         7,      0.47,   0.46768,   0.46766,    336.53,    83.972,         1,         0.0
simulation,         8,       0.1,   0.10016,   0.10017,    50.638,    50.644,         0,      4.2138
simulation,         9,       0.2,    0.2002,    0.2002,    51.619,    51.625,         0,       4.189
simulation,        10,       0.3,   0.30022,   0.30023,    53.267,    53.274,         0,      4.1758
simulation,        11,       0.4,   0.39973,   0.39974,     139.9,    68.869,         0,      4.2385
