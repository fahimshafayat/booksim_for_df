wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 60;
internal_speedup = 2.0;
local_latency = 40;
shift_offset = 2;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
num_vcs = 5;
routing_function = PAR_restricted_src_only;
seed = 80;
vc_buf_size = 64;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,    0.3109,   0.33904,    1099.8,    745.34,         1,         0.0
simulation,         2,      0.25,   0.24104,   0.24103,    655.38,    337.92,         1,         0.0
simulation,         3,      0.13,    0.1302,    0.1302,    182.82,    182.98,         0,      4.5526
simulation,         4,      0.19,   0.19022,   0.19022,    186.31,    186.45,         0,      4.5394
simulation,         5,      0.22,   0.21855,   0.21856,    449.26,    233.49,         1,         0.0
simulation,         6,       0.2,   0.20019,    0.2002,    189.49,    189.61,         0,      4.5366
simulation,         7,      0.21,   0.21003,   0.21005,    229.38,    204.51,         0,       4.531
simulation,         8,      0.05,  0.050084,  0.050086,    185.62,    185.76,         0,      4.6201
simulation,         9,       0.1,   0.10016,   0.10017,    182.98,    183.11,         0,      4.5659
simulation,        10,      0.15,    0.1502,   0.15021,     183.4,    183.54,         0,      4.5488
simulation,        11,       0.2,   0.20019,    0.2002,    189.49,    189.61,         0,      4.5366
