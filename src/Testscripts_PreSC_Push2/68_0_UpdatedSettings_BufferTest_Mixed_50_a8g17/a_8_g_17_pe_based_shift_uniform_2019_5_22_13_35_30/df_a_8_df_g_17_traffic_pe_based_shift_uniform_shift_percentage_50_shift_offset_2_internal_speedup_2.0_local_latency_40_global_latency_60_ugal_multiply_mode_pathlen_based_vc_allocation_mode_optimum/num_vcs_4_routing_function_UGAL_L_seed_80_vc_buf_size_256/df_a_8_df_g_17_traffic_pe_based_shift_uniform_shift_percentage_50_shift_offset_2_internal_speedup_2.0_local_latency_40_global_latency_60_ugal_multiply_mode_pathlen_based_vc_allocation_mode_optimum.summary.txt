wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 60;
internal_speedup = 2.0;
local_latency = 40;
shift_offset = 2;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
num_vcs = 4;
routing_function = UGAL_L;
seed = 80;
vc_buf_size = 256;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.45394,   0.49112,    447.13,    446.89,         1,         0.0
simulation,         2,      0.25,   0.25024,   0.25024,    253.58,    254.08,         0,      4.5706
simulation,         3,      0.37,   0.36681,   0.36731,    486.58,    460.08,         1,         0.0
simulation,         4,      0.31,    0.3102,   0.31024,    278.37,    279.26,         0,      4.7174
simulation,         5,      0.34,   0.33971,   0.34016,    357.31,    356.27,         0,       4.772
simulation,         6,      0.35,   0.34914,   0.34954,    449.85,    404.61,         0,      4.7876
simulation,         7,      0.36,   0.35839,   0.35895,     484.6,    437.85,         1,         0.0
simulation,         8,       0.1,   0.10016,   0.10017,    168.71,    168.83,         0,      4.3163
simulation,         9,       0.2,   0.20018,    0.2002,     224.3,    224.58,         0,      4.4027
simulation,        10,       0.3,   0.30022,   0.30023,    274.12,    274.96,         0,      4.6954
