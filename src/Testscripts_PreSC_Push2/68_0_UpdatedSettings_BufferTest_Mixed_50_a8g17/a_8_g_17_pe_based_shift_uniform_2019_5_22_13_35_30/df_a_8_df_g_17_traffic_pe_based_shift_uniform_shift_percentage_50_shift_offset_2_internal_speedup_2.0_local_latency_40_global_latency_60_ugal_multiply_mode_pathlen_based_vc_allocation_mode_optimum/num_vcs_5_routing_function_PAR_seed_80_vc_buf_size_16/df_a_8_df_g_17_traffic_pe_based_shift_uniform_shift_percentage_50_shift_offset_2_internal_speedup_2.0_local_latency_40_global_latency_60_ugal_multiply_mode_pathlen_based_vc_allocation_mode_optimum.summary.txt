wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 60;
internal_speedup = 2.0;
local_latency = 40;
shift_offset = 2;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
num_vcs = 5;
routing_function = PAR;
seed = 80;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,  0.075607,  0.084956,    3506.4,    1006.3,         1,         0.0
simulation,         2,      0.25,  0.072393,  0.081385,    2492.3,    1017.2,         1,         0.0
simulation,         3,      0.13,  0.069704,  0.077693,    1395.3,    884.42,         1,         0.0
simulation,         4,      0.07,  0.057719,  0.062426,     638.4,    573.02,         1,         0.0
simulation,         5,      0.04,   0.04008,  0.040084,    204.65,    204.85,         0,      4.9463
simulation,         6,      0.05,  0.048769,  0.048822,    534.06,    345.76,         1,         0.0
simulation,         7,      0.02,  0.020049,  0.020053,    205.12,    205.35,         0,      5.0835
simulation,         8,      0.04,   0.04008,  0.040084,    204.65,    204.85,         0,      4.9463
