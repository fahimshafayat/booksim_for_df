wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 60;
internal_speedup = 2.0;
local_latency = 40;
shift_offset = 2;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
num_vcs = 4;
routing_function = UGAL_G;
seed = 80;
vc_buf_size = 32;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,   0.11269,   0.12776,    3097.1,    1130.1,         1,         0.0
simulation,         2,      0.25,   0.10616,   0.12004,    1957.5,    1064.7,         1,         0.0
simulation,         3,      0.13,  0.089979,   0.10072,    1062.6,    862.31,         1,         0.0
simulation,         4,      0.07,  0.070096,    0.0701,    190.38,    190.54,         0,      4.4451
simulation,         5,       0.1,  0.082147,  0.089646,    681.97,    627.23,         1,         0.0
simulation,         6,      0.08,    0.0762,  0.078119,    459.97,    403.23,         1,         0.0
simulation,         7,      0.02,  0.020053,  0.020053,    177.97,    178.12,         0,      4.5034
simulation,         8,      0.04,  0.040076,  0.040084,    176.65,    176.81,         0,      4.4725
simulation,         9,      0.06,  0.060104,  0.060106,    176.97,    177.11,         0,      4.4498
