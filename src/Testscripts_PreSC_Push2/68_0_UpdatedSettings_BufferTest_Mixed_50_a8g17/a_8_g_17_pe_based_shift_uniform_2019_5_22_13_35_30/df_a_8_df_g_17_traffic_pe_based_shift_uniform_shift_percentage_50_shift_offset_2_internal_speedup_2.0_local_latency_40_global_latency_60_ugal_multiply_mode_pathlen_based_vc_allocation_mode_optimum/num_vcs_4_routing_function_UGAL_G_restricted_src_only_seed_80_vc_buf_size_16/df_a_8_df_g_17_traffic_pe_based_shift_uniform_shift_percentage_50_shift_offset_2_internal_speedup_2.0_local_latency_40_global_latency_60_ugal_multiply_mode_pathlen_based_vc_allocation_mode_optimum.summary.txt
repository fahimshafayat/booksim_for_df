wait_for_tail_credit = 0;
vc_allocator = separable_input_first;
sw_allocator = separable_input_first;
alloc_iters = 1;
credit_delay = 2;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
input_speedup = 1;
output_speedup = 1;
warmup_periods = 3;
sim_count = 1;
sample_period = 10000;
priority = none;
packet_size = 1;
injection_rate_uses_flits = 1;
df_a = 8;
df_arrangement = absolute_improved;
df_g = 17;
global_latency = 60;
internal_speedup = 2.0;
local_latency = 40;
shift_offset = 2;
shift_percentage = 50;
topology = dragonflyfull;
traffic = pe_based_shift_uniform;
ugal_multiply_mode = pathlen_based;
vc_allocation_mode = optimum;
num_vcs = 4;
routing_function = UGAL_G_restricted_src_only;
seed = 80;
vc_buf_size = 16;

defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, flit latency, deadlock?, average_hop_length
simulation,         1,       0.5,  0.062845,  0.070546,    3886.3,    1022.4,         1,         0.0
simulation,         2,      0.25,  0.063322,  0.070998,    2885.4,    1021.7,         1,         0.0
simulation,         3,      0.13,   0.05972,  0.066694,    1904.9,    952.92,         1,         0.0
simulation,         4,      0.07,  0.052938,   0.05802,    868.54,    685.19,         1,         0.0
simulation,         5,      0.04,  0.040079,  0.040084,    177.25,    177.29,         0,      4.1914
simulation,         6,      0.05,  0.047204,  0.048389,    490.85,    398.21,         1,         0.0
simulation,         7,      0.02,   0.02005,  0.020053,    167.58,    167.66,         0,      4.2287
simulation,         8,      0.04,  0.040079,  0.040084,    177.25,    177.29,         0,      4.1914
