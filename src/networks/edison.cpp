/*
Bare-bone implementation of Edison (Cray Cascade) topology.

Will use Peyman's implementation as a guide, but still starting a new file instead of modifying his as this is supposed to be cleaner.

- Shafayat, Nov 20 2018
*/

//TODO:
// 1. make port_map -> DONE
// 2. complete alloc 
// 3. complete _BuildNet
// 4. write min_edison()


#include "booksim.hpp"
#include "edison.hpp"
#include "random_utils.hpp"
#include "global_stats.hpp"

#include <string>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <sstream>
#include <algorithm>
#include <ctime>
using namespace std;

#define FLIT_TO_TRACK -1
// #define FLIT_TO_TRACK 48707 //min
// #define FLIT_TO_TRACK 54591 //src non min, tier 0
// #define FLIT_TO_TRACK 71746 //src non min, tier 1
// #define FLIT_TO_TRACK 373088 //src non min, tier 2
//#define FLIT_TO_TRACK 10826 //min -> non min, tier 0
// #define FLIT_TO_TRACK 126749 //min -> non min, tier 1
// #define FLIT_TO_TRACK 103130 //min -> non min, tier 2

#define PAKCET_CATCH false
#define PACKET_CATCH2 false
#define PACKET_CATCH3 false


//string ed_edison_connection_filename = "/home/rahman/BooksimResourcesUpdated/edison_globallinks.txt"; //for Tesla

string ed_edison_connection_filename = "/nfsdata/rahman/BooksimResources/edison_globallinks.txt";   //for the Big Six

//string ed_edison_connection_filename = "edison_globallinks.txt"; //local

std::vector < std::vector <int> > ed_graph;
std::vector < std::vector <int> > ed_link_weights;
std::vector < std::vector <int> > ed_link_widths;

// g_graph[0][5] = 10 means node 0 is connected to node 10 as its sixth neighbor.
// g_link_weights[0][5] = 3 means the weight of the link between node 0 
// and its sixth neighbor (node 10) is 3.
//     // weight 3 -> global link
//     // weight 1 -> local link
// g_link_widths[0][5] = 2 means the width of the link between node 0 and its sixth neighbor (node 10) is 2.
//     //in Edison, column-wise node pairs have 3 links beteen them, so 
//     //we consider them as one link with wieght 3. Similarly, global links
//     //have weight of 2.
                        
// These three arrays could've been glued together, but I think this is cleaner.


//We need all the channels between groups for routing
std::vector< std::vector < std::vector <std::pair<int,int> > > > ed_inter_group_links;
    //ed_inter_group_links[src_group][dst_group] -> a vector of (s,d) pairs listing all the global links between src_group and dst_group

std::unordered_map< std::pair<int, int>, std::pair<int,int>, pair_hash >  ed_port_map; 
    //ed_port_map[(0,5)] = (5,6) means node 0 is connected to node 5, and there are two output ports, ports 5 and 6, from node 0 to 5.

//Edison parameters. we need to access some of them from the routing function.
//so making them global.
int ed_a;
int ed_g;
int ed_p;
int ed_N;
int ed_dim;
int ed_dims[2] = {16, 6}; // Group dimensions, 16 x 6 router-processor pairs
string ed_routing_mode; 

int ed_log_Qlen_data;
//flie handle to write Q_len data
ofstream ed_q_len_file;

int ed_mt_thld_1 = -1;      //threshold for ugal_mt routing in edison.
int ed_mt_thld_2 = -1;      //default values. not changing it through config 
                            //file will convert UGAL_L_mt to regular UGAL_L

//data structure to collect routing stats
long int ed_total_min_flit; 
long int ed_total_non_min_flit;
long int ed_ingroup_path_dist[ED_MAX_PATH_LEN];
long int ed_outgroup_path_dist[ED_MAX_PATH_LEN];

#define ED_LOCAL_LINK_WEIGHT 1
#define ED_GLOBAL_LINK_WEIGHT 3

Edison::Edison(const Configuration &config, const string &name) : Network(config, name){
    
    cout << "inside Edison() constructor ..." << endl;

    //trafficmanager initializes the seed. 
    //This block is only needed if we want to play with the constructor and test/debug something.
    /*int seed = config.GetInt("seed");
    RandomSeed(seed);*/

    //these three needs to be parameterized later
    _dim = 2; //dimension
    _a = 96; //no of routers in a group
    _g = 15; //no of Edison groups
    _p = 4; //no of processors per router
    
    _N = _a * _g; //no of routers in the system

    _radix = _p + 15 * 1 + 5 * 3 ;
            // _p -> ports to PEs
            // 15 single-channel links to nodes in the same row
            // 5 three-channel links to nodes in different rows
            // global link channels are not included here and needs to be updated before use.

    //_local_latency = config.GetInt("local_latency");
    //_global_latency = config.GetInt("global_latency");
    _local_latency = 10;
    _global_latency = 10;

    _routing = config.GetStr("routing_function");
    
    ed_mt_thld_1 = config.GetInt("multitiered_routing_threshold_0");
    ed_mt_thld_2 = config.GetInt("multitiered_routing_threshold_1");

    /*_local_latency = config.GetInt("local_latency");
    _global_latency = config.GetInt("global_latency");*/

    ed_log_Qlen_data = config.GetInt("log_Qlen_data");
    
    if (ed_log_Qlen_data == 1){

        // current date/time based on current system
        time_t now = time(0);
        tm *ltm = localtime(&now);

        int year, month, day, hour, min, sec;

        year  = 1900 + ltm->tm_year;
        month = 1 + ltm->tm_mon;
        day = ltm->tm_mday;
        hour = ltm->tm_hour;
        min = ltm->tm_min;
        sec = ltm->tm_sec;
       
        int i_rate = int(config.GetFloat("injection_rate")*100); 
        string q_len_file_name = "QLenData/Qdata_" + std::to_string(_a) + "_" +  std::to_string(_g) + "_" + _routing + "_" + config.GetStr("traffic") + "_" + std::to_string(i_rate) +  "_" + std::to_string(year) + "_" + std::to_string(month) +"_" + std::to_string(day) + "_" + std::to_string(hour)  + "_" + std::to_string(min) + "_" + std::to_string(sec) +".qdata";
        cout << "q_len_file_name: " << q_len_file_name << endl;   
        ed_q_len_file.open(q_len_file_name); 

        if (ed_q_len_file.is_open() == false){
            cout << "Error opening Qlen_data file!" << endl;
        }
    }

    
    _setGlobals();
    _setRoutingMode();
    _AllocateArrays();
    _BuildGraphForLocal();
    _BuildGraphForGlobal();
    //_printGraph();
    _CreatePortMap();
    
    _ComputeSize( config );
    _Alloc( );
    _BuildNet( config );
    
    // _discover_djkstra_paths();
    // _generate_one_hop_neighbors();
    // _generate_two_hop_neighbors();

    /*std::vector<int> pathVector;
    select_min_path(3, 166, pathVector);
    print_vector(pathVector);*/
    
    cout << "Done with Edison() constructor ..." << endl;
    
    //for test
    /*int src_router = 0;
    int dst_router = 10;
    int min_q_len = 55; 
    Flit * f  = Flit::New();

    //generate_multitiered_imdt_node(f, src_router, dst_router, min_q_len);

    exit(-1);*/
}

void Edison::_setGlobals(){
    /*
    Edison parameters. we need to access some of them from the routing function.
    so made them global.
    */
    ed_dim = _dim;
    ed_a = _a;
    ed_g = _g;
    ed_p = _p;
    ed_N = _N;

    //set globals
    //stat collection variables
    ed_total_min_flit = 0;
    ed_total_non_min_flit = 0;

    for(int ii = 0; ii < ED_MAX_PATH_LEN; ii++){
        ed_ingroup_path_dist[ii] = 0;
        ed_outgroup_path_dist[ii] = 0;
    }

        
}

void Edison::_setRoutingMode(){
    
    //possible supported routings: 
        //min/ vlb/ UGAL_L/ UGAL_L_two_hop / UGAL_L_threshold/
        //UGAL_L_multi_tiered // PAR

    //possible modes: vanilla_ugal, two_hop, threshold, multi_tiered, not_applicable

    if (_routing == "UGAL_L") {
        ed_routing_mode = "ugal_l";
    }
    else if (_routing == "PAR"){
        ed_routing_mode = "par";
    } 
    else if ((_routing == "UGAL_mt_src") || (_routing == "PAR_mt_src")){
        ed_routing_mode = "mt_src";
    } 
    else if (_routing == "UGAL_L_mt"){
        ed_routing_mode = "mt";
    }
    else if (_routing == "UGAL_L_restricted"){
        ed_routing_mode = "ugal_l_restricted";    
    }

    /*else if (_routing == "UGAL_L_two_hop"){
        g_routing_mode = "two_hop";    
    }
    else if (_routing == "UGAL_L_threshold"){
        g_routing_mode = "threshold";    
    }
    else if ((_routing == "UGAL_L_multi_tiered") || (_routing == "PAR_multi_tiered")){
        g_routing_mode = "multi_tiered";    
    }*/
    else{
        ed_routing_mode = "not_applicable";    
    }

}


void Edison::_ComputeSize(const Configuration &config){
    cout << "_Computesize starts ..." << endl;
    
    //values needs to be provided
    
    //_nodes -> no of PEs
    _nodes = _a * _g * _p;
    cout << "_nodes: " << _nodes << endl;
    
    //_size -> no of routers
    _size = _a * _g;
    cout << "_size: " << _size << endl;
    
    //_channels -> no of uni-directional links between routers only

    //count the channels using ed_link_widths
    //There are cleverer ways. But not much overhead anyway.
    int channel_count = 0;
    for(size_t ii = 0; ii < ed_link_widths.size(); ii++){
        for(size_t jj = 0; jj < ed_link_widths[ii].size(); jj++){    
            channel_count += ed_link_widths[ii][jj];
        }
    }
    
    _channels = channel_count;

    cout << "_channels: " << _channels << endl;
    
    cout << "_ComputeSize ends ..." << endl;
}

void Edison::_AllocateArrays(){
    //allocate the graph, link_weight and link_width arrays

    cout << "inside _AllocateArrays() ... " << endl;
    
    ed_graph.resize(_N);
    for (std::size_t ii=0; ii < ed_graph.size(); ii++){
        ed_graph[ii] = std::vector<int> (ed_dims[0]-1+ ed_dims[1]-1, -1);  
                //array allocated only for the local links. 
                //There will be one or two extra global links, but those can be just pushed_back execution time.
                //-1 default value
    }
    
    ed_link_weights.resize(_N);
    for (std::size_t ii=0; ii < ed_link_weights.size(); ii++){
        ed_link_weights[ii] = std::vector<int> (ed_dims[0]-1+ ed_dims[1]-1, -1);  
            //only for local links, -1 default value
    }
    
    ed_link_widths.resize(_N);
    for (std::size_t ii=0; ii < ed_link_widths.size(); ii++){
        ed_link_widths[ii] = std::vector<int> (ed_dims[0]-1+ ed_dims[1]-1, -1);  //only for local links, -1 default value
    }
    
    ed_inter_group_links.resize(_g, std::vector< std::vector <std::pair<int,int>> >  (_g) );

    
    out_port_tracker.resize(_N, 0);
    in_port_tracker.resize(_N, 0);
    
    
    //print_2d_vector(g_link_weights);
    //test_vector_of_maps(g_port_map);
    
    cout << "done with _AllocateArrays() ... " << endl;
    
}


void Edison::_BuildGraphForLocal(){
    cout << "inside _BuildGraphForLocal() ..." << endl;

    int group_id;

    for(group_id = 0; group_id < _g; group_id++){ 
        _BuildASingleGroup(group_id);
    }

    cout << "done with _BuildGraphForLocal() ..." << endl;
}

void Edison::_BuildASingleGroup(int group_id){
    // An edison group contains 6 rows and 16 columns.
    // Each node in a column is fully connected with each other with 1 link.
    // Each node in a row is connected to the same-column element in five other rows. 

    //ed_dims[2] = {16, 6}

    int col, row;
    int col_idx;
    int src, dst; 
    int neighbor_count;

    for (row = 0; row < ed_dims[1]; row++){
        for(col = 0; col < ed_dims[0]; col++){
            src = group_id * _a + row * ed_dims[0] + col;
            //cout << src << " : ";
            neighbor_count = 0;
            
            //connect src to all other nodes in the same row
            for(dst = group_id * _a + row * ed_dims[0]; dst < group_id * _a + (row+1) * ed_dims[0]; dst++ ){
                    if (dst == src){
                        continue;
                    }else{
                        //cout << dst << " ";
                        ed_graph[src][neighbor_count] = dst;
                        ed_link_widths[src][neighbor_count] = 1; //1 wire per link row-wise
                        ed_link_weights[src][neighbor_count] = ED_LOCAL_LINK_WEIGHT; //local link
                        
                        neighbor_count += 1;
                    }
            }

            //now connect all the nodes in different row but same columns
            for(col_idx = 0; col_idx < ed_dims[1]; col_idx++ ){
                    dst = group_id * _a + col_idx * ed_dims[0] + col;
                    if (dst == src){
                        continue;
                    }else{
                        //cout << dst << " ";
                        ed_graph[src][neighbor_count] = dst;
                        ed_link_widths[src][neighbor_count] = 3; //3 wires per link column-wise
                        ed_link_weights[src][neighbor_count] = ED_LOCAL_LINK_WEIGHT; //local link
                        
                        neighbor_count += 1;
                    }
            }

            //cout << endl;
        }
    }
        
    
}



void Edison::_BuildGraphForGlobal(){
    /*
    Need to read the config from a file. 
    The file location should be fixed, so hard-coding is okay.
    */

    ifstream is;
    int src_nodeid, dest_nodeid;
    char source[30], color[6], arrow[4], dest[30];
    int src_group = -1, dest_group = -1, chassis = -1, slot = -1;
    char ee[4];

    std::pair<int, int> global_link;
  
    std::unordered_map < std::pair<int, int>, int, pair_hash > global_link_frequency;


    string filename = ed_edison_connection_filename;


    is.open(filename.c_str());
    if (!is.is_open()) {
        cout << "cannot open global links file " << filename << endl;
        exit(-1);
    } else{
        cout << filename << " file opened successfully." << endl;
    }

    //make a vector of links in the format of (src,dst) pairs
    
    is >> source >> color  >> arrow >> dest;
    while (!is.eof()) {
        if (strcmp(dest, "unused") != 0){

            sscanf(source, "c%*d-%*dc%*ds%*da%4s(%d:%d:%d)", ee, &src_group, &chassis, &slot);

            if (src_group >= 13) src_group--; //since group 12 does not exist
            
            src_nodeid = src_group * (ed_dims[0] * ed_dims[1]) + chassis * ed_dims[0] + slot;

            sscanf(dest, "c%*d-%*dc%*ds%*da%4s(%d:%d:%d)", ee, &dest_group, &chassis, &slot);
            
            if (dest_group >= 13) dest_group--; //group 12 does not exist
            
            dest_nodeid = dest_group * (ed_dims[0] * ed_dims[1]) + chassis * ed_dims[0] + slot;

            //cout << src_nodeid << " , " << dest_nodeid << endl;

            global_link = std::make_pair(src_nodeid, dest_nodeid);

            //There are multiple links between same (src,dst) pair. 
            //So we need to count their frequency.
            if (global_link_frequency.find(global_link) != global_link_frequency.end() ){
                global_link_frequency[global_link] += 1;
            }else{
                global_link_frequency[global_link] = 1;    
            }

            out_port_tracker[src_nodeid] += 1;
            in_port_tracker[dest_nodeid] += 1;
            
        }

        is >> source >> color  >> arrow >> dest;
    }
    is.close();

    //all the global links and their frequencies are saved in the unordered_map.
    //Just go through it and add the links in the graph.

    for(auto it = global_link_frequency.begin(); it != global_link_frequency.end(); it++){
        //cout << it->first.first << "," << it->first.second << "," << it->second << endl;

        ed_graph[it->first.first].push_back(it->first.second);
        ed_link_widths[it->first.first].push_back(it->second); //should be 2, without any exception
        ed_link_weights[it->first.first].push_back(ED_GLOBAL_LINK_WEIGHT); //global link weight 3

        src_group = it->first.first / _a;
        dest_group = it->first.second / _a;
        
        //cout << it->first.first << " , " << src_group << " , " << it->first.second << " , " << dest_group << endl;

        ed_inter_group_links[src_group][dest_group].push_back(std::make_pair(it->first.first, it->first.second));
       
    }

}

void Edison :: _CreatePortMap(){
    /*
    Assumpiton: the graph is already generated. 
    
    Each PE has a weight of 1.    
    
    The weight of each local and global link is saved in ed_link_widths.
    (row-wise local links: 1, column-wise local links: 3, global links: 2).

    Populate the ed_port_map accordingly.
    
    */
        
    int port_count;
    
    std::size_t node;
    int dst;
    
    std::size_t ii;
    int width;
    
    for (node = 0; node < ed_graph.size(); node++){

        port_count = _p;
        
        for(ii = 0; ii < ed_graph[node].size(); ii++){
            dst = ed_graph[node][ii];
            width = ed_link_widths[node][ii];

            ed_port_map[ std::make_pair(node, dst) ] = std::make_pair(port_count, port_count+width-1);

            port_count += width;
        }

    }
            
    //test print ed_port_map
    // cout << "\nTest printing the port maps:" << endl;
    
    // for(node = 0; node < ed_graph.size(); node++){
    //     cout << node << " : ";
    //     for (ii = 0 ; ii < ed_graph[node].size(); ii++){
    //         dst = ed_graph[node][ii];
    //         cout << "(" << dst << "," << ed_port_map[std::make_pair(node, dst)].first << "," << ed_port_map[std::make_pair(node, dst)].second << ")" << " , " ;
    //     }
    //     cout << endl;
    // }
}



void Edison::_printGraph(){
    /*
    Just a helper function to help debug.
    */

    size_t ii, jj;
    for(ii = 0; ii < ed_graph.size(); ii++){
        cout << ii << " : ";
        for(jj = 0; jj < ed_graph[ii].size(); jj++){
            cout << ed_graph[ii][jj] << " ";
            cout << ed_link_weights[ii][jj] << " ";
            cout << ed_link_widths[ii][jj] << " ";

        }
        cout << endl;

    }

    // for(ii = 0; ii < out_port_tracker.size(); ii++){
    //     cout << ii << " : " << out_port_tracker[ii] << " , " << in_port_tracker[ii] << endl;
    //     if (out_port_tracker[ii] != in_port_tracker[ii]){
    //         cout << "FOUND ******************************************" << endl;
    //     }
    // }


    /*size_t kk;
    cout << "global links:" << endl;
    for(ii = 0; ii < ed_inter_group_links.size(); ii++){
        for(jj = 0; jj < ed_inter_group_links[ii].size(); jj++){
            cout << "(" << ii  << "," << jj << "): ";
            for(kk = 0; kk < ed_inter_group_links[ii][jj].size(); kk++){
                cout << "(" << ed_inter_group_links[ii][jj][kk].first  << "," << ed_inter_group_links[ii][jj][kk].second << "): ";
             
            }
            cout << " ==> " << ed_inter_group_links[ii][jj].size();
            cout << endl;
        }
    }*/
}

void Edison::_BuildNet( const Configuration &config ){
    cout << "inside _BuildNet() ..." << endl;
    
    int node, dst, count, channel_id, width, channel_count, kk, link_type;
    ostringstream router_name; 
    std::size_t idx;
    
    //create the routers
    for(node = 0; node < _N; node++){
        router_name.str("");
        router_name << "router";
        router_name << "_" << node;
        //cout << router_name.str() << endl;        
        _routers[node] = Router::NewRouter( config, this, router_name.str(), 
                    node, _radix + in_port_tracker[node], _radix + out_port_tracker[node] );     
                    //_radix for the # of input and output ports, respectively
        _timed_modules.push_back(_routers[node]);
    }
    
    std::cout << "Routers created ..." << std::endl;
    
    //create the PEs
    //add input and output channels to processing nodes
    for(node = 0; node < _N; node++){
        //cout << "router: " << node << endl;
        for (count = 0; count < _p; count++ ) {
            channel_id = _p * node +  count;
                                    //if _p == 3:   
                                        //for router 0, channel_id is 0,1,2
                                        //for router 1, channel_id is 3,4,5 
                                        // and so on.
            //cout << "channel_id:" << channel_id << endl;
            _routers[node]->AddInputChannel( _inject[channel_id], _inject_cred[channel_id] );
                            // _inject and _inject_cred arrays are initialized in _alloc()
            _routers[node]->AddOutputChannel( _eject[channel_id], _eject_cred[channel_id] );
               
        }
    }
    
    std::cout << "PEs connected ..." << std::endl;
    
    
    //create the channels
    
    /*
    // ed_graph already lists all the unidirectional links.
    // ed_link_width contains the width of each link.
    // Based on these two, connect the actual links.
    
    // Go through each unidirectional link, check its width. 
    // Add that many outgoing channels in the src, and incoming channels in the dst. Done.
    */

    cout << "starting to connect the router channels ..." << endl;
    channel_count = 0;
    for(node = 0; node < _N; node++){
        //cout << "router: " << node << endl;
        for(idx = 0; idx < ed_graph[node].size(); idx++){
            //cout << "idx: " << idx << endl;

            dst = ed_graph[node][idx];
            width = ed_link_widths[node][idx];
            link_type = ed_link_weights[node][idx];
            
            //cout << "(dst, width, type): " << dst << " , " << width << " , " << link_type << " , " << endl;

            for (kk = 0; kk < width; kk++){
                //connect the channels
                _routers[node] -> AddOutputChannel(_chan[channel_count], _chan_cred[channel_count]);
                
                if (link_type == ED_LOCAL_LINK_WEIGHT){
                    _chan[channel_count]->SetLatency(_local_latency);
                    _chan_cred[channel_count]->SetLatency(_local_latency);
                }
                else if (link_type == ED_GLOBAL_LINK_WEIGHT){
                    _chan[channel_count]->SetLatency(_global_latency);
                    _chan_cred[channel_count]->SetLatency(_global_latency);
                }
                
                _routers[dst] -> AddInputChannel(_chan[channel_count], _chan_cred[channel_count]);
                channel_count += 1;
            }

            //cout << "channel " << channel_count << " , node: " << node << " , dst: " << dst << " ,width: " << width << endl;
        
             
        }
    }
    

    //then go through the list and connect the links.
    cout << "done with _BuildNet() ..." << endl;
}


void Edison :: RegisterRoutingFunctions(){
    cout << "inside _RegisterRoutingFunctions() ..." << endl;

    gRoutingFunctionMap["min_edison"] = &min_edison;
    
    gRoutingFunctionMap["vlb_edison"] = &vlb_edison;
    
    gRoutingFunctionMap["UGAL_L_edison"] = &UGAL_L_edison;
    
    gRoutingFunctionMap["UGAL_L_mt_edison"] = &UGAL_L_mt_edison;
    gRoutingFunctionMap["UGAL_mt_src_edison"] = &UGAL_L_mt_edison;
    
    gRoutingFunctionMap["UGAL_L_restricted_edison"] = &UGAL_L_mt_edison;

    gRoutingFunctionMap["PAR_edison"] = &PAR_edison;
    gRoutingFunctionMap["PAR_mt_src_edison"] = &PAR_mt_src_edison;
    
    

            //same routing funtion as UGAL_L_mt_edison, 
            //just different branches for different functionality
    
    cout << "done with _RegisterRoutingFunctions() ..." << endl;
}


void min_edison( const Router *r, const Flit *f, int in_channel, OutputSet *outputs, bool inject){
    /*
    The routing function needs to find:
        - an output port
        - an output vc
        
    For the output port:
        - First check if it is an injection port. If yes, do accordingly.
        
        - Then check if it is the destination router. If yes, then find the port to the destination PE.
         
        - Then check if it is the source router. If yes:
            - Find a suitable path to the destination router.
            - Save the whole path in the flit 
        
        - Based on the hop count, find the port id to the next router.
        - Increase hop-count.
        
    For the output vc:
        - give a new vc on each hop.
            - For non-minimal routing, max hop-count is 10. So we need 10 vcs.
        - For flit injection, randomly assign a vc 
            
    So two extra functions needed:
        - A function to generate the complete path for the flit, given a pair of src and dst.
        - A function for finding the port to the next hop given the flit's hop count, saved path, current and next routers.
    */
    
    //First check if it is an injection port. If yes, do accordingly.
    
    bool flag = false;

    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }

    if (flag){
        cout << "inside min_edison() for flit: " << f-> id << endl;
    }
    

    if(inject) {
        outputs->Clear( ); //doesn't matter really. If the flit is at injection,
                            //means it was just generated, so the outputset is empty anyway.

        int inject_vc= RandomInt(gNumVCs-1);
        outputs->AddRange(-1, inject_vc, inject_vc);
          
        if (flag){
            cout << "injecting flit: " << f->id << " with vc: " << inject_vc << endl;
        }
        

        return;
    }
    
    //gather necessary info from the flit
    outputs -> Clear();
    
    int current_router = r->GetID();
    
    int out_port = -33;    //instead of setting all the default error values to -1, we used different values in different places. Works kinda like different error codes, helps in debugging.
    
    int out_vc = 0;
    
    int dst_PE = f->dest;
    int dst_router = dst_PE / ed_p;
    if (flag){
        cout << "current router, dest router: " << current_router << "," <<dst_router << endl;
    }
        
    
    //Then check if it is the destination router. 
    //If yes, then find the port to the destination PE.
    if(current_router == dst_router){
        out_vc = RandomInt(gNumVCs-1);
            //This could be a static VC as well. But a PE is basically a exit drain for packets,
            //so differentiating between VCs doesn't seem neceesary. Rather just draining them as fast as possible.
        
        //find the port that goes to the destination processing node
        out_port = dst_PE % ed_p;

        if (flag){
            cout << "destination router. ejecting through port " << out_port << " through vc " << out_vc << endl;
        }

        collect_path_stat(f);
    }
    else if (f->hop_count == 0){   //source router
    
        if (flag){
            cout << "inside source router ..." << endl;
        }

        //sanity check. CShoudn't happen if everything is alright.
        if (in_channel >= ed_p){
            cout << "EXCEPTION! Source router but port is not smaller than p" << endl;
            cout << "flitID: " << f->id << " src: " << f->src << " dest: " << f->dest  << " rID: " << r->GetID() << endl;
    
            cout << "p: " << ed_p << " in_channel: " << in_channel << endl;
        }

        std::vector<int> pathVector;
        
        int temp = select_min_path_edison(current_router, dst_router, pathVector, f);
        
        if (flag){
            cout << "the path returned is: ";
            print_vector(pathVector);
        }


        //Save the whole path in the flit
        f->path = std::move(pathVector);
        
        //now get the port to the next hop node 
        out_port = find_port_to_node_edison(current_router, f->path[1], f);
        
        if(flag){
            cout << "port to next router is: " << out_port << endl;
        }
        
        //assign vc. first hop, so vc 0.
        out_vc = 0;
        
        //increase hop_count
        f->hop_count += 1;

        f->min_or_vlb = 0;  //min routing


        //for q_len analysis
        if (ed_log_Qlen_data == 1){
                string q_len_data = "";                
                int min_q_len;

                min_q_len = find_port_queue_len_to_node_edison(r, f->path[0], f->path[1]);
                //cout << "Router:, " << r->GetID() << " , " << "min_q:, " << min_shortest_path_weight << " , " << "non-min_q:, " << min_VLB_path_weight << " , chosen:, " << chosen << endl;
            //return the id of the chosen one
                q_len_data += std::to_string(r->GetID());
                q_len_data += ",";
                q_len_data += std::to_string(min_q_len);
                q_len_data += "\n";
                
                //cout << q_len_data;
                ed_q_len_file << q_len_data;
            
        }

    
    }else{  //neither src nor dst router. Packet in flight.
        //get current hop count 
        
        //get port to the next hop node
        out_port = find_port_to_node_edison(current_router, f->path[ f->hop_count + 1], f);
        
        //assign vc 
        out_vc = f->hop_count;
        
        if (flag){
            cout << "port to next hop: " << out_port << " through vc: " << out_vc << endl; 
        }
        
        //increase hop_count
        f->hop_count += 1;
        
    }

    if (out_port == -1){
        cout << "ERROR! port -1 returned !!!!!!" << endl;
        cout << "flit: " << f-> id << endl;
        cout << "current router: " << current_router << endl;
        cout << "path: ";
        print_vector(f->path);
        cout << "hop: " << f->hop_count << endl;
    }


    //finally, build the output set
    outputs->AddRange( out_port, out_vc, out_vc );
    
}


int select_min_path_edison(int src_router, int dst_router, std::vector<int> & pathVector, const Flit *f){
    //the flit is only passed for debugging

    // '''
    // Here, min path means Dragonfly paths.
    
    // So, we have a source group and a dest group.
    
    // First we select a global link between the groups. Then the full paths
    // are generatd as src + node + src_gateway + dest_gateway + node + dst.
    
    // So, this is a five-hop path.
    
    // Sometimes, the actual shortest path can be four, three, two or even 
    // one hop, 
    // if the src_gateway is present on the same row or same column as the src_node.
    // OR, the dst_gateway is present on the same row or same column as the dst_node.
    // OR, if the src and src_gateway are same, and/or, dst and dst_gateway
    // are the same.


    
    // When either of the options is true, we have a four-hop path:
    //     a) src + src_gateway + dst_gateway + node + dst
    //     b) src + node + src_gateway + dst_gateway + dst
    
    // When both options are true, we have a three hop path:
    //     a) src + src_gateway + dst_gateway + dst
    
    // '''
    

    // Get the src and dest group.
    //         - Get a list of links in current group that goes directly to the destination group. [ed_inter_group_links maintains this list. Just look it up.]
    //         - Pick one randomly.
    //         - Then have a path from src_router -> gateway_router -> gateway_router_at_dst_group -> dst_router.  
            
    // Copy the selected path in pathVector.

    // Returns a unique int to signify which path was taken. 

    bool flag = false;
    int return_val = -1;

    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }

    if (flag){
        cout << "inside select_min_path_edison() for flit " << f->id << endl; 
    }

    int src_group, dst_group, src_gateway, dst_gateway;
    int link_to_select;
    std::pair<int,int> selected_global_link;
    
    src_group = (int)src_router / ed_a;
    dst_group = (int)dst_router / ed_a;
    
    if (flag){
        cout << "src,src_group, dst, dst_group: " << src_router << "," << src_group << "," << dst_router << "," << dst_group << endl;
    }

    pathVector.clear(); //in case there was something already in the vector

    //case 0: src and dst are in the same group.
    if (src_group == dst_group){
        select_min_path_inside_group_edison(src_router, dst_router, pathVector,f);
        if (flag){
            cout << "Same group. Path returned: ";
            print_vector(pathVector);
        }
        return_val = 999; 
            //just an arbitrary large value to signify that same_group path was returned.

    }
    
    //case 1: src and dst are in different groups
    else{
        assert(ed_inter_group_links[src_group][dst_group].size() > 0);
        
        link_to_select = RandomInt(ed_inter_group_links[src_group][dst_group].size() - 1); 
                    //RandomInt gets a number in the range[0,max], inclusive.
        
        return_val = link_to_select; 
                    //Just a way to inform the caller function on which path was selected.
                    //This way the caller can call this multiple times if multiple unique paths are needed.

        selected_global_link = ed_inter_group_links[src_group][dst_group][link_to_select];
        
        src_gateway = selected_global_link.first;
        dst_gateway = selected_global_link.second;

        //cout << "selected global link: " << src_gateway << "," << dst_gateway << endl;

        std::vector<int> first_half_pathVector;
        std::vector<int> second_half_pathVector;
        
        select_min_path_inside_group_edison(src_router, src_gateway, first_half_pathVector,f);
        select_min_path_inside_group_edison(dst_gateway, dst_router, second_half_pathVector,f);
        if (flag){      
            cout << "first path: ";
            print_vector(first_half_pathVector);
            cout << "second path: ";
            print_vector(second_half_pathVector);
        }

        //merge the two vectors
        pathVector.reserve(first_half_pathVector.size() + second_half_pathVector.size() );
        pathVector.insert(pathVector.end(), first_half_pathVector.begin(), first_half_pathVector.end());
        pathVector.insert(pathVector.end(), second_half_pathVector.begin(), second_half_pathVector.end());
    }


    if (flag){
        cout << "leaving select_min_path_edison() for flit " << f->id << endl; 
    }


    return return_val;



}

int select_min_path_inside_group_edison(int src_router, int dst_router, std::vector<int> & pathVector, const Flit *f){
    //the flit is passed only for debugging.

    // If the src and dst are in the same row, or in same column, there is only one shourtest path.
    // Else, there are two, and one will be randomly picked.
    
    //cout << "inside select_min_path_inside_group_edison()" << endl;
    bool flag = false;

    // if (f->id == FLIT_TO_TRACK){
    //     flag = true;
    // }

    if (flag){
        cout << "inside select_min_path_inside_group_edison() for flit " << f->id << endl; 
    }

    //this can happen when the src and src_gateway are the same.
    if (src_router == dst_router){
        pathVector = {src_router};
        return 1;
    }

    int src_group, dst_group, src_row, dst_row, src_col, dst_col; 
    int imdt[2];
    int random_idx;

    pathVector.clear(); //in case there was something already in the vector

    src_group = src_router / ed_a;
    dst_group = dst_router / ed_a;

    if (src_group != dst_group){
        cout << "Error! src_group != dst_group for src_router " << src_router << " dst_router " << dst_router << " for flit " << f->id << endl;
        exit(-1);
    }

    src_row = (src_router - src_group * ed_a) / ed_dims[0];
    src_col = (src_router - src_group * ed_a) % ed_dims[0];

    if (flag){
        cout << "src, group, row , col: " << src_router << "," << src_group << "," << src_row << "," << src_col << endl;
    }


    dst_row = (dst_router - dst_group * ed_a) / ed_dims[0];
    dst_col = (dst_router - dst_group * ed_a) % ed_dims[0];

    if (flag){
        cout << "dst, group, row , col: " << dst_router << "," << dst_group << "," << dst_row << "," << dst_col << endl;
    }

    if ( (src_row == dst_row) || (src_col == dst_col)){
        pathVector = {src_router, dst_router};
    }
    else{
        imdt[0] = (src_group * ed_a) + dst_row * ed_dims[0] + src_col;
        imdt[1] = (src_group * ed_a) + src_row * ed_dims[0] + dst_col;
        //cout << "imdt1: " << imdt[0] << endl;
        //cout << "imdt2: " << imdt[1] << endl;

        //randomly choose 1
        random_idx = RandomInt(1); //RandomInt gets a number in the range[0,max], inclusive.
        pathVector = {src_router, imdt[random_idx], dst_router};
    }


    //cout << "done with select_min_path_inside_group_edison()" << endl;
    if (flag){
        cout << "leaving select_min_path_inside_group_edison() for flit " << f->id << endl; 
    }  

    return 1;   //indicates success, no broader meaning.

}


int select_two_min_paths_inside_group_edison(int src_router, int dst_router, std::vector< int > & pathVector1, std::vector< int > & pathVector2, const Flit *f){
    // Ugly design. Natural instinct is to make it suitable for any number 
    // of paths.
    // However, inside Edison groups, there are only maximum two shortest 
    // paths possible between a pair.
    // So Manually hard-cding the property. Will never need more than two
    // intra-group min paths.


    //the flit is passed only for debugging.

    // If the src and dst are in the same row, or in same column, there is 
    // only one shortest path. In that case, put the same path in pathVector1
    // and in pathVector2.
    // Else, there are two. Store them and return.
    
    // Returns 1 if there is a single path, else returns 2.

    //cout << "inside select_min_path_inside_group_edison()" << endl;
    bool flag = false;
    int return_val = -1;

    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }

    if (flag){
        cout << "inside select_two_min_paths_inside_group_edison() for flit " << f->id << endl; 
    }


    pathVector1.clear(); 
    pathVector2.clear(); 

    //this can happen when the src and src_gateway are the same.
    if (src_router == dst_router){
        pathVector1 = {src_router};
        pathVector2 = {src_router};
        return_val = 1;
    }
    else{

        int src_group, dst_group, src_row, dst_row, src_col, dst_col; 
        int imdt[2];

        src_group = src_router / ed_a;
        dst_group = dst_router / ed_a;

        assert(src_group == dst_group);

        src_row = (src_router - src_group * ed_a) / ed_dims[0];
        src_col = (src_router - src_group * ed_a) % ed_dims[0];

        if (flag){
            cout << "src, group, row , col: " << src_router << "," << src_group << "," << src_row << "," << src_col << endl;
        }


        dst_row = (dst_router - dst_group * ed_a) / ed_dims[0];
        dst_col = (dst_router - dst_group * ed_a) % ed_dims[0];

        if (flag){
            cout << "dst, group, row , col: " << dst_router << "," << dst_group << "," << dst_row << "," << dst_col << endl;
        }

        if ( (src_row == dst_row) || (src_col == dst_col)){
            pathVector1 = {src_router, dst_router};
            pathVector2 = {src_router, dst_router};
            return_val = 1;
        }
        else{
            imdt[0] = (src_group * ed_a) + dst_row * ed_dims[0] + src_col;
            imdt[1] = (src_group * ed_a) + src_row * ed_dims[0] + dst_col;
            //cout << "imdt1: " << imdt[0] << endl;
            //cout << "imdt2: " << imdt[1] << endl;

            pathVector1 = {src_router, imdt[0], dst_router};
            pathVector2 = {src_router, imdt[1], dst_router};
            return_val = 2;
        }

    }

    //cout << "done with select_min_path_inside_group_edison()" << endl;
    if (flag){
        cout << "leaving select_min_path_inside_group_edison() for flit " << f->id << endl; 
    }  

    return return_val;  

}


void print_vector(std::vector<int> & vec){
    for(std::size_t idx = 0; idx < vec.size(); idx++){
        cout << vec[idx] << "  ";
    }
    cout << endl;
}



int find_port_to_node_edison(int current_router, int next_router, const Flit *f){
    /*
    As the name suggests. Get the port and return.
    
    Return -1 if no port found.
    */
    
    bool flag = false;
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }

    if (flag){
        cout << "inside find_port_to_node_edison() for flit " << f->id << endl;cout << "current_router, next_router :" << current_router << " , " << next_router << endl; 
    }

    //we already populated ed_port_map for this purpose
    
    if (ed_port_map.find(std::make_pair(current_router, next_router)) != ed_port_map.end()){
        auto port_range = ed_port_map[std::make_pair(current_router, next_router)];
    
        int selected = RandomInt(port_range.second - port_range.first); 
                        //range returned by ed_port_map is a closed range. For example, for port 5 it'll return (5,5) pair
                        //RandomInt also returns an int in the range [0, x]. So subtracting 1 is not needed in this case.
        //cout << "port returning: " << port_range.first + selected << endl;  
        return (port_range.first + selected);
    }else{

        //if (flag){
        cout << "port returning: " << -1 << " for flit " << f->id << endl;
        //}
        return -1;
    }

}


void vlb_edison( const Router *r, const Flit *f, int in_channel, OutputSet *outputs, bool inject){
    /*
    Regular VLB routing function.
    For a Src and Dst, (randomly) select an Intermediate node.
    Then route (src, imdt) and (imdt, dst).
    
    Outsource the imdt-node selection to a eparate function so that it can be manipulated later, if needed.
    
    As usual,
    The routing function needs to find:
        - an output port
        - an output vc
        
    For the output port:
        - First check if it is an injection port. If yes, do accordingly.
        
        - Then check if it is the destination router. If yes, then find the port
         to the destination PE.
         
        - Then check if it is the source router. If yes:
            - if src and dst are in the same group, route them normally.
            
            - else, select an intermediate node. 
            
            - then generate paths in the form of (min, imdt, dst)
            
            - Save the whole path in the flit 
            
        - Based on the hop count, find the port id to the next router.
        - Increase hop-count.
        
    For the output vc:
        - give a new vc on each hop.
            - For non-minimal routing, max hop-count is 10. So we need 10 vcs.
        - For flit injection, just randomly assign a vc. Probably not harmful.
    
    So two functions needed:
        - A function to generate the complete path for the flit, given a pair of src and dst.
        - A function for finding the port to the next hop given the flit's hop count, 
        saved path, current and next routers.
    */
    
    //First check if it is an injection port. If yes, do accordingly.
    
    bool flag = false;

    if(inject) {
        outputs->Clear( ); //doesn't matter really. If the flit is at injection,
                            //means it was just generated, so the outputset is empty anyway.

        int inject_vc= RandomInt(gNumVCs-1);
        outputs->AddRange(-1, inject_vc, inject_vc);
          
        return;
    }
    
    
    //gather necessary info from the flit
    outputs -> Clear();
    
    int current_router = r->GetID();
    
    int out_port = -33;    //instead of setting all the default error values to -1, we used different values in different places. Works kinda like different error codes, helps in debugging.
    
    int out_vc = 0;
    
    int dst_PE = f->dest;
    int dst_router = dst_PE / ed_p;
    
    //Then check if it is the destination router. 
    //If yes, then find the port to the destination PE.
    if(current_router == dst_router){
        out_vc = RandomInt(gNumVCs-1);
            //This could be a static VC as well. But a PE is basically a exit drain for packets,
            //so differentiating between VCs doesn't seem neceesary. Rather just draining them as fast as possible.
        
        //find the port that goes to the destination processing node
        out_port = dst_PE % ed_p;

        collect_path_stat(f);
        
    }
    else if (f->hop_count == 0){   //source router
        //actual VLB routing needs to happen here.
        
        std::vector<int> pathVector;
        
        //cout << "calling select_vlb_path() for src, dst pair: " << current_router << "," << dst_router << endl;
        
        int temp = select_vlb_path_regular_edison(current_router, dst_router, pathVector, f);
        
        if (flag){
        cout << "value from select_vlb_path() returned: " << temp << endl;
        cout << "the path returned is: ";
               for(std::size_t ii = 0; ii < pathVector.size(); ii++){
                   cout << pathVector[ii] << " ";
               }
        cout << endl;       
        }

        //Save the whole path in the flit
        f->path = std::move(pathVector);
        
        //now get the port to the next hop node 
        out_port = find_port_to_node_edison(current_router, f->path[1], f);
        
        //assign vc. first hop, so vc 0.
        out_vc = 0;
        
        //increase hop_count
        f->hop_count += 1;

        f->min_or_vlb = 1;  // 0 -> min, 1 -> vlb 
    
    }else{  //neither src nor dst router. Packet in flight.
        //get current hop count 
        
        //get port to the next hop node
        out_port = find_port_to_node_edison(current_router, f->path[ f->hop_count + 1], f);
        
        //assign vc 
        out_vc = f->hop_count;
        
        //increase hop_count
        f->hop_count += 1;
        
    }

    //finally, build the output set
    outputs->AddRange( out_port, out_vc, out_vc );
    
}


int select_vlb_path_regular_edison(int src_router, int dst_router, std::vector<int> & pathVector, const Flit *f){
    /*
        Plain vanilla VLB routing.
        
        Randomly select an intermediate node not within the group.
        
        Get paths from src to imdt, and then from imdt to dst.
        
        Join the paths. Win. 
        
        Returns:    The selected path in pathVector.
                    
                    The selected intermediate path used to generate the path. 
                    This helps to avoid duplicate paths in UGAL routing where multiple VLB paths are considered.
        
        
        The flit is passed for debugging.
        */    
        
    //cout << "inside select_vlb_path_regular for " << "src: " << src_router << " dest: " << dst_router << "  " << endl;
    
    int src_group, dst_group;
    std::pair<int,int> selected_global_link;
    
    std::vector<int> gateway_router_list;
    int imdt_router;

    src_group = src_router / ed_a;
    dst_group = dst_router / ed_a;
        
    //cout << "src_group: " << src_group << " dst_group: " << dst_group << endl; 
    
    pathVector.clear();

    //case 0: src and dst are in the same group.
    //TODO: ship this out to it's own function.
    if (src_group == dst_group){
        //pathVector = {src_router, dst_router};
        
        int mode = 4;
        imdt_router = select_vlb_path_inside_group_edison(src_router, dst_router, mode, pathVector, f);

        
        //        cout << "same group. path: ";
        //        for(ii = 0; ii < pathVector.size(); ii++){
        //            cout << pathVector[ii] << " ";
        //        }
        //        cout << endl;
    }
    
    //case 1: src and dst are in differnet groups 
    else{
        std::vector<int> first_half_pathVector;
        std::vector<int> second_half_pathVector;
        
        //select intermediate node 
        //shipping it out to a separate function for easy modifications.
        imdt_router = vlb_intermediate_node_vanilla_edison(src_router, dst_router, f);
        
        select_min_path_edison(src_router, imdt_router, first_half_pathVector, f);
        select_min_path_edison(imdt_router, dst_router, second_half_pathVector, f);
        
        //join the two vectors
        pathVector.reserve(first_half_pathVector.size() + second_half_pathVector.size() - 1);
        pathVector.insert(pathVector.end(), first_half_pathVector.begin(), first_half_pathVector.end());
        pathVector.insert(pathVector.end(), second_half_pathVector.begin() + 1, second_half_pathVector.end());
        
    }
    
    //test printing
    //    cout << "imdt: " << imdt_router << "  path: ";
    //    for(ii = 0; ii < pathVector.size(); ii++){
    //        cout << pathVector[ii] << " ";
    //    }
    //    cout << endl;
    //    
    return imdt_router;
}


int select_vlb_path_inside_group_edison(int src_router, int dst_router, int mode,  std::vector<int> & pathVector, const Flit *f){
    /*  Both src and dst are within the same group. 
        So generate a vlb path inside the group.
        Basically, randomly pick an intermediate node in the group.
        Then, {src, imdt, dst}

        mode is an integer that dictates the expected max length of the vlb paths.
        Currently supported mode is 4.
        
        Returns:
            The path itself in pathVector.
            The intermediate node selected to make the path. The info may be useful in UGAL to avoid duplicate paths.
    */
    //safety check
    int src_group = src_router / ed_a;
    
    if (src_group != (dst_router / ed_a)){
        cout << "Error in calling select_vlb_path_inside_group()" << endl;
        return -1; //error
    }
    
    int imdt_router = -1;

    if (mode == 4){
        imdt_router = find_max_four_hop_vlb_paths_in_group(src_router, dst_router, pathVector, f);
    }else{
        cout << "Unsupported mode in select_vlb_path_inside_group_edison() : " << mode << endl;
        exit(-1);
    }
        
    return imdt_router; //success
    
}

int find_max_four_hop_vlb_paths_in_group(int src_router, int dst_router, std::vector<int> & pathVector, const Flit *f){
    int src_group = src_router / ed_a;
    //int dst_group = dst_router / ed_a;

    int imdt_router = src_group * ed_a + RandomInt(ed_a - 1); //RandomInt gets a number in the range[0,max], inclusive

    while((src_router == imdt_router) || (dst_router == imdt_router)){
        imdt_router = src_group * ed_a + RandomInt(ed_a - 1);
    }

    std::vector<int> first_half_pathVector;
    std::vector<int> second_half_pathVector;
        
    select_min_path_inside_group_edison(src_router, imdt_router, first_half_pathVector, f);
    select_min_path_inside_group_edison(imdt_router, dst_router, second_half_pathVector, f);
    
    //join the two vectors
    pathVector.clear();
    pathVector.reserve(first_half_pathVector.size() + second_half_pathVector.size() - 1);
    pathVector.insert(pathVector.end(), first_half_pathVector.begin(), first_half_pathVector.end());
    pathVector.insert(pathVector.end(), second_half_pathVector.begin() + 1, second_half_pathVector.end());
    
    return imdt_router;

}
        
int vlb_intermediate_node_vanilla_edison( int src_router, int dst_router, const Flit *f){
    /*
    Just select a node not part of either source group or destination group.
        
    The flit is passed for debugging.
    */
    
    bool flag = false;
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }

    int imdt_node, imdt_group;
    
    int src_group = src_router / ed_a;
    int dst_group = dst_router / ed_a;
        
    if (flag){
        cout << "inside vlb_intermediate_node_vanilla_edison() " << endl;
        cout << "src: " << src_router << " , src_group: " << src_group << endl;
        cout << "dst: " << dst_router << " , dst_group: " << dst_group << endl;
    }

    imdt_node = RandomInt(ed_N - 1); //RandomInt gets a number in the range[0,max], inclusive.
    imdt_group = imdt_node / ed_a;

    if(flag){
        cout << "imdt_node: " << imdt_node << " , imdt_group: " << imdt_group << endl;
    }
        
    while ((imdt_group == src_group) || (imdt_group == dst_group)){
        imdt_node = RandomInt(ed_N - 1); //RandomInt gets a number in the range[0,max], inclusive. 
        imdt_group = imdt_node / ed_a;

        if(flag){
            cout << "imdt_node: " << imdt_node << " , imdt_group: " << imdt_group << endl;
        }
    }
     
    if (flag){
        cout << "returning imdt_node: " << imdt_node << endl;
    }    
    return imdt_node;
}

void UGAL_L_edison( const Router *r, const Flit *f, int in_channel, OutputSet *outputs, bool inject){
    /*
    Vanilla UGAL_l routing function.
        
    ******************************
    source routing. 
    If it is the source router: 
                 Make the routing decision (MIN or VLB).     
                 Save the complete path in the flit. 
                 Forward the filt to the next hop.
    If it is not the source router, then:
                 Just look up the saved path in the flit
                 And forward to the next hop.
    That's it!
    ******************************
    
    Things to decide: 
        No of min paths and no of vlb paths to consider while path selection.
        Check default dragonfly implementation in booksim + dragonfly literature to get the numbers.
            Update: default Booksim implementaiton compared one min and one non-min paths.
    
    As usual,
    The routing function needs to find:
        - an output port
        - an output vc
        
    For the output port:
        - First check if it is an injection port. If yes, do accordingly.
        
        - Then check if it is the destination router. If yes, then find the port
         to the destination PE.
         
        - Then check if it is the source router. If yes:
            - Make the routing decision. generate the full path accordingly.
            
            - Save the whole path in the flit 
            
        - Based on the hop count, find the port id to the next router.
        - Increase hop-count.
        
    For the output vc:
        - give a new vc on each hop.
            - For non-minimal routing, max hop-count is 10. So we need 10 Vcs.
        - For flit injection, just randomly assign one.
    
    So two functions needed:
        - A function to generate the complete path for the flit, given a pair of src and dst.
        - A function for finding the port to the next hop given the flit's hop count, 
        saved path, current and next routers.
    */
    
    //First check if it is an injection port. If yes, do accordingly.
    
    bool flag = false;
    
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }
    
    if (flag){
        cout << "inside UGAL_L_edison() for flit: " << f-> id << endl;
        //cout << "routing mode: " << g_routing_mode << endl;
    }
    
    
    if(inject) {
        outputs->Clear( ); //doesn't matter really. If the flit is at injection,
                            //means it was just generated, so the outputset is empty anyway.

        int inject_vc= RandomInt(gNumVCs-1);
        outputs->AddRange(-1, inject_vc, inject_vc);
        
        if (flag){
            cout << "injecting flit: " << f->id << " with vc: " << inject_vc << endl;
        }
          
        return;
    }
    
    
    //gather necessary info from the flit
    outputs -> Clear();
    
    int current_router = r->GetID();
    if (flag){
        cout << "current router:" << current_router << endl;
    }
    
    int out_port = -33;    //instead of setting all the default error values to -1, we used different values in different places. Works kinda like different error codes, helps in debugging.
    
    int out_vc = 0;
    
    int dst_PE = f->dest;
    int dst_router = dst_PE / ed_p;
    
    //Then check if it is the destination router. 
    //If yes, then find the port to the destination PE.
    if(current_router == dst_router){
        out_vc = RandomInt(gNumVCs-1);
            
        //find the port that goes to the destination processing node
        out_port = dst_PE % ed_p;
        
        if (flag){
            cout << "destination router. ejecting through port " << out_port << " through vc " << out_vc << endl;
        }

        collect_path_stat(f);
    }
    
    else if (f->hop_count == 0){   //source router
        //if it is at the source router, then the input port must be a PE. Check if it is any different.
        if (flag){
            cout << "inside source router ..." << endl;
        }
        
        if (in_channel >= ed_p){
            cout << "EXCEPTION! Source router but port is not smaller than p" << endl;
            cout << "flitID: " << f->id << " src: " << f->src << " dest: " << f->dest  << " rID: " << r->GetID() << endl;
    
            cout << "p: " << ed_p << " in_channel: " << in_channel << endl;
            exit(-1);
        }
        
    
        //actual UGAL routing needs to happen here.
        
        std::vector<int> pathVector;
        
        if (flag){
            cout << "dest router: " << dst_router << endl;
        }
        
        int temp; 
        
        temp = select_UGAL_L_path_edison(r, f, current_router, dst_router, pathVector, ed_routing_mode);
        
        if (flag){
            cout << "select_ugal_l_path_edison() returned: " << temp << endl;
            cout << "the path returned is: ";
            print_vector(pathVector);
        }
        //Save the whole path in the flit
        f->path = std::move(pathVector);
        
        //now get the port to the next hop node 
        out_port = find_port_to_node_edison(current_router, f->path[1], f);
        
        if(flag){
            cout << "port to next router is: " << out_port << endl;
        }
        
        //assign vc. first hop, so vc 0.
        out_vc = 0;
        
        //increase hop_count
        f->hop_count += 1;
    
    }else{  //neither src nor dst router. Packet in flight.
        //get current hop count 
        
        //get port to the next hop node
        out_port = find_port_to_node_edison(current_router, f->path[ f->hop_count + 1], f);
        
        //assign vc 
        out_vc = f->hop_count;
        
        if (flag){
            cout << "port to next hop: " << out_port << " through vc: " << out_vc << endl; 
        }
        
        //increase hop_count
        f->hop_count += 1;
        
    }

    //finally, build the output set
    outputs->AddRange( out_port, out_vc, out_vc );
    
    if (flag){
        cout << "leaving UGAL_L_dragonflyfull() for flit: " << f-> id << endl;
    }
    
}


int select_UGAL_L_path_edison( const Router *r, const Flit *f, int src_router, int dst_router, std::vector<int> & pathVector, string routing){
    
    /*
        //Vanilla UGAL_L routing, but a twist can be added using "routing".
        
        //"routing" can have values "ugal_l" and "par". "ugal_l" is set as 
        //the defualt parameter in the header file.
        
        //flit is only passed for debugging
        
        //generate two min paths
        //generate two VLB paths
        //multiply the Q-length for each path with the paths hop count
        //choose the path with the smallest value
    */
        
    bool flag = false;
    
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }
        
    if (flag){
        cout << "inside select_UGAL_L_path_edison() for flit " << f->id << ", routing: " << routing << endl;
    }

    if ((routing != "ugal_l") && (routing != "par")){
        cout << "Error! Unsupported routing in select_UGAL_L_path_edison():  " << routing << " for flit << " << f->id << ". Exiting." << endl;
        exit(-1);
    }
    
    int no_of_MIN_paths_to_consider = 2;        
                            //At present it doesn't support any other value.
                            //But can change this in future
                            //Update: for Edison, we actually need 2 min-paths.
                                //So make sure the code is compatible.

    int no_of_VLB_paths_to_consider = 2;  //can change this in future 
    
    int ii, temp, chosen_pathID, chosen_pathID1, chosen_pathID2;
        
    std::vector< std::vector<int> > paths;
    std::vector<int> imdt_nodes;
    
    //int min_q_len;
    //int chosen_tier = 0; //only useful for multi-tiered routing

    int src_group, dst_group;

    src_group = src_router / ed_a;
    dst_group = dst_router / ed_a;

    paths.resize(no_of_MIN_paths_to_consider + no_of_VLB_paths_to_consider); 
                        //2 min path, the rest non-min paths
        
    //individual paths need not be initialized, as ath generator functions initialize the vectors themselves.    
    
    //We will only consider two min paths. In Edison, we probably will never change that.
    if (src_group == dst_group){
        temp = select_two_min_paths_inside_group_edison(src_router, dst_router, paths[0], paths[1], f);
    }
    else{
        
        chosen_pathID1 = select_min_path_edison(src_router, dst_router, paths[0], f);
        chosen_pathID2 = select_min_path_edison(src_router, dst_router, paths[1], f);
        while(chosen_pathID2 == chosen_pathID1){
            chosen_pathID2 = select_min_path_edison(src_router, dst_router, paths[1], f);
        }
    }


    if (flag){
        cout << "selected shortest paths are: " << endl;
        print_vector(paths[0]);
        print_vector(paths[1]);
    }

    //imdt_nodes.resize(no_of_VLB_paths_to_consider, -1); 
            //allocate inside the generator functions
    
    //Now, get the non-min paths   
    //first, pick intermediate nodes
    if (src_group == dst_group){
        //select in-group nodes only as imdt nodes
        generate_imdt_nodes_in_group_edison(f, src_router, dst_router,  no_of_VLB_paths_to_consider, imdt_nodes);
    }else{
        //select out-group imdt nodes
        generate_imdt_nodes_vanilla_edison(f, src_router, dst_router, no_of_VLB_paths_to_consider, imdt_nodes);
    }

    if (flag){
        cout << "generated imdt nodes: ";
        print_vector(imdt_nodes);
    }

    //now generate paths using the picked nodes
    for(size_t ii = 0; ii < imdt_nodes.size(); ii++){
        generate_vlb_path_from_given_imdt_node_vanilla_edison(f, src_router, dst_router, imdt_nodes[ii], paths[no_of_MIN_paths_to_consider + ii]);
    }

    if (flag){
        cout << "Generated vlb paths:" << endl;
        for(std::size_t jj = no_of_MIN_paths_to_consider; jj < paths.size(); jj++){
            print_vector(paths[jj]);
        }
        cout << endl;
    }
    
    //paths generated, now compare Q length and select one
    chosen_pathID = make_UGAL_L_path_choice_edison(r, f, no_of_MIN_paths_to_consider, no_of_VLB_paths_to_consider, paths);
    
    // if (chosen_pathID < no_of_MIN_paths_to_consider){
    //     cout << "min path generated for flit " << f->id << endl;
    // }else{
    //     cout << "NON-min path generated for flit " << f->id << endl;
    // }

    if (flag){
        cout << "chosen ugal_l path id: " << chosen_pathID << endl;
        cout << "chosen path: ";
        print_vector(paths[chosen_pathID]);
        cout << endl;
    }

    //decide here if a min path was selected in UGAL. If yes, mark it for PAR routing.
    if (routing == "par"){

        if ((chosen_pathID < no_of_MIN_paths_to_consider) && (src_group != dst_group)) {
            f->PAR_need_to_revaluate = true;  
                    
            //calculate and put the src_out_gatway
            //just do a brute-force. go through the nodes in the path and 
            //check when a different group starts
            for(ii = 0; ii < paths[chosen_pathID].size(); ii++){
                if (paths[chosen_pathID][ii] / ed_a != src_group){
                    break;
                }
            }
            f->src_out_gateway = paths[chosen_pathID][ii-1];

            if (flag){
                cout << "Phase: src. Path_type_chosen: min. src_out_gateway :" << f->src_out_gateway << endl;
            }

        }else{
            f->PAR_need_to_revaluate = false;
            f->src_out_gateway = -1;
            
            if (flag){
                cout << "Phase: src. Path_type_chosen: non-min. src_out_gateway :" << f->src_out_gateway << endl;
            }
        }
    }

    /*if (chosen_pathID < no_of_MIN_paths_to_consider){
        f->PAR_need_to_revaluate = true;
                //for PAR routing only. For others, it will have no effect.
    }*/

    //Now copy the chosen path in pathVector
    pathVector = std::move(paths[chosen_pathID]);  
    
    return 1;   //to indicate success, no other significance at this moment.
}


int make_UGAL_L_path_choice_edison(const Router *r, const Flit *f, int no_of_min_paths_to_consider, int no_of_VLB_paths_to_consider, std::vector< std::vector<int> > paths){
    /* 
    At this moment, we are just considering min path weight as 1
    and non-min path weight as 2.
    
    The flit is passed for debugging. 

    */
    
    bool flag = false;
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }
    
    int min_shortest_path_weight = 9999; //arbitrary large number
    int min_VLB_path_weight = 9999;
    
    int ii;
    int q_len;
    
    int selected_min_path_id = -1;  //dummy values. If they are not get changed then it means we have a problem.
    int selected_VLB_path_id = -1;    
    int chosen_path_id;

    int selected_min_path_hop_count = 0;
    int selected_VLB_path_hop_count = 0; 
    
    //go through the min paths and select the one with the lowest Q length
    if (flag){
        cout << "min paths q len:" << endl;
    }
    for(ii = 0; ii< no_of_min_paths_to_consider; ii++){
        q_len = find_port_queue_len_to_node_edison(r, paths[ii][0], paths[ii][1]);
        if (flag){
            cout << "path " << ii << " q_len: " << q_len << endl;
        }
        if (q_len < min_shortest_path_weight){
            selected_min_path_id = ii;
            min_shortest_path_weight = q_len;
            selected_min_path_hop_count = paths[ii].size()-1;
        }
    }
    
    if (flag){
        cout << "non-min paths q len:" << endl;
    }    
    //go through the non-min paths and select the one with the lowest Q length
    for(ii = no_of_min_paths_to_consider; ii < (no_of_min_paths_to_consider + no_of_VLB_paths_to_consider); ii++){
        q_len = find_port_queue_len_to_node_edison(r, paths[ii][0], paths[ii][1]);
        if (flag){
            cout << "path " << ii << " q_len: " << q_len << endl;
        }
        if (q_len < min_VLB_path_weight){
            selected_VLB_path_id = ii;
            min_VLB_path_weight = q_len;
            selected_VLB_path_hop_count = paths[ii].size()-1;
        }
    }
    
    int chosen = -1; // 0 for min, 1 for non-min     
    //make comparison
    if (min_shortest_path_weight <= min_VLB_path_weight * 2){
        chosen_path_id = selected_min_path_id;
        chosen  = 0;
        f->min_or_vlb = 0;  //0 min, 1 vlb

        // g_total_min_flit += 1;
        // g_cases_when_not_taken[chosen_tier] += 1;
        // g_lens_when_not_taken[selected_VLB_path_hop_count] += 1;
        if (flag){
            cout << "choosing MIN path. path id: " << chosen_path_id <<  " for flit " << f->id << endl;
        }        

    }
    else{
        chosen_path_id = selected_VLB_path_id;
        chosen = 1;
        f->min_or_vlb = 1;  //0 min, 1 vlb

        // g_total_non_min_flit += 1;
        // g_cases_when_taken[chosen_tier] += 1;
        // g_lens_when_taken[selected_VLB_path_hop_count] += 1;

        /*if ((selected_VLB_path_hop_count == 4) && (chosen_tier == 4)){
            cout << "flit " << f->id << " : ";
            for(int ii = 0; ii < paths[chosen_path_id].size(); ii++){
                cout << paths[chosen_path_id][ii] << "  ";
            }            
            cout << endl;
        }*/

        if (flag){
            cout << "choosing VLB path. path id: " << chosen_path_id << " for flit " << f->id << endl;
        }
    }
    


    //This block will be needed for threshold value analysis.
    if (ed_log_Qlen_data == 1){
        if (chosen == 1){
            //cout << "Router:, " << r->GetID() << " , " << "min_q:, " << min_shortest_path_weight << " , " << "non-min_q:, " << min_VLB_path_weight << " , chosen:, " << chosen << endl;
        //return the id of the chosen one
            string q_len_data = "";

            q_len_data += std::to_string(r->GetID());
            q_len_data += ",";
            q_len_data += std::to_string(min_shortest_path_weight);
            q_len_data += ",";
            q_len_data += std::to_string(min_VLB_path_weight);
            q_len_data += ",";
            q_len_data += std::to_string(chosen);
            q_len_data += "\n";
            
            ed_q_len_file << q_len_data;
        }
    }

    return chosen_path_id;
}



int find_port_queue_len_to_node_edison(const Router *r, int current_router, int next_router){
    //first find the port connected to the next_router
    
    /*
    Issue: 
    Though unlikely, but theoretically it is possible for some arrangements that
    some nodes will have multiple links between them. In that case how do we 
    take Q len or select a node?
    Well, the situation is not unlike SlimFly b/w allocation scheme.
    So we can just do the same and take the average of the Q_lens.
    While forwarding a node, we can select a Q randomly.     
    */
    
    auto port_range = ed_port_map[std::make_pair(current_router, next_router)];
    
    //then check its Q_length
    int port_id;
    
    int queue_length = 0;
    int count = 0;
        
    for (port_id = port_range.first; port_id <= port_range.second; port_id++) {
        //port_range is a closed range
        queue_length += max(0,r->GetUsedCredit(port_id));
                            //GetUsedCredit() may return 0 sometimes.
        count += 1;
    }
    
    queue_length = queue_length / count; 
        /* 
        //        This can result in some loss of fraction. 
        //        A more perfect way is probably to have it as a float. May be later. 
        */                             
    
    return queue_length;
}

int generate_imdt_nodes_vanilla_edison(const Flit *f, int src_router, int dst_router, int no_of_nodes_to_generate, std::vector<int> & imdt_nodes){
    //Vanilla version. Just randomly picks a number of nodes and returns.
    //Only constraint: picked nodes can not be from the same group as src or dst.
    //For selective picking for other routing modes, needs to be updated.

    bool flag = false;

    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }

    if(flag){
        cout << "inside generate_imdt_nodes_in_group_edison() for flit " << f->id << endl;
        cout << "src_router: " << src_router << " , dst_router: " << dst_router << endl;   
    }


    std::unordered_set <int> node_cache;
    int ii;
    int src_group = src_router / ed_a;
    int dst_group = dst_router / ed_a;
    int imdt_node = -1;
    int imdt_group = -1;

    for(ii = 0; ii < no_of_nodes_to_generate; ii++){
        while( (imdt_node == -1) ||(imdt_group == -1) || (imdt_group == src_group) ||(imdt_group == dst_group) ||( node_cache.find(imdt_node) != node_cache.end())){
                imdt_node = RandomInt(ed_N - 1); //RandomInt gets a number in the range[0,max], inclusive.
                imdt_group = imdt_node / ed_a;

                if (flag){
                    cout << "imdt_node generated: " << imdt_node << endl;
                }
        }
        node_cache.insert(imdt_node);
    }
    
    //nodes picked. Put them in the vector and return.
    imdt_nodes.clear();
    imdt_nodes.resize(no_of_nodes_to_generate, -1);
    
    ii = 0;
    for(auto it = node_cache.begin(); it != node_cache.end(); it++, ii++){
        imdt_nodes[ii] = *it;
    }
    if(flag){
        cout << "returning from generate_imdt_nodes_in_group_edison()" << endl;
    }


    return no_of_nodes_to_generate; //no meaning.
}


int generate_imdt_nodes_in_group_edison(const Flit *f, int src_router, int dst_router, int no_of_nodes_to_generate, std::vector<int> & imdt_nodes){
    //Vanilla version. Just randomly picks a number of nodes and returns.
    //For selective picking for other routing modes, needs to be updated.

    bool flag = false;

    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }

    if(flag){
        cout << "inside generate_imdt_nodes_in_group_edison() for flit " << f->id << endl;
        cout << "src_router: " << src_router << " , dst_router: " << dst_router << endl;   
    }


    std::unordered_set <int> node_cache;
    int ii;
    int src_group = src_router / ed_a;
    int imdt_node = -1;

    for(ii = 0; ii < no_of_nodes_to_generate; ii++){
        while( (imdt_node == -1) ||(imdt_node == src_router) || (imdt_node == dst_router) ||( node_cache.find(imdt_node) != node_cache.end())){
                imdt_node = src_group * ed_a + RandomInt(ed_a - 1); //RandomInt gets a number in the range[0,max], inclusive.
                
                if (flag){
                    cout << "imdt_node generated: " << imdt_node << endl;
                }
        }
        node_cache.insert(imdt_node);
    }
    
    //nodes picked. Put them in the vector and return.
    imdt_nodes.clear();
    imdt_nodes.resize(no_of_nodes_to_generate, -1);
    ii = 0;
    for(auto it = node_cache.begin(); it != node_cache.end(); it++, ii++){
        imdt_nodes[ii] = *it;
    }

    if(flag){
        cout << "returning from generate_imdt_nodes_in_group_edison()" << endl;
    }


    return no_of_nodes_to_generate; //no meaning.
}

int generate_vlb_path_from_given_imdt_node_vanilla_edison(const Flit *f, int src_router, int dst_router, int imdt_router,  std::vector<int> & pathVector){
    /*
    This funtion is handy when we are generating the intermediate nodes from an outside
    function following some specific scheme. In that case, this function just accepts that

    This is the vanilla version. Generates path using regular edison min path rule. For putting other restrictions on generated path, write another function.
    */
    
    /*
    Skipping error checking. Assumption is that the outer function does all these.    
    */

    bool flag = false;

    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }

    if(flag){
        cout << "Inside generate_vlb_path_from_given_imdt_node_vanilla_edison() for flit " << f->id << endl;
        cout << "src_router: " << src_router << " , dst_router: " << dst_router
        << " , imdt_router: " << imdt_router << endl;   
    }

    std::vector<int> first_half_pathVector;
    std::vector<int> second_half_pathVector;
    
    select_min_path_edison(src_router, imdt_router, first_half_pathVector, f);
    select_min_path_edison(imdt_router, dst_router, second_half_pathVector, f);

    //join the two vectors
    pathVector.clear();
    pathVector.reserve(first_half_pathVector.size() + second_half_pathVector.size() -1);
    pathVector.insert(pathVector.end(), first_half_pathVector.begin(), first_half_pathVector.end());
    pathVector.insert(pathVector.end(), second_half_pathVector.begin() + 1, second_half_pathVector.end());
    
    if(flag){
        cout << "Leaving generate_vlb_path_from_given_imdt_node_vanilla_edison() for flit " << f->id << endl;
    }


    return 1; // means nothing at this point.
}

void UGAL_L_mt_edison( const Router *r, const Flit *f, int in_channel, OutputSet *outputs, bool inject){
    
    // Threshold based multi-tier UGAL_L. Basically what we originally proposed 
    // for DF.   

    // Basic idea:
    //     While making the UGAL path selection decision between min and vlb,
    //     look at the buffer Q-length in the min path. Compare the length with
    //     some pre-determined threshold value. Based on the comparison result,
    //     select the pool from which to select the intermediate node for the 
    //     vlb path.

    //     For example, say the thresholds are 10 and 20.
    //     So we have three tiers:
    //         tier 0: <= 10
    //         tier 1: > 10 and <= 20
    //         tier 2: > 20
    //     At some point, the Q-len in the min path is 15. 
    //     So this is tier 1.

    //     We will select the regular vlab-path when a flit is at tier 2.
    //     In Edison, that is max 10 hops.

    //     For tier 1, we will restrict the vlb-path pool to slightly smaller 
    //     paths. For DF, we opted to use 5-hop paths. For Edison, it can be a 
    //     bit more complex. Explained below.

    //     For tier 0, the pool for vlb-path will be of even smaller length.
    //     For DF, it was <= 4 hop paths. For Edison, explained below.

    //     The reasoning behind this strategy is, when a non-min path is chosen
    //     over a min path based on comparing their Q-lens, we only know that
    //     the non-min path is less congested than the min path. However, the 
    //     Q-len value tells us a bit more about the likely scenario throughout
    //     the network. If, the min path Q-len is relatively low, say 5, it is
    //     more likely that the congestion is temporary. Probably the rest of the
    //     network is free, and the congestion will quickly go away after a few
    //     packets take the non-min paths. So we can safely choose a smaller pool
    //     of (shorter) non-min paths.

    //     But when the min-Q len is large (say, 40) it means it had been accumu-
    //     -lating for quite some time. Either the selected non-min paths were 
    //     not diverse enough, or the rest of the network is clogged too. In this
    //     case, we need to spread the traffic as far as possible, so use the
    //     traditional vlb routing.

    // But, how to choose the pool of paths in Edison? It is more complex than 
    // 4,5,6 hop paths in regular DF.

    // First, we can play with the hop-length inside the soruce group. Depending 
    // on how we choose out intermediate node (actually, group, more on it later),
    // it can be zero, one or two hops. 

    // Then, in the intermediate group, it can be two, three or four hops. 
    // (technically, one hop may be possible too. But that would be too complex/
    // restrictive. So ignore.) We could determine that in the source router like
    // what we were doing for DF; but considering it is basically deciding 
    // between min and vlb routing *inside* the imdt group, it makes more sense 
    // to do it in the imdt group itself. So, we make routing decision twice.

    // So here is the algorithm:
    //     - Step 1: Source Router:
    //         - Depending on threshold and q_len, choose min/vlb. For Vlb,
    //         choose an intermediate group. Generate the path to the
    //         imdt_in_gateway. Save this gateway in the flit.

    //     - Step 2: Imdt_src_gateway:
    //         - Depending on threshold value, generate the min path from the 
    //          gateway to the final destination. Get the imdt_out_gateway.
    //         - Now, depending on threshold and Q-len, do another in-group
    //         ugal-mt here. Take the q_len to the min-path, sleect either a 
    //         three-hop or a four-hop non min path, to the imdt_out_gateway. 
    //         Pick one.
    //         - Finally generate the rest of the path as <path to 
    //         imdt_out_gateway> + <global link> + <path within dest_group>.

    //     - Proceed as usual.
 
    //First check if it is an injection port. If yes, do accordingly.
    
    bool flag = false;
    
    //flag = true;
    
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }
    
    if (flag){
        cout << "inside UGAL_L_mt_edison() for flit: " << f-> id << endl;
    }
    
    
    if(inject) {
        outputs->Clear( ); //doesn't matter really. If the flit is at injection,
                            //means it was just generated, so the outputset is empty anyway.

        int inject_vc= RandomInt(gNumVCs-1);
        outputs->AddRange(-1, inject_vc, inject_vc);
        
        if (flag){
            cout << "injecting flit: " << f->id << " with vc: " << inject_vc << endl;
        }
          
        return;
    }
    
    
    //gather necessary info from the flit
    outputs -> Clear();
    
    int current_router = r->GetID();
    
    int out_port = -33;    //instead of setting all the default error values to -1, we used different values in different places. Works kinda like different error codes, helps in debugging.
    
    int out_vc = 0;
    
    int dst_PE = f->dest;
    int dst_router = dst_PE / ed_p;
    
    if (flag){
        cout << "current router:" << current_router << " , destination router: " << dst_router << endl;
    }

    //Then check if it is the destination router. 
    //If yes, then find the port to the destination PE.
    if(current_router == dst_router){
        out_vc = RandomInt(gNumVCs-1);
            
        //find the port that goes to the destination processing node
        out_port = dst_PE % ed_p;
        
        if (flag){
            cout << "destination router. ejecting through port " << out_port << " through vc " << out_vc << endl;
        }

        collect_path_stat(f);
    }
    
    else if (f->hop_count == 0){   //source router
        //if it is at the source router, then the input port must be a PE. Check if it is any different.
        if (flag){
            cout << "inside source router ..." << endl;
        }
        
        if (in_channel >= ed_p){
            cout << "EXCEPTION! Source router but port is not smaller than p" << endl;
            cout << "flitID: " << f->id << " src: " << f->src << " dest: " << f->dest  << " rID: " << r->GetID() << endl;
    
            cout << "p: " << ed_p << " in_channel: " << in_channel << endl;
            exit(-1);
        }
        
    
        //actual UGAL routing needs to happen here.
        
        std::vector<int> pathVector;
        
        int chosen_path_type; 
        
        chosen_path_type = select_UGAL_L_mt_path_edison(r, f, current_router, dst_router, pathVector, ed_routing_mode); 
              
        if (flag){
            cout << "select_ugal_l_mt_path_edison() returned: " << chosen_path_type << endl;
            cout << "the path returned is: ";
            print_vector(pathVector);
        }

        //Save the whole path in the flit
        f->path = std::move(pathVector);

        //depending on chosen path type, save the imdt_group_incoming_gateway in the flit.
        if (chosen_path_type == 0){
            //min path. no imdt_group_incoming_gateway
            f->imdt_gateway = -1;
        }else if (current_router/ed_a == dst_router/ed_a) { 
            //src and dst in the same group, so no imdt_gateway
            f->imdt_gateway = -1;
        }
        else{
            //non-min path. save the imdt_group_incoming_gateway
            f->imdt_gateway = f->path[f->path.size()-1];  
                // the vlb path should contain the gateway at the end, for this // particular routing only.
                // When both src and dst are inside the same group, we don't
                // have an imdt_gateway and in this case the dst_router
                // will be set to f->imdt_gateway. However, we are checking
                // if the dst_router is reached before we are checking this,
                // so should be safe.
        }
        
        //now get the port to the next hop node 
        out_port = find_port_to_node_edison(current_router, f->path[1], f);
        
        if(flag){
            cout << "port to next router is: " << out_port << endl;
        }
        
        //assign vc. first hop, so vc 0.
        out_vc = 0;
        
        //increase hop_count
        f->hop_count += 1;
    
    }

    // else if (this is the input_gateway in the intermediate group){
    //     //take intermediate group routing decision
    // }
    else if(current_router == f->imdt_gateway){
        //Vanilla mode: go to an intermediate node. Then go to the destination.
        std::vector<int> pathVector;
        
        int temp = select_imdt_group_mt_path_edison(r, f, current_router, dst_router, pathVector, ed_routing_mode);

        if (flag){
            cout << "select_imdt_group_mt_path_edison() returned: " << temp << endl;
            cout << "the path returned is: ";
            print_vector(pathVector);
        }

        //append the path at the end of the current path
        //DO NOT CLEAR the exisiting path-portion. Just append.
        f->path.reserve(f->path.size() + pathVector.size() -1);
        f->path.insert(f->path.end(), pathVector.begin() + 1, pathVector.end());

        // clear the f->imdt_gateway entry. Otherwise this might introduce a
        // cycle if the subsequent path happens to go throguh this node again.
        f->imdt_gateway = -2; 
                //a unique value just for future debugging purposes, if needed

        if (flag){
            cout << "complete path saved in the flit: ";
            print_vector(f->path); 
        }

        //now get the port to the next hop node 
        out_port = find_port_to_node_edison(current_router, f->path[ f->hop_count + 1], f);
        
        if(flag){
            cout << "port to next router is: " << out_port << endl;
        }
        
        //assign vc
        out_vc = f->hop_count;
        
        //increase hop_count
        f->hop_count += 1;
    
    }

    else{  //neither src nor dst router. Packet in flight.
        //get current hop count 
        
        //get port to the next hop node
        out_port = find_port_to_node_edison(current_router, f->path[ f->hop_count + 1], f);
        
        //assign vc 
        out_vc = f->hop_count;
        
        if (flag){
            cout << "port to next hop: " << out_port << " through vc: " << out_vc << endl; 
        }
        
        //increase hop_count
        f->hop_count += 1;
        
    }

    //finally, build the output set
    outputs->AddRange( out_port, out_vc, out_vc );
    
    if (flag){
        cout << "leaving UGAL_L_mt_dragonflyfull() for flit: " << f-> id << endl;
    }
}

int select_UGAL_L_mt_path_edison(const Router *r, const Flit *f, int src_router, int dst_router, std::vector<int> & pathVector, string routing){
    
    // This only decides between the min and vlb paths in the src node.
    // Also, will choose a routing-tier (for the src group only for
    // "ugal_mt" mode, and for the entire path for "ugal_mt_src" mode.

    // "routing" can have values "ugal_l" and "par". "ugal_l" is set as 
    // the defualt parameter in the header file.

        //Update: routing can also have "ugal_l_restricted."
        //But the only value that have any significance is "par". All other //basically do nothing.
    
    // Will generate path upto the intermediate node, will determine the
    // intermediate_group_incoming_gateway and save it in the flit.

    // For intermidate group routing decision, we'll write another helper 
    // function later.

    // return value indicates what type of path was selected. 
    // 0 -> min path, 1 -> non-min path

    bool flag = false;
    
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }
        
    if (flag){
        cout << "inside select_UGAL_L_mt_path_edison() for flit " << f->id << endl;
    }
    
    int no_of_MIN_paths_to_consider = 2;  
            //For edison, this should be fixed to 2.     
            //For same group src-dst pair, it'll be a problem otherwise,
            //because there are at most two min paths in that case.

                            
    int no_of_VLB_paths_to_consider = 2;  //can change this in future 
    
    int ii, jj, temp, chosen_pathID, chosen_pathID1, chosen_pathID2;
        
    std::vector< std::vector<int> > paths;
    std::vector<int> imdt_nodes;
    
    //int min_q_len;
    //int chosen_tier = 0; //only useful for multi-tiered routing

    int src_group, dst_group;

    int q_len1, q_len2, min_q_len;
    int returnVal;

    src_group = src_router / ed_a;
    dst_group = dst_router / ed_a;

    paths.resize(no_of_MIN_paths_to_consider + no_of_VLB_paths_to_consider); 
                        //1 min path, the rest non-min paths
        
    //individual paths need not be initialized, as path generator functions initialize the vectors themselves.    
    
    //We will only consider two min paths. In Edison, we probably will never change that.
    if (src_group == dst_group){
        temp = select_two_min_paths_inside_group_edison(src_router, dst_router, paths[0], paths[1], f);
    }
    else{
        
        chosen_pathID1 = select_min_path_edison(src_router, dst_router, paths[0], f);
        chosen_pathID2 = select_min_path_edison(src_router, dst_router, paths[1], f);
        while(chosen_pathID2 == chosen_pathID1){
            chosen_pathID2 = select_min_path_edison(src_router, dst_router, paths[1], f);
                //select_min_path_edison() clears the vector.
                //So it's safe, the previous paths won't stay.
        }
    }


    if (flag){
        cout << "selected shortest paths are: " << endl;
        print_vector(paths[0]);
        print_vector(paths[1]);
    }

    //get the q_len of the min-paths. 
    //Because there are two, lets take the average.
    q_len1 = find_port_queue_len_to_node_edison(r, paths[0][0], paths[0][1]);
    q_len2 = find_port_queue_len_to_node_edison(r, paths[1][0], paths[1][1]);
    min_q_len = (q_len1 + q_len2)/2;
        
    if (flag){
        cout << "q_len1, q_len2, min_q_len: " << q_len1 << "," << q_len2 << "," << min_q_len << endl;
    }

    //imdt_nodes.resize(no_of_VLB_paths_to_consider, -1);

    //Now, get the non-min paths   
    std::vector<std::vector<int>> vlb_paths;
    vlb_paths.resize(no_of_VLB_paths_to_consider);
    int chosen_tier = -1;

    //first, pick intermediate nodes
    if (src_group == dst_group){
        //select in-group nodes only as imdt nodes
        chosen_tier = generate_vlb_paths_in_group_threshold_edison(f, src_router, dst_router,  no_of_VLB_paths_to_consider, vlb_paths, min_q_len);
    }else{
        //select out-group imdt nodes
                //Update: we can not just have an imdt_node and then call the 
        //vlb_path_generation function on it to get the path.
        //We need to have the djkstra path.
        //That means we need to have the complete paths generated from these
        //functions already. 

        chosen_tier = generate_vlb_paths_threshold_edison(f, src_router, dst_router, no_of_VLB_paths_to_consider, vlb_paths, min_q_len, "src");
            //acceptable value for chosen_tier is 0, 1 or 2.

    }

    if (flag){
        cout << "generated non-min paths to imdt_gateway: " << endl;
        for(ii = 0; ii < vlb_paths.size(); ii++){
            print_vector(vlb_paths[ii]);
        }
    }

    //move the vlb_paths in the paths vector
    for(ii = 0, jj = no_of_MIN_paths_to_consider; ii < no_of_VLB_paths_to_consider; ii++, jj++){
        paths[jj] = std::move(vlb_paths[ii]);
    }

    //paths generated, now compare Q length and select one
    chosen_pathID = make_UGAL_L_path_choice_edison(r, f, no_of_MIN_paths_to_consider, no_of_VLB_paths_to_consider, paths);
    //if the out-of-group vlb path was chosen, save the imdt_group_in_gateway in the flit
    
    if (chosen_pathID < no_of_MIN_paths_to_consider){
        returnVal = 0; //min_path was selected
    }else{
        returnVal = 1; //vlb_path was selected
        f -> routing_tier = chosen_tier;
                    //only needed in ugal_mt_src routing

        if (PACKET_CATCH2){
            if (chosen_tier == 0){
                cout << "SRC. vlb path selected for flit: " << f->id << " , ";
                cout << " tier: " << chosen_tier << " ~~~~" << endl;
            }
            if (chosen_tier == 1){
                cout << "SRC. vlb path selected for flit: " << f->id << " , ";
                cout << " tier: " << chosen_tier << " @@@@@@@@" << endl;
            }
            if (chosen_tier == 2){
                cout << "SRC. vlb path selected for flit: " << f->id << " , ";
                cout << " tier: " << chosen_tier << " {}{}{}{}{}{}{}{}" << endl;
            }
            if (chosen_tier > 2){
                cout << "SRC. vlb path selected for flit: " << f->id << " , ";
                cout << " tier: " << chosen_tier << " !!!!!!!!!!!!!!!!!!!!" << endl;
            }
        }


    }

    if (flag){
        cout << "chosen ugal_l path id: " << chosen_pathID << endl;
        cout << "chosen path: ";
        print_vector(paths[chosen_pathID]);
        cout << endl;
    }

    //decide here if a min path was selected in UGAL. If yes, mark it for PAR routing.
    if (routing == "par"){

        if ((chosen_pathID < no_of_MIN_paths_to_consider) && (src_group != dst_group)) {
            f->PAR_need_to_revaluate = true;  
                    
            //calculate and put the src_out_gatway
            //just do a brute-force. go through the nodes in the path and 
            //check when a different group starts
            for(ii = 0; ii < paths[chosen_pathID].size(); ii++){
                if (paths[chosen_pathID][ii] / ed_a != src_group){
                    break;
                }
            }
            f->src_out_gateway = paths[chosen_pathID][ii-1];

            if (flag){
                cout << "Phase: src. Path_type_chosen: min. src_out_gateway :" << f->src_out_gateway << endl;
            }

        }else{
            f->PAR_need_to_revaluate = false;
            f->src_out_gateway = -1;
            
            if (flag){
                cout << "Phase: src. Path_type_chosen: non-min. src_out_gateway :" << f->src_out_gateway << endl;
            }
        }
    }

    //Now copy the chosen path in pathVector

    pathVector = std::move(paths[chosen_pathID]);  

    return returnVal;   //0 -> min path; 1 -> non-min path

}

int generate_vlb_paths_in_group_threshold_edison(const Flit *f, int src_router, int dst_router, int no_of_VLB_paths_to_consider, std::vector< std::vector<int> > & pathVectors, int min_q_len){
    //pathVectors are already allocated in caller

    //inside group, there are two options only:
        //three-hop vlb paths
        //four-hop vlb paths
    //compare min_q_len with threshold2, and take decision accordingly.
    
    //threshold1 or threshold2? We dont know. Without experiemtn hard to tell.

    bool flag = false;
    int chosen_tier = -1;

    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }

    if(flag){
        cout << "inside generate_vlb_paths_in_group_threshold_edison() for flit " << f->id << endl;
        cout << "src_router: " << src_router << " , dst_router: " << dst_router << endl;   
    }

    if (ed_routing_mode == "ugal_l_restricted"){
        generate_three_hop_vlb_paths_in_group_edison(f, src_router, dst_router,  no_of_VLB_paths_to_consider, pathVectors);

        chosen_tier = 3;
    }
    else{
        if ( min_q_len < ed_mt_thld_2 ){
            //generate_three_hops_imdt_nodes_in_group_edison(f, src_router, dst_router, no_of_nodes_to_generate, imdt_nodes);
            generate_three_hop_vlb_paths_in_group_edison(f, src_router, dst_router,  no_of_VLB_paths_to_consider, pathVectors);

            chosen_tier = 3;

        }else{
            std::vector<int> imdt_nodes;
            imdt_nodes.resize(no_of_VLB_paths_to_consider);
            generate_imdt_nodes_in_group_edison(f, src_router, dst_router, no_of_VLB_paths_to_consider, imdt_nodes);
            for(int ii = 0; ii < no_of_VLB_paths_to_consider; ii++){
                generate_vlb_path_from_given_imdt_node_vanilla_edison(f, src_router, dst_router, imdt_nodes[ii],  pathVectors[ii]);
            }
            chosen_tier = 4;
        }
    }

    if(flag){
        cout << "returning from generate_vlb_paths_in_group_threshold_edison()" << endl;
    }

    return chosen_tier;
}


int generate_vlb_paths_threshold_edison(const Flit *f, int src_router, int dst_router, int no_of_vlb_paths_to_generate, std::vector< std::vector<int>> & pathVectors, int min_q_len, string PAR_phase){
    //only generate paths upto the i-group gateway

    //only deals with routing in the src node
    //if min_q_len < ed_mt_thld_1, select i_node gateway that is 0 hop away.
    //else, if min_q_len < ed_mt_thld_2, select i_node gateway that is at-most 1 hop away.
    //else, select i_node gateway that is 2 hop away (regular mode).

    //PAR_phase has a default value of "src". Acceptable values are "src" and "gateway".
    //This is needed for PAR in src_out_gateway. There, because we are
    // re-evaluating min routing decision, considering tier 0 is basically 
    // using the same global link as the min path with 50% to 100% chance. So it 
    // might be better to consider tier 1 as the minimum threshold. 

    if ( (PAR_phase != "src") && (PAR_phase != "gateway") ){
        cout << "ERROR! unsupported value of PAR_phase: " << PAR_phase;
        cout << " in generate_vlb_paths_threshold_edison(). Exiting."<< endl;
        exit(-1);
    }

    bool flag = false;
    int chosen_tier = -1;
    int imdt_gateway, imdt_group;
    int count;

    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }

    if(flag){
        cout << "inside generate_vlb_paths_threshold_edison() for flit " << f->id << endl;
        cout << "src_router: " << src_router << " , dst_router: " << dst_router << endl;   
    }

    //for ugal / PAR src
    if (PAR_phase == "src"){
        if (min_q_len < ed_mt_thld_1){
            chosen_tier = 0;
        }else if (min_q_len < ed_mt_thld_2){
            chosen_tier = 1;
        }else{
            chosen_tier = 2;
        }
    }
    else if (PAR_phase == "gateway"){
        if (min_q_len < ed_mt_thld_2){
            chosen_tier = 1;
        }else{
            chosen_tier = 2;
        }
    }
    
    if (ed_routing_mode == "ugal_l_restricted"){
        generate_one_hop_vlb_paths_to_gateway_edison(f, src_router, dst_router, no_of_vlb_paths_to_generate, pathVectors);
        chosen_tier = 1;    
    }

    else{

        if (chosen_tier == 0){
            for(count = 0; count < no_of_vlb_paths_to_generate; count++){
                imdt_gateway = ed_graph[src_router][20 + RandomInt( ed_graph[src_router].size() - 20 -1)];    
                    //-1 because RandomInt() works on close range
                pathVectors[count] = {src_router, imdt_gateway};
            }

        }else if (chosen_tier == 1){
            
            //generate_three_hops_imdt_nodes_edison(f, src_router, dst_router, no_of_nodes_to_generate, imdt_nodes);
            generate_one_hop_vlb_paths_to_gateway_edison(f, src_router, dst_router, no_of_vlb_paths_to_generate, pathVectors);
        }
        else{
            //generate_imdt_nodes_vanilla_edison(f, src_router, dst_router, no_of_nodes_to_generate, imdt_nodes);
            generate_regular_vlb_paths_to_gateway_edison(f, src_router, dst_router, no_of_vlb_paths_to_generate, pathVectors);
            
        }
    }


    if(flag){
        cout << "returning generate_vlb_paths_threshold_edison()" << endl;
    }    

    return chosen_tier;
}



int generate_three_hop_vlb_paths_in_group_edison(const Flit *f, int src_router, int dst_router, int no_of_VLB_paths_to_consider, std::vector<std::vector<int> > & pathVectors){
    //Only select the intermediate nodes on the same row or columns with src or dst.
    //there are fifteen + five options for src.
    //there are fifteen + five options for dst.
    //When src and dst share a row or column, options reduce.
    
    bool flag = false;
    
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }

    if(flag){
        cout << "inside generate_three_hop_vlb_paths_in_group_edison() for flit " << f->id << endl;
        cout << "src_router: " << src_router << " , dst_router: " << dst_router << endl;   
    }

    int src_group, dst_group, src_row, dst_row, src_col, dst_col;
    int count;
    int inode;

    std::unordered_set < int > node_cache;

    src_group = src_router / ed_a;
    dst_group = dst_router / ed_a;

    src_row = (src_router - src_group * ed_a) / ed_dims[0];
    src_col = (src_router - src_group * ed_a) % ed_dims[0];

    dst_row = (dst_router - dst_group * ed_a) / ed_dims[0];
    dst_col = (dst_router - dst_group * ed_a) % ed_dims[0];


    inode = -1;
    for(count = 0; count < no_of_VLB_paths_to_consider; count ++){
        while ( (inode == -1) || (inode == src_router) || (inode == dst_router) || (node_cache.find(inode) != node_cache.end() ) ){

            inode = get_inode_on_same_row_or_column(f, src_group, dst_group, src_row, dst_row, src_col, dst_col);  
            if(flag){
                cout << "inode: " << inode << endl;
                cout << "src, dst, count, no_of_VLB_paths_to_consider: " << src_router << "," << dst_router << "," << count << "," << no_of_VLB_paths_to_consider << endl;
                cout << "node_cache size: " << node_cache.size() << endl;      
            }
        }
        node_cache.insert(inode);
    }

    //now generate vlb paths in the form of {src, inode, dst}
    count = 0;
    for(auto it = node_cache.begin(); it!= node_cache.end(); it++){
        generate_vlb_path_from_given_imdt_node_vanilla_edison(f, src_router, dst_router, *it,  pathVectors[count]);
        count += 1;
    }

    if(flag){
        cout << "returning generate_three_hop_vlb_paths_in_group_edison()" << endl;
    }    

    return 1;   //success
}



//int generate_three_hops_imdt_nodes_edison(const Flit *f, int src_router, int dst_router, int no_of_nodes_to_generate, std::vector<int> & imdt_nodes){
int  generate_one_hop_vlb_paths_to_gateway_edison(const Flit *f, int src_router, int dst_router, int no_of_vlb_paths_to_generate, std::vector<std::vector<int> > & pathVectors){
    //generate paths in the form of {src, src_group_out_gw, imdt_group_in_gateway}

    bool flag = false;
    
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }

    if(flag){
        cout << "inside generate_two_hops_vlb_paths_edison() for flit " << f->id << endl;
        cout << "src_router: " << src_router << " , dst_router: " << dst_router << endl;   
    }

    int src_group, src_row, src_col;
    //int dst_group, dst_row, dst_col;
    int random_idx, count;
    int col, row, port;
    int neighbor, imdt_gateway;
    std::unordered_set < std::pair<int,int>, pair_hash > node_cache;
    
    src_group = src_router / ed_a;
    //dst_group = dst_router / ed_a;

    src_row = (src_router - src_group * ed_a) / ed_dims[0];
    src_col = (src_router - src_group * ed_a) % ed_dims[0];

    //dst_row = (dst_router - dst_group * ed_a) / ed_dims[0];
    //dst_col = (dst_router - dst_group * ed_a) % ed_dims[0];

    neighbor = -1;
    port = -1;

    for(count = 0; count < no_of_vlb_paths_to_generate; count++){
        while( (neighbor == -1) || ( node_cache.find(std::make_pair(neighbor,port)) != node_cache.end())){
                //it's okay if the neighbor is the same as src.
                //neighbor will never be the same as dst, as dst is in a different group to begin with
    
            if (src_col <= 11){
                random_idx = RandomInt(37); 
                    //  39 = 12*2 + 4 + 5*2 - 1, because it starts from 0
                    //  five rows instead of six, because otherwise the src 
                    //  node will be considered twice. Once for row and 
                    //  once for col
            }else{
                random_idx = RandomInt(32); 
                    // 33 = 12*2 + 4 + 5 - 1, because it starts from 0
            }

            if (random_idx < 24){
                //its from the first 12 columns
                col = random_idx / 2;
                row = src_row;
                port = random_idx % 2;
            }
            else if(random_idx < 28){
                //next 4 columns
                col = 11 + (random_idx - 23);
                row = src_row;
                port = 0;
            }
            else{
                //different row, same column
                col = src_col;
                if (src_col <= 11){
                    //each node has two global outgoing connections
                    row = (random_idx - 28) / 2;
                    if (row >= src_row){
                        row += 1;
                    }
                    port = (random_idx - 28) % 2;
                }else{
                    //each node has one global outgoing connections
                    row = random_idx - 28;
                    if (row >= src_row){
                        row += 1;
                    }
                    port = 0;

                }
            }

            neighbor = src_group * ed_a + row * ed_dims[0] + col;
            
            if (flag){
                cout << "random_idx, row, col, port, neighbor: " << random_idx << "," << row << "," << col << "," << port << "," << neighbor  << endl;
            }
        }
        node_cache.insert(std::make_pair(neighbor,port));

    }

    count = 0;
    for (auto it = node_cache.begin(); it != node_cache.end(); it++){
        neighbor = (*it).first;
        port =  (*it).second;
        imdt_gateway = ed_graph[neighbor][20 + port];
        
        if (flag){
            cout << "The neigbors of " << neighbor << " : ";
            print_vector(ed_graph[neighbor]);
            cout << "neighbor, port, imdt_gateway: " << neighbor << " , " << port << " , " << imdt_gateway << endl;
        }

        //now generate the paths in the form of {src, neighbor, imdt_gateway}
        if (neighbor == src_router){
            pathVectors[count] = {src_router, imdt_gateway};    
        }
        else{
            pathVectors[count] = {src_router, neighbor, imdt_gateway};
        }
        count += 1;
    }

    if(flag){
        cout << "returning generate_two_hops_vlb_paths_edison()" << endl;
    }    

    return 1;   //success

}



int generate_regular_vlb_paths_to_gateway_edison(const Flit *f, int src_router, int dst_router, int no_of_vlb_paths_to_generate, std::vector<std::vector<int> > & pathVectors){
    //from src, generate path to a randomly selected intermediate node.
    //but return a path only upto the first node in the intermediate group.

    bool flag = false;
    
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }

    if(flag){
        cout << "inside generate_regular_vlb_paths_to_gateway_edison() for flit " << f->id << endl;
        cout << "src_router: " << src_router << " , dst_router: " << dst_router << endl;   
    }


    std::vector<int> imdt_nodes;
    imdt_nodes.resize(no_of_vlb_paths_to_generate);

    int imdt_node, imdt_group, src_group;
    int src_gateway, imdt_gateway;
    int link_to_select;
    std::pair<int, int> selected_global_link;

    src_group = src_router / ed_a;

    generate_imdt_nodes_vanilla_edison(f, src_router, dst_router, no_of_vlb_paths_to_generate, imdt_nodes);
    if (flag){
        cout << "the selected intermediate nodes are: ";
        print_vector(imdt_nodes);
    }

    //imdt nodes generated. Now generate vlb path for each one of these.
    for(int ii = 0; ii< no_of_vlb_paths_to_generate; ii++){

        imdt_node = imdt_nodes[ii];
        imdt_group = imdt_node / ed_a;

        link_to_select = RandomInt(ed_inter_group_links[src_group][imdt_group].size() - 1); 
                    //RandomInt gets a number in the range[0,max], inclusive.
        
        selected_global_link = ed_inter_group_links[src_group][imdt_group][link_to_select];
        
        src_gateway = selected_global_link.first;
        imdt_gateway = selected_global_link.second;

        if (flag){
            cout << "selected global link: " << src_gateway << "," << imdt_gateway << endl;
        }

        std::vector<int> first_half_pathVector;
        //std::vector<int> second_half_pathVector;
        
        select_min_path_inside_group_edison(src_router, src_gateway, first_half_pathVector,f);
        //select_min_path_inside_group_edison(dst_gateway, dst_router, second_half_pathVector,f);

        //merge the two vectors

        pathVectors[ii].clear();
        pathVectors[ii].reserve(first_half_pathVector.size() + 1 );

        if (flag){
            cout << "pathVector size: " << pathVectors[ii].size() << endl;
        }

        pathVectors[ii].insert(pathVectors[ii].end(), first_half_pathVector.begin(), first_half_pathVector.end());
        pathVectors[ii].push_back(imdt_gateway);

        if (flag){
            cout << "first_half_pathVector: ";
            print_vector(first_half_pathVector);
            cout << "first_half_pathVector size: " << first_half_pathVector.size() << endl;
            cout << "path to be returned: ";
            print_vector(pathVectors[ii]);                
            cout << "pathVector size: " << pathVectors[ii].size() << endl;
            cout << "forced print: " << pathVectors[ii][first_half_pathVector.size()] << endl;
        }


    }

    if(flag){
        cout << "returning generate_regular_vlb_paths_to_gateway_edison()" << endl;
    }    

    return 1; //success
}



int get_inode_on_same_row_or_column(const Flit *f, int src_group, int dst_group, int src_row, int dst_row, int src_col, int dst_col){
    //helper function
    //this version picks a node that resides on either src or dst col or row.

    bool flag = false;
    
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }

    if(flag){
        cout << "inside get_inode_on_same_row_or_column() for flit " << f->id << endl;  
    }

    int random_idx = RandomInt(16 + 6 + 16 + 6 - 1); //because indexing starts from 0
    if (flag){
        cout << "random idx: " << random_idx << endl;
    }

    int inode;

    if (random_idx < 16){
        inode = (src_group * ed_a) + src_row * ed_dims[0] + random_idx;
    }else if(random_idx < 22){
        inode = (src_group * ed_a) + (random_idx-16) * ed_dims[0] + src_col;
    }else if(random_idx < 38){
        inode = (dst_group * ed_a) + dst_row * ed_dims[0] + (random_idx - 22);
    }else {
        inode = (dst_group * ed_a) + (random_idx-38) * ed_dims[0] + dst_col;
    }

    if(flag){
        cout << "leaving get_inode_on_same_row_or_column()" << endl;
    }    

    return inode;
}

int get_inode_on_same_row_or_column_of_node(const Flit *f, int node){
    //helper function
    //this version picks a node on either src_row or src_col

    bool flag = false;
    
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }

    if(flag){
        cout << "inside get_inode_on_same_row_or_column_of_node() for flit " << f->id << endl;  
    }

    int src_group = node / ed_a;
    int src_row = (node - src_group * ed_a) / ed_dims[0];
    int src_col = (node - src_group * ed_a) % ed_dims[0];
    int random_idx;

    int inode = -1;

    while( (inode == -1) || (inode == node) ){
        random_idx = RandomInt(16 + 6 - 1); 
                //because indexing starts from 0

        if (random_idx < 16){
            inode = (src_group * ed_a) + src_row * ed_dims[0] + random_idx;
        }else {
            inode = (src_group * ed_a) + (random_idx-16) * ed_dims[0] + src_col;
        }

        if (flag){
            cout << "random idx: " << random_idx << " , inode: " << inode <<  endl;
        }

        
    }


    if(flag){
        cout << "leaving get_inode_on_same_row_or_column_of_node() " << endl;
    }    

    return inode;
}

int select_imdt_group_mt_path_edison(const Router *r, const Flit *f, int current_router, int dst_router, std::vector<int> & pathVector, string mt_routing_mode){
    
    // mt_routing_mode is used to differnetiate between ugal_mt and ugal_mt_src.

    // This function is called when the flit has just entered the intermediate 
    // group. It needs to go out of this group towards the final destination.
    // The decision to take is whether to take a direct route to the out_gateway, 
    // or whether to take an inter-group non-minimal path using an in-group
    // intermediate node.

    // First, two min-paths will be chosen. Based on the min-path Q_len, two three-hop
    // or four-hop non-min paths will be chosen. Then a ugal_decision will be made 
    // again.

    // Then the complete path will be generated and returned via pathVector.

    bool flag = false;
    
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }

    if(flag){
        cout << "inside select_imdt_group_mt_path_edison() for flit " << f->id << endl;
        cout << "current_router: " << current_router << " , dst_router: " << dst_router << endl;   
    }

    std::vector<int> min_pathVector;

    //TODO: TOTHINK
    //Right now we are choosing only one min and one nin-min path, for simplicity.
    //These is scope here to play with the no of paths used.

    std::vector <int> min_path;
    std::vector<int> first_half_pathVector;
    std::vector<int> second_half_pathVector;

    int min_q_len, chosen_pathID, return_val;
    int imdt_router, current_group;
    int routing_tier;
    
    if (flag){
        cout << "First selecting min path to the destination." << endl;
    }
    //get min-path to dst. Then get the min_q len for the min-path. 
    chosen_pathID = select_min_path_edison(current_router, dst_router, min_path, f);
    
    if (flag){
        cout << "selected min path: " << endl;
        print_vector(min_path);
    }


    if (mt_routing_mode == "mt"){
        //get the q_len of the min-paths. 
        //Because there are two, lets take the average.
        min_q_len = find_port_queue_len_to_node_edison(r, min_path[0], min_path[1]);
            
        if (flag){
            cout << "min_q_len: " << min_q_len << endl;
        }

        if (min_q_len < ed_mt_thld_1){
            routing_tier = 0;
        }else if (min_q_len < ed_mt_thld_2){
            routing_tier = 1;    
        } else{
            routing_tier = 2;
        }
    }else if(mt_routing_mode == "mt_src"){
        routing_tier = f->routing_tier;
    }else if(mt_routing_mode == "ugal_l_restricted"){
        routing_tier = 1; //f->routing_tier would've worked too. 
    }
    else{
        cout << "Error! Unsupported routing_mode " << ed_routing_mode << " . Exiting." << endl;
        exit(-2);   //just a random value
    }

    if (flag){
        cout << "mt_routing mode: " << mt_routing_mode << " , ";
        cout << "routing_tier: " << routing_tier << endl;
    }

    if (routing_tier == 0){
        //min path is the path
        pathVector = std::move(min_path);
        return_val = 0; //just to signify this branch. No further meaning.
        
    }else if (routing_tier == 1){
        //select a three-hop path
        imdt_router = get_inode_on_same_row_or_column_of_node(f, current_router);
        select_min_path_edison(current_router, imdt_router, first_half_pathVector, f);
        select_min_path_edison(imdt_router, dst_router, second_half_pathVector, f);
        //join the two vectors
        pathVector.clear();
        pathVector.reserve(first_half_pathVector.size() + second_half_pathVector.size() -1);
        pathVector.insert(pathVector.end(), first_half_pathVector.begin(), first_half_pathVector.end());
        pathVector.insert(pathVector.end(), second_half_pathVector.begin() + 1, second_half_pathVector.end());
        return_val = 1;

    }else{
        //select a regular vlb path
        current_group = current_router / ed_a;

        imdt_router = current_group * ed_a + RandomInt(ed_a - 1);

        select_min_path_edison(current_router, imdt_router, first_half_pathVector, f);
        select_min_path_edison(imdt_router, dst_router, second_half_pathVector, f);
        
        //join the two vectors
        pathVector.clear();
        pathVector.reserve(first_half_pathVector.size() + second_half_pathVector.size() -1);
        pathVector.insert(pathVector.end(), first_half_pathVector.begin(), first_half_pathVector.end());
        pathVector.insert(pathVector.end(), second_half_pathVector.begin() + 1, second_half_pathVector.end());
        return_val = 2;
    }

    //for debugging only
    /*if (return_val == 2){
        cout << "choice " << return_val << " min_q_len " << min_q_len << " for flit " << f->id << endl;
        cout << "path: ";
        print_vector(pathVector);
    }*/

    if(flag){
        cout << "returning select_imdt_group_mt_path_edison()" << endl;
    }    

    return return_val;
}


void collect_path_stat(const Flit *f){
    //check if the flit is min or vlb.
    if (f->min_or_vlb == 0){
        ed_total_min_flit += 1;
    }else if (f->min_or_vlb == 1){
        ed_total_non_min_flit += 1;
    }else{
        //not applicable
    }

    int src_group = f->src / ed_a;
    int dst_group = f->dest / ed_a;
    
    int path_len = f->path.size() - 1; 

    //max_path_len is usually 10, but can be 12 for PAR
    if (path_len > 12){
        cout << "ERROR! path len " << path_len << " for flit " << f->id << " (src, dst) : " << f->src << " , " << f->dest << endl;
    }

    //in case of vlb:
        //case 1: src_group == dst_group
        //case 2: src_group != dst_group
    if (f->min_or_vlb == 1){
        if (src_group == dst_group){
            ed_ingroup_path_dist[path_len] += 1;
        }else{
            ed_outgroup_path_dist[path_len] += 1;
        }
    }
}


void PAR_edison( const Router *r, const Flit *f, int in_channel, OutputSet *outputs, bool inject){

    /*
    PAR: Prograssive Adaptive routing function.
        
    ******************************
    source routing. 
    If it is the source router: 
                 Make the routing decision (MIN or VLB).     
                 Save the complete path in the flit.
                 Mark if the min path was taken. 
                 Forward the filt to the next hop.
    
    If it is on gateway router on its source group, then:
                 Check if the non-min path was taken in the source router. If yes, do nothing.
                 Else,
                    (Min router was chosen in the non-gateway source router.)
                    Re-evaluate the routing decision. 
                    If necessary, move to non-Min path. In that case:
                        Save the new path in the flit.
                        Bump the VC by 1.
                 Forward to the next hop.
    Otherwise:
                 Just look up the saved path in the flit
                 And forward to the next hop.
    That's it!
    ******************************
    
    
    As usual,
    The routing function needs to find:
        - an output port
        - an output vc
        
    For the output port:
        - First check if it is an injection port. If yes, do accordingly.
        
        - Then check if it is the destination router. If yes, then find the port
         to the destination PE.
         
        - Then check if it is the source router. If yes:
            - make the routing decision. generate the full path accordingly.
            - Save the whole path in the flit
            - in case of non-min path, mark the src_out_gateway in the flit 
            
        - Then check if it is in the src_out_gateway on the path. Re-evaluate routing decision if needed.

        - Based on the hop count, find the port id to the next router.
        - Increase hop-count.
        
    For the output vc:
        - give a new vc on each hop.
            - For non-minimal routing, max hop-count is 12 (2 extra for PAR
            re-evaluation). So we need at least 12 vcs.
        - For flit injection, randomly assign a vc.
    
    So two functions needed:
        - A function to generate the complete path for the flit, given a pair of src and dst.
        - A function for finding the port to the next hop given the flit's hop count, 
        saved path, current and next routers.
    */
    

    
    //First check if it is an injection port. If yes, do accordingly.
    
    bool flag = false;
    
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }
    
    if (flag){
        cout << "inside PAR_edison() for flit: " << f-> id << endl;
        //cout << "routing mode: " << g_routing_mode << endl;
    }
    
    
    if(inject) {
        outputs->Clear( ); //doesn't matter really. If the flit is at injection,
                            //means it was just generated, so the outputset is empty anyway.

        int inject_vc= RandomInt(gNumVCs-1);
        outputs->AddRange(-1, inject_vc, inject_vc);
        
        if (flag){
            cout << "injecting flit: " << f->id << " with vc: " << inject_vc << endl;
        }
          
        return;
    }
    
    
    //gather necessary info from the flit
    outputs -> Clear();
    
    int current_router = r->GetID();
    
    int out_port = -33;    //instead of setting all the default error values to -1, we used different values in different places. Works kinda like different error codes, helps in debugging.
    
    int out_vc = 0;
    
    int dst_PE = f->dest;
    int dst_router = dst_PE / ed_p;

    if (flag){
        cout << "current router:" << current_router << " ,destination router:" << dst_router <<  endl;
    }
    
    //Then check if it is the destination router. 
    //If yes, then find the port to the destination PE.
    if(current_router == dst_router){
        out_vc = RandomInt(gNumVCs-1);
            
        //find the port that goes to the destination processing node
        out_port = dst_PE % ed_p;
        
        if (flag){
            cout << "destination router. ejecting through port " << out_port << " through vc " << out_vc << endl;
        }

        collect_path_stat(f);
    }
    
    else if (f->hop_count == 0){   //source router
        //if it is at the source router, then the input port must be a PE. Check if it is any different.
        if (flag){
            cout << "inside source router ..." << endl;
        }
        
        if (in_channel >= ed_p){
            cout << "EXCEPTION! Source router but port is not smaller than p" << endl;
            cout << "flitID: " << f->id << " src: " << f->src << " dest: " << f->dest  << " rID: " << r->GetID() << endl;
    
            cout << "p: " << ed_p << " in_channel: " << in_channel << endl;
            exit(-1);
        }
        
    
        //actual source routing needs to happen here.
        
        std::vector<int> pathVector;
        
        int temp; 
        
        temp = select_UGAL_L_path_edison(r, f, current_router, dst_router, pathVector, ed_routing_mode);
        
        if (flag){
            cout << "select_ugal_l_path_edison() returned: " << temp << endl;
            cout << "the path returned is: ";
            print_vector(pathVector);
        }

        //Save the whole path in the flit
        f->path = std::move(pathVector);
        
        //gateway and min_vs_vlb marking is done inside the select_ugal_l function.
        //So no need to worry about it here.

        //now get the port to the next hop node 
        out_port = find_port_to_node_edison(current_router, f->path[1], f);
        
        if(flag){
            cout << "port to next router is: " << out_port << endl;
        }
        
        //assign vc. first hop, so vc 0.
        out_vc = 0;
        
        //increase hop_count
        f->hop_count += 1;
    
    }
    
    else if ( (f->PAR_need_to_revaluate == true ) && ( current_router == f-> src_out_gateway) ){
        //re-evaluate the routing decision
        if (flag){
            cout << "inside src_gateway router ..." << endl;
        }
        
        std::vector<int> pathVector;
        int temp;

        temp = select_PAR_path_at_gateway_edison( r, f, current_router, dst_router, pathVector);

        if (flag){
            cout << "select_PAR_path_at_gateway_edison() returned: " << temp << endl;
            cout << "the path returned is: ";
            print_vector(pathVector);
        }

        if (temp == 1){  //non-min path was chosen

            //We want to record the whole path a flit takes.
            //So insert the nodes upto the gateway in front of the new path
            for (int ii = f->hop_count - 1; ii >= 0; ii--){
                pathVector.insert(pathVector.begin(), f->path[ii]);
            }

            //Save the whole path in the flit 
            f->path = std::move(pathVector);
            if (flag){
                cout << "the complete path is: ";
                print_vector(f->path);
            }
        }

        //now get the port to the next hop node 
        out_port = find_port_to_node_edison(current_router, f->path[f->hop_count + 1], f);
        
        if(flag){
            cout << "port to next router is: " << out_port << endl;
        }
        
        //assign vc 
        out_vc = f->hop_count;
        
        //increase hop_count
        f->hop_count += 1;
    
    }

    else{  //neither src nor dst router. Packet in flight.
        //get current hop count 
        
        //get port to the next hop node
        out_port = find_port_to_node_edison(current_router, f->path[ f->hop_count + 1], f);
        
        //assign vc 
        out_vc = f->hop_count;
        
        if (flag){
            cout << "port to next hop: " << out_port << " through vc: " << out_vc << endl; 
        }
        
        //increase hop_count
        f->hop_count += 1;
        
    }

    //finally, build the output set
    outputs->AddRange( out_port, out_vc, out_vc );
    
    if (flag){
        cout << "leaving UGAL_L_dragonflyfull() for flit: " << f-> id << endl;
    }
    

}


int select_PAR_path_at_gateway_edison( const Router *r, const Flit *f, int src_router, int dst_router, std::vector<int> & pathVector){
        
    
        //this is the step where PAR min-path option is evaluated at the gateway.
        //So the flit already contains the min path, just generate 2 more
        //non-min paths and compare.

        //Note: src_router is the gateway. May not be the actual flit source.

        //Also: assume that src_group != dst_group. Those cases will be 
        //taken care of in the src phase and will never reach this phase.

        // returns: 
        //     0 -> min path chosen, so no path updating
        //     1 -> non-min path chosen, so path updated

    
        
        //look up the min path from the flit
        //generate two VLB paths
        //multiply the Q-length for each path with the paths hop count
        //choose the path with the smallest value

        
    bool flag = false;
    
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }
        
    if (flag){
        cout << "inside select_PAR_path_at_gateway_edison() for flit " << f->id << endl;
    }
    
    int no_of_MIN_paths_to_consider = 1;        
                            //the path is already in the flit

    int no_of_VLB_paths_to_consider = 2;  //can change this in future 
    
    int temp, chosen_pathID, chosen_pathID1, chosen_pathID2;
        
    std::vector< std::vector<int> > paths;
    std::vector<int> imdt_nodes;
    
    //int min_q_len;
    //int chosen_tier = 0; //only useful for multi-tiered routing

    int src_group, dst_group;
    int ii, jj;

    int return_val = -1; 

    src_group = src_router / ed_a;
    dst_group = dst_router / ed_a;

    paths.resize(no_of_MIN_paths_to_consider + no_of_VLB_paths_to_consider); 
                        //1 or 2 min path, the rest non-min paths
        

    //In this phase, the min path is already selected.
    //Just look it up from the flit
    paths[0].resize(f->path.size() - f->hop_count);
    for (ii = f->hop_count, jj = 0; ii < f->path.size(); ii++, jj++){
        paths[0][jj] = f->path[ii];
    }

    if (flag){
        cout << "path stored in the flit is: ";
        print_vector(f->path);
        cout << "path to be considered as min path: ";
        print_vector(paths[0]);
    }

    
    //Now, get the non-min paths   
    //first, pick intermediate nodes
    generate_imdt_nodes_vanilla_edison(f, src_router, dst_router, no_of_VLB_paths_to_consider, imdt_nodes);
    
    if (flag){
        cout << "generated imdt nodes: ";
        print_vector(imdt_nodes);
    }

    //now generate paths using the picked nodes
    for(size_t ii = 0; ii < imdt_nodes.size(); ii++){
        generate_vlb_path_from_given_imdt_node_vanilla_edison(f, src_router, dst_router, imdt_nodes[ii], paths[no_of_MIN_paths_to_consider + ii]);
    }


    if (flag){
        cout << "Generated vlb paths:" << endl;
        for(std::size_t jj = no_of_MIN_paths_to_consider; jj < paths.size(); jj++){
            print_vector(paths[jj]);
        }
        cout << endl;
    }
    
    //paths generated, now compare Q length and select one
    chosen_pathID = make_UGAL_L_path_choice_edison(r, f, no_of_MIN_paths_to_consider, no_of_VLB_paths_to_consider, paths);

    if (chosen_pathID < no_of_MIN_paths_to_consider){
        return_val = 0; // min path
    }else{
        return_val = 1; // non-min path
    }

    // if (chosen_pathID >= no_of_MIN_paths_to_consider){
    //     cout << "PAR path correction for flit " << f->id << " ####################################" << endl;
    // }else{
    //     cout << "no PAR path-correction for flit " << f->id << " -------------------" << endl;
    // }

    
    if (flag){
        cout << "chosen ugal_l path id: " << chosen_pathID << endl;
        cout << "chosen path: ";
        print_vector(paths[chosen_pathID]);
        cout << endl;
    }

    //ensure the flit will not be evaluated anymore.
    f->PAR_need_to_revaluate = false;
    f->src_out_gateway = -1;

    
    //Now copy the chosen path in pathVector
    pathVector = std::move(paths[chosen_pathID]);  

    if (flag){
        cout << "leaving select_PAR_path_at_gateway_edison() for flit: " << f-> id << endl;
    }
    
    
    return return_val;   
}

int select_PAR_mt_path_at_gateway_edison( const Router *r, const Flit *f, int src_router, int dst_router, std::vector<int> & pathVector){

    //this is the step where PAR min-path option is evaluated at the gateway.
    //So the flit already contains the min path, just generate 2 more
    //non-min paths and compare.

    //Note: src_router is the gateway. May not be the actual flit source.

    //Also: assume that src_group != dst_group. Those cases will be 
    //taken care of in the src phase and will never reach this phase.

    // returns: 
    //     0 -> min path chosen, so no path updating
    //     1 -> non-min path chosen, so path updated

    bool flag = false;
    
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }
        
    if (flag){
        cout << "inside select_PAR_mt_path_at_gateway_edison() for flit " << f->id << endl;
    }
    
    int no_of_MIN_paths_to_consider = 1;        
                            //the path is already in the flit

    int no_of_VLB_paths_to_consider = 2;  //can change this in future 
    
    int temp, chosen_pathID, chosen_pathID1, chosen_pathID2;
        
    std::vector< std::vector<int> > paths;
    std::vector<int> imdt_nodes;
    
    //int min_q_len;
    //int chosen_tier = 0; //only useful for multi-tiered routing

    int src_group, dst_group;
    int ii, jj;

    int return_val = -1; 

    int min_q_len;

    int chosen_tier = -1;


    src_group = src_router / ed_a;
    dst_group = dst_router / ed_a;

    paths.resize(no_of_MIN_paths_to_consider + no_of_VLB_paths_to_consider); 
                        //1 min path, the rest non-min paths
        

    //In this phase, the min path is already selected.
    //Just look it up from the flit
    paths[0].resize(f->path.size() - f->hop_count);
    for (ii = f->hop_count, jj = 0; ii < f->path.size(); ii++, jj++){
        paths[0][jj] = f->path[ii];
    }

    if (flag){
        cout << "path stored in the flit is: ";
        print_vector(f->path);
        cout << "path to be considered as min path: ";
        print_vector(paths[0]);
    }

    //get the q_len of the min-path. 
    
    //the path is already copied and chopped to have the src_out_gatway 
    //at paths[0][0], so this will work.
    min_q_len = find_port_queue_len_to_node_edison(r, paths[0][0], paths[0][1]);

    if (flag){
        cout << "min_q_len: "  << min_q_len << endl;
    }

    //Now, get the non-min paths   
    std::vector<std::vector<int>> vlb_paths;
    vlb_paths.resize(no_of_VLB_paths_to_consider);
    
    chosen_tier = generate_vlb_paths_threshold_edison(f, src_router, dst_router, no_of_VLB_paths_to_consider, vlb_paths, min_q_len, "gateway");
            //acceptable value for chosen_tier is 0, 1 or 2.

    if (flag){
        cout << "generated non-min paths to imdt_gateway: " << endl;
        for(ii = 0; ii < vlb_paths.size(); ii++){
            print_vector(vlb_paths[ii]);
        }
    }

    //move the vlb_paths in the paths vector
    for(ii = 0, jj = no_of_MIN_paths_to_consider; ii < no_of_VLB_paths_to_consider; ii++, jj++){
        paths[jj] = std::move(vlb_paths[ii]);
    }

    //paths generated, now compare Q length and select one
    chosen_pathID = make_UGAL_L_path_choice_edison(r, f, no_of_MIN_paths_to_consider, no_of_VLB_paths_to_consider, paths);
    
    //if a vlb path was chosen, save the imdt_group_in_gateway in the flit
    if (chosen_pathID < no_of_MIN_paths_to_consider){
        return_val = 0; //min_path was selected
    }else{
        return_val = 1; //vlb_path was selected
        f -> routing_tier = chosen_tier;



        if (PACKET_CATCH3){
            // if (chosen_tier == 0){
            //     cout << "GATEWAY. vlb path selected for flit: " << f->id << " , ";
            //     cout << " tier: " << chosen_tier << " ~~~~" << endl;
            // }
            // if (chosen_tier == 1){
            //     cout << "GATEWAY. vlb path selected for flit: " << f->id << " , ";
            //     cout << " tier: " << chosen_tier << " @@@@@@@@" << endl;
            // }
            if (chosen_tier == 2){
                cout << "GATEWAY. vlb path selected for flit: " << f->id << " , ";
                cout << " tier: " << chosen_tier << " {}{}{}{}{}{}{}{}" << endl;
            }
            if (chosen_tier > 2){
                cout << "GATEWAY. vlb path selected for flit: " << f->id << " , ";
                cout << " tier: " << chosen_tier << " !!!!!!!!!!!!!!!!!!!!" << endl;
            }
        }

    }

    if (flag){
        cout << "chosen ugal_l path id: " << chosen_pathID << endl;
        cout << "chosen path: ";
        print_vector(paths[chosen_pathID]);
        cout << endl;
    }

    //ensure the flit will not be evaluated anymore.
    f->PAR_need_to_revaluate = false;
    f->src_out_gateway = -1;

    //Now copy the chosen path in pathVector
    pathVector = std::move(paths[chosen_pathID]);  

    if (flag){
        cout << "leaving select_PAR_mt_path_at_gateway_edison() for flit: " << f-> id << endl;
    }
        
    return return_val;   


}


void PAR_mt_src_edison( const Router *r, const Flit *f, int in_channel, OutputSet *outputs, bool inject){
    // Multi-tiered UGAL routing for PAR.
    // Src based decision making, so effectively it is PAR_mt_sc

    //Works same as UGAL_L_mt_sc, with one PAR decision-reevaluation.

    //There is a detailed comment on how it works at the definiton of 
    //UGAL_L_mt_sc, so check that to understand the inner workings.
    
    //First check if it is an injection port. If yes, do accordingly.
    
    bool flag = false;
    
    //flag = true;
    
    if (f->id == FLIT_TO_TRACK){
        flag = true;
    }
    
    if (flag){
        cout << "inside PAR_mt_edison() for flit: " << f-> id << endl;
    }
    
    if(inject) {
        outputs->Clear( ); //doesn't matter really. If the flit is at injection,
                            //means it was just generated, so the outputset is empty anyway.

        int inject_vc= RandomInt(gNumVCs-1);
        outputs->AddRange(-1, inject_vc, inject_vc);
        
        if (flag){
            cout << "injecting flit: " << f->id << " with vc: " << inject_vc << endl;
        }
          
        return;
    }
        
    //gather necessary info from the flit
    outputs -> Clear();
    
    int current_router = r->GetID();
    
    int out_port = -33;    //instead of setting all the default error values to -1, we used different values in different places. Works kinda like different error codes, helps in debugging.
    
    int out_vc = 0;
    
    int dst_PE = f->dest;
    int dst_router = dst_PE / ed_p;
    
    if (flag){
        cout << "current router:" << current_router << " , destination router: " << dst_router << endl;
    }

    //Then check if it is the destination router. 
    //If yes, then find the port to the destination PE.
    if(current_router == dst_router){
        out_vc = RandomInt(gNumVCs-1);
            
        //find the port that goes to the destination processing node
        out_port = dst_PE % ed_p;
        
        if (flag){
            cout << "destination router. ejecting through port " << out_port << " through vc " << out_vc << endl;
        }

        collect_path_stat(f);
    }
    
    else if (f->hop_count == 0){   //source router
        //if it is at the source router, then the input port must be a PE. Check if it is any different.
        if (flag){
            cout << "inside source router ..." << endl;
        }
        
        if (in_channel >= ed_p){
            cout << "EXCEPTION! Source router but port is not smaller than p" << endl;
            cout << "flitID: " << f->id << " src: " << f->src << " dest: " << f->dest  << " rID: " << r->GetID() << endl;
    
            cout << "p: " << ed_p << " in_channel: " << in_channel << endl;
            exit(-1);
        }
        
    
        //actual PAR routing needs to happen here.
        
        std::vector<int> pathVector;
        
        int chosen_path_type; 
        
        chosen_path_type = select_UGAL_L_mt_path_edison(r, f, current_router, dst_router, pathVector, "par"); 
                
        if (flag){
            cout << "select_ugal_l_mt_path_edison() returned: " << chosen_path_type << endl;
            cout << "the path returned is: ";
            print_vector(pathVector);
        }

        //Save the whole path in the flit
        f->path = std::move(pathVector);

        //depending on chosen path type, save the imdt_group_incoming_gateway in the flit.
        if (chosen_path_type == 0){
            //min path. no imdt_group_incoming_gateway
            f->imdt_gateway = -1;
        }else if (current_router/ed_a == dst_router/ed_a) { 
            //src and dst in the same group, so no imdt_gateway
            f->imdt_gateway = -1;
        }
        else{
            //non-min path. save the imdt_group_incoming_gateway
            f->imdt_gateway = f->path[f->path.size()-1];  
                // the vlb path should contain the gateway at the end, for this // particular routing only.
                // When both src and dst are inside the same group, we don't
                // have an imdt_gateway and in this case the dst_router
                // will be set to f->imdt_gateway. However, we are checking
                // if the dst_router is reached before we are checking this,
                // so should be safe.

            if (PAKCET_CATCH){
                cout << "NON-min path at src for flit: " << f->id << endl; 
            }
        }
        
        //now get the port to the next hop node 
        out_port = find_port_to_node_edison(current_router, f->path[1], f);
        
        if(flag){
            cout << "port to next router is: " << out_port << endl;
        }
        
        //assign vc. first hop, so vc 0.
        out_vc = 0;
        
        //increase hop_count
        f->hop_count += 1;
    
    }

    //PAR decision re-evaluation phase
    else if ( (f->PAR_need_to_revaluate == true ) && ( current_router == f-> src_out_gateway) ){
        
        if (flag){
            cout << "inside src_out_gateway router ..." << endl;
        }
        
        std::vector<int> pathVector;
        int chosen_path_type;

        chosen_path_type = select_PAR_mt_path_at_gateway_edison( r, f, current_router, dst_router, pathVector);

        if (flag){
            cout << "select_PAR_path_at_gateway_edison() returned: " << chosen_path_type << endl;
            cout << "the path returned is: ";
            print_vector(pathVector);
        }

        if (chosen_path_type == 1){  //non-min path was chosen. 
                                     //Path update necessary
            if (PAKCET_CATCH){
                cout << "min updated to NON-min path at gateway for flit: " << f->id << endl; 
            }

            //We want to record the whole path a flit takes.
            //So insert the nodes upto the gateway in front of the new path
            for (int ii = f->hop_count - 1; ii >= 0; ii--){
                pathVector.insert(pathVector.begin(), f->path[ii]);
            }

            //Save the whole path in the flit 
            f->path = std::move(pathVector);
            if (flag){
                cout << "the complete path is: ";
                print_vector(f->path);
            }

            //new vlb path, so update the imdt_gateway
            f->imdt_gateway = f->path[f->path.size()-1];  
                // the vlb path should contain the gateway at the end, for this // particular routing only.
                // When both src and dst are inside the same group, we don't
                // have an imdt_gateway and in this case the dst_router
                // will be set to f->imdt_gateway. However, we are checking
                // if the dst_router is reached before we are checking this,
                // so should be safe.

        }else{
            if (PAKCET_CATCH){
                cout << "min path all the way for flit: " << f->id <<  endl; 
            }
        
        }

        //now get the port to the next hop node 
        out_port = find_port_to_node_edison(current_router, f->path[f->hop_count + 1], f);
        
        if(flag){
            cout << "port to next router is: " << out_port << endl;
        }
        
        //assign vc 
        out_vc = f->hop_count;
        
        //increase hop_count
        f->hop_count += 1;
    
    }

    //take intermediate group routing decision
    else if(current_router == f->imdt_gateway){
        //Vanilla mode: go to an intermeaidate node. Then go to the destination.
        std::vector<int> pathVector;
        
        int temp = select_imdt_group_mt_path_edison(r, f, current_router, dst_router, pathVector, ed_routing_mode);

        if (flag){
            cout << "select_imdt_group_mt_path_edison() returned: " << temp << endl;
            cout << "the path returned is: ";
            print_vector(pathVector);
        }

        //append the path at the end of the current path
        //DO NOT CLEAR the exisiting path-portion. Just append.
        f->path.reserve(f->path.size() + pathVector.size() -1);
        f->path.insert(f->path.end(), pathVector.begin() + 1, pathVector.end());

        // clear the f->imdt_gateway entry. Otherwise this might introduce a
        // cycle if the subsequent path happens to go throguh this node again.
        f->imdt_gateway = -2; 
                //a unique value just for future debugging purposes, if needed

        if (flag){
            cout << "complete path saved in the flit: ";
            print_vector(f->path); 
        }

        //now get the port to the next hop node 
        out_port = find_port_to_node_edison(current_router, f->path[ f->hop_count + 1], f);
        
        if(flag){
            cout << "port to next router is: " << out_port << endl;
        }
        
        //assign vc
        out_vc = f->hop_count;
        
        //increase hop_count
        f->hop_count += 1;
    
    }

    else{  //neither src nor dst router. Packet in flight.
        //get current hop count 
        
        //get port to the next hop node
        out_port = find_port_to_node_edison(current_router, f->path[ f->hop_count + 1], f);
        
        //assign vc 
        out_vc = f->hop_count;
        
        if (flag){
            cout << "port to next hop: " << out_port << " through vc: " << out_vc << endl; 
        }
        
        //increase hop_count
        f->hop_count += 1;
        
    }

    //finally, build the output set
    outputs->AddRange( out_port, out_vc, out_vc );
    
    if (flag){
        cout << "leaving UGAL_L_mt_dragonflyfull() for flit: " << f-> id << endl;
    }



}