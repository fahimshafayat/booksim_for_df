/*
Author: Shafayat R
Date: 28 Feb 2019

A function that generates combination(N, K) for given N and K.
*/

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include "combinations.hpp"

void combinations(int N, int K, std::vector <std::vector <int> >& combos)
{
    /*
    Generates all the combinations of length K of N, and stores them in combos.
    */
    
    //first allocate the 2D vector, populated by -1;
    //The size of the vector depends on N and K. So determine the size first.
    long int temp1, temp2;
    long int len;
    temp1 = 1;
    temp2 = 1;
    int ii, jj, combo_count, bit_count;
    
    for (ii = 0, jj = N; ii < K; ii++, jj--)
    {
        temp1 = temp1 * jj;
    }
    
    for(ii = 0, jj = 1; ii < K; ii ++, jj++){
        temp2 = temp2 * jj;
    }
    
    len = temp1/temp2;

    //now allocate the vector
    combos.clear();
    combos.resize(len, std::vector<int> (K, -1) );
    
    //Main logic starts here. 
    //It just uses all the possible permutation of K 1's and (N-K) 0's.
    //For all permutation, it uses the indexes of the bits marked with 1.

    std::string bitmask(K, 1); // K leading 1's
    bitmask.resize(N, 0); // N-K trailing 0's
   
    // generate combinations and permute bitmask
    combo_count = 0;
    do {
        for (ii = 0, bit_count = 0; ii < N; ++ii) // [0..N-1] integers
        {
            if (bitmask[ii] == 1) {
                combos[combo_count][bit_count] = ii;
                bit_count += 1;
            }
        }
        combo_count += 1;
    } while (std::prev_permutation(bitmask.begin(), bitmask.end()));
    
}



