#ifndef _COMBINATIONS_HPP_
#define _COMBINATIONS_HPP_

void combinations(int N, int K, std::vector <std::vector <int> >& combos);

#endif