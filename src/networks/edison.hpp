#ifndef _Edison_HPP_
#define _Edison_HPP_

/*
Bare-bone implementation of Edison (Cray Cascade) topology.

Will use Peyman's implementation as a guide, but still starting a new file instead of modifying his as this is supposed to be cleaner.

- Shafayat, Nov 20 2018
*/

#include "network.hpp"
#include "pair_hash.hpp"
#include "routefunc.hpp"

class Edison : public Network{
    int _dim; //dimension
    int _a; //no of routers in a group
    int _g; //no of Edison groups
    int _p; //no of processors per router
    int _N; //no of routers in the system
    int _radix; //router radix
    int _local_latency;
    int _global_latency;
    string _routing;

    std::vector<int> out_port_tracker;  //needed to keep trakc of global links only
    std::vector<int> in_port_tracker;

    void _ComputeSize( const Configuration &config );
    void _BuildNet( const Configuration &config );

    void _AllocateArrays();

    //read from file to edison_global_adj structure
    int _CountGlobalLinks(const string & name);
    void _ConnectGlobalLinks(const string & name);

    void _setGlobals();
    void _setRoutingMode();
    void _BuildGraphForLocal();
    void _BuildASingleGroup(int group_id);
    void _BuildGraphForGlobal();
    void _CreatePortMap();

    void _printGraph();

public:
    Edison( const Configuration &config, const string & name );
    static void RegisterRoutingFunctions();  
};

int find_port_to_node_edison(int current_router, int next_router, const Flit *f);

int select_min_path_edison(int src_router, int dst_router, std::vector<int> & pathVector, const Flit *f);
int select_min_path_inside_group_edison(int src_router, int dst_router, std::vector<int> & pathVector, const Flit *f);

int select_two_min_paths_inside_group_edison(int src_router, int dst_router, std::vector< int > & pathVector1, std::vector< int > & pathVector2, const Flit *f);
                //this will be useful for selecting ugal_l paths

void min_edison( const Router *r, const Flit *f, int in_channel, OutputSet *outputs, bool inject);

int select_vlb_path_regular_edison(int src_router, int dst_router, std::vector<int> & pathVector, const Flit *f);
int select_vlb_path_inside_group_edison(int src_router, int dst_router, int mode, std::vector<int> & pathVector, const Flit *f);
int find_max_four_hop_vlb_paths_in_group(int src_router, int dst_router, std::vector<int> & pathVector, const Flit *f);

int vlb_intermediate_node_vanilla_edison(int src_router, int dst_router, const Flit *f);

void vlb_edison( const Router *r, const Flit *f, int in_channel, OutputSet *outputs, bool inject);


int select_UGAL_L_path_edison(const Router *r, const Flit *f, int src_router, int dst_router, std::vector<int> & pathVector, string routing = "ugal_l");

void UGAL_L_edison( const Router *r, const Flit *f, int in_channel, OutputSet *outputs, bool inject);

int find_port_queue_len_to_node_edison(const Router *r, int current_router, int next_router);

int generate_imdt_nodes_in_group_edison(const Flit *f, int src_router, int dst_router, int no_of_nodes_to_generate, std::vector<int> & imdt_nodes);
int generate_vlb_path_from_given_imdt_node_vanilla_edison(const Flit *f, int src_router, int dst_router, int imdt_router,  std::vector<int> & pathVector);
int generate_imdt_nodes_vanilla_edison(const Flit *f, int src_router, int dst_router, int no_of_nodes_to_generate, std::vector<int> & imdt_nodes);
int make_UGAL_L_path_choice_edison(const Router *r, const Flit *f, int no_of_min_paths_to_consider, int no_of_VLB_paths_to_consider, std::vector< std::vector<int> > paths);


void UGAL_L_mt_edison( const Router *r, const Flit *f, int in_channel, OutputSet *outputs, bool inject);
int select_UGAL_L_mt_path_edison(const Router *r, const Flit *f, int src_router, int dst_router, std::vector<int> & pathVector, string routing = "ugal_l");
int generate_vlb_paths_in_group_threshold_edison(const Flit *f, int src_router, int dst_router, int no_of_VLB_paths_to_consider, std::vector< std::vector<int> > & pathVectors, int min_q_len);
//int generate_imdt_nodes_threshold_edison(const Flit *f, int src_router, int dst_router, int no_of_node_to_generate, std::vector<int> & imdt_nodes, int min_q_len);
int generate_vlb_paths_threshold_edison(const Flit *f, int src_router, int dst_router, int no_of_vlb_paths_to_generate, std::vector< std::vector<int>> & pathVectors, int min_q_len, string PAR_phase = "src");

//int generate_three_hops_imdt_nodes_in_group_edison(const Flit *f, int src_router, int dst_router, int no_of_nodes_to_generate, std::vector<int> & imdt_nodes);
int generate_three_hop_vlb_paths_in_group_edison(const Flit *f, int src_router, int dst_router, int no_of_VLB_paths_to_consider, std::vector<std::vector<int> > & pathVectors);
//int generate_three_hops_imdt_nodes_edison(const Flit *f, int src_router, int dst_router, int no_of_nodes_to_generate, std::vector<int> & imdt_nodes);
int generate_one_hop_vlb_paths_to_gateway_edison(const Flit *f, int src_router, int dst_router, int no_of_vlb_paths_to_generate, std::vector<std::vector<int> > & pathVectors);

int generate_regular_vlb_paths_to_gateway_edison(const Flit *f, int src_router, int dst_router, int no_of_vlb_paths_to_generate, std::vector<std::vector<int> > & pathVectors);

int select_imdt_group_mt_path_edison(const Router *r, const Flit *f, int current_router, int dst_router, std::vector<int> & pathVector, string mt_routing_mode);


void PAR_edison( const Router *r, const Flit *f, int in_channel, OutputSet *outputs, bool inject);

int select_PAR_path_at_gateway_edison( const Router *r, const Flit *f, int src_router, int dst_router, std::vector<int> & pathVector);


void PAR_mt_src_edison( const Router *r, const Flit *f, int in_channel, OutputSet *outputs, bool inject);

int select_PAR_mt_path_at_gateway_edison( const Router *r, const Flit *f, int src_router, int dst_router, std::vector<int> & pathVector);


void print_vector(std::vector<int> & vec);

int get_inode_on_same_row_or_column(const Flit *f, int src_group, int dst_group, int src_row, int dst_row, int src_col, int dst_col);
int get_inode_on_same_row_or_column_of_node(const Flit *f, int node);

void collect_path_stat(const Flit *f);

#endif



// Flits to debug with:

// 60385. src_group NOT = dst_group. flit, src, dst : 60385,14,1106 Y

// 58838. src_group = dst_group. flit, src, dst : 58838,688,689 Y

// 57780. choosing MIN path. path id: 1 for flit 57780

// 56392. choosing VLB path. path id: 2 for flit 56392

