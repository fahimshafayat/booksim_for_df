#include <iostream>

#include "global_stats.hpp"

bool comparator_for_counters( std::pair< std::pair<int, int>, struct Counters > e1, std::pair< std::pair<int, int>, struct Counters > e2 ) {
    if (e1.first.first == e2.first.first){
        return (e1.first.second <= e2.first.second);
    }else{
        return (e1.first.first < e2.first.first);
    }   
}
