
# coding: utf-8

# In[55]:


import pandas as pd
import numpy as np
print("Hello World!")


# In[56]:


filename1 = "a8_g9_17_25_33_min_vlb_ugal_mt_per_permt_summary.csv"
filename2 = "a8_g9_17_25_33_ugal_restricted_and_2hop.summary.csv"


# In[57]:


df1 = pd.read_csv(filename1)
print(df1.shape)
print(df1.columns)


# In[58]:


df1.rename(columns=lambda x: x.strip(), inplace=True)
print(df1.shape)
print(df1.columns)


# In[59]:


df2 = pd.read_csv(filename2)
print(df2.shape)
print(df2.columns)


# In[60]:


df2.rename(columns=lambda x: x.strip(), inplace=True)
print(df2.shape)
print(df2.columns)


# In[61]:


df1 = df1.dropna(axis = 0, how = "all")
print(df1.shape)
print(df1.columns)


# In[62]:


df2 = df2.dropna(axis = 0, how = "all")
print(df2.shape)
print(df2.columns)


# In[63]:


df3 = pd.concat([df1, df2], axis = 0, ignore_index = True)
print(df3.shape)
print(df3.columns)


# In[64]:


df3.head(5)


# In[65]:


#to sort by routing_functions, we need to assign an order to them.
#sort order for routings are: {"min": 0, "vlb":1, "UGAL_L":2, "UGAL_L_two_hop":3, "UGAL_L_restricted": 4, "UGAL_L_threshold":5, "UGAL_L_multi_tiered" : 6, "PAR" : 7, "PAR_multi_tiered" : 8,  }

def get_routing_index(row):
    routing_order = {"min": 0, "vlb":1, "UGAL_L":2, "UGAL_L_two_hop":3, "UGAL_L_restricted": 4, "UGAL_L_threshold":5, "UGAL_L_multi_tiered" : 6, "PAR" : 7, "PAR_multi_tiered" : 8  }
    txt = row["routing_function"].strip()
    return routing_order[txt]

df3["routing_idx"] = df3.apply(get_routing_index, axis = 1)

df3.head(5)


# In[66]:


#ok, database joined. Let's sort.

#sort by traffic, df_g, seed, shift_by_group, perm_seed, routings

sorted_df = df3.sort_values(by = ['traffic', 'df_g', 'seed',
       'shift_by_group', 'perm_seed','routing_idx'])

sorted_df.head(30)


# In[67]:


sorted_df.to_csv("combined.csv", index = False)


# In[79]:


#include separator lines
fp  = open("combined.csv","r")
fp2 = open("combined_and_formatted.csv","w")

for idx,line in enumerate(fp):
    fp2.write(line)
    if (idx) % 8 == 0:
        fp2.write("\n")
        
fp.close()
fp2.close()
print("written.")

