#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 10 18:37:14 2019

@author: rahman
"""

from multiprocessing import Pool, Lock
import time
import os

def f(tup):
    (dic1,dic2) = tup
    lock.acquire()
    try:
        print("starting process with id ", os.getpid())
        print("dic1", dic1)
        print("dic2", dic2)
    finally:
        lock.release()
        
    res =  dic1["a"] * dic2["b"]
    time.sleep(3)
    
    lock.acquire()
    try:
        print("ending process with id ", os.getpid())
    finally:
        lock.release()
    
    return res

def init_child(_lock):
    global lock
    lock = _lock

if __name__ == "__main__":
    
    A = [x*10 for x in range(50)]
    B = [x for x in range(50)]
    
    params = []
    
    print("cpu count:", os.cpu_count())
    
    for (a,b) in zip(A,B):
        dict1 = {}
        dict2 = {}
        dict1["a"] = a
        dict2["b"] = b
        params.append((dict1,dict2))
    
    lock = Lock()
    
    #with Pool(3, initializer = init_child, initargs = (lock,)) as p:
    with Pool(os.cpu_count()) as p:
        results = p.map(f, params)
        
    print("results:", results)