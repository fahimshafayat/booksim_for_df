import os
import multiprocessing as MP
from multiprocessing import Pool, Lock
import math
import sys
import datetime

#for multiprocessing, each process gets its own copy of globals in its own memory pool,
#so using global is safe. It might be considered bad design though.

topLevelOutputFolderName = "" #set downunder
secondLevelFolderName = ""    #changed downunder
thirdLevelFolderName = ""    #changed downunder

parameters_default = {
                    "vc_buf_size"               :   64,
                    "wait_for_tail_credit"      :   0,
                    "vc_allocator"              :   "separable_input_first", 
                    "sw_allocator"              :   "separable_input_first",
                    "alloc_iters"               :   1,
                    "credit_delay"              :   2,
                    "routing_delay"             :   0,
                    "vc_alloc_delay"            :   1,
                    "sw_alloc_delay"            :   1,
                    "st_final_delay"            :   1,
                    "input_speedup"             :   1,
                    "output_speedup"            :   1,
                    "sim_count"                 :   1,
                    "num_vcs"                   :   7,
                    "priority"                  :   "none",
                    "packet_size"               :   1,
                    "injection_rate_uses_flits" :   1,
                    "internal_speedup"          :   4.0,
                    "warmup_periods"            :   3,
                    "sample_period"             :   1000  
                    }


#may be change to params dict
def setSecondLevelFolderName(params):
    global secondLevelFolderName
    
    #secondLevelFolderName = "Q_{}_seed_{}_traffic_{}_tSeed_{}_permSeed_{}".format(params["slimflyQ"], params["seed"],params["traffic"], params["traffic_seed"], params["perm_seed"])
    for k,v in params.items():
        if (k == "topology") or (k == "df_arrangement"):
            continue 
        secondLevelFolderName += k + "_" + str(v) + "_" 
    
    secondLevelFolderName = secondLevelFolderName[:len(secondLevelFolderName)-1] #to chop off the trailing underscore
    
    pass
    

#change to params dict
def setThirdLevelFolderName(params):
    global thirdLevelFolderName
    
    for k,v in params.items():
        thirdLevelFolderName += k + "_" + str(v) + "_" 
    
    thirdLevelFolderName = thirdLevelFolderName[:len(thirdLevelFolderName)-1] #to chop off the trailing underscore
    
    pass


def launchSimulation(second_level_params, third_level_params, injection_rate, count):
    
    
    outputFolderName = topLevelOutputFolderName + "/" + secondLevelFolderName+ "/" + thirdLevelFolderName 
    
    lock.acquire()
    try:
        print("second_level_params:", second_level_params)
        print("third_level_params:", third_level_params)
        print("topLevelOutputFolderName:", topLevelOutputFolderName)
        print("secondLevelFolderName:", secondLevelFolderName)
        print("thirdLevelFolderName:", thirdLevelFolderName)
        print("outputFolderName:", outputFolderName, "\n")
    finally:
        lock.release()
    
    
    os.makedirs(outputFolderName, mode=0o777, exist_ok= True)

    
    inputFileName = outputFolderName + "/"
    
    inputFileName += "irate" + "_" + "{:.0f}".format(injection_rate*100)
    
    fp = open(inputFileName,"w")

    #then give the changing values        
    parameters = "" #initializing
    
    for k,v in parameters_default.items():
        parameters += "{} = {};\n".format(k, str(v))
    
    for k,v in second_level_params.items():
        parameters += "{} = {};\n".format(k, str(v))
    
    for k,v in third_level_params.items():
        parameters += "{} = {};\n".format(k, str(v))
    
    parameters += "injection_rate = {:.2f};\n".format(injection_rate)
    
    fp.write(parameters)
    
    fp.close()


    #run the simulation. Save the result to a specific output file.
    outputFileName = inputFileName  + ".out"    #inputFilename already contains the outputFolderName2 as part of its name
    repeated_irate = False
        
    if os.path.exists(outputFileName) == False:
        command = exe_name + " " + inputFileName + " > " + outputFileName
#        lock.acquire()
#        try:
#            print(command)
#        finally:
#            lock.release()
#            
        #******************************************************************
        os.system(command)
        #******************************************************************
        
    else:
#        lock.acquire()
#        try:
#            print("skipping simulation for " + inputFileName + " as it was already done at last round ...")
#        finally:
#            lock.release()
        
        repeated_irate = True
    
    #simulation done. Now open the output files, scrap important info, present in a final output file.
    
    summaryResultFileName = outputFolderName + "/" + secondLevelFolderName
    
    summaryResultFileName += ".summary.txt"
    
#    lock.acquire()
#    try:
#        print("*********** summaryResultFileName: ", summaryResultFileName)
#    finally:
#        lock.release()
#        
    
    fp2 = open(summaryResultFileName,"a")
    
    if count == 1:
        parameters = ""
        
        for k,v in parameters_default.items():
            parameters += "{} = {};\n".format(k, str(v))
    
        for k,v in sorted(second_level_params.items()):
            parameters += "{} = {};\n".format(k, str(v))
        for k,v in sorted(third_level_params.items()):
            parameters += "{} = {};\n".format(k, str(v))
        fp2.write(parameters)
        fp2.write("\n")
        fp2.write("defaultText, count, i-rate in config file, i-rate injected, i-rate accepted, packet latency, flit latency, deadlock?\n")

    #for outputFile in outputFiles:
    fp = open(outputFileName,"r")
    text = fp.readlines()

    line1 = ""
    line2 = ""
    line3 = ""
    line4 = ""
    iRate = 0.0
    aRate = 0.0
    p_latency = 0.0
    f_latency = 0.0
    deadlock = False
    flag1 = False
    flag2 = False
    flag3 = False
    flag4 = False

    #extra multi-tiered ruting stats added later
    flits_through_min = 0
    flits_through_non_min = 0
    r_tier_dist_when_min_taken = ""
    r_tier_dist_when_non_min_taken = ""
    non_min_pathlen_dist_when_min_taken = ""
    non_min_pathlen_dist_when_non_min_taken = ""

    for line in text[::-1]:
        if "Accepted flit rate average" in line and flag1 is False:
            line1 = line
            flag1 = True
        if "Injected flit rate average" in line and flag2 is False:
            line2 = line
            flag2 = True
        if "Flit latency average" in line and flag3 is False:
            line3 = line
            flag3 = True
        if "Packet latency average" in line and flag4 is False:
            line4 = line
            flag4 = True  

        if "Total flits through min paths" in line:
            flits_through_min = int(line[line.find(":") + 2 : ])
        if "Total flits through non-min paths" in line:
            flits_through_non_min = int(line[line.find(":") + 2 : ])
        if "routing tier distribution when non-min paths are not chosen" in line:
            r_tier_dist_when_min_taken = (line[line.find(":") + 2 : ]).strip()
        if "routing tier distribution when non-min paths are chosen" in line:
            r_tier_dist_when_non_min_taken = (line[line.find(":") + 2 : ]).strip()
        if "non-min path length distribution when non-min paths are not chosen" in line:
            non_min_pathlen_dist_when_min_taken = (line[line.find(":") + 2 : ]).strip()
        if "non-min path length distribution when non-min paths are chosen" in line:
            non_min_pathlen_dist_when_non_min_taken = (line[line.find(":") + 2 : ]).strip()
        



    if line1 != "":
        iRate = float(line1[line1.find("=")+2 : line1.find("(")])
    if line2 != "":
        aRate = float(line2[line2.find("=")+2 : line2.find("(")])
    if line3 != "":
        f_latency = float(line3[line3.find("=")+2 : line3.find("(")])        
    if line4 != "":
        p_latency = float(line4[line4.find("=")+2 : line4.find("(")])        
    
    for line in text[::-1]:
        if "Simulation unstable, ending " in line:
            deadlock = True
            break
        if "WARNING: Possible network deadlock" in line:
            deadlock = True
            break
        if "At least one router queue full" in line:
            deadlock = True
            break


        
    result = "simulation,{:>10},{:>10.5},{:>10.5},{:>10.5},{:>10.5},{:>10.5},{:>10},  {},  {},  {},  {},  {},  {}\n".format(count,injection_rate,iRate,aRate,p_latency,f_latency,deadlock,flits_through_min,flits_through_non_min, r_tier_dist_when_min_taken, r_tier_dist_when_non_min_taken, non_min_pathlen_dist_when_min_taken, non_min_pathlen_dist_when_non_min_taken)
     
    if repeated_irate is False:
        fp2.write(result)

    fp.close()
    fp2.close()

    #cleanup. Remove the input files.
    os.remove(inputFileName)
    
    return deadlock
    
    pass    




def probeDeeperAndDeeperBinarySearch(param_tuple, lowLim = 1, highLim = 99):
    #no global. bad bad global.
    
    second_level_params, third_level_params = param_tuple
    
#    lock.acquire()
#    try:
#        temp_str = "process started with "
#        for k,v in second_level_params.items():
#            temp_str += " " + k + " : " + str(v) + " , "
#        for k,v in third_level_params.items():
#            temp_str += " " + k + " : " + str(v) + " , "
#        temp_str += " pid: {}".format(os.getpid())
#        print(temp_str)  
#    finally:
#        lock.release()
#    
        
    setSecondLevelFolderName(second_level_params)
    
    setThirdLevelFolderName(third_level_params)
    
    low = lowLim
    high = highLim
    
    count = 1
    satLow = launchSimulation(second_level_params, third_level_params, low*0.01, count) 
    
    count = 2
    satHigh = launchSimulation(second_level_params, third_level_params, high*0.01, count)
    
#    if satLow == True or satHigh == False:
#        sys.exit("Incorrect low and high i_rate settings. Exiting.")    
#   
    if satLow == True:
                
        lock.acquire()
        try:
            print("saturated at ", lowLim, "%. Won't continue.")
        finally:
            lock.release()
    
    elif satHigh == False:
        lock.acquire()
        try:
            print("Not saturated at ", highLim, "%. Won't continue.")
        finally:
            lock.release()
    
    else:
        
        while( (high - low) > 1):    
            mid = math.floor((low + high)/2)
            count += 1
            satMid = launchSimulation(second_level_params, third_level_params, mid*0.01, count)
            if satMid is True:
                high = mid
            else:
                low = mid
                    
        #mid was the last one we ran a test on. 
        #Because we are now more concerned abour latency, so let's run a few more simulations
        #in the non-saturated zone. We'll reduce the i-rate by 0.05 four times.
        irate = (mid//5) * 5
        count2 = 0
        while(irate > 0 and count2 < 4):
            count += 1
            launchSimulation(second_level_params, third_level_params, irate*0.01, count)
            irate -= 5
            count2 += 1
            
    #    lock.acquire()
    #    try:
    #        temp_str = "process ending with "
    #        temp_str += " pid: {}".format(os.getpid())
    #        print(temp_str)  
    #    finally:
    #        lock.release()
    #    
    return 0

    pass

def create_process_pool():
    
    #put the defaults
    parameters_specific =            {
                        "topology"          :   "dragonflyfull",     
                        "df_a"              :   4,
                        #"df_g"              :   33,
                        "df_arrangement"    :   "absolute_improved",
                        "traffic"           :   "randperm",
                        #"seed"              :   11,
                        #"perm_seed"         :   42,
                        "multitiered_routing_threshold_0" : 10,
                        "multitiered_routing_threshold_1" : 50,
                        "global_latency" : 10,
                        "local_latency" : 10,
                        }

    #exe_name = "../../booksim"    #global
    
    global topLevelOutputFolderName
    
    topLevelOutputFolderName = "a_{}_{}_{}_{}_{}_{}_{}".format(parameters_specific["df_a"], parameters_specific["traffic"], now.year, now.month, now.day, now.hour, now.minute)
    os.makedirs(topLevelOutputFolderName, mode=0o777, exist_ok= True)

    #parameters we want to loop over with
    Gs = [4,5]

    #shift_by_groups = [2,9,16]
    perm_seeds = [60, 62, 64,66]
    #seeds = [10,12,14,16]

    #traffics = ["randperm"]
    routing_functions = ["UGAL_L_multi_tiered", "UGAL_L"]
    
    processes = []
    
    parameter_list = []
    
    for df_g in Gs:
        for routing_function in routing_functions:
            for perm_seed in perm_seeds:
            #for shift_by_group in shift_by_groups:
            #for seed in seeds:
                second_level_params = {}
                third_level_params = {}
                    
                for k,v in parameters_specific.items():
                    second_level_params[k] = v
                
                third_level_params["routing_function"] = routing_function
                third_level_params["df_g"] = df_g
                
                #third_level_params["seed"] = seed
                #third_level_params["shift_by_group"] = shift_by_group
                third_level_params["perm_seed"] = perm_seed
                
                parameter_list.append((second_level_params, third_level_params))
                
    return parameter_list    
    
    pass

if __name__ == "__main__":
    print("Hello World!")
    
    now = datetime.datetime.now()
    
    exe_name = "../src/booksim"    #global
    #exe_name = "/home/rahman/Codes/Booksim_for_DF/booksim_for_df/src/booksim"
    
    processes = []
    
    parameter_list = create_process_pool()
    
    lock = Lock()
    
    process_count = os.cpu_count()
    #process_count = 2
    
    with Pool(process_count) as p:
        results = p.map(probeDeeperAndDeeperBinarySearch, parameter_list)

    print("------ All process ended.")
    
    then = datetime.datetime.now()
    
    print("total time: ", then-now, "seconds")
        
    print("The world never says hello back.")
    
